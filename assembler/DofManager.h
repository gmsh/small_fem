#ifndef _DOFMANAGER_H_
#define _DOFMANAGER_H_

#include <string>
#include <vector>
#include <map>
#include <set>

#include "Dof.h"

/**
   @class DofManager
   @brief This class manages the degrees of freedom (Dof)

   This class numbers the degrees of freedom (Dof).

   It can map a Dof to a unique number, called global ID.

   In addtion, this class allows to assign a Dof to a given value.
   A Dof that has been assigned to a value is called a fixed Dof.
   The global ID of a fixed Dof is not unique and is equal to
   DofManager::isFixedId().

   A DofManager can be distributed across multiple computing node,
   or it can be local to each computing node.
   It the first case, every Dof will have a unique number, no matter the node.
   In the second case, Dof may have different unique number,
   depending on the node.

   Finaly, the global IDs given to the unfixed Dof%s ranges from 0 to
   (total number of Dof - number of fixed Dof - 1).
*/

namespace sf{
  template<typename scalar> class DofManager{
  private:
    static const size_t isFixed;
    static const size_t isUndef;

  private:
    bool local;

    std::vector<std::vector<size_t> > globalIdV;
    std::map<Dof, size_t>             globalIdM;
    std::map<Dof, scalar>             fixedDof;

    size_t nTotUnfixedLocalDof;
    size_t nTotUnfixedGlobalDof;

    size_t first;
    size_t last;

  public:
    static size_t isFixedId(void);

  public:
    DofManager(bool isLocal);
    DofManager(void);
    ~DofManager(void);

    bool   isLocal(void)       const;
    size_t getLocalSize(void)  const;
    size_t getGlobalSize(void) const;

    void addToDofManager(const Dof& dof);
    void addToDofManager(const std::set<Dof>& dof);
    void addToDofManager(const std::vector<std::vector<Dof> >& dof);
    void generateGlobalIdSpace(void);

    std::set<Dof> getAllDofs(void) const;

    size_t getGlobalId(const Dof& dof)              const;
    void   getGlobalId(const std::vector<Dof>& dof,
                       std::vector<size_t>& gId)    const;
    size_t getGlobalIdSafe(const Dof& dof)          const;
    void   sortDof(std::vector<Dof>& v)             const;

    bool   fixValue(const Dof& dof, scalar value);
    scalar getValue(const Dof& dof) const;

    std::string toString(void)                 const;
    std::string toPre(void)                    const;
    std::string toPre(bool entityOffset)       const;
    void        writePre(std::string filename) const;

  private:
    void localSpace (void);
    void globalSpace(void);

    void count(std::map<Dof, size_t>& dof);
    void retag(std::map<Dof, size_t>& dof, std::map<Dof, scalar>& fix);
    void vectorize(void);

    std::pair<bool, size_t> findSafe(const Dof& dof) const;

    std::string toStringFromMap(void) const;
    std::string toStringFromVec(void) const;

  private:
    struct SortPredicateFunctor{
      const DofManager<scalar>& dofM;
      SortPredicateFunctor(const DofManager<scalar>& a);
      bool operator()(const Dof& a, const Dof& b);
    };
  };
}

/**
   @fn DofManager::isFixedId

   Fixed Dof got a special global ID (which is the highest possible number).

   @return Returns the special ID of a fixed Dof
   **

   @fn DofManager::DofManager(bool)
   @param isLocal A boolean value

   Instantiates a new DofManager.
   If isLocal is true, a local DofManager will be created.
   If isLocal is false, a distributed DofManager will be created.
   **

   @fn DofManager::DofManager(void)

   Same as DofManager::DofManager(true)
   **

   @fn DofManager::~DofManager

   Deletes this DofManager
   **

   @fn DofManager::isLocal(void)
   @return Returns true if this DofManager is local,
   and false if it is distributed.
   **

   @fn DofManager::getLocalSize
   @return Returns the number of local Unfixed Dof%s in this DofManager
   **

   @fn DofManager::getGlobalSize
   @return Returns the number of Unfixed Dof%s in this DofManager
   across all nodes

   If DofManager::isLocal() is true, then DofManager::getGlobalSize()
   is equal to DofManager::getLocalSize()
   **

   @fn DofManager::addToDofManager(const Dof& dof);
   @param dof A Dof

   Adds the given Dof in this DofManager.
   **

   @fn DofManager::addToDofManager(const std::set<Dof>& dof);
   @param dof A set of Dof%s

   Adds the given Dof%s in this DofManager.
   The same Dof may be insterd multiple time,
   but it will be given the same unique ID.
   **

   @fn DofManager::addToDofManager(const std::vector<std::vector<Dof> >& dof);
   @param dof A vector of vector of Dof%s

   Adds the given Dof%s in this DofManager.
   The same Dof may be insterd multiple time,
   but it will be given the same unique ID.
   **

   @fn DofManager::generateGlobalIdSpace

   Numbers every non fixed Dof of this DofManager.
   Each Dof will be given a unique global ID.
   **

   @fn DofManager::getAllDofs(void) const
   @return Returns a set<Dof> with all the Dofs in this DofManager
   **

   @fn DofManager::getGlobalId(const Dof& dof) const
   @param dof The Dof from which we want the global ID
   @return Returns the global ID of the given Dof

   If the requested Dof has not been added to this DofManager,
   or if DofManager::generateGlobalIdSpace() has not been called,
   the behaviour of this method is unpredicable.
   Actually, it will most probably lead to a process crash.

   @see DofManager::getGlobalIdSafe
   **

   @fn DofManager::getGlobalId(const std::vector<Dof>& dof, std::vector<size_t>& gId) const
   @param dof A vector of Dof
   @param gId A vector of integers

   Polpulates the vector gId, such that: gId[i] = globalId(dof[i])
   **

   @fn DofManager::getGlobalIdSafe
   @param dof The Dof from which we want the global ID
   @return Returns the global ID of the given Dof

   If the requested Dof has not been added to this DofManager,
   or if DofManager::generateGlobalIdSpace() has not been called,
   an Exception is thrown.

   This method is safer but slower than DofManager::getGlobalId().

   @see DofManager::getGlobalId
   **

   @fn DofManager::sortDof
   @param v A vector of Dof%s

   Sorts the given vector such that:
   getGlobalId(v[0]) < getGlobalId(v[1]) < ... < getGlobalId(v[end-1])
   **

   @fn DofManager::fixValue
   @param dof A Dof
   @param value A real number

   Fixes the given Dof to the given value.

   @return Returns:
   @li true, if the operation is a success
   @li false, otherwise

   Here are three important cases, where DofManager::fixValue() will fail:
   @li The given Dof is not in this DofManager
   @li The given Dof is already fixed
   @li DofManager::generateGlobalIdSpace() has been called
   **

   @fn DofManager::getValue
   @param dof A Dof
   @return Returns the value of the given Dof, if it has been fixed

   This method throws an Exception if the Dof has not been fixed
   **

   @fn  DofManager::toString
   @return Returns the DofManager string
   **

   @fn DofManager::toPre(void)

   @return Same as DofManager::toPre(true)
   **

   @fn DofManager::toPre(bool)
   @param  entityOffset A boolean value
   @return Returns a string corresponding to this DofManager
   as if it was a GetDP .pre file
   @see http://getdp.info/doc/texinfo/getdp.html#File-pre

   If entityOffset is false, the entity numbering is the one of SmallFem (i.e. starts at 0).
   Otherwise, the entity numbering is the one of GetDP (i.e. starts at 1).
   **

   @fn DofManager::writePre
   @param filename A file name

   This method writes the result of this->toPre() into the given file
   **
*/

//////////////////////
// Inline Functions //
//////////////////////

template<typename scalar>
inline bool sf::DofManager<scalar>::SortPredicateFunctor::
operator()(const sf::Dof& a, const sf::Dof& b){
  return dofM.getGlobalId(a) < dofM.getGlobalId(b);
}

template<typename scalar>
inline size_t sf::DofManager<scalar>::isFixedId(void){
  return isFixed;
}

template<typename scalar>
inline size_t sf::DofManager<scalar>::getGlobalId(const sf::Dof& dof) const{
  return globalIdV[dof.getEntity() - first][dof.getType()];
}

#endif
