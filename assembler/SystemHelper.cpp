#include "SystemHelper.h"
#include "SmallFem.h"
#include "System.h"

using namespace std;
using namespace sf;

template<typename scalar>
SystemHelper<scalar>::SystemHelper(void){
}

template<typename scalar>
SystemHelper<scalar>::~SystemHelper(void){
}

template<typename scalar>
void SystemHelper<scalar>::
dirichlet(SystemAbstract<scalar>& sys,
          const FunctionSpace& fs,
          const GroupOfElement& domain,
          scalar (*f)(fullVector<double>& xyz)){

  // Is domain empty ? //
  if(domain.isEmpty())
    return;

  // Scalar Formulation //
  FormulationProjection<scalar> formulation(domain, fs, f);

  // Dirichlet
  dirichlet(sys, fs, domain, formulation);
}

template<typename scalar>
void SystemHelper<scalar>::
dirichlet(SystemAbstract<scalar>& sys,
          const FunctionSpace& fs,
          const GroupOfElement& domain,
          scalar (*f)(const MElement& element,
                      fullVector<double>& xyz)){

  // Is domain empty ? //
  if(domain.isEmpty())
    return;

  // Scalar Formulation //
  FormulationProjection<scalar> formulation(domain, fs, f);

  // Dirichlet
  dirichlet(sys, fs, domain, formulation);
}

template<typename scalar>
void SystemHelper<scalar>::
dirichlet(SystemAbstract<scalar>& sys,
          const FunctionSpace& fs,
          const GroupOfElement& domain,
          fullVector<scalar> (*f)(fullVector<double>& xyz)){

  // Is domain empty ? //
  if(domain.isEmpty())
    return;

  // Vector Formulation //
  FormulationProjection<scalar> formulation(domain, fs, f);

  // Dirichlet
  dirichlet(sys, fs, domain, formulation);
}

template<typename scalar>
void SystemHelper<scalar>::tctGauge(SystemAbstract<scalar>& sys,
                                    const FunctionSpace1Form& fs,
                                    const GroupOfElement& tree){

  // Get low-order Dofs of the tree
  set<Dof> dof;
  fs.getSpecialKeys(tree, Basis::Filter::LowOrder, dof);

  // Set those Dofs to zero
  map<Dof, scalar>   constraint;
  set<Dof>::iterator it  = dof.begin();
  set<Dof>::iterator end = dof.end();

  for(; it != end; it++)
    constraint.insert(pair<Dof, scalar>(*it, 0));

  // Set constraint
  sys.constraint(constraint);
}

template<typename scalar>
void SystemHelper<scalar>::dirichletZero(SystemAbstract<scalar>& sys,
                                         const FunctionSpace& fs,
                                         const GroupOfElement& domain){

  // Map of Dofs //
  set<Dof> dof;

  fs.getKeys(domain, dof);
  set<Dof>::iterator it  = dof.begin();
  set<Dof>::iterator end = dof.end();

  // Homogeneous constraint //
  map<Dof, scalar> constraint;
  for(; it != end; it++)
    constraint.insert(pair<Dof, scalar>(*it, 0));

  // Do it //
  sys.constraint(constraint);
}

template<typename scalar>
void SystemHelper<scalar>::
dirichlet(SystemAbstract<scalar>& sys,
          const FunctionSpace& fs,
          const GroupOfElement& domain,
          FormulationProjection<scalar>& formulation){

  // Solve Projection //
  System<scalar> projection;

  projection.addFormulation(formulation);
  projection.assemble();
  projection.solve();

  // Map of Dofs //
  set<Dof> dof;
  map<Dof, scalar> constraint;

  fs.getKeys(domain, dof);
  set<Dof>::iterator it  = dof.begin();
  set<Dof>::iterator end = dof.end();

  for(; it != end; it++)
    constraint.insert(pair<Dof, scalar>(*it, 0));

  // Get Solution and Dirichlet Constraint //
  projection.getSolution(constraint, 0);
  sys.constraint(constraint);
}

template<typename scalar>
void SystemHelper<scalar>::dofMap(const FunctionSpace& fs,
                                  const GroupOfElement& domain,
                                  map<Dof, scalar>& map,
                                  string filter){
  // Dofs
  set<Dof> dof;
  if(filter.empty())
    fs.getKeys(domain, dof);
  else
    fs.getSpecialKeys(domain, filter, dof);

  // Loop and add to map
  set<Dof>::iterator  it = dof.begin();
  set<Dof>::iterator end = dof.end();
  for(; it != end; it++)
    map.insert(pair<Dof, scalar>(*it, 0.));
}

////////////////////////////
// Explicit instantiation //
////////////////////////////
template class sf::SystemHelper<Complex>;
template class sf::SystemHelper<double>;
