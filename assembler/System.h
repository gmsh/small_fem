#ifndef _SYSTEM_H_
#define _SYSTEM_H_

#include "SystemAbstract.h"
#include "Solver.h"

/**
   @class System
   @brief This class assembles a linear system

   This class assembles a linear system.
 */

namespace sf{
  template<typename scalar> class System: public SystemAbstract<scalar>{
  protected:
    Solver<scalar>*       solver;
    SolverMatrix<scalar>* A;
    SolverVector<scalar>* b;
    fullVector<scalar>*   x;

  public:
    System(void);
    System(std::string solver);
    virtual ~System(void);

    virtual size_t getNComputedSolution(void)                             const;
    virtual void   getSolution(fullVector<scalar>& sol, size_t nSol)      const;
    virtual void   getSolution(std::map<Dof, scalar>& sol, size_t nSol)   const;
    virtual void   getSolution(FEMSolution<scalar>& feSol,
                               const FunctionSpace& fs,
                               const GroupOfElement& domain)              const;
    virtual void   getSolution(FEMSolution<scalar>& feSol,
                               const FunctionSpace& fs,
                               const std::vector<const GroupOfElement*>& domain)
                                                                          const;
    virtual void assemble(void);
    virtual void solve(void);

    void clearRHS(void);
    void addToRHS(const fullVector<scalar>& b);

    void assembleAgainRHS(void);
    void solveAgain(void);

    Solver<scalar>& getSolver(void);

    virtual void writeMatrix(std::string fileName,
                             std::string matrixName) const;

  private:
    void init(std::string solver);
  };
}

/**
   @fn System::System(void)
   Instantiates a new System.

   @note The default solver is "mumps"
   **

   @fn System::System(std::string)
   @param solver Solver name
   Instantiates a new System with the given Solver.

   @note Solvers are "mumps" and "pastix"
   **

   @fn System::~System
   Deletes this System.
   **

   @fn System::clearRHS(void)
   Set this System's RHS to 0.
   **

   @fn System::addToRHS(const fullVector<scalar>& b)
   @param b A vector of the same size as this System's RHS
   Adds the given vector to this System's RHS.
   **

   @fn System::assembleAgainRHS
   This method reassemble the right hand sides of the Formulation%s given by
   System::addFormulation().

   This method works only if the full system was already assembled by
   System::assemble().
   **

   @fn System::solveAgain
   This method solve the new system given by System::assembleAgainRHS.

   This method works only if the full sysem was already solved by
   System::solve().
   **

   @fn System::getSolver
   @return Returns the Solver of this System
*/

#endif
