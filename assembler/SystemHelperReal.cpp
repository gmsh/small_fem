#include "SystemHelper.h"
#include "SmallFem.h"
#include <iomanip>

using namespace std;
using namespace sf;

template<>
void SystemHelper<double>::writeDof(const map<Dof, double>& map,
                                    string filename){
  // Open stream //
  ofstream stream;
  stream.open(filename.c_str());

  // Real and format //
  stream << "real" << endl;
  stream << std::scientific << std::setprecision(16);

  // Write dof map //
  for(std::map<Dof, double>::const_iterator it=map.begin(); it!=map.end();it++)
    stream << it->first.getEntity() << ";"
           << it->first.getType()   << ";"
           << it->second            << endl;

  // Done //
  stream.close();
}

template<>
void SystemHelper<double>::readDof(string filename,
                                   map<Dof, double>& map){
  // Open stream //
  ifstream stream;
  stream.open(filename.c_str());

  // Check if double //
  string scalar;
  stream >> scalar;
  if(scalar.compare("real") != 0)
    throw Exception("SystemHelper<Double>::readDof: wrong data type (%s)",
                    scalar.c_str());

  // Fill dof map //
  string line;
  size_t entity;
  size_t type;
  double val;
  char   semicol;
  while(!stream.eof()){
    stream >> line;

    istringstream iss(line);
    iss >> entity >> semicol
        >> type   >> semicol
        >> val    >> semicol;

    map.insert(pair<Dof, double>(Dof(entity, type), val));
  }

  // Done //
  stream.close();
}
