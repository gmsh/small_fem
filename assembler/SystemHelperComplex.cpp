#include "SystemHelper.h"
#include "SmallFem.h"
#include <iomanip>

using namespace std;
using namespace sf;

template<>
void SystemHelper<Complex>::writeDof(const map<Dof, Complex>& map,
                                    string filename){
  // Open stream //
  ofstream stream;
  stream.open(filename.c_str());

  // Real and format //
  stream << "complex" << endl;
  stream << std::scientific << std::setprecision(16);

  // Write dof map //
  for(std::map<Dof, Complex>::const_iterator it=map.begin(); it!=map.end();it++)
    stream << it->first.getEntity() << ";"
           << it->first.getType()   << ";"
           << it->second.real()     << ";"
           << it->second.imag()     << endl;

  // Done //
  stream.close();
}

template<>
void SystemHelper<Complex>::readDof(string filename,
                                    map<Dof, Complex>& map){
  // Open stream //
  ifstream stream;
  stream.open(filename.c_str());

  // Check if complex //
  string scalar;
  stream >> scalar;
  if(scalar.compare("complex") != 0)
    throw Exception("SystemHelper<Complex>::readDof: wrong data type (%s)",
                    scalar.c_str());

  // Fill dof map //
  string line;
  size_t entity;
  size_t type;
  double real;
  double imag;
  char   semicol;
  while(!stream.eof()){
    stream >> line;

    istringstream iss(line);
    iss >> entity >> semicol
        >> type   >> semicol
        >> real   >> semicol
        >> imag   >> semicol;

    map.insert(pair<Dof, Complex>(Dof(entity, type), Complex(real, imag)));
  }

  // Done //
  stream.close();
}
