#ifndef _SYSTEMHELPER_H_
#define _SYSTEMHELPER_H_

/**
   @class SystemHelper
   @brief A bunch of helping class methods for linear systems

   A bunch of helping class methods for linear systems
*/

#include "FormulationProjection.h"
#include "SystemAbstract.h"
#include "GroupOfElement.h"
#include "FunctionSpace.h"

namespace sf{
  template<typename scalar> class SystemHelper{
  public:
    SystemHelper(void);
    ~SystemHelper(void);

    static void dirichlet(SystemAbstract<scalar>& sys,
                          const FunctionSpace& fs,
                          const GroupOfElement& domain,
                          scalar (*f)(fullVector<double>& xyz));

    static void dirichlet(SystemAbstract<scalar>& sys,
                          const FunctionSpace& fs,
                          const GroupOfElement& domain,
                          scalar (*f)(const MElement& element,
                                      fullVector<double>& xyz));

    static void dirichlet(SystemAbstract<scalar>& sys,
                          const FunctionSpace& fs,
                          const GroupOfElement& domain,
                          fullVector<scalar> (*f)(fullVector<double>& xyz));

    static void dirichletZero(SystemAbstract<scalar>& sys,
                              const FunctionSpace& fs,
                              const GroupOfElement& domain);

    static void tctGauge(SystemAbstract<scalar>& sys,
                         const FunctionSpace1Form& fs,
                         const GroupOfElement& tree);

    static void   dofMap(const FunctionSpace& fs,
                         const GroupOfElement& domain,
                         std::map<Dof, scalar>& map,
                         std::string filter="");
    static void writeDof(const std::map<Dof, scalar>& map,
                         std::string filename);
    static void  readDof(std::string filename,
                         std::map<Dof, scalar>& map);

  private:
    static void dirichlet(SystemAbstract<scalar>& sys,
                          const FunctionSpace& fs,
                          const GroupOfElement& domain,
                          FormulationProjection<scalar>& formulation);
  };
}

/**
   @fn SystemHelper::SystemHelper
   Instantiates a new SystemHelper (unneeded since it has only class methods)
   **

   @fn SystemHelper::~SystemHelper
   Deletes this SystemHelper
   **

   @fn SystemHelper::dirichlet(SystemAbstract<scalar>&, const FunctionSpace&, const GroupOfElement&, scalar (*f)(fullVector<double>& xyz))
   @param sys A SystemAbstract
   @param fs A FunctionSpace
   @param domain A GroupOfElement
   @param f A scalar function

   Imposes on the given SystemAbstract a scalar dirichlet condition with:
   @li The Dof%s of the given FunctionSpace (restricted to the given domain)
   @li The given scalar function
   **

   @fn SystemHelper::dirichlet(SystemAbstract<scalar>&, const FunctionSpace&, const GroupOfElement&, scalar (*f)(const MElement&, fullVector<double>& xyz))
   @param sys A SystemAbstract
   @param fs A FunctionSpace
   @param domain A GroupOfElement
   @param f A scalar function defined element-wise

   Imposes on the given SystemAbstract a scalar dirichlet condition with:
   @li The Dof%s of the given FunctionSpace (restricted to the given domain)
   @li The given scalar function
   **

   @fn SystemHelper::dirichlet(SystemAbstract<scalar>&, const FunctionSpace&, const GroupOfElement&, fullVector<scalar> (*f)(fullVector<double>& xyz))
   @param sys A SystemAbstract
   @param fs A FunctionSpace
   @param domain A GroupOfElement
   @param f A vectorial function

   Imposes on the given SystemAbstract a vectorial dirichlet condition with:
   @li The Dof%s of the given FunctionSpace (restricted to the given domain)
   @li The given vectorial function
   **

   @fn SystemHelper::dirichletZero
   @param sys A SystemAbstract
   @param fs A FunctionSpace
   @param domain A GroupOfElement

   Imposes on the given SystemAbstract an homogeneous dirichlet condition
   on the given FunctionSpace (restricted to the given GrouopOfElement).
   **

   @fn SystemHelper::tctGauge
   @param sys A SystemAbstract
   @param fs A FunctionSpace1Form
   @param tree A GroupOfElement representing a spanning tree

   Imposes a tree/cotree gauge on the given SystemAbstract
   for the field represented by fs and with the given spanning tree
   **

   @fn SystemHelper::dofMap
   @param fs A FunctionSpace
   @param domain A GroupOfElement
   @param map A std::map<Dof, scalar>
   @param filter A string (see Basis::Filter)

   Populates the given map with the Dof%s of the given FunctionSpace
   restricted to the given GroupOfElement.
   The values associated to the Dof%s are set to zero.
   If an optional Basis::Filter is given as filter,
   then only the Dof%s associated to this filter will be inserted into map
   **

   @fn SystemHelper::writeDof
   @param map A map of Dof%s (@see SystemHelper::dofMap)
   @param filename A file name

   Writes the given map in the given file

   @fn SystemHelper::readDof(std::string filename,
   @param filename A file name (@see SystemHelper::writeDof)
   @param map A map of Dof%s (@see SystemHelper::dofMap)

   Reads the given file (obtained via SystemHelper::writeDof)
   and populates the given map accordingly
 */

#endif
