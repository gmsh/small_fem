#include "System.h"

#include "SolverPastix.h"
#include "SolverMUMPS.h"
#include "SolverPETSc.h"

using namespace std;
using namespace sf;

template<typename scalar>
void System<scalar>::init(std::string solver){
  this->A      = NULL;
  this->b      = NULL;
  this->x      = NULL;
  this->solver = NULL;

  if(solver.compare("mumps") == 0)
    this->solver = new SolverMUMPS<scalar>;
  else if(solver.compare("pastix") == 0)
    this->solver = new SolverPastix<scalar>;
  else if(solver.compare("petsc") == 0)
    this->solver = new SolverPETSc<scalar>;
  else
    throw Exception("System::init(): unknown solver %s", solver.c_str());

  // Dof Manager //
  this->dofM = new DofManager<scalar>;

  // Per-thread non-zero term //
  #pragma omp parallel
  {
    #pragma omp master
    this->nNZCount.resize(omp_get_num_threads());

    #pragma omp for
    for(size_t i = 0; i < this->nNZCount.size(); i++)
      this->nNZCount[i] = 0;
  }

  // The system is not assembled and not solved //
  this->assembled = false;
  this->solved    = false;
}

template<typename scalar>
System<scalar>::System(void){
  init("mumps");
}

template<typename scalar>
System<scalar>::System(std::string solver){
  init(solver);
}

template<typename scalar>
System<scalar>::~System(void){
  delete this->dofM;

  if(A)
    delete A;

  if(b)
    delete b;

  if(x)
    delete x;

  if(solver)
    delete solver;
}

template<typename scalar>
void System<scalar>::assemble(void){
  // If already assembled, clear nNNSCount and delete A and b //
  if(this->assembled){
    #pragma omp parallel for
    for(size_t i = 0; i < this->nNZCount.size(); i++)
      this->nNZCount[i] = 0;

    delete A; A = NULL;
    delete b; b = NULL;
  }

  // Generate DofManager global ID space //
  if(!this->assembled)
    this->dofM->generateGlobalIdSpace();

  // Formulations Iterators //
  typename list<FormulationBlock<scalar>*>::iterator it;
  typename list<FormulationBlock<scalar>*>::iterator end;

  // Count FE terms //
  it  = this->formulation.begin();
  end = this->formulation.end();

  for(; it != end; it++){
    // Get All Dofs (Field & Test) per Element
    const vector<vector<Dof> >& dofField =
      (*it)->field().getKeys((*it)->domain());
    const vector<vector<Dof> >& dofTest  =
      (*it)->test().getKeys((*it)->domain());

    // Count
    const size_t E = dofField.size(); // Should be equal to dofTest.size().?.

    #pragma omp parallel for
    for(size_t i = 0; i < E; i++)
      this->nNZCount[omp_get_thread_num()] =
        SystemAbstract<scalar>::countTerms(this->nNZCount[omp_get_thread_num()],
                                           i, dofField[i], dofTest[i], **it);
  }

  // Alloc //
  const size_t size = this->dofM->getLocalSize();

  A = new SolverMatrix<scalar>(size, size, this->nNZCount);
  b = new SolverVector<scalar>(size);

  // Assemble //
  it  = this->formulation.begin();
  end = this->formulation.end();

  for(; it != end; it++){
    // Get All Dofs (Field & Test) per Element
    const vector<vector<Dof> >& dofField =
      (*it)->field().getKeys((*it)->domain());
    const vector<vector<Dof> >& dofTest  =
      (*it)->test().getKeys((*it)->domain());

    // Assemble
    const size_t E = dofField.size(); // Should be equal to dofTest.size().?.

    #pragma omp parallel for
    for(size_t i = 0; i < E; i++)
      SystemAbstract<scalar>::
        assemble(*A, *b, i, dofField[i], dofTest[i], **it);
  }

  // The system is assembled //
  this->assembled = true;
}

template<typename scalar>
void System<scalar>::solve(void){
  // Is the System assembled ? //
  if(!this->assembled)
    assemble();

  // Was the System already solved ? //
  if(this->solved)
    delete x;

  // Use Solver //
  x = new fullVector<scalar>;      // RHS memory
  this->solver->solve(*A, *b, *x); // Solve

  // System solved ! //
  this->solved = true;
}

template<typename scalar>
void System<scalar>::clearRHS(void){
  // Is the System assembled ? //
  if(!this->assembled)
    throw Exception
      ("System::clearRHS() needs a first call to System::assemble()");

  delete this->b;
  this->b = new SolverVector<scalar>(this->dofM->getLocalSize());
}

template<typename scalar>
void System<scalar>::addToRHS(const fullVector<scalar>& b){
  // Is the System assembled ? //
  if(!this->assembled)
    throw Exception
      ("System::addToRHS() needs a first call to System::assemble()");

  // Get Size and compare //
  const int size = this->dofM->getLocalSize();
  if(size != b.size())
    throw Exception
      ("System::setRHS() vector b has the wrong size: "
       "%d given -- %d expected", b.size(), size);

  // Add to b //
  for(int i = 0; i < size; i++)
    this->b->add(i, b(i));
}

template<typename scalar>
void System<scalar>::assembleAgainRHS(void){
  // Is the full system assembled ? //
  if(!this->assembled)
    throw Exception
      ("System::assembleAgainRHS() needs a first call to System::assemble()");

  // Set RHS to zero //
  this->b->reset();

  // Iterate on Formulations //
  typename list<FormulationBlock<scalar>*>::iterator it;
  typename list<FormulationBlock<scalar>*>::iterator end;

  it  = this->formulation.begin();
  end = this->formulation.end();

  for(; it != end; it++){
    // Get All Dofs (Field & Test) per Element
    const vector<vector<Dof> >& dofField =
      (*it)->field().getKeys((*it)->domain());
    const vector<vector<Dof> >& dofTest =
      (*it)->test().getKeys((*it)->domain());

    // Assemble
    const size_t E = dofField.size(); // Should be equal to dofTest.size().?.

    #pragma omp parallel for
    for(size_t i = 0; i < E; i++)
      SystemAbstract<scalar>::
        assembleRHSOnly(*b, i, dofField[i], dofTest[i], **it);
  }
}

template<typename scalar>
void System<scalar>::solveAgain(void){
  // Is the full system solved ? //
  if(!this->solved)
    throw Exception
      ("System::solveAgain() needs a first call to System::solve()");

  this->solver->setRHS(*b);
  this->solver->solve(*x);
}

template<typename scalar>
size_t System<scalar>::getNComputedSolution(void) const{
  return 1;
}

template<typename scalar>
void System<scalar>::getSolution(fullVector<scalar>& sol, size_t nSol) const{
  //sol.setAsProxy(*x, 0, x->size());
  int size = x->size();
  sol.resize(size, true);
  for(int i = 0; i < size; i++)
    sol(i) = (*x)(i);
}

template<typename scalar>
void System<scalar>::getSolution(map<Dof, scalar>& sol, size_t nSol) const{
  // Get All Dofs
  typename map<Dof, scalar>::iterator it  = sol.begin();
  typename map<Dof, scalar>::iterator end = sol.end();

  // Loop on Dofs and set Values
  for(; it != end; it++){
    size_t gId = this->dofM->getGlobalId(it->first);

    if(gId == DofManager<scalar>::isFixedId())
      it->second = this->dofM->getValue(it->first);

    else
      it->second = (*x)(gId);
  }
}

template<typename scalar>
void System<scalar>::getSolution(FEMSolution<scalar>& feSol,
                                 const FunctionSpace& fs,
                                 const GroupOfElement& domain) const{
  // Solved ?
  if(!this->solved)
    throw Exception("System: addSolution -- System not solved");

  // Coefficients //
  // Get Dofs
  set<Dof> dof;
  fs.getKeys(domain, dof);

  // Get Coefficient
  const set<Dof>::iterator  end = dof.end();
  set<Dof>::iterator        it  = dof.begin();
  map<Dof, scalar>          coef;

  for(; it != end; it++)
    coef.insert(pair<Dof, scalar>(*it, 0));

  // Populate Map
  getSolution(coef, 0);

  // FEMSolution //
  feSol.addCoefficients(0, 0, domain, fs, coef);
}

template<typename scalar>
void System<scalar>::
getSolution(FEMSolution<scalar>& feSol,
            const FunctionSpace& fs,
            const vector<const GroupOfElement*>& domain) const{
  // Get size
  const size_t size = domain.size();

  // Get solution for each domain
  for(size_t i = 0; i < size; i++)
    getSolution(feSol, fs, *domain[i]);
}

template<typename scalar>
Solver<scalar>& System<scalar>::getSolver(void){
  return *this->solver;
}

template<typename scalar>
void System<scalar>::writeMatrix(string fileName,
                                 string matrixName) const{
  A->writeToMatlabFile(fileName, matrixName);
}

////////////////////////////
// Explicit instantiation //
////////////////////////////
template class sf::System<Complex>;
template class sf::System<double>;
