#ifndef _BASISTET1FORM_H_
#define _BASISTET1FORM_H_

#include "BasisHierarchical1Form.h"

/**
   @class BasisTet1Form
   @brief A 1-form Basis for Tetrahedra

   This class can instantiate a 1-form Basis
   (high or low order) for Tetrahedra

   It uses
   <a href="http://www.hpfem.jku.at/publications/szthesis.pdf">Zaglmayr's</a>
   Basis for high order Polynomial%s generation.
*/

namespace sf{
  class BasisTet1Form: public BasisHierarchical1Form{
  public:
    //! @param order The order of the Basis
    //! @param noGrad A boolean
    //!
    //! Returns a new 1-form Basis for Tetrahedra of the given order.
    //! If noGrad is false, a full basis is constructed,
    //! otherwise gradient functions are ignored.
    BasisTet1Form(size_t order, bool noGrad = false);

    //! Deletes this Basis
    //!
    virtual ~BasisTet1Form(void);
  };
}

#endif
