#ifndef _BASISTETLAGRANGE_H_
#define _BASISTETLAGRANGE_H_

#include "BasisLagrange.h"

/**
   @class BasisTetLagrange
   @brief Lagrange Basis for Tetrahedra

   This class can instantiate a Lagrange Basis
   for a Tetrahedron and for a given order.

   It uses
   <a href="http://geuz.org/gmsh/">gmsh</a> Basis.
 */

namespace sf{
  class BasisTetLagrange: public BasisLagrange{
  public:
    //! @param order A natural number
    //!
    //! Returns a new BasisTetLagrange of the given order
    BasisTetLagrange(size_t order);

    //! Deletes this Basis
    //!
    virtual ~BasisTetLagrange(void);

  private:
    virtual void reorder(fullMatrix<double>& mat, size_t orientation) const;
  };
}

#endif
