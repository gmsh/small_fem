#ifndef _REFERENCESPACETRI_H_
#define _REFERENCESPACETRI_H_

#include <string>
#include "ReferenceSpace.h"

/**
   @class ReferenceSpaceTri
   @brief ReferenceSpace for a Triangle

   This class implements a ReferenceSpace for a Triangle.
 */

namespace sf{
  class ReferenceSpaceTri: public ReferenceSpace{
  public:
    ReferenceSpaceTri(void);
    virtual ~ReferenceSpaceTri(void);

    virtual std::string toLatex(void) const;
  };
}

/**
   @fn ReferenceSpaceTri::ReferenceSpaceTri
   Instatiate a new ReferenceSpace for a Triangle
   **

   @fn ReferenceSpaceTri::~ReferenceSpaceTri
   Deletes this ReferenceSpaceTri
*/

#endif
