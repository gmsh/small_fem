#ifndef _BASISLINE0FORM_H_
#define _BASISLINE0FORM_H_

#include "BasisHierarchical0Form.h"

/**
   @class BasisLine0Form
   @brief A 0-form Basis for Lines

   This class can instantiate a 0-form Basis (high or low order) for Lines.

   It uses an adaptation of
   <a href="http://www.hpfem.jku.at/publications/szthesis.pdf">Zaglmayr's</a>
   Basis for high order Polynomial%s generation.

   This Basis is a restriction of a Quad Basis to @f$y = 0@f$.

   It also uses the following mapping: @f$x = \frac{u + 1}{2}@f$.
 */

namespace sf{
  class BasisLine0Form: public BasisHierarchical0Form{
  public:
    //! @param order The order of the Basis
    //!
    //! Returns a new 0-form Basis for Lines of the given order
    BasisLine0Form(size_t order);

    //! Deletes this Basis
    //!
    virtual ~BasisLine0Form(void);
  };
}

#endif
