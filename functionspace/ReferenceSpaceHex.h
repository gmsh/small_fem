#ifndef _REFERENCESPACEHEX_H_
#define _REFERENCESPACEHEX_H_

#include <string>
#include "ReferenceSpace.h"

/**
   @class ReferenceSpaceHex
   @brief ReferenceSpace for Hexahedron

   This class implements a ReferenceSpace for a Hexahedron.
 */

namespace sf{
  class ReferenceSpaceHex: public ReferenceSpace{
  public:
    ReferenceSpaceHex(void);
    virtual ~ReferenceSpaceHex(void);

    virtual std::string toLatex(void) const;

  private:
    void initHex(void);
  };
}

/**
   @fn ReferenceSpaceHex::ReferenceSpaceHex
   Instatiate a new ReferenceSpace for a Hexahedron
   **

   @fn ReferenceSpaceHex::~ReferenceSpaceHex
   Deletes this ReferenceSpaceHex
*/

#endif
