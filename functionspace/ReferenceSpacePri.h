#ifndef _REFERENCESPACEPRI_H_
#define _REFERENCESPACEPRI_H_

#include <string>
#include "ReferenceSpace.h"

/**
   @class ReferenceSpacePri
   @brief ReferenceSpace for Prisms

   This class implements a ReferenceSpace for a Prism.
 */

namespace sf{
  class ReferenceSpacePri: public ReferenceSpace{
  public:
    ReferenceSpacePri(void);
    virtual ~ReferenceSpacePri(void);

    virtual std::string toLatex(void) const;
  };
}

/**
   @fn ReferenceSpacePri::ReferenceSpacePri
   Instatiate a new ReferenceSpace for a Prism
   **

   @fn ReferenceSpacePri::~ReferenceSpacePri
   Deletes this ReferenceSpacePri
*/

#endif
