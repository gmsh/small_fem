#include "gmsh/ElementType.h"
#include "gmsh/GmshDefines.h"
#include "gmsh/pointsGenerators.h"
#include "BasisHexLagrange.h"

using namespace sf;

BasisHexLagrange::BasisHexLagrange(size_t order){
  // If order 0 (Nedelec): use order 1
  if(order == 0)
    order = 1;

  // Set Basis Type //
  this->order = order;

  type = TYPE_HEX;
  dim  = 3;

  nVertex   =  8;
  nEdge     = 12 * (order - 1);
  nFace     =  6 * (order - 1) * (order - 1);
  nCell     =      (order - 1) * (order - 1) * (order - 1);
  nFunction = nVertex + nEdge + nFace + nCell;

  // Init polynomialBasis //
  lBasis = new polynomialBasis(ElementType::getType(TYPE_HEX, order, false));

  // Init Lagrange Point //
  lPoint = new fullMatrix<double>(gmshGeneratePointsHexahedron(order, false));
}

BasisHexLagrange::~BasisHexLagrange(void){
  delete lBasis;
  delete lPoint;
}

void BasisHexLagrange::
reorder(fullMatrix<double>& mat, size_t orientation) const{
  throw Exception("TODO");
}
