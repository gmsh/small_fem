#ifndef _BASISQUADNEDELEC_H_
#define _BASISQUADNEDELEC_H_

#include "BasisHierarchical1Form.h"

/**
   @class BasisQuadNedelec
   @brief Nedelec Basis for Quads

   This class can instantiate a Nedelec Basis for Quadrangles.
*/

namespace sf{
  class BasisQuadNedelec: public BasisHierarchical1Form{
  public:
    //! Returns a new Nedelec Basis for Quadrangles
    //!
    BasisQuadNedelec(void);

    //! Deletes this Basis
    //!
    virtual ~BasisQuadNedelec(void);
  };
}

#endif
