#ifndef _BASISQUADLAGRANGE_H_
#define _BASISQUADLAGRANGE_H_

#include "BasisLagrange.h"

/**
   @class BasisQuadLagrange
   @brief Lagrange Basis for Quadrangles

   This class can instantiate a Lagrange Basis
   for a Quadrangle and for a given order.

   It uses
   <a href="http://geuz.org/gmsh/">gmsh</a> Basis.
 */

namespace sf{
  class BasisQuadLagrange: public BasisLagrange{
  public:
    //! @param order A natural number
    //!
    //! Returns a new BasisQuadLagrange of the given order
    BasisQuadLagrange(size_t order);

    //! Deletes this Basis
    //!
    virtual ~BasisQuadLagrange(void);

  private:
    virtual void reorder(fullMatrix<double>& mat, size_t orientation) const;
  };
}

#endif
