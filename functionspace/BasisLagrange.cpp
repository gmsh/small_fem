#include "ReferenceSpaceManager.h"
#include "BasisLagrange.h"
#include "Exception.h"

using namespace std;
using namespace sf;

BasisLagrange::BasisLagrange(void){
  family = Basis::Family::Lagrange;
  option = std::string(""); // Non a priori
  scalar = true;
  form   = 0;

  preEvaluated     = false;
  preEvaluatedGrad = false;

  preEvaluatedFunction     = NULL;
  preEvaluatedGradFunction = NULL;
}

BasisLagrange::~BasisLagrange(void){
  // Define Orientation //
  const size_t nOrientation = ReferenceSpaceManager::getNOrientation(getType());

  // Functions //
  if(preEvaluated){
    for(size_t i = 0; i < nOrientation; i++)
      delete preEvaluatedFunction[i];

    delete[] preEvaluatedFunction;
  }

  // Gradients //
  if(preEvaluatedGrad){
    for(size_t i = 0; i < nOrientation; i++)
      delete preEvaluatedGradFunction[i];

    delete[] preEvaluatedGradFunction;
  }
}

void BasisLagrange::
getFunctions(fullMatrix<double>& retValues,
             const MElement& element,
             double u, double v, double w) const{
  // Define Orientation //
  const size_t orientation = ReferenceSpaceManager::getOrientation(element);

  // Compute
  getFunctions(retValues, orientation, u, v, w);
}

void BasisLagrange::
getFunctions(fullMatrix<double>& retValues,
             size_t orientation,
             double u, double v, double w) const{

  // Get Function in Gmsh's reference space //
  fullMatrix<double> tmp;
  fullMatrix<double> point(1, 3);
  point(0, 0) = u;
  point(0, 1) = v;
  point(0, 2) = w;

  lBasis->f(point, tmp);

  // Coherence with hierarchical bases //
  retValues = tmp.transpose();

  // Orient with respect to orientation //
  reorder(retValues, orientation);
}

void BasisLagrange::getDerivative(fullMatrix<double>& retValues,
                                  const MElement& element,
                                  double u, double v, double w) const{
  // Define Orientation //
  const size_t orientation = ReferenceSpaceManager::getOrientation(element);

  // Get Function in Gmsh's reference space //
  fullMatrix<double> tmp;
  fullMatrix<double> point(1, 3);
  point(0, 0) = u;
  point(0, 1) = v;
  point(0, 2) = w;

  lBasis->df(point, tmp);

  // Coherence with hierarchical bases //
  retValues = tmp.transpose();

  // Orient with respect to orientation //
  reorder(retValues, orientation);
}

void BasisLagrange::preEvaluateFunctions(const fullMatrix<double>& point) const{
  // Define Orientation //
  const size_t nOrientation = ReferenceSpaceManager::getNOrientation(getType());

  // Delete if older //
  if(preEvaluated){
    for(size_t i = 0; i < nOrientation; i++)
      delete preEvaluatedFunction[i];

    delete[] preEvaluatedFunction;
  }

  // Pre Evaluate in Gmsh's unique reference space //
  fullMatrix<double> tmpp;
  lBasis->f(point, tmpp);

  // Coherence with hierarchical bases //
  fullMatrix<double> tmp;
  tmp = tmpp.transpose();

  // Alloc and populate for each ReferenceSpace's orientations //
  preEvaluatedFunction = new fullMatrix<double>*[nOrientation];
  for(size_t i = 0; i < nOrientation; i++){
    preEvaluatedFunction[i] = new fullMatrix<double>(tmp); // Copy
    reorder(*preEvaluatedFunction[i], i);                   // Orient
  }

  // PreEvaluated //
  preEvaluated = true;
}

void BasisLagrange::
preEvaluateDerivatives(const fullMatrix<double>& point) const{
  // Define Orientation //
  const size_t nOrientation = ReferenceSpaceManager::getNOrientation(getType());

  // Delete if older //
  if(preEvaluatedGrad){
    for(size_t i = 0; i < nOrientation; i++)
      delete preEvaluatedGradFunction[i];

    delete[] preEvaluatedGradFunction;
  }

  // Pre Evaluate in Gmsh's unique reference space //
  fullMatrix<double> tmpp;
  lBasis->df(point, tmpp);

  // Coherence with hierarchical bases //
  fullMatrix<double> tmp;
  tmp = tmpp.transpose();

  // Alloc and populate for each ReferenceSpace's orientations //
  preEvaluatedGradFunction = new fullMatrix<double>*[nOrientation];
  for(size_t i = 0; i < nOrientation; i++){
    preEvaluatedGradFunction[i] = new fullMatrix<double>(tmp); // Copy
    reorder(*preEvaluatedGradFunction[i], i);
  }

  // PreEvaluated //
  preEvaluatedGrad = true;
}

const fullMatrix<double>&
BasisLagrange::getPreEvaluatedFunctions(const MElement& element) const{
  return
    getPreEvaluatedFunctions(ReferenceSpaceManager::getOrientation(element));
}

const fullMatrix<double>&
BasisLagrange::getPreEvaluatedDerivatives(const MElement& element) const{
  return
    getPreEvaluatedDerivatives(ReferenceSpaceManager::getOrientation(element));
}

const fullMatrix<double>&
BasisLagrange::getPreEvaluatedFunctions(size_t orientation) const{
  if(!preEvaluated)
    throw Exception
      ("getPreEvaluatedFunction: function has not been preEvaluated");

  return *preEvaluatedFunction[orientation];
}

const fullMatrix<double>&
BasisLagrange::getPreEvaluatedDerivatives(size_t orientation) const{
  if(!preEvaluatedGrad)
    throw Exception
      ("getPreEvaluatedDerivative: gradient has not been preEvaluated");

  return *preEvaluatedGradFunction[orientation];
}

template<typename scalarT>
vector<scalarT> BasisLagrange::project(const MElement& element,
                                       const vector<scalarT>& coef,
                                       const FunctionSpace& fSpace,
                                       bool derivative){
  // Is the projection 3D or 1D? //
  int form  = fSpace.getForm();
  bool is3D =
    ((form == 0) &&  derivative) ||
    ((form == 1)               ) ||
    ((form == 2) && !derivative);

  // Init New Coefs //
  const size_t size = lPoint->size1();
  const size_t dim  = lPoint->size2();

  size_t   sizeCoef;
  if(is3D) sizeCoef = size * 3;
  else     sizeCoef = size;
  vector<scalarT> newCoef(sizeCoef);

  // Temp data //
  fullVector<scalarT> tmp(3);
  fullVector<double>  uvw(3);
  fullVector<double>  abc(3);
  double           tmpABC[3];

  // Interpolation at Lagrange Points //
  for(size_t i = 0; i < size; i++){
    // Lagrange point in UVW plane
    uvw.setAll(0);

    if(dim > 0)
      uvw(0) = (*lPoint)(i, 0);
    else
      uvw(0) = 0;

    if(dim > 1)
      uvw(1) = (*lPoint)(i, 1);
    else
      uvw(1) = 0;

    if(dim > 2)
      uvw(2) = (*lPoint)(i, 2);
    else
      uvw(2) = 0;

    // Lagrange point in ABC plane
    abc.setAll(0);
    ReferenceSpaceManager::mapFromUVWtoABC(element,
                                           uvw(0),uvw(1),uvw(2), tmpABC);
    abc(0) = tmpABC[0];
    abc(1) = tmpABC[1];
    abc(2) = tmpABC[2];

    // Interpolate
    if(derivative)
      fSpace.interpolateDerivativeInRefSpace(element, coef, abc, tmp);
    else
      fSpace.interpolateInRefSpace(element, coef, abc, tmp);

    // Populate newCoef
    if(is3D){
      newCoef[i * 3 + 0] = tmp(0);
      newCoef[i * 3 + 1] = tmp(1);
      newCoef[i * 3 + 2] = tmp(2);
    }
    else{
      newCoef[i] = tmp(0);
    }
  }

  // Return
  return newCoef;
}

string BasisLagrange::toString(void) const{
  return string("BasisLagrange::toString() not Implemented");
}

void BasisLagrange::getReordering(size_t nVertexPerElm, size_t nEdgePerElm,
                                  size_t nFacePerElm,   size_t preFlipEdge){
  // Reference Space //
  const size_t nOrientation = ReferenceSpaceManager::getNOrientation(type);

  const vector<vector<vector<size_t> > >&
    edgeIdx = ReferenceSpaceManager::getEdgeNodeIndex(type);

//  const vector<vector<vector<size_t> > >&
//    faceIdx = ReferenceSpaceManager::getFaceNodeIndex(type);

  // Functions per entity //
  size_t nFunPerVertex = 0;
  size_t nFunPerEdge   = 0;
  size_t nFunPerFace   = 0;

  if(nVertex) nFunPerVertex = nVertex / nVertexPerElm;
  if(nEdge)   nFunPerEdge   = nEdge   / nEdgePerElm;
  if(nFace)   nFunPerFace   = nFace   / nFacePerElm;

  // Entity indexing //
  vector<vector<vector<size_t> > > v(nOrientation);
  vector<vector<vector<size_t> > > e(nOrientation);
  vector<vector<vector<size_t> > > f(nOrientation);

  // Alloc and start with Gmsh's Indexing //
  // Vertices
  for(size_t i = 0; i < nOrientation; i++)
    v[i].resize(nVertexPerElm);

  for(size_t i = 0; i < nOrientation; i++)
    for(size_t j = 0; j < nVertexPerElm; j++)
      v[i][j].resize(nFunPerVertex);

  for(size_t i = 0; i < nOrientation; i++)
    for(size_t j = 0; j < nVertexPerElm; j++)
      for(size_t k = 0; k < nFunPerVertex; k++)
        v[i][j][k] = k + j*nFunPerVertex;

  // Edges
  for(size_t i = 0; i < nOrientation; i++)
    e[i].resize(nEdgePerElm);

  for(size_t i = 0; i < nOrientation; i++)
    for(size_t j = 0; j < nEdgePerElm; j++)
      e[i][j].resize(nFunPerEdge);

  for(size_t i = 0; i < nOrientation; i++)
    for(size_t j = 0; j < nEdgePerElm; j++)
      for(size_t k = 0; k < nFunPerEdge; k++)
        e[i][j][k] = nVertex + k + j*nFunPerEdge;

  // Faces
  for(size_t i = 0; i < nOrientation; i++)
    f[i].resize(nFacePerElm);

  for(size_t i = 0; i < nOrientation; i++)
    for(size_t j = 0; j < nFacePerElm; j++)
      f[i][j].resize(nFunPerFace);

  for(size_t i = 0; i < nOrientation; i++)
    for(size_t j = 0; j < nFacePerElm; j++)
      for(size_t k = 0; k < nFunPerFace; k++)
        f[i][j][k] = nVertex + nEdge + k + j*nFunPerFace;

  // Pre-Flip to match ReferenceSpace convention "small to big" //
  // Edges
  if(preFlipEdge != (size_t)(-1))
    for(size_t i = 0; i < nOrientation; i++)
      flip(e[i][preFlipEdge]);

  // Faces: TODO

  // Indexing following ReferenceSpace order //
  // Edges
  for(size_t i = 0; i < nOrientation; i++)
    for(size_t j = 0; j < nEdgePerElm; j++)
      if(edgeIdx[i][j][0] > edgeIdx[i][j][1]) // If inverted edge: flip
        flip(e[i][j]);

  // Faces: TODO

  // New indices vector //
  idx.resize(nOrientation);
  for(size_t i = 0; i < nOrientation; i++)
    idx[i].resize(nFunction);

  for(size_t i = 0; i < nOrientation; i++){
    size_t count = 0;

    // Vertices
    for(size_t j = 0; j < nVertexPerElm; j++)
      for(size_t k = 0; k < nFunPerVertex; k++, count++)
        idx[i][count] = v[i][j][k];

    // Edges
    for(size_t j = 0; j < nEdgePerElm; j++)
      for(size_t k = 0; k < nFunPerEdge; k++, count++)
        idx[i][count] = e[i][j][k];

    // Faces
    for(size_t j = 0; j < nFacePerElm; j++)
      for(size_t k = 0; k < nFunPerFace; k++, count++)
        idx[i][count] = f[i][j][k];

    // Cell
    for(; count < nFunction; count++)
      idx[i][count] = count;
  }

//  // Disp
//  static bool once = true;
//  if(once){
//    once = false;
//
//    for(size_t i = 0; i < nOrientation; i++){
//      cout << "T R I" << endl;
//      cout << "Orientation: " << i << endl
//           << "###########  " << endl;
//
//      cout << " Vertex" << endl
//           << " ------" << endl;
//      for(size_t j = 0; j < nVertexPerElm; j++)
//        for(size_t k = 0; k < nFunPerVertex; k++)
//          cout << "  * " << j << ": " << v[i][j][k] << endl;
//
//      cout << " Edges" << endl
//           << " -----" << endl;
//      for(size_t j = 0; j < nEdgePerElm; j++)
//        for(size_t k = 0; k < nFunPerEdge; k++)
//          cout << "  * " << j << ": " << e[i][j][k] << endl;
//
//      cout << " Faces" << endl
//           << " -----" << endl;
//      for(size_t j = 0; j < nFacePerElm; j++)
//        for(size_t k = 0; k < nFunPerFace; k++)
//          cout << "  * " << j << ": " << f[i][j][k] << endl;
//
//      cout << " Orient" << endl
//           << " ------" << endl;
//      for(size_t j = 0; j < nFunction; j++)
//        cout << "  * " << idx[i][j] << endl;
//
//      cout << " EdgeIdx" << endl
//           << " -------" << endl;
//      for(size_t j = 0; j < nEdgePerElm; j++){
//        cout << "  * " << j << ": [";
//        for(size_t l = 0; l < edgeIdx[i][j].size(); l++)
//          cout << edgeIdx[i][j][l] << ", ";
//
//        cout << "]" << endl;
//      }
//
//      cout << " FaceIdx" << endl
//           << " -------" << endl;
//      for(size_t j = 0; j < nFacePerElm; j++){
//        cout << "  * " << i << ": [";
//        for(size_t l = 0; l < faceIdx[i][j].size(); l++)
//          cout << faceIdx[i][j][l] << ", ";
//
//        cout << "]" << endl;
//      }
//    }
//  }
}

void BasisLagrange::reorderCommon(fullMatrix<double>& mat,
                                  size_t orientation) const{
  // Copy Mat //
  fullMatrix<double> copy(mat);

  // Size //
  size_t N = mat.size1(); if(N != nFunction) throw Exception("Wrong state oO!");
  size_t M = mat.size2();

  // Repopulate mat wiht new indices //
  for(size_t i = 0; i < N; i++)
    for(size_t j = 0; j < M; j++)
      mat(i, j) = copy(idx[orientation][i], j);
}

void BasisLagrange::flip(vector<size_t>& a){
  // Size //
  size_t      N = a.size();
  size_t      H = N/2 + N%2; // Upper half (integer division)

  // Flip //
  size_t tmp;
  for(size_t i = 0; i < H; i++){
    tmp      = a[i];
    a[i]     = a[N-1-i];
    a[N-1-i] = tmp;
  }
}

////////////////////////////
// Explicit instantiation //
////////////////////////////
template
vector<double>  BasisLagrange::project<double>(const MElement& element,
                                               const vector<double>& coef,
                                               const FunctionSpace& fSpace,
                                               bool derivative);
template
vector<Complex> BasisLagrange::project<Complex>(const MElement& element,
                                                const vector<Complex>& coef,
                                                const FunctionSpace& fSpace,
                                                bool derivative);
