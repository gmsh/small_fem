#ifndef _REFERENCESPACEPYR_H_
#define _REFERENCESPACEPYR_H_

#include <string>
#include "ReferenceSpace.h"

/**
   @class ReferenceSpacePyr
   @brief ReferenceSpace for Pyramids

   This class implements a ReferenceSpace for a Pyramid.
 */

namespace sf{
  class ReferenceSpacePyr: public ReferenceSpace{
  public:
    ReferenceSpacePyr(void);
    virtual ~ReferenceSpacePyr(void);

    virtual std::string toLatex(void) const;
  };
}

/**
   @fn ReferenceSpacePyr::ReferenceSpacePyr
   Instatiate a new ReferenceSpace for a Pyramid
   **

   @fn ReferenceSpacePyr::~ReferenceSpacePyr
   Deletes this ReferenceSpacePyr
*/

#endif
