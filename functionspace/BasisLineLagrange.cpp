#include "gmsh/ElementType.h"
#include "gmsh/GmshDefines.h"
#include "gmsh/pointsGenerators.h"
#include "BasisLineLagrange.h"

using namespace sf;

BasisLineLagrange::BasisLineLagrange(size_t order){
  // If order 0 (Nedelec): use order 1
  if(order == 0)
    order = 1;

  // Set Basis Type //
  this->order = order;

  type = TYPE_LIN;
  dim  = 1;

  nVertex   = 2;
  nEdge     = (order - 1);
  nFace     = 0;
  nCell     = 0;
  nFunction = nVertex + nEdge + nFace + nCell;

  // Init polynomialBasis //
  lBasis = new polynomialBasis(ElementType::getType(TYPE_LIN, order, false));

  // Init Lagrange Point //
  lPoint = new fullMatrix<double>(gmshGeneratePointsLine(order));

  // Compute orientation reordering //
  size_t nVertexPerElm = 2;
  size_t nEdgePerElm   = 1;
  size_t nFacePerElm   = 0;
  size_t preFlipEdge   = -1;
  getReordering(nVertexPerElm, nEdgePerElm, nFacePerElm, preFlipEdge);
}

BasisLineLagrange::~BasisLineLagrange(void){
  delete lBasis;
  delete lPoint;
}

void BasisLineLagrange::
reorder(fullMatrix<double>& mat, size_t orientation) const{
  reorderCommon(mat, orientation);
}
