#include "gmsh/GmshDefines.h"

#include "Legendre.h"
#include "ReferenceSpaceManager.h"
#include "BasisPoint0Form.h"

using namespace std;
using namespace sf;

BasisPoint0Form::BasisPoint0Form(void){
  // Set Basis Type //
  this->order = 1;

  type = TYPE_PNT;
  dim  = 0;

  nVertex   = 1;
  nEdge     = 0;
  nFace     = 0;
  nCell     = 0;
  nFunction = nVertex + nEdge + nFace + nCell;

  // Reference Space //
  const size_t nOrientation = ReferenceSpaceManager::getNOrientation(type);

  // Lagrange 'Polynomial' //
  const Polynomial lagrange[1] = { Polynomial(1, 0, 0, 0) };

  // Basis //
  basis = new Polynomial**[nOrientation];

  for(size_t s = 0; s < nOrientation; s++)
    basis[s] = new Polynomial*[nFunction];

  // Vertex Based //
  for(size_t s = 0; s < nOrientation; s++)
    basis[s][0] = new Polynomial(lagrange[0]);
}

BasisPoint0Form::~BasisPoint0Form(void){
  const size_t nOrientation = ReferenceSpaceManager::getNOrientation(type);

  // Basis //
  for(size_t i = 0; i < nOrientation; i++){
    for(size_t j = 0; j < nFunction; j++)
      delete basis[i][j];

    delete[] basis[i];
  }

  delete[] basis;
}
