#ifndef _BASISGENERATOR_H_
#define _BASISGENERATOR_H_

#include <string>
#include "BasisLocal.h"

/**
    @class BasisGenerator
    @brief A bunch of class method to generate a Local Basis

    A BasisGenerator is a bunch of class methods to generate a local Basis.
*/

namespace sf{
  class BasisGenerator{
  public:
    BasisGenerator(void);
   ~BasisGenerator(void);

    static BasisLocal* generate(size_t elementType,
                                size_t form,
                                size_t order,
                                std::string family =Basis::Family::Hierarchical,
                                std::string option ="");

    static BasisLocal* pntHierarchicalGen(size_t form,size_t order,bool noGrad);
    static BasisLocal* linHierarchicalGen(size_t form,size_t order,bool noGrad);
    static BasisLocal* triHierarchicalGen(size_t form,size_t order,bool noGrad);
    static BasisLocal* quaHierarchicalGen(size_t form,size_t order,bool noGrad);
    static BasisLocal* tetHierarchicalGen(size_t form,size_t order,bool noGrad);
    static BasisLocal* hexHierarchicalGen(size_t form,size_t order,bool noGrad);

  private:
    static BasisLocal* generateHierarchical(size_t elementType,
                                            size_t form,
                                            size_t order,
                                            std::string option);

    static BasisLocal* generateLagrange(size_t elementType,
                                        size_t form,
                                        size_t order);
  };
}

/**
   @fn BasisGenerator::BasisGenerator
   Instantiates a new BasisGenerator

   This class got only class methods, so it is not required to instanciate it.
   **

   @fn BasisGenerator::~BasisGenerator
   Deletes this BasisGenerator
   **

   @fn BasisGenerator::generate(size_t, size_t, size_t, std::string, std::string)
   @param elementType The element type of the requested Basis
   @param form The Basis form
   @param order The order or the requested Basis
   @param family A string
   @param option A string

   This method will instanciate the requested Basis of the requested family.
   Additional options can be used when required.

   @return Returns a pointer to a newly instantiated Basis

   Element types are:
   @li @c TYPE_LIN for Lines
   @li @c TYPE_TRI for Triangles
   @li @c TYPE_QUA for Quadrangles
   @li @c TYPE_TET for Tetrahedrons
   @li @c TYPE_HEX for Hexahedrons

   Basis forms are:
   @li 0 for 0-Form
   @li 1 for 1-Form
   @li 2 for 2-Form
   @li 3 for 3-Form

   @see Basis for available families and options
   **

   @fn BasisGenerator::linHierarchicalGen
   @param form The Basis form
   @param order The order or the requested Basis

   This method will instanciate the requested Basis with a Line support

   @return Returns a pointer to a newly instantiated Basis

   Basis forms are:
   @li 0 for 0-Form
   @li 1 for 1-Form
   @li 2 for 2-Form
   @li 3 for 3-Form

   The Basis family will be hierarchical
   **

   @fn BasisGenerator::triHierarchicalGen
   @param form The Basis form
   @param order The order or the requested Basis

   Same as BasisGenerator::linHierarchicalGen() but for Triangles
   **

   @fn BasisGenerator::quaHierarchicalGen
   @param form The Basis form
   @param order The order or the requested Basis

   Same as BasisGenerator::linHierarchicalGen() but for Quadrangles
   **

   @fn BasisGenerator::tetHierarchicalGen
   @param form The Basis form
   @param order The order or the requested Basis

   Same as BasisGenerator::linHierarchicalGen() but for Tetrahedra
   **

   @fn BasisGenerator::hexHierarchicalGen
   @param form The Basis form
   @param order The order or the requested Basis

   Same as BasisGenerator::linHierarchicalGen() but for Hexahedra
 */

#endif
