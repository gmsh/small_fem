#ifndef _BASISPOINT0FORM_H_
#define _BASISPOINT0FORM_H_

#include "BasisHierarchical0Form.h"

/**
   @class BasisPoint0Form
   @brief A 0-form Basis for Points

   This class can instantiate a 0-form Basis for Points.
 */

namespace sf{
  class BasisPoint0Form: public BasisHierarchical0Form{
  public:
    //! Returns a new 0-form Basis for Points
    BasisPoint0Form(void);

    //! Deletes this Basis
    //!
    virtual ~BasisPoint0Form(void);
  };
}

#endif
