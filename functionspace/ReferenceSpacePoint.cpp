#include <sstream>
#include "ReferenceSpacePoint.h"

using namespace std;
using namespace sf;

ReferenceSpacePoint::ReferenceSpacePoint(void){
  // Vertex Definition //
  nVertex = 1;

  // Edge Definition //
  refEdgeNodeIdx.clear(); // No Edge in Point

  // Face Definition //
  refFaceNodeIdx.clear(); // No Face in Point

  init();
}

ReferenceSpacePoint::~ReferenceSpacePoint(void){
}

string ReferenceSpacePoint::toLatex(void) const{
  const size_t nRefSpace = refSpaceNodeId.size();
  stringstream stream;

  stream << "\\documentclass{article}" << endl << endl

         << "\\usepackage{longtable}"  << endl
         << "\\usepackage{tikz}"       << endl
         << "\\usetikzlibrary{arrows}" << endl << endl

         << "\\begin{document}"                                   << endl
         << "\\tikzstyle{vertex} = [circle, fill = black!25]"     << endl
         << "\\tikzstyle{point}   = [draw, thick, black, -latex']" << endl
         << endl

         << "\\begin{longtable}{c}" << endl << endl;

  for(size_t s = 0; s < nRefSpace; s++){
    stream << "\\begin{tikzpicture}" << endl

           << "\\node[vertex] (n0) at(0, 0) {$" << refSpaceNodeId[s][0] << "$};"
           << endl

           << "\\end{tikzpicture} \\\\ \\\\" << endl << endl;
  }

  stream << "\\end{longtable}" << endl
         << "\\end{document}"  << endl;

  return stream.str();
}
