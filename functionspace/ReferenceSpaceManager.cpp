#include "Exception.h"
#include "ReferenceSpaceManager.h"

using namespace std;
using namespace sf;

const size_t ReferenceSpaceManager::nSpace = 9;
vector<ReferenceSpace*> ReferenceSpaceManager::refSpace(nSpace, NULL);
const Transformation* ReferenceSpaceManager::transformation = NULL;

ReferenceSpaceManager::ReferenceSpaceManager(void){
}

ReferenceSpaceManager::~ReferenceSpaceManager(void){
}

void ReferenceSpaceManager::init(int elementType){
  // Warning: no elementType == 0
  //  --> refSpace[0] will be alwys null --> not realy important

  // Init ReferenceSpace
  switch(elementType){
  case 1: refSpace[elementType] = new ReferenceSpacePoint; break;
  case 2: refSpace[elementType] = new ReferenceSpaceLine;  break;
  case 3: refSpace[elementType] = new ReferenceSpaceTri;   break;
  case 4: refSpace[elementType] = new ReferenceSpaceQuad;  break;
  case 5: refSpace[elementType] = new ReferenceSpaceTet;   break;
  case 6: refSpace[elementType] = new ReferenceSpacePyr;   break;
  case 7: refSpace[elementType] = new ReferenceSpacePri;   break;
  case 8: refSpace[elementType] = new ReferenceSpaceHex;   break;

  case 9:  throw Exception("ReferenceSpace for POLYG not implemented");
  case 10: throw Exception("ReferenceSpace for POLYH not implemented");
  case 11: throw Exception("ReferenceSpace for XFEM not implemented");
  }

  // Set transformation if any
  if(transformation)
    setTransformation(*transformation);
}

void ReferenceSpaceManager::clear(void){
  for(size_t i = 0; i < nSpace; i++)
    if(refSpace[i])
      delete refSpace[i];

  for(size_t i = 0; i < nSpace; i++)
    refSpace[i] = NULL;
}

void
ReferenceSpaceManager::setTransformation(const Transformation& transformation){
  // Save transformation
  ReferenceSpaceManager::transformation = &transformation;

  // Apply transformation
  for(size_t i = 0; i < nSpace; i++)
    if(refSpace[i])
      refSpace[i]->setTransformation(transformation);
}
