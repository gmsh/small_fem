#ifndef _BASISTETNEDELEC_H_
#define _BASISTETNEDELEC_H_

#include "BasisHierarchical1Form.h"

/**
   @class BasisTetNedelec
   @brief A Nedelec Basis for Tetrahedra

   This class can instantiate a Nedelec Basis for Tetrahedra.
*/

namespace sf{
  class BasisTetNedelec: public BasisHierarchical1Form{
  public:
    //! Returns a new Nedelec Basis for Tetrahedra
    //!
    BasisTetNedelec(void);

    //! Deletes this Basis
    //!
    virtual ~BasisTetNedelec(void);
  };
}

#endif
