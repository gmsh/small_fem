#ifndef _REFERENCESPACEQUAD_H_
#define _REFERENCESPACEQUAD_H_

#include <string>
#include "ReferenceSpace.h"

/**
   @class ReferenceSpaceQuad
   @brief ReferenceSpace for a Quadrangle

   This class implements a ReferenceSpace for a Quadrangle.
 */

namespace sf{
  class ReferenceSpaceQuad: public ReferenceSpace{
  public:
    ReferenceSpaceQuad(void);
    virtual ~ReferenceSpaceQuad(void);

    virtual std::string toLatex(void) const;
  };
}

/**
   @fn ReferenceSpaceQuad::ReferenceSpaceQuad
   Instatiate a new ReferenceSpace for a Quadrangle
   **

   @fn ReferenceSpaceQuad::~ReferenceSpaceQuad
   Deletes this ReferenceSpaceQuad
*/

#endif
