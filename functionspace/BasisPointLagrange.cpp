#include "gmsh/GmshDefines.h"
#include "gmsh/ElementType.h"
#include "gmsh/pointsGenerators.h"
#include "BasisPointLagrange.h"

using namespace sf;

BasisPointLagrange::BasisPointLagrange(void){
  // Set Basis Type //
  this->order = 1;

  type = TYPE_PNT;
  dim  = 0;

  nVertex   = 1;
  nEdge     = 0;
  nFace     = 0;
  nCell     = 0;
  nFunction = nVertex + nEdge + nFace + nCell;

  // Init polynomialBasis //
  lBasis = new polynomialBasis(ElementType::getType(TYPE_LIN, order, false));

  // Init Lagrange Point //
  lPoint = new fullMatrix<double>(1, 1);
  (*lPoint)(0, 0) = 0;

  std::cout << "Warning: check value of point with Lagrange" << std::endl;
}

BasisPointLagrange::~BasisPointLagrange(void){
  delete lBasis;
  delete lPoint;
}

void BasisPointLagrange::
reorder(fullMatrix<double>& mat, size_t orientation) const{
}
