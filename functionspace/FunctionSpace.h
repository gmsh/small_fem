#ifndef _FUNCTIONSPACE_H_
#define _FUNCTIONSPACE_H_

#include <map>
#include <vector>
#include "gmsh/MElement.h"

#include "Dof.h"
#include "Mesh.h"
#include "Basis.h"
#include "Exception.h"
#include "GroupOfElement.h"
#include "SmallFem.h"

/**
    @interface FunctionSpace
    @brief Common Interface of all Function Spaces

    This is the common interface of all Function Spaces.

    A FunctionSpace is defined on a support,
    which is a collection of MElement%s (GroupOfElement).

    Those MElement%s must belong to the same Mesh.

    A FunctionSpace is also responsible for the generation of all
    the Dof%s related to its geometrical Support.
    Furthermore, a FunctionSpace (or its derivative) can be interpolated.
*/

namespace sf{
  class Mesh;
  class GroupOfElement;

  class FunctionSpace{
  private:
    // Number of possible geomtrical topologies & Dof Type offset //
    static const size_t nGeoType;
    static       size_t nxtOffset;

  private:
    // Offset //
    size_t offset;

    // Mesh //
    const Mesh* mesh;

    // Basis //
    std::vector<const Basis*> basis;

    std::vector<size_t> fPerVertex;
    std::vector<size_t> fPerEdge;
    std::vector<size_t> fPerFace;
    std::vector<size_t> fPerCell;

    // Rejected Dofs //
    std::set<Dof> rejected;

    // Dofs //
    std::map<size_t, std::vector<std::vector<Dof> > > dof;

  protected:
    // Differential From & Order //
    bool   scalar;
    size_t form;
    size_t order;

  public:
    static void Finalize(void);

  public:
    virtual ~FunctionSpace(void);

    bool   isScalar(void) const;
    size_t getForm(void)  const;
    size_t getOrder(void) const;

    const Basis& getBasis(const MElement& element) const;
    const Basis& getBasis(size_t eType)            const;

    void getKeys(const MElement& element, std::vector<Dof>& dof) const;
    void getKeys(const GroupOfElement& goe, std::set<Dof>& dof)  const;
    const std::vector<std::vector<Dof> >&
      getKeys(const GroupOfElement& goe)                         const;

    void getSpecialKeys(const MElement& element, std::string type,
                        std::vector<Dof>& dof) const;
    void getSpecialKeys(const GroupOfElement& goe, std::string type,
                        std::set<Dof>& dof)    const;

    template<typename scalarT>
    void interpolate(const MElement& element,
                     const std::vector<scalarT>& coef,
                     const fullVector<double>& xyz,
                     fullVector<scalarT>& interp) const;

    template<typename scalarT>
    void interpolateInRefSpace(const MElement& element,
                               const std::vector<scalarT>& coef,
                               const fullVector<double>& abc,
                               fullVector<scalarT>& interp) const;

    template<typename scalarT>
    void interpolateDerivative(const MElement& element,
                               const std::vector<scalarT>& coef,
                               const fullVector<double>& xyz,
                               fullVector<scalarT>& interp) const;

    template<typename scalarT>
    void interpolateDerivativeInRefSpace(const MElement& element,
                                         const std::vector<scalarT>& coef,
                                         const fullVector<double>& abc,
                                         fullVector<scalarT>& interp) const;

  protected:
    FunctionSpace(void);
    void build(const std::vector<const GroupOfElement*>& goe,
               const std::vector<const GroupOfElement*>& exl,
               std::string family, std::string option);

    virtual void interpolateInABC(const MElement& element,
                                  const std::vector<double>& coef,
                                  double abc[3],
                                  fullVector<double>& interp) const=0;

    virtual void interpolateDerivativeInABC(const MElement& element,
                                            const std::vector<double>& coef,
                                            double abc[3],
                                            fullVector<double>& interp) const=0;

    static  void navieInvertInPlace(fullMatrix<double>& A);

  private:
    void getBases(const GroupOfElement& goe,
                  std::string family, std::string option);
    void getMyDof(const GroupOfElement& goe);
    void getRejec(const GroupOfElement& goe);

    size_t findMaxType(void);

    void getUnorderedKeys(const MElement& element,
                          std::vector<Dof>& dof) const;
    void markKeys(std::vector<Dof>& dof)         const;

    static void splitRealImag(const std::vector<Complex>& cmplx,
                              std::vector<double>& re,
                              std::vector<double>& im);

    static void combiRealImag(const fullVector<double>& re,
                              const fullVector<double>& im,
                              fullVector<Complex>& cmplx);
  };
}

/**
   @internal
   @fn FunctionSpace::FunctionSpace
   Instatiate a new FunctionSpace
   @endinternal
   **

   @fn FunctionSpace::~FunctionSpace
   Deletes this FunctionSpace
   **

   @fn FunctionSpace::Finalize
   Restarts the FunctionSpace ID system
   **

   @fn FunctionSpace::isScalar
   @return Returns:
   @li true, if this FunstionSpace is scalar
   @li flase, otherwise
   **

   @fn FunctionSpace::getForm
   @return Returns this FunctionSpace differential form (0, 1, 2 or 3)
   **

   @fn FunctionSpace::getOrder
   @return Returns this FunctionSpace order
   **

   @fn FunctionSpace::getBasis(const MElement& element) const
   @param element A MElement
   @return Returns the Basis associated to the given element
   **

   @fn FunctionSpace::getBasis(size_t eType) const
   @param eType A geomtrical element type tag
   @return Returns the Basis associated to the given geomtrical element type tag
   **

   @fn void FunctionSpace::getKeys(const MElement&,std::vector<Dof>&) const
   @param element A MElement
   @param dof A vector of Dof%s

   Populates the given vector with the Dof%s associated to the given MElement
   **

   @fn void FunctionSpace::getKeys(const GroupOfElement&, std::set<Dof>&) const
   @param goe A GroupOfElement
   @param dof A set of Dof%s

   Populates the given set with the Dof%s associated to the MElement%s
   of the given GroupOfElement
   **

   @fn const std::vector<std::vector<Dof> >& FunctionSpace::getKeys(const GroupOfElement&) const
   @param goe A GroupOfElement
   @return Returns a vector of vector of Dof such that:
   dof[i][j] is the jth Dof of the ith element of the given GroupOfElement
   **

   @fn FunctionSpace::getSpecialKeys(const MElement&,std::string,std::vector<Dof>&) const
   @param element A MElement
   @param type A string
   @param dof A vector of Dof%s

   Populates the given vector with the Dof%s associated to the given MElement.
   In contrast to FunctionSpace::getKeys(element, dof),
   only the Dof%s associated to the basis functions matching
   getBasis(element).getIndexFilterByType(type, ...) will be inserted in dof.
   **

   @fn FunctionSpace::getSpecialKeys(const GroupOfElement&, std::string,std::set<Dof>&) const
   @param goe A GroupOfElement
   @param type A string
   @param dof A set of Dof%s

   Populates the given set with the Dof%s associated to the given GroupOfElement.
   In contrast to FunctionSpace::getKeys(goe, dof),
   only the Dof%s associated to the basis functions matching
   getBasis(element).getIndexFilterByType(type, ...) will be inserted in dof.
   **

   @fn FunctionSpace::interpolate
   @param element The MElement to interpolate on
   @param coef The coefficients of the interpolation
   @param xyz The coordinate (of a point inside the given element)
   of the interpolation in the @em physical space
   @param interp A vector

   Interpolates this FunctionSpace on the given element and on the given point
   for the given interpolation coefficients.
   The interpolated value is stored in interp.
   If this->isScalar() is true then interp is a 1D vector,
   otherwise it is a 3D vector.
   **

   @fn FunctionSpace::interpolateInRefSpace
   @param element The MElement to interpolate on
   @param coef The coefficients of the interpolation
   @param abc The coordinate (of a point inside the given element)
   of the interpolation in the @em reference space
   @param interp A vector

   Same as FunctionSpace::interpolate() but the coordinate
   of the interpolation point is given in the reference space.
   **

   @fn FunctionSpace::interpolateDerivative
   @param element The MElement to interpolate on
   @param coef The coefficients of the interpolation
   @param xyz The coordinate (of a point inside the given element)
   of the interpolation in the @em physical space
   @param interp A vector

   Same as FunctionSpace::interpolate()
   but interpolates the derivative.
   **

   @fn FunctionSpace::interpolateDerivativeInRefSpace
   @param element The MElement to interpolate on
   @param coef The coefficients of the interpolation
   @param abc The coordinate (of a point inside the given element)
   of the interpolation in the @em reference space
   @param interp A vector

   Same as FunctionSpace::interpolateInRefSpace()
   but interpolates the derivative.
*/

//////////////////////
// Inline Functions //
//////////////////////

inline bool sf::FunctionSpace::isScalar(void) const{
  return scalar;
}

inline size_t sf::FunctionSpace::getForm(void) const{
  return form;
}

inline size_t sf::FunctionSpace::getOrder(void) const{
  return order;
}

inline const sf::Basis&
sf::FunctionSpace::getBasis(const MElement& element) const{
  if(basis[element.getType()])
    return *basis[element.getType()];
  else
    throw Exception("FunctionSpace::getBasis() -- "
                    "no basis build for element type %d", element.getType());
}

inline const sf::Basis&
sf::FunctionSpace::getBasis(size_t eType) const{
  if(eType >= basis.size())
    throw Exception("FunctionSpace::getBasis() -- unknown geometrical type %u",
                    eType);

  if(basis[eType])
    return *basis[eType];
  else
    throw Exception("FunctionSpace::getBasis() -- "
                    "no basis build for element type %d", eType);
}

#endif
