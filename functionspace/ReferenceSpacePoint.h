#ifndef _REFERENCESPACEPOINT_H_
#define _REFERENCESPACEPOINT_H_

#include <string>
#include "ReferenceSpace.h"

/**
   @class ReferenceSpacePoint
   @brief ReferenceSpace for a Point

   This class implements a ReferenceSpace for a Point.
 */

namespace sf{
  class ReferenceSpacePoint: public ReferenceSpace{
  public:
    ReferenceSpacePoint(void);
    virtual ~ReferenceSpacePoint(void);

    virtual std::string toLatex(void) const;
  };
}

/**
   @fn ReferenceSpacePoint::ReferenceSpacePoint
   Instatiate a new ReferenceSpace for a Point
   **

   @fn ReferenceSpacePoint::~ReferenceSpacePoint
   Deletes this ReferenceSpacePoint
*/

#endif
