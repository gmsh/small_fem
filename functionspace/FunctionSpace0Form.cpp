#include "Mapper.h"
#include "GeoHelper.h"
#include "Exception.h"
#include "FunctionSpace0Form.h"

using namespace std;
using namespace sf;

sf::FunctionSpace0Form::
FunctionSpace0Form(const vector<const GroupOfElement*>& goe,
                   const vector<const GroupOfElement*>& exclude,
                   size_t order, string family){
  // Init
  init(goe, exclude, order, family);
}

sf::FunctionSpace0Form::
FunctionSpace0Form(const vector<const GroupOfElement*>& goe,
                   size_t order, string family){
  // Dummy exclude
  vector<const GroupOfElement*> dummy;

  // Init
  init(goe, dummy, order, family);
}

sf::FunctionSpace0Form::
FunctionSpace0Form(const GroupOfElement& goe, size_t order, string family){
  // Temp vector
  vector<const GroupOfElement*> tmp(1);
  tmp[0] = &goe;

  // Dummy exclude
  vector<const GroupOfElement*> dummy;

  // Init
  init(tmp, dummy, order, family);
}

sf::FunctionSpace0Form::~FunctionSpace0Form(void){
  // Done by FunctionSpace
}

void sf::FunctionSpace0Form::init(const vector<const GroupOfElement*>& goe,
                                  const vector<const GroupOfElement*>& exclude,
                                  size_t order, string family){
  // Check
  if(order == 0)
    throw Exception("FunctionSpace0Form: "
                    "Cannot have a order 0 scalar function space");
  // Init
  this->scalar = true;
  this->form   = 0;
  this->order  = order;

  // Build FunctionSpace
  build(goe, exclude, family, "");
}

void sf::FunctionSpace0Form::
interpolateInABC(const MElement& element,
                 const vector<double>& coef,
                 double abc[3],
                 fullVector<double>& interp) const{

  // Get Basis Functions //
  const Basis& basis = getBasis(element);
  const size_t nFun  = basis.getNFunction();
  fullMatrix<double> fun(nFun, 1);

  basis.getFunctions(fun, element, abc[0], abc[1], abc[2]);

  // Get All Dofs
  vector<Dof> myDof;
  getKeys(element, myDof);

  // Interpolate (in Reference Place) //
  interp.setAll(0);
  for(size_t i = 0; i < nFun; i++)
    if(myDof[i] != Dof::RejectedDof())
      interp(0) += fun(i, 0) * coef[i];
}

void sf::FunctionSpace0Form::
interpolateDerivativeInABC(const MElement& element,
                           const vector<double>& coef,
                           double abc[3],
                           fullVector<double>& interp) const{

  // Get Jacobian //
  fullMatrix<double> invJac(3, 3);
  double det =
    ReferenceSpaceManager::getJacobian(element, abc[0], abc[1], abc[2], invJac);
  GeoHelper<double>::mat3by3InvertInPlace(invJac, det);

  // Get Basis Functions //
  const Basis& basis = getBasis(element);
  const size_t nFun  = basis.getNFunction();
  fullMatrix<double> fun(nFun, 3);

  basis.getDerivative(fun, element, abc[0], abc[1], abc[2]);

  // Get All Dofs
  vector<Dof> myDof;
  getKeys(element, myDof);

  // Interpolate (in Reference Place) //
  fullMatrix<double> val(1, 3);
  val(0, 0) = 0;
  val(0, 1) = 0;
  val(0, 2) = 0;

  for(size_t i = 0; i < nFun; i++){
    if(myDof[i] != Dof::RejectedDof()){
      val(0, 0) += fun(i, 0) * coef[i];
      val(0, 1) += fun(i, 1) * coef[i];
      val(0, 2) += fun(i, 2) * coef[i];
    }
  }

  // Map //
  interp.setAll(0);
  Mapper::hCurl(val, 0, 0, invJac, interp);
}
