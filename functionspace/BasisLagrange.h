#ifndef _BASISLAGRANGE_H_
#define _BASISLAGRANGE_H_

#include "gmsh/fullMatrix.h"
#include "gmsh/polynomialBasis.h"
#include "BasisLocal.h"
#include "FunctionSpace0Form.h"
#include "FunctionSpace1Form.h"

/**
   @interface BasisLagrange
   @brief Interface for Lagrange Basis

   This is an interface for Lagrange Basis.

   These local scalar Basis allow a coefficient matrix and a monomial matrix
   to be consulted.

   Coefficients from an other Basis can be projected into a Lagrange Basis.
*/

namespace sf{
  class BasisLagrange: public BasisLocal{
  protected:
    polynomialBasis*        lBasis;   // Lagrange Basis
    fullMatrix<double>*     lPoint;   // Lagrange Points

    // Orientation reordering //
    std::vector<std::vector<size_t> > idx;

    // PreEvaluation //
    mutable bool preEvaluated;
    mutable bool preEvaluatedGrad;

    mutable fullMatrix<double>** preEvaluatedFunction;
    mutable fullMatrix<double>** preEvaluatedGradFunction;

  public:
    virtual ~BasisLagrange(void);

    virtual void getFunctions(fullMatrix<double>& retValues,
                              const MElement& element,
                              double u, double v, double w) const;

    virtual void getFunctions(fullMatrix<double>& retValues,
                              size_t orientation,
                              double u, double v, double w) const;

    virtual void getDerivative(fullMatrix<double>& retValues,
                               const MElement& element,
                               double u, double v, double w) const;

    virtual void preEvaluateFunctions(const fullMatrix<double>& point) const;
    virtual void preEvaluateDerivatives(const fullMatrix<double>& point) const;

    virtual const fullMatrix<double>&
      getPreEvaluatedFunctions(const MElement& element) const;

    virtual const fullMatrix<double>&
      getPreEvaluatedDerivatives(const MElement& element) const;

    virtual const fullMatrix<double>&
      getPreEvaluatedFunctions(size_t orientation) const;

    virtual const fullMatrix<double>&
      getPreEvaluatedDerivatives(size_t orientation) const;

    const fullMatrix<double>& getCoefficient(void) const;
    const fullMatrix<double>& getMonomial(void) const;

    template<typename scalarT>
    std::vector<scalarT> project(const MElement& element,
                                 const std::vector<scalarT>& coef,
                                 const FunctionSpace& fSpace,
                                 bool derivative = false);

    virtual std::string toString(void) const;

  protected:
    BasisLagrange(void);
    void getReordering(size_t nVertexPerElm, size_t nEdgePerElm,
                       size_t nFacePerElm,   size_t preFlipEdge);
    void reorderCommon(fullMatrix<double>& mat, size_t orientation) const;
    virtual void reorder(fullMatrix<double>& mat, size_t orientation) const = 0;

  private:
    static void flip(std::vector<size_t>& a);
  };
}

/**
   @internal
   @fn BasisLagrange::BasisLagrange
   Instanciates an new BasisLagrange
   @endinternal
   **

   @fn BasisLagrange::~BasisLagrange
   Deletes this BasisLagrange
   **

   @fn BasisLagrange::getCoefficient
   @return Returns the Coefficient Matrix
   **

   @fn BasisLagrange::getMonomial
   @return Returns the Monomial Matrix
   **

   @fn BasisLagrange::project
   @param element A MElement
   @param coef A vector of coefficient associated to the given element
   @param fSpace The FunctionSpace of the given coefficients
   @param derivative A boolean
   @return Returns a vector with the
   @li derivative of the projection (if derivative is true)
   @li projection (if derivative is false)
   projection of the given coefficients in this BasisLagrange

   If the projected space is a vectorial space,
   the returned vector has a size three times bigger than coef,
   since we need three coefficients with a Lagrange basis,
   when we project a vectorial basis on it (one per direction).
   The Lagranges coefficients corresponding to the ith entry of coef
   are located, in the returned vector, at the index i * 3,
   with the following padding:
   @li index i * 3 + 0 is the projected coefficient for direction x;
   @li index i * 3 + 1 is the projected coefficient for direction y;
   @li index i * 3 + 2 is the projected coefficient for direction z.
 */

//////////////////////
// Inline Functions //
//////////////////////

inline const fullMatrix<double>& sf::BasisLagrange::
getCoefficient(void) const{
  return lBasis->coefficients;
}

inline const fullMatrix<double>& sf::BasisLagrange::
getMonomial(void) const{
  return lBasis->monomials;
}

#endif
