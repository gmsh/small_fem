#include "Mapper.h"
#include "GeoHelper.h"
#include "FunctionSpace1Form.h"

using namespace std;
using namespace sf;

sf::FunctionSpace1Form::
FunctionSpace1Form(const vector<const GroupOfElement*>& goe,
                   const vector<const GroupOfElement*>& exclude,
                   size_t order, string family, string option){
  // Init
  init(goe, exclude, order, family, option);
}

sf::FunctionSpace1Form::
FunctionSpace1Form(const vector<const GroupOfElement*>& goe,
                   size_t order, string family, string option){
  // Dummy Exclude
  vector<const GroupOfElement*> dummy;

  // Init
  init(goe, dummy, order, family, option);
}

sf::FunctionSpace1Form::
FunctionSpace1Form(const GroupOfElement& goe,
                   size_t order, string family, string option){
  // Temp vector
  vector<const GroupOfElement*> tmp(1);
  tmp[0] = &goe;

  // Dummy Exclude
  vector<const GroupOfElement*> dummy;

  // Init
  init(tmp, dummy, order, family, option);
}

sf::FunctionSpace1Form::~FunctionSpace1Form(void){
  // Done by FunctionSpace
}

void sf::FunctionSpace1Form::init(const vector<const GroupOfElement*>& goe,
                                  const vector<const GroupOfElement*>& exclude,
                                  size_t order, string family, string option){
  // Init
  this->scalar = false;
  this->form   = 1;
  this->order  = order;

  // Build FunctionSpace
  build(goe, exclude, family, option);
}

void sf::FunctionSpace1Form::
interpolateInABC(const MElement& element,
                 const vector<double>& coef,
                 double abc[3],
                 fullVector<double>& interp) const{
  // Get Jacobian //
  fullMatrix<double> invJac(3, 3);
  double det =
    ReferenceSpaceManager::getJacobian(element, abc[0], abc[1], abc[2], invJac);
  GeoHelper<double>::mat3by3InvertInPlace(invJac, det);

  // Get Basis Functions //
  const Basis& basis = getBasis(element);
  const size_t nFun  = basis.getNFunction();
  fullMatrix<double> fun(nFun, 3);

  basis.getFunctions(fun, element, abc[0], abc[1], abc[2]);

  // Get All Dofs
  vector<Dof> myDof;
  getKeys(element, myDof);

  // Interpolate (in Reference Place) //
  fullMatrix<double> val(1, 3);
  val(0, 0) = 0;
  val(0, 1) = 0;
  val(0, 2) = 0;

  for(size_t i = 0; i < nFun; i++){
    if(myDof[i] != Dof::RejectedDof()){
      val(0, 0) += fun(i, 0) * coef[i];
      val(0, 1) += fun(i, 1) * coef[i];
      val(0, 2) += fun(i, 2) * coef[i];
    }
  }

  // Map //
  interp.setAll(0);
  Mapper::hCurl(val, 0, 0, invJac, interp);
}

void sf::FunctionSpace1Form::
interpolateDerivativeInABC(const MElement& element,
                           const vector<double>& coef,
                           double abc[3],
                           fullVector<double>& interp) const{
  // Get Jacobian //
  fullMatrix<double> jac(3, 3);
  double det =
    ReferenceSpaceManager::getJacobian(element, abc[0], abc[1], abc[2], jac);

  // Get Basis Functions //
  const Basis& basis = getBasis(element);
  const size_t nFun  = basis.getNFunction();
  fullMatrix<double> fun(nFun, 3);

  basis.getDerivative(fun, element, abc[0], abc[1], abc[2]);

  // Get All Dofs
  vector<Dof> myDof;
  getKeys(element, myDof);

  // Interpolate (in Reference Place) //
  fullMatrix<double> val(1, 3);
  val(0, 0) = 0;
  val(0, 1) = 0;
  val(0, 2) = 0;

  for(size_t i = 0; i < nFun; i++){
    if(myDof[i] != Dof::RejectedDof()){
      val(0, 0) += fun(i, 0) * coef[i];
      val(0, 1) += fun(i, 1) * coef[i];
      val(0, 2) += fun(i, 2) * coef[i];
    }
  }

  // Map //
  interp.setAll(0);
  Mapper::hDiv(val, 0, 0, jac, det, interp);
}
