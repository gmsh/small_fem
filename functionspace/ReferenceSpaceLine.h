#ifndef _REFERENCESPACELINE_H_
#define _REFERENCESPACELINE_H_

#include <string>
#include "ReferenceSpace.h"

/**
   @class ReferenceSpaceLine
   @brief ReferenceSpace for a Line

   This class implements a ReferenceSpace for a Line.
 */

namespace sf{
  class ReferenceSpaceLine: public ReferenceSpace{
  public:
    ReferenceSpaceLine(void);
    virtual ~ReferenceSpaceLine(void);

    virtual std::string toLatex(void) const;
  };
}

/**
   @fn ReferenceSpaceLine::ReferenceSpaceLine
   Instatiate a new ReferenceSpace for a Line
   **

   @fn ReferenceSpaceLine::~ReferenceSpaceLine
   Deletes this ReferenceSpaceLine
*/

#endif
