#ifndef _BASISLINENEDELEC_H_
#define _BASISLINENEDELEC_H_

#include "BasisHierarchical1Form.h"

/**
   @class BasisLineNedelec
   @brief Nedelec Basis for Lines

   This class can instantiate a Nedelec Basis for Lines
*/

namespace sf{
  class BasisLineNedelec: public BasisHierarchical1Form{
  public:
    //! Returns a new Nedelec Basis for Lines
    //!
    BasisLineNedelec(void);

    //! Deletes this Basis
    //!
    virtual ~BasisLineNedelec(void);
  };
}

#endif
