#include "FunctionSpace.h"

using namespace std;

namespace sf{
  template<> void sf::FunctionSpace::
  interpolate<double>(const MElement& element,
                      const vector<double>& coef,
                      const fullVector<double>& xyz,
                      fullVector<double>& interp) const{

    // Get ABC Space coordinate //
    double abc[3];
    ReferenceSpaceManager::mapFromXYZtoABC(element, xyz(0),xyz(1),xyz(2), abc);

    // Interpolate in ABC //
    interpolateInABC(element, coef, abc, interp);
  }

  template<> void sf::FunctionSpace::
  interpolateInRefSpace(const MElement& element,
                        const vector<double>& coef,
                        const fullVector<double>& abc,
                        fullVector<double>& interp) const{

    // Interpolate in ABC //
    double tmp[3] = {abc(0), abc(1), abc(2)};
    interpolateInABC(element, coef, tmp, interp);
  }

  template<> void sf::FunctionSpace::
  interpolateDerivative(const MElement& element,
                        const vector<double>& coef,
                        const fullVector<double>& xyz,
                        fullVector<double>& interp) const{

    // Get ABC Space coordinate //
    double abc[3];
    ReferenceSpaceManager::mapFromXYZtoABC(element, xyz(0),xyz(1),xyz(2), abc);

    // Interpolate in ABC //
    interpolateDerivativeInABC(element, coef, abc, interp);
  }

  template<> void sf::FunctionSpace::
  interpolateDerivativeInRefSpace(const MElement& element,
                                  const vector<double>& coef,
                                  const fullVector<double>& abc,
                                  fullVector<double>& interp) const{

    // Interpolate in ABC //
    double tmp[3] = {abc(0), abc(1), abc(2)};
    interpolateDerivativeInABC(element, coef, tmp, interp);
  }
}
