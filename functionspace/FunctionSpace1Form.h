#ifndef _FUNCTIONSPACE1FORM_H_
#define _FUNCTIONSPACE1FORM_H_

#include "ReferenceSpaceManager.h"
#include "FunctionSpace.h"

/**
   @class FunctionSpace1Form
   @brief A FunctionSpace for 1-forms

   This class is a FunctionSpaces for 1-forms.
*/

namespace sf{
  class FunctionSpace1Form : public FunctionSpace{
  public:
    FunctionSpace1Form(const std::vector<const GroupOfElement*>& goe,
                       const std::vector<const GroupOfElement*>& exclude,
                       size_t order,
                       std::string family = Basis::Family::Hierarchical,
                       std::string option = "");

    FunctionSpace1Form(const std::vector<const GroupOfElement*>& goe,
                       size_t order,
                       std::string family = Basis::Family::Hierarchical,
                       std::string option = "");

    FunctionSpace1Form(const GroupOfElement& goe,
                       size_t order,
                       std::string family = Basis::Family::Hierarchical,
                       std::string option = "");

    virtual ~FunctionSpace1Form(void);

  protected:
    virtual void interpolateInABC(const MElement& element,
                                  const std::vector<double>& coef,
                                  double abc[3],
                                  fullVector<double>& interp) const;

    virtual void interpolateDerivativeInABC(const MElement& element,
                                            const std::vector<double>& coef,
                                            double abc[3],
                                            fullVector<double>& interp) const;

  private:
    void init(const std::vector<const GroupOfElement*>& goe,
              const std::vector<const GroupOfElement*>& exclude,
              size_t order, std::string family, std::string option);
  };
}

/**
   @fn FunctionSpace1Form::FunctionSpace1Form(const std::vector<const GroupOfElement*>&,const std::vector<const GroupOfElement*>&,size_t,std::string,std::string)
   @param goe A vector of GroupOfElement
   @param exclude An other of GroupOfElement
   @param order A natural number
   @param family A string (defaulted to 'hierarchical')
   @param option A string (defaulted to '')

   Instanciates a new FunctionSpace1Form
   on the GroupOfElement%s of 'goe',
   with the exception of the GroupOfElement%s of 'exclude',
   and with the given order

   The instanciated FunctionSpace will use the requested Basis family:
   @li If family is equal to 'lagrange' a Lagrange Basis will be used
   @li If family is equal to 'hierarchical' a hierarchical Basis will be used

   Futhermore, the following options can be added:
   @li If option is equal to '', no options are used
   @li If option is equal to 'nogradient',
   the subspace used within this FunctionSpace1Form will exclude gradients

   @see See BasisGenerator::generate()
   **

   @fn FunctionSpace1Form::FunctionSpace1Form(const std::vector<const GroupOfElement*>&,size_t,std::string,std::string)
   @param goe A vector of GroupOfElement
   @param order A natural number
   @param family A string (defaulted to 'hierarchical')
   @param option A string (defaulted to '')

   Same as FunctionSpace1Form::FunctionSpace1Form(goe, [], order, family, option)
   **

   @fn FunctionSpace1Form::FunctionSpace1Form(const GroupOfElement&,size_t,std::string,std::string)
   @param goe A GroupOfElement
   @param order A natural number
   @param family A string  (defaulted to 'hierarchical')
   @param option A string (defaulted to '')

   Same as FunctionSpace1Form::FunctionSpace1Form([goe], [], order, family, option)
   **

   @fn FunctionSpace1Form::~FunctionSpace1Form
   Deletes this FunctionSpace1Form
*/

#endif
