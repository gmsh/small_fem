#include "gmsh/GmshDefines.h"
#include "BasisGenerator.h"
#include "Exception.h"

#include "BasisPoint0Form.h"
#include "BasisPointLagrange.h"

#include "BasisLine0Form.h"
#include "BasisLine1Form.h"
#include "BasisLineNedelec.h"
#include "BasisLineLagrange.h"

#include "BasisTri0Form.h"
#include "BasisTri1Form.h"
#include "BasisTriNedelec.h"
#include "BasisTriLagrange.h"

#include "BasisQuad0Form.h"
#include "BasisQuad1Form.h"
#include "BasisQuadNedelec.h"
#include "BasisQuadLagrange.h"

#include "BasisTet0Form.h"
#include "BasisTet1Form.h"
#include "BasisTetNedelec.h"
#include "BasisTetLagrange.h"

#include "BasisHex0Form.h"
#include "BasisHex1Form.h"
#include "BasisHexLagrange.h"

using namespace sf;

BasisGenerator::BasisGenerator(void){
}

BasisGenerator::~BasisGenerator(void){
}

BasisLocal* BasisGenerator::generate(size_t elementType,
                                     size_t form,
                                     size_t order,
                                     std::string family,
                                     std::string option){

  if(!family.compare(Basis::Family::Hierarchical))
    return generateHierarchical(elementType, form, order, option);

  else if(!family.compare(Basis::Family::Lagrange))
    return generateLagrange(elementType, form, order);

  else
    throw Exception("Unknwown Basis Family: %s", family.c_str());
}

BasisLocal* BasisGenerator::generateHierarchical(size_t elementType,
                                                 size_t form,
                                                 size_t order,
                                                 std::string option){
  bool noGrad = !option.compare(Basis::Option::NoGradient); // True if no grad

  switch(elementType){
  case TYPE_PNT: return pntHierarchicalGen(form, order, noGrad);
  case TYPE_LIN: return linHierarchicalGen(form, order, noGrad);
  case TYPE_TRI: return triHierarchicalGen(form, order, noGrad);
  case TYPE_QUA: return quaHierarchicalGen(form, order, noGrad);
  case TYPE_TET: return tetHierarchicalGen(form, order, noGrad);
  case TYPE_HEX: return hexHierarchicalGen(form, order, noGrad);

  default: throw Exception("Unknown Element Type (%d) for Basis Generation",
                           elementType);
  }
}

BasisLocal* BasisGenerator::generateLagrange(size_t elementType,
                                             size_t form,
                                             size_t order){
  if(form != 0)
    throw
      Exception("Cannot Have a %d-Form Lagrange Basis (0-Form only)",
                form);

  switch(elementType){
  case TYPE_PNT: return new BasisPointLagrange();
  case TYPE_LIN: return new BasisLineLagrange(order);
  case TYPE_TRI: return new BasisTriLagrange(order);
  case TYPE_QUA: return new BasisQuadLagrange(order);
  case TYPE_TET: return new BasisTetLagrange(order);
  case TYPE_HEX: return new BasisHexLagrange(order);

  default: throw Exception("Unknown Element Type (%d) for Basis Generation",
                           elementType);
  }
}

BasisLocal* BasisGenerator::pntHierarchicalGen(size_t form,
                                               size_t order,
                                               bool noGrad){
  switch(form){
  case 0: return new BasisPoint0Form();
  case 1: throw Exception("1-form not implemented on Points");
  case 2: throw Exception("2-form not implemented on Points");
  case 3: throw Exception("3-form not implemented on Points");

  default: throw Exception("There is no %d-form", form);
  }
}

BasisLocal* BasisGenerator::linHierarchicalGen(size_t form,
                                               size_t order,
                                               bool noGrad){
  switch(form){
  case 0: return new BasisLine0Form(order);
  case 1:
    if (order == 0) return new BasisLineNedelec();
    else            return new BasisLine1Form(order, noGrad);

  case 2: throw Exception("2-form not implemented on Lines");
  case 3: throw Exception("3-form not implemented on Lines");

  default: throw Exception("There is no %d-form", form);
  }
}

BasisLocal* BasisGenerator::triHierarchicalGen(size_t form,
                                               size_t order,
                                               bool noGrad){
  switch(form){
  case 0: return new BasisTri0Form(order);
  case 1:
    if (order == 0) return new BasisTriNedelec();
    else            return new BasisTri1Form(order, noGrad);

  case 2: throw Exception("2-form not implemented on Triangles");
  case 3: throw Exception("3-form not implemented on Triangles");

  default: throw Exception("There is no %d-form", form);
  }
}

BasisLocal* BasisGenerator::quaHierarchicalGen(size_t form,
                                               size_t order,
                                               bool noGrad){
  switch(form){
  case 0: return new BasisQuad0Form(order);
  case 1:
    if (order == 0) return new BasisQuadNedelec();
    else            return new BasisQuad1Form(order, noGrad);

  case 2: throw Exception("2-form not implemented on Quads");
  case 3: throw Exception("3-form not implemented on Quads");

  default: throw Exception("There is no %d-form", form);
  }
}

BasisLocal* BasisGenerator::tetHierarchicalGen(size_t form,
                                               size_t order,
                                               bool noGrad){
  switch(form){
  case 0: return new BasisTet0Form(order);
  case 1:
    if (order == 0) return new BasisTetNedelec();
    else            return new BasisTet1Form(order, noGrad);

  case 2: throw Exception("2-form not implemented on Tetrahedrons");
  case 3: throw Exception("3-form not implemented on Tetrahedrons");

  default: throw Exception("There is no %d-form", form);
  }
}

BasisLocal* BasisGenerator::hexHierarchicalGen(size_t form,
                                               size_t order,
                                               bool noGrad){
  switch(form){
  case 0: return new BasisHex0Form(order);
    //case 1: return new BasisHex1Form(order, noGrad);
  case 1: throw Exception("1-form not implemented on Hexs");
  case 2: throw Exception("2-form not implemented on Hexs");
  case 3: throw Exception("3-form not implemented on Hexs");

  default: throw Exception("There is no %d-form", form);
  }
}
