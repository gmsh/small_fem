#ifndef _BASISQUAD0FORM_H_
#define _BASISQUAD0FORM_H_

#include "BasisHierarchical0Form.h"

/**
   @class BasisQuad0Form
   @brief A 0-form Basis for Quads

   This class can instantiate a 0-form Basis (high or low order) for Quads.

   It uses a variation of
   <a href="http://www.hpfem.jku.at/publications/szthesis.pdf">Zaglmayr's</a>
   Basis for high order Polynomial%s generation.@n

   The following mapping has been applied to Zaglmayr's Basis for Quads:
   @li @f$x = \frac{u + 1}{2}@f$
   @li @f$y = \frac{v + 1}{2}@f$
*/

namespace sf{
  class BasisQuad0Form: public BasisHierarchical0Form{
  public:
    //! @param order The order of the Basis
    //!
    //! Returns a new 0-form Basis for Quads of the given order
    BasisQuad0Form(size_t order);

    //! @return Deletes this Basis
    //!
    virtual ~BasisQuad0Form(void);
  };
}

#endif
