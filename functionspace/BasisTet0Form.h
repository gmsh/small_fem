#ifndef _BASISTET0FORM_H_
#define _BASISTET0FORM_H_

#include "BasisHierarchical0Form.h"

/**
   @class BasisTet0Form
   @brief A 0-form Basis for Tetrahedra

   This class can instantiate a 0-form Basis
   (high or low order) for Tetrahedra.

   It uses
   <a href="http://www.hpfem.jku.at/publications/szthesis.pdf">Zaglmayr's</a>
   Basis for high order Polynomial%s generation.@n
 */

namespace sf{
  class BasisTet0Form: public BasisHierarchical0Form{
  public:
    //! @param order The order of the Basis
    //!
    //! Returns a new 0-form Basis for Tetrahedra of the given order
    BasisTet0Form(size_t order);

    //! Deletes this Basis
    //!
    virtual ~BasisTet0Form(void);
  };
}

#endif
