#include "Basis.h"

#include "BasisGenerator.h"
#include "Exception.h"

using namespace sf;

const std::string Basis::Family::Hierarchical = "hierarchical";
const std::string Basis::Family::Lagrange     = "lagrange";
const std::string Basis::Option::NoGradient   = "nogradient";
const std::string Basis::Filter::LowOrder     = "loworder";

Basis::Basis(void){
}

Basis::~Basis(void){
}

Basis* Basis::copy(void) const{
  if(local)
    return BasisGenerator::generate(type, form, order, family, option);
  else
    throw Exception("Basis::copy(): cannot copy a non-local Basis");
}

void Basis::getIndexFilterByType(std::string type,
                                 std::vector<bool>& filter) const{
  filter.assign(nFunction, false);
}
