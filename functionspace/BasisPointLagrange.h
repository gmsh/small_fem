#ifndef _BASISPOINTLAGRANGE_H_
#define _BASISPOINTLAGRANGE_H_

#include "BasisLagrange.h"

/**
   @class BasisPointLagrange
   @brief Lagrange Basis for Points

   This class can instantiate a Lagrange Basis
   for a Point and for a given order.

   It uses
   <a href="http://geuz.org/gmsh/">gmsh</a> Basis.
 */

namespace sf{
  class BasisPointLagrange: public BasisLagrange{
  public:
    //! Returns a new BasisPointLagrange
    BasisPointLagrange(void);

    //! Deletes this Basis
    //!
    virtual ~BasisPointLagrange(void);

  private:
    virtual void reorder(fullMatrix<double>& mat, size_t orientation) const;
  };
}

#endif
