#ifndef _REFERENCESPACETET_H_
#define _REFERENCESPACETET_H_

#include <string>
#include "ReferenceSpace.h"

/**
   @class ReferenceSpaceTet
   @brief ReferenceSpace for Tetrahedron

   This class implements a ReferenceSpace for a Tetrahedron.
 */

namespace sf{
  class ReferenceSpaceTet: public ReferenceSpace{
  public:
    ReferenceSpaceTet(void);
    virtual ~ReferenceSpaceTet(void);

    virtual std::string toLatex(void) const;
  };
}

/**
   @fn ReferenceSpaceTet::ReferenceSpaceTet
   Instatiate a new ReferenceSpace for a Tetrahedron
   **

   @fn ReferenceSpaceTet::~ReferenceSpaceTet
   Deletes this ReferenceSpaceTet
*/

#endif
