#ifndef _BASISLINELAGRANGE_H_
#define _BASISLINELAGRANGE_H_

#include "BasisLagrange.h"

/**
   @class BasisLineLagrange
   @brief Lagrange Basis for Lines

   This class can instantiate a Lagrange Basis
   for a Line and for a given order.

   It uses
   <a href="http://geuz.org/gmsh/">gmsh</a> Basis.
 */

namespace sf{
  class BasisLineLagrange: public BasisLagrange{
  public:
    //! @param order A natural number
    //!
    //! Returns a new BasisLineLagrange of the given order
    BasisLineLagrange(size_t order);

    //! Deletes this Basis
    //!
    virtual ~BasisLineLagrange(void);

  private:
    virtual void reorder(fullMatrix<double>& mat, size_t orientation) const;
  };
}

#endif
