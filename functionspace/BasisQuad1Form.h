#ifndef _BASISQUAD1FORM_H_
#define _BASISQUAD1FORM_H_

#include "BasisHierarchical1Form.h"

/**
   @class BasisQuad1Form
   @brief A 1-form Basis for Quads

   This class can instantiate a 1-form Basis (high or low order) for Quads.

   It uses a variation of
   <a href="http://www.hpfem.jku.at/publications/szthesis.pdf">Zaglmayr's</a>
   Basis for high order Polynomial%s generation.@n

   The following mapping has been applied to Zaglmayr's Basis for Quads:
   @li @f$x = \frac{u + 1}{2}@f$
   @li @f$y = \frac{v + 1}{2}@f$
*/

namespace sf{
  class BasisQuad1Form: public BasisHierarchical1Form{
  public:
    //! @param order The order of the Basis
    //! @param noGrad A boolean
    //!
    //! Returns a new 1-form Basis for Quads of the given order.
    //! If noGrad is false, a full basis is constructed,
    //! otherwise gradient functions are ignored.
    BasisQuad1Form(size_t order, bool noGrad = false);

    //! Deletes this Basis
    //!
    virtual ~BasisQuad1Form(void);
  };
}

#endif
