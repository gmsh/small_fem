#ifndef _FUNCTIONSPACE0FORM_H_
#define _FUNCTIONSPACE0FORM_H_

#include "ReferenceSpaceManager.h"
#include "FunctionSpace.h"

/**
   @class FunctionSpace0Form
   @brief A FunctionSpace for 0-forms

   This class is a FunctionSpaces for 0-forms.
*/

namespace sf{
  class FunctionSpace0Form : public FunctionSpace{
  public:
    FunctionSpace0Form(const std::vector<const GroupOfElement*>& goe,
                       const std::vector<const GroupOfElement*>& exclude,
                       size_t order,
                       std::string family = Basis::Family::Hierarchical);

    FunctionSpace0Form(const std::vector<const GroupOfElement*>& goe,
                       size_t order,
                       std::string family = Basis::Family::Hierarchical);

    FunctionSpace0Form(const GroupOfElement& goe,
                       size_t order,
                       std::string family = Basis::Family::Hierarchical);

    virtual ~FunctionSpace0Form(void);

  protected:
    virtual void interpolateInABC(const MElement& element,
                                  const std::vector<double>& coef,
                                  double abc[3],
                                  fullVector<double>& interp) const;

    virtual void interpolateDerivativeInABC(const MElement& element,
                                            const std::vector<double>& coef,
                                            double abc[3],
                                            fullVector<double>& interp) const;

  private:
    void init(const std::vector<const GroupOfElement*>& goe,
              const std::vector<const GroupOfElement*>& exclude,
              size_t order, std::string family);
  };
}

/**
   @fn FunctionSpace0Form::FunctionSpace0Form(const std::vector<const GroupOfElement*>&,const std::vector<const GroupOfElement*>&,size_t,std::string)
   @param goe A vector of GroupOfElement
   @param exclude An other of GroupOfElement
   @param order A natural number
   @param family A string (defaulted to 'hierarchical')

   Instanciates a new FunctionSpace0Form
   on the GroupOfElement%s of 'goe',
   with the exception of the GroupOfElement%s of 'exclude',
   and with the given order

   The instanciated FunctionSpace will use the requested Basis family:
   @li If family is equal to 'lagrange' a Lagrange Basis will be used
   @li If family is equal to 'hierarchical' a hierarchical Basis will be used

   @see See BasisGenerator::generate()
   **

   @fn FunctionSpace0Form::FunctionSpace0Form(const std::vector<const GroupOfElement*>&,size_t,std::string)
   @param goe A vector of GroupOfElement
   @param order A natural number
   @param family A string (defaulted to 'hierarchical')

   Same as FunctionSpace0Form::FunctionSpace0Form(goe, [], order, family)
   **

   @fn FunctionSpace0Form::FunctionSpace0Form(const GroupOfElement&,size_t,std::string)
   @param goe A GroupOfElement
   @param order A natural number
   @param family A string  (defaulted to 'hierarchical')

   Same as FunctionSpace0Form::FunctionSpace0Form([goe], [], order, family)
   **

   @fn FunctionSpace0Form::~FunctionSpace0Form
   Deletes this FunctionSpace0Form
*/

#endif
