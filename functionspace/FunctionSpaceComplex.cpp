#include "FunctionSpace.h"

using namespace std;

namespace sf{
  template<> void sf::FunctionSpace::
  interpolate<Complex>(const MElement& element,
                       const vector<Complex>& coef,
                       const fullVector<double>& xyz,
                       fullVector<Complex>& interp) const{
    // Sizes //
    const size_t cN = coef.size();
    const size_t iN = interp.size();

    // Get ABC Space coordinate //
    double abc[3];
    ReferenceSpaceManager::mapFromXYZtoABC(element, xyz(0),xyz(1),xyz(2), abc);

    // Split: real/imaginary //
    vector<double> cRe(cN);
    vector<double> cIm(cN);
    splitRealImag(coef, cRe, cIm);

    // Interpolate in ABC //
    fullVector<double> iRe(iN); interpolateInABC(element, cRe, abc, iRe);
    fullVector<double> iIm(iN); interpolateInABC(element, cIm, abc, iIm);

    // Complex //
    combiRealImag(iRe, iIm, interp);
  }

  template<> void sf::FunctionSpace::
  interpolateInRefSpace(const MElement& element,
                        const vector<Complex>& coef,
                        const fullVector<double>& abc,
                        fullVector<Complex>& interp) const{
    // Sizes //
    const size_t cN = coef.size();
    const size_t iN = interp.size();

    // Split: real/imaginary //
    vector<double> cRe(cN);
    vector<double> cIm(cN);
    splitRealImag(coef, cRe, cIm);

    // Interpolate in ABC //
    double tmp[3] = {abc(0), abc(1), abc(2)};
    fullVector<double> iRe(iN); interpolateInABC(element, cRe, tmp, iRe);
    fullVector<double> iIm(iN); interpolateInABC(element, cIm, tmp, iIm);

    // Complex //
    combiRealImag(iRe, iIm, interp);
  }

  template<> void sf::FunctionSpace::
  interpolateDerivative(const MElement& element,
                        const vector<Complex>& coef,
                        const fullVector<double>& xyz,
                        fullVector<Complex>& interp) const{
    // Sizes //
    const size_t cN = coef.size();
    const size_t iN = interp.size();

    // Get ABC Space coordinate //
    double abc[3];
    ReferenceSpaceManager::mapFromXYZtoABC(element, xyz(0),xyz(1),xyz(2), abc);

    // Split: real/imaginary //
    vector<double> cRe(cN);
    vector<double> cIm(cN);
    splitRealImag(coef, cRe, cIm);

    // Interpolate in ABC //
    fullVector<double> iRe(iN); interpolateDerivativeInABC(element,cRe,abc,iRe);
    fullVector<double> iIm(iN); interpolateDerivativeInABC(element,cIm,abc,iIm);

    // Complex //
    combiRealImag(iRe, iIm, interp);
  }

  template<> void sf::FunctionSpace::
  interpolateDerivativeInRefSpace(const MElement& element,
                                  const vector<Complex>& coef,
                                  const fullVector<double>& abc,
                                  fullVector<Complex>& interp) const{
    // Sizes //
    const size_t cN = coef.size();
    const size_t iN = interp.size();

    // Split: real/imaginary //
    vector<double> cRe(cN);
    vector<double> cIm(cN);
    splitRealImag(coef, cRe, cIm);

    // Interpolate in ABC //
    double tmp[3] = {abc(0), abc(1), abc(2)};

    // Interpolate in ABC //
    fullVector<double> iRe(iN); interpolateDerivativeInABC(element,cRe,tmp,iRe);
    fullVector<double> iIm(iN); interpolateDerivativeInABC(element,cIm,tmp,iIm);

    // Complex //
    combiRealImag(iRe, iIm, interp);
  }

  void FunctionSpace::splitRealImag(const vector<Complex>& cmplx,
                                    vector<double>& re,
                                    vector<double>& im){
    const size_t N = cmplx.size();
    for(size_t i = 0; i < N; i++)
      re[i] = cmplx[i].real();
    for(size_t i = 0; i < N; i++)
      im[i] = cmplx[i].imag();
  }

  void FunctionSpace::combiRealImag(const fullVector<double>& re,
                                    const fullVector<double>& im,
                                    fullVector<Complex>& cmplx){
    const size_t N = re.size();
    for(size_t i = 0; i < N; i++)
      cmplx(i) = Complex(re(i), im(i));
  }
}
