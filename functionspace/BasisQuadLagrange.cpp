#include "gmsh/ElementType.h"
#include "gmsh/GmshDefines.h"
#include "gmsh/pointsGenerators.h"
#include "BasisQuadLagrange.h"

using namespace sf;

BasisQuadLagrange::BasisQuadLagrange(size_t order){
  // If order 0 (Nedelec): use order 1
  if(order == 0)
    order = 1;

  // Set Basis Type //
  this->order = order;

  type = TYPE_QUA;
  dim  = 2;

  nVertex   = 4;
  nEdge     = 4 * (order - 1);
  nFace     =     (order - 1) * (order - 1);
  nCell     = 0;
  nFunction = nVertex + nEdge + nFace + nCell;

  // Init polynomialBasis //
  lBasis = new polynomialBasis(ElementType::getType(TYPE_QUA, order, false));

  // Init Lagrange Point //
  lPoint = new fullMatrix<double>(gmshGeneratePointsQuadrangle(order, false));

  // Compute orientation reordering //
  size_t nVertexPerElm = 4;
  size_t nEdgePerElm   = 4;
  size_t nFacePerElm   = 1;
  size_t preFlipEdge   = 3;
  getReordering(nVertexPerElm, nEdgePerElm, nFacePerElm, preFlipEdge);
}

BasisQuadLagrange::~BasisQuadLagrange(void){
  delete lBasis;
  delete lPoint;
}

void BasisQuadLagrange::
reorder(fullMatrix<double>& mat, size_t orientation) const{
  reorderCommon(mat, orientation);
}
