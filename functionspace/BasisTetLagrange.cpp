#include "gmsh/ElementType.h"
#include "gmsh/GmshDefines.h"
#include "gmsh/pointsGenerators.h"
#include "BasisTetLagrange.h"

using namespace sf;

BasisTetLagrange::BasisTetLagrange(size_t order){
  // If order 0 (Nedelec): use order 1
  if(order == 0)
    order = 1;

  // Set Basis Type //
  this->order = order;

  type = TYPE_TET;
  dim  = 3;

  nVertex   = 4;
  nEdge     = 6 * (order - 1);
  nFace     = 2 * (order - 1) * (order - 2);
  nCell     =     (order - 1) * (order - 2) * (order - 3) / 6;
  nFunction = nVertex + nEdge + nFace + nCell;

  // Init polynomialBasis //
  lBasis = new polynomialBasis(ElementType::getType(TYPE_TET, order, false));

  // Init Lagrange Point //
  lPoint = new fullMatrix<double>(gmshGeneratePointsTetrahedron(order, false));
}

BasisTetLagrange::~BasisTetLagrange(void){
  delete lBasis;
  delete lPoint;
}

void BasisTetLagrange::
reorder(fullMatrix<double>& mat, size_t orientation) const{
  throw Exception("TODO");
}
