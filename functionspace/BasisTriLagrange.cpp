#include "gmsh/ElementType.h"
#include "gmsh/GmshDefines.h"
#include "gmsh/pointsGenerators.h"
#include "BasisTriLagrange.h"

using namespace sf;

BasisTriLagrange::BasisTriLagrange(size_t order){
  // If order 0 (Nedelec): use order 1
  if(order == 0)
    order = 1;

  // Set Basis Type //
  this->order = order;

  type = TYPE_TRI;
  dim  = 2;

  nVertex   = 3;
  nEdge     = 3 * (order - 1);
  nFace     =     (order - 1) * (order - 2) / 2;
  nCell     = 0;
  nFunction = nVertex + nEdge + nFace + nCell;

  // Init polynomialBasis //
  lBasis = new polynomialBasis(ElementType::getType(TYPE_TRI, order, false));

  // Init Lagrange Point //
  lPoint = new fullMatrix<double>(gmshGeneratePointsTriangle(order, false));

  // Compute orientation reordering //
  size_t nVertexPerElm = 3;
  size_t nEdgePerElm   = 3;
  size_t nFacePerElm   = 1;
  size_t preFlipEdge   = 2;
  getReordering(nVertexPerElm, nEdgePerElm, nFacePerElm, preFlipEdge);
}

BasisTriLagrange::~BasisTriLagrange(void){
  delete lBasis;
  delete lPoint;
}


void BasisTriLagrange::
reorder(fullMatrix<double>& mat, size_t orientation) const{
  reorderCommon(mat, orientation);
}
