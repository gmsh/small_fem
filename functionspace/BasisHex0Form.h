#ifndef _BASISHEX0FORM_H_
#define _BASISHEX0FORM_H_

#include "BasisHierarchical0Form.h"

/**
   @class BasisHex0Form
   @brief A 0-form Basis for Hexahedra

   This class can instantiate a 0-form Basis
   (high or low order) for Hexahedra.

   It uses
   <a href="http://www.hpfem.jku.at/publications/szthesis.pdf">Zaglmayr's</a>
   Basis for high order Polynomial%s generation.
 */

namespace sf{
  class BasisHex0Form: public BasisHierarchical0Form{
  public:
    //! @param order The order of the Basis
    //!
    //! Returns a new 0-form Basis for Hexahedra of the given order
    BasisHex0Form(size_t order);

    //! @return Deletes this Basis
    //!
    virtual ~BasisHex0Form(void);
  };
}

#endif
