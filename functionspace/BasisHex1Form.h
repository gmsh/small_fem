#ifndef _BASISHEX1FORM_H_
#define _BASISHEX1FORM_H_

#include "BasisHierarchical1Form.h"

/**
   @class BasisHex1Form
   @brief A 1-form Basis for Hexahedra

   This class can instantiate a 1-form Basis
   (high or low order) for Hexahedra.

   It uses
   <a href="http://www.hpfem.jku.at/publications/szthesis.pdf">Zaglmayr's</a>
   Basis for high order Polynomial%s generation.
 */

namespace sf{
  class BasisHex1Form: public BasisHierarchical1Form{
  public:
    //! @param order The order of the Basis
    //!
    //! Returns a new 1-form Basis for Hexahedra of the given order
    BasisHex1Form(size_t order);

    //! Deletes this Basis
    //!
    virtual ~BasisHex1Form(void);
  };
}

#endif
