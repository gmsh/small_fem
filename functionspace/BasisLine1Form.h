#ifndef _BASISLINE1FORM_H_
#define _BASISLINE1FORM_H_

#include "BasisHierarchical1Form.h"

/**
   @class BasisLine1Form
   @brief A 1-form Basis for Lines

   This class can instantiate a 1-form Basis (high or low order) for Lines.

   It uses an adaptation of
   <a href="http://www.hpfem.jku.at/publications/szthesis.pdf">Zaglmayr's</a>
   Basis for high order Polynomial%s generation.@n

   This Basis is a restriction of a Quad Basis to @f$y = 0@f$.

   It also uses the following mapping: @f$x = \frac{u + 1}{2}@f$.
*/

namespace sf{
  class BasisLine1Form: public BasisHierarchical1Form{
  public:
    //! @param order The order of the Basis
    //! @param noGrad A boolean
    //!
    //! Returns a new 1-form Basis for Lines of the given order.
    //! If noGrad is false, a full basis is constructed,
    //! otherwise gradient functions are ignored.
    BasisLine1Form(size_t order, bool noGrad = false);

    //! Deletes this Basis
    //!
    virtual ~BasisLine1Form(void);
  };
}

#endif
