#ifndef _BASISTRI0FORM_H_
#define _BASISTRI0FORM_H_

#include "BasisHierarchical0Form.h"

/**
   @class BasisTri0Form
   @brief A 0-form Basis for Triangles

   This class can instantiate a 0-form Basis
   (high or low order) for Triangles.

   It uses
   <a href="http://www.hpfem.jku.at/publications/szthesis.pdf">Zaglmayr's</a>
   Basis for high order Polynomial%s generation.
 */

namespace sf{
  class BasisTri0Form: public BasisHierarchical0Form{
  public:
    //! @param order The order of the Basis
    //!
    //! Returns a new 0-form Basis for Triangles of the given order
    BasisTri0Form(size_t order);

    //! Deletes this Basis
    //!
    virtual ~BasisTri0Form(void);
  };
}

#endif
