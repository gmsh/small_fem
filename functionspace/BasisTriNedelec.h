#ifndef _BASISTRINEDELEC_H_
#define _BASISTRINEDELEC_H_

#include "BasisHierarchical1Form.h"

/**
   @class BasisTriNedelec
   @brief Nedelec Basis for Triangles

   This class can instantiate a Nedelec Basis for Triangles.
*/

namespace sf{
  class BasisTriNedelec: public BasisHierarchical1Form{
  public:
    //! Returns a new Nedelec Basis for Triangles
    //!
    BasisTriNedelec(void);

    //! Deletes this Basis
    //!
    virtual ~BasisTriNedelec(void);
  };
}

#endif
