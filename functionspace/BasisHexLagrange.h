#ifndef _BASISHEXLAGRANGE_H_
#define _BASISHEXLAGRANGE_H_

#include "BasisLagrange.h"

/**
   @class BasisHexLagrange
   @brief Lagrange Basis for Hexahedra

   This class can instantiate a Lagrange Basis
   for a Hexahedron and for a given order.

   It uses
   <a href="http://geuz.org/gmsh/">gmsh</a> Basis.
 */

namespace sf{
  class BasisHexLagrange: public BasisLagrange{
  public:
    //! @param order A natural number
    //!
    //! Returns a new BasisHexLagrange of the given order
    BasisHexLagrange(size_t order);

    //! Deletes this Basis
    //!
    virtual ~BasisHexLagrange(void);

  private:
    virtual void reorder(fullMatrix<double>& mat, size_t orientation) const;
  };
}

#endif
