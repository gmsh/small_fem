small_fem is a computationally efficient finite element kernel handling high-order Whitney forms

Mandatory dependencies:
 - BLAS/LAPACK
 - MUMPS  (can come from PETSc)
 - Gmsh   (options: BLAS_LAPACK, BUILD_SHARED or BUILD_LIB, PARSER and POST)

Optional dependencies:
 - PETSc  (compiled with MUMPS, C++ and complex arithmetics)
 - SLEPc

Install:
 - mkdir build
 - cd build
 - cmake .. (specify path and other options with ccmake ..)
 - make

Many executables are compiled and linked with the small_fem library.
See 'simulation' folder for the corresponding source code.

Example:
 - Go to ./tests/waveguide
 - gmsh guide3d.geo -3
 - Go to ./build
 - Run: ./waves -type vector -o 3 -k 25 -mode tm -msh ../tests/waveguide/guide3d.msh
 - Solution is written in waveguide.msh
 - It is the FE solution (order 3) of the TM11 mode in a waveguide (k = 25)
