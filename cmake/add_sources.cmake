######################
## Add Source Macro ##
######################
macro(add_sources dir files)
  foreach(file ${files})
    list(APPEND list ${dir}/${file})
  endforeach()

  set(sources ${sources} ${list} PARENT_SCOPE)
endmacro()
