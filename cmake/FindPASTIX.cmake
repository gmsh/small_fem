####################################################
# Try to find PaStiX                               #
#                                                  #
# Once done this will define:                      #
#  PASTIX_FOUND   - system has PaStiX              #
#  PASTIX_INC     - PaStiX include directory       #
#  PASTIX_LIB     - PaStiX library                 #
#                                                  #
# Usage:                                           #
#  find_package(PaStiX)                            #
#                                                  #
# Setting these changes the behavior of the search #
#  PASTIX_INC     - PaStiX include directory       #
#  PASTIX_LIB     - PaStiX library path            #
####################################################

## Try to set PASTIX_LIB and PASTIX_INC from environment variables ##
#####################################################################
if(NOT DEFINED PASTIX_LIB)
  set(PASTIX_LIB $ENV{PASTIX_LIB})
endif()
if(NOT DEFINED PASTIX_INC)
  set(PASTIX_INC $ENV{PASTIX_INC})
endif()

## CMake check and done ##
##########################
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(PASTIX
  "PaStiX could not be found: be sure to set PASTIX_LIB and PASTIX_INC in your environment variables"
  PASTIX_LIB PASTIX_INC)
