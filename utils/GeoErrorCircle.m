%Calcultes the relative L2 error of a 2D geoemtrical curvature. Implemented
% here for upper half-circular curvature by reading points on the
% approximate curve from a text file.
% 
% Author : Ramah Sharaf


R = 2;
wavenumber = 40;
lambda = 2*pi/wavenumber;

Err(idx).Nlambda = nLambda;
Err(idx).error   = l2Err(fileName, R);
elementLength = lambda./N_lambda;

function err = l2Err(fileName,R)
    Msh = readmatrix(fileName,'FileType', 'text', 'Delimiter',' ');
    Geo = zeros(size(Msh));
    Geo(:,1) = Msh(:,1);
    Geo(:,2) = sqrt(R^2 - Msh(:,1).^2);
    [Geos, I] = sortrows(Geo);
    Mshs = Msh(I,:);
    err = sqrt(trapz(Geos(:,1),(Geos(:,2)-Mshs(:,2)).^2)/trapz(Geos(:,1),Geos(:,2).^2));
end