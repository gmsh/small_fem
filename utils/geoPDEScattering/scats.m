function output = scats(d, n_Lambda)
% SCATS: implementation of soft scattering by a disc
%
%   output = scats(d, n_lambda)
%
% Inputs:
%  d:        order of b-splines
%  n_lambda: divisions per wavelength; size of mesh/space 
%            per number of wavelengths in the solution
% Outputs:
%  output.l2error : l2 error
%  output.int_dofs: number of internal degrees of freedom
%
% author: Ramah Sharaf

k           = 40;
Lambda      = 2*pi/k;
L           = 1;
sub         = ceil(L/Lambda * n_Lambda); % mesh refinment parameter
R1 = 1;
R2 = 2;
% * 
crv = nrbdegelev (nrbline ([R1 0 0], [R2 0 0]), 1);
disc_nrbs = nrbrevolve (crv, [0 0 0], [0 0 1], pi); clear crv;

% Discretization parameters
deg = [d d];
nsub = [sub sub];
fprintf('Degree: %d\t n_lambda: %d\n', d,n_Lambda);
%% Problem Setup
%%%%%%%% parameters %%%%%%%%%%%%%%%
src_data    = @(x, y) zeros(size(x));
anaSol      = @(x, y) fAnalyticalLoop(x,y);
wave        = @(x,y) -exp(1i*k*x);
%%%%%%%% Space  %%%%%%%%%%%%%
% * 
geometry = geo_load(disc_nrbs);
% ** 
regularity = deg - 1;
% regularity = [0 0]; global regularity refinment
fprintf('Regularity set to: %d\n', regularity(1));
[rknots, zeta] = kntrefine (geometry.nurbs.knots, nsub-1, deg, regularity);
rule = msh_gauss_nodes(deg + 1);
[qn, qw] = msh_set_quad_nodes(zeta,rule);
% * create mesh and space
msh = msh_cartesian(zeta,qn,qw,geometry);
space = sp_bspline(rknots, deg,msh);
%%%%%%%% Want to implement a NURBS space instead of a B-Spline's? %%%%%%%%%%%%%%%
% 1) create a nurbs space
% geometry = geo_load(disc_nrbs);
% knots = geometry.nurbs.knots;
% rule = msh_gauss_nodes(deg_rt + 1);
% [qn, qw] = msh_set_quad_nodes(knots,rule);
% msh = msh_cartesian(knots,qn,qw,geometry);
% space = sp_nurbs(geometry.nurbs,msh);
% % 2) refine
% msh = msh_refine (msh, nsub_rt);
% space = sp_refine (space, msh, nsub_rt);
% figure;
% nrbspacekntplot(disc_nrbs,space)
% view([0 90])
% 
%%%%%%
%%%%%%%%%% Boundary %%%%%%%%%%%%%%
% bnd_geometry = nrbextract(disc_nrbs);
% identifying boundaries
% >
% figure; ...
% nrbplot(bnd_geometry(1),100), hold on, ...
% nrbplot(bnd_geometry(2),100), hold on, ...
% nrbplot(bnd_geometry(3),100), hold on, ...
% nrbplot(bnd_geometry(4),100), hold on, ...
% set(gca,'color','none')
% hline = findobj(gcf, 'type', 'line');
% set(hline,'LineWidth',3)
% legend('1','2','3','4')

% boundaries 1 and 2 are Homogenous Neumman
% * boundary 3  Dirichelet 
% * boundary 4  ABC
%%
tic
drchlt_dofs = [];
for iref = 3
  drchlt_dofs = union (drchlt_dofs, space.boundary(iref).dofs);
end

int_dofs = setdiff(1:space.ndof,drchlt_dofs); 
n_int_dofs = numel (int_dofs);
fprintf('n_int_dofs: %d\n',n_int_dofs);

u = zeros(space.ndof,1); 
B = op_u_v_tp(space.boundary(3),space.boundary(3),msh.boundary(3));
c = op_f_v_tp(space.boundary(3),msh.boundary(3),wave);
u(drchlt_dofs) = B(drchlt_dofs,drchlt_dofs) \ c(drchlt_dofs); 
gama_bnd = spalloc(space.ndof,space.ndof,numel(space.boundary(4).dofs));
gama_bnd(space.boundary(4).dofs,space.boundary(4).dofs) = op_u_v_tp(space.boundary(4),space.boundary(4),msh.boundary(4));
A = op_gradu_gradv_tp(space,space,msh) - (k^2) * op_u_v_tp(space,space,msh) - 1j*k*gama_bnd;
rhs = zeros(space.ndof,1);
rhs(int_dofs) = rhs(int_dofs) - A(int_dofs,drchlt_dofs)*u(drchlt_dofs); %b - blue * u_B 
fprintf('System: Assembled\n'); 
toc
%%%%%%%%%%Solve%%%%%%%%%%%%%%
tic
u(int_dofs) = A(int_dofs,int_dofs) \ rhs(int_dofs); 
fprintf('System: Solved\n');
toc
output.n_int_dofs = n_int_dofs;
%% Calculate L2 error
tic
output.err_l2 = sp_l2_error (space, msh, u,anaSol) / sp_l2_error (space, msh, u*0,anaSol);
fprintf("L2error: %0.16e\n",output.err_l2); 
toc
%% Export-plot
R0 = 1;
R1 = 2;
k = 40;
nMax = round(k*R0) + 30;
    H1R0 = zeros(2*nMax+1);
    H2R0 = zeros(2*nMax+1);
    H1R1 = zeros(2*nMax+1);
    H2R1 = zeros(2*nMax+1);
    J1R0 = zeros(2*nMax+2);
    for n = -nMax:nMax+1 %index offset +1: (cpp to matlab indexing convension add 1)
        H1R0(n+nMax+1) = besselh(n,1,k*R0);
        H2R0(n+nMax+1) = besselh(n,2,k*R0);
        H1R1(n+nMax+1) = besselh(n,1,k*R1);
        H2R1(n+nMax+1) = besselh(n,2,k*R1);
        J1R0(n+nMax+1) = besselj(n-1,k*R0);
    end
    J1R0(length(J1R0)) = besselj(nMax+1,k*R0);
    
fprintf('Export and plot\n');
tic
%%%%%%%%%% export to PARAVIEW %%%%%%%%%%%%%%%%
%sp_to_vtk (u_rt, space, geometry, [40 40], 'discScattering.vts', 'u');
%%%%%%%%%% visulaize on Matlab %%%%%%%%%%%%%%%%
% sample = {linspace(0, 1, 100), linspace(0, 1, 100)};
[eu, F] = sp_eval (u, space, geometry, {linspace(0, 1, 100), linspace(0, 1, 100)});
[X, Y]  = deal(squeeze(F(1,:,:)), squeeze(F(2,:,:)));
% mesh(X,Y,real(eu))

zana = zeros(size(X));
for i = 1:size(X, 1)
    for j = 1:size(X, 2)
        zana(i,j) = WaveSolAnalytical(X(i,j), Y(i,j),H1R0,H2R0,H1R1,H2R1,J1R0,nMax);
    end
end
%Alternative for plotting fem sol.
%sp_plot_solution(u_rt,space,geometry,sample); axis equal, colorbar, shading interp, colormap jet

figure;
sgtitle('Analytical Solution')
subplot(2,1,1)
surf(X,Y,real(zana));
title('real')
shading interp
axis equal
colormap jet
colorbar
view([0 90])
set(gca,'color','none')
axis off
subplot(2,1,2)
surf(X,Y,imag(zana));
title('imaganry')
shading interp
axis equal
colormap jet
colorbar
view([0 90])
axis off

figure;
sgtitle('Numerical Solution')
subplot(2,1,1)
surf (X, Y, real(eu));
title('real')
shading interp
colormap jet
colorbar 
view([0 90]);
axis equal;
axis off
subplot(2,1,2)
surf(X,Y, imag(eu))
title('imaginary')
shading interp
colormap jet
colorbar 
view([0 90]);
axis equal; 
axis off
toc
end