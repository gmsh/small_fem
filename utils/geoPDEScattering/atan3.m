function theta = atan3(y, x)
% implementation of the matlab atan function in all four quadrants
% author: Ramah Sharaf
if ~isequal(size(x), size(y))
    error("Vectors must be of the same size.")
end
if ~isreal(x) || ~isreal(y)
    error("Both vectors must be real.")
end

for i=1:length(x) 
    if x(i)>=0 && y(i) >=0                 % Quadrant I
        theta(i) = atan(y(i)/x(i));
    elseif x(i)>=0 && y(i) < 0             % Quadrant IV
        theta(i) = atan(y(i)/x(i)) + 2*pi;
    else                                   % Qudarants II and III
        theta(i) = atan(y(i)/x(i)) + pi;
    end
end
