function [fAnalytical] = fAnalyticalLoop(x,y)
% Should be in the same path as the WaveSolAnalytical and accessible by
% scats and mpscats functions.
% author: Ramah Sharaf
R0 = 1;
R1 = 2;
k = 40;
nMax = round(k*R0) + 30;
    H1R0 = zeros(2*nMax+1);
    H2R0 = zeros(2*nMax+1);
    H1R1 = zeros(2*nMax+1);
    H2R1 = zeros(2*nMax+1);
    J1R0 = zeros(2*nMax+2);
    for n = -nMax:nMax+1 %index offset +1: (cpp to matlab indexing convension add 1)
        H1R0(n+nMax+1) = besselh(n,1,k*R0);
        H2R0(n+nMax+1) = besselh(n,2,k*R0);
        H1R1(n+nMax+1) = besselh(n,1,k*R1);
        H2R1(n+nMax+1) = besselh(n,2,k*R1);
        J1R0(n+nMax+1) = besselj(n-1,k*R0);
    end
    J1R0(length(J1R0)) = besselj(nMax+1,k*R0);
fAnalytical = zeros(size(x));
    for N = 1:length(x)
            fAnalytical(N) = WaveSolAnalytical(x(N),y(N),H1R0,H2R0,H1R1,H2R1,J1R0,nMax);
    end
end
