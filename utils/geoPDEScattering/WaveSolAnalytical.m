%% WaveSolAnalytical computes analytical expansion of -exp(-jkx)
% This function is called by fAnalyticalLoop and should be in the same path
% This implementation is an adaption of the original implementation in C++
% by Nicolas Marsic, PhD
% author: Ramah Sharaf

function waveAnalytical = WaveSolAnalytical(x,y,H1R0,H2R0,H1R1,H2R1,J1R0,nMax)
type = 1;
    if (type ~= 0 && type ~= 1)
        error('unkown type')
    end
    R0 = 1;
    R1 = 2;
    k = 40;
    warning('ON', 'all');
    waveAnalytical = 0 + 0j;
    [phi, r] = cart2pol(x,y);
    
   if type == 0
        for n=-nMax:nMax
            waveAnalytical = waveAnalytical + (1j^n) * unHard(n,r,phi,H1R0,H2R0,H1R1,H2R1,J1R0,nMax);
        end
    else
        for n=-nMax:nMax
            waveAnalytical = waveAnalytical + (1j^n) * unSoft(n,r,phi,H1R0,H2R0,H1R1,H2R1,J1R0,nMax);
        end
   end
    
    function Soft = unSoft(n,r,phi,H1R0,H2R0,H1R1,H2R1,J1R0,nMax)
        alpha = 0;
        k = 40;
        beta = -1j*k;
        R0 = 1;
        R1 = 2;
        bo = -alpha * (n*n + 0j) / (R1*R1) - beta;
        A11 = H1R0(n+nMax +1); % +1 offsetting index
        A12 = H2R0(n+nMax +1); % +1 //  //
        A21 = k*derivH(n, k*R1, H1R1, nMax) - bo*H1R1(n+nMax +1); % +1 indexing
        A22 = k*derivH(n, k*R1, H2R1, nMax) - bo*H2R1(n+nMax +1); % +1 

        d = A11*A22 - A21*A12;

        a = -A22*real(+A11) / d;
        b = -A21*real(-A12) / d;
        Soft = (a*besselh(n,1,k*r) + b*besselh(n,2,k*r)) * exp(1j*(n*phi));
    end

    function Hard = unHard(n,r,phi,H1R0,H2R0,H1R1,H2R1,J1R0,nMax)
       k = 40; R1 = 2; R0 = 1;
        alpha = 0;
        beta = -1j*k;
        bo = -alpha * (n*n + 0j) / (R1*R1) - beta;
        A11 = derivH(n,k*R0,H1R0,nMax);
        A12 = derivH(n,k*R0,H2R0,nMax);
        A21 = k*derivH(n, k*R1, H1R1,nMax) - bo*H1R1(n+nMax +1); % +1 indexing
        A22 = k*derivH(n, k*R1, H2R1,nMax) - bo*H2R1(n+nMax +1); % +1 //

        d = A11*A22 - A21*A12;
        a = -A22 * derivJ(n,k*R0,J1R0,nMax) / d;
        b = +A21 * derivJ(n,k*R0,J1R0,nMax) / d;
        Hard = (a*besselh(n,1,k*r) + b*besselh(n,2,k*r)) * exp(1j*(n*phi));
    end
    

    function H = derivH(n,x,Z,nMax)
        H = (n + 0j) * Z(n+0 +nMax +1) / (x + 0j) - Z(n+1 +nMax +1);
    end

    function J = derivJ(n,~,Z,nMax)
        J = (0.5) * (Z(n-1 +nMax+1 +1) - Z(n+1 +nMax+1 +1)); % +1 indexing
    end
end