function output = mpscats(d,n_Lambda)
% NOTE: SOME RESULTS ARE INCONSISTENT, PELASE REMOVE THIS LINE IF YOU HAVE
%       VERIFIED THE RESULTS
% SCATS: mult-patch implementation of soft scattering by a disc
% 
%   output = mpscats(d, n_lambda)
%
% Inputs:
%  d:        order of b-splines
%  n_lambda: divisions per wavelength; size of mesh/space 
%            per number of wavelengths in the solution
% Outputs:
%  output.l2error : l2 error
%  output.int_dofs: number of internal degrees of freedom
%
% author: Ramah Sharaf
%% Parameters
R1          = 1;
R2          = 2;
k           = 40; % wave num
Lambda      = 2*pi/k;
L           = 1;
divs        = 2; % number of patches
sub         = ceil(L/Lambda * n_Lambda);
deg = [d d];
nsub = [ceil(sub/divs) sub];
scatType = 1; % 0-hard, 1-soft (presently no impl. of hard scats.)
% *
begA = 0;
endA = pi/divs;
for pn=1:divs
    crvi(pn) = nrbcirc (R1, [0 0 0],begA,endA);
    crve(pn) = nrbcirc (R2, [0 0 0],begA,endA);
    srf_nrbs(pn) = nrbruled(crvi(pn),crve(pn));
    begA = begA + pi/divs; endA = endA + pi/divs;
end
fprintf('Order: %d\t n_lambda: %d\n', d, n_Lambda);

% figure;
% for pn=1:divs
%     hold on
%     nrbkntplot(srf_nrbs(pn));
%     view([0 90])
% end
% title('srf NURBS'); 

[geo, bnd, intrfcs, ~, bnd_intrfcs] = mp_geo_load (srf_nrbs);
%% Problem Setup
%%%%%%%% parameters %%%%%%%%%%%%%%%
% src_data    = @(x, y) zeros(size(x)); applicable source info.
Sol         = @(x, y) fAnalyticalLoop(x,y); %analytical (exact) sol.
wave        = @(x,y) -exp(1i*k*x); % -------> x-axis (direction of travel)
%%%%%%%% Geometry  %%%%%%%%%%%%%
msh = cell (1, divs);
space  = cell (1, divs);
regularity = deg-1;
for ipct = 1:divs
[rknots, zeta] = kntrefine (geo(ipct).nurbs.knots, nsub-1, deg, regularity);
rule = msh_gauss_nodes(deg + 1);
[qn, qw] = msh_set_quad_nodes(zeta,rule);
msh{ipct} = msh_cartesian(zeta,qn,qw,geo(ipct));
space{ipct} = sp_bspline(rknots, deg,msh{ipct});
end
mpMsh = msh_multipatch(msh, bnd);
mpSpace = sp_multipatch(space,mpMsh,intrfcs,bnd_intrfcs);
fprintf('Space assembled.\n');
%%%%%%%% boundaries  %%%%%%%%%%%%%
% 1. Identify
% bnd_nrb = cell(1,divs);
% figure;
% for pn=1:divs
%     bnd_nrb{pn} = nrbextract(srf_nrbs(pn));
% %     subplot(divs,1,pn)
%     title(['patch number: ' num2str(pn)])
%     for It=1:numel(bnd_nrb{pn}(:))
%         hold on
%         
%         if (pn == 1 && It ~= 2) || (pn == 2 && It ~= 1)
%         nrbplot(bnd_nrb{pn}(It), 20)
%         leg(It) = string(['boundary: ' num2str(It)]);
%         end
%     end
% %     legend(leg);
%     hold off
% end, close

% boundaries 1 and 2 are Neumann, 3 Dirichlet, and 4 ABC (same numring on both patches)
inner  = 3; % disk's inner
outter = 4; % disk's outter
dirchs = find ([bnd.faces] == inner);
abcs   = find ([bnd.faces] == outter);
tic
bnd_src = [];
bnd_inf = [];
for iref = dirchs
  bnd_src = union (bnd_src, mpSpace.boundary.gnum{iref});
end
for iref = abcs
    bnd_inf = union (bnd_inf, mpSpace.boundary.gnum{iref});
end

drchlt_dofs  = mpSpace.boundary.dofs(bnd_src);
infin_dofs   = mpSpace.boundary.dofs(bnd_inf);
int_dofs     = setdiff(1:mpSpace.ndof,drchlt_dofs);
n_int_dofs   = numel (int_dofs);
output.n_int_dofs = n_int_dofs;
fprintf('n_int_dofs: %d\n',output.n_int_dofs);
%% Assemble

uc = zeros(mpSpace.ndof,1); 
c = zeros(mpSpace.ndof,1);
B = spalloc(mpSpace.ndof,mpSpace.ndof,3*mpSpace.boundary.ndof);
b = spalloc(mpSpace.ndof,mpSpace.ndof,3*mpSpace.boundary.ndof);
[~, drchlt_dofs_bnd] = ismember(drchlt_dofs, mpSpace.boundary.dofs);
c_bnd = op_f_v_mp(mpSpace.boundary,mpMsh.boundary,wave,dirchs);%*
c(drchlt_dofs) = c_bnd(drchlt_dofs_bnd);
b = op_u_v_mp(mpSpace.boundary,mpSpace.boundary,...
                             mpMsh.boundary,@(x,y)ones(size(x)));       %*
B(drchlt_dofs,drchlt_dofs) = b(drchlt_dofs_bnd,drchlt_dofs_bnd);
        % * the goal is to assemble these two for inner (tagged 3) boundary only
uc(drchlt_dofs) = B(drchlt_dofs,drchlt_dofs) \ c(drchlt_dofs);
[~, abc_dofs_bnd] = ismember(infin_dofs, mpSpace.boundary.dofs);
inf_bnd = spalloc(mpSpace.ndof,mpSpace.ndof,3*mpSpace.ndof);
inf_bnd(infin_dofs,infin_dofs) = b(abc_dofs_bnd,abc_dofs_bnd);
A = op_gradu_gradv_mp(mpSpace,mpSpace,mpMsh) - (k^2) * op_u_v_mp(mpSpace,mpSpace,mpMsh) - 1j*k*inf_bnd;
rhs = zeros(mpSpace.ndof,1);
rhs(int_dofs) = rhs(int_dofs) - A(int_dofs,drchlt_dofs)*uc(drchlt_dofs);
fprintf('System assembled.\n'); toc
%% Solve
uc(int_dofs) = A(int_dofs,int_dofs) \ rhs(int_dofs); 
fprintf('System solved.\n');
%% Calculate L2 error
tic
output.err_l2 = sp_l2_error (mpSpace, mpMsh,uc,Sol) / sp_l2_error (mpSpace, mpMsh,uc*0,Sol);
fprintf("L2error: %0.16e\n",output.err_l2); 
toc
%% Export-plot
fprintf('Export and plot\n');
tic
%%%%%%%%%% export to PARAVIEW %%%%%%%%%%%%%%%%
% sp_to_vtk (u, mpSpace, geo, [100 100], 'halfDiskPatch', 'u');
%%%%%%%%%% visulaize on Matlab %%%%%%%%%%%%%%%%
sample = {linspace(0, 1, 100), linspace(0, 1, 100)};
figure;
sgtitle('fem solution')
subplot(2,1,1)
sp_plot_solution(real(uc),mpSpace,geo,sample); axis equal
colorbar, shading interp, colormap jet
view([0 90])
title('real')
subplot(2,1,2)
sp_plot_solution(imag(uc),mpSpace,geo,sample); axis equal
colorbar, shading interp, colormap jet
view([0 90])
title('imag')
figure;
for iptc = 1:mpSpace.npatch
  if (isempty (mpSpace.dofs_ornt))
    [eu, F] = sp_eval (uc(mpSpace.gnum{iptc}), mpSpace.sp_patch{iptc}, geo(iptc), sample);
  else
    [eu, F] = sp_eval (uc(mpSpace.gnum{iptc}) .* mpSpace.dofs_ornt{iptc}, mpSpace.sp_patch{iptc}, geo(iptc), sample);
  end
    [X, Y]  = deal(squeeze(F(1,:,:)), squeeze(F(2,:,:)));
    subplot(2,1,1)
    hold on
    surf (X, Y, real(eu));
    title('real')
    shading interp
    colormap jet
    colorbar 
    view([0 90]);
    axis equal
    hold off
    subplot(2,1,2)
    hold on
    surf(X,Y, imag(eu))
    view([0 90]);
    axis equal;
    title('imaginary')
    shading interp
    colormap jet
    colorbar 
    view([0 90]);
    axis equal;
    hold off
    sgtitle('fem solution')
end
end