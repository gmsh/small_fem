#include "SmallFem.h"
#include "Evaluator.h"

namespace sf{
  template<>
  double Evaluator<double>::modSquare(double a){
    return a * a;
  }

  template<>
  double Evaluator<double>::sqrtReal(double a){
    return sqrt(a);
  }
}
