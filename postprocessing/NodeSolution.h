#ifndef _NODESOLUTION_H_
#define _NODESOLUTION_H_

#include <string>
#include <complex>
#include <map>

#include "gmsh/PViewDataGModel.h"
#include "Mesh.h"

/**
   @class NodeSolution
   @brief TODO
 */

namespace sf{
  template<typename scalar> class NodeSolution{
  private:
    PViewDataGModel* pView;

  public:
    NodeSolution(void);
   ~NodeSolution(void);

    void addNodeValue(size_t step,
                      double time,
                      const Mesh& mesh,
                      std::map<const MVertex*, std::vector<scalar> >& data);

    void write(std::string fileName) const;
  };
}

#endif
