#include "SmallFem.h"
#include "Interpolator.h"

using namespace std;

namespace sf{
  template<>
  void Interpolator<double>::write(string filename,
                                   const map<const MVertex*,
                                             vector<double> >& value){
    // Open //
    ofstream stream;
    stream.open(filename.c_str());

    // Header //
    stream << "nodeNum;nodeX;nodeY;nodeZ;data"     << endl
           << value.size()                         << endl
           << 1+3 + 1*value.begin()->second.size() << endl;

    // Iterators //
    map<const MVertex*, vector<double> >::const_iterator  it = value.begin();
    map<const MVertex*, vector<double> >::const_iterator end = value.end();

    // Write //
    map<int, vector<double> > data;

    // Populate
    const int      dim = it->second.size();
    vector<double> tmp(3 + dim * 2);

    for(; it != end; it++){
      tmp[0] = it->first->x();
      tmp[1] = it->first->y();
      tmp[2] = it->first->z();

      for(int i = 0, j = 3; i < dim; i++, j++)
        tmp[j] = it->second[i];

      data.insert(pair<int, vector<double> >(it->first->getNum(), tmp));
    }

    // Dump
    dump(stream, data);

    // Close //
    stream.close();
  }
}
