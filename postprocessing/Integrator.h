#ifndef _INTEGRATOR_H_
#define _INTEGRATOR_H_

#include "gmsh/fullMatrix.h"
#include "GroupOfElement.h"
#include "Quadrature.h"

/**
   @class Integrator
   @brief Integrates scalar fields.

   This class allows the integration of scalar fields.
 */

namespace sf{
  template <typename scalar> class Integrator{
  private:
    const GroupOfElement* goe;
    Quadrature*          quad;

  public:
     Integrator(const GroupOfElement& goe, int order);
    ~Integrator(void);

    void   getQuadraturePoints(std::vector<
                                 std::pair<const MElement*,
                                           fullMatrix<double> > >& Q)  const;
    scalar integrate(const std::vector<
                             std::pair<const MElement*,
                                       fullMatrix<scalar> > >& values) const;
  };
}

/**
   @fn Integrator::Integrator
   @param goe A GroupOfElement
   @param order A positive integer
   Instanciate a new Integrator
   with the given GroupOfElement as integration domain
   and the given order for the quadrature.
   **

   @fn Integrator::~Integrator
   Deletes this Integrator
   **

   @fn Integrator::getQuadraturePoints
   @param Q A vector of pair of MElement and fullMatrix
   Populates the given vector with the coordinate of the quadrature points
   of each MElement defining this Integrator.
   Q[e].first is the eth MElement.
   Q[e].second(i, d) is the dth coordinate of the ith quadrature point
   of the eth element of this Integrator.
   All coordinates are given in the physical space.
   The order of the elements in Q
   is given by the GroupOfElement defining this Integrator.
   **

   @fn Integrator::integrate
   @param values A vector of pair of MElement and real one-column fullMatrix

   @return Returns the integral of the given values

   values[e].first is the eth MElement of defining this Integrator.
   values[e].second(i, 0) is the value of the field eveluated at
   the ith quadrature point of the eth MElement defining this Integrator.
   The elements and the quadrature point are given in the order imposed by
   Integrator::getQuadraturePoints().
 */

#endif
