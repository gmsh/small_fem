#include "SmallFem.h"
#include "Interpolator.h"

using namespace std;

namespace sf{
  template<>
  void Interpolator<Complex>::write(string filename,
                                    const map<const MVertex*,
                                              vector<Complex> >& value){
    // Open //
    ofstream stream;
    stream.open(filename.c_str());

    // Header //
    stream << "nodeNum;nodeX;nodeY;nodeZ;dataReal...dataImag"  << endl
           << value.size()                                     << endl
           << 1+3 + 2*value.begin()->second.size()             << endl;

    // Iterators //
    map<const MVertex*, vector<Complex> >::const_iterator  it = value.begin();
    map<const MVertex*, vector<Complex> >::const_iterator end = value.end();

    // Write //
    map<int, vector<double> > data;

    // Populate
    const int      dim = it->second.size();
    vector<double> tmp(3 + dim * 2);

    for(; it != end; it++){
      tmp[0] = it->first->x();
      tmp[1] = it->first->y();
      tmp[2] = it->first->z();

      for(int i = 0, j = 3;       i < dim; i++, j++)
        tmp[j] = it->second[i].real();

      for(int i = 0, j = 3 + dim; i < dim; i++, j++)
        tmp[j] = it->second[i].imag();

      data.insert(pair<int, vector<double> >(it->first->getNum(), tmp));
    }

    // Dump
    dump(stream, data);

    // Close //
    stream.close();
  }

  template<>
  void Interpolator<Complex>::write(string filename,
                                    const fullMatrix<double>& point,
                                    const fullMatrix<Complex>& values){
    // Open //
    ofstream stream;
    stream.open(filename.c_str());

    // Header //
    stream << "complex"      << endl
           << values.size1() << endl
           << values.size2() << endl;

    // Vector size //
    int M = values.size2();

    // Populate
    int N = values.size1();
    for(int i = 0; i < N; i++){
      stream << point(i, 0) << " "
             << point(i, 1) << " "
             << point(i, 2) << " ";

      for(int j = 0; j < M; j++)
        stream << values(i, j).real() << " "
               << values(i, j).imag() << " ";

      stream << endl;
    }

    // Close //
    stream.close();
  }
}
