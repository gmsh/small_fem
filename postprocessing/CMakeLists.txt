set(src
  FEMSolution.cpp
  FEMSolutionReal.cpp
  FEMSolutionComplex.cpp

  NodeSolution.cpp
  NodeSolutionReal.cpp
  NodeSolutionComplex.cpp

  Interpolator.cpp
  InterpolatorReal.cpp
  InterpolatorComplex.cpp

  Integrator.cpp

  Evaluator.cpp
  EvaluatorReal.cpp
  EvaluatorComplex.cpp

  ElementSolution.cpp
  TextSolution.cpp
)


add_sources(postprocessing "${src}")
