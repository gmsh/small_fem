#include "SmallFem.h"
#include "FEMSolution.h"

using namespace std;

namespace sf{
  template<>
  void FEMSolution<Complex>::
  toPView(GModel& model, map<int, vector<Complex> >& data,
          size_t step, double time, int partition, int nComp){

    // Split Data //
    // New Real / Imag
    map<int, vector<double> > real;
    map<int, vector<double> > imag;

    // Iterate on Data
    const map<int, vector<Complex> >::iterator end = data.end();
    map<int, vector<Complex> >::iterator        it = data.begin();

    for(; it != end; it++){
      // Element
      int eNum = it->first;

      // Number of coefficients
      size_t nCoef = it->second.size();

      // New vectors
      vector<double> vReal(nCoef);
      vector<double> vImag(nCoef);

      for(size_t i = 0; i < nCoef; i++)
        vReal[i] = it->second[i].real();

      for(size_t i = 0; i < nCoef; i++)
        vImag[i] = it->second[i].imag();

      real.insert(pair<int, vector<double> >(eNum, vReal));
      imag.insert(pair<int, vector<double> >(eNum, vImag));
    }

    // Add to PView //
    pView->addData(&model, real, 2 * step + 0, time, partition, nComp);
    pView->addData(&model, imag, 2 * step + 1, time, partition, nComp);
  }
}
