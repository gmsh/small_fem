#ifndef _INTERPOLATOR_H_
#define _INTERPOLATOR_H_

#include <fstream>
#include <map>

#include "gmsh/fullMatrix.h"
#include "GroupOfElement.h"
#include "FunctionSpace.h"

/**
   @class Interpolator
   @brief Interpolating method for FEM Solutions

   This class allows the interpolation of a map of (Dof, value)
   associated to a FunctionSpace on a set of points
 */

namespace sf{
  template <typename scalar> class Interpolator{
  public:
    Interpolator(void);
    ~Interpolator(void);

    static void interpolate(const FunctionSpace& fs,
                            const std::map<Dof, scalar>& coef,
                            const std::vector<
                                    std::pair<const MElement*,
                                              fullMatrix<double> > >& point,
                            std::vector<
                               std::pair<const MElement*,
                                         fullMatrix<scalar> > >& values,
                            bool derivative = false);

    static void interpolate(const GroupOfElement& goe,
                            const FunctionSpace& fs,
                            const std::map<Dof, scalar>& coef,
                            const fullMatrix<double>& point,
                            fullMatrix<scalar>& values,
                            std::vector<bool>& isValid,
                            bool derivative = false);

    static void interpolate(const GroupOfElement& goe,
                            const GroupOfElement& point,
                            const FunctionSpace& fs,
                            const std::map<Dof, scalar>& coef,
                            std::map<const MVertex*,
                                     std::vector<scalar> >& values,
                            bool derivative = false);

    static void write(std::string filename,
                      const std::map<const MVertex*, std::vector<scalar> >&value);
    static void write(std::string filename,
                      const fullMatrix<double>& point,
                      const fullMatrix<scalar>& values);

  private:
    static void interpolate(const MElement& element,
                            const FunctionSpace& fs,
                            const std::vector<scalar>& coef,
                            const fullVector<double>& xyz,
                            fullVector<scalar>& value,
                            bool derivative);

    static bool isIt3D(const FunctionSpace& fs, bool derivative);

    static void dump(std::ofstream& stream,
                     const std::map<int, std::vector<double> >& data);
  };
}

/**
   @fn Interpolator::Interpolator
   Instanciate a new Interpolator
   (this is not required, since Interpolator has only one class method)
   **

   @fn Interpolator::~Interpolator
   Deletes this Interpolator
   (this is not required, since Interpolator has only one class method)
   **

   @fn Interpolator::interpolate(const FunctionSpace&,const std::map<Dof,scalar>&,const std::vector<std::pair<const MElement*,fullMatrix<double> > >&,std::vector<std::pair<const MElement*,std::vector<scalar> > >&)
   @param fs A FunctionSpace
   @param coef A map of (Dof, value) to be interpolated with the FunvtionSpace
   @param point A set of point coordinates (3D) per element:
   one point per row and one coordinate per column;
   this point must lie in its corresponding element
   @param values A vector of value per element

   Interpolate the given parameters on the given set of point.

   The interpolated values are stored in values:
   @li values.first is a MElement
   in which the points in values.second are located
   @li Each row of values.second is point
   @li Each column of values.second is a coordinate of the solution
   (1 for scalar problems and 3 for vectorial ones)
   **

   @fn Interpolator::interpolate(const GroupOfElement&,const FunctionSpace&,const std::map<Dof, scalar>&,const fullMatrix<double>&,fullMatrix<scalar>&)
   @param goe A GroupOfElement on which the solution will be interpolated
   @param fs A FunctionSpace
   @param coef A map of (Dof, value) to be interpolated with the FunvtionSpace
   @param point A set of point coordinates (3D):
   one point per row and one coordinate per column
   @param values A matrix

   Interpolate the given parameters on the given set of point.

   The interpolated values are stored in values:
   @li Each row is point
   @li Each column is a coordinate of the solution
   (1 for scalar problems and 3 for vectorial ones)
 */

//////////////////////
// Inline Functions //
//////////////////////

template<typename scalar>
inline void sf::Interpolator<scalar>::
interpolate(const MElement& element,
            const FunctionSpace& fs,
            const std::vector<scalar>& coef,
            const fullVector<double>& xyz,
            fullVector<scalar>& value,
            bool derivative){

  if(derivative)
    fs.interpolateDerivative(element, coef, xyz, value);
  else
    fs.interpolate(element, coef, xyz, value);
}

#endif
