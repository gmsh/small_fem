#include "SmallFem.h"
#include "FEMSolution.h"

using namespace std;

namespace sf{
  template<>
  void FEMSolution<double>::
  toPView(GModel& model, map<int, vector<double> >& data,
          size_t step, double time, int partition, int nComp){

    pView->addData(&model, data, step, time, partition, nComp);
  }
}
