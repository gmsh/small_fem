#include "SmallFem.h"
#include "Evaluator.h"

namespace sf{
  template<>
  Complex Evaluator<Complex>::modSquare(Complex a){
    return Complex((a.real() * a.real()) + (a.imag() * a.imag()), 0);
  }

  template<>
  double Evaluator<Complex>::sqrtReal(Complex a){
    return sqrt(a).real();
  }
}
