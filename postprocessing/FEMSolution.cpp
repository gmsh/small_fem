#include "SmallFem.h"
#include "FEMSolution.h"
#include "BasisGenerator.h"

using namespace std;
using namespace sf;

template<typename scalar>
FEMSolution<scalar>::FEMSolution(bool derivative){
  init(derivative);
}

template<typename scalar>
FEMSolution<scalar>::FEMSolution(void){
  init(false);
}

template<typename scalar>
void FEMSolution<scalar>::init(bool derivative){
  this->derivative = derivative;
  this->saveMesh   = true;
  this->binary     = false;
  this->partition  = 0;
  this->version    = 2.2;
  this->pView      = new PViewDataGModel(PViewDataGModel::ElementNodeData);
}

template<typename scalar>
FEMSolution<scalar>::~FEMSolution(void){
  pView->destroyData();
  delete pView;
}

template<typename scalar>
void FEMSolution<scalar>::setSaveMesh(bool saveMesh){
  this->saveMesh = saveMesh;
}

template<typename scalar>
void FEMSolution<scalar>::setBinaryFormat(bool binary){
  this->binary = binary;
}

template<typename scalar>
void FEMSolution<scalar>::setParition(int partition){
  this->partition = partition;
}

template<typename scalar>
void FEMSolution<scalar>::clear(void){
  pView->destroyData();
}

template<typename scalar>
void FEMSolution<scalar>::write(string fileName) const{
  pView->setName(fileName);

  stringstream stream;
  stream << fileName;

  if(partition > 0)
    stream << "_part" << partition;

  stream << ".msh";
  pView->writeMSH(stream.str(),
                  version, binary, saveMesh, false, partition, true, false);
}

template<typename scalar>
void FEMSolution<scalar>::addCoefficients(size_t step,
                                          double time,
                                          const GroupOfElement& goe,
                                          const FunctionSpace& fsX,
                                          const map<Dof, scalar>& coefX,
                                          const FunctionSpace& fsY,
                                          const map<Dof, scalar>& coefY,
                                          const FunctionSpace& fsZ,
                                          const map<Dof, scalar>& coefZ){
  // Get GModel //
  GModel& model = goe.getMesh().getModel();

  // Vector ! //
  size_t nComp = 3;

  // Populate data per component //
  map<int, vector<scalar> > dataX;
  map<int, vector<scalar> > dataY;
  map<int, vector<scalar> > dataZ;

  addCoefficients(step, time, goe, fsX, coefX, dataX);
  addCoefficients(step, time, goe, fsY, coefY, dataY);
  addCoefficients(step, time, goe, fsZ, coefZ, dataZ);

  // Alloc vector data //
  map<int, vector<scalar> > data;
  for(typename map<int, vector<scalar> >::iterator it = dataX.begin();
      it != dataX.end();
      it++)
    data.insert(pair<int, vector<scalar> >(it->first,
                                           vector<scalar>(it->second.size()*3))
                );

  // Combine data //
  typename map<int, vector<scalar> >::iterator itX = dataX.begin();
  typename map<int, vector<scalar> >::iterator itY = dataY.begin();
  typename map<int, vector<scalar> >::iterator itZ = dataZ.begin();
  typename map<int, vector<scalar> >::iterator it  =  data.begin();
  typename map<int, vector<scalar> >::iterator end =  data.end();
  for(; it != end; it++, itX++, itY++, itZ++){
    for(size_t i = 0; i < itX->second.size(); i++){
      it->second[i * 3 + 0] = itX->second[i];
      it->second[i * 3 + 1] = itY->second[i];
      it->second[i * 3 + 2] = itZ->second[i];
    }
  }

  // Add to PView //
  toPView(model, data, step, time, 0, nComp);
}

template<typename scalar>
void FEMSolution<scalar>::addCoefficients(size_t step,
                                          double time,
                                          const GroupOfElement& goe,
                                          const FunctionSpace& fs,
                                          const map<Dof, scalar>& coef){
  // Get GModel //
  GModel& model = goe.getMesh().getModel();

  // Vector or scalar ? //
  size_t nComp = nComponent(fs);

  // Populate data //
  map<int, vector<scalar> > data;
  addCoefficients(step, time, goe, fs, coef, data);

  // Add to PView //
  toPView(model, data, step, time, 0, nComp);
}

template<typename scalar>
void FEMSolution<scalar>::addCoefficients(size_t step,
                                          double time,
                                          const GroupOfElement& goe,
                                          const FunctionSpace& fs,
                                          const map<Dof, scalar>& coef,
                                          map<int, vector<scalar> >& data){
  // Get Support and GModel //
  const vector<const MElement*>& element = goe.getAll();
  const size_t                  nElement = element.size();

  // Check mesh format and version //
  if(goe.getMesh().getFormat() == Mesh::Format::MSH)
    version = goe.getMesh().getVersion();

  // Which form ?
  const int form  = fs.getForm();

  // Lagrange Basis & Interpolation matrices //
  // One lagrange basis per geo type //
  const vector<size_t> typeStat = goe.getTypeStats();
  const size_t         nGeoType = typeStat.size();

  vector<BasisLagrange*> lagrange(nGeoType, NULL);

  for(size_t i = 0; i < nGeoType; i++){
    if(typeStat[i]){
      size_t order = fs.getBasis(i).getOrder();

      if((form == 1) && (i == TYPE_QUA || i == TYPE_HEX))
        order++; // Increase order by one for 1-forms on non-simplex elements

      if((form == 2) || i == TYPE_PYR || i == TYPE_PRI)
        throw Exception("TODO");

      lagrange[i] = static_cast<BasisLagrange*>
        (BasisGenerator::generate(i, 0, order, Basis::Family::Lagrange));

      pView->setInterpolationMatrices(i,
                                      lagrange[i]->getCoefficient(),
                                      lagrange[i]->getMonomial());
    }
  }

  // Iterate on Element //
  vector<Dof>                               dof;
  typename map<Dof, scalar>::const_iterator end = coef.end();
  typename map<Dof, scalar>::const_iterator it;

  for(size_t i = 0; i < nElement; i++){
    // Get Element Dofs
    fs.getKeys(*element[i], dof);
    const size_t size = dof.size();

    // Get Coef In FS Basis
    vector<scalar> fsCoef(size);
    for(size_t j = 0; j < size; j++){
      // Get Value of Dof 'j'
      it = coef.find(dof[j]);

      // If found in map
      if(it != end)
        fsCoef[j] = it->second;

      // Else
      else{
        cout << "WARNING: FEMSolution: coef set to 0" << endl;
        fsCoef[j] = 0;
      }
    }

    // Get Coef In Lagrange Basis
    vector<scalar> lCoef;
    toLagrange(*element[i], lagrange, fsCoef, fs, derivative, lCoef);

    // Add in data map
    data.insert(pair<int, vector<scalar> >(element[i]->getNum(), lCoef));
  }

  // Clean //
  for(size_t i = 0; i < nGeoType; i++)
    if(typeStat[i])
      delete lagrange[i];
}

template<typename scalar>
void FEMSolution<scalar>::toLagrange(const MElement& element,
                                     const vector<BasisLagrange*>& lagrange,
                                     const vector<scalar>& fsCoef,
                                     const FunctionSpace& fs,
                                     bool derivative,
                                     vector<scalar>& lCoef){
  // Element Type //
  const int eType = element.getType();

  // Projection //
  lCoef = lagrange[eType]->project(element, fsCoef, fs, derivative);
}

template<typename scalar>
size_t FEMSolution<scalar>::nComponent(const FunctionSpace& fs) const{
  int form  = fs.getForm();

  bool is3D =
    ((form == 0) &&  derivative) ||
    ((form == 1)               ) ||
    ((form == 2) && !derivative);

  if(is3D) return 3;
  else     return 1;
}

////////////////////////////
// Explicit instantiation //
////////////////////////////
template class sf::FEMSolution<double>;
template class sf::FEMSolution<Complex>;
