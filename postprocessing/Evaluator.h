#ifndef _EVALUATOR_H_
#define _EVALUATOR_H_

#include <vector>
#include "gmsh/fullMatrix.h"
#include "gmsh/MElement.h"
#include "Integrator.h"

/**
   @class Evaluator
   @brief Helper functions for evaluating things

   This class offers a set of static functions for helping in evaluating things.
 */

namespace sf{
  template<typename scalar> class Evaluator{
  public:
     Evaluator(void);
    ~Evaluator(void);

    static void evaluate(scalar (*f)(fullVector<double>& xyz),
                         const fullMatrix<double>& point,
                         fullMatrix<scalar>& eval);

    static void evaluate(fullVector<scalar> (*f)(fullVector<double>& xyz),
                         const fullMatrix<double>& point,
                         fullMatrix<scalar>& eval);

    static void evaluate(scalar (*f)(fullVector<double>& xyz),
                         const std::vector<std::pair<const MElement*,
                                                     fullMatrix<double> > >&pnt,
                         std::vector<std::pair<const MElement*,
                                               fullMatrix<scalar> > >& eval);

    static void evaluate(fullVector<scalar> (*f)(fullVector<double>& xyz),
                         const std::vector<std::pair<const MElement*,
                                                     fullMatrix<double> > >&pnt,
                         std::vector<std::pair<const MElement*,
                                               fullMatrix<scalar> > >& eval);

    static void evaluate(fullVector<scalar> (*f)(const MElement& element,
                                                 fullVector<double>& xyz),
                         const std::vector<std::pair<const MElement*,
                                                     fullMatrix<double> > >&pnt,
                         std::vector<std::pair<const MElement*,
                                               fullMatrix<scalar> > >& eval);

    static void modDiffSquare(const std::vector<std::pair<const MElement*,
                                                          fullMatrix<scalar> >
                                               >& A,
                              const std::vector<std::pair<const MElement*,
                                                          fullMatrix<scalar> >
                                               >& B,
                              std::vector<std::pair<const MElement*,
                                                    fullMatrix<scalar> >
                                               >& val);

    static void modSquare(const std::vector<std::pair<const MElement*,
                                            fullMatrix<scalar> > >& A,
                          std::vector<std::pair<const MElement*,
                                                fullMatrix<scalar> > >& val);

    static scalar     modSquare(scalar a);
    static scalar     modSquare(const fullMatrix<scalar>& a, int i);
    static scalar modDiffSquare(const fullMatrix<scalar>& a,
                                const fullMatrix<scalar>& b,
                                int i);

    static double sqrtReal(scalar a);

    static double norm(const fullMatrix<scalar>& a);
    static double norm(const fullMatrix<scalar>& a,
                       const fullMatrix<scalar>& b);

    static void dot(const std::vector<std::pair<const MElement*,
                                                fullMatrix<scalar> > >& a,
                    const std::vector<std::pair<const MElement*,
                                                fullMatrix<scalar> > >& b,
                    std::vector<std::pair<const MElement*,
                                          fullMatrix<scalar> > >& c);

    static scalar l2Error(const std::vector<std::pair<const MElement*,
                                                      fullMatrix<scalar> >
                                           >& A,
                          const std::vector<std::pair<const MElement*,
                                                      fullMatrix<scalar> >
                                           >& B,
                          const Integrator<scalar>& integrator);
  };
}

/**
   @fn Evaluator::Evaluator
   Instanciate a new Evaluator (not required, since all methods are static)
   **

   @fn Evaluator::~Evaluator
   Deletes this Evaluator
   **

*/

#endif
