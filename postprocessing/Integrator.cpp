#include "SmallFem.h"
#include "Integrator.h"
#include "Exception.h"

using namespace std;
using namespace sf;

template<typename scalar>
Integrator<scalar>::Integrator(const GroupOfElement& goe, int order){
  // Save Goe
  this->goe = &goe;

  // Check if GOE is uniform
  pair<bool, size_t> uniform = goe.isUniform();
  size_t               eType = uniform.second;

  if(!uniform.first)
    throw Exception("Integrator needs a unifom GroupOfElement");

  // Init quadrature
  quad = new Quadrature(eType, order, 1);
}

template<typename scalar>
Integrator<scalar>::~Integrator(void){
  delete quad;
}

template<typename scalar>
void Integrator<scalar>::
getQuadraturePoints(vector<pair<const MElement*,
                                fullMatrix<double> > >& Q) const{
  // Get all elements
  const vector<const MElement*>& element = goe->getAll();

  // Get quadrature points in the reference space
  const fullMatrix<double>& abc = quad->getPoints();

  // Get the number of quadrature points
  int nE   = element.size(); // Number of elements in GOE
  int nQpE = abc.size1();    // Number of quadrature points per element

  // Allocate Q
  Q.resize(nE);
  for(int e = 0; e < nE; e++)
    Q[e].second.resize(nQpE, 3);

  // Iterate on elements
  double xyz[3];
  for(int e = 0; e < nE; e++){
    for(int i = 0; i < nQpE; i++){
      ReferenceSpaceManager::mapFromABCtoXYZ(*element[e],
                                             abc(i, 0), abc(i, 1), abc(i, 2),
                                             xyz);
      Q[e].first        = element[e];
      Q[e].second(i, 0) = xyz[0];
      Q[e].second(i, 1) = xyz[1];
      Q[e].second(i, 2) = xyz[2];
    }
  }
}

template<typename scalar>
scalar Integrator<scalar>::
integrate(const vector<pair<const MElement*,
                            fullMatrix<scalar> > >& values) const{
  // Get number of elements and of quadrature point per element
  int nE   = values.size();
  int nQpE = quad->getPoints().size1();

  // Get weights
  const fullVector<double>& w = quad->getWeights();

  // Get integration point
  const fullMatrix<double>& abc = quad->getPoints();

  // Integrate
  fullMatrix<double> jac(3, 3);
  scalar sum = 0;
  for(int e = 0; e < nE; e++){
    if(int(values[e].second.size1()) != nQpE)
      throw Exception("Integrator::integrate needs a unifom GroupOfElement");

    if(values[e].second.size2() != 1)
      throw Exception("Integrator::integrate can only handel scalar fields");

    for(int i = 0; i < nQpE; i++)
      sum +=
        values[e].second(i, 0) * w(i) *
        abs(ReferenceSpaceManager::getJacobian(*values[e].first,
                                               abc(i, 0), abc(i, 1), abc(i, 2),
                                               jac));
  }

  return sum;
}

////////////////////////////
// Explicit instantiation //
////////////////////////////
template class sf::Integrator<double>;
template class sf::Integrator<Complex>;
