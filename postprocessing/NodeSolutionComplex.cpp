#include "SmallFem.h"
#include "NodeSolution.h"

using namespace std;

namespace sf{
  template<>
  void NodeSolution<Complex>::addNodeValue(size_t step,
                                           double time,
                                           const Mesh& mesh,
                                           map<const MVertex*,
                                               vector<Complex> >& data){
    // GModel //
    GModel& model = mesh.getModel();

    // Map with (Vertex Id, Node Value) //
    map<int, vector<double> > gmshDataReal;
    map<int, vector<double> > gmshDataImag;

    // Scalar or vector field ? //
    const size_t nComp = data.begin()->second.size();

    // Populate gmshData //
    map<const MVertex*, vector<Complex> >::iterator it  = data.begin();
    map<const MVertex*, vector<Complex> >::iterator end = data.end();

    vector<double> tmpReal(nComp);
    vector<double> tmpImag(nComp);

    for(; it != end; it++){
      for(size_t i = 0; i < nComp; i++)
        tmpReal[i] = it->second.at(i).real();

      for(size_t i = 0; i < nComp; i++)
        tmpImag[i] =it->second.at(i).imag();

      gmshDataReal.insert
        (pair<int, vector<double> >(it->first->getNum(), tmpReal));
      gmshDataImag.insert
        (pair<int, vector<double> >(it->first->getNum(), tmpImag));
    }

    // Add map to PView //
    pView->addData(&model, gmshDataReal, 2 * step + 0, time, 0, nComp);
    pView->addData(&model, gmshDataImag, 2 * step + 1, time, 0, nComp);
  }
}
