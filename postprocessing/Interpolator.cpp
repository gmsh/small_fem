#include <iomanip>
#include "SmallFem.h"
#include "Interpolator.h"

using namespace std;
using namespace sf;

template<typename scalar>
Interpolator<scalar>::Interpolator(void){
}

template<typename scalar>
Interpolator<scalar>::~Interpolator(void){
}

template<typename scalar>
void Interpolator<scalar>::interpolate(const FunctionSpace& fs,
                                       const map<Dof, scalar>& coef,
                                       const vector<pair<const MElement*,
                                                    fullMatrix<double> >
                                                   >& point,
                                       vector<pair<const MElement*,
                                                   fullMatrix<scalar> >
                                                  >& values,
                                       bool derivative){
  // 3D field ? //
  const bool is3D = isIt3D(fs, derivative);
  int nDim;
  if(is3D) nDim = 3;
  else     nDim = 1;

  // Alloc values //
  const size_t nElement = point.size();
  values.resize(nElement);

  for(size_t e = 0; e < nElement; e++)
    values[e].second.resize(point[e].second.size1(), nDim);

  // Copy MElement in point //
  for(size_t e = 0; e < nElement; e++)
    values[e].first = point[e].first;

  // Coef iterators //
  typename map<Dof, scalar>::const_iterator end = coef.end();
  typename map<Dof, scalar>::const_iterator it;

  // Temp //
  #pragma omp parallel
  {
    fullVector<scalar> tmp(nDim);
    fullVector<double> xyz(3);
    vector<Dof>           dof;
    vector<scalar>     fsCoef;

    // Iterate on elements //
    #pragma omp for
    for(size_t e = 0; e < nElement; e++){
      // Get Dofs related to this MElement
      fs.getKeys(*point[e].first, dof);
      const size_t size = dof.size();

      // Get Coef In FS Basis
      fsCoef.resize(size);
      for(size_t j = 0; j < size; j++){
        // Get Value of Dof 'j'
        it = coef.find(dof[j]);

        // If found in map
        if(it != end)
          fsCoef[j] = it->second;

        // Else
        else
          throw Exception("Interpolator: "
                          "missing Dof %s for element %d",
                          dof[j].toString().c_str(), point[e].first->getNum());
      }

      // Iterate on points
      const size_t nPoint = point[e].second.size1();
      for(size_t i = 0; i < nPoint; i++){
        xyz(0) = point[e].second(i, 0);
        xyz(1) = point[e].second(i, 1);
        xyz(2) = point[e].second(i, 2);

        // Interpolate (AT LAST !!)
        interpolate(*point[e].first, fs, fsCoef, xyz, tmp, derivative);

        values[e].second(i, 0) = tmp(0);
        if(is3D){
          values[e].second(i, 1) = tmp(1);
          values[e].second(i, 2) = tmp(2);
        }
      }
    }
  }
}

template<typename scalar>
void Interpolator<scalar>::interpolate(const GroupOfElement& goe,
                                       const FunctionSpace& fs,
                                       const map<Dof, scalar>& coef,
                                       const fullMatrix<double>& point,
                                       fullMatrix<scalar>& values,
                                       vector<bool>& isValid,
                                       bool derivative){
  // Get GModel //
  GModel&    model = goe.getMesh().getModel();
  const size_t dim = model.getDim();

  // 3D field ? //
  const bool is3D = isIt3D(fs, derivative);
  int nDim;
  if(is3D) nDim = 3;
  else     nDim = 1;

  // Alloc values //
  const size_t nPoint = point.size1();
  values.resize(nPoint, nDim);

  // Alloc isValid //
  isValid.resize(nPoint);

  // Temp //
  fullVector<scalar> tmp(nDim);
  fullVector<double> xyz(3);
  vector<scalar>     fsCoef;

  // Coef iterators //
  vector<Dof>                               dof;
  typename map<Dof, scalar>::const_iterator end = coef.end();
  typename map<Dof, scalar>::const_iterator it;

  // Iterate on 'point' //
  for(size_t i = 0; i < nPoint; i++){
    // Search all the elements containg this point
    SPoint3   thisPoint(point(i, 0), point(i, 1), point(i, 2));
    vector<MElement*> element =
      model.getMeshElementsByCoord(thisPoint, dim, true);

    // If no element found, point is invalid
    if(element.empty())
      isValid[i] = false;

    else{
      // Search an element of this GroupOfElement
      size_t nElement = element.size();
      size_t idx = 0;

      for(size_t e = 0; e < nElement; e++)
        if(goe.isMember(*element[idx]))
          break;
        else
          idx++;

      // Is point valid ?
      isValid[i] = !(idx == nElement);

      // If it is valid, proceed to interpolation
      if(isValid[i]){
        // Get Dofs related to this Element
        fs.getKeys(*element[idx], dof);
        const size_t size = dof.size();

        // Get Coef In FS Basis
        fsCoef.resize(size);
        for(size_t j = 0; j < size; j++){
          // Get Value of Dof 'j'
          it = coef.find(dof[j]);

          // If found in map
          if(it != end)
            fsCoef[j] = it->second;

          // Else
          else
            throw Exception("Interpolator: "
                            "missing Dof %s on element %d of GroupOfElement %d",
                            dof[j].toString().c_str(), i, goe.getId());
        }

        // Get Node coordinate
        xyz(0) = point(i, 0);
        xyz(1) = point(i, 1);
        xyz(2) = point(i, 2);

        // Interpolate (AT LAST !!)
        interpolate(*element[idx], fs, fsCoef, xyz, tmp, derivative);

        values(i, 0) = tmp(0);
        if(is3D){
          values(i, 1) = tmp(1);
          values(i, 2) = tmp(2);
        }
      }
    }
  }
}

template<typename scalar>
void Interpolator<scalar>::interpolate(const GroupOfElement& goe,
                                       const GroupOfElement& point,
                                       const FunctionSpace& fs,
                                       const map<Dof, scalar>& coef,
                                       map<const MVertex*,
                                           vector<scalar> >& data,
                                       bool derivative){
  // Get the Vertices of 'point' //
  MapVertex vertex;
  point.getAllVertex(vertex);

  // Get those Vertices coordinates //
  const size_t nVertex = vertex.size();
  fullMatrix<double> coordinate(nVertex, 3);

  MapVertex::iterator it;
  MapVertex::iterator end = vertex.end();

  it = vertex.begin();
  for(size_t i = 0; it != end; it++, i++){
    coordinate(i, 0) = it->first->x();
    coordinate(i, 1) = it->first->y();
    coordinate(i, 2) = it->first->z();
  }

  // Interpolate //
  fullMatrix<scalar>  value;
  vector<bool> isValid;
  interpolate(goe, fs, coef, coordinate, value, isValid, derivative);

  // Get Data Map //
  const size_t nDim = value.size2();
  vector<scalar> tmp(nDim);

  it = vertex.begin();
  for(size_t i = 0; it != end; it++, i++){
    // Insert Vertex only if valid
    if(isValid[i]){
      // Pair
      pair<const MVertex*, vector<scalar> > pair;

      // Vertex
      pair.first = it->first;

      // Value
      pair.second.resize(nDim);
      for(size_t j = 0; j < nDim; j++)
        pair.second.at(j) = value(i, j);

      // Add to data
      data.insert(pair);
    }
  }
}

template<typename scalar>
bool Interpolator<scalar>::isIt3D(const FunctionSpace& fs, bool derivative){
  const int form = fs.getForm();
  return
    ((form == 0) &&  derivative) ||
    ((form == 1)               ) ||
    ((form == 2) && !derivative);
}

template<typename scalar>
void Interpolator<scalar>::dump(ofstream& stream,
                                const map<int,vector<double> >& data){
  // Iterator //
  map<int, vector<double> >::const_iterator  it = data.begin();
  map<int, vector<double> >::const_iterator end = data.end();

  // Dump //
  for(; it != end; it++){
    stream << it->first << ";";

    for(size_t i = 0; i < it->second.size()-1; i++)
      stream << std::scientific << std::setprecision(16)
             << it->second[i] << ";";

    stream << std::scientific << std::setprecision(16)
           << it->second[it->second.size()-1] << endl;
  }
}

////////////////////////////
// Explicit instantiation //
////////////////////////////
template class sf::Interpolator<double>;
template class sf::Interpolator<Complex>;
