#include "SmallFem.h"
#include "NodeSolution.h"

using namespace std;
using namespace sf;

template<typename scalar>
NodeSolution<scalar>::NodeSolution(void){
  pView = new PViewDataGModel(PViewDataGModel::NodeData);
}

template<typename scalar>
NodeSolution<scalar>::~NodeSolution(void){
  pView->destroyData();
  delete pView;
}

template<typename scalar>
void NodeSolution<scalar>::write(string fileName) const{
  pView->setName(fileName);
  pView->writeMSH(fileName + ".msh");
}

////////////////////////////
// Explicit instantiation //
////////////////////////////
template class sf::NodeSolution<double>;
template class sf::NodeSolution<Complex>;
