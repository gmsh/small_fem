#include "NodeSolution.h"

using namespace std;

namespace sf{
  template<>
  void NodeSolution<double>::addNodeValue(size_t step,
                                          double time,
                                          const Mesh& mesh,
                                          map<const MVertex*,
                                              vector<double> >& data){
    // GModel //
    GModel& model = mesh.getModel();

    // Map with (Vertex Id, Node Value) //
    map<int, vector<double> > gmshData;

    // Scalar or vector field ? //
    const size_t nComp = data.begin()->second.size();

    // Populate gmshData //
    map<const MVertex*, vector<double> >::iterator it  = data.begin();
    map<const MVertex*, vector<double> >::iterator end = data.end();

    vector<double> tmp(nComp);

    for(; it != end; it++){
      for(size_t i = 0; i < nComp; i++)
        tmp[i] = it->second.at(i);

      gmshData.insert(pair<int, vector<double> >(it->first->getNum(), tmp));
    }

    // Add map to PView //
    pView->addData(&model, gmshData, step, time, 0, nComp);
  }
}
