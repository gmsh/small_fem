#include "Exception.h"
#include "SmallFem.h"
#include "Evaluator.h"

using namespace sf;
using namespace std;

template<typename scalar>
Evaluator<scalar>::Evaluator(void){
}

template<typename scalar>
Evaluator<scalar>::~Evaluator(void){
}

template<typename scalar>
void Evaluator<scalar>::evaluate(scalar (*f)(fullVector<double>& xyz),
                                 const fullMatrix<double>& point,
                                 fullMatrix<scalar>& eval){
  // Alloc eval for Scalar Values //
  const size_t nPoint = point.size1();
  eval.resize(nPoint, 1);

  // Loop on point and evaluate f //
  fullVector<double> xyz(3);
  for(size_t i = 0; i < nPoint; i++){
    xyz(0) = point(i, 0);
    xyz(1) = point(i, 1);
    xyz(2) = point(i, 2);

    eval(i, 0) = f(xyz);
  }
}

template<typename scalar>
void Evaluator<scalar>::evaluate(fullVector<scalar> (*f)(fullVector<double>& xyz),
                                 const fullMatrix<double>& point,
                                 fullMatrix<scalar>& eval){
  // Alloc eval for Vectorial Values //
  const size_t nPoint = point.size1();
  eval.resize(nPoint, 3);

  // Loop on point and evaluate f //
  fullVector<double> xyz(3);
  fullVector<scalar> tmp(3);
  for(size_t i = 0; i < nPoint; i++){
    xyz(0) = point(i, 0);
    xyz(1) = point(i, 1);
    xyz(2) = point(i, 2);

    tmp = f(xyz);

    eval(i, 0) = tmp(0);
    eval(i, 1) = tmp(1);
    eval(i, 2) = tmp(2);
  }
}

template<typename scalar>
void Evaluator<scalar>::
evaluate(scalar (*f)(fullVector<double>& xyz),
         const vector<pair<const MElement*, fullMatrix<double> > >& point,
         vector<pair<const MElement*, fullMatrix<scalar> > >& eval){
  // Alloc eval //
  const size_t nElement = point.size();
  eval.clear();
  eval.resize(nElement);

  // Loop on elements //
  #pragma omp parallel for
  for(size_t e = 0; e < nElement; e++){
    // Copy element
    eval[e].first = point[e].first;

    // Number of points
    const size_t nPoint = point[e].second.size1();

    // Allocate
    eval[e].second.resize(nPoint, 1);

    // Loop on points and evaluate
    for(size_t i = 0; i < nPoint; i++){
      fullVector<double> xyz(3);

      xyz(0) = point[e].second(i, 0);
      xyz(1) = point[e].second(i, 1);
      xyz(2) = point[e].second(i, 2);

      eval[e].second(i, 0) = f(xyz);
    }
  }
}

template<typename scalar>
void Evaluator<scalar>::
evaluate(fullVector<scalar> (*f)(fullVector<double>& xyz),
         const vector<pair<const MElement*, fullMatrix<double> > >& point,
         vector<pair<const MElement*, fullMatrix<scalar> > >& eval){
  // Alloc eval //
  const size_t nElement = point.size();
  eval.clear();
  eval.resize(nElement);

  // Loop on elements //
  #pragma omp parallel for
  for(size_t e = 0; e < nElement; e++){
    // Copy element
    eval[e].first = point[e].first;

    // Number of points
    const size_t nPoint = point[e].second.size1();

    // Allocate
    eval[e].second.resize(nPoint, 3);

    // Loop on points and evaluate
    for(size_t i = 0; i < nPoint; i++){
      fullVector<double> xyz(3);
      fullVector<scalar> tmp(3);

      xyz(0) = point[e].second(i, 0);
      xyz(1) = point[e].second(i, 1);
      xyz(2) = point[e].second(i, 2);

      tmp = f(xyz);

      eval[e].second(i, 0) = tmp(0);
      eval[e].second(i, 1) = tmp(1);
      eval[e].second(i, 2) = tmp(2);
    }
  }
}

template<typename scalar>
void Evaluator<scalar>::
evaluate(fullVector<scalar> (*f)(const MElement& element,
                                 fullVector<double>& xyz),
         const vector<pair<const MElement*, fullMatrix<double> > >& point,
         vector<pair<const MElement*, fullMatrix<scalar> > >& eval){
  // Alloc eval //
  const size_t nElement = point.size();
  eval.clear();
  eval.resize(nElement);

  // Loop on elements //
  #pragma omp parallel for
  for(size_t e = 0; e < nElement; e++){
    // Copy element
    eval[e].first = point[e].first;

    // Number of points
    const size_t nPoint = point[e].second.size1();

    // Allocate
    eval[e].second.resize(nPoint, 3);

    // Loop on points and evaluate
    for(size_t i = 0; i < nPoint; i++){
      fullVector<double> xyz(3);
      fullVector<scalar> tmp(3);

      xyz(0) = point[e].second(i, 0);
      xyz(1) = point[e].second(i, 1);
      xyz(2) = point[e].second(i, 2);

      tmp = f(*point[e].first, xyz);

      eval[e].second(i, 0) = tmp(0);
      eval[e].second(i, 1) = tmp(1);
      eval[e].second(i, 2) = tmp(2);
    }
  }
}

template<typename scalar>
void Evaluator<scalar>::
modDiffSquare(const vector<pair<const MElement*, fullMatrix<scalar> > >& A,
              const vector<pair<const MElement*, fullMatrix<scalar> > >& B,
              vector<pair<const MElement*, fullMatrix<scalar> > >& val){

  // Allocate val //
  const size_t nElement = A.size();
  val.clear();
  val.resize(nElement);

  // Check size //
  if(B.size() != A.size())
    throw Exception("Evaluator::modDiffSquare(): "
                    "sizes of A and B do not match");

  // Iterate on elements //
  for(size_t e = 0; e < nElement; e++){
    // Number of points
    const size_t nPoint = A[e].second.size1();

    // Check with B
    if(size_t(B[e].second.size1()) != nPoint)
      throw Exception("Evaluator::modDiffSquare(): "
                      "sizes of A and B do not match");

    // Allocate
    val[e].second.resize(nPoint, 1);

    // Copy Element
    val[e].first = A[e].first;

    // Check Element
    if(A[e].first->getNum() != B[e].first->getNum())
    throw Exception("Evaluator::modDiffSquare(): "
                    "elements of A and B do not match");

    // Loop on points and compute mod
    for(size_t i = 0; i < nPoint; i++)
      val[e].second(i, 0) = modDiffSquare(A[e].second, B[e].second, i);
  }
}

template<typename scalar>
void Evaluator<scalar>::
modSquare(const vector<pair<const MElement*, fullMatrix<scalar> > >& A,
          vector<pair<const MElement*, fullMatrix<scalar> > >& val){

  // Allocate val //
  const size_t nElement = A.size();
  val.clear();
  val.resize(nElement);

  // Iterate on elements //
  for(size_t e = 0; e < nElement; e++){
    // Number of points
    const size_t nPoint = A[e].second.size1();

    // Allocate
    val[e].second.resize(nPoint, 1);

    // Copy Element
    val[e].first = A[e].first;

    // Loop on points and compute mod
    for(size_t i = 0; i < nPoint; i++)
      val[e].second(i, 0) = modSquare(A[e].second, i);
  }
}

template<typename scalar>
scalar Evaluator<scalar>::modSquare(const fullMatrix<scalar>& a, int i){
  int    dim = a.size2();
  scalar  ms = 0;
  for(int j = 0; j < dim; j++)
    ms += modSquare(a(i, j));

  return ms;
}

template<typename scalar>
scalar Evaluator<scalar>::modDiffSquare(const fullMatrix<scalar>& a,
                                        const fullMatrix<scalar>& b,
                                        int i){
  int    dim = a.size2();
  scalar  ms = 0;
  for(int j = 0; j < dim; j++)
    ms += modSquare(a(i, j) - b(i, j));

  return ms;
}

template<typename scalar>
double Evaluator<scalar>::norm(const fullMatrix<scalar>& a){
  const size_t nPoint = a.size1();
  const size_t    dim = a.size2();

  scalar   norm = 0;
  scalar modSqu = 0;

  for(size_t i = 0; i < nPoint; i++){
    modSqu = 0;

    for(size_t j = 0; j < dim; j++)
      modSqu += modSquare(a(i, j));

    norm += modSqu;
  }

  return sqrtReal(norm);
}

template<typename scalar>
double Evaluator<scalar>::norm(const fullMatrix<scalar>& a,
                               const fullMatrix<scalar>& b){
  const size_t nPoint = a.size1();
  const size_t    dim = a.size2();

  scalar   norm = 0;
  scalar modSqu = 0;

  for(size_t i = 0; i < nPoint; i++){
    modSqu = 0;

    for(size_t j = 0; j < dim; j++)
      modSqu += modSquare(a(i, j) - b(i, j));

    norm += modSqu;
  }

  return sqrtReal(norm);
}

template<typename scalar>
void Evaluator<scalar>::
dot(const vector<pair<const MElement*, fullMatrix<scalar> > >& a,
    const vector<pair<const MElement*, fullMatrix<scalar> > >& b,
    vector<pair<const MElement*, fullMatrix<scalar> > >& c){
  // Check size of vector //
  size_t nE = a.size();
  if(nE != b.size())
    throw Exception("Evaluator::dot: a and b must have the same size");

  // Alloc //
  c.clear();
  c.resize(nE);

  // Empty ? //
  if(a.empty())
    return;

  // Check size of matrices //
  if(a[0].second.size2() != 3 || b[0].second.size2() != 3)
    throw Exception("Evaluator::dot: {a,b}.second.size2() must be equal to 3");

  // Polulate //
  #pragma omp parallel for
  for(size_t e = 0; e < nE; e++){
    // Copy element
    c[e].first = a[e].first;

    // Number of points
    const size_t nPoint = a[e].second.size1();

    // Allocate
    c[e].second.resize(nPoint, 1);

    // Loop on points and evaluate
    for(size_t i = 0; i < nPoint; i++)
      c[e].second(i, 0) = a[e].second(i, 0) * b[e].second(i, 0) +
                          a[e].second(i, 1) * b[e].second(i, 1) +
                          a[e].second(i, 2) * b[e].second(i, 2);
  }
}

template<typename scalar>
scalar Evaluator<scalar>::
l2Error(const vector<pair<const MElement*, fullMatrix<scalar> > >& A,
        const vector<pair<const MElement*, fullMatrix<scalar> > >& B,
        const Integrator<scalar>& integrator){

  // ||A - B||^2
  vector<pair<const MElement*, fullMatrix<scalar> > > modDiffValue;
  modDiffSquare(A, B, modDiffValue);

  // ||A||^2
  vector<pair<const MElement*, fullMatrix<scalar> > > modValue;
  modSquare(A, modValue);

  // L2 Error
  scalar diff = integrator.integrate(modDiffValue);
  scalar ref  = integrator.integrate(modValue);

  // Done
  return sqrt(diff/ref);
}

////////////////////////////
// Explicit instantiation //
////////////////////////////
template class sf::Evaluator<double>;
template class sf::Evaluator<Complex>;
