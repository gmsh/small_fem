#!/bin/bash

FILE='guide2d'

for k in 30 40 50 60 70 80 90
do
    for n in  5 10 20 40
    do
        gmsh $FILE'.geo' -setnumber K $k -setnumber N_LAMBDA $n -3
        mv $FILE'.msh' $FILE'_'$k'_'$n'.msh'
    done
done
