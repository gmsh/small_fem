// User Data
DefineConstant[R  = { 1e-3, Name "Input/00Geometry/00Radius [m]" },
               md = { 10,   Name "Input/01Mesh/00Density [Radius^-1]" }];
cl = R/md;

// Geo
Point(0) = { 0,  0, 0, cl};

Point(1) = {+R,  0, 0, cl};
Point(2) = { 0, +R, 0, cl};
Point(3) = {-R,  0, 0, cl};
Point(4) = { 0, -R, 0, cl};

Circle(1) = {1, 0, 2};
Circle(2) = {2, 0, 3};
Circle(3) = {3, 0, 4};
Circle(4) = {4, 0, 1};

Line     Loop(1) = {1, 2, 3, 4};
Plane Surface(1) = {1};

// Physicals
Physical Surface(7) = {1};
Physical    Line(5) = {1, 2, 3, 4};

// Mesh
Mesh.Algorithm = 6; // Frontal
