#!/bin/bash

O=2

#N=24
#for D in 2 4 8 16 32
#do
#    gmsh boubouchons.geo -setnumber RodN $N -setnumber DoMesh 1 \
#         -setnumber MeshOrder $O -setnumber DomN $D -
#    mv boubouchons.msh 'boubouchons_'$D'.msh'
#done

for D in 512
do
    N=$(bc <<< $D'-8')
    gmsh boubouchons.geo -setnumber RodN $N -setnumber DoMesh 1 \
         -setnumber MeshOrder $O -setnumber DomN $D -
    mv boubouchons.msh 'boubouchons_'$D'.msh'
done
