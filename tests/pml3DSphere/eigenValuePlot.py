import numpy
import os
import matplotlib.pyplot as plt
import math
import seaborn
from matplotlib2tikz import save as tikz_save


    
eigReal = numpy.loadtxt('run_results/EigenValuesReal.txt', usecols = [1])
eigImag = numpy.loadtxt('run_results/EigenValuesImag.txt', usecols = [5])

print(eigReal)
print(eigImag)

plt.plot(eigReal, eigImag,'*'); 

plt.ylabel('Real part (rad / s)')
plt.xlabel('Imaginary part (rad / s)')
plt.title('Spectrum for 3D PML')

plt.show()
