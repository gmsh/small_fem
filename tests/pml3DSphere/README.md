A Onelab model for 3D scattering problems in nanophotonics.

## Synopsis

This project contains a [Onelab](http://onelab.info/wiki/ONELAB) model for solving various 3D electromagnetic scattering problems on an isolated object :
* T-matrix computation: See https://arxiv.org/abs/1802.00596 for details
* Quasi-normal modes
* Plane wave response
* Green's function (LDOS)

## Installation

This model requires the following (open-source) programs:
* [onelab](http://onelab.info/wiki/ONELAB), which bundles both [gmsh](http://www.gmsh.info/) and [getdp](http://www.getdp.info/)
* python (v2.7.x or v3.6.x) with numpy, scipy and matplotlib

## Running the model

Open `scattering.pro` with Gmsh.

## Authors

Guillaume Demésy and Brian Stout