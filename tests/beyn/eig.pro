Jacobian{
  { Name JVol; Case{ { Region All; Jacobian Vol; } } }
  { Name JSur; Case{ { Region All; Jacobian Sur; } } }
  { Name JLin; Case{ { Region All; Jacobian Lin; } } }
}

Integration{
  { Name I1;
    Case{
      { Type Gauss;
        Case{
          { GeoElement Point;       NumberOfPoints 1; }
          { GeoElement Line;        NumberOfPoints 2; }
          { GeoElement Triangle;    NumberOfPoints 3; }
          { GeoElement Quadrangle;  NumberOfPoints 4; }
          { GeoElement Tetrahedron; NumberOfPoints 4; }
          { GeoElement Hexahedron;  NumberOfPoints 6; }
          { GeoElement Prism;       NumberOfPoints 9; }
        }
      }
    }
  }
}

Constraint{
  { Name Dirichlet; Case{ { Region GammaD; Value 0.; } } }
}

FunctionSpace{
  { Name Hgrad; Type Form0;
    BasisFunction{
      { Name sn; NameOfCoef un; Function BF_Node;
        Support Region[ {Omega, GammaD} ]; Entity NodesOf[ All ];
      }
    }
    Constraint{
      { NameOfCoef un; EntityType NodesOf; NameOfConstraint Dirichlet; }
    }
  }

  { Name Hcurl; Type Form1;
    BasisFunction{
      { Name sn1; NameOfCoef un1; Function BF_Edge_1E;
        Support Region[ {Omega, GammaD} ]; Entity EdgesOf[ All ];
      }
      //{ Name sn2; NameOfCoef un2; Function BF_Edge_2E;
      //  Support Region[ {Omega, GammaD} ]; Entity EdgesOf[ All ];
      //}
    }
    Constraint{
      { NameOfCoef un1; EntityType EdgesOf; NameOfConstraint Dirichlet; }
      //{ NameOfCoef un2; EntityType EdgesOf; NameOfConstraint Dirichlet; }
    }
  }
}

Formulation{
  { Name Eig; Type FemEquation;
    Quantity{
      { Name u; Type Local; NameOfSpace Hcurl; }
    }
    Equation{
      Galerkin{        [ Dof{d u} , {d u} ];
        In Omega; Jacobian JVol; Integration I1; }
      Galerkin{ DtDtDof[ Dof{u} ,     {u} ];
        In Omega; Jacobian JVol; Integration I1; }
    }
  }
}

Resolution{
  { Name Eig;
    System{
      { Name Sys; NameOfFormulation Eig; Type Complex; }
    }
    Operation{
      GenerateSeparate[Sys];
      EigenSolve[Sys, 2, 9, 0];
    }
  }
}

PostProcessing{
  { Name Eig; NameOfFormulation Eig;
    PostQuantity{
      { Name u; Value{ Local{ [ {u} ]; In Omega; Jacobian JVol; } } }
    }
  }
}

PostOperation{
  { Name Eig; NameOfPostProcessing Eig;
    Operation{
      Print[ u, OnElementsOf Omega, File "u.pos", EigenvalueLegend ];
    }
  }
}
