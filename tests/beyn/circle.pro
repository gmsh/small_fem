Include "circle.dat";

Group{
  Omega = Region[{}];
  For idom In {0:N_DOM-1}
    Omega += Region[{(100 + idom)}];

    If (idom == N_DOM-1)
      GammaD = Region[{(2000 + idom)}];
    EndIf
  EndFor
}

// Change space in eig.pro to switch between Helmholtz and Maxwell //
Include "eig.pro";
