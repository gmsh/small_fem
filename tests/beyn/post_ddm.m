clear all;
close all;

%% Parameters
type  = 'vector';
mode  = 2;
order = 0;
ddm   = {'oo0', 'jfl', 'pade1', 'pade2', 'pade4'};

%% Get data
data = cell(1, length(ddm));
for i = 1:length(ddm)
  data{i} = ...
    dlmread(sprintf('%s_%s_o%d_mode%d.csv', type, ddm{i}, order, mode), ';');
end

tol = data{i}(4:end, 1);

imag = zeros(size(data{1}(4:end, 3), 1), length(ddm));
rerr = zeros(size(data{1}(4:end, 4), 1), length(ddm));
iter = zeros(size(data{1}(4:end, 5), 1), length(ddm));
for i = 1:length(ddm)
  imag(:, i) = data{i}(4:end, 3);
  rerr(:, i) = data{i}(4:end, 4);
  iter(:, i) = data{i}(4:end, 5);
end

%% Plot
figure;
loglog(tol, rerr, '-o');
legend(ddm, 'location', 'northwest');
grid;
xlabel('Relative tolerance [-]');
ylabel('Relative error [-]');
title(sprintf('%s case - order %d - mode %d', type, order, mode));

figure;
semilogx(tol, iter, '-o');
legend(ddm, 'location', 'northeast');
grid;
xlabel('Relative tolerance [-]');
ylabel('Mean iteration count [-]');
title(sprintf('%s case - order %d - mode %d', type, order, mode));
