clear all;
close all;

%% Parameters
type  = 'vector';
mode  = 2;
order = 0;

%% Get data
data   = dlmread(sprintf('%s_noddm_o%d_mode%d.csv', type, order, mode), ';');
nnodes = data(2:end-1, 1);
real   = data(2:end-1, 3);
imag   = data(2:end-1, 4);
rerr   = (real - data(end, 3)) / data(end, 3);

%% Plot
%figure;
%semilogx(nnodes, real, '-o');
%grid;
%xlabel('# integration nodes [-]');
%ylabel('Real part [rad/s]');
%title(sprintf('%s case', type));

figure;
loglog(nnodes, abs(imag), '-o');
grid;
xlabel('# integration nodes [-]');
ylabel('Imaginary part [rad/s]');
title(sprintf('%s case - order %d - mode %d', type, order, mode));

figure;
loglog(nnodes, abs(rerr), '-o');
grid;
xlabel('# integration nodes [-]');
ylabel('Real part relative (to shift-and-invert) error [-]');
title(sprintf('%s case - order %d - mode %d', type, order, mode));
