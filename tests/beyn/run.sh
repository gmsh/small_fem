#!/bin/bash

make beync && OMP_NUM_THREADS=1 mpirun -np 4 ./beync -type scalar -ddm emda -o 1 -max 1000 -chi 0 -msh ../tests/beyn/circle.msh -n 4 -nopos -solver -ksp_rtol 1e-12
