clear all;
close all;

%% Parameters
type  = 'vector';
order = 0;
ddm   = {'oo0', 'jfl', 'pade1', 'pade2', 'pade4'};

%% Get data
data = cell(1, length(ddm));
for i = 1:length(ddm)
  data{i} = ...
    dlmread(sprintf('%s_%s_o%d_modeSweep.csv', type, ddm{i}, order), ';');
end

n   = data{1}(2:end, 1);
eig = zeros(size(data{1}(2:end, 2), 1), length(ddm));
mit = zeros(size(data{1}(2:end, 3), 1), length(ddm));
for i = 1:length(ddm)
  eig(:, i) = data{i}(2:end, 2);
  mit(:, i) = data{i}(2:end, 3);
end

%% Plot
figure;
plot(n, mit, '-o');
legend(ddm, 'location', 'northwest');
grid;
xlabel('Mode number [-]');
ylabel('Mean iteration count [-]');
title(sprintf('%s case - order %d - mode sweep', type, order));

figure;
plot(n, eig, '-o');
legend(ddm, 'location', 'northwest');
grid;
xlabel('Mode number [-]');
ylabel('Eigenwavenumber [rad/m]');
title(sprintf('%s case - order %d - mode sweep', type, order));
