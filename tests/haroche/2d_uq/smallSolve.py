## Imports
import numpy
import os

def smallSolve(DU_X, DU_Y, DU_W, DCrv, DRMr):
    """Returns the resonance frequency and the lifetime of the photon trapping
    mode for the given perturbations of the original 2D geometry.

    Arguments:
     * DU_X - displacement of the upper mirror along the x-axis       [m]
     * DU_Y - displacement of the upper mirror along the y-axis       [m]
     * DU_W - tilt of the upper mirror along the z-axis               [m]
     * DCrv - deviation from the reference curvature of the mirrors   [m]
     * DRMr - deviation from the reference radius of the mirrors      [m]

    Note: the function will always return something, even if there is no
    guarantee that the TEM009 mode (the photon trapping one) is indeed found!

    TODO: allow a DCrv and DRMr different per mirror
    """
    ## TODO: if things get more complex, we should split this functions...

    ## Binaries
    gmsh = 'gmsh'
    sf   = 'har2d'

    ## Physic data & scaling
    c  = 299792458 # Speed of light in vacuum                   [m/s]
    mm = 1e-3      # Millimetre                                 [1e-3m]

    ## Mirror data
    rCurvSmall = 39.4   * mm + DCrv # Small curvature radius    [m]
    rCurvLarge = 40.6   * mm + DCrv # Large curvature radius    [m]
    rCurv      = rCurvLarge         # Curvature radius          [m]
    L          = 27.57  * mm        # Distance between apexes   [m]
    mirrorT    =  1.415 * mm        # Thickness at centre       [m]
    mirrorR    = 25     * mm + DRMr # Radius                    [m]

    ## Eigenvalue solver data
    guessF  = 9./2. * c/(L+DU_Y)     # Guess of resonance freq. [Hz]
    guessL  = c / guessF             # Corresponding wavelength [m]
    nEig    = 10                     # Number of eigenvalue     [-]
    shift   = (2*numpy.pi*guessF)**2 # Spectral shift           [(rad/s)^2]
    nProc   = 2                      # Number of processes      [-]
    tol     = 1e-15                  # Relative tolerance       [-]
    maxit   = 100                    # Maximum iteration count  [-]
    postpro = 0                      # Plot field maps?         [boolean]

    ## PML data
    pmlT = 1 # PML thickness                                    [wavelength]
    pmlD = 2 # PML-Mirror distance                              [wavelength]

    ## Mesh data
    mshA = 5 # Mesh density in air                              [per wavelength]
    mshP = 5 # Mesh density in the PML                          [per wavelength]
    mshM = 5 # Mesh density on the mirrors                      [per wavelength]
    mshO = 2 # Mesh order                                       [-]

    # FE data
    femO = 5 # FE order                                         [-]

    ## Other data
    pmlTMetre = pmlT * guessL             # pmlT in metre       [m]
    pmlDMetre = pmlD * guessL             # pmlD in metre       [m]
    pmlK      = 2*numpy.pi / guessL       # Wavenumber          [m]
    pmlBoxX   = mirrorR + pmlDMetre       # x coordinate of PML [m]
    pmlBoxY   = L/2 + mirrorT + pmlDMetre # y coordinate of PML [m]

    ## Write PML stuff
    pmlFile = open('pml.dat' ,'w')
    pmlFile.write(str(pmlTMetre) + '\n')
    pmlFile.write(str(pmlTMetre) + '\n')
    pmlFile.write(str(pmlBoxX)   + '\n')
    pmlFile.write(str(pmlBoxY)   + '\n')
    pmlFile.write(str(pmlK)      + '\n')
    os.fsync(pmlFile)
    pmlFile.close()

    ## Write Gmsh stuff
    gmshFile = open('cavity_haroche_2D.dat' ,'w')
    gmshFile.write('DU_X                = ' + str(DU_X)      + ';\n')
    gmshFile.write('DU_Y                = ' + str(DU_Y)      + ';\n')
    gmshFile.write('DU_W                = ' + str(DU_W)      + ';\n')
    gmshFile.write('DL_X                = ' + str(0)         + ';\n')
    gmshFile.write('DL_Y                = ' + str(0)         + ';\n')
    gmshFile.write('DL_W                = ' + str(0)         + ';\n')
    gmshFile.write('MSH_A               = ' + str(mshA)      + ';\n')
    gmshFile.write('MSH_P               = ' + str(mshP)      + ';\n')
    gmshFile.write('MSH_M               = ' + str(mshM)      + ';\n')
    gmshFile.write('ORDER               = ' + str(mshO)      + ';\n')
    gmshFile.write('TYPE                = ' + str(3)         + ';\n')
    gmshFile.write('lambda_haroche      = ' + str(guessL)    + ';\n')
    gmshFile.write('R                   = ' + str(rCurv)     + ';\n')
    gmshFile.write('L_cav               = ' + str(L)         + ';\n')
    gmshFile.write('thick_mirror_center = ' + str(mirrorT)   + ';\n')
    gmshFile.write('radius_mirror       = ' + str(mirrorR)   + ';\n')
    gmshFile.write('pml_x               = ' + str(pmlTMetre) + ';\n')
    gmshFile.write('pml_y               = ' + str(pmlTMetre) + ';\n')
    gmshFile.write('box_x               = ' + str(pmlBoxX)   + ';\n')
    gmshFile.write('box_y               = ' + str(pmlBoxY)   + ';\n')
    os.fsync(gmshFile)
    gmshFile.close()

    # quit()

    ## Mesh
    geo = 'cavity_haroche_2D.geo'
    msh = 'cavity_haroche_2D.msh'
    os.system(gmsh + ' -2 -part ' + str(nProc) + ' ' + geo + ' -o ' + msh)

    ## Simulate
    cmd  = 'taskset 0xFFFFFFFF mpirun -np ' + str(nProc) + ' '
    cmd +=  sf      + ' ' + \
           '-msh'   + ' ' + msh             + ' ' + \
           '-pml'   + ' ' + 'pml.dat'       + ' ' + \
           '-o'     + ' ' + str(int(femO))  + ' ' + \
           '-n'     + ' ' + str(int(nEig))  + ' ' + \
           '-shift' + ' ' + str(shift)      + ' ' + \
           '-tol'   + ' ' + str(tol)        + ' ' + \
           '-maxit' + ' ' + str(int(maxit)) + ' '

    if postpro == 0:
        cmd += '-nopos' + ' '

    cmd += '-solver -eps_monitor' # -eps_view'

    print guessF
    print cmd
    os.environ['OMP_NUM_THREADS'] = str(1)
    os.system(cmd)

    ## Post pro
    freq = numpy.loadtxt('harocheValues.txt', usecols = [3], skiprows = 1)
    time = numpy.loadtxt('harocheValues.txt', usecols = [4], skiprows = 1)
    maxi = numpy.argmax(time)

    return [freq[maxi], time[maxi]]
