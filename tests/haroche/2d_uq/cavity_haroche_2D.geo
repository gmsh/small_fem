// Data //
Include "cavity_haroche_2D.dat";

// Other data //
paramaille_air  = lambda_haroche / MSH_A;
paramaille_pml  = lambda_haroche / MSH_P;
paramaille_mir  = lambda_haroche / MSH_M;
apert           = Sqrt[R^2 - radius_mirror^2] + L_cav / 2 - R;

// Mirror up //
// Points
Point(00)  = {0,              L_cav/2 - R,                   0, paramaille_air};
Point(01)  = {+radius_mirror, apert,                         0, paramaille_mir};
Point(02)  = {+radius_mirror, L_cav/2 + thick_mirror_center, 0, paramaille_mir};
Point(03)  = {-radius_mirror, L_cav/2 + thick_mirror_center, 0, paramaille_mir};
Point(04)  = {-radius_mirror, apert,                         0, paramaille_mir};

// Lines
Line(01)   = {01, 02};
Line(02)   = {02, 03};
Line(03)   = {03, 04};
Circle(04) = {04, 00, 01};

// Surface
ll~{01} = 01; Line     Loop(ll~{01}) = {01, 02, 03, 04};
ps~{01} = 01; Plane Surface(ps~{01}) = {01};

// Mirror down: symmetry //
out[]   = Symmetry{0, 1, 0, 0}{ Duplicata{ Surface{ps~{01}}; } };
ps~{02} = out[0];
out[]   = Boundary{ Surface{ps~{2}}; };
ll~{02} = 02; Line Loop(ll~{02}) = {out[]};

// Translation //
Translate{DU_X, DU_Y, 0}{ Surface{ps~{01}}; }
Translate{DL_X, DL_Y, 0}{ Surface{ps~{02}}; }

// Rotations //
Rotate{{0, 0, 1}, {0, 0, 0}, DU_W * Pi / 180}{ Surface{ps~{01}}; Point{05}; }
Rotate{{0, 0, 1}, {0, 0, 0}, DL_W * Pi / 180}{ Surface{ps~{02}}; Point{06}; }

// Air Box //
// Points
p~{01} = newp; Point(p~{01}) = {+box_x, +box_y, 0, paramaille_air};
p~{02} = newp; Point(p~{02}) = {-box_x, +box_y, 0, paramaille_air};
p~{03} = newp; Point(p~{03}) = {-box_x, -box_y, 0, paramaille_air};
p~{04} = newp; Point(p~{04}) = {+box_x, -box_y, 0, paramaille_air};

// Lines
l~{01} = newl; Line(l~{01}) = {p~{01}, p~{02}};
l~{02} = newl; Line(l~{02}) = {p~{02}, p~{03}};
l~{03} = newl; Line(l~{03}) = {p~{03}, p~{04}};
l~{04} = newl; Line(l~{04}) = {p~{04}, p~{01}};

// Surface
ll~{03} = newll; Line     Loop(ll~{03}) = { l~{01},  l~{02},  l~{03},  l~{04}};
ps~{03} =  news; Plane Surface(ps~{03}) = {ll~{03}, ll~{02}, ll~{01}};

// PML //
// Points
p~{05} = newp; Point(p~{05}) = {+box_x+pml_x, +box_y,       0, paramaille_pml};
p~{06} = newp; Point(p~{06}) = {+box_x+pml_x, +box_y+pml_y, 0, paramaille_pml};
p~{07} = newp; Point(p~{07}) = {+box_x,       +box_y+pml_y, 0, paramaille_pml};
p~{08} = newp; Point(p~{08}) = {-box_x,       +box_y+pml_y, 0, paramaille_pml};
p~{09} = newp; Point(p~{09}) = {-box_x-pml_x, +box_y+pml_y, 0, paramaille_pml};
p~{10} = newp; Point(p~{10}) = {-box_x-pml_x, +box_y,       0, paramaille_pml};
p~{11} = newp; Point(p~{11}) = {-box_x-pml_x, -box_y,       0, paramaille_pml};
p~{12} = newp; Point(p~{12}) = {-box_x-pml_x, -box_y-pml_y, 0, paramaille_pml};
p~{13} = newp; Point(p~{13}) = {-box_x,       -box_y-pml_y, 0, paramaille_pml};
p~{14} = newp; Point(p~{14}) = {+box_x,       -box_y-pml_y, 0, paramaille_pml};
p~{15} = newp; Point(p~{15}) = {+box_x+pml_x, -box_y-pml_y, 0, paramaille_pml};
p~{16} = newp; Point(p~{16}) = {+box_x+pml_x, -box_y,       0, paramaille_pml};

// Lines (XY)
l~{05} = newl; Line(l~{05}) = {p~{01}, p~{05}};
l~{06} = newl; Line(l~{06}) = {p~{05}, p~{06}};
l~{07} = newl; Line(l~{07}) = {p~{06}, p~{07}};
l~{08} = newl; Line(l~{08}) = {p~{07}, p~{01}};

l~{09} = newl; Line(l~{09}) = {p~{02}, p~{08}};
l~{10} = newl; Line(l~{10}) = {p~{08}, p~{09}};
l~{11} = newl; Line(l~{11}) = {p~{09}, p~{10}};
l~{12} = newl; Line(l~{12}) = {p~{10}, p~{02}};

l~{13} = newl; Line(l~{13}) = {p~{03}, p~{11}};
l~{14} = newl; Line(l~{14}) = {p~{11}, p~{12}};
l~{15} = newl; Line(l~{15}) = {p~{12}, p~{13}};
l~{16} = newl; Line(l~{16}) = {p~{13}, p~{03}};

l~{17} = newl; Line(l~{17}) = {p~{04}, p~{14}};
l~{18} = newl; Line(l~{18}) = {p~{14}, p~{15}};
l~{19} = newl; Line(l~{19}) = {p~{15}, p~{16}};
l~{20} = newl; Line(l~{20}) = {p~{16}, p~{04}};

// Lines (Y)
l~{21} = newl; Line(l~{21}) = {p~{08}, p~{07}};
l~{22} = newl; Line(l~{22}) = {p~{14}, p~{13}};

// Lines (X)
l~{23} = newl; Line(l~{23}) = {p~{05}, p~{16}};
l~{24} = newl; Line(l~{24}) = {p~{11}, p~{10}};

// Surfaces (XY)
ll~{04} = newll; Line     Loop(ll~{04}) = { l~{05},  l~{06},  l~{07},  l~{08}};
ll~{05} = newll; Line     Loop(ll~{05}) = { l~{09},  l~{10},  l~{11},  l~{12}};
ll~{06} = newll; Line     Loop(ll~{06}) = { l~{13},  l~{14},  l~{15},  l~{16}};
ll~{07} = newll; Line     Loop(ll~{07}) = { l~{17},  l~{18},  l~{19},  l~{20}};

ps~{04} =  news; Plane Surface(ps~{04}) = {ll~{04}};
ps~{05} =  news; Plane Surface(ps~{05}) = {ll~{05}};
ps~{06} =  news; Plane Surface(ps~{06}) = {ll~{06}};
ps~{07} =  news; Plane Surface(ps~{07}) = {ll~{07}};

// Surfaces (Y)
ll~{08} = newll; Line     Loop(ll~{08}) = { l~{01},  l~{09},  l~{21},  l~{08}};
ll~{09} = newll; Line     Loop(ll~{09}) = { l~{03},  l~{17},  l~{22},  l~{16}};
ps~{08} =  news; Plane Surface(ps~{08}) = {ll~{08}};
ps~{09} =  news; Plane Surface(ps~{09}) = {ll~{09}};

// Surfaces (X)
ll~{10} = newll; Line     Loop(ll~{10}) = { l~{04},  l~{05},  l~{23},  l~{20}};
ll~{11} = newll; Line     Loop(ll~{11}) = { l~{02},  l~{12},  l~{24},  l~{13}};
ps~{10} =  news; Plane Surface(ps~{10}) = {ll~{10}};
ps~{11} =  news; Plane Surface(ps~{11}) = {ll~{11}};

// Physicals //
Physical     Line(103) = {04, out[3]};                         // Mirror
Physical     Line(104) = {l~{06}, l~{07}, l~{21}, l~{10},
                          l~{11}, l~{24}, l~{14}, l~{15},
                          l~{22}, l~{18}, l~{19}, l~{23}};     // Outer PML
Physical     Line(105) = {01, 02, 03, out[0], out[1], out[2]}; // Frame
Physical Surface(1000) = {ps~{10}, ps~{11}};                   // PML_X
Physical Surface(2000) = {ps~{04}, ps~{05}, ps~{06}, ps~{07}}; // PML_XY
Physical Surface(3000) = {ps~{08}, ps~{09}};                   // PML_Y
Physical Surface(4000) = {ps~{03}};                            // Air

// Display //
BoundingBox {-box_x-pml_x, +box_x+pml_x, -box_y-pml_y, +box_y+pml_y, 0, 0};

// Options //
If(TYPE == 4)
  Recombine Surface "*";
EndIf

If(ORDER > 1)
  Mesh.HighOrderOptimize = 1;
EndIf

Mesh.Optimize     = 1;
Mesh.ElementOrder = ORDER;
