// Scaling
mm = 1e-3;
nm = 1e-9;

// Physics
C0   = 299792458;           // [m/s]
Mu0  = 4 * Pi * 1e-7;       // [H/m]
Eps0 = 1 / (C0 * C0 * Mu0); // [F/m]

// First guess: resonance frequency, wavelength & wavenumber
DefineConstant[
  F0      = { 51.099e9,   Name "Input/00FirstGuess/00Frequency",  Units "Hz" },
  Lambda0 = { C0/F0,      Name "Input/00FirstGuess/01Wavelength", Units "m",
              ReadOnly 1 },
  K0      = { 2*Pi*F0/C0, Name "Input/00FirstGuess/02Wavenumber", Units "rad/m",
              ReadOnly 1}
];

// Cavity
DefineConstant[
  r             = { 39.400*mm, Name "Input/01Cavity/00Small curv.", Units "m" },
  R             = { 40.600*mm, Name "Input/01Cavity/01Large curv.", Units "m" },
  L_cav         = { 27.570*mm, Name "Input/01Cavity/02Length",      Units "m" },
  radius_mirror = { 25.000*mm, Name "Input/01Cavity/03Radius",      Units "m" },
  thick_mirror  = {  1.415*mm, Name "Input/01Cavity/04Thickness",   Units "m" }
];

// Air box and PML
DefineConstant[
  distXY = { 1, Name "Input/02PML/00Air box size (XY)", Units "wavelength" },
  distZ  = { 1, Name "Input/02PML/01Air box size (Z)",  Units "wavelength" },
  thick  = { 1, Name "Input/02PML/03Thickness",         Units "wavelength" }
];

dist2PML_xy =  distXY * Lambda0;
dist2PML_z  =  distZ  * Lambda0;

box_x =             radius_mirror + dist2PML_xy;
box_y =             radius_mirror + dist2PML_xy;
box_z = L_cav / 2. + thick_mirror + dist2PML_z;

pml_x = thick * Lambda0;
pml_y = thick * Lambda0;
pml_z = thick * Lambda0;

// Mesh
DefineConstant[
  mAir  = { 5, Name "Input/03Mesh/00Air density",    Units "elem./wavelength" },
  mPML  = { 5, Name "Input/03Mesh/01PML density",    Units "elem./wavelength" },
  mMir  = { 5, Name "Input/03Mesh/02Mirror density", Units "elem./wavelength" },
  mO    = { 2, Name "Input/03Mesh/03Order",          Units "-"                },
  nPart = { 2, Name "Input/03Mesh/04#partitions",    Units "-"                }
];

clPML = Lambda0 / mPML;
clAir = Lambda0 / mAir;
clMir = Lambda0 / mMir;
