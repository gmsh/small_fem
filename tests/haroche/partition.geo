// Data //
//////////
Include "cavity.dat";
Merge   "cavity.msh";

// Partitioning //
//////////////////
Mesh.PartitionCreateTopology   = 0;
Mesh.PartitionCreatePhysicals  = 0;
Mesh.PartitionCreateGhostCells = 0;

If(nPart > 1)
  PartitionMesh nPart;
  Save Sprintf("cavity_part%g.msh", nPart);
EndIf
