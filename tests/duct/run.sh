#!/bin/bash

k=40
for m in 04 06 08 10 12 14 16 18 20 22 24 26 28 30 32
do
    echo ${m}
    gmsh duct.geo -setnumber N_LAMBDA ${m} -setnumber K ${k} -o duct_${m}.msh -2 &> log_${m}
done
