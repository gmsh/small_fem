// User constants //
DefineConstant[
  K        = {40,              Name "Input/01Mesh/00Wavenumber"},
  LAMBDA   = {2* Pi / K,       Name "Input/01Mesh/01Wavelength",   ReadOnly 1},
  N_LAMBDA = {8,               Name "Input/01Mesh/02Points per wavelength"},
  LC       = {LAMBDA/N_LAMBDA, Name "Input/01Mesh/03Mesh density", ReadOnly 1}
];

Printf("K       : %g", K);
Printf("N_LAMBDA: %g", N_LAMBDA);

// Other constants //
LX = 2;
LY = 1;

// Geometry (1D) //
Point(1) = {0,  0, 0, LC};
Point(2) = {LX, 0, 0, LC};

Line(1) = {1, 2};
Transfinite Line {1} = (Ceil(LX / LAMBDA * N_LAMBDA)) Using Progression 1;

// Geometry (2D) //
// Extrusion
Extrude {0, LY, 0} {
  Line{1};
  Layers{Ceil(LY / LAMBDA * N_LAMBDA)};
}

// Physicals //
Physical    Line(4) = {4};    // Infinity
Physical    Line(5) = {3};    // Source
Physical Surface(7) = {5};    // Volume
