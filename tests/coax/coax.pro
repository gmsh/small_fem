Include "coax.dat";

Group{
  Inner = Region[1];
  Gap   = Region[2];
  Outer = Region[3];

  dSide = Region[4];
  dOut  = Region[5];

     Src = Region[{Inner, Outer}];
   Omega = Region[{Src, Gap}];
  dOmega = Region[{dSide, dOut}];

  If(gauge == 0)
    Tree = EdgesOfTreeIn[Omega, StartingOn dOmega];
  EndIf
  If(gauge == 1)
    Tree = Region[6];
  EndIf
  If(gauge > 1)
    Tree = Region[{}];
  EndIf

  Dummy = Region[7];
}

Function{
  // Physical constants
  Mu0  = 4*Pi*1e-7;
  Nu0  = 1/Mu0;

  // Hack
  One = 1;

  // Radial and azimuthal positions
  R[]   = Sqrt[X[]^2 + Y[]^2];
  Phi[] = Atan2[Y[], X[]];

  // Current density
  Js[Inner] = Vector[0, 0, +I0 / (Pi*(a^2))];
  Js[Outer] = Vector[0, 0, -I0 / (Pi*(c^2-b^2))];

  // Analytic solution
  BMag[Inner] = (Mu0 * I0) / (2*Pi*a^2) * R[];
  BMag[Gap]   = (Mu0 * I0) / (2*Pi*R[]);
  BMag[Outer] = (Mu0 * I0) / (2*Pi*R[]) * ((c^2-R[]^2)/(c^2-b^2));
  B[]         = Vector[-Sin[Phi[]] * BMag[],
                       +Cos[Phi[]] * BMag[],
                       0];
}

Jacobian{
  { Name Vol; Case{ { Region All; Jacobian Vol; } } }
  { Name Lin; Case{ { Region All; Jacobian Lin; } } }
}

Integration{
  { Name I1;
    Case{ { Type Gauss;
            Case{ { GeoElement Line;        NumberOfPoints 3;  }
                  { GeoElement Triangle;    NumberOfPoints 3;  }
                  { GeoElement Tetrahedron; NumberOfPoints 4;  } } } }
  }
}

Constraint{
  If(gauge == 0)
    { Name TCTGauge; Type Assign; Case{ { Region  Omega; Value 0.;
                                       SubRegion dOmega;           } } }
  EndIf
  If(gauge == 1)
    { Name TCTGauge; Type Assign; Case{ { Region Tree;   Value 0.; } } }
  EndIf
    { Name PEC;      Type Assign; Case{ { Region dSide;  Value 0.; }
                                        { Region dOut;   Value 0.; } } }
}

FunctionSpace{
  { Name HCurl; Type Form1;
    BasisFunction{
      { Name se; NameOfCoef an; Function BF_Edge;
        Support Region[{Omega, Src, Tree}]; Entity EdgesOf[All]; }
    }
    Constraint{
      If(gauge == 0)
        { NameOfCoef an; NameOfConstraint TCTGauge;
          EntityType EdgesOfTreeIn; EntitySubType StartingOn;           }
      EndIf
      If(gauge == 1)
        { NameOfCoef an; NameOfConstraint TCTGauge; EntityType EdgesOf; }
      EndIf
        { NameOfCoef an; NameOfConstraint PEC;      EntityType EdgesOf; }
    }
  }

  { Name HGrad; Type Form0;
    BasisFunction{
      { Name un; NameOfCoef bn; Function BF_Node;
        Support Region[{Omega, Src, Tree}]; Entity NodesOf[All]; }
    }
    Constraint{
      { NameOfCoef bn; NameOfConstraint PEC; EntityType NodesOf; }
    }
  }
}

Formulation{
  { Name MagStaA; Type FemEquation;
    Quantity{
      If(gauge == 2)
        { Name xi; Type Local; NameOfSpace HGrad; }
      EndIf
        { Name a;  Type Local; NameOfSpace HCurl; }
   }
    Equation{
      Galerkin{ [Nu0 * Dof{d a}, {d a}];
        In Omega; Jacobian Vol; Integration I1; }
      Galerkin{ [-Js[], {a}];
        In Src;   Jacobian Vol; Integration I1; }

      If(gauge == 2)
        Galerkin{ [Dof{d xi}, {a}];
          In Omega; Jacobian Vol; Integration I1; }
        Galerkin{ [Dof{a}, {d xi}];
          In Omega; Jacobian Vol; Integration I1; }
      EndIf
    }
  }
}

Resolution{
  { Name MagStaA;
    System{
      { Name A; NameOfFormulation MagStaA; }
    }
    Operation{
      Generate[A];
      Solve[A];
    }
  }
}

PostProcessing{
  { Name MagStaA; NameOfFormulation MagStaA;
    PostQuantity{
      { Name js;   Value{ Term{ [Js[]];  In Src;   Jacobian Vol; } } }
      { Name a;    Value{ Term{ [{a}];   In Omega; Jacobian Vol; } } }
      { Name b;    Value{ Term{ [{d a}]; In Omega; Jacobian Vol; } } }
      { Name bAna; Value{ Term{ [B[]];   In Omega; Jacobian Vol; } } }

      { Name eAbS; Value{ Integral{ [Norm[B[]-{d a}]^2]; In Gap; Jacobian Vol;
                                                         Integration I1;   } } }
      { Name bInt; Value{ Integral{ [Norm[B[]]^2];       In Gap; Jacobian Vol;
                                                         Integration I1;   } } }
      { Name e;    Value{     Term{ [Sqrt[$eAbs/$bInt]]; In Dummy;
                                                         Type Global;      } } }

    If(gauge == 1)
      { Name Tree; Value{ Term{ [One];    In Tree;  Jacobian Lin; } } }
    EndIf
    If(gauge == 2)
      { Name dxi;  Value{ Term{ [{d xi}]; In Omega; Jacobian Vol; } } }
    EndIf
    }
  }
}

PostOperation MagStaA UsingPost MagStaA{
  Print[js,   OnElementsOf Src,   File   "js.pos"];
  Print[a,    OnElementsOf Omega, File    "a.pos"];
  Print[b,    OnElementsOf Omega, File    "b.pos"];
  Print[bAna, OnElementsOf Omega, File "bAna.pos"];

  Print[eAbS[Gap], OnGlobal,       Format Table, StoreInVariable $eAbs];
  Print[bInt[Gap], OnGlobal,       Format Table, StoreInVariable $bInt];
  Print[e,         OnRegion Dummy, Format Table,
        SendToServer "04Output/00L2 Error [-]", Color "Ivory"];

  If(gauge == 0)
    PrintGroup[Tree,      In Omega, File "tree.pos"];
  EndIf
  If(gauge == 1)
    Print[Tree, OnElementsOf Tree,  File "tree.pos"];
  EndIf
  If(gauge == 2)
    Print[dxi,  OnElementsOf Omega, File  "dxi.pos"];
  EndIf
}
