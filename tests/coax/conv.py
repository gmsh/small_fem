import numpy
import onelab

## Geometry
ra = 0.1
rb = 0.2
rc = 0.3
l  = 0.1

## Mesh and FEM
h = numpy.array([0.1, 0.05, 0.025]) # Mesh
o = numpy.array([1, 2, 3, 4])       # FEM order
p = 2                               # Mesh order
g = 1                               # Gauge (1: Tree/cotree | 2: Coulomb)

## Onelab
c = onelab.client(__file__)

## Set geometry
c.setNumber('00Geo/00Inner radius [m]', value=ra)
c.setNumber('00Geo/01Gap radius [m]',   value=rb)
c.setNumber('00Geo/02Outer radius [m]', value=rc)
c.setNumber('00Geo/03Length [m]',       value=l )

## Set gauge
c.setNumber('03Solver/00Gauge', value=g)

## Set mesh order
c.setNumber('01Mesh/01Order', value=p)

## Check
if c.action == 'check':
   exit(0)

## Convergence test
l2B = numpy.zeros(shape=(o.size, h.size))
l2L = numpy.zeros(shape=(o.size, h.size))

for io in range(o.size):
    for ih in range(h.size):
        c.setNumber('01Mesh/00Size [m]', value=h[ih])
        c.runSubClient('myGmsh',  'gmsh coax.geo -')

        print(h[ih], o[io])
        c.setNumber('03Solver/01Order', value=o[io])
        c.runSubClient('coax',     './coax.exe -nopos')

        l2B[io][ih] = c.getNumber('04Output/00L2 Error [-]')
        l2L[io][ih] = c.getNumber('04Output/02Inductance error [-]')

numpy.savetxt("l2B.csv", l2B, delimiter=";", fmt="%+.16e")
numpy.savetxt("l2L.csv", l2L, delimiter=";", fmt="%+.16e")
numpy.savetxt("l2h.csv", h,   delimiter=";", fmt="%+.16e")
numpy.savetxt("l2o.csv", o,   delimiter=";", fmt="%+.16e")
