// OpenCASCADE //
/////////////////
SetFactory("OpenCASCADE");

// Data //
//////////
Include "coax.dat";

// Coax cable //
////////////////
v1  = newv; Cylinder(v1) = {0,0,-l/2, 0,0,l, a}; // Inner conductor
v2  = newv; Cylinder(v2) = {0,0,-l/2, 0,0,l, b}; // Gap
v3  = newv; Cylinder(v3) = {0,0,-l/2, 0,0,l, c}; // Outer conductor

v[] = BooleanFragments{Volume{v1, v2, v3}; Delete;}{};

// Loop //
//////////
l2  = newl; Line(l2) = {2, 3}; Line{l2} In Surface{2};
l4  = newl; Line(l4) = {4, 1}; Line{l4} In Surface{3};
l[] = {-1, l2, 6, l4};

// Mesh size //
///////////////
p[] = PointsOf{Volume{v[]};};
Characteristic Length{p[]} = cl;

// Boundaries //
////////////////
tmp[] = Boundary{Volume{v[0]};};
bS[]  = tmp[{1:2}];
tmp[] = Boundary{Volume{v[1]};};
bS[] += tmp[{1:2}];
tmp[] = Boundary{Volume{v[2]};};
bS[] += tmp[{1:2}];
bO    = tmp[0];

// Physicals //
///////////////
Physical  Volume(1) = {v[0]};  // Inner conductor
Physical  Volume(2) = {v[1]};  // Gap
Physical  Volume(3) = {v[2]};  // Outer conductor

Physical Surface(4) = {bS[]}; // Side
Physical Surface(5) = {bO};   // Outer
Physical   Point(7) = {p[0]}; // Dummy point for post-pro
Physical    Line(8) = {l[{1:3}]};  // Loop+ for inductance
Physical    Line(9) = {l[0]};      // Loop- for inductance

// Mesh and Spanning Tree //
////////////////////////////
Solver.AutoMesh   = -1;           // The geometry script generates the mesh
Mesh.ElementOrder = mO;           // Set mesh order
If(StrCmp(OnelabAction, "check")) // Only mesh if not in onelab check mode
  // Mesh //
  Mesh 3;

  // Spanning tree //
  Plugin(SpanningTree).PhysicalVolumes  = "1, 2, 3";
  Plugin(SpanningTree).PhysicalSurfaces = "4, 5";
  Plugin(SpanningTree).OutputPhysical   = 6;
  Plugin(SpanningTree).Run;

  // Save //
  Save "coax.msh";
EndIf
