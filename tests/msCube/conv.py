import numpy
import onelab

## Mesh and FEM
h = numpy.array([1, 2, 3, 4]) # Mesh level
o = numpy.array([1, 2, 3, 4]) # FEM order
g = 1                         # Gauge (1: Tree/cotree | 2: Coulomb)

## Onelab
c = onelab.client(__file__)

## Set gauge
c.setNumber('03Solver/00Gauge', value=g)

## Check
if c.action == 'check':
   exit(0)

## Convergence test
l2B = numpy.zeros(shape=(o.size, h.size))
l2F = numpy.zeros(shape=(o.size, h.size))

for ih in range(h.size):
   c.setNumber('01Mesh/00Level', value=h[ih])
   c.runSubClient('myGmsh', 'gmsh cube.geo -')

   for io in range(o.size):
      print(h[ih], o[io])
      c.setNumber('03Solver/01Order', value=o[io])
      c.runSubClient('cube', './cube.exe -nopos')

      l2B[io][ih] = c.getNumber('04Output/00L2 Error [-]')
      l2F[io][ih] = c.getNumber('04Output/02Flux error [-]')

numpy.savetxt("l2B.csv", l2B, delimiter=";", fmt="%+.16e")
numpy.savetxt("l2F.csv", l2F, delimiter=";", fmt="%+.16e")
numpy.savetxt("l2h.csv", h,   delimiter=";", fmt="%+.16e")
numpy.savetxt("l2o.csv", o,   delimiter=";", fmt="%+.16e")
