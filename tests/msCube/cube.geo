// Data //
//////////
Include "cube.dat";

// Mesh refinement //
/////////////////////
m = 2^(n+1); // We need at least 4 line elements per edge when n=1

// Geo //
/////////
Point(1) = {0, 0, 0};

c1[] = Extrude{Pi/2, 0, 0}{Point{1};                 Layers{m/4};};
c2[] = Extrude{Pi,   0, 0}{Point{c1[0]};             Layers{m/2};};
c3[] = Extrude{Pi/2, 0, 0}{Point{c2[0]};             Layers{m/4};};

s1[] = Extrude{0, Pi/2, 0}{Curve{c1[1],c2[1],c3[1]}; Layers{m/4};};
s2[] = Extrude{0, Pi,   0}{Curve{s1[{0:8:4}]};       Layers{m/2};};
s3[] = Extrude{0, Pi/2, 0}{Curve{s2[{0:8:4}]};       Layers{m/4};};

v1[] = Extrude{0, 0, 2*Pi}{Surface{s1[{0+1:8+1:4}],
                                   s2[{0+1:8+1:4}],
                                   s3[{0+1:8+1:4}]}; Layers{m/1};};
v2[] = v1[{0+1:(8*6)+1:6}];

// Loop //
//////////
tmp[]  = Boundary{Volume{v2[]};};
loop[] = Boundary{Surface{tmp[6*4+4]};};

// Outer boundary //
////////////////////
bnd[] = CombinedBoundary{Volume{v2[]};};

// Physical //
//////////////
Physical  Volume(1) = v2[];
Physical Surface(2) = bnd[];
Physical    Line(4) = loop[];

// Mesh and Spanning Tree //
////////////////////////////
Solver.AutoMesh   = -1;           // The geometry script generates the mesh
If(StrCmp(OnelabAction, "check")) // Only mesh if not in onelab check mode
  // Mesh //
  Mesh 3;

  // Spanning tree //
  Plugin(SpanningTree).PhysicalVolumes  = "1";
  Plugin(SpanningTree).PhysicalSurfaces = "2";
  Plugin(SpanningTree).OutputPhysical   = 3;
  Plugin(SpanningTree).Run;

  // Save //
  Save "cube.msh";
EndIf
