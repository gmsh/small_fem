B = dlmread('l2B.csv', ';');
F = dlmread('l2F.csv', ';');
o = dlmread('l2o.csv', ';');
l = dlmread('l2h.csv', ';');
h = 2.^(l-1);

dB = conv(B, o, h)
title('B-field');

dF = conv(F, o, h)
title('Flux');
