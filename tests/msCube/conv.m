function d = conv(l2, o, h)
  %% Plot
  figure;
  loglog(h, l2', '-*');
  xlabel('Mesh density [1/m]');
  ylabel('L2 error [-]');
  grid;

  %% Legend
  l  = cell(size(o));
  nO = length(o);
  for i = 1:nO
    l{i} = num2str(o(i));
  end
  legend(l);

  %% Make sure h is a line vector
  nH       = length(h);
  hLine    = zeros(1, nH);
  hLine(:) = h(:);

  %% Compute slopes in loglog
  d = zeros(nO, nH-1);
  for o = 1:nO
    d(o, :) = diff(log10(l2(o, :)))./diff(log10(1./hLine));
  end
end
