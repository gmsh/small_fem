Include "cube.dat";

Group{
   Omega = Region[1];
  dOmega = Region[2];

If(gauge == 0)
  Tree = EdgesOfTreeIn[Omega, StartingOn dOmega];
EndIf
If(gauge == 1)
  Tree = Region[3];
EndIf
If(gauge > 1)
  Tree = Region[{}];
EndIf
}

Function{
  // Physical constants
  Mu0  = 4*Pi*1e-7;
  Nu0  = 1/Mu0;

  // Current density
  Js[] = Vector[0, 0, 2*Nu0*Sin[X[]]*Sin[Y[]]];
  A[]  = Vector[0, 0,       Sin[X[]]*Sin[Y[]]];
  B[]  = Vector[+Sin[X[]]*Cos[Y[]],
                -Cos[X[]]*Sin[Y[]],
                0];

  // One
  One[] = 1;
}

Jacobian{
  { Name Vol; Case{ { Region All; Jacobian Vol; } } }
  { Name Sur; Case{ { Region All; Jacobian Sur; } } }
  { Name Lin; Case{ { Region All; Jacobian Lin; } } }
}

Integration{
  { Name I1;
    Case{ { Type Gauss;
            Case{ { GeoElement Line;        NumberOfPoints 3;  }
                  { GeoElement Triangle;    NumberOfPoints 3;  }
                  { GeoElement Tetrahedron; NumberOfPoints 4;  } } } }
  }
}

Constraint{
If(gauge == 0)
  { Name TCTGauge;  Type Assign;
    Case{ { Region Omega;  Value 0.; SubRegion dOmega;  } } }
EndIf
If(gauge == 1)
  { Name TCTGauge;  Type Assign;
    Case{ { Region Tree;   Value 0.; } } }
EndIf
  { Name Dirichlet; Type AssignFromResolution;
    Case{ { Region dOmega; NameOfResolution Dirichlet; } } }
  { Name Dirichlet0; Type Assign;
    Case{ { Region dOmega; Value 0.; } } }
}

FunctionSpace{
  { Name HCurl; Type Form1;
    BasisFunction{
      { Name se; NameOfCoef an; Function BF_Edge;
        Support Region[{Omega, dOmega}]; Entity EdgesOf[All]; }
    }
    Constraint{
    If(gauge == 0)
      { NameOfCoef an; NameOfConstraint TCTGauge;   EntityType EdgesOfTreeIn;
        EntitySubType StartingOn; }
    EndIf
    If(gauge == 1)
      { NameOfCoef an; NameOfConstraint TCTGauge;   EntityType EdgesOf; }
    EndIf
      { NameOfCoef an; NameOfConstraint Dirichlet0; EntityType EdgesOf; }
    }
  }

  { Name HGrad; Type Form0;
    BasisFunction{
      { Name un; NameOfCoef bn; Function BF_Node;
        Support Region[{Omega, dOmega}]; Entity NodesOf[Omega, Not dOmega]; }
    }
  }
}

Formulation{
  { Name Dirichlet; Type FemEquation;
    Quantity{
      { Name a; Type Local; NameOfSpace HCurl; }
    }
    Equation{
      Galerkin{ [Dof{a}, {a}];
        In dOmega; Jacobian Sur; Integration I1; }
      Galerkin{ [-A[], {a}];
        In dOmega; Jacobian Sur; Integration I1; }
    }
  }

  { Name MagStaA; Type FemEquation;
    Quantity{
    If(gauge == 2)
      { Name xi; Type Local; NameOfSpace HGrad; }
    EndIf
      { Name a;  Type Local; NameOfSpace HCurl; }
    }
    Equation{
      Galerkin{ [Nu0 * Dof{d a}, {d a}];
        In Omega; Jacobian Vol; Integration I1; }
      Galerkin{ [-Js[], {a}];
        In Omega; Jacobian Vol; Integration I1; }

    If(gauge == 2)
      Galerkin{ [Dof{d xi}, {a}];
        In Omega; Jacobian Vol; Integration I1; }
      Galerkin{ [Dof{a}, {d xi}];
        In Omega; Jacobian Vol; Integration I1; }
    EndIf
    }
  }
}

Resolution{
  { Name MagStaA;
    System{
      { Name A; NameOfFormulation MagStaA; }
    }
    Operation{
      Generate[A];
      Solve[A];
    }
  }

  { Name Dirichlet;
    System{
      { Name B; NameOfFormulation Dirichlet; DestinationSystem A; }
    }
    Operation{
      Generate[B];
      Solve[B];
//    PostOperation[Dirichlet];
      TransferSolution[B];
    }
  }
}

PostProcessing{
  { Name MagStaA; NameOfFormulation MagStaA;
    PostQuantity{
      { Name js;    Value{ Term{ [Js[]];   In Omega; Jacobian Vol; } } }
      { Name aAna;  Value{ Term{ [A[]];    In Omega; Jacobian Vol; } } }
      { Name a;     Value{ Term{ [{a}];    In Omega; Jacobian Vol; } } }
      { Name bAna;  Value{ Term{ [B[]];    In Omega; Jacobian Vol; } } }
      { Name b;     Value{ Term{ [{d a}];  In Omega; Jacobian Vol; } } }
    If(gauge == 1)
      { Name gauge; Value{ Term{ [One[]];  In Tree;  Jacobian Lin; } } }
    EndIf
    If(gauge == 2)
      { Name gauge; Value{ Term{ [{d xi}]; In Omega; Jacobian Vol; } } }
    EndIf
    }
  }

  { Name Dirichlet; NameOfFormulation Dirichlet;
    PostQuantity{
      { Name a; Value{ Term{ [{a}]; In dOmega; Jacobian Sur; } } }
    }
  }
}

PostOperation MagStaA UsingPost MagStaA{
  Print[js,    OnElementsOf Omega, File    "js.pos"];
  Print[aAna,  OnElementsOf Omega, File  "aAna.pos"];
  Print[a,     OnElementsOf Omega, File     "a.pos"];
  Print[bAna,  OnElementsOf Omega, File  "bAna.pos"];
  Print[b,     OnElementsOf Omega, File     "b.pos"];
If(gauge == 0)
  PrintGroup[Tree,       In Omega, File "gauge.pos"];
EndIf
If(gauge == 1)
  Print[gauge, OnElementsOf Tree,  File "gauge.pos"];
EndIf
If(gauge == 2)
  Print[gauge, OnElementsOf Omega, File "gauge.pos"];
EndIf
}

PostOperation Dirichlet UsingPost Dirichlet{
  Print[a, OnElementsOf dOmega, File "ab.pos"];
}
