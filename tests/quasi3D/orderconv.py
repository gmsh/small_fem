import numpy as np
import os
import matplotlib.pyplot as plt
import math
from tikzplotlib import save as tikz_save
import scipy
import pillboxFreq as pillbox

#Tests the convergence with respect to the mesh refinement for a single
# mode and a single parameter configuration for multiple order choices.
#
# Generates plots like Fig. 5.8. in my BSc thesis.

#Author: Erik Schnaubelt

############################### USER PARAMETERS ################################
# 0 : ephiStar, 1: param., 2: Dunn, 3: Oh
ansatz = 3
beta   = 1
alpha  = 1

# choose pillbox mode TE/TM_{npq}
n  = 1
p  = 1
q  = 1
TE = 1

# choose mesh densities
mshList = np.array([8,16,32]);

# choose orders HGrad
orderHGrad= np.array([4,3]);
# choose orders HCurl
orderHCurl= np.array([3,2]);

# postpro
nocut=1
nopos=1
nopdf=0

# choose quadrature order 
quadOrder = 16

############################ Automatized tests #################################
# get target freq
if(TE == 1):
    f = pillbox.TEfreg(n,p,q)
else:
    f = pillbox.TMfreg(n,p,q)

# data
solList = np.zeros((orderHGrad.size,mshList.size))

c0          = 299792458
transfinite = 0
radius      = 0.15
height      = 0.294

geo = 'geo/trafo.geo'
msh = 'geo/trafo.msh'
dat = 'geo/trafo.dat'
smallFem = '../../build/'

omega    = 2*math.pi*f
eigShift = omega**2
l        = c0/f
tol      = 1e-18

i = 0

for mm in mshList:
    j      = 0

    # write .dat
    MSH     = mm
    cl      = l/MSH
    par_dat = open(dat, 'w');
    par_dat.write('radius = %3.15e;\n' %(radius))
    par_dat.write('height = %3.15e;\n' %(height))
    par_dat.write('f = %3.15e;\n' %(f))
    par_dat.write('cl = %3.15e;\n' %(cl))
    par_dat.write('MSH = %3.15e;\n' %(MSH))
    par_dat.write('transfinite = %i;\n' %(transfinite))
    par_dat.close()

    os.system('gmsh '+ geo +' -2 -format msh2 ')

    #iterate over orders
    for hGrad in orderHGrad:
        oHG = hGrad
        oHC = orderHCurl[j]
        opt = ' -msh '    + msh           + \
              ' -beta '   + str(beta)     + \
              ' -alpha '  + str(alpha)    + \
              ' -shift '  + str(eigShift) + \
              ' -oHG '    + str(oHG)      + \
              ' -oHC   '  + str(oHC)      + \
              ' -n '      + str(n)        + \
              ' -ansatz ' + str(ansatz)   + \
              ' -tol '    + str(tol)      + \
              ' -oGauss ' + str(quadOrder)
        if(nocut == 0):
            opt += ' -cut'
        if(nopos == 1):
            opt += ' -nopos'

        os.system(smallFem + 'q3d' + opt)

        eigTxt = open('eigen.txt', 'r');
        eig = eigTxt.readline()
        print(eig)
        solList[j,i] = (abs(float(eig) - omega) / omega)
        j = j + 1
    i = i + 1

# calculate all slopes and print to terminal
slope = np.divide(np.diff(np.log(solList)), np.diff(np.log(mshList)))
print(slope)

# plot solution
for i in range(0, orderHGrad.size):
    iLabel = '$O(' + str(orderHGrad[i]) + ',' + str(orderHCurl[i]) + ')$'
    plt.loglog(mshList, solList[i,:], '-*', label = iLabel)

if(TE == 1):
    plt.title('Convergence of the TE' + str(n) + str(p) + str(q) + ' mode')
else:
    plt.title('Convergence of the TM' + str(n) + str(p) + str(q) + ' mode')

plt.ylabel('Relative error on the eigenfrequency (-)')
plt.xlabel('Number of mesh elements per wavelength (-)')
plt.legend()

plt.show()

if(nopdf == 0):
    plt.savefig('conv.pdf', format='pdf')
