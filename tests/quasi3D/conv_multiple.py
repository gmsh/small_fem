import numpy
import os
import matplotlib.pyplot as plt
import math
from tikzplotlib import save as tikz_save
import pillboxFreq as pillbox

#Tests the convergence with respect to the mesh refinement for multiple, for 
#each of which the parameter configuration can be chosen freely. 
#Testing is done for a certain order choice.
# 
# Generates plots like Fig. 5.1. in my BSc thesis. 

#Author: Erik Schnaubelt

################################ USER PARAMETERS ###############################



# 0 : ephiStar, 1: param., 2: Dunn, 3: Oh 
ansatz = 3
# parameter configurations for each mode
alphaList = numpy.array([1,1,1,1,1,1]); 
betaList  = numpy.array([2,2,1,1,1,1]); 

# choose pillbox modes TE/TM_{npq}
nList  = numpy.array([0,0,1,1,2,2]);
pList  = numpy.array([1,2,1,2,1,3]); 
qList  = numpy.array([3,1,2,0,1,2]); 
TEList = numpy.array([1,0,1,0,1,0]); 
 
# choose mesh densities 
mshList = numpy.array([1, 2, 4, 8, 16]); 

# choose order HGrad
oHG = 4
# choose order HCurl
oHC = 3

# postpro
nocut  = 1
nopos  = 1
notikz = 0

# choose quadrature order 
quadOrder = 16

# tikz save name 
tikzname = 'test'

################################ Automatic tests ###############################

# get target freq
fList = numpy.zeros((nList.size))
for i in range(0, fList.size): 
    n = nList[i]
    p = pList[i]
    q = qList[i]
    
    if(TEList[i] == 1):
        fList[i] = pillbox.TEfreg(n,p,q)
    else:
        fList[i] = pillbox.TMfreg(n,p,q)

# data 
solList = numpy.zeros((fList.size, mshList.size))

geo = 'geo/trafo.geo'
msh = 'geo/trafo.msh'
dat = 'geo/trafo.dat'
smallFem = '../../build/'

radius = 0.15
height = 0.294
c0 = 299792458
transfinite = 0

# iteration helper
j = 0
#iterate over all frequencies 
for f in fList: 
    
    # get freq
    omega  = 2*math.pi*f
    eigShift  = omega**2
    l      = c0/f

    i      = 0

    #iterate over mesh densities
    for mm in mshList: 
        MSH    = mm
        cl     = l/MSH
        
        # write .dat 
        par_dat = open(dat, 'w');
        par_dat.write('radius = %3.15e;\n' %(radius))
        par_dat.write('height = %3.15e;\n' %(height)) 
        par_dat.write('f = %3.15e;\n' %(f))
        par_dat.write('cl = %3.15e;\n' %(cl))
        par_dat.write('MSH = %3.15e;\n' %(MSH))
        par_dat.write('transfinite = %i;\n' %(transfinite))
        par_dat.close()

        os.system('gmsh '+ geo +' -2 -format msh2 ')


        opt = ' -msh '    + msh                 + \
              ' -beta '   + str(betaList[j])    + \
              ' -alpha '  + str(alphaList[j])   + \
              ' -shift '  + str(eigShift)       + \
              ' -oHG '    + str(oHG)            + \
              ' -oHC   '  + str(oHC)            + \
              ' -n '      + str(nList[j])       + \
              ' -ansatz ' + str(ansatz)         + \
              ' -oGauss ' + str(quadOrder)
        if(nocut == 0):
            opt += ' -cut'
        if(nopos == 1):
            opt += ' -nopos'
        os.system(smallFem + 'q3d' + opt)
      
        eigTxt = open('eigen.txt', 'r');
        eig = eigTxt.readline()
    
        solList[j,i] = (abs(float(eig) - omega) / omega)
        i = i + 1
    j = j + 1

# calculate all slopes and print to terminal 
slope = numpy.divide(numpy.diff(numpy.log(solList)), \
    numpy.diff(numpy.log(mshList)))
print(slope)

# plot solution
for i in range(0, fList.size):
    n = nList[i]
    p = pList[i]
    q = qList[i]
    
    if(TEList[i] == 1):
        labelStr = 'TE' + str(n) + str(p) + str(q) + ' mode' + ', alpha = ' + \
            str(alphaList[i]) + ', beta = ' + str(betaList[i])
        plt.loglog(mshList, solList[i,:], '-x', label = labelStr)
    else:
        labelStr = 'TM' + str(n) + str(p) + str(q) + ' mode' + ', alpha = ' + \
            str(alphaList[i]) + ', beta = ' + str(betaList[i])
        plt.loglog(mshList, solList[i,:], '-x', label = labelStr)

    
plt.ylabel('Relative error of the eigenfrequency (-)')
plt.xlabel('Number of mesh elements per wavelength (-)')
plt.title('Convergence study for multiple eigenvalues (-)') 
plt.legend()

if(notikz == 0): 
    tikz_save(tikzname + '.tikz')
    
plt.show()
