import numpy as np
import os
import matplotlib.pyplot as plt
import math
from tikzplotlib import save as tikz_save
import pillboxFreq as pillbox

# Tests the dependency of the convergence on the parameters alpha and beta for 
# an arbitrarily chosen pillbox mode and a given pair of orders. For n = 0, 
# only betaList needs to be used. 
#The relative error of the eigenfrequency for each alpha/beta pair is plotted 
# over the number of mesh elements per wavelength. 
# 
# Generates plots like Fig. 6.1 or 6.2 in my BSc thesis. 

#Author: Erik Schnaubelt

################################ USER PARAMETERS################################

# choose pillbox mode TE/TM_{npq} 
n  = 2
p  = 1
q  = 2
TE = 1

# choose order HGrad
oHG = 1
# choose order HCurl
oHC = 0

# give pairs of alpha and beta, the first entries build a pair, then the second 
# and so on 
alphaList = np.array([1,2]);
betaList  = np.array([1,2]);

# choose numbers of mesh elements for which to plot 
mshList   = np.array([2,4,8,16,32]); 

# postpro
nocut =1
nopos =1
notikz=0

# choose quadrature order 
quadOrder = 16

# name for tikz save 
tikzName  = 'test' 

############################### Automatized tests###############################

# n = 0: make sure alphaList exists even though not needed 
if (n == 0): 
    alphaList = betaList; 

# get target freq
if(TE == 1):
    f = pillbox.TEfreg(n,p,q)
else:
    f = pillbox.TMfreg(n,p,q)

# data    
solList   = np.zeros((betaList.size,mshList.size))
c0 = 299792458
transfinite = 0
radius = 0.15
height = 0.294

geo = 'geo/trafo.geo'
msh = 'geo/trafo.msh'
dat = 'geo/trafo.dat'
smallFem = '../../build/'

omega    = 2*math.pi*f
eigShift = omega**2
l        = c0/f

i = 0

# iterate over mesh densities
for mm in mshList: 
    j      = 0
    
    # write .dat
    MSH    = mm
    cl     = l/MSH
    par_dat = open(dat, 'w');
    par_dat.write('radius = %3.15e;\n' %(radius))
    par_dat.write('height = %3.15e;\n' %(height)) 
    par_dat.write('f = %3.15e;\n' %(f))
    par_dat.write('cl = %3.15e;\n' %(cl))
    par_dat.write('MSH = %3.15e;\n' %(MSH))
    par_dat.write('transfinite = %i;\n' %(transfinite))
    par_dat.close()

    os.system('gmsh '+ geo +' -2 -format msh2 ')
    
    # iterate over betas
    for bb in betaList:
        
        beta = bb
        
        opt = ' -msh '    + msh                  + \
              ' -beta '   + str(beta)            + \
              ' -alpha '  + str(alphaList[j])    + \
              ' -shift '  + str(eigShift)        + \
              ' -oHG '    + str(oHG)             + \
              ' -oHC   '  + str(oHC)             + \
              ' -n '      + str(n)               + \
              ' -ansatz 1 '                      + \
              ' -oGauss ' + str(quadOrder)
        if(nocut == 0):
            opt += ' -cut'
        if(nopos == 1):
            opt += ' -nopos'
        
        os.system(smallFem + 'q3d' + opt) 
                
        eigTxt = open('eigen.txt', 'r');
        eig = eigTxt.readline()
        print(eig)
        solList[j,i] = (abs(float(eig) - omega) / omega)
        j = j + 1
    i = i + 1

# calculate all slopes and print to terminal 
slope = np.divide(np.diff(np.log(solList)),np.diff(np.log(mshList)))
print(slope)

# plot solution

for i in range(0, betaList.size):
    
    labelStr = '(' + str(alphaList[i]) + ',' + str(betaList[i]) + ')'
    
    plt.loglog(mshList, solList[i,:], label = labelStr)

if(TE == 1): 
    plt.title('Convergence of the TE' + str(n) + str(p) + str(q) + ' mode')
else:
    plt.title('Convergence of the TM' + str(n) + str(p) + str(q) + ' mode')

plt.ylabel('Relative error eigenfrequency (-)')
plt.xlabel('Number of elements per wavelength (-)')
plt.legend()

if(notikz == 0): 
    tikz_save(tikzName + '.tikz')
plt.show()


