import numpy
import os
import matplotlib.pyplot as plt
import math
import csv 
from numpy import genfromtxt
from scipy.sparse import coo_matrix
import pillboxFreq as pillbox

# Plots the spectrum obtained for a 2.5D simulation with PML to model open 
# boundaries. 

#Author: Erik Schnaubelt

################################ USER PARAMETERS #################################

# 0 : ephiStar, 1: param. 
ansatz = 1
beta   = 1
alpha  = 1

f = 3.88249e7; 

# choose mesh densities 
MSH = 2; 
n = 1; 

# choose order HGrad
oHG = 3
# choose order HCurl
oHC = 2

# number of eigenvalues 
nEig = 10; 

# show eigenvectors in gmsh? 
plotGmsh = 1

################################ Automatic tests #################################
ansatzMap = ["ephiStar", "parameterized", "Dunn", "Oh"];

geo = 'pml.geo'
msh = 'pml.msh'
dat = 'pml.dat'
smallFem = '../../build/'

c0 = 299792458
transfinite = 0
d = 5
w = 5
R = 1; 
epsSphere = 10; 



omega     = 2*math.pi*f
eigShift  = omega**2
l         = c0/f
cl        = l/MSH

tol    = 1e-12

i      = 0



# iterate over mesh densities

# write .dat 
par_dat = open(dat, 'w');
par_dat.write('d = %3.15e;\n' %(d))
par_dat.write('w = %3.15e;\n' %(w)) 
par_dat.write('R = %3.15e;\n' %(R)) 
par_dat.write('f = %3.15e;\n' %(f))
par_dat.write('cl = %3.15e;\n' %(cl))
par_dat.write('MSH = %3.15e;\n' %(MSH))
par_dat.write('transfinite = %i;\n' %(transfinite))
par_dat.close()

os.system('gmsh '+ geo +' -2 -format msh2 -order 2')

if(plotGmsh == 1): 
    os.system(smallFem + 'q3dPML -msh '+ msh +' -beta '+ str(beta) + ' -alpha ' + str(alpha) +' -shift ' + str(eigShift) + ' -oHG ' + str(oHG) + ' -oHC ' + str(oHC) + ' -n ' + str(n) + ' -ansatz ' + str(ansatz) + '-ogauss 12 -nEig ' + str(nEig))
else: 
    os.system(smallFem + 'q3dPML -msh '+ msh +' -beta '+ str(beta) + ' -alpha ' + str(alpha) +' -shift ' + str(eigShift) + ' -oHG ' + str(oHG) + ' -oHC ' + str(oHC) + ' -n ' + str(n) + ' -ansatz ' + str(ansatz) + '-ogauss 12 -nEig ' + str(nEig) + ' -nopos')

eigTxt    = open('eigenReal.txt', 'r');
lines     = eigTxt.readlines()
count     = len(lines)
eigReal   = numpy.zeros([count])

# iterate over all numerical solutions 
a = 0
for line in lines:
    eigReal[a] = float(line)
    a          = a+1
    
eigTxt    = open('eigenImag.txt', 'r');
lines     = eigTxt.readlines()
count     = len(lines)
eigImag   = numpy.zeros([count])

# iterate over all numerical solutions 
a = 0
for line in lines:
    eigImag[a] = float(line)
    a          = a+1

plt.scatter(eigReal, eigImag); 
plt.xlabel('Real part (rad / s)'); 
plt.ylabel('Imaginary part (rad / s)'); 
plt.title('Spectrum for 2.5D PML for n = ' + str(n) + ' and ansatz = ' + str(ansatz)); 
plt.show(); 

if(plotGmsh == 1): 
    os.system('gmsh eigenModesCurl' + ansatzMap[ansatz]+ '.msh eigenModesGrad' + ansatzMap[ansatz]+ '.msh eigenModesTxt' + ansatzMap[ansatz] + '.pos'); 


