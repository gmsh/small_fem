close all;
clear all;

f = 12*10^(-6)
epsr = 10;             % Relative electric permittivity of dielectric [-]
c = 299792458*1/sqrt(epsr);                      

%% User data
R    = 1;        % Radius of sphere [m]
n    = 5;             % nth mode [-]
kG   = 0.5;             % Guess for wavenumber [rad/s]1

                      % Relative magnetic permeability is equal to 1 [-]

                      
                      
%% Relative impedance and wavenumber
etar = sqrt(1/epsr);  % Relative impedance  (eta_diel/eta_vacuum) [-]
kr   = sqrt(epsr);    % Relative wavenumber (k_diel/k_vacuum)     [-]

%% Vector k for plotting functions
kk = [1:1e-1:50];

%% Solver options
options = optimset('TolFun', 1e-12, 'MaxIter', 1e4);

%% TE mode
EigTE = @(k)...
         (besselj(n-1/2,       k*R) ./ besselj(n+1/2,       k*R)) - ...
         (besselh(n-1/2, 2, k/kr*R) ./ besselh(n+1/2, 2, k/kr*R)) * etar;

kTE = fsolve(@(k) EigTE(k), kG, options);

%% TM mode
EigTM = @(k)...
         (     n/(k*R)-besselj(n-1/2,       k*R)/besselj(n+1/2,       k*R))-...
         ((n*kr)/(k*R)-besselh(n-1/2, 2, (k/kr)*R)/besselh(n+1/2, 2, (k/kr)*R))/etar;

kTM = fsolve(@(k) EigTM(k), kG, options);

%% Disp
wTER = kTE*c
wTMR = kTM*c
