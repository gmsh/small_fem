/*
GetDP implementation of the ephi* = r ephi ansatz. 

Author: Erik Schnaubelt
*/

Include "pmlEx.dat";

// solver parameters
If(highOrder == 1)
    DefineConstant[C_ = {"-solve -pos -slepc -eps_tol 1e-6 -petsc_prealloc 200", Name "GetDP/9ComputeCommand"}];
EndIf

If(highOrder == 0)
    DefineConstant[C_ = {"-solve -pos -slepc -eps_tol 1e-6 ", Name "GetDP/9ComputeCommand"}];
EndIf

// physical regions (boundary, domain etc.)
Group {
    Nonsymm = Region[101] ;
    Symm = Region[102] ; 
    
     
    Dom  = Region[120] ;
    Bnd = Region[{Nonsymm,Symm}];
    
    
    PMLLowerZZ = Region[121]; 
    PMLLowerRZ = Region[122]; 
    PMLUpperRZ = Region[124];
    PMLUpperZZ = Region[125]; 
    PMLRR      = Region[123];
    Sphere     = Region[126];  
    
    AllPml = Region[{PMLLowerZZ,PMLLowerRZ,PMLUpperRZ,PMLUpperZZ,PMLRR}]; 
    NonPml = Region[{Dom,Sphere}];
    
    DomNPML = Region[{AllPml,NonPml}];
    Tot = Region[{Bnd,DomNPML}];
    
    // this point is only used to dump values 
    PrintPoint = Region[10000];
}

// needed functions, such as materials and definition of the radial coordinate
Function {
    //radial coordinate
    R[] = X[];
    myZ[] = Y[]; 
    
    //materials
    
    nu = 1. / mu0;
    cSq = 1/(mu0*eps0); 
    nuR = 1; 
    epsilonR[Dom] = 1.; 
    epsilonR[Sphere] = epsSphere;
    epsilonR[AllPml] = 1.; 
    
    
    sr[NonPml]     = 1.; 
    sz[NonPml]     = 1.; 
    rTilde[NonPml] = R[]; 
    
    
    
    sr[PMLLowerZZ]     = 1.; 
    sz[PMLLowerZZ]     = Complex[1., - alpha * ((myZ[]  + d)/w)^2]; 
    rTilde[PMLLowerZZ] = R[];
    
    sr[PMLLowerRZ]     = Complex[1., - alpha * ((R[] - d)/w)^2]; 
    sz[PMLLowerRZ]     = Complex[1, - alpha * ((myZ[] + d)/w)^2]; 
    rTilde[PMLLowerRZ] = Complex[R[], - alpha * ((R[] - d)^3)/(3*w^2)];
    
    sr[PMLUpperZZ]     = 1.; 
    sz[PMLUpperZZ]     = Complex[1., - alpha * ((myZ[]  - d)/w)^2]; 
    rTilde[PMLUpperZZ] = R[];
    
    sr[PMLUpperRZ]     = Complex[1., - alpha * ((R[] - d)/w)^2]; 
    sz[PMLUpperRZ]     = Complex[1, - alpha * ((myZ[] - d)/w)^2]; 
    rTilde[PMLUpperRZ] = Complex[R[], - alpha * ((R[] - d)^3)/(3*w^2)]; 
    
    sr[PMLRR]     = Complex[1., - alpha * ((R[] - d)/w)^2]; 
    sz[PMLRR]     = 1.; 
    rTilde[PMLRR] = Complex[R[], - alpha * ((R[] - d)^3)/(3*w^2)];
    
    Lrr[]     = rTilde[]/R[] * sz[]/sr[]; 
    Lphiphi[] = (R[]/rTilde[]) * sz[] * sr[]; 
    Lzz[]     = rTilde[]/R[] * sr[]/sz[]; 
    
    
    epsilon[NonPml] = epsilonR[] * TensorDiag[1.,1.,1.];
    epsilon[AllPml] = epsilonR[] * TensorDiag[Lrr[], Lzz[], Lphiphi[]];
    nu[NonPml] = nuR * TensorDiag[1.,1.,1.];
    nu[AllPml] = nuR * TensorDiag[1.0/Lrr[], 1.0/Lzz[], 1.0/Lphiphi[]];
}

// mappings
Jacobian {
    { Name Jac ;
        Case {
            { Region DomNPML ; Jacobian Vol ; }
            { Region PrintPoint ; Jacobian Vol ; }
        }
    }
}

// number of integration points
Integration {
    { Name I1 ;
        Case {
            { Type Gauss ;
                Case {
                    { GeoElement Triangle ; NumberOfPoints gaussOrder ; }
                }
            }
        }
    }
}

// boundary conditions
Constraint {
    //ephi*
    { Name outOfPlaneConstraint ; Type Assign ;
        Case { 
            { Region Bnd ; Value 0. ; }
        }
    }
    
    //erz
    { Name inPlaneConstraint ; Type Assign ;
        Case { 
            If (n != 0) 
                { Region Bnd ; Value 0. ; }
            EndIf 
            If (n == 0) 
                { Region Nonsymm ; Value 0.; }
            EndIf
        }
    }
}

//function spaces 
FunctionSpace {  
    { Name HCurl ; Type Form1 ;
        BasisFunction {
            { Name eRZPlane ; NameOfCoef eRZCoeff ; Function BF_Edge ;  Support Tot ; Entity EdgesOf[All] ; }
            
        }
        Constraint {
            { NameOfCoef eRZCoeff ; EntityType EdgesOf ; NameOfConstraint inPlaneConstraint ; } 
        }
    }
    
    { Name H1 ; Type Form0 ;
        BasisFunction {
            { Name eStarPlane ; NameOfCoef eStarCoeff ; Function BF_Node ; Support Tot ; Entity NodesOf[All] ; }
        }
        Constraint {
            { NameOfCoef eStarCoeff ; EntityType NodesOf ; NameOfConstraint outOfPlaneConstraint ; }
        }
    }
}

// weak form 
Formulation {
    { Name Form ; Type FemEquation ;
        Quantity {
            { Name eRZ ; Type Local ; NameOfSpace HCurl ; }
            { Name eStar ; Type Local ; NameOfSpace H1 ; }
        }
        
        Equation {
            Galerkin {[CompZZ[nu[]] * R[] * Dof{d eRZ} , {d eRZ} ] ;
                In DomNPML ; Integration I1 ; Jacobian Jac ; }
            
            Galerkin {[nu[] * n^2/(R[]) * Dof{eRZ} , {eRZ} ] ;
                In DomNPML ; Integration I1 ; Jacobian Jac ; }
            
            Galerkin {[nu[] * 1/(R[]) * Dof{d eStar } , {d eStar } ] ;
                In DomNPML ; Integration I1 ; Jacobian Jac ; }    
            
            Galerkin { [nu[] * n/(R[]) * Dof{ eRZ} , {d eStar} ] ;
                In DomNPML ; Integration I1 ; Jacobian Jac ; } 
            
            Galerkin {[nu[] * n/(R[]) * Dof{d eStar} , {eRZ} ] ;
                In DomNPML ; Integration I1 ; Jacobian Jac ; }
            
            Galerkin { DtDtDof[epsilon[] * R[]*(1/cSq) * Dof{eRZ} , {eRZ} ] ;
                In DomNPML ; Integration I1 ; Jacobian Jac ; }
            
            Galerkin { DtDtDof[CompZZ[epsilon[]] * 1/R[] *(1/cSq)* Dof{eStar} , {eStar} ] ;
                In DomNPML ; Integration I1 ; Jacobian Jac ; } 
        }
    }
}

Resolution {
    { Name Reso ;
        System {
            { Name A ; NameOfFormulation Form ;
                Type ComplexValue ;
            }
        }
        Operation {
            CreateDir["output/"] ;
            GenerateSeparate[A] ;
            EigenSolve[A,NbEigenvalues,target,0] ;
            SaveSolutions[A] ;
        }
    }
}

PostProcessing {
    { Name PostPro ; NameOfFormulation Form ;
        Quantity {
            { Name eRZ ; Value{ Local{ [{eRZ}] ; In DomNPML ; Jacobian Jac ; } } }
            { Name eStar ; Value{ Local{ [{eStar}] ; In DomNPML ; Jacobian Jac ; } } }
            { Name EigenValuesReal; Value { Local{ [$EigenvalueReal]  ; In PrintPoint; Jacobian Jac; } } }
            
            /* Possibly interesting postprocessing quantities
                { Name gradEr; Value {Local{[CompX[{Grad eStar}]]; In DomNPML; Jacobian Jac;}}}
                { Name gradEz; Value {Local{[CompY[{Grad eStar}]]; In DomNPML; Jacobian Jac;}}}
                { Name curlEI; Value {Local{[CompZ[{Curl eRZ}]] ; In DomNPML; Jacobian Jac;}}}
                { Name Er; Value {Local{[CompX[{eRZ}]] ; In DomNPML; Jacobian Jac;}}}
                { Name Ez; Value {Local{[CompY[{eRZ}]] ; In DomNPML; Jacobian Jac;}}}
            */
        }
    }
}

PostOperation {
    { Name PostOp ; NameOfPostProcessing PostPro ;
        Operation {
            
            // save eigenvectors and eigenvalues 
            Print [eRZ, OnElementsOf DomNPML, File StrCat["output/eigenvectoreRZ.pos"], EigenvalueLegend];
            Print [eStar, OnElementsOf DomNPML, File StrCat["output/eigenvectore0.pos"], EigenvalueLegend];
            Print[EigenValuesReal, OnElementsOf PrintPoint, Format TimeTable,
            File "EigenValuesReal.txt"];
        
            // dump value of solution along a radial line (on-line plot) 
            Print [ eStar, OnLine {{0,0.294/2,0}{0.15,0.294/2,0}} {10000}, Format SimpleTable, File StrCat["output/mode",Sprintf("%g",n),"Middle.txt"]];
            
            /* Possibly interesting postprocessing quantities
                Print [gradEr, OnElementsOf DomNPML, File StrCat["output/eigenvectoreGradER.pos"], EigenvalueLegend];
                Print [gradEz, OnElementsOf DomNPML, File StrCat["output/eigenvectoreGradEZ.pos"], EigenvalueLegend];
                Print [curlEI, OnElementsOf DomNPML, File StrCat["output/eigenvectoreGradEZ.pos"], EigenvalueLegend];
                Print [Er, OnElementsOf DomNPML, File StrCat["output/eigenvectoreER.pos"], EigenvalueLegend];
                Print [Ez, OnElementsOf DomNPML, File StrCat["output/eigenvectorEZ.pos"], EigenvalueLegend];
            */
        }
    }

}


 
