import numpy
import os
import matplotlib.pyplot as plt
import math
import csv 
from numpy import genfromtxt
from scipy.sparse import coo_matrix
import pillboxFreq as pillbox

#Tests the convergence with respect to the mesh refinement for a single mode and a single parameter configuration for a certain order choice. 

#Author: Erik Schnaubelt

################################ USER PARAMETERS #################################

# 0 : ephiStar, 1: param., 2: Dunn, 3: Oh 
ansatz =1 
beta   = 1
alpha  = 0

f = 3.88249e7; 

# choose mesh densities 
mshList = numpy.array([25]); 
n = 1; 

# choose order HGrad
oHG = 3
# choose order HCurl
oHC = 2

################################ Automatic tests #################################



print(f) 
# data
solList = numpy.zeros(mshList.shape)

geo = 'pml.geo'
msh = 'pml.msh'
dat = 'pml.dat'
smallFem = '../../build/'

c0 = 299792458
transfinite = 0
d = 5
w = 5
R = 1; 
epsSphere = 10; 
alpha = 5; 



omega     = 2*math.pi*f
eigShift  = omega**2
l         = c0/f

tol    = 1e-12

i      = 0

# iterate over mesh densities
for mm in mshList: 
    MSH    = mm
    cl     = l/MSH
    
    # write .dat 
    par_dat = open(dat, 'w');
    par_dat.write('d = %3.15e;\n' %(d))
    par_dat.write('w = %3.15e;\n' %(w)) 
    par_dat.write('R = %3.15e;\n' %(R)) 
    par_dat.write('f = %3.15e;\n' %(f))
    par_dat.write('cl = %3.15e;\n' %(cl))
    par_dat.write('MSH = %3.15e;\n' %(MSH))
    par_dat.write('transfinite = %i;\n' %(transfinite))
    par_dat.close()

    os.system('gmsh '+ geo +' -2 -format msh2 -order 2')
        
    os.system(smallFem + 'q3dPML -msh '+ msh +' -beta '+ str(beta) + ' -alpha ' + str(alpha) +' -shift ' + str(eigShift) + ' -oHG ' + str(oHG) + ' -oHC ' + str(oHC) + ' -n ' + str(n) + ' -ansatz ' + str(ansatz) + '-ogauss 12') 
      
      
    eigTxt = open('eigen.txt', 'r');
    eig = eigTxt.readline()
    print(eig)
   
            
    solList[i] = (abs(float(eig) - omega) / omega)
    print(solList[i])
    
    i = i + 1

# calculate all slopes and print to terminal 
slope = numpy.divide(numpy.diff(numpy.log(solList)),numpy.diff(numpy.log(mshList)))
print(slope)

# plot solution
plt.loglog(mshList, solList)

if(TE == 1): 
    plt.title('Convergence of the TE' + str(n) + str(p) + str(q) + ' mode')
else:
    plt.title('Convergence of the TM' + str(n) + str(p) + str(q) + ' mode')


plt.ylabel('Relative error eigenfrequency (-)')
plt.xlabel('Number of elements per wavelength (-)')
plt.show()


