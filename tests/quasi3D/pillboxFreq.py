import numpy as np
import scipy
from scipy import special 

#Defines two functions to calculate the analytical eigenfrequencies for the pillbox cavity. One is for
#TE and one for TM modes. 

#Author: Erik Schnaubelt

# data
h = 0.294
a = 0.15
c = 299792458

# calculate TM modes
def TMfreg(m,n,p): 
   
    kzp = p*np.pi/h;
    k = scipy.special.jn_zeros(m, n+1)
    kpnl = k[n-1]/a

    w = c*np.sqrt((kpnl**2 + kzp**2))
    f = w/(2*np.pi)

    return f
    
#calculate TE modes
def TEfreg(m,n,p): 

    kzp = p*np.pi/h;
    k = scipy.special.jnp_zeros(m, n+1)
    kpnl = k[n-1]/a

    w = c*np.sqrt((kpnl**2 + kzp**2))
    f = w/(2*np.pi)

    return f
