import numpy as np
import os
import matplotlib.pyplot as plt
import math
from tikzplotlib import save as tikz_save
import pillboxFreq as pillbox

# Compares the convergence of the rel. error of the eigenfrequency with respect 
# to the mesh refinement between the parameterized and the ephiStar ansatz. 
# Tests are done depending on the order of the Gauss quadrature. 
# 
# Generates plots like Fig. 6.7. in my BSc thesis. 

#Author: Erik Schnaubelt

################################ USER PARAMETERS ###############################


# ansatz 0: eStar, 1: Param, 2: Dunn, 3: Oh 
ansatz1 = 0; 
ansatz2 = 3;

# parameters
beta   = 1
alpha  = 1 

# choose pillbox mode TE/TM_{npq} 
n  = 2
p  = 1
q  = 1
TE = 1

# choose integration orders 
gaussList = np.array([3,4,5,6,7,8,9]);

# choose order HGrad
oHG = 3
# choose order HCurl
oHC = 2

# postpro
nocut  = 1
nopos  = 1
notikz = 0

# choose mesh densities 
mshList = np.array([2, 4, 8, 16, 32]); 

# tikz save name 
tikzname = 'test'

################################ Automatic tests ###############################

# get target freq
if(TE == 1):
    f = pillbox.TEfreg(n,p,q)
else:
    f = pillbox.TMfreg(n,p,q)

# data
solListAnsatz2 = np.zeros((gaussList.size,mshList.size))
solListAnsatz1 = np.zeros((gaussList.size,mshList.size))

geo = 'geo/trafo.geo'
msh = 'geo/trafo.msh'
dat = 'geo/trafo.dat'
smallFem = '../../build/'

c0 = 299792458
transfinite = 0
radius = 0.15
height = 0.294

omega    = 2*math.pi*f
eigShift = omega**2
l        = c0/f

# iterate over mesh densities
i = 0
for mm in mshList: 
    j      = 0
    MSH    = mm
    cl     = l/MSH
    
    # write .dat 
    par_dat = open(dat, 'w');
    par_dat.write('radius = %3.15e;\n' %(radius))
    par_dat.write('height = %3.15e;\n' %(height)) 
    par_dat.write('f = %3.15e;\n' %(f))
    par_dat.write('cl = %3.15e;\n' %(cl))
    par_dat.write('MSH = %3.15e;\n' %(MSH))
    par_dat.write('transfinite = %i;\n' %(transfinite))
    par_dat.close()

    os.system('gmsh '+ geo +' -2 -format msh2 ')
    
    # iterate over integration points 
    for gauss in gaussList:
        
        opt = ' -msh '    + msh           + \
              ' -beta '   + str(beta)     + \
              ' -alpha '  + str(alpha)    + \
              ' -shift '  + str(eigShift) + \
              ' -oHG '    + str(oHG)      + \
              ' -oHC   '  + str(oHC)      + \
              ' -n '      + str(n)        + \
              ' -ansatz ' + str(ansatz1)  + \
              ' -oGauss ' + str(gauss)
        if(nocut == 0):
            opt += ' -cut'
        if(nopos == 1):
            opt += ' -nopos'

        os.system(smallFem + 'q3d' + opt) 
       
        eigTxt = open('eigen.txt', 'r');
        eig = eigTxt.readline()
        print(eig)
        solListAnsatz1[j,i] = (abs(float(eig) - omega) / omega)
        
        opt = ' -msh '    + msh           + \
              ' -beta '   + str(beta)     + \
              ' -alpha '  + str(alpha)    + \
              ' -shift '  + str(eigShift) + \
              ' -oHG '    + str(oHG)      + \
              ' -oHC   '  + str(oHC)      + \
              ' -n '      + str(n)        + \
              ' -ansatz ' + str(ansatz2)  + \
              ' -oGauss ' + str(gauss)
        if(nocut == 0):
            opt += ' -cut'
        if(nopos == 1):
            opt += ' -nopos'
            
        os.system(smallFem + 'q3d' + opt) 

        eigTxt = open('eigen.txt', 'r');
        eig = eigTxt.readline()
        print(eig)
        solListAnsatz2[j,i] = (abs(float(eig) - omega) / omega)
        
        j = j + 1
        
    i = i + 1

# calculate all slopes and print to terminal 
slopeAnsatz1 = np.divide(np.diff(np.log(solListAnsatz1)), \
    np.diff(np.log(mshList)))
print('Slope eStar Ansatz: ' + str(slopeAnsatz1))

slopeAnsatz2 = np.divide(np.diff(np.log(solListAnsatz2)), \
    np.diff(np.log(mshList)))
print('Slope parameterized Ansatz: ' + str(slopeAnsatz2))


# plot solution
for i in range(0, gaussList.size):
    label1 = 'gauss = ' + str(gaussList[i])+ ' ansatz = ' + str(ansatz1)
    plt.loglog(mshList, solListAnsatz1[i,:], '--', label = label1)
    label2 = 'gauss = ' + str(gaussList[i]) + ' ansatz = ' + str(ansatz2)
    plt.loglog(mshList, solListAnsatz2[i,:], '-x',label = label2)

plt.ylabel('Relative error eigenfrequency (-)')
plt.xlabel('Number of elements per wavelength (-)')
plt.legend()

if(notikz == 0): 
    tikz_save(tikzname + '.tikz')
    
plt.show()


