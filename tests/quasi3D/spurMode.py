import numpy
import os
import matplotlib.pyplot as plt
import math
from tikzplotlib import save as tikz_save

# Plots part of the spectrum for a certain n. The interval of frequencies 
# (= part of the spectrum) is defined by min and max values and 
# a target frequency with min < f < max. Until now, analytical reference 
# frequencies have to be added by hand. 
# 
# Generates plots like Fig. 5.4. in my BSc thesis. 

#Author: Erik Schnaubelt

################################ USER PARAMETERS ###############################

# 0 : ephiStar, 1: param., 2: Dunn, 3: Oh 
ansatz = 0
beta   = 1
alpha  = 1

# minimum and maximum frequency of the spectrum
minF = 1.0e9
maxF = 2.0e9

# target frequency [in interval (minF, maxF)]
f   = 1408419742.797339

# choose order HGrad
oHG = 3
# choose order HCurl
oHC = 2

# choose mode
n   = 1

# choose mesh densities 
mshList = numpy.array([1,2,4,8,16]);

# choose number of eigenvalues (same size as mshList!)
nEigList   = [20,50,100,100,100]; 

# 0: pillbox cavity 1: tesla cavity  
problem = 0

# postpro
nocut  = 1
nopos  = 1
notikz = 0

# choose quadrature order 
quadOrder = 16

# for tesla cavity: order of mesh 
orderMesh = 1

# tikz save name 
tikzname = 'test'

################################ Automatic tests ###############################

# data 
if(problem == 0): 
    geo = 'geo/trafo.geo'
    msh = 'geo/trafo.msh'
    dat = 'geo/trafo.dat'
    smallFem = '../../build/'
else: 
    geo = 'geo/geo_tesla_1_cell_midcup.geo'
    msh = 'geo/geo_tesla_1_cell_midcup.msh'
    dat = 'geo/geo_tesla_1_cell_midcup.dat'
    smallFem = '../../build/'

radius = 0.15
height = 0.294
eps0 = 8.854187818e-12
mu0 = 4e-7 * math.pi
c0 = 299792458

transfinite = 0
     
omega  = 2*math.pi*f
eigShift  = omega**2
l      = c0/f

# iterate over mesh densities 
i      = 0
for mm in mshList: 
    MSH    = mm
    cl     = l/MSH
    
    # write .dat
    
    if(problem == 0): 
        par_dat = open(dat, 'w');
        par_dat.write('radius = %3.15e;\n' %(radius))
        par_dat.write('height = %3.15e;\n' %(height)) 
        par_dat.write('f = %3.15e;\n' %(f))
        par_dat.write('cl = %3.15e;\n' %(cl))
        par_dat.write('MSH = %3.15e;\n' %(MSH))
        par_dat.write('transfinite = %i;\n' %(transfinite))
        par_dat.close()
    else: 
        par_dat = open(dat, 'w');
        par_dat.write('F = %3.15e;\n' %(f))
        par_dat.write('N = %3.15e;\n' %(mm))
        par_dat.write('O = %i;\n' %(orderMesh))
        par_dat.close()
        

    os.system('gmsh '+ geo +' -2 -format msh2 ')
    
    opt = ' -msh '    + msh                  + \
              ' -beta '   + str(beta)        + \
              ' -alpha '  + str(alpha)       + \
              ' -shift '  + str(eigShift)    + \
              ' -oHG '    + str(oHG)         + \
              ' -oHC   '  + str(oHC)         + \
              ' -n '      + str(n)           + \
              ' -ansatz ' + str(ansatz)      + \
              ' -nEig '   + str(nEigList[i]) + \
              ' -oGauss ' + str(quadOrder)
          
    if(nocut == 0):
            opt += ' -cut'
    if(nopos == 1):
            opt += ' -nopos'
    
    os.system(smallFem + 'q3d' + opt) 
              

    
    eigTxt = open('eigen.txt', 'r');
    lines = eigTxt.readlines()
    count = len(lines)
    eig   = numpy.zeros([count])
    
    # iterate over all numerical solutions 
    a = 0
    for line in lines:
        eig[a] = float(line)
        a        = a+1
    
    # only keep solutions in given interval 
    eigFilter = eig[eig > 2*math.pi*minF]
    solution  = eigFilter[eigFilter < 2*math.pi*maxF]
      
    # plot data 
    plt.semilogx(mm*numpy.ones([len(solution)]), solution,'*')    
    i = i + 1
    

# Example: add analytical solution as a horizontal line (until now, only 
# possible to do it manually)    
# E.g. TM112
plt.hlines(2*math.pi*f,mshList[0],mshList[len(mshList)-1])

plt.ylabel('Angular eigenfrequency (rad/s)')
plt.xlabel('Number of mesh elements per wavelength (-)')
plt.title('Part of the spectrum for n = ' + str(n) + ', O(' + str(oHG) + ', '+ \
    str(oHC) + '), ansatz ' + str(ansatz) + ' and problem = ' + str(problem)) 

if(notikz == 0): 
    tikz_save(tikzname + '.tikz')
    
plt.show()
