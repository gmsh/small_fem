import numpy
import os
import matplotlib.pyplot as plt
import math
import csv
from numpy import genfromtxt
from scipy.sparse import coo_matrix
import pillboxFreq as pillbox

#Tests the convergence with respect to the mesh refinement
#for the tesla cavity.

#Author: Erik Schnaubelt

################################ USER PARAMETERS ###############################

# Q3D ansatz: { 0 : ephiStar, 1: param., 2: Dunn, 3: Oh }
ansatz = 3

# axi mode
n = 1

# target frequency [Hz]
f     = 1.016816396651845551e10 ## For eigenvalue solver
fMesh = 1e9                     ## For mesher (potentially lower, to save time)

# mesh densities and order
mshList   = numpy.array([3,4,5,6,7,8])

orderMesh = 2

# FE order (HGrad and HCurl)
oHG = 2
oHC = 1

# postpro
nocut=1
nopos=1
nopdf=0

################################ Automatic tests ###############################

# data
geo      = 'geo/geo_tesla_1_cell_midcup.geo'
msh      = 'geo/geo_tesla_1_cell_midcup.msh'
smallFem = '../../build/'

omega    = 2*numpy.pi*f
eigShift = omega**2
solList  = []

# iterate over mesh densities
for mm in mshList:
    print(str(mm))
    os.system('gmsh ' + geo +
              ' -2 -format msh2 '           + \
              ' -setnumber F ' + str(fMesh) + \
              ' -setnumber N ' + str(mm)    + \
              ' -setnumber O ' + str(orderMesh))

    os.system(smallFem    + 'q3d '        + \
              ' -msh '    + msh           + \
              ' -shift '  + str(eigShift) + \
              ' -oHG '    + str(oHG)      + \
              ' -oHC '    + str(oHC)      + \
              ' -n '      + str(n)        + \
              ' -ansatz ' + str(ansatz)   + \
              ' -nopos ')

    eig = numpy.loadtxt('eigen.txt') / (2*numpy.pi)
    idx = numpy.nanargmin(abs(eig-f))
    solList.append(eig[idx])
    print('Closest eigenvalue: %e' %eig[idx])
    print('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')

# plot solution
for mm in range(len(mshList)):
    print('%e;%.16e' %(mshList[mm], solList[mm]))
