APhi     = dlmread("cutPhi.csv");
AGradPhi = dlmread("cutGradPhi.csv");
ARZ      = dlmread("cutRZ.csv");
beta     = 1;
alpha    = 1;
n        = 1;

APhi     =     APhi(4:end, :);
AGradPhi = AGradPhi(4:end, :);
ARZ      =      ARZ(4:end, :);

r        = APhi(:, 1);
ePhiReal = APhi(:, 4) .* r.^(beta-1);
ePhiImag = APhi(:, 5) .* r.^(beta-1);

gradEPhiRReal = (AGradPhi(:, 4)-(1-beta).*r.^(-beta).*ePhiReal) ./ r.^(1-beta);
gradEPhiRImag = (AGradPhi(:, 5)-(1-beta).*r.^(-beta).*ePhiImag) ./ r.^(1-beta);
gradEPhiZReal = (AGradPhi(:, 6))                                ./ r.^(1-beta);
gradEPhiZImag = (AGradPhi(:, 7))                                ./ r.^(1-beta);

eRReal = (r.^(alpha).*ARZ(:, 4) - ePhiReal - r .* gradEPhiRReal) ./ n;
eRImag = (r.^(alpha).*ARZ(:, 5) - ePhiImag - r .* gradEPhiRImag) ./ n;
eZReal = (r.^(alpha).*ARZ(:, 6)            - r .* gradEPhiZReal) ./ n;
eZImag = (r.^(alpha).*ARZ(:, 7)            - r .* gradEPhiZImag) ./ n;

phiMod = sqrt(ePhiReal.^2 + ePhiImag.^2);
rMod   = sqrt(  eRReal.^2 +   eRImag.^2);
zMod   = sqrt(  eZReal.^2 +   eZImag.^2);

eRRealSlope   = diff(eRReal)   ./ diff(r);
eZRealSlope   = diff(eZReal)   ./ diff(r);
ePhiRealSlope = diff(ePhiReal) ./ diff(r);

eRImagSlope   = diff(eRImag)   ./ diff(r);
eZImagSlope   = diff(eZImag)   ./ diff(r);
ePhiImagSlope = diff(ePhiImag) ./ diff(r);

rModSlope     = diff(rMod)     ./ diff(r);
zModSlope     = diff(zMod)     ./ diff(r);
phiModSlope   = diff(phiMod)   ./ diff(r);

figure;
hold on;
title('Field (re/im)');
plot(r, eRReal  , '-r');
plot(r, eZReal  , '-b');
plot(r, ePhiReal, '-k');
legend({'r', 'z', 'phi'})
plot(r, eRImag  , '--r');
plot(r, eZImag  , '--b');
plot(r, ePhiImag, '--k');
grid;

figure;
hold on;
title('Field (modulus)');
plot(r, rMod  , '-r');
plot(r, zMod  , '-b');
plot(r, phiMod, '-k');
legend({'r', 'z', 'phi'})
grid;

figure;
hold on;
title('Slope (re/im)');
plot(r(1:end-1), eRRealSlope  , '-r');
plot(r(1:end-1), eZRealSlope  , '-b');
plot(r(1:end-1), ePhiRealSlope, '-k');
legend({'r', 'z', 'phi'})
plot(r(1:end-1), eRImagSlope  , '--r');
plot(r(1:end-1), eZImagSlope  , '--b');
plot(r(1:end-1), ePhiImagSlope, '--k');
grid;

figure;
hold on;
title('Slope (modulus)');
plot(r(1:end-1), rModSlope  , '-r');
plot(r(1:end-1), zModSlope  , '-b');
plot(r(1:end-1), phiModSlope, '-k');
legend({'r', 'z', 'phi'})
grid;
