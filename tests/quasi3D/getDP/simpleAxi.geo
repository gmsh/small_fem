/*
This file contains the geometry information used to setup the axisymmetric GetDP quasi3D solver in simpleAxi.pro. 

Author: Erik Schnaubelt
*/

Include "simpleAxi.dat";

p[] += newp ; Point(newp) = {0, 0, 0,cl} ;
p[] += newp ; Point(newp) = { radius, 0, 0,cl} ;
p[] += newp ; Point(newp) = { radius,  height, 0,cl} ;
p[] += newp ; Point(newp) = {0,  height, 0,cl} ;
Printf("", p[]);
l[] += newl ; Line(newl) = {p[0], p[1]} ;
l[] += newl ; Line(newl) = {p[1], p[2]} ;
l[] += newl ; Line(newl) = {p[2], p[3]} ;
l[] += newl ; Line(newl) = {p[3], p[0]} ;
Printf("", l[]);
ll = newll ; Curve Loop(ll) = {l[0], l[1], l[2], l[3]} ;
s = news ; Plane Surface(s) = {ll} ;

If(transfinite == 1)
    Transfinite Curve{1,3} = MSH;
    Transfinite Curve{2,4} = MSH;
    Transfinite Surface{s};
EndIf

Physical Line("NONSYMM",101) = {l[0],l[1],l[2]} ;
Physical Line("SYMM",102) = {l[3]} ;

Physical Surface("DOM",120) = {s} ;
Physical Point(10000)  = {2};             // Print point
