/*
GetDP implementation of the ephi* = r ephi ansatz. 

Author: Erik Schnaubelt
*/

Include "simpleAxi.dat";

// solver parameters
If(highOrder == 1)
    DefineConstant[C_ = {"-solve -pos -slepc -eps_tol 1e-12 -petsc_prealloc 200", Name "GetDP/9ComputeCommand"}];
EndIf

If(highOrder == 0)
    DefineConstant[C_ = {"-solve -pos -slepc -eps_tol 1e-12 ", Name "GetDP/9ComputeCommand"}];
EndIf

// physical regions (boundary, domain etc.)
Group {
    Nonsymm = Region[101] ;
    Symm = Region[102] ; 
    Dom  = Region[120] ;
    Bnd = Region[{Nonsymm,Symm}];
    Tot = Region[{Bnd,Dom}] ;
    
    // this point is only used to dump values 
    PrintPoint = Region[10000];
}

// needed functions, such as materials and definition of the radial coordinate
Function {
    //radial coordinate
    R[] = X[];
    
    //materials
    
    nu = 1. / mu0;
    cSq = 1/(mu0*eps0); 
    nuR = 1; 
    epsilonR = 1; 
    epsilon[] = epsilonR * TensorDiag[1., 1., 1.];
    nu[] = nuR * TensorDiag[1., 1., 1.];
}

// mappings
Jacobian {
    { Name Jac ;
        Case {
            { Region Dom ; Jacobian Vol ; }
            { Region PrintPoint ; Jacobian Vol ; }
        }
    }
}

// number of integration points
Integration {
    { Name I1 ;
        Case {
            { Type Gauss ;
                Case {
                    { GeoElement Triangle ; NumberOfPoints gaussOrder ; }
                }
            }
        }
    }
}

// boundary conditions
Constraint {
    //ephi*
    { Name outOfPlaneConstraint ; Type Assign ;
        Case { 
            { Region Bnd ; Value 0. ; }
        }
    }
    
    //erz
    { Name inPlaneConstraint ; Type Assign ;
        Case { 
            If (n != 0) 
                { Region Bnd ; Value 0. ; }
            EndIf 
            If (n == 0) 
                { Region Nonsymm ; Value 0.; }
            EndIf
        }
    }
}

//function spaces 
FunctionSpace {  
    { Name HCurl ; Type Form1 ;
        BasisFunction {
            { Name eRZPlane ; NameOfCoef eRZCoeff ; Function BF_Edge ;  Support Tot ; Entity EdgesOf[All] ; }
            
            If(highOrder == 1)
                //see http://onelab.info/pipermail/getdp/2010/001297.html
                { Name eRZPlane2; NameOfCoef eRZCoeff2; Function BF_Edge_2E;  Support Tot; Entity EdgesOf[All]; }
            EndIf
            
        }
        Constraint {
            { NameOfCoef eRZCoeff ; EntityType EdgesOf ; NameOfConstraint inPlaneConstraint ; } 
            
            If(highOrder == 1)
                { NameOfCoef eRZCoeff2 ; EntityType EdgesOf ; NameOfConstraint inPlaneConstraint ; } 
            EndIf
        }
    }
    
    { Name H1 ; Type Form0 ;
        BasisFunction {
            { Name eStarPlane ; NameOfCoef eStarCoeff ; Function BF_Node ; Support Tot ; Entity NodesOf[All] ; }
            If(highOrder == 1)
                { Name eStarPlane2 ; NameOfCoef eStarCoeff2 ; Function BF_Node_2E ; Support Tot ; Entity EdgesOf[All] ; }
            EndIf
        }
        Constraint {
            { NameOfCoef eStarCoeff ; EntityType NodesOf ; NameOfConstraint outOfPlaneConstraint ; }
            If(highOrder == 1)   
                { NameOfCoef eStarCoeff2 ; EntityType EdgesOf ; NameOfConstraint outOfPlaneConstraint ; }
            EndIf
        }
    }
}

// weak form 
Formulation {
    { Name Form ; Type FemEquation ;
        Quantity {
            { Name eRZ ; Type Local ; NameOfSpace HCurl ; }
            { Name eStar ; Type Local ; NameOfSpace H1 ; }
        }
        
        Equation {
            Galerkin {[nu[] * R[] * Dof{d eRZ} , {d eRZ} ] ;
                In Dom ; Integration I1 ; Jacobian Jac ; }
            
            Galerkin {[nu[] * n^2/(R[]) * Dof{eRZ} , {eRZ} ] ;
                In Dom ; Integration I1 ; Jacobian Jac ; }
            
            Galerkin {[nu[] * 1/(R[]) * Dof{d eStar } , {d eStar } ] ;
                In Dom ; Integration I1 ; Jacobian Jac ; }    
            
            Galerkin { [nu[] * n/(R[]) * Dof{ eRZ} , {d eStar} ] ;
                In Dom ; Integration I1 ; Jacobian Jac ; } 
            
            Galerkin {[nu[] * n/(R[]) * Dof{d eStar} , {eRZ} ] ;
                In Dom ; Integration I1 ; Jacobian Jac ; }
            
            Galerkin { DtDtDof[epsilon[] * R[]*(1/cSq) * Dof{eRZ} , {eRZ} ] ;
                In Dom ; Integration I1 ; Jacobian Jac ; }
            
            Galerkin { DtDtDof[CompZZ[epsilon[]] * 1/R[] *(1/cSq)* Dof{eStar} , {eStar} ] ;
                In Dom ; Integration I1 ; Jacobian Jac ; } 
        }
    }
}

Resolution {
    { Name Reso ;
        System {
            { Name A ; NameOfFormulation Form ;
                Type ComplexValue ;
            }
        }
        Operation {
            CreateDir["output/"] ;
            GenerateSeparate[A] ;
            EigenSolve[A,NbEigenvalues,target,0] ;
            SaveSolutions[A] ;
        }
    }
}

PostProcessing {
    { Name PostPro ; NameOfFormulation Form ;
        Quantity {
            { Name eRZ ; Value{ Local{ [{eRZ}] ; In Dom ; Jacobian Jac ; } } }
            { Name eStar ; Value{ Local{ [{eStar}] ; In Dom ; Jacobian Jac ; } } }
            { Name EigenValuesReal; Value { Local{ [$EigenvalueReal]  ; In PrintPoint; Jacobian Jac; } } }
            
            /* Possibly interesting postprocessing quantities
                { Name gradEr; Value {Local{[CompX[{Grad eStar}]]; In Dom; Jacobian Jac;}}}
                { Name gradEz; Value {Local{[CompY[{Grad eStar}]]; In Dom; Jacobian Jac;}}}
                { Name curlEI; Value {Local{[CompZ[{Curl eRZ}]] ; In Dom; Jacobian Jac;}}}
                { Name Er; Value {Local{[CompX[{eRZ}]] ; In Dom; Jacobian Jac;}}}
                { Name Ez; Value {Local{[CompY[{eRZ}]] ; In Dom; Jacobian Jac;}}}
            */
        }
    }
}

PostOperation {
    { Name PostOp ; NameOfPostProcessing PostPro ;
        Operation {
            
            // save eigenvectors and eigenvalues 
            Print [eRZ, OnElementsOf Dom, File StrCat["output/eigenvectoreRZ.pos"], EigenvalueLegend];
            Print [eStar, OnElementsOf Dom, File StrCat["output/eigenvectore0.pos"], EigenvalueLegend];
            Print[EigenValuesReal, OnElementsOf PrintPoint, Format TimeTable,
            File "EigenValuesReal.txt"];
        
            // dump value of solution along a radial line (on-line plot) 
            Print [ eStar, OnLine {{0,0.294/2,0}{0.15,0.294/2,0}} {10000}, Format SimpleTable, File StrCat["output/mode",Sprintf("%g",n),"Middle.txt"]];
            
            /* Possibly interesting postprocessing quantities
                Print [gradEr, OnElementsOf Dom, File StrCat["output/eigenvectoreGradER.pos"], EigenvalueLegend];
                Print [gradEz, OnElementsOf Dom, File StrCat["output/eigenvectoreGradEZ.pos"], EigenvalueLegend];
                Print [curlEI, OnElementsOf Dom, File StrCat["output/eigenvectoreGradEZ.pos"], EigenvalueLegend];
                Print [Er, OnElementsOf Dom, File StrCat["output/eigenvectoreER.pos"], EigenvalueLegend];
                Print [Ez, OnElementsOf Dom, File StrCat["output/eigenvectorEZ.pos"], EigenvalueLegend];
            */
        }
    }

}


