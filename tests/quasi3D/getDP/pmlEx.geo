/*
This file contains the geometry information used to setup the axisymmetric GetDP quasi3D solver in simpleAxi.pro. 

Author: Erik Schnaubelt
*/

Include "pmlEx.dat";

//actual domain
Point(0) = {0, -d, 0,cl} ;
Point(1) = { d, -d, 0,cl} ;
Point(2) = { d,  d, 0,cl} ;
Point(3) = {0,  d, 0,cl} ;
Line(0) = {0, 1} ;
Line(1) = {1, 2} ;
Line(2) = {2, 3} ;

// big boundary
Point(4) = {0, -d - w, 0,cl} ;
Point(5) = { d + w, - d - w, 0,cl} ;
Point(6) = { d + w,  d + w, 0,cl} ;
Point(7) = {0,  d + w, 0,cl} ;


//lower yy
Point(8) = {d, -d - w, 0,cl} ;
Line(8) = {1, 8} ;
Line(9) = {8, 4} ;
Line(10) = {4 ,0} ;
Curve Loop(1004) = {0,8,9,10} ;
Plane Surface(1005) = {1004} ;

//lower xy
Point(9) = {d+w, -d, 0,cl} ;
Line(11) = {9,1}; 
Line(12) = {5,9}; 
Line(13) = {8,5}; 
Curve Loop(1006) = {8,13,12,11} ;
Plane Surface(1007) = {1006} ;

//upper xy
Point(10) = {d+w, d, 0,cl} ;
Point(11) = {d, d+w, 0,cl} ;

Line(14) = {2,10}; 
Line(15) = {10,6}; 
Line(16) = {6,11};
Line(17) = {11,2}; 
Curve Loop(1008) = {14,15,16,17} ;
Plane Surface(1009) = {1008} ;

// upper yy 
Line(18) = {7,11}; 
Line(19) = {3,7}; 
Curve Loop(1010) = {17,18,19,2} ;
Plane Surface(1011) = {1010} ;

// xx 
Line(20) = {10,9}; 

Curve Loop(1012) = {11,1,14,20} ;
Plane Surface(1013) = {1012} ;

// circle 

Point(12) = {0,0,0,cl}; 
Point(13) = {0,-R,0,cl}; 
Point(14) = {R,0,0,cl}; 
Point(15) = {0,R,0,cl}; 

Circle(21) = {14, 12, 13};
Circle(22) = {15, 12, 14};
Line(23) = {13,12};
Line(26) = {12,15};

Curve Loop(1014) = {21,22,26,23} ;
Plane Surface(1015) = {1014} ;

Line(24) = {3,15}; 
Line(25) = {13,0}; 

Curve Loop(1016) = {0,1,2,24,21,22,25} ;
Plane Surface(1017) = {1016} ;





Physical Line("SYMM",102) = {10,25,23,26,24,19} ;
Physical Line("NONSYMM",101) = {18,16,15,20,12,13,9};
Physical Surface("DOM",120) = {1017} ;
Physical Surface("lowerYY",121) = {1005} ;
Physical Surface("lowerXY",122) = {1007} ;
Physical Surface("XX",123) = {1013} ;
Physical Surface("upperXY",124) = {1009} ;
Physical Surface("upperYY",125) = {1011} ;
Physical Surface("sphere",126) = {1015} ;





Physical Point(10000)  = {2};             // Print point
