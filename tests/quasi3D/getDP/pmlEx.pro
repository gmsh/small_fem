/*
GetDP implementation of the ephi* = r ephi ansatz with PML to model open boundaries.  

Author: Erik Schnaubelt
*/

Include "pmlEx.dat";

// solver parameters

DefineConstant[C_ = {"-solve -pos -slepc -eps_tol 1e-6 ", Name "GetDP/9ComputeCommand"}];


// physical regions (boundary, domain etc.)
Group {
    Nonsymm = Region[101] ;
    Symm = Region[102] ; 
    
    // actual domains 
    
    Dom  = Region[120] ;
    Bnd = Region[{Nonsymm,Symm}];
    Sphere     = Region[126];  
    
    // PML 
    PMLLowerZZ = Region[121]; 
    PMLLowerRZ = Region[122]; 
    PMLUpperRZ = Region[124];
    PMLUpperZZ = Region[125]; 
    PMLRR      = Region[123];
    
    // all pmls in one region, all others in another 
    AllPml = Region[{PMLLowerZZ,PMLLowerRZ,PMLUpperRZ,PMLUpperZZ,PMLRR}]; 
    NonPml = Region[{Dom,Sphere}];
    
    //all surface regions (so without boundary curves) 
    DomNPML = Region[{AllPml,NonPml}];
    
    // every region 
    Tot = Region[{Bnd,DomNPML}];
    
    // this point is only used to dump values 
    PrintPoint = Region[10000];
}

// needed functions, such as materials and definition of the radial coordinate
Function {
    //radial coordinate
    R[] = X[];
    // z-coordinate: by definition y-coordinate 
    myZ[] = Y[]; 
    
    //materials
    
    cmpl = 1.; 
    
    nu = 1. / mu0;
    cSq = 1./(mu0*eps0); 
    nuR = 1.; 
    epsilonR[Dom] = 1.; 
    epsilonR[Sphere] = epsSphere;
    epsilonR[AllPml] = 1.; 
    
    sr[NonPml]     = 1.; 
    sz[NonPml]     = 1.; 
    sPhiTilde[NonPml] = 1.;
    
    sr[PMLLowerZZ]     = 1.; 
    sz[PMLLowerZZ]     = Complex[1., cmpl]; 
    sPhiTilde[PMLLowerZZ] = 1.;
    
    sr[PMLLowerRZ]     = Complex[1., cmpl]; 
    sz[PMLLowerRZ]     = Complex[1, cmpl]; 
    sPhiTilde[PMLLowerRZ] = Complex[1., cmpl];
    
    sr[PMLUpperZZ]     = 1.; 
    sz[PMLUpperZZ]     = Complex[1., cmpl]; 
    sPhiTilde[PMLUpperZZ] = 1.;
    
    sr[PMLUpperRZ]     = Complex[1., cmpl]; 
    sz[PMLUpperRZ]     = Complex[1, cmpl]; 
    sPhiTilde[PMLUpperRZ] = Complex[1., cmpl]; 
    
    sr[PMLRR]     = Complex[1., cmpl]; 
    sz[PMLRR]     = 1.; 
    sPhiTilde[PMLRR] = Complex[1., cmpl];
    
    // sPhiTilde is now calculated directly to avoid division by zero
    // sPhiTilde[] = rTilde[] / R[]; 
    
    Lrr[]     = (sPhiTilde[]) * (sz[]/sr[]); 
    Lphiphi[] = (1/sPhiTilde[]) * sz[] * sr[]; 
    Lzz[]     = (sPhiTilde[]) * (sr[]/sz[]); 
    
    
    epsilon[NonPml] = epsilonR[] * TensorDiag[1.,1.,1.];
    epsilon[AllPml] = epsilonR[] * TensorDiag[Lrr[], Lzz[], Lphiphi[]];
    nu[NonPml] = nuR * TensorDiag[1.,1.,1.];
    nu[AllPml] = nuR * TensorDiag[1.0/Lrr[], 1.0/Lzz[], 1.0/Lphiphi[]];
}

// mappings
Jacobian {
    { Name Jac ;
        Case {
            { Region DomNPML ; Jacobian Vol ; }
            { Region PrintPoint ; Jacobian Vol ; }
        }
    }
}

// number of integration points
Integration {
    { Name I1 ;
        Case {
            { Type Gauss ;
                Case {
                    { GeoElement Triangle ; NumberOfPoints gaussOrder ; }
                }
            }
        }
    }
}

// boundary conditions
Constraint {
    //ephi*
    { Name outOfPlaneConstraint ; Type Assign ;
        Case { 
            { Region Bnd ; Value 0. ; }
        }
    }
    
    //erz
    { Name inPlaneConstraint ; Type Assign ;
        Case { 
            If (n != 0) 
                { Region Bnd ; Value 0. ; }
            EndIf 
            If (n == 0) 
                { Region Nonsymm ; Value 0.; }
            EndIf
        }
    }
}

//function spaces 
FunctionSpace {  
    { Name HCurl ; Type Form1 ;
        BasisFunction {
            { Name eRZPlane ; NameOfCoef eRZCoeff ; Function BF_Edge ;  Support Tot ; Entity EdgesOf[All] ; }
            
        }
        Constraint {
            { NameOfCoef eRZCoeff ; EntityType EdgesOf ; NameOfConstraint inPlaneConstraint ; } 
        }
    }
    
    { Name H1 ; Type Form0 ;
        BasisFunction {
            { Name eStarPlane ; NameOfCoef eStarCoeff ; Function BF_Node ; Support Tot ; Entity NodesOf[All] ; }
        }
        Constraint {
            { NameOfCoef eStarCoeff ; EntityType NodesOf ; NameOfConstraint outOfPlaneConstraint ; }
        }
    }
    
    { Name H1Phi ; Type Form0 ;
        BasisFunction {
            { Name ePhiPlane ; NameOfCoef ePhiCoeff ; Function BF_Node ; Support Tot ; Entity NodesOf[All] ; }
        }
        /*Constraint {
            { NameOfCoef eStarCoeff ; EntityType NodesOf ; NameOfConstraint outOfPlaneConstraint ; }
        }*/
    }
}

// weak form 
Formulation {
    { Name Form ; Type FemEquation ;
        Quantity {
            { Name eRZ ; Type Local ; NameOfSpace HCurl ; }
            { Name eStar ; Type Local ; NameOfSpace H1 ; }
            { Name ePhi ; Type Local ; NameOfSpace H1Phi ; }
        }
        
        Equation {
            Galerkin {[CompZZ[nu[]] * R[] * Dof{d eRZ} , {d eRZ} ] ;
                In DomNPML ; Integration I1 ; Jacobian Jac ; }
            
            Galerkin {[nu[] * n^2/(R[]) * Dof{eRZ} , {eRZ} ] ;
                In DomNPML ; Integration I1 ; Jacobian Jac ; }
            
            Galerkin {[nu[] * 1/(R[]) * Dof{d eStar } , {d eStar } ] ;
                In DomNPML ; Integration I1 ; Jacobian Jac ; }    
            
            Galerkin { [nu[] * n/(R[]) * Dof{ eRZ} , {d eStar} ] ;
                In DomNPML ; Integration I1 ; Jacobian Jac ; } 
            
            Galerkin {[nu[] * n/(R[]) * Dof{d eStar} , {eRZ} ] ;
                In DomNPML ; Integration I1 ; Jacobian Jac ; }
            
            Galerkin { DtDtDof[epsilon[] * R[]*(1/cSq) * Dof{eRZ} , {eRZ} ] ;
                In DomNPML ; Integration I1 ; Jacobian Jac ; }
            
            Galerkin { DtDtDof[CompZZ[epsilon[]] * 1/R[] *(1/cSq)* Dof{eStar} , {eStar} ] ;
                In DomNPML ; Integration I1 ; Jacobian Jac ; } 
        }
        
        Equation {
            Galerkin {[Dof{ePhi} , {ePhi} ] ;
                In DomNPML ; Integration I1 ; Jacobian Jac ; }
                
            Galerkin {[- 1/R[] * Dof{eStar} , {ePhi} ] ;
                In DomNPML ; Integration I1 ; Jacobian Jac ; }
        }
    }
}

Resolution {
    { Name Reso ;
        System {
            { Name A ; NameOfFormulation Form ;
                Type ComplexValue ;
            }
        }
        Operation {
            CreateDir["output/"] ;
            GenerateSeparate[A] ;
            Printf["WARNING! Fixed target for eigenvalue solver!"]; 
            EigenSolve[A,NbEigenvalues,2.12840025389035e+14,4.29279989999625e+14] ;
            SaveSolutions[A] ;
        }
    }
}

PostProcessing {
    { Name PostPro ; NameOfFormulation Form ;
        Quantity {
        // Unknowns 
        { Name eRZ ; Value{ Local{ [{eRZ}] ; In DomNPML ; Jacobian Jac ; } } }
        { Name eStar ; Value{ Local{ [{eStar}] ; In DomNPML ; Jacobian Jac ; } } }
        { Name ePhi ; Value{ Local{ [{ePhi}] ; In DomNPML ; Jacobian Jac ; } } }
            
        //Postprocessing quantities 
        { Name DRZ ; Value{ Local{ [epsilon[] * {eRZ}] ; In DomNPML ; Jacobian Jac ; } } }
        { Name DStar ; Value{ Local{ [CompZZ[epsilon[]] * {eStar}] ; In DomNPML ; Jacobian Jac ; } } }
        { Name DPhi ; Value{ Local{ [CompZZ[epsilon[]] * {ePhi}] ; In DomNPML ; Jacobian Jac ; } } }
        { Name EigenValuesReal; Value { Local{ [$EigenvalueReal]  ; In PrintPoint; Jacobian Jac; } } }
        { Name EigenValuesImag; Value { Local{ [$EigenvalueImag]  ; In PrintPoint; Jacobian Jac; } } }
        }
    }
}

PostOperation {
    { Name PostOp ; NameOfPostProcessing PostPro ;
        Operation {
            
            // save eigenvectors and eigenvalues 
            Print [eRZ, OnElementsOf DomNPML, File StrCat["output/eigenvectoreRZ.pos"], EigenvalueLegend];
            Print [eStar, OnElementsOf DomNPML, File StrCat["output/eigenvectoreStar.pos"], EigenvalueLegend];
            Print [ePhi, OnElementsOf DomNPML, File StrCat["output/eigenvectorePhi.pos"], EigenvalueLegend];
            Print [DRZ, OnElementsOf DomNPML, File StrCat["output/eigenvectorDRz.pos"], EigenvalueLegend];
            Print [DStar, OnElementsOf DomNPML, File StrCat["output/eigenvectorDStar.pos"], EigenvalueLegend];
            Print [DPhi, OnElementsOf DomNPML, File StrCat["output/eigenvectorDPhi.pos"], EigenvalueLegend];
            Print[EigenValuesReal, OnElementsOf PrintPoint, Format TimeTable,
            File "EigenValuesReal.txt"];
            Print[EigenValuesImag, OnElementsOf PrintPoint, Format TimeTable,
            File "EigenValuesImag.txt"];
        
            // dump value of solution along a radial line (on-line plot) 
            //Print [ eStar, OnLine {{0,0.294/2,0}{0.15,0.294/2,0}} {10000}, Format SimpleTable, File StrCat["output/mode",Sprintf("%g",n),"Middle.txt"]];
            
            /* Possibly interesting postprocessing quantities
                Print [gradEr, OnElementsOf DomNPML, File StrCat["output/eigenvectoreGradER.pos"], EigenvalueLegend];
                Print [gradEz, OnElementsOf DomNPML, File StrCat["output/eigenvectoreGradEZ.pos"], EigenvalueLegend];
                Print [curlEI, OnElementsOf DomNPML, File StrCat["output/eigenvectoreGradEZ.pos"], EigenvalueLegend];
                Print [Er, OnElementsOf DomNPML, File StrCat["output/eigenvectoreER.pos"], EigenvalueLegend];
                Print [Ez, OnElementsOf DomNPML, File StrCat["output/eigenvectorEZ.pos"], EigenvalueLegend];
            */
        }
    }

}


 
