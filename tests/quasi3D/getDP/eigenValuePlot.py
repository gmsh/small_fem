import numpy
import os
import matplotlib.pyplot as plt
import math
import seaborn
from matplotlib2tikz import save as tikz_save


    
eigReal = numpy.loadtxt('EigenValuesReal.txt', usecols = [1])
eigImag = numpy.loadtxt('EigenValuesImag.txt', usecols = [5])

print(eigReal)
print(eigImag)

plt.plot(eigReal, eigImag,'*'); 

plt.xlabel('Real part (rad / s)')
plt.ylabel('Imaginary part (rad / s)')
plt.title('Spectrum for 2.5D PML')

plt.show()
