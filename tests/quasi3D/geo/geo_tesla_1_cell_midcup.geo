// OpenCASCADE
SetFactory("OpenCASCADE");

// Millimetres
Geometry.OCCScaling = 1e-3;

// IGS File
Merge "geo_tesla_1_cell_midcup.igs";

// Physics
C = 299792458;

// Data
DefineConstant[ F = {1.3e9, Name "Input/00Problem/00Target [Hz]"},
                N = {10,    Name "Input/01Mesh/00Density [per wavelength (mm)]"},
                O = { 2,    Name "Input/01Mesh/01Order [-]"},
                S = { 1,    Name "Input/01Mesh/02Structured?", Choices {0, 1}},
                Q = { 1,    Name "Input/01Mesh/03Quads?",      Choices {0, 1}}];

// Mesh
LY = 2 * 0.0577;
LX = 1 * 0.0350;
L  = C / F;

Characteristic Length{PointsOf{Surface{:};}} = L / N;
Mesh.ElementOrder          = O;
If(S == 0)
  Mesh.Optimize            = 1;
  If(O > 1)
    Mesh.HighOrderOptimize = 1;
  EndIf
EndIf

// Structured mesh
If(S == 1)
  Transfinite   Curve{1, 3} = /*Round[N/L*LY+1]*/ 2^(N-1)+1 Using Progression 1;
  Transfinite   Curve{2, 4} = /*Round[N/L*LX+1]*/ 2^(N-1)+1 Using Progression 1;
  Transfinite Surface{1};
EndIf

// Quadrangles
If(Q == 1)
  Recombine Surface{1};
EndIf

// Physical tags
Physical Surface(120) = {1};    // Domain
Physical    Line(101) = {3};    // Metallic walls
Physical    Line(4)   = {2, 4}; // In-Out (PMC)
Physical    Line(102) = {1};    // Symmetry axis
Physical   Point(100) = {1};    // Print point
