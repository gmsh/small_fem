Include "trafo.dat";

//Point(1) = {0, 0, 0} ;
//Point(2) = { radius, 0, 0} ;
//Point(3) = { radius,  height, 0} ;
//Point(4) = {0,  height, 0} ;

//Curve(1) = {1, 2} ;
//Curve(2) = {2, 3} ;
//Curve(3) = {3, 4} ;
//Curve(4) = {4, 1} ;

//Curve Loop(1) = {1,2,3,4} ;
//Plane Surface(1) = {1} ;

//Transfinite Curve{1, 3} = 50;
//Transfinite Curve{2, 4} = 100;
//Transfinite Surface{1};

//Physical Line("NONSYMM",101) = {l[0],l[1],l[2]} ;
//Physical Line("SYMM",102) = {l[3]} ;
//
//Physical Surface("DOM",120) = {s} ;
//Physical Point(10000)  = {2};             // Print point

p[] += newp ; Point(newp) = {0, 0, 0,cl} ;
p[] += newp ; Point(newp) = { radius, 0, 0,cl} ;
p[] += newp ; Point(newp) = { radius,  height, 0,cl} ;
p[] += newp ; Point(newp) = {0,  height, 0,cl} ;
Printf("", p[]);
l[] += newl ; Line(newl) = {p[0], p[1]} ;
l[] += newl ; Line(newl) = {p[1], p[2]} ;
l[] += newl ; Line(newl) = {p[2], p[3]} ;
l[] += newl ; Line(newl) = {p[3], p[0]} ;
Printf("", l[]);
ll = newll ; Curve Loop(ll) = {l[0], l[1], l[2], l[3]} ;
s = news ; Plane Surface(s) = {ll} ;

If(transfinite == 1)
    Transfinite Curve{1,3} = MSH;
    Transfinite Curve{2,4} = MSH;
    Transfinite Surface{s};
EndIf

Point(5) = {0, height/2, 0, cl}; 
Point(6) = {radius, height/2, 0, cl}; 
Line(5) = {5,6}; 

Physical Line("MIDDLE",103) = {5} ;
Physical Line("NONSYMM",101) = {l[0],l[1],l[2]} ;
Physical Line("SYMM",102) = {l[3]} ;

Physical Surface("DOM",120) = {s} ;
Physical Point(10000)  = {2};             // Print point
