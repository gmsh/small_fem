import numpy
import os
import matplotlib.pyplot as plt
import math
import csv 
from numpy import genfromtxt
from scipy.sparse import coo_matrix
import pillboxFreq as pillbox
from tikzplotlib import save as tikz_save

#Tests the convergence with respect to the mesh refinement for a single mode and
#a single parameter configuration for a certain order choice. 

#Author: Erik Schnaubelt

################################ USER PARAMETERS################################

# 0 : ephiStar, 1: param., 2: Dunn, 3: Oh 
ansatz = 1
beta   = 1
alpha  = 1

# choose pillbox mode TE/TM_{npq} 
n  = 2
p  = 1
q  = 2
TE = 1

# choose mesh densities 
mshList = numpy.array([1,2,4,8,16]); 

# choose order HGrad
oHG = 4
# choose Order HCurl
oHC = 3

# postpro
nocut  = 1
nopos  = 1
notikz = 0

# choose quadrature order 
quadOrder = 16

# tikz save name 
tikzname = 'test'

################################ Automatic tests ###############################

# get target freq
if(TE == 1):
    f = pillbox.TEfreg(n,p,q)
else:
    f = pillbox.TMfreg(n,p,q)


# data
solList = numpy.zeros(mshList.shape)

geo = 'geo/trafo.geo'
msh = 'geo/trafo.msh'
dat = 'geo/trafo.dat'
smallFem = '../../build/'

c0 = 299792458
transfinite = 0
radius = 0.15
height = 0.294

omega     = 2*math.pi*f
eigShift  = omega**2
l         = c0/f

tol    = 1e-12

i      = 0

# iterate over mesh densities
for mm in mshList: 
    MSH    = mm
    cl     = l/MSH
    
    # write .dat 
    par_dat = open(dat, 'w');
    par_dat.write('radius = %3.15e;\n' %(radius))
    par_dat.write('height = %3.15e;\n' %(height)) 
    par_dat.write('f = %3.15e;\n' %(f))
    par_dat.write('cl = %3.15e;\n' %(cl))
    par_dat.write('MSH = %3.15e;\n' %(MSH))
    par_dat.write('transfinite = %i;\n' %(transfinite))
    par_dat.close()

    os.system('gmsh '+ geo +' -2 -format msh2 ')
    
    opt =  ' -msh '    + msh           + \
           ' -beta '   + str(beta)     + \
           ' -alpha '  + str(alpha)    + \
           ' -shift '  + str(eigShift) + \
           ' -oHG '    + str(oHG)      + \
           ' -oHC   '  + str(oHC)      + \
           ' -n '      + str(n)        + \
           ' -ansatz ' + str(ansatz)   + \
           ' -oGauss ' + str(quadOrder)
    if(nocut == 0):
            opt += ' -cut'
    if(nopos == 1):
            opt += ' -nopos'
        
    os.system(smallFem + 'q3d' + opt)
      
    eigTxt = open('eigen.txt', 'r');
    eig = eigTxt.readline()

    solList[i] = (abs(float(eig) - omega) / omega)

    i = i + 1

# calculate all slopes and print to terminal 
slope =numpy.divide(numpy.diff(numpy.log(solList)), \
             numpy.diff(numpy.log(mshList)))

print(slope)

# plot solution
plt.loglog(mshList, solList)

if(TE == 1): 
    plt.title('Convergence of the TE' + str(n) + str(p) + str(q) + ' mode')
else:
    plt.title('Convergence of the TM' + str(n) + str(p) + str(q) + ' mode')


plt.ylabel('Relative error eigenfrequency (-)')
plt.xlabel('Number of elements per wavelength (-)')

if(notikz == 0): 
    tikz_save(tikzname + '.tikz')

plt.show()

