// Simple DDM example for Helmholtz

Jacobian {
  { Name JVol ; Case{ { Region All ; Jacobian Vol ; } } }
  { Name JSur ; Case { { Region All ; Jacobian Sur ; } } }
  { Name JLin ; Case { { Region All ; Jacobian Lin ; } } }
}

Integration {
  { Name I1 ;
    Case {
      { Type Gauss ;
        Case {
          { GeoElement Point ; NumberOfPoints  1 ; }
          { GeoElement Line ; NumberOfPoints  2 ; }
          { GeoElement Triangle ; NumberOfPoints 3 ; }
          { GeoElement Quadrangle ; NumberOfPoints 4 ; }
          { GeoElement Tetrahedron ; NumberOfPoints 4 ; }
          { GeoElement Hexahedron ; NumberOfPoints 6 ; }
          { GeoElement Prism ; NumberOfPoints 9 ; }
        }
      }
    }
  }
}

Constraint{
  { Name Dirichlet; Case{ { Region GammaD; Value 0.; } } }
}

FunctionSpace {
  { Name Hgrad_u; Type Form0;
    BasisFunction {
      { Name sn ; NameOfCoef un ; Function BF_Node ;
        Support Region[ {Omega, GammaInf, GammaD} ] ;
        Entity NodesOf[ All ] ;
      }
    }
    Constraint {
      { NameOfCoef un ; EntityType NodesOf ; NameOfConstraint Dirichlet; }
    }
  }
}

Formulation {
  { Name DDM_Helmholtz; Type FemEquation ;
    Quantity {
      { Name u; Type Local ; NameOfSpace Hgrad_u; }
    }
    Equation {
      Galerkin { [ Dof{d u} , {d u} ] ;
        In Omega; Jacobian JVol ; Integration I1 ; }
      Galerkin { [ -k[]^2 * Dof{u} , {u} ] ;
        In Omega; Jacobian JVol ; Integration I1 ; }
      Galerkin { [ -uinc[], {u} ] ;
        In Omega; Jacobian JVol ; Integration I1 ; }

      // Bayliss-Turkel absorbing boundary condition
      Galerkin { [ - I[] * kInf[] * Dof{u} , {u} ] ;
        In GammaInf; Jacobian JSur ; Integration I1 ; }
      Galerkin { [ alphaBT[] * Dof{u} , {u} ] ;
        In GammaInf; Jacobian JSur ; Integration I1 ; }
      // this assumes that GammaInf is closed; we need to add the boundary terms if it is open
      Galerkin { [ betaBT[] * Dof{d u} , {d u} ] ;
        In GammaInf; Jacobian JSur ; Integration I1 ; }
    }
  }
}

Resolution {
  { Name DDM ;
    System {
      { Name Helmholtz; NameOfFormulation DDM_Helmholtz; Type Complex; }
    }
    Operation {
      Generate[Helmholtz];
      Solve[Helmholtz];
    }
  }
}

PostProcessing {
  { Name DDM_Helmholtz; NameOfFormulation DDM_Helmholtz;
    PostQuantity {
      { Name u; Value { Local { [ {u} ] ; In Omega; Jacobian JVol ; } } }
    }
  }
}

PostOperation {
  { Name DDM; NameOfPostProcessing DDM_Helmholtz;
    Operation {
      Print[ u, OnElementsOf Omega, File "u.pos" ] ;
    }
  }
}
