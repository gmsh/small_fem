Include "circle_concentric_data.geo";

Function {
  I[] = Complex[0, 1];
  k = WAVENUMBER;
  k[] = k;

  // incidence angle
  theta_inc = THETA_INC;
  XYZdotTheta[] = X[] * Cos[theta_inc] + Y[] * Sin[theta_inc];
  //uinc[] = Complex[Cos[k*XYZdotTheta[]], Sin[k*XYZdotTheta[]]];
  uinc[] = Complex[1, 0];
  einc[] = Vector[Complex[0, 0], Complex[1, 0], Complex[0, 0]];

  grad_uinc[] =  I[] * k * Vector[1,0,0] * uinc[];
  dn_uinc[] = Normal[] * grad_uinc[];

  // parameter for ABC
  kInf[]    = k;
  alphaBT[] = 0;//1/(2*R_EXT) - I[]/(8*k*R_EXT^2*(1+I[]/(k*R_EXT)));
  betaBT[]  = 0;//- 1/(2*I[]*k*(1+I[]/(k*R_EXT)));

  // parameter for 0th order TC
  kDtN[] = k;// + (2*Pi /-I[]);
}

Group{
  Omega = Region[{}];
  For idom In {0:N_DOM-1}
    Omega += Region[{(100 + idom)}];

    If (idom == 0)
      GammaD = Region[{(1000 + idom)}];
    EndIf
    If (idom == N_DOM-1)
      GammaInf = Region[{(2000 + idom)}];
    EndIf
  EndFor
}

Include "HelmholtzNoDdm.pro" ;

DefineConstant[
  // default getdp parameters for onelab
  R_ = {"DDM", Name "GetDP/1ResolutionChoices", Visible 0},
  C_ = {"-solve -v 3 -bin -ksp_monitor", Name "GetDP/9ComputeCommand", Visible 0},
  P_ = {"", Name "GetDP/2PostOperationChoices", Visible 0}
];
