#!/bin/bash

## Binaries
ROOT=''
BINANA=$ROOT'wavea'
BINDDM=$ROOT'ddm'

## Data
THREADS=2
TYPE='vector'
MODE='tm'
DDM='emda'
NDOM=2
NMSH=6
MSH='./guide3d_'
INTERP='./guide3d_64.msh'
OV=2
OB=2
K=25

## Useful
NDOMMinus=$(bc <<< $NDOM'-1')

## Grab every residual in a single file
FILE=$TYPE'_'$DDM'_'$NMSH'_'$OV'_'$OB'.l2.dat'
OCTAVE='grabL2(''"'$TYPE'"'', ''"'$DDM'"'', '
OCTAVE=$OCTAVE$NDOM', '$NMSH', '$OV', '$OB', '
OCTAVE=$OCTAVE'"'$FILE'"'')'
octave -q <<< "$OCTAVE"

## Grab every history in a single file
FILE=$TYPE'_'$DDM'_'$NMSH'_'$OV'_'$OB'.hist.dat'
OCTAVE='grabHist(''"'$TYPE'"'', ''"'$DDM'"'', '
OCTAVE=$OCTAVE$NMSH', '$OV', '$OB', '
OCTAVE=$OCTAVE'"'$FILE'"'')'
octave -q <<< "$OCTAVE"

 ## Clear temp files
for m in $(seq 1 $NMSH)
do
    for v in $(seq 1 $OV)
    do
        for b in $(seq 1 $v)
        do
            rm $TYPE'_'$DDM'_'$m'_'$v'_'$b'.hist'
	    
            for d in $(seq 0 $NDOMMinus)
            do
                rm $TYPE'_'$DDM'_'$m'_'$v'_'$b'_'$d'.tmp'
            done
        done
    done
done
