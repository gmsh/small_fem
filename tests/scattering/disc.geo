// User data //
DefineConstant[
  // Frequency
  WAVENUMBER = {40,              Name "Input/0Physics/0Wavenumber"},
  LAMBDA     = {2*Pi/WAVENUMBER, Name "Input/0Physics/1Wavelength", ReadOnly 1},

  // Mesh
  N_LAMBDA      = {5, Name "Input/1Mesh/0Points per wavelength"},
  // Mesh element curvature
  ELEMENT_ORDER = {2, Name "Input/1Mesh/0Geometrical order"},

  // Geometry
  R_IN  = {1, Name "Input/2Geometry/0Internal radius"},
  R_OUT = {2, Name "Input/2Geometry/1External radius"}
];

cl   = LAMBDA/N_LAMBDA;
Rin  = R_IN;
Rout = R_OUT;

// Geometry //
Point(0) = {0, 0, 0, cl};

Point(1) = {+Rin, 0,    0, cl};
Point(2) = {0,    +Rin, 0, cl};
Point(3) = {-Rin, 0,    0, cl};
Point(4) = {0,    -Rin, 0, cl};

Point(5) = {+Rout, 0,     0, cl};
Point(6) = {0,     +Rout, 0, cl};
Point(7) = {-Rout, 0,     0, cl};
Point(8) = {0,     -Rout, 0, cl};

Circle(1) = {1, 0, 2};
Circle(2) = {2, 0, 3};
Circle(3) = {3, 0, 4};
Circle(4) = {4, 0, 1};

Circle(5) = {5, 0, 6};
Circle(6) = {6, 0, 7};
Circle(7) = {7, 0, 8};
Circle(8) = {8, 0, 5};

Line     Loop(1) = {1, 2, 3, 4};
Line     Loop(2) = {5, 6, 7, 8};
Plane Surface(1) = {2, 1};

// Physcials //
Physical Surface(1) = {1};
Physical    Line(2) = {1, 2, 3, 4};
Physical    Line(3) = {5, 6, 7, 8};

// Mesh //
Mesh.ElementOrder = ELEMENT_ORDER;

// Info
Printf("N_LAMBDA:      %g", N_LAMBDA);
Printf("ELEMENT_ORDER: %g", ELEMENT_ORDER);
