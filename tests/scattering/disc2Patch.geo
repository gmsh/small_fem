// User data //
DefineConstant[
  // Frequency
  WAVENUMBER = {40,              Name "Input/0Physics/0Wavenumber"},
  LAMBDA     = {2*Pi/WAVENUMBER, Name "Input/0Physics/1Wavelength", ReadOnly 1},
  // Mesh
  N_LAMBDA      = {5, Name "Input/1Mesh/0Points per wavelength"},
  // Mesh element curvature
  ELEMENT_ORDER = {2, Name "Input/1Mesh/0Geometrical order"},

  // Geometry
  R_IN  = {1, Name "Input/2Geometry/0Internal radius"},
  R_OUT = {2, Name "Input/2Geometry/1External radius"},

  //Structure mesh S and Quads Q
  S = { 0,    Name "Input/01Mesh/02Structured?", Choices {0, 1}},
  Q = { 0,    Name "Input/01Mesh/03Quads?",      Choices {0, 1}}
];

cl   = LAMBDA/N_LAMBDA;
Rin  = R_IN;
Rout = R_OUT; 

// cntrl pts
Point(1) = {+Rin, 0, 0, cl};
Point(2) = {+Rout, 0, 0, cl};
Point(3) = {-Rout, 0, 0, cl};
Point(4) = {-Rin, 0, 0, cl};
Point(5) = {0, 0, 0, cl};

// curves
Circle(1) = {1, 5, 4}; // upper
Circle(2) = {2, 5, 3};
Line(3) = {2, 1};
Line(4) = {4, 3};
Circle(5) = {4, 5, 1}; // lower 
Circle(6) = {3, 5, 2}; // lower

// For constructing the structured mesh
Curve Loop(3) = {4, 6, 3, -5}; //lower
Plane Surface(2) = {3};
Curve Loop(4) = {1, 4, -2, 3}; //upper
Plane Surface(3) = {4};

N = DefineNumber[Ceil(1 / LAMBDA * N_LAMBDA), Name "Subdivisions"];
 
// Structured mesh
If(S == 1)
  Transfinite Curve {3, 4} = N Using Progression 1;
  Transfinite Curve {1, 2} = N Using Progression 1;
  Transfinite Surface {2} = {3, 4, 1, 2};
  Transfinite Curve {3, 4} = N Using Progression 1;
  Transfinite Curve {5, 6} = N Using Progression 1;
  Transfinite Surface {3} = {4, 3, 1, 2};
  Printf("Subdivisions:     %g", N);
EndIf

// Quadrangles
If(Q == 1)
  Recombine Surface {2};
  Recombine Surface {3};
EndIf

// Boundary conditions
Physical Surface(1) = {2,3}; //volume
Physical Curve(2) = {1,5}; //source
Physical Curve(3) = {2,6}; //infinity

// Mesh recombine algorithim
//Mesh.RecombinationAlgorithm = 3;