// User data //
DefineConstant[
  // Frequency
  WAVENUMBER = {40,              Name "Input/0Physics/0Wavenumber"},
  LAMBDA     = {2*Pi/WAVENUMBER, Name "Input/0Physics/1Wavelength", ReadOnly 1},

  // Mesh
  N_LAMBDA      = {5, Name "Input/1Mesh/0Points per wavelength"},
  // Mesh element curvature
  ELEMENT_ORDER = {2, Name "Input/1Mesh/0Geometrical order"},

  // Geometry
  R_IN  = {1, Name "Input/2Geometry/0Internal radius"},
  R_OUT = {2, Name "Input/2Geometry/1External radius"},

  //Structure mesh S and Quads Q
  S = { 0,    Name "Input/01Mesh/02Structured?", Choices {0, 1}},
  Q = { 0,    Name "Input/01Mesh/03Quads?",      Choices {0, 1}}
];

cl   = LAMBDA/N_LAMBDA;
Rin  = R_IN;
Rout = R_OUT; 

// cntrl pts
Point(1) = {+Rin, 0, 0, cl};
Point(2) = {+Rout, 0, 0, cl};
Point(3) = {-Rout, 0, 0, cl};
Point(4) = {-Rin, 0, 0, cl};
Point(5) = {0, 0, 0, cl};
// curves
Circle(1) = {1, 5, 4};
Circle(2) = {2, 5, 3};
Line(3) = {2, 1};
Line(4) = {4, 3};
Curve Loop(1) = {1, 4, -2, 3};

// Physicals
Plane Surface(1) = {1};

L = 1;
N = DefineNumber[Ceil(L/LAMBDA * N_LAMBDA)-1, Name "Subdivisions"];
 
// Structured mesh
If(S == 1)
  Transfinite Curve {3, 4} = N Using Progression 1;
  Transfinite Curve {1, 2} = N Using Progression 1;
  Transfinite Surface {1} = {3, 4, 1, 2};
  Printf("Subdivisions:  %g", N);
EndIf

// Quadrangles
If(Q == 1)
  Recombine Surface {1};
EndIf

// Boundary conditions
Physical Surface(1) = {1};
Physical Curve(2) = {1};
Physical Curve(3) = {2};

// Mesh recombbine algorhithim 
Mesh.RecombinationAlgorithm = 3;