function [d, e] = conv(l2, g2, o, h)
  %% Plot
  figure;
  hold on;
  loglog(h, l2', '-*');
  loglog(h, g2', '-x');
  hold off;
  xlabel('Mesh density [1/m]');
  ylabel('L2 error [-]');
  grid;

  %% Legend
  l  = cell(size(o, 1) + 1, 1);
  nO = length(o);
  for i = 1:nO
    l{i} = num2str(o(i));
  end
  l{nO+1} = 'Geometry';
  legend(l);

  %% Make sure h is a line vector
  nH       = length(h);
  hLine    = zeros(1, nH);
  hLine(:) = h(:);

  %% Compute slopes in loglog (FE solution)
  d = zeros(nO, nH-1);
  for o = 1:nO
    d(o, :) = diff(log10(l2(o, :)))./diff(log10(1./hLine));
  end

  %% Compute slopes in loglog (geometry)
  e = diff(log10(g2))./diff(log10(1./hLine));
end
