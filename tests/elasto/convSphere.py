import numpy
import scipy.optimize
import matplotlib.pyplot
import onelab

## Mesh and FEM
h = 0.1 * numpy.array([1/1, 1/2, 1/4]) # Mesh size
o = numpy.array([1, 2, 3])             # FEM order
p = 2                                  # Mesh order

## Physics data
E   = 50e6  # Young's modulus [Pa]
nu  = 0.25  # Poisson's ratio [-]
rho = 2.2e3 # Density [kg/m3]
R   = 0.1   # Radius [m]

## Moduli and wave speeds
M  = E*(1-nu) / ((1+nu)*(1-2*nu)) # P-wave modulus [Pa]
G  = E        / (2*(1+nu))        # Shear modulus [Pa]
cl = numpy.sqrt(M/rho)            # Speed of pressure wave [m/s]
ct = numpy.sqrt(G/rho)            # Speed of torsional wave [m/s]

## Equation whose roots are the wavenumbers of a pure pressure wave in a sphere
def F(k):
   return numpy.tan(k*R) - k*R / (1 - ((k*R*cl) / (2*ct))**2)

# First angular frequency [rad/s] (first guess k = [20, 30])
print('## Computing resonance angular frequency (pure pressure wave)...')
k     = scipy.optimize.brentq(F, 20, 30) # [rad/m]
omega = k * cl                           # [rad/s]

if(abs(F(k)) > 1e-12):
   print("## Angular frequency not found :(")
   print(" -> Residual:", abs(F(k)))
   xMax = 100
   x    = numpy.linspace(0, xMax, 1000)
   matplotlib.pyplot.plot(x, F(x))
   matplotlib.pyplot.axhline(color='k')
   matplotlib.pyplot.axis([0, xMax, -1, +1])
   matplotlib.pyplot.show()
   exit(0)

print("## Angular frequency found:", omega)
print(" -> Residual:", abs(F(k)))

### Onelab
c = onelab.client(__file__)

## Set data
c.setNumber("00Physics/00Young's modulus", value=E)
c.setNumber("00Physics/01Poisson's ratio", value=nu)
c.setNumber("00Physics/02Density",         value=rho)
c.setNumber("01Geo/00Radius",              value=R)
c.setNumber("02Mesh/01Order",              value=p)
c.setNumber("03Solver/01Omega guess",      value=omega)

## Check
if c.action == 'check':
   exit(0)

## Convergence test
l2 = numpy.zeros(shape=(o.size, h.size))
g2 = numpy.zeros(shape=(1,      h.size))

for ih in range(h.size):
   c.setNumber('02Mesh/00Size', value=h[ih])
   c.runSubClient('myGmsh', 'gmsh sphere.geo -')
   g2[0][ih] = c.getNumber('02Mesh/02Integrated Error') / (4 * numpy.pi * R**2)

   for io in range(o.size):
      print(h[ih], o[io])
      c.setNumber('03Solver/00Order', value=o[io])
      c.runSubClient('sphere', './sphere.exe -nopos')
      l2[io][ih] = c.getNumber('04Output/00L2 Error [-]')

numpy.savetxt("l2U.csv", l2, delimiter=";", fmt="%+.16e")
numpy.savetxt("l2h.csv", h,  delimiter=";", fmt="%+.16e")
numpy.savetxt("l2o.csv", o,  delimiter=";", fmt="%+.16e")
numpy.savetxt("l2g.csv", g2, delimiter=";", fmt="%+.16e")
