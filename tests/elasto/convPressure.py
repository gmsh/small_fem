import numpy
import onelab

## Mesh and FEM
h = 0.1 * numpy.array([1/1, 1/2, 1/4]) # Mesh size
o = numpy.array([1, 2, 3])             # FEM order
p = 2                                  # Mesh order

## Physics data
E    = 50e6  # Young's modulus [Pa]
nu   = 0.25  # Poisson's ratio [-]
Rin  = 0.2   # Inner radius [m]
Rout = 0.3   # Outer radius [m]
L    = 0.25  # Length [m]

## Onelab
c = onelab.client(__file__)

## Set data
c.setNumber("00Physics/00Young's modulus", value=E)
c.setNumber("00Physics/01Poisson's ratio", value=nu)
c.setNumber("01Geo/00Inner radius",        value=Rin)
c.setNumber("01Geo/00Outre radius",        value=Rout)
c.setNumber("01Geo/01Length",              value=L)
c.setNumber("02Mesh/01Order",              value=p)

## Check
if c.action == 'check':
   exit(0)

## Convergence test
l2 = numpy.zeros(shape=(o.size, h.size))

for ih in range(h.size):
   c.setNumber('02Mesh/00Size', value=h[ih])
   c.runSubClient('myGmsh', 'gmsh pressure.geo -3')

   for io in range(o.size):
      print(h[ih], o[io])
      c.setNumber('03Solver/00Order', value=o[io])
      c.runSubClient('pressure', './pressure.exe -nopos')
      l2[io][ih] = c.getNumber('04Output/00L2 Error [-]')

numpy.savetxt("l2U.csv", l2, delimiter=";", fmt="%+.16e")
numpy.savetxt("l2h.csv", h,  delimiter=";", fmt="%+.16e")
numpy.savetxt("l2o.csv", o,  delimiter=";", fmt="%+.16e")
