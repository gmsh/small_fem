// OpenCASCADE //
/////////////////
SetFactory("OpenCASCADE");

// Data //
//////////
Include "cylinder.dat";

// Cylinder //
//////////////
Cylinder(1) = {0,0,-L/2, 0,0,L, R};

// Mesh //
//////////
Mesh.ElementOrder = mO;
Characteristic Length{PointsOf{Volume{1};}} = cl;

// Boundary //
//////////////
bnd[] = Boundary{Volume{1};};

// Physicals //
///////////////
Physical  Volume(1) = {1};
Physical Surface(2) = {bnd[0]};

// Mesh and geometric error //
//////////////////////////////
Solver.AutoMesh = -1;             // The geometry script generates the mesh
If(StrCmp(OnelabAction, "check")) // Only mesh if not in onelab check mode
  // Mesh //
  Mesh 3;

  // Geo error //
  Plugin(DiscretizationError).SuperSamplingNodes = 30;
  Plugin(DiscretizationError).Run;
  Plugin(Integrate).Run;

  SendToServer View[1] "02Mesh/02Integrated Error";

  // Clean //
  Delete View[1];
  Delete View[0];

  // Save //
  Save "cylinder.msh";
EndIf
