// Data //
//////////
Include "pressure.dat";

// Cylinder //
//////////////
Point(0) = {0,     0,     -L/2};   // Center

Point(1) = {+Rout, 0,     -L/2};   // Outer points
Point(2) = {0,     +Rout, -L/2};   // ...
Point(3) = {-Rout, 0,     -L/2};   // ...
Point(4) = {0,     -Rout, -L/2};   // ...

Circle(1) = {1, 0, 2};             // Outer curves
Circle(2) = {2, 0, 3};             // ...
Circle(3) = {3, 0, 4};             // ...
Circle(4) = {4, 0, 1};             // ...

Point(5) = {+Rin, 0,     -L/2};    // Inner points
Point(6) = {0,     +Rin, -L/2};    // ...
Point(7) = {-Rin, 0,     -L/2};    // ...
Point(8) = {0,     -Rin, -L/2};    // ...

Circle(5) = {5, 0, 6};             // Inner curves
Circle(6) = {6, 0, 7};             // ...
Circle(7) = {7, 0, 8};             // ...
Circle(8) = {8, 0, 5};             // ...

Line     Loop(1) = {1, 2, 3, 4};   // Outer loop
Line     Loop(2) = {5, 6, 7, 8};   // Inner loop
Plane Surface(1) = {1, 2};         // Bottom face

v[] = Extrude{0,0,L}{Surface{1};}; // Cylindrical volume

// Mesh //
//////////
Mesh.ElementOrder = mO;
Characteristic Length{PointsOf{Volume{v[1]};}} = cl;

// Boundary //
//////////////
bnd[] = Boundary{Volume{v[1]};};

// Physicals //
///////////////
Physical  Volume(1) = {v[1]};
Physical Surface(2) = {bnd[{2:5}]}; // Outer radius
Physical Surface(3) = {bnd[{0,1}]}; // Sides
Physical Surface(4) = {bnd[{6:9}]}; // Inner radius
Physical   Point(5) = {1, 3};       // Pin-point sliding along the x-axis
Physical   Point(6) = {2, 4};       // Pin-point sliding along the y-axis
