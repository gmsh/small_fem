import numpy
import onelab

## Mesh and FEM
h = 0.1 * numpy.array([1/1, 1/2, 1/4]) # Mesh size
o = numpy.array([1, 2, 3])             # FEM order
p = 2                                  # Mesh order

## Physics data
E   = 50e6  # Young's modulus [Pa]
nu  = 0.25  # Poisson's ratio [-]
rho = 2.2e3 # Density [kg/m3]
L   = 0.4   # Length [m]
R   = 0.1   # Radius [m]

## Torsional resonance frequency [rad/s]
G     = E / (2*(1+nu))        # Shear modulus [Pa]
ct    = numpy.sqrt(G/rho)     # Speed of torsional wave [m/s]
omega = numpy.pi * 3 * ct / L # Third angular frequency [rad/s]

## Onelab
c = onelab.client(__file__)

## Set data
c.setNumber("00Physics/00Young's modulus", value=E)
c.setNumber("00Physics/01Poisson's ratio", value=nu)
c.setNumber("00Physics/02Density",         value=rho)
c.setNumber("01Geo/00Radius",              value=R)
c.setNumber("01Geo/01Length",              value=L)
c.setNumber("02Mesh/01Order",              value=p)
c.setNumber("03Solver/01Omega guess",      value=omega)

## Check
if c.action == 'check':
   exit(0)

## Convergence test
l2 = numpy.zeros(shape=(o.size, h.size))
g2 = numpy.zeros(shape=(1,      h.size))

for ih in range(h.size):
   c.setNumber('02Mesh/00Size', value=h[ih])
   c.runSubClient('myGmsh', 'gmsh cylinder.geo -')
   g2[0][ih] = c.getNumber('02Mesh/02Integrated Error') / (2*numpy.pi*R * (L+R))

   for io in range(o.size):
      print(h[ih], o[io])
      c.setNumber('03Solver/00Order', value=o[io])
      c.runSubClient('cylinder', './cylinder.exe -nopos')
      l2[io][ih] = c.getNumber('04Output/00L2 Error [-]')

numpy.savetxt("l2U.csv", l2, delimiter=";", fmt="%+.16e")
numpy.savetxt("l2h.csv", h,  delimiter=";", fmt="%+.16e")
numpy.savetxt("l2o.csv", o,  delimiter=";", fmt="%+.16e")
numpy.savetxt("l2g.csv", g2, delimiter=";", fmt="%+.16e")
