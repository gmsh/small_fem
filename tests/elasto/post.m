U = dlmread('l2U.csv', ';');
o = dlmread('l2o.csv', ';');
h = dlmread('l2h.csv', ';');

hasGeo = exist('l2g.csv', 'file');
if(hasGeo)
  g = dlmread('l2g.csv', ';');
else
  g = ones(1, size(U, 2));
end

[d, e] = conv(U, g, o, 1./h);

disp("FE error")
d

disp("Geometric error")
e
