#!/bin/bash

name=tri
tol="0"
for t in $tol;do
  c=$(echo "$t")
  file="$name$c"
  echo $file
  ./proj -o 1 2 3 4 5 -type vector -msh ~/Programs/small_fem_tmp/tests/l2Projection/mesh/tri{0,1,2,3,4}.msh -l2 8 -tol $t -name $file
done

