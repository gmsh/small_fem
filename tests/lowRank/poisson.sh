#!/bin/bash

name=Laplace
order="1 2 3 4 5"
tol="0 1e-20 1e-18 1e-16 1e-14 1e-12"

rm Error/*
rm Residu/*
rm L2Error*

for t in $tol;do
  ff=$(echo "$t" | cut -d'-' -f2)
  file="$name-e$ff.csv"
  touch "$file"

for mesh in $@; do
  h=$(echo "$mesh" | cut -d'_' -f3 | cut -d'.' -f1)
  k=$(echo "$mesh" | cut -d'_' -f2)
  bMesh=$(echo "$mesh" | awk -F "/" '{print $NF}' | cut -d'.' -f1)

  for o in $order;do
     res="res_$h-$k-$o-$t"
     echo "####  Tol: $t - Order: $o - Mesh: $bMesh"
     printf "$h,$o,$k" >> $file
     touch tmp
     ./poisson -o $o -msh $mesh -l2 8 -tol $t >> tmp
     awk '/Peak Memory:/ || /L2 Error:/ || /System Assembled:/ {printf ","$3 }' tmp >> $file
     awk '/- iteration/ {printf ","$3}' tmp | awk -F "," '{printf ","$NF}' >> $file
     awk '/error/ {print $2}' tmp >> $res
     printf "\n" >> $file
     mv $res  Residu/
     rm tmp

   done
 done

mv $file Error/
done
