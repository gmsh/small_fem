#!/usr/bin/env python
import re
import csv
import glob
import numpy as np
import matplotlib.pyplot as plt
from collections import namedtuple
from collections import defaultdict

"""
@brief A plotting tool for low rank tests

This class is an interface to plot the result of low rank 
test ran via the shell script l2error.sh.

The file format is as follows:

| h the mesh size | p the order | k the wave-number | N the nb of unknowns | memormy consumption | l2 error |

The name of the problem that is helmoltz or laplace should be given is the filename.
The same goes for the tolerance.

"""

################### CLASS DATAFRAME ############################
class dataFrame:

###### INIT ######
    def __init__(self,files):
        # Ploting tools
        self.colors=['black','blue','darkorange','green','purple']
        self.markers = ['x--','o--','^--','D--','+--']
        self.legend  = ['x'  ,'o'  ,'^'  ,'D'  ,'+'  ]
        # Initialise the dataFrame
        self.l2error    = defaultdict(list)
        PARAM = namedtuple('PARAM',['h','p','k','N','tol','name'])
        DATA  = namedtuple('DATA' ,['l2','mem'])
        for idx, file in enumerate(files):
            # Checking name of the file for Laplace or Helmoltz problem
            try:
                if re.search("Laplace",file):
                    n="laplace"
                else:
                    n="helmoltz"
                # Matching the tolerance with the name of the file
                tolerance = re.search("[0-9][0-9]",file).group(0)
                # Need to check if the tol is 08 008 etc ...
                if re.search("0[1-9]+",tolerance):
                    tolerance=re.sub("0","",tolerance)
                elif re.search("0+",tolerance):
                    tolerance=0
                else:
                    pass

                with open(file) as myData:
                    myCsv = csv.reader(myData)
                    for row in myCsv:
                        self.l2error[PARAM(h=int(row[0]),p=int(row[1]),\
                                        k=int(row[2]),N=int(row[3]),\
                                        tol=int(tolerance),name=n)] = \
                                        (DATA(l2=float(row[5]),\
                                        mem=np.ceil(float(row[4]))))
            except:
                print(file, "could not be opened")
######## STATIC METHODS ###############

    @staticmethod
    '''
    Simple method to determine the structure of the 
    multiple plot

    '''
        
    def plotStructure(nb):
        nRow, nCol = 1, 1
        while (nRow * nCol < nb):
            nRow+=1
            if (nRow * nCol >= nb):
                break
            else:
                nCol+=1
        return nRow, nCol

    @staticmethod
    '''
    Calculates the relative error between two lists
    '''
    
    def relativeError(l1,l2):
        rel = []
        for val1, val2 in zip(l1,l2):
            rel.append(abs((val2-val1)/val2))
        return rel

    @staticmethod
    '''
    Export the result to the given filename.
    The variable you want to export should be given in the
    following arguments.
    '''
    def export(filename, *args):
        with open(filename, "w") as csvFile:
            writer = csv.writer(csvFile, delimiter=",")
            for l in zip(*args):
                writer.writerow(list(l))

    @staticmethod
    '''
    Main method of the plotting script
    Select the data in the dataframe corresponding to the given criteria

    Criteria are:
    "max" for the maximum nb of unknowns
    "min" for the minimum nb of unknowns
    (h,p) a tuple of mesh size and order value

    '''
    def selection(dic,*argv):
        # Select a DF on unknows criteria
        assert(len(argv) > 0)
        if (type(argv[0]) is str):
            # min max case
            if argv[0] == "min":
                RET = min(dic, key = lambda item: item.N)
            elif argv[0] == "max":
                RET = max(dic, key = lambda item: item.N)
            else:
                raise ValueError("Wrong string")

        elif (type(argv[0]) is tuple):
            # (h,p) case
            for key, val in dic.items():
                if (key.h == argv[0][0] and key.p == argv[0][1]):
                    RET = key
        else:
            raise TypeError\
                ("Too many arguments. \
                  Either (h,p) or 'min' or 'max'")
        return RET

####### GET #######

    def get(self,lb,ub,tol,name,waveNumber,*argv):
        '''
        Get the data in the dataframe corresponding to the criterias given

        lb    lower boundari   of l2 error.
        ub    upper boundary   of l2 error.
        tol   tolerance you want to look for.
        name  helmoltz or laplace.
        waveNumber   the wave-number.
        '''
        processedDict, gainDict = dict(), dict()
        for k in waveNumber:
            ref, tmpDict = dict(), dict()
            ref.clear()
            tmpDict.clear()
            # Looping through the dict to find values
            # that match the criterias
            for key, val in self.l2error.items():
                if (  key.tol == tol  \
                    and key.k == k    \
                    and val.l2 >= lb  \
                    and val.l2 <= ub  \
                    and key.name == name):
                    tmpDict[key] = val

            try:
                # Selecting the values for N min or max or (h,p)
                comp = self.selection(tmpDict,*argv)
                processedDict[comp] = tmpDict.get(comp)
            except:
                pass

        return processedDict

######## Options ######
    def Options(self, ax, newDict, waveNumber, gain, **kwargs):
        '''
        Options method parses the different options that are:
        
        annotate   (string)  "hp" for mesh size and order or "N" for unknowns
                             or "l2" for l2 error

        verbose    to print the output in the python shell

        export     to export the data.
        
        '''
        
    ## Kwargs Handling
        # Annotation
        if(kwargs.get("annotate")=="hp"):
            HP       = [(key.h, key.p) for key in newDict]
            for k,(i,j) in enumerate(zip(waveNumber, HP)):
                ax.annotate(str(j),xy=(i,gain[k]),
                            color="purple")
        elif(kwargs.get("annotate")=="unknowns"):
            unknowns = [key.N for key in newDict]
            for k,(i,j) in enumerate(zip(waveNumber, unknowns)):
                ax.annotate(str(j),xy=(i,gain[k]),
                            color="purple")

        elif(kwargs.get("annotate")=="l2"):
            l2 = [val.l2 for val in newDict.values()]
            for k,(i,j) in enumerate(zip(waveNumber, l2)):
                # Scientific notation
                ax.annotate('{:.2e}'.format(j),xy=(i,gain[k]),
                            color="purple")
        # Verbose
        if (kwargs.get("verbose")):
            for key, val in newDict.items():
                print(key, '--->', val)
        # Export
        if (kwargs.get("export")):
            self.export("l2_"+str(boundary[0])+\
                        "_"+str(boundary[1])+\
                        "_tol_"+str(t)+".csv",\
                        waveNumber,unknowns,gain)
        plt.yscale(kwargs.get("yscale","linear"))
        plt.xticks(kwargs.get("xticks"))
        return 0

    def getRefMem(self,dic,refVal):
        refMem = []
        for key in dic:
            refMem.append((self.l2error.get((key.h,key.p,key.k, key.N, refVal,key.name))).mem)

        return refMem

###### PLOT ############

    def plot(self,tol,waveNumber,boundaries,fName,select,**kwargs):
        '''
        plotting method:
        Plots the values in the dataframe that match the criterias given

        Arguments:
        tol         list(int)           a list containing the values of tolerance to look for
        waveNumber  list(int)           list containing the values of K to look for.
        boundaires  list[(float,float)] list containing duplets of lower and upper boundaries
        fName       string              the name to look for either "laplace" or "helmoltz"
        select      string of tuple     Either "min", "max" for min or max of unknowns or (h,p) for a given
                                        mesh size and order pair.
        

        kwargs:
        "save"       True to save the figure
        "yscale"     to change the yscale
        "xticks"     to change the xticks
        "annotate"   to annotate the plot with either "N", "hp", "l2"
        "verbose"    True for verbosity
        "export"     True for exporting the data in multiple files
        '''
        
        # Boundaries list of tuples of different boundaries for L2 error
        # Making the multiple subplots
        nRow,nCol = self.plotStructure(len(boundaries))

        for j, boundary in enumerate(boundaries):
            ax       = plt.subplot(nRow,nCol,j+1)
            # For every value of tol except the fist one
            for i,t in enumerate(tol[1:]):
                # Getting the values I want in a dict
                newDict  =  self.get(boundary[0],boundary[1],\
                                    t,fName,waveNumber,select)

                refMemory = self.getRefMem(newDict,tol[0])
                memory    = [val.mem for val in newDict.values()]

                try:
                    gain = self.relativeError(memory, refMemory)
                    ax.plot(waveNumber,gain,self.markers[i],\
                        color=self.colors[i],\
                        label="Tol = 1e-"+str(t))
                    self.Options(ax, newDict, waveNumber, gain, **kwargs)

                except:
                    pass
                refMemory.clear()

                plt.title("L2 in "+str(boundary[0])+\
                          " and "+str(boundary[1]))
            ax.grid(True,"both")
            ax.legend()
            plt.ylabel("Gain in memory")
            plt.xlabel("Wave-number")

        plt.show()
        if(kwargs.get("save")!=None):
            plt.savefig(kwargs.get("save"))

################## MAIN ###########################
def main():
    Files = glob.glob('./Current/*.csv')
    DF    = dataFrame(Files)
    tol   = [0,8,12,18]
    K     = [30,40,50,60]
    bound = [(0,1)]
    DF.plot(tol,K,bound,"laplace","max", annotate="hp",yscale="log")

if __name__=="__main__":
    main()
