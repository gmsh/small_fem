#!/usr/bin/env python -i
#coding=utf-8

import os
import numpy as np
import sqlite3
import matplotlib.pyplot as plt
import matplotlib.lines  as pln

field = ['h', 'o', 'k', 'mem', 'l2', 'N']
plt.rcParams['text.usetex'] = False #True
plt.rcParams['font.size']   = 11    #28

def latex_float(f):
    """Return a latex style string 'a x 10^b' for a given float"""
    float_str = "{0:.1e}".format(f)
    if "e" in float_str:
        base, exponent = float_str.split("e")
        return r"{0} \times 10^{{{1}}}".format(base, int(exponent))
    else:
        return float_str

def populate(filename, db, name, delimiter=','):
    """Populate a database with a .csv file
    * filename: name of the file
    * db: database cursor
    * name: name of newly created TABLE
    * delemiter: .csv delimiter
    """
    data = np.genfromtxt(filename, delimiter=delimiter)
    db.execute('CREATE TABLE '+str(name)+' '+
               '('+' real, '.join(field)+' real)')

    for i in range(data.shape[0]):
        c = 'INSERT INTO '+str(name)+' ('+', '.join(field)+') '+ \
             'VALUES (?, ?, ?, ?, ?, ?)'
        db.execute(c, data[i])

def rdiff(db, entry, ref, case, isAbs=False):
    """ Compute the relative difference (r-c)/r
    * db: database cursor
    * entry: database entry specified as an index in 'field' list
    * ref: the database TABLE whose entry will define 'r'
    * case: list whose 'entry' will define 'c'
    * isAbs: take absolute value?
    """
    data = list()
    for i in range(len(case)):
        if case[i] is None:
            data.append(None)
            continue

        db.execute('SELECT '+str(field[entry])+' FROM '+str(ref)+
                   ' WHERE '+'=? AND '.join([field[i] for i in range(3)])+'=?',
                   (case[i][0], case[i][1], case[i][2]))
        found = db.fetchall()
        if(len(found) != 1):
            raise Error('Not only one possibility')

        rel = (case[i][entry] - found[0][0]) / found[0][0]
        if(isAbs):
            data.append(np.abs(rel))
        else:
            data.append(rel)

    return data


## Database
conn = sqlite3.connect(':memory:')
db   = conn.cursor()

populate('./ErrorMUMPS/L2Error-e0-ina.csv',  db, 'tau0')
populate('./ErrorMUMPS/L2Error-e3-ina.csv',  db, 'tau3')
populate('./ErrorMUMPS/L2Error-e6-ina.csv',  db, 'tau6')
populate('./ErrorMUMPS/L2Error-e9-ina.csv',  db, 'tau9')
populate('./ErrorMUMPS/L2Error-e12-ina.csv', db, 'tau12')

## Param
k     = [30, 40, 50, 60]
tau   = [3, 6, 9, 12]
mark  = ['o', 's', 'v', '^']
title = '$h = 8$ and $p = 4$'
where = 'h=8 AND o=4' #'l2 < 1e-2'
order = 'l2 ASC'      #l2 ASC, mem DESC, mem ASC

## Refence L2 error and systems size
refError = list()
size     = list()
for kk in k:
    db.execute('SELECT * FROM tau0 '+
               'WHERE k=? AND '+where+' '+
               'ORDER BY '+order, [kk])
    found     = db.fetchone()
    refError += [found[4]]
    size     += [int(found[5])]
print(refError)

## Plot
color   = [plt.cm.viridis(i) for i in np.linspace(0, 1, len(tau))]
fig, ax = plt.subplots()
ay      = ax.twiny()
ax.set_title(title)
ax.set_xlabel('Wavenumber [rad/s]')
ax.set_ylabel('Memory gain [-]')

for tt, ti in zip(tau, range(len(tau))):
    rdMem = list()
    rdL2  = list()
    error = list()
    for kk in k:
        db.execute('SELECT * FROM tau'+str(tt)+' '+
                   'WHERE k=? AND '+where+' '+
                   'ORDER BY '+order, [kk])
        found  = [db.fetchone()]
        if(found[0] is not None):
            error += [found[0][4]]
        else:
            error += [None]
        rdMem += rdiff(db, 3, 'tau0', found)
        rdL2  += rdiff(db, 4, 'tau0', found, isAbs=True)

    ax.plot(k,-np.asarray(rdMem),marker=mark[ti],linestyle='-',color=color[ti])
    for i in range(len(error)):
        if(error[i] is not None):
            if(i < len(error) - 1):
                ax.annotate('${0}$'.format(latex_float(error[i])),
                            xy=(k[i],     -rdMem[i]+0.03), color='blue')
            else:
                ax.annotate('${0}$'.format(latex_float(error[i])),
                            xy=(k[i]-4.2, -rdMem[i]+0.00), color='blue')

## Legend and annotation with reference L2 error
lns  = [pln.Line2D([0],[0], color=color[i], marker=mark[i])
        for i in range(len(color))]
txt  = ['$\\tau = 10^{{-{0:d}}}$'.format(tt) for tt in tau]

ax.legend(lns, txt, loc='upper left', frameon=False)
ax.annotate('Average $L_2$ error w/o BLR: ${0}$ (std = ${1}$)'
            .format(latex_float(np.mean(refError)),
                    latex_float(np.std(refError))),
            xy=(35, 0.04), color='red')

## Auxiliary x-axis with system size
fig.subplots_adjust(bottom=0.2)
ay.xaxis.set_ticks_position('bottom')
ay.xaxis.set_label_position('bottom')
ay.spines['bottom'].set_position(('axes', -0.15))
ay.spines['bottom'].set_color('orange')
ax.set_xlim(k[0], k[-1])

ay.set_xticks(np.linspace(0, 1, len(size)))
ay.set_xticklabels(size)
ay.tick_params(axis='x', colors='orange')
ay.set_xlabel('System size [-]')
ay.xaxis.label.set_color('orange')

## Save or show
#fig.set_size_inches(38/2.54, 25/2.54)
#plt.savefig('figure.pgf')
plt.show(block=False)
