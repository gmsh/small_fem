import csv
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.lines as mlines
import glob
import numpy as np

def read(file):
    with open(file) as data:
        H = []
        alpha = []
        beforeMerge = []
        afterMerge = []
        order = []
        dataRead = csv.reader(data)
        for row in dataRead:
            H.append(int(row[0]))
            order.append(int(row[1]))
            beforeMerge.append(int(row[2]))
            afterMerge.append(int(row[3]))
            alpha.append(int(row[4]))
        return H, order, alpha, beforeMerge, afterMerge

def alpha_h(H,alpha,order,unknowns, whichH):
#### Returns the values of l2 error and H for given order and K
    A=[]
    O=[]
    N=[]
    i=0
    for h in H:
        if (int(h) == whichH):
            O.append(order[i])
            A.append(alpha[i])
            N.append(unknowns[i])
        i+=1
    return O,A,N

def alpha_p(H,alpha,order, whichO):
    A=[]
    h=[]
    i=0
    for o in order:
        if (int(o) == whichO):
            h.append(H[i])
            A.append(alpha[i])
        i+=1
    return h,A
    

def PlotAlphaO(file,valueO,name):
    H, O, alpha, beforeMerge, afterMerge = read(file)
    plt.figure()
    ax = plt.subplot(111)
    for o in valueO:
        h, A = alpha_p(H, alpha, O, o)
        ax.plot(np.array(h),np.array(A),label="P = "+str(o),marker="o")

    plt.xlabel('1/h')
    plt.ylabel( 'Relative error allocation %')
    plt.title('Evolution of over allocation for K=30')
    ax.legend()
    plt.grid()
    plt.savefig(name+'.pdf')
    plt.close()
    plt.clf()

def PlotAlphaH(file,valueH,name):
    H, O, alpha, beforeMerge, afterMerge = read(file)
    plt.figure()
    ax = plt.subplot(111)
    for h in valueH:
        order, A, N = alpha_h(H, alpha, O, beforeMerge, h)
        ax.bar(np.array(order),np.array(A),label="H = "+str(h))
        
    plt.xlabel('Order p')
    plt.ylabel( 'Relative error allocation %')
    plt.title('Evolution of over allocation for K=30')
    ax.legend()
    for i,j in zip(order,A):
        ax.annotate(str(N[i-1]),xy=(i-0.3,j+2))
    plt.grid()
    plt.savefig(name+'.pdf')
    plt.close()
    plt.clf()

allocFile = glob.glob("/home/yoann/Programs/SF/build/Alloc2dVector.csv")
PlotAlphaH(allocFile[0],[4,8,16,32,64,128],"Alloc2DVectorH")
PlotAlphaO(allocFile[0],[1,2,3,4,5,6],"Alloc2DVecotrP")

