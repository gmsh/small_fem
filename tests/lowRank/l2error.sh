#!/bin/bash
# This script gets the data from the waves exe
# To use give the mesh as argument

name=L2Error
order="1 2 3"
tol="0 1e-12 1e-10 1e-8"
itol="1e-3 1e-15"

rm Error/*
rm Residu/*
rm L2Error*

for t in $tol;do
    for it in $itol;do
    ff=$(echo "$t" | cut -d'-' -f2)
    if=$(echo "$it" | cut -d'-' -f2)
    file="$name-e$ff-i$if.csv"
    touch "$file"

    for mesh in $@; do
    h=$(echo "$mesh" | cut -d'_' -f3 | cut -d'.' -f1)
    k=$(echo "$mesh" | cut -d'_' -f2)
    bMesh=$(echo "$mesh" | awk -F "/" '{print $NF}' | cut -d'.' -f1)

    for o in $order;do
        res="res_$h-$k-$o-$t-it$it"
        echo "####  Tol: $t - Order: $o - Mesh: $bMesh - It tol: $it"
        printf "$h,$o,$k" >> $file
        touch tmp
        OMP_NUM_THREADS=2 ./waves -o $o -type vector -msh $mesh -mode tm -k $k -l2 8 -nopos -tol $t -itol $it >> tmp
        awk '/Peak Memory:/ || /L2 Error:/ || /System Assembled:/ {printf ","$3 }' tmp >> $file
        awk '/- iteration/ {printf ","$3}' tmp | awk -F "," '{printf ","$NF}' >> $file
        awk '/error/ {print $2}' tmp >> $res
        printf "\n" >> $file
        mv $res  Residu/
        rm tmp

    done
    done

    mv $file Error/
    done
 done
