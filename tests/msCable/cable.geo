// OpenCASCADE //
/////////////////
SetFactory("OpenCASCADE");

// Data //
//////////
Include "cable.dat";

// Geo //
/////////
v1  = newv; Cylinder(v1) = {X,Y,-L/2, 0,0,L, R};    // Cable
v2  = newv; Cylinder(v2) = {0,0,-L/2, 0,0,L, Rin};  // Shell inner radius
v3  = newv; Cylinder(v3) = {0,0,-L/2, 0,0,L, Rout}; // Shell outer radius

v[] = BooleanFragments{Volume{v1, v2, v3}; Delete;}{};

// Mesh size //
///////////////
p[] = PointsOf{Volume{v[]};};
Characteristic Length{p[]} = cl;

// Boundaries //
////////////////
tmp[]  = Boundary{Volume{v[0]};};
bnd[]  = tmp[{1:2}];
tmp[]  = Boundary{Volume{v[1]};};
bnd[] += tmp[{1:2}];
tmp[]  = Boundary{Volume{v[2]};};
bnd[] += tmp[{0:2}];

// Physical //
//////////////
Physical  Volume(1) = v[0];  // Cable
Physical  Volume(2) = v[1];  // Air
Physical  Volume(3) = v[2];  // Shell
Physical Surface(4) = bnd[]; // Boundary

// Mesh and Spanning Tree //
////////////////////////////
Solver.AutoMesh   = -1;           // The geometry script generates the mesh
If(StrCmp(OnelabAction, "check")) // Only mesh if not in onelab check mode
  // Mesh //
  Mesh 3;

  // Spanning tree //
  Plugin(SpanningTree).PhysicalVolumes  = "1, 2, 3";
  Plugin(SpanningTree).PhysicalSurfaces = "4";
  Plugin(SpanningTree).OutputPhysical   = 5;
  Plugin(SpanningTree).Run;

  // Save //
  Save "cable.msh";
EndIf
