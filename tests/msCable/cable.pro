Include "cable.dat";

Group{
  Src    = Region[1];
  Air    = Region[2];
  Shell  = Region[3];

  Omega  = Region[{Src, Air, Shell}];
  dOmega = Region[4];
  Tree   = Region[5];
  Disp   = Region[{Src, Air, Shell}];
}

Function{
  // Physical constant and current density
  Nu0     = 1/Mu0;
  Js[Src] = Vector[0, 0, 1/(Pi*R^2)];
}

Jacobian{
  { Name Jac; Case{
      { Region Shell; Jacobian VolCylShell{Rin, Rout, 0, 0,0, 1}; }
      { Region All;   Jacobian Vol; }
    }
  }
}

Integration{
  { Name I1;
    Case{ { Type Gauss;
            Case{ { GeoElement Line;        NumberOfPoints 3;  }
                  { GeoElement Triangle;    NumberOfPoints 3;  }
                  { GeoElement Tetrahedron; NumberOfPoints 4;  } } } }
  }
}

Constraint{
  { Name TCTGauge; Type Assign; Case{ { Region Tree;   Value 0.; } } }
  { Name PEC;      Type Assign; Case{ { Region dOmega; Value 0.; } } }
}

FunctionSpace{
  { Name HCurl; Type Form1;
    BasisFunction{
      { Name se; NameOfCoef an; Function BF_Edge;
        Support Omega; Entity EdgesOf[All]; }
    }
    Constraint{
      { NameOfCoef an; NameOfConstraint TCTGauge; EntityType EdgesOf; }
      { NameOfCoef an; NameOfConstraint PEC;      EntityType EdgesOf; }
    }
  }
}

Formulation{
  { Name MagStaA; Type FemEquation;
    Quantity{
      { Name a; Type Local; NameOfSpace HCurl; }
    }
    Equation{
      Galerkin{ [Nu0 * Dof{d a}, {d a}];
        In Omega; Jacobian Jac; Integration I1; }
      Galerkin{ [-Js[], {a}];
        In Src;   Jacobian Jac; Integration I1; }
    }
  }
}

Resolution{
  { Name MagStaA;
    System{
      { Name A; NameOfFormulation MagStaA; }
    }
    Operation{
      Generate[A];
      Solve[A];
    }
  }
}

PostProcessing{
  { Name MagStaA; NameOfFormulation MagStaA;
    PostQuantity{
      { Name js; Value{ Term{ [Js[]];  In Src;  Jacobian Jac; } } }
      { Name a;  Value{ Term{ [{a}];   In Disp; Jacobian Jac; } } }
      { Name b;  Value{ Term{ [{d a}]; In Disp; Jacobian Jac; } } }
    }
  }
}

PostOperation MagStaA UsingPost MagStaA{
  Print[js, OnElementsOf Src,  File "js.pos"];
  Print[a,  OnElementsOf Disp, File  "a.pos"];
  Print[b,  OnElementsOf Disp, File  "b.pos"];
}
