Include "cable.dat";

Plugin(MathEval).Expression0= "Sqrt(v0^2+v1^2+v2^2)";
Plugin(MathEval).View=-1;
Plugin(MathEval).Run;

Plugin(CutGrid).X0=X;
Plugin(CutGrid).Y0=Y;
Plugin(CutGrid).Z0=0;
Plugin(CutGrid).X1=Rout;
Plugin(CutGrid).Y1=Y;
Plugin(CutGrid).Z1=0;
Plugin(CutGrid).X2=0;
Plugin(CutGrid).Y2=0;
Plugin(CutGrid).Z2=0;
Plugin(CutGrid).NumPointsU=200;
Plugin(CutGrid).NumPointsV=1;
Plugin(CutGrid).ConnectPoints=1;
Plugin(CutGrid).View=-1;
Plugin(CutGrid).Run;
