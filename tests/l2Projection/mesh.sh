#!/bin/bash

FILE=$1

for N in ${@:2}
do
    gmsh $FILE.geo -3 -setnumber N $N -o $FILE$N.msh
done
