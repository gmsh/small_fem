DefineConstant[
  N = { 2, Name "Input/00Mesh refinement level" },
  l = { 1, Name "Input/01Domain size"           }
];

msh = 2^(N-1)+1;

Point(1) = {0, 0, 0};
Point(2) = {l, 0, 0};

Line(1) = {1, 2};

Transfinite Line {1} = msh Using Progression 1;

Physical Line(7) = {1};
