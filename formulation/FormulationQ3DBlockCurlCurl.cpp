#include "SmallFem.h"
#include "FormulationQ3DBlockCurlCurl.h"

#include "Quadrature.h"
#include "Mapper.h"

using namespace std;
using namespace sf;

FormulationQ3DBlockCurlCurl::FormulationQ3DBlockCurlCurl(void){
}

FormulationQ3DBlockCurlCurl::
FormulationQ3DBlockCurlCurl(const GroupOfElement& domain,
                          const FunctionSpace1Form& fs,
                          double multConst, 
                          double exponent, 
                          const Basis& basis, 
                          const GroupOfJacobian& jac,
                          int G,
                          const fullMatrix<double>& gC,
                          const fullVector<double>& gW,
                          void (*f)(fullVector<double>&, fullMatrix<Complex>&)
                         ){
  // Save Data //
  this->ddomain   = &domain;
  this->fs        = &fs; 
  this->multConst = multConst; 
  this->exponent  = exponent; 

  // Get Basis //
  this->basis      = &basis;
  this->jac        = &jac; 
  this->gC         = &gC; 
  this->gW         = &gW; 
  this->G          = G;
  
  this->f          = f; 
}

FormulationQ3DBlockCurlCurl::~FormulationQ3DBlockCurlCurl(void){
  
}

Complex FormulationQ3DBlockCurlCurl::
weak(size_t dofI, size_t dofJ, size_t elementId) const{
  // Init Some Stuff //
  fullMatrix<double> mat;
  fullVector<double> phiI(3);
  Complex intMatPhiIPhiJ;
  fullVector<double> phiJ(3);
  fullMatrix<Complex> TMatrix(3,3); 
  

  Complex integral = Complex(0, 0);
  double  det;
  fullVector<double>  xyz(3);
  double  pxyz[3];
  

  // Get Element //
  const MElement& element = ddomain->get(elementId);

  // Get Basis Functions //
  const fullMatrix<double>& eCurlFun =
    basis->getPreEvaluatedDerivatives(element);

  // Get Jacobians //
  const vector<pair<fullMatrix<double>, double> >& MJac =
    jac->getJacobianMatrix(elementId);

  // Loop over Integration Point //
  for(int g = 0; g < G; g++){
    det = MJac[g].second;   // det(Jac) at integration point g
    mat = MJac[g].first;    // J        at integration point g
    ReferenceSpaceManager::
      mapFromABCtoXYZ(element, (*gC)(g, 0), (*gC)(g, 1), (*gC)(g, 2), pxyz);
                            // xyz coordinates of point g

    Mapper::hDiv(eCurlFun, dofI, g, mat, det, phiI); // curl(e_I)|g in xyz-space
    Mapper::hDiv(eCurlFun, dofJ, g, mat, det, phiJ); // curl(e_J)|g in xyz-space
    
     xyz(0) = pxyz[0];
     xyz(1) = pxyz[1];
     xyz(2) = pxyz[2];
    
    
    f(xyz, TMatrix); 
    
    // WARNING! Only diagonal matrices possible! 
    intMatPhiIPhiJ = TMatrix(0,0) * phiI(0) * phiJ(0)
                        + TMatrix(1,1) * phiI(1) * phiJ(1)
                        + TMatrix(2,2) * phiI(2) * phiJ(2);
    

    integral +=
      intMatPhiIPhiJ * fabs(det) * (*gW)(g) * pow(pxyz[0],exponent);
  }

  // Done
  return multConst * integral;
}

Complex FormulationQ3DBlockCurlCurl::
rhs(size_t equationI, size_t elementId) const{
  return Complex(0, 0);
}

const FunctionSpace& FormulationQ3DBlockCurlCurl::field(void) const{
  return *fs;
}

const FunctionSpace& FormulationQ3DBlockCurlCurl::test(void) const{
  return *fs;
}

const GroupOfElement& FormulationQ3DBlockCurlCurl::domain(void) const{
  return *ddomain;
}

bool FormulationQ3DBlockCurlCurl::isBlock(void) const{
  return true;
}

