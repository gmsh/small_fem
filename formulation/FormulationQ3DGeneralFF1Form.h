#ifndef _FORMULATIONQ3DGENERALFF_H_
#define _FORMULATIONQ3DGENERALFF_H_

// include needed header files
#include "SmallFem.h"
#include "FormulationCoupled.h"
#include "FunctionSpace0Form.h"
#include "FunctionSpace1Form.h"
#include "GroupOfJacobian.h"
#include "Quadrature.h"

/**
   @class FormulationQ3DGeneralFF1Form
   @brief General Term for function-function bilinear forms 
   

   Calculates multConst * (r^exponent U, V) 
       
   @author Erik Schnaubelt
*/

namespace sf{
  class FormulationQ3DGeneralFF1Form: public FormulationCoupled<Complex>{
  private:
    friend class FormulationQ3DBlockStiffness;
    friend class FormulationQ3DBlockMass;
  
  private:
    std::list<FormulationBlock<Complex>*> fList;

  public:
    FormulationQ3DGeneralFF1Form(const GroupOfElement& domain,
                          const FunctionSpace1Form& fs,
                          double multConst, 
                          double exponent, 
                          const Basis& basis, 
                          const GroupOfJacobian& jac,
                          int G,
                          const fullMatrix<double>& gC,
                          const fullVector<double>& gW,
                          void (*f)(fullVector<double>&, fullMatrix<Complex>&)
                         );

    virtual ~FormulationQ3DGeneralFF1Form(void);

    virtual const std::list<FormulationBlock<Complex>*>&
                                               getFormulationBlocks(void) const;
    virtual bool isBlock(void) const;
  };
}

#endif
 
