#include "SmallFem.h"
#include "FormulationQ3DEStarStiffnessFive.h"

#include "Quadrature.h"
#include "Mapper.h"

#include "TermFieldField.h"
#include "TermGradGrad.h"
#include "TermCurlCurl.h"

using namespace std;
using namespace sf;

FormulationQ3DEStarStiffnessFive::FormulationQ3DEStarStiffnessFive(void){
}

FormulationQ3DEStarStiffnessFive::
FormulationQ3DEStarStiffnessFive(const GroupOfElement& domain,
                                  const FunctionSpace0Form& test,
                                  const FunctionSpace1Form& field,
                                  int n, 
                                  const TermGradGrad<Complex>& localInG
                                  ){
  
  // Save Data //
  this->ddomain = &domain;
  this->ffield  = &field;
  this->ttest   = &test; 
  this->n       = n; 
  this->localInG = &localInG; 

}

FormulationQ3DEStarStiffnessFive::~FormulationQ3DEStarStiffnessFive(void){

}

Complex FormulationQ3DEStarStiffnessFive::
weak(size_t dofI, size_t dofJ, size_t elementId) const{
  Complex c(1, 0); 
  return static_cast<Complex>(n) * c * localInG->getTerm(dofI, dofJ, elementId);
}

Complex FormulationQ3DEStarStiffnessFive::
rhs(size_t equationI, size_t elementId) const{
  return Complex(0, 0);
}

const FunctionSpace& FormulationQ3DEStarStiffnessFive::field(void) const{
  return *ffield;
}

const FunctionSpace& FormulationQ3DEStarStiffnessFive::test(void) const{
  return *ttest;
}

const GroupOfElement& FormulationQ3DEStarStiffnessFive::domain(void) const{
  return *ddomain;
}

bool FormulationQ3DEStarStiffnessFive::isBlock(void) const{
  return true;
}
