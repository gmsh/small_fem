#ifndef _FORMULATIONSOURCE_H_
#define _FORMULATIONSOURCE_H_

#include "FormulationBlock.h"
#include "GroupOfElement.h"
#include "FunctionSpace.h"
#include "Term.h"

/**
   @class FormulationSource
   @brief Formulation for source terms

   Formulation adding the following RHS: (f, u')
 */

namespace sf{
  template<typename scalar> class FormulationSource:
    public FormulationBlock<scalar>{
  private:
    const FunctionSpace*  ffs;
    const GroupOfElement* ddomain;
    Term<scalar>*         term;

  public:
    FormulationSource(const GroupOfElement& domain,
                      const FunctionSpace& fs,
                      scalar (*f)(fullVector<double>&));

    FormulationSource(const GroupOfElement& domain,
                      const FunctionSpace& fs,
                      scalar (*f)(const MElement& element,
                                  fullVector<double>&));

    FormulationSource(const GroupOfElement& domain,
                      const FunctionSpace& fs,
                      fullVector<scalar> (*f)(fullVector<double>&),
                      int iOrder = -1);

    virtual ~FormulationSource(void);

    virtual scalar weak(size_t dofI, size_t dofJ, size_t elementId) const;
    virtual scalar rhs(size_t equationI, size_t elementId)          const;

    virtual const FunctionSpace&   field(void) const;
    virtual const FunctionSpace&    test(void) const;
    virtual const GroupOfElement& domain(void) const;

    virtual bool isBlock(void) const;
  };
}

/**
   @fn FormulationSource::FormulationSource(const GroupOfElement&,const FunctionSpace0Form&,scalar (*f)(fullVector<double>&))
   @param domain A GroupOfElement
   @param fs A FunctionSpace0Form for both unknown and test field
   @param f a scalar function used as source

   Instantiates a new scalar FormulationSource with the given parametres.
   **

   @fn FormulationSource::FormulationSource(const GroupOfElement&,const FunctionSpace0Form&,scalar (*f)(const MElement&,fullVector<double>&))
   @param domain A GroupOfElement
   @param fs A FunctionSpace0Form for both unknown and test field
   @param f a scalar function used as source; this function is defined on a MElement only

   Instantiates a new scalar FormulationSource with the given parametres.
   **

   @fn FormulationSource::FormulationSource(const GroupOfElement&,const FunctionSpace1Form&,fullVector<scalar> (*f)(fullVector<double>&))
   @param domain A GroupOfElement
   @param fs A FunctionSpace1Form for both unknown and test field
   @param f A vector function used as source
   @param iOrder An integration order

   Instantiates a new vector FormulationSource with the given parametres.
   The integration order is determined according to iOrder
   (-1 means that this integration order is determined automatically
   via the function space).
   **

   @fn FormulationSource::~FormulationSource
   Deletes this FormulationSource
*/

#endif
