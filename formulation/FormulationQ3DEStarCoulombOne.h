#ifndef _FORMULATIONQ3DESTARCOULOMBONE_H_
#define _FORMULATIONQ3DESTARCOULOMBONE_H_

#include "SmallFem.h"
#include "FunctionSpace0Form.h"
#include "FunctionSpace1Form.h"
#include "GroupOfJacobian.h"

#include "FormulationBlock.h"
#include "FormulationQ3DEStarCoulomb.h"

#include "TermFieldField.h"
#include "TermGradGrad.h"

/**
   @class FormulationQ3DEStarCoulombOne
   @brief Term in (d xi_i, aRZ_j) of the Coulomb gauge for the ephiStar ansatz

   Term in (d xi_i, aRZ_j) of the Coulomb gauge for the ephiStar ansatz
*/

namespace sf{
  class FormulationQ3DEStarCoulombOne: public FormulationBlock<Complex>{
  private:
    friend class FormulationQ3DEStarCoulomb;

  private:
    // Function Space & Domain //
    const FunctionSpace0Form* ffield;
    const FunctionSpace1Form* ttest;
    const GroupOfElement*     ddomain;


    // local term //
    const TermGradGrad<Complex>* lXRZ;


  private:
    FormulationQ3DEStarCoulombOne(void);
    FormulationQ3DEStarCoulombOne(const GroupOfElement& domain,
                                  const FunctionSpace0Form& field,
                                  const FunctionSpace1Form& test,
                                  const TermGradGrad<Complex>& lXRZ);

  public:
    virtual ~FormulationQ3DEStarCoulombOne(void);

    virtual Complex weak(size_t dofI, size_t dofJ, size_t elementId) const;
    virtual Complex rhs(size_t equationI, size_t elementId)          const;

    virtual const FunctionSpace&   field(void) const;
    virtual const FunctionSpace&    test(void) const;
    virtual const GroupOfElement& domain(void) const;

    virtual bool isBlock(void) const;
  };
}

/**
   @fn FormulationQ3DEStarCoulombOne::~FormulationQ3DEStarCoulombOne
   Deletes this FormulationQ3DEStarCoulombOne
*/

#endif
