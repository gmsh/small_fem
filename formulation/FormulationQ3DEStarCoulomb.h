#ifndef _FORMULATIONQ3DESTARCOULOMB_H_
#define _FORMULATIONQ3DESTARCOULOMB_H_

#include "SmallFem.h"
#include "FormulationCoupled.h"
#include "FunctionSpace0Form.h"
#include "FunctionSpace1Form.h"

#include "TermFieldField.h"
#include "TermGradGrad.h"

/**
   @class FormulationQ3DEStarCoulomb
   @brief Coulomb gauge for the ephiStar ansatz

   Coulomb gauge for the ephiStar ansatz
*/

namespace sf{
  class FormulationQ3DEStarCoulomb: public FormulationCoupled<Complex>{
  private:
    std::list<FormulationBlock<Complex>*> fList;

    TermFieldField<Complex>* lXPhi;
    TermGradGrad<Complex>*   lXRZ;

  public:
    FormulationQ3DEStarCoulomb(const GroupOfElement& dom,
                               const FunctionSpace0Form& hPhi,
                               const FunctionSpace1Form& hRZ,
                               const FunctionSpace0Form& hXi,
                               int N,
                               void (*r)(fullVector<double>&,
                                         fullMatrix<Complex>&),
                               Complex (*invR)(fullVector<double>&),
                               int order);

    virtual ~FormulationQ3DEStarCoulomb(void);

    virtual const std::list<FormulationBlock<Complex>*>&
                                               getFormulationBlocks(void) const;
    virtual bool isBlock(void) const;
  };
}

/**
   @fn FormulationQ3DEStarCoulomb::FormulationQ3DEStarCoulomb
   @param dom A GroupOfElement for the domain
   @param hPhi FunctionSpace for the out of plane component
   @param hRZ FunctionSpace for the in plane component
   @param hXi FunctionSpace for the Coulomb gauge
   @param r function representing the radial coordinate
   @param invR function representing the inverse of the radial coordinate
   @param order order of the Gauss quadrature

   Instantiates a new FormulationQ3DEStarCoulomb
   **

   @fn FormulationQ3DEStarCoulomb::~FormulationQ3DEStarCoulomb
   Deletes this FormulationQ3DEStarCoulomb
*/

#endif
