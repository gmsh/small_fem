#include "FormulationElastoMassUU.h"

using namespace std;
using namespace sf;

template<typename scalar>
FormulationElastoMassUU<scalar>::
FormulationElastoMassUU(void){
}

template<typename scalar>
FormulationElastoMassUU<scalar>::
FormulationElastoMassUU(const GroupOfElement& domain,
                        const FunctionSpace0Form& fSpace,
                        const TermFieldField<scalar>& term,
                        double rho){
  // Save Data //
  this->ddomain = &domain;
  this->ffield  = &fSpace;
  this->ttest   = &fSpace;
  this->term    = &term;
  this->rho     =  rho;
}

template<typename scalar>
FormulationElastoMassUU<scalar>::
~FormulationElastoMassUU(void){
}

template<typename scalar>
scalar FormulationElastoMassUU<scalar>::
weak(size_t dofI, size_t dofJ, size_t elementId) const{
  return rho * term->getTerm(dofI, dofJ, elementId);
}

template<typename scalar>
scalar FormulationElastoMassUU<scalar>::
rhs(size_t equationI, size_t elementId) const{
  return 0;
}

template<typename scalar>
const FunctionSpace& FormulationElastoMassUU<scalar>::field(void) const{
  return *ffield;
}

template<typename scalar>
const FunctionSpace& FormulationElastoMassUU<scalar>::test(void) const{
  return *ttest;
}

template<typename scalar>
const GroupOfElement& FormulationElastoMassUU<scalar>::domain(void) const{
  return *ddomain;
}

template<typename scalar>
bool FormulationElastoMassUU<scalar>::isBlock(void) const{
  return true;
}

////////////////////////////
// Explicit instantiation //
////////////////////////////
template class sf::FormulationElastoMassUU<Complex>;
template class sf::FormulationElastoMassUU<double>;
