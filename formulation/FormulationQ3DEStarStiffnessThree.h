#ifndef _FORMULATIONQ3DESTARSTIFFNESSTHREE_H_
#define _FORMULATIONQ3DESTARSTIFFNESSTHREE_H_

#include "SmallFem.h"
#include "FunctionSpace0Form.h"
#include "GroupOfJacobian.h"

#include "FormulationBlock.h"
#include "FormulationQ3DEStarStiffness.h"

#include "TermFieldField.h"
#include "TermGradGrad.h"
#include "TermCurlCurl.h"

/**
   @class FormulationQ3DEStarStiffnessThree
   @brief HGrad part of FormulationQ3DEStarStiffness

   Stiffness term for Quasi 3D full-wave eigenvalue Maxwell problems: HCurl part
*/

namespace sf{
  class FormulationQ3DEStarStiffnessThree: public FormulationBlock<Complex>{
  private:
    friend class FormulationQ3DEStarStiffness;

  private:
    // Function Space & Domain //
    const FunctionSpace1Form* fs;
    const GroupOfElement*     ddomain;

    
    // parameters
    int n; 
    
     // local term // 
    const TermGradGrad<Complex>* localGIn;

  private:
    FormulationQ3DEStarStiffnessThree(void);
    FormulationQ3DEStarStiffnessThree(const GroupOfElement& domain,
                                      const FunctionSpace1Form& fs,
                                      int n, 
                                      const TermGradGrad<Complex>& localGIn
                                       );

  public:
    virtual ~FormulationQ3DEStarStiffnessThree(void);

    virtual Complex weak(size_t dofI, size_t dofJ, size_t elementId) const;
    virtual Complex rhs(size_t equationI, size_t elementId)          const;

    virtual const FunctionSpace&   field(void) const;
    virtual const FunctionSpace&    test(void) const;
    virtual const GroupOfElement& domain(void) const;

    virtual bool isBlock(void) const;
  };
}

/**
   @fn FormulationQ3DEStarStiffnessThree::~FormulationQ3DEStarStiffnessThree
   Deletes this FormulationQ3DEStarStiffnessThree
*/

#endif
