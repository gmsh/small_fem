#include "SmallFem.h"
#include "Exception.h"
#include "FormulationQ3DErzEphiCoulomb.h"

#include "FormulationQ3DBlockGradF.h"
#include "FormulationQ3DBlockFGrad.h"
#include "FormulationQ3DBlockRadialFGrad.h"
#include "FormulationQ3DBlockGradRadialF.h"
#include "FormulationQ3DBlockFF0Form.h"

using namespace std;
using namespace sf;

void FormulationQ3DErzEphiCoulomb::I(fullVector<double>& xyz,
                                     fullMatrix<Complex>& T){
  T.scale(0);
  T(0, 0) = 1;
  T(1, 1) = 1;
  T(2, 2) = 1;
}


FormulationQ3DErzEphiCoulomb::
FormulationQ3DErzEphiCoulomb(const GroupOfElement& dom,
                             const FunctionSpace0Form& hPhi,
                             const FunctionSpace1Form& hRZ,
                             const FunctionSpace0Form& hXi,
                             int n,
                             int order){

  // Check domain stats: uniform mesh //
  pair<bool, size_t> uniform = dom.isUniform();
  size_t               eType = uniform.second;

  if(!uniform.first)
    throw Exception("FormulationQ3DErzEphiCoulomb needs a uniform mesh");

  this->bPhi = hPhi.getBasis(eType).copy();
  this->bRZ  =  hRZ.getBasis(eType).copy();
  this->bXi  =  hXi.getBasis(eType).copy();

  Quadrature gauss(eType,order, 1);

  this->gC = new fullMatrix<double>(gauss.getPoints());
  this->gW = new fullVector<double>(gauss.getWeights());
  int G = gW->size();

  bPhi->preEvaluateDerivatives(*gC);
  bPhi->preEvaluateFunctions(*gC);

  bRZ->preEvaluateDerivatives(*gC);
  bRZ->preEvaluateFunctions(*gC);

  bXi->preEvaluateDerivatives(*gC);
  bXi->preEvaluateFunctions(*gC);

  this->jac = new GroupOfJacobian(dom, *gC, "both");

  double overN = 1.0/double(n);

  switch(n){
  case 0:
    throw Exception("TODO");
    break;

  default:
    fList.push_back
      (new FormulationQ3DBlockGradF(dom, hXi, hRZ, +overN, 2, *bRZ, *bXi,
                                    *jac, G, *gC, *gW, I));
    fList.push_back
      (new FormulationQ3DBlockRadialFGrad(dom, hPhi, hXi, -overN, 1, *bPhi,*bXi,
                                          *jac, G, *gC, *gW, I));
    fList.push_back
      (new FormulationQ3DBlockFF0Form(dom, hXi,  hPhi, -n, 0, *bXi, *bPhi,
                                      *jac, G, *gC, *gW, I));

    fList.push_back
      (new FormulationQ3DBlockFGrad(dom, hXi,  hRZ, +overN, 2, *bXi, *bRZ,
                                    *jac, G, *gC, *gW, I));
    fList.push_back
      (new FormulationQ3DBlockGradRadialF(dom, hXi, hPhi, -overN, 1, *bXi,*bPhi,
                                          *jac, G, *gC, *gW, I));
    fList.push_back
      (new FormulationQ3DBlockFF0Form(dom, hPhi, hXi, -n, 0, *bPhi, *bXi,
                                      *jac, G, *gC, *gW, I));

    break;
  }
}

FormulationQ3DErzEphiCoulomb::~FormulationQ3DErzEphiCoulomb(void){
  list<FormulationBlock<Complex>*>::iterator end = fList.end();
  list<FormulationBlock<Complex>*>::iterator it  = fList.begin();

  for(; it !=end; it++)
    delete *it;

  delete bPhi;
  delete bRZ;
  delete bXi;

  delete gC;
  delete gW;
  delete jac;
}

const list<FormulationBlock<Complex>*>&
FormulationQ3DErzEphiCoulomb::getFormulationBlocks(void) const{
  return fList;
}

bool FormulationQ3DErzEphiCoulomb::isBlock(void) const{
  return false;
}
