#include "SmallFem.h"
#include "BasisGenerator.h"
#include "GroupOfJacobian.h"
#include "Quadrature.h"

#include "FormulationCoulombOne.h"

using namespace std;
using namespace sf;

FormulationCoulombOne::FormulationCoulombOne(void){
}

FormulationCoulombOne::FormulationCoulombOne(const GroupOfElement& domain,
                                             const FunctionSpace0Form& field,
                                             const FunctionSpace1Form& test,
                                             const TermGradGrad<double>& term){
  // Save Data //
  this->ddomain = &domain;
  this->ffield  = &field;
  this->ttest   = &test;
  this->term    = &term;
}

FormulationCoulombOne::~FormulationCoulombOne(void){
}

double FormulationCoulombOne::
weak(size_t dofI, size_t dofJ, size_t elementId) const{
  return term->getTerm(dofI, dofJ, elementId);
}

double FormulationCoulombOne::rhs(size_t equationI, size_t elementId) const{
  return 0;
}

const FunctionSpace& FormulationCoulombOne::field(void) const{
  return *ffield;
}

const FunctionSpace& FormulationCoulombOne::test(void) const{
  return *ttest;
}

const GroupOfElement& FormulationCoulombOne::domain(void) const{
  return *ddomain;
}

bool FormulationCoulombOne::isBlock(void) const{
  return true;
}
