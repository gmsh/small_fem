#include "SmallFem.h"
#include "FormulationQ3DEStarMassTwo.h"

#include "Quadrature.h"
#include "Mapper.h"

#include "TermFieldField.h"
#include "TermGradGrad.h"

using namespace std;
using namespace sf;

double FormulationQ3DEStarMassTwo::C0 = 299792458;

FormulationQ3DEStarMassTwo::FormulationQ3DEStarMassTwo(void){
}

FormulationQ3DEStarMassTwo::
FormulationQ3DEStarMassTwo(const GroupOfElement& domain,
                             const FunctionSpace0Form& fs,
                             const TermFieldField<Complex>& localFF
                            ){
  
  // Save Data //
  this->ddomain = &domain;
  this->fs      = &fs;
  this->localFF = &localFF; 
}

FormulationQ3DEStarMassTwo::~FormulationQ3DEStarMassTwo(void){
}

Complex FormulationQ3DEStarMassTwo::
weak(size_t dofI, size_t dofJ, size_t elementId) const{

  Complex c(1, 0); 
  return c * localFF->getTerm(dofI, dofJ, elementId) / (C0*C0);
}

Complex FormulationQ3DEStarMassTwo::
rhs(size_t equationI, size_t elementId) const{
  return Complex(0, 0);
}

const FunctionSpace& FormulationQ3DEStarMassTwo::field(void) const{
  return *fs;
}

const FunctionSpace& FormulationQ3DEStarMassTwo::test(void) const{
  return *fs;
}

const GroupOfElement& FormulationQ3DEStarMassTwo::domain(void) const{
  return *ddomain;
}

bool FormulationQ3DEStarMassTwo::isBlock(void) const{
  return true;
}
