#include "SmallFem.h"
#include "FormulationQ3DEStarMassOne.h"

#include "Quadrature.h"
#include "Mapper.h"

#include "TermFieldField.h"
#include "TermGradGrad.h"

using namespace std;
using namespace sf;

double FormulationQ3DEStarMassOne::C0 = 299792458;

FormulationQ3DEStarMassOne::FormulationQ3DEStarMassOne(void){
}

FormulationQ3DEStarMassOne::
FormulationQ3DEStarMassOne(const GroupOfElement& domain,
                             const FunctionSpace1Form& fs,
                             const TermGradGrad<Complex>& localGG
                            ){

  // Save Data //
  this->ddomain = &domain;
  this->fs      = &fs;
  this->localGG = &localGG;
}

FormulationQ3DEStarMassOne::~FormulationQ3DEStarMassOne(void){
}

Complex FormulationQ3DEStarMassOne::
weak(size_t dofI, size_t dofJ, size_t elementId) const{
    Complex c(1, 0); 
    return c * localGG->getTerm(dofI, dofJ, elementId) / (C0*C0);
  
}

Complex FormulationQ3DEStarMassOne::
rhs(size_t equationI, size_t elementId) const{
  return Complex(0, 0);
}

const FunctionSpace& FormulationQ3DEStarMassOne::field(void) const{
  return *fs;
}

const FunctionSpace& FormulationQ3DEStarMassOne::test(void) const{
  return *fs;
}

const GroupOfElement& FormulationQ3DEStarMassOne::domain(void) const{
  return *ddomain;
}

bool FormulationQ3DEStarMassOne::isBlock(void) const{
  return true;
}
