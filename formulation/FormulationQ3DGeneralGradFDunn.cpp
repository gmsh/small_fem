#include "SmallFem.h"
#include "Exception.h"

#include "FormulationQ3DGeneralGradFDunn.h"
#include "FormulationQ3DBlockGradF.h"
#include "FormulationQ3DBlockRadialFF.h"
#include "FormulationQ3DBlockFF0Form.h"
#include "FormulationQ3DBlockGradRadialF.h"


using namespace std;
using namespace sf;

FormulationQ3DGeneralGradFDunn::FormulationQ3DGeneralGradFDunn(const GroupOfElement& domain,
                          const FunctionSpace0Form& fsGrad,      
                          const FunctionSpace1Form& fsCurl,
                          double multConst, 
                          double exponent,
                          int n, 
                          const Basis& basisGrad, 
                          const Basis& basisCurl, 
                          const GroupOfJacobian& jac,
                          int G,
                          const fullMatrix<double>& gC,
                          const fullVector<double>& gW,
                          void (*f)(fullVector<double>&, fullMatrix<Complex>&)
                                            ){

      fList.push_back(
          new FormulationQ3DBlockGradF (domain, fsGrad, fsCurl, multConst/(static_cast<double>(n)), 
                                        exponent + 2.0, basisCurl, basisGrad, jac, G, gC, gW, f));
      
      fList.push_back(
          new FormulationQ3DBlockRadialFF (domain, fsGrad, fsCurl, multConst/(static_cast<double>(n)), 
                                        exponent + 1.0, basisCurl, basisGrad, jac, G, gC, gW, f));
          
      fList.push_back(
          new FormulationQ3DBlockFF0Form (domain, fsGrad, -1.0 * multConst/(static_cast<double>(n)), 
                                         exponent , basisGrad, jac, G, gC, gW, f));
      
      fList.push_back(
          new FormulationQ3DBlockGradRadialF (
              domain, fsGrad, -1.0 * multConst/(static_cast<double>(n)), exponent + 1.0, basisGrad, jac, G, gC, gW, f));
      
}

FormulationQ3DGeneralGradFDunn::~FormulationQ3DGeneralGradFDunn(void){
  list<FormulationBlock<Complex>*>::iterator end = fList.end();
  list<FormulationBlock<Complex>*>::iterator it  = fList.begin();

  for(; it !=end; it++)
    delete *it;
  
}

const list<FormulationBlock<Complex>*>&
FormulationQ3DGeneralGradFDunn::getFormulationBlocks(void) const{
  return fList;
}

bool FormulationQ3DGeneralGradFDunn::isBlock(void) const{
  return false;
}
 
