#include "SmallFem.h"
#include "Exception.h"

#include "FormulationQ3DGeneralFF1Form.h"
#include "FormulationQ3DBlockFF1Form.h"

using namespace std;
using namespace sf;

FormulationQ3DGeneralFF1Form::FormulationQ3DGeneralFF1Form(const GroupOfElement& domain,
                          const FunctionSpace1Form& fs,
                          double multConst, 
                          double exponent, 
                          const Basis& basis, 
                          const GroupOfJacobian& jac,
                          int G,
                          const fullMatrix<double>& gC,
                          const fullVector<double>& gW,
                          void (*f)(fullVector<double>&, fullMatrix<Complex>&)
                                            ){

      fList.push_back(
          new FormulationQ3DBlockFF1Form (
              domain, fs, multConst, exponent, basis, jac, G, gC, gW, f));
}

FormulationQ3DGeneralFF1Form::~FormulationQ3DGeneralFF1Form(void){
  list<FormulationBlock<Complex>*>::iterator end = fList.end();
  list<FormulationBlock<Complex>*>::iterator it  = fList.begin();

  for(; it !=end; it++)
    delete *it;
  
}

const list<FormulationBlock<Complex>*>&
FormulationQ3DGeneralFF1Form::getFormulationBlocks(void) const{
  return fList;
}

bool FormulationQ3DGeneralFF1Form::isBlock(void) const{
  return false;
}
 
