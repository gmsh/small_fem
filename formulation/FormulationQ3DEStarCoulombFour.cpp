#include "SmallFem.h"
#include "FormulationQ3DEStarCoulombFour.h"

#include "Quadrature.h"
#include "Mapper.h"

#include "TermFieldField.h"
#include "TermGradGrad.h"

using namespace std;
using namespace sf;

FormulationQ3DEStarCoulombFour::FormulationQ3DEStarCoulombFour(void){
}

FormulationQ3DEStarCoulombFour::
FormulationQ3DEStarCoulombFour(const GroupOfElement& domain,
                               const FunctionSpace0Form& field,
                               const FunctionSpace0Form& test,
                               const TermFieldField<Complex>& lXPhi,
                               int N){

  // Save Data //
  this->ddomain = &domain;
  this->ffield  = &field;
  this->ttest   = &test;
  this->lXPhi   = &lXPhi;
  this->minusN  = Complex(-N, 0);
}

FormulationQ3DEStarCoulombFour::~FormulationQ3DEStarCoulombFour(void){
}

Complex FormulationQ3DEStarCoulombFour::
weak(size_t dofI, size_t dofJ, size_t elementId) const{
  return minusN * lXPhi->getTerm(dofJ, dofI, elementId);
  // ! Transposed version of FormulationQ3DEStarCoulombTwo
  // (see FormulationQ3DEStarCoulomb.cpp)
}

Complex FormulationQ3DEStarCoulombFour::
rhs(size_t equationI, size_t elementId) const{
  return Complex(0, 0);
}

const FunctionSpace& FormulationQ3DEStarCoulombFour::field(void) const{
  return *ffield;
}

const FunctionSpace& FormulationQ3DEStarCoulombFour::test(void) const{
  return *ttest;
}

const GroupOfElement& FormulationQ3DEStarCoulombFour::domain(void) const{
  return *ddomain;
}

bool FormulationQ3DEStarCoulombFour::isBlock(void) const{
  return true;
}
