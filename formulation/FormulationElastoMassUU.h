#ifndef _FORMULATIONELASTOMASSUU_H_
#define _FORMULATIONELASTOMASSUU_H_

#include "FormulationBlock.h"
#include "FormulationElastoMass.h"

/**
   @class FormulationElastoMassUU
   @brief Formulation for Elastodynamic (helping formulation for mass UU)

   Formulation for Elastodynamic (helping formulation for mass UU)
 */

namespace sf{
  template<typename scalar>
  class FormulationElastoMassUU: public FormulationBlock<scalar>{
  private:
    friend class FormulationElastoMass<scalar>;

  private:
    // Denstity //
    double rho;

    // Function Space & Domain //
    const FunctionSpace0Form* ffield;
    const FunctionSpace0Form* ttest;
    const GroupOfElement*     ddomain;

    // Local Terms //
    const TermFieldField<scalar>* term;

  private:
    FormulationElastoMassUU(void);
    FormulationElastoMassUU(const GroupOfElement& domain,
                            const FunctionSpace0Form& fSpace,
                            const TermFieldField<scalar>& term,
                            double rho);

  public:
    virtual ~FormulationElastoMassUU(void);

    virtual scalar weak(size_t dofI, size_t dofJ, size_t elementId) const;
    virtual scalar rhs(size_t equationI, size_t elementId)          const;

    virtual const FunctionSpace&   field(void) const;
    virtual const FunctionSpace&    test(void) const;
    virtual const GroupOfElement& domain(void) const;

    virtual bool isBlock(void) const;
  };
}

/**
   @fn FormulationElastoMassUU::~FormulationElastoMassUU
   Deletes this FormulationElastoMassUU
*/

#endif
