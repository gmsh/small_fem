#ifndef _FORMULATIONQ3DPARAMGRADGRAD_H_
#define _FORMULATIONQ3DPARAMGRADGRAD_H_

#include "SmallFem.h"
#include "FunctionSpace1Form.h"
#include "GroupOfJacobian.h"

#include "FormulationBlock.h"
#include "FormulationQ3DGeneralGradGrad.h"

namespace sf{
  class FormulationQ3DBlockGradGrad: public FormulationBlock<Complex>{
  private:
    friend class FormulationQ3DGeneralGradGrad;

  private:
    // multiplicative constant and exponent 
    double multConst; 
    double exponent; 
      
    // Function Space & Domain //
    const FunctionSpace0Form* fs;
    const GroupOfElement*     ddomain;

    // Gaussian Quadrature Data //
    int G;
    const fullMatrix<double>* gC;
    const fullVector<double>* gW;

    // Jacobians and basis//
    const GroupOfJacobian* jac;
    const Basis*         basis;
    
    void (*f)(fullVector<double>&, fullMatrix<Complex>&); 

  private:
    FormulationQ3DBlockGradGrad(void);
    FormulationQ3DBlockGradGrad(const GroupOfElement& domain,
                          const FunctionSpace0Form& fs,
                          double multConst, 
                          double exponent, 
                          const Basis& basis, 
                          const GroupOfJacobian& jac,
                          int G,
                          const fullMatrix<double>& gC,
                          const fullVector<double>& gW,
                          void (*f)(fullVector<double>&, fullMatrix<Complex>&)
                             );

  public:
    virtual ~FormulationQ3DBlockGradGrad(void);

    virtual Complex weak(size_t dofI, size_t dofJ, size_t elementId) const;
    virtual Complex rhs(size_t equationI, size_t elementId)          const;

    virtual const FunctionSpace&   field(void) const;
    virtual const FunctionSpace&    test(void) const;
    virtual const GroupOfElement& domain(void) const;

    virtual bool isBlock(void) const;
  };
}

#endif
 
