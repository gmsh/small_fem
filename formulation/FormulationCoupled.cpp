#include "SmallFem.h"
#include "FormulationCoupled.h"

using namespace sf;

template<typename scalar>
FormulationCoupled<scalar>::~FormulationCoupled(void){
}

template<typename scalar>
bool FormulationCoupled<scalar>::isBlock(void) const{
  return false;
}

////////////////////////////
// Explicit instantiation //
////////////////////////////
template class sf::FormulationCoupled<Complex>;
template class sf::FormulationCoupled<double>;
