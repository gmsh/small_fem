#ifndef _FORMULATIONQ3DESTARMASSONE_H_
#define _FORMULATIONQ3DESTARMASSONE_H_

#include "SmallFem.h"
#include "FunctionSpace1Form.h"
#include "GroupOfJacobian.h"

#include "FormulationBlock.h"
#include "FormulationQ3DEStarMass.h"

#include "TermFieldField.h"
#include "TermGradGrad.h"

/**
   @class FormulationQ3DEStarMassOne
   @brief HCurl part of FormulationQ3DEStarMass

   Mass term for Quasi 3D full-wave eigenvalue Maxwell problems: HCurl part
*/

namespace sf{
  class FormulationQ3DEStarMassOne: public FormulationBlock<Complex>{
  private:
    friend class FormulationQ3DEStarMass;

  private:
    static double C0;

  private:
    // Function Space & Domain //
    const FunctionSpace1Form* fs;
    const GroupOfElement*     ddomain;

    
    // local term // 
    const TermGradGrad<Complex>* localGG;


  private:
    FormulationQ3DEStarMassOne(void);
    FormulationQ3DEStarMassOne(const GroupOfElement& domain,
                                 const FunctionSpace1Form& fs, 
                                 const TermGradGrad<Complex>& localGG
                                );

  public:
    virtual ~FormulationQ3DEStarMassOne(void);

    virtual Complex weak(size_t dofI, size_t dofJ, size_t elementId) const;
    virtual Complex rhs(size_t equationI, size_t elementId)          const;

    virtual const FunctionSpace&   field(void) const;
    virtual const FunctionSpace&    test(void) const;
    virtual const GroupOfElement& domain(void) const;

    virtual bool isBlock(void) const;
  };
}

/**
   @fn FormulationQ3DEStarMassOne::~FormulationQ3DEStarMassOne
   Deletes this FormulationQ3DEStarMassOne
*/

#endif
