#ifndef _FORMULATIONQ3DESTARSTIFFNESSONE_H_
#define _FORMULATIONQ3DESTARSTIFFNESSONE_H_

#include "SmallFem.h"
#include "FunctionSpace1Form.h"
#include "GroupOfJacobian.h"

#include "FormulationBlock.h"
#include "FormulationQ3DEStarStiffness.h"

#include "TermFieldField.h"
#include "TermGradGrad.h"
#include "TermCurlCurl.h"

/**
   @class FormulationQ3DEStarStiffnessOne
   @brief HCurl part of FormulationQ3DEStarStiffness

   Stiffness term for Quasi 3D full-wave eigenvalue Maxwell problems: HCurl part
*/

namespace sf{
  class FormulationQ3DEStarStiffnessOne: public FormulationBlock<Complex>{
  private:
    friend class FormulationQ3DEStarStiffness;

  private:
    // Function Space & Domain //
    const FunctionSpace1Form* fs;
    const GroupOfElement*     ddomain;

    
    // local term // 
    const TermCurlCurl<Complex>* localCC;

  private:
    FormulationQ3DEStarStiffnessOne(void);
    FormulationQ3DEStarStiffnessOne(const GroupOfElement& domain,
                                      const FunctionSpace1Form& fs,
                                      const TermCurlCurl<Complex>& localCC);

  public:
    virtual ~FormulationQ3DEStarStiffnessOne(void);

    virtual Complex weak(size_t dofI, size_t dofJ, size_t elementId) const;
    virtual Complex rhs(size_t equationI, size_t elementId)          const;

    virtual const FunctionSpace&   field(void) const;
    virtual const FunctionSpace&    test(void) const;
    virtual const GroupOfElement& domain(void) const;

    virtual bool isBlock(void) const;
  };
}

/**
   @fn FormulationQ3DEStarStiffnessOne::~FormulationQ3DEStarStiffnessOne
   Deletes this FormulationQ3DEStarStiffnessOne
*/

#endif
