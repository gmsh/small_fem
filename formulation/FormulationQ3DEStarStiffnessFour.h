#ifndef _FORMULATIONQ3DESTARSTIFFNESSFOUR_H_
#define _FORMULATIONQ3DESTARSTIFFNESSFOUR_H_

#include "SmallFem.h"
#include "FunctionSpace0Form.h"
#include "FunctionSpace1Form.h"
#include "GroupOfJacobian.h"

#include "FormulationBlock.h"
#include "FormulationQ3DEStarStiffness.h"

#include "TermFieldField.h"
#include "TermGradGrad.h"
#include "TermCurlCurl.h"

/**
   @class FormulationQ3DEStarStiffnessFour
   @brief HGrad part of FormulationQ3DEStarStiffness

   Stiffness term for Quasi 3D full-wave eigenvalue Maxwell problems: HGrad part
*/

namespace sf{
  class FormulationQ3DEStarStiffnessFour: public FormulationBlock<Complex>{
  private:
    friend class FormulationQ3DEStarStiffness;

  private:
    // Function Space & Domain //
    const FunctionSpace0Form* ffield; 
    const FunctionSpace1Form* ttest;
    const GroupOfElement*     ddomain;

    
    // parameters
    int n; 

    // local term // 
    const TermGradGrad<Complex>* localGIn; 
    
  private:
    FormulationQ3DEStarStiffnessFour(void);
    FormulationQ3DEStarStiffnessFour(const GroupOfElement& domain,
                                      const FunctionSpace0Form& field,
                                      const FunctionSpace1Form& test,
                                      int n, 
                                      const TermGradGrad<Complex>& localGIn
                                       );

  public:
    virtual ~FormulationQ3DEStarStiffnessFour(void);

    virtual Complex weak(size_t dofI, size_t dofJ, size_t elementId) const;
    virtual Complex rhs(size_t equationI, size_t elementId)          const;

    virtual const FunctionSpace&   field(void) const;
    virtual const FunctionSpace&    test(void) const;
    virtual const GroupOfElement& domain(void) const;

    virtual bool isBlock(void) const;
  };
}

/**
   @fn FormulationQ3DEStarStiffnessFour::~FormulationQ3DEStarStiffnessFour
   Deletes this FormulationQ3DEStarStiffnessFour
*/

#endif
