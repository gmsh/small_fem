#include "FormulationBeynIBCLeontovich.h"
#include "GroupOfJacobian.h"
#include "Quadrature.h"
#include "Exception.h"

#include "TermFieldField.h"
#include "TermGradGrad.h"
#include "TermCurlCurl.h"
#include "TermDummy.h"

using namespace std;
using namespace sf;

const Complex FormulationBeynIBCLeontovich::J   = Complex(0, 1);
const double  FormulationBeynIBCLeontovich::Pi  = atan(1.0) * 4;
const double  FormulationBeynIBCLeontovich::Mu0 = 4 * Pi * 1e-7;

FormulationBeynIBCLeontovich::
FormulationBeynIBCLeontovich(const GroupOfElement& domain,
                             const FunctionSpace1Form& fs,
                             BeynContext& context,
                             Complex sigma){
  // Save Data //
  ddomain      = &domain;
  ffs          = &fs;
  ctx          = &context;
  this->sigma  = sigma;
  this->omega  = ctx->getLambda();

  // Is domain empty ? //
  if(domain.isEmpty()){
    term = new TermDummy<double>;
    return;
  }

  // Check domain stats: uniform mesh //
  pair<bool, size_t> uniform = domain.isUniform();
  size_t               eType = uniform.second;

  if(!uniform.first)
    throw Exception("FormulationBeynIBCLeontovich needs a uniform mesh");

  // Get Basis //
  const Basis& basis = fs.getBasis(eType);
  const size_t order = basis.getOrder();

  // Gaussian Quadrature //
  Quadrature gauss(eType, order, 2);
  const fullMatrix<double>& gC = gauss.getPoints();

  // Functions //
  basis.preEvaluateFunctions(gC);

  // Local Terms //
  GroupOfJacobian jac(domain, gC, "invert");
  term = new TermGradGrad<double>(jac, basis, gauss);
}

FormulationBeynIBCLeontovich::~FormulationBeynIBCLeontovich(void){
  delete term;
}

Complex FormulationBeynIBCLeontovich::
weak(size_t dofI, size_t dofJ, size_t elementId) const{
  return
    J*omega * sqrt((J*omega*Mu0)/sigma) * term->getTerm(dofI, dofJ, elementId);
}

Complex FormulationBeynIBCLeontovich::
rhs(size_t equationI, size_t elementId) const{
  return Complex(0, 0);
}

const FunctionSpace& FormulationBeynIBCLeontovich::field(void) const{
  return *ffs;
}

const FunctionSpace& FormulationBeynIBCLeontovich::test(void) const{
  return *ffs;
}

const GroupOfElement& FormulationBeynIBCLeontovich::domain(void) const{
  return *ddomain;
}

bool FormulationBeynIBCLeontovich::isBlock(void) const{
  return true;
}

void FormulationBeynIBCLeontovich::update(void){
  omega  = ctx->getLambda();
}
