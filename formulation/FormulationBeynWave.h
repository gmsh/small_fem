#ifndef _FORMULATIONBEYNWAVE_H_
#define _FORMULATIONBEYNWAVE_H_

#include "SmallFem.h"
#include "BeynContext.h"
#include "FormulationBlock.h"
#include "GroupOfElement.h"
#include "FunctionSpace1Form.h"
#include "Term.h"

/**
   @class FormulationBeynWave
   @brief Wave formulation for the Beyn's method (h-formulation)

   Wave formulation for the Beyn's method (h-formulation)
 */

namespace sf{
  class FormulationBeynWave: public FormulationBlock<Complex>{
  private:
    // Function Space & Domain //
    const FunctionSpace1Form* ffs;
    const GroupOfElement*     ddomain;

    // Beyn Context //
    BeynContext* ctx;

    // Squared angular frequency //
    Complex omegaSq;

    // Physics //
    double mu;
    double eps;

    // Local Terms //
    Term<double>* stif;
    Term<double>* mass;

  public:
    FormulationBeynWave(const GroupOfElement& domain,
                        const FunctionSpace1Form& fs,
                        BeynContext& context,
                        double mu,
                        double eps);

    virtual ~FormulationBeynWave(void);

    virtual Complex weak(size_t dofI, size_t dofJ, size_t elementId) const;
    virtual Complex rhs(size_t equationI, size_t elementId)          const;

    virtual const FunctionSpace&   field(void) const;
    virtual const FunctionSpace&    test(void) const;
    virtual const GroupOfElement& domain(void) const;

    virtual bool isBlock(void) const;
    virtual void update(void);
  };
}

/**
   @fn FormulationBeynWave::FormulationBeynWave
   @param domain A GroupOfElement
   @param fs A FunctionSpace1Form for both unknown and test field
   @param context k a BeynContext
   @param mu a real number
   @param eps a real number

   Instantiates a new FormulationBeynWave with given parametres:
   @li domain for the domain of this Formulation
   @li fs for the function space used for the unknown field
       and the test functions
   @li context the BeynContext containing the current angular frequency
   @li mu the magnetic permeability
   @li eps the electric permittivity
   **

   @fn FormulationBeynWave::~FormulationBeynWave
   Deletes this FormulationBeynWave
   **

   @fn FormulationBeynWave::update
   Asks this FormulationBeynWave for the new angular frequency and propagate
*/
#endif
