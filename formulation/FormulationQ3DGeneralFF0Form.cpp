#include "SmallFem.h"
#include "Exception.h"

#include "FormulationQ3DGeneralFF0Form.h"
#include "FormulationQ3DBlockFF0Form.h"

using namespace std;
using namespace sf;

FormulationQ3DGeneralFF0Form::FormulationQ3DGeneralFF0Form(const GroupOfElement& domain,
                          const FunctionSpace0Form& fs,
                          double multConst, 
                          double exponent, 
                          const Basis& basis, 
                          const GroupOfJacobian& jac,
                          int G,
                          const fullMatrix<double>& gC,
                          const fullVector<double>& gW,
                          void (*f)(fullVector<double>&, fullMatrix<Complex>&)
                                            ){

      fList.push_back(
          new FormulationQ3DBlockFF0Form (
              domain, fs, multConst, exponent, basis, jac, G, gC, gW, f));
}

FormulationQ3DGeneralFF0Form::~FormulationQ3DGeneralFF0Form(void){
  list<FormulationBlock<Complex>*>::iterator end = fList.end();
  list<FormulationBlock<Complex>*>::iterator it  = fList.begin();

  for(; it !=end; it++)
    delete *it;
  
}

const list<FormulationBlock<Complex>*>&
FormulationQ3DGeneralFF0Form::getFormulationBlocks(void) const{
  return fList;
}

bool FormulationQ3DGeneralFF0Form::isBlock(void) const{
  return false;
}
 
