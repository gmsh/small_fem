#ifndef _FORMULATIONQ3DGENERALGRADFDUNN_H_
#define _FORMULATIONQ3DGENERALGRADFDUNN_H_

// include needed header files
#include "SmallFem.h"
#include "FormulationCoupled.h"
#include "FunctionSpace0Form.h"
#include "FunctionSpace1Form.h"
#include "GroupOfJacobian.h"
#include "Quadrature.h"

/**
   @class FormulationQ3DGeneralFGradDunn
   @brief General Term for function-gradient bilinear forms 
   

   Calculates multConst * (r^exponent Erz, grad(r*ephi')) 
                   = 1/n * multConst * {(r^(exponent+1) U, grad(r*v'))
                                           + (r^exponent [v;0], grad(r*v'))
                   = ... 
                    
       
   @author Erik Schnaubelt
*/

namespace sf{
  class FormulationQ3DGeneralGradFDunn: public FormulationCoupled<Complex>{
  private:
    friend class FormulationQ3DBlockStiffness;
    friend class FormulationQ3DBlockMass;

  
  private:
    std::list<FormulationBlock<Complex>*> fList;

  public:
    FormulationQ3DGeneralGradFDunn(const GroupOfElement& domain,
                          const FunctionSpace0Form& fsGrad,      
                          const FunctionSpace1Form& fsCurl,
                          double multConst, 
                          double exponent, 
                          int n, 
                          const Basis& basisGrad, 
                          const Basis& basisTest, 
                          const GroupOfJacobian& jac,
                          int G,
                          const fullMatrix<double>& gC,
                          const fullVector<double>& gW,
                          void (*f)(fullVector<double>&, fullMatrix<Complex>&)
                         );

    virtual ~FormulationQ3DGeneralGradFDunn(void);

    virtual const std::list<FormulationBlock<Complex>*>&
                                               getFormulationBlocks(void) const;
    virtual bool isBlock(void) const;
  };
}

#endif
 
