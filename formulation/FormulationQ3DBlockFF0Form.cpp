#include "SmallFem.h"
#include "FormulationQ3DBlockFF0Form.h"

#include "Quadrature.h"
#include "Mapper.h"

using namespace std;
using namespace sf;

FormulationQ3DBlockFF0Form::FormulationQ3DBlockFF0Form(void){
}

FormulationQ3DBlockFF0Form::
FormulationQ3DBlockFF0Form(const GroupOfElement& domain,
                           const FunctionSpace0Form& fs,
                           double multConst,
                           double exponent,
                           const Basis& basis,
                           const GroupOfJacobian& jac,
                           int G,
                           const fullMatrix<double>& gC,
                           const fullVector<double>& gW,
                           void (*f)(fullVector<double>&,
                                     fullMatrix<Complex>&)){
  // Save Data //
  this->ddomain   = &domain;
  this->ffield    = &fs;
  this->ttest     = &fs;
  this->multConst = multConst;
  this->exponent  = exponent;

  // Get Basis //
  this->basisField = &basis;
  this->basisTest  = &basis;
  this->jac        = &jac;
  this->gC         = &gC;
  this->gW         = &gW;
  this->G          = G;

  this->f          = f;
}

FormulationQ3DBlockFF0Form::
FormulationQ3DBlockFF0Form(const GroupOfElement& domain,
                           const FunctionSpace0Form& field,
                           const FunctionSpace0Form& test,
                           double multConst,
                           double exponent,
                           const Basis& basisField,
                           const Basis& basisTest,
                           const GroupOfJacobian& jac,
                           int G,
                           const fullMatrix<double>& gC,
                           const fullVector<double>& gW,
                           void (*f)(fullVector<double>&,
                                     fullMatrix<Complex>&)){
  // Save Data //
  this->ddomain   = &domain;
  this->ffield    = &field;
  this->ttest     = &test;
  this->multConst = multConst;
  this->exponent  = exponent;

  // Get Basis //
  this->basisField = &basisField;
  this->basisTest  = &basisTest;
  this->jac        = &jac;
  this->gC         = &gC;
  this->gW         = &gW;
  this->G          = G;

  this->f          = f;
}

FormulationQ3DBlockFF0Form::~FormulationQ3DBlockFF0Form(void){

}

Complex FormulationQ3DBlockFF0Form::
weak(size_t dofI, size_t dofJ, size_t elementId) const{
  // Init Some Stuff //
  // Init Some Stuff //
  double  phiI;
  double  phiJ;

  Complex integral = Complex(0, 0);
  double  det;
  double  pxyz[3];

  fullVector<double>  xyz(3);
  fullMatrix<Complex> TMatrix(3,3);
  Complex intMatPhiIPhiJ;

  // Get Element //
  const MElement& element = ddomain->get(elementId);

  // Get Basis Functions //
  const fullMatrix<double>& eField =
    basisField->getPreEvaluatedFunctions(element);
  const fullMatrix<double>& eTest  =
    basisTest->getPreEvaluatedFunctions(element);

  // Get Jacobians //
  const vector<pair<fullMatrix<double>, double> >& MJac =
    jac->getJacobianMatrix(elementId);

  // Loop over Integration Point //
  for(int g = 0; g < G; g++){
    det = MJac[g].second;   // det(Jac) at integration point g
    ReferenceSpaceManager::
      mapFromABCtoXYZ(element, (*gC)(g, 0), (*gC)(g, 1), (*gC)(g, 2), pxyz);
                            // xyz coordinates of point g

    phiI = eTest(dofI, g);   // eTest_I|g  in xyz-space
    phiJ = eField(dofJ, g);  // eField_J|g in xyz-space

    xyz(0) = pxyz[0];
    xyz(1) = pxyz[1];
    xyz(2) = pxyz[2];

    f(xyz, TMatrix);
    // WARNING! Only diagonal matrices possible!
    intMatPhiIPhiJ = TMatrix(2,2) * phiI * phiJ;


    integral +=
      intMatPhiIPhiJ * fabs(det) * (*gW)(g) * pow(pxyz[0],exponent);
  }

  // Done
  return multConst * integral;
}

Complex FormulationQ3DBlockFF0Form::
rhs(size_t equationI, size_t elementId) const{
  return Complex(0, 0);
}

const FunctionSpace& FormulationQ3DBlockFF0Form::field(void) const{
  return *ffield;
}

const FunctionSpace& FormulationQ3DBlockFF0Form::test(void) const{
  return *ttest;
}

const GroupOfElement& FormulationQ3DBlockFF0Form::domain(void) const{
  return *ddomain;
}

bool FormulationQ3DBlockFF0Form::isBlock(void) const{
  return true;
}
