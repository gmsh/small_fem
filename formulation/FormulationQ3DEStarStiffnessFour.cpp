#include "SmallFem.h"
#include "FormulationQ3DEStarStiffnessFour.h"

#include "Quadrature.h"
#include "Mapper.h"

#include "TermFieldField.h"
#include "TermGradGrad.h"
#include "TermCurlCurl.h"

using namespace std;
using namespace sf;

FormulationQ3DEStarStiffnessFour::FormulationQ3DEStarStiffnessFour(void){
}

FormulationQ3DEStarStiffnessFour::
FormulationQ3DEStarStiffnessFour(const GroupOfElement& domain,
                                  const FunctionSpace0Form& field,
                                  const FunctionSpace1Form& test,
                                  int n,
                                  const TermGradGrad<Complex>& localGIn
                                  ){
 

  // Save Data //
  this->ddomain = &domain;
  this->ffield  = &field;
  this->ttest   = &test; 
  this->n       = n; 
  this->localGIn = &localGIn; 
}

FormulationQ3DEStarStiffnessFour::~FormulationQ3DEStarStiffnessFour(void){
 
}

Complex FormulationQ3DEStarStiffnessFour::
weak(size_t dofI, size_t dofJ, size_t elementId) const{
  Complex c(1, 0); 
  return static_cast<double>(n) * c * localGIn->getTerm(dofI, dofJ, elementId);
}

Complex FormulationQ3DEStarStiffnessFour::
rhs(size_t equationI, size_t elementId) const{
  return Complex(0, 0);
}

const FunctionSpace& FormulationQ3DEStarStiffnessFour::field(void) const{
  return *ffield;
}

const FunctionSpace& FormulationQ3DEStarStiffnessFour::test(void) const{
  return *ttest;
}

const GroupOfElement& FormulationQ3DEStarStiffnessFour::domain(void) const{
  return *ddomain;
}

bool FormulationQ3DEStarStiffnessFour::isBlock(void) const{
  return true;
}
