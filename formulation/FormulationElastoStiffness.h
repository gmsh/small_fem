#ifndef _FORMULATIONELASTOSTIFFNESS_H_
#define _FORMULATIONELASTOSTIFFNESS_H_

#include "FunctionSpace0Form.h"
#include "FormulationCoupled.h"
#include "TermGradGrad.h"

/**
   @class FormulationElastoStiffness
   @brief Formulation for Elastodynamic (stiffness term)

   Formulation for Elastodynamic (stiffness term)
 */

namespace sf{
  template<typename scalar>
  class FormulationElastoStiffness: public FormulationCoupled<scalar>{
  private:
    // Hook // /!\ TODO: Find a cleaner way and remove static stuffs
    static bool   once;
    static double lambda;
    static double mu;

    static void Dxx(fullVector<double>& xyz, fullMatrix<scalar>& T);
    static void Dyx(fullVector<double>& xyz, fullMatrix<scalar>& T);
    static void Dzx(fullVector<double>& xyz, fullMatrix<scalar>& T);

    static void Dxy(fullVector<double>& xyz, fullMatrix<scalar>& T);
    static void Dyy(fullVector<double>& xyz, fullMatrix<scalar>& T);
    static void Dzy(fullVector<double>& xyz, fullMatrix<scalar>& T);

    static void Dxz(fullVector<double>& xyz, fullMatrix<scalar>& T);
    static void Dyz(fullVector<double>& xyz, fullMatrix<scalar>& T);
    static void Dzz(fullVector<double>& xyz, fullMatrix<scalar>& T);

    // Local Terms //
    TermGradGrad<scalar>* termXX;
    TermGradGrad<scalar>* termYX;
    TermGradGrad<scalar>* termZX;

    TermGradGrad<scalar>* termXY;
    TermGradGrad<scalar>* termYY;
    TermGradGrad<scalar>* termZY;

    TermGradGrad<scalar>* termXZ;
    TermGradGrad<scalar>* termYZ;
    TermGradGrad<scalar>* termZZ;

    // Formulations //
    std::list<FormulationBlock<scalar>*> fList;

  public:
    FormulationElastoStiffness(const GroupOfElement& domain,
                               const FunctionSpace0Form& hGradX,
                               const FunctionSpace0Form& hGradY,
                               const FunctionSpace0Form& hGradZ,
                               double E,
                               double nu);

    virtual ~FormulationElastoStiffness(void);

    virtual const std::list<FormulationBlock<scalar>*>&
                                               getFormulationBlocks(void) const;
    virtual bool isBlock(void) const;
  };
}

/**
   @fn FormulationElastoStiffness::FormulationElastoStiffness
   @param domain A GroupOfElement
   @param hGradX A FunctionSpace0Form for the displacement along x
   @param hGradY A FunctionSpace0Form for the displacement along y
   @param hGradZ A FunctionSpace0Form for the displacement along z
   @param E The Young modulus
   @param nu The Poisson ratio

   Instantiates a new FormulationElastoStiffness with the given parametres
   **

   @fn FormulationElastoStiffness::~FormulationElastoStiffness
   Deletes this FormulationElastoStiffness
*/

#endif
