#ifndef _FORMULATIONQ3DGENERALGRADF_H_
#define _FORMULATIONQ3DGENERALGRADF_H_

// include needed header files
#include "SmallFem.h"
#include "FormulationCoupled.h"
#include "FunctionSpace0Form.h"
#include "FunctionSpace1Form.h"
#include "GroupOfJacobian.h"
#include "Quadrature.h"

/**
   @class FormulationQ3DGeneralGradF
   @brief General Term for gradient-function bilinear forms 
   

   Calculates multConst * (r^(alpha + 1) grad (r^beta u), V) = 
       multConst * (r^(alpha + beta + 1) grad u, V) + 
       multConst * beta * (r^(alpha + beta) [u ; 0], V) +
       
   @author Erik Schnaubelt
*/

namespace sf{
  class FormulationQ3DGeneralGradF: public FormulationCoupled<Complex>{
  private:
    friend class FormulationQ3DBlockStiffness;
    friend class FormulationQ3DBlockMass;
  
  private:
    std::list<FormulationBlock<Complex>*> fList;

  public:
    FormulationQ3DGeneralGradF(const GroupOfElement& domain,
                          const FunctionSpace0Form& fsGrad,      
                          const FunctionSpace1Form& fsCurl,
                          double multConst, 
                          double alpha, 
                          double beta, 
                          const Basis& basisTest, 
                          const Basis& basisField, 
                          const GroupOfJacobian& jac,
                          int G,
                          const fullMatrix<double>& gC,
                          const fullVector<double>& gW,
                          void (*f)(fullVector<double>&, fullMatrix<Complex>&)
                         );

    virtual ~FormulationQ3DGeneralGradF(void);

    virtual const std::list<FormulationBlock<Complex>*>&
                                               getFormulationBlocks(void) const;
    virtual bool isBlock(void) const;
  };
}

#endif
 
