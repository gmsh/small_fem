#include "SmallFem.h"
#include "Exception.h"
#include "FormulationQ3DEStarStiffness.h"
#include "FormulationQ3DEStarStiffnessOne.h"
#include "FormulationQ3DEStarStiffnessTwo.h"
#include "FormulationQ3DEStarStiffnessThree.h"
#include "FormulationQ3DEStarStiffnessFour.h"
#include "FormulationQ3DEStarStiffnessFive.h"

#include "TermFieldField.h"
#include "TermGradGrad.h"
#include "TermCurlCurl.h"

using namespace std;
using namespace sf;

FormulationQ3DEStarStiffness::
FormulationQ3DEStarStiffness(const GroupOfElement& domain,
                             const FunctionSpace0Form& hGrad,
                             const FunctionSpace1Form& hCurl,
                             int n,
                             void (*r)(fullVector<double>&,
                                       fullMatrix<Complex>&),
                             void (*invR)(fullVector<double>&,
                                          fullMatrix<Complex>&),
                             int order){

  pair<bool, size_t> uniform = domain.isUniform();
  size_t               eType = uniform.second;

  if(!uniform.first)
    throw Exception("FormulationQ3DEStarStiffness needs a uniform mesh");

  const Basis& basisGrad = hGrad.getBasis(eType);
  const Basis& basisCurl = hCurl.getBasis(eType);

  int gOrder;
  if(order < 0)
    gOrder = max(basisGrad.getOrder(), basisCurl.getOrder()) * 2;
  else
    gOrder = order;
  Quadrature gauss(eType, gOrder, 1);

  const fullMatrix<double>& gC = gauss.getPoints();
  basisGrad.preEvaluateFunctions(gC);
  basisGrad.preEvaluateDerivatives(gC);

  basisCurl.preEvaluateFunctions(gC);
  basisCurl.preEvaluateDerivatives(gC);

  GroupOfJacobian jac(domain, gC, "both");

  localGIn  = new TermGradGrad<Complex> (jac, basisGrad, basisCurl,gauss,invR);
  localInG  = new TermGradGrad<Complex> (jac, basisCurl, basisGrad,gauss,invR);
  localInIn = new TermGradGrad<Complex> (jac, basisCurl, gauss, invR);
  localGG   = new TermGradGrad<Complex> (jac, basisGrad, gauss, invR);
  localCC   = new TermCurlCurl<Complex> (jac, basisCurl, gauss, r);

  // Formulations //
  // NB: FormulationQ3DEStarStiffness is a friend of
  //     FormulationQ3DEStarStiffness{One,Two} !
  //     So it can instanciate those classes...
  fList.push_back
    (new FormulationQ3DEStarStiffnessOne  (domain, hCurl, *localCC));
  fList.push_back
    (new FormulationQ3DEStarStiffnessTwo  (domain, hGrad, *localGG));
  fList.push_back
    (new FormulationQ3DEStarStiffnessThree(domain, hCurl, n, *localInIn));
  fList.push_back
    (new FormulationQ3DEStarStiffnessFour (domain, hGrad, hCurl, n, *localGIn));
  fList.push_back
    (new FormulationQ3DEStarStiffnessFive (domain, hGrad, hCurl, n, *localInG));
}

FormulationQ3DEStarStiffness::~FormulationQ3DEStarStiffness(void){
  // Iterate & Delete Formulations //
  list<FormulationBlock<Complex>*>::iterator end = fList.end();
  list<FormulationBlock<Complex>*>::iterator it  = fList.begin();

  for(; it !=end; it++)
    delete *it;
}

const list<FormulationBlock<Complex>*>&
FormulationQ3DEStarStiffness::getFormulationBlocks(void) const{
  return fList;
}

bool FormulationQ3DEStarStiffness::isBlock(void) const{
  return false;
}
