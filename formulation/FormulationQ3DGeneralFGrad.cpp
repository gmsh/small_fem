#include "SmallFem.h"
#include "Exception.h"

#include "FormulationQ3DGeneralFGrad.h"
#include "FormulationQ3DBlockFGrad.h"
#include "FormulationQ3DBlockFRadialF.h"


using namespace std;
using namespace sf;

FormulationQ3DGeneralFGrad::FormulationQ3DGeneralFGrad(const GroupOfElement& domain,
                          const FunctionSpace0Form& fsGrad,      
                          const FunctionSpace1Form& fsCurl,
                          double multConst, 
                          double alpha, 
                          double beta, 
                          const Basis& basisTest, 
                          const Basis& basisField, 
                          const GroupOfJacobian& jac,
                          int G,
                          const fullMatrix<double>& gC,
                          const fullVector<double>& gW,
                          void (*f)(fullVector<double>&, fullMatrix<Complex>&)
                                            ){

      fList.push_back(
          new FormulationQ3DBlockFGrad (
              domain, fsGrad, fsCurl, multConst, alpha + beta + 1, basisTest, basisField, jac, G, gC, gW, f));
      if(multConst*beta != 0) {
          fList.push_back(
              new FormulationQ3DBlockFRadialF (
                  domain, fsGrad, fsCurl, multConst*beta, alpha + beta, basisTest, basisField, jac, G, gC, gW, f)    
                         );
      }
}

FormulationQ3DGeneralFGrad::~FormulationQ3DGeneralFGrad(void){
  list<FormulationBlock<Complex>*>::iterator end = fList.end();
  list<FormulationBlock<Complex>*>::iterator it  = fList.begin();

  for(; it !=end; it++)
    delete *it;
  
}

const list<FormulationBlock<Complex>*>&
FormulationQ3DGeneralFGrad::getFormulationBlocks(void) const{
  return fList;
}

bool FormulationQ3DGeneralFGrad::isBlock(void) const{
  return false;
}
 
