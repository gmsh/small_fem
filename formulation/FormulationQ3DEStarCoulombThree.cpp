#include "SmallFem.h"
#include "FormulationQ3DEStarCoulombThree.h"

#include "Quadrature.h"
#include "Mapper.h"

#include "TermFieldField.h"
#include "TermGradGrad.h"

using namespace std;
using namespace sf;

FormulationQ3DEStarCoulombThree::FormulationQ3DEStarCoulombThree(void){
}

FormulationQ3DEStarCoulombThree::
FormulationQ3DEStarCoulombThree(const GroupOfElement& domain,
                                const FunctionSpace1Form& field,
                                const FunctionSpace0Form& test,
                                const TermGradGrad<Complex>& lXRZ){

  // Save Data //
  this->ddomain = &domain;
  this->ffield  = &field;
  this->ttest   = &test;
  this->lXRZ    = &lXRZ;
}

FormulationQ3DEStarCoulombThree::~FormulationQ3DEStarCoulombThree(void){
}

Complex FormulationQ3DEStarCoulombThree::
weak(size_t dofI, size_t dofJ, size_t elementId) const{
  return lXRZ->getTerm(dofJ, dofI, elementId);
  // ! Transposed version of FormulationQ3DEStarCoulombOne
  // (see FormulationQ3DEStarCoulomb.cpp)
}

Complex FormulationQ3DEStarCoulombThree::
rhs(size_t equationI, size_t elementId) const{
  return Complex(0, 0);
}

const FunctionSpace& FormulationQ3DEStarCoulombThree::field(void) const{
  return *ffield;
}

const FunctionSpace& FormulationQ3DEStarCoulombThree::test(void) const{
  return *ttest;
}

const GroupOfElement& FormulationQ3DEStarCoulombThree::domain(void) const{
  return *ddomain;
}

bool FormulationQ3DEStarCoulombThree::isBlock(void) const{
  return true;
}
