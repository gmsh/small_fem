#ifndef _FORMULATIONQ3DGENERALCURLCURL_H_
#define _FORMULATIONQ3DGENERALCURLCURL_H_

// include needed header files
#include "SmallFem.h"
#include "FormulationCoupled.h"
#include "FunctionSpace0Form.h"
#include "FunctionSpace1Form.h"
#include "GroupOfJacobian.h"
#include "Quadrature.h"

/**
   @class FormulationQ3DGeneralFF1Form
   @brief General Term for curl-curl bilinear forms 
   

   Calculates multConst * (r^exponent curl(r^alpha U), curl(r^alpha V)) = 
       multConst * (r^(2 alpha + 1) curl U, curl V) - 
       multConst * alpha * (r^(2 alpha) U_z ephi, curl V) -
       multConst * alpha * (r^(2 alpha) curl U, U_z ephi) + 
       multConst * alpha^2 (r^(2 alpha - 1) U, V)

       
   @author Erik Schnaubelt
*/



namespace sf{
  class FormulationQ3DGeneralCurlCurl: public FormulationCoupled<Complex>{
  private:
    friend class FormulationQ3DBlockStiffness;
    friend class FormulationQ3DBlockMass;
  
  private:
    std::list<FormulationBlock<Complex>*> fList;

  public:
    FormulationQ3DGeneralCurlCurl(const GroupOfElement& domain,
                          const FunctionSpace1Form& fs,
                          double multConst, 
                          double exponent, 
                          double alpha, 
                          const Basis& basis, 
                          const GroupOfJacobian& jac,
                          int G,
                          const fullMatrix<double>& gC,
                          const fullVector<double>& gW,
                          void (*f)(fullVector<double>&, fullMatrix<Complex>&)
                         );

    virtual ~FormulationQ3DGeneralCurlCurl(void);

    virtual const std::list<FormulationBlock<Complex>*>&
                                               getFormulationBlocks(void) const;
    virtual bool isBlock(void) const;
  };
}

#endif
 
