#include "SmallFem.h"
#include "FormulationContainer.h"

using namespace std;
using namespace sf;

template<typename scalar>
FormulationContainer<scalar>::FormulationContainer(void){
}

template<typename scalar>
FormulationContainer<scalar>::~FormulationContainer(void){
}

template<typename scalar>
void FormulationContainer<scalar>::
addFormulation(FormulationBlock<scalar>& formulation){
  fList.push_back(&formulation);
}

template<typename scalar>
void FormulationContainer<scalar>::
addFormulation(FormulationCoupled<scalar>& formulation){
  const list<FormulationBlock<scalar>*>
    tmp = formulation.getFormulationBlocks();

  typename list<FormulationBlock<scalar>*>::const_iterator  it = tmp.begin();
  typename list<FormulationBlock<scalar>*>::const_iterator end = tmp.end();

  for(; it != end; it++)
    fList.push_back(*it);
}

template<typename scalar>
const list<FormulationBlock<scalar>*>&
FormulationContainer<scalar>::getFormulationBlocks(void) const{
  return fList;
}

template<typename scalar>
void FormulationContainer<scalar>::update(void){
  typename list<FormulationBlock<scalar>*>::iterator  it = fList.begin();
  typename list<FormulationBlock<scalar>*>::iterator end = fList.end();

  for(; it != end; it++)
      (*it)->update();
}

////////////////////////////
// Explicit instantiation //
////////////////////////////
template class sf::FormulationContainer<Complex>;
template class sf::FormulationContainer<double>;
