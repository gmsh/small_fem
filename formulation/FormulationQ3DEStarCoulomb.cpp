#include "SmallFem.h"
#include "Exception.h"
#include "FormulationQ3DEStarCoulomb.h"
#include "FormulationQ3DEStarCoulombOne.h"
#include "FormulationQ3DEStarCoulombTwo.h"
#include "FormulationQ3DEStarCoulombThree.h"
#include "FormulationQ3DEStarCoulombFour.h"
#include "TermFieldField.h"
#include "TermGradGrad.h"

using namespace std;
using namespace sf;

FormulationQ3DEStarCoulomb::
FormulationQ3DEStarCoulomb(const GroupOfElement& dom,
                           const FunctionSpace0Form& hPhi,
                           const FunctionSpace1Form& hRZ,
                           const FunctionSpace0Form& hXi,
                           int N,
                           void (*r)(fullVector<double>&,
                                     fullMatrix<Complex>&),
                           Complex (*invR)(fullVector<double>&),
                           int order){

  pair<bool, size_t> uniform = dom.isUniform();
  size_t               eType = uniform.second;

  if(!uniform.first)
    throw Exception("FormulationQ3DEStarCoulomb needs a uniform mesh");

  const Basis& basisPhi = hPhi.getBasis(eType);
  const Basis& basisRZ  =  hRZ.getBasis(eType);
  const Basis& basisXi  =  hXi.getBasis(eType);

  int gOrder;
  if(order < 0)
    gOrder = max(basisPhi.getOrder(), basisRZ.getOrder()) * 2;
  else
    gOrder = order;
  Quadrature gauss(eType, gOrder, 1);

  const fullMatrix<double>& gC = gauss.getPoints();
  basisPhi.preEvaluateFunctions(gC);
  basisRZ.preEvaluateFunctions(gC);
  basisXi.preEvaluateFunctions(gC);
  basisXi.preEvaluateDerivatives(gC);

  GroupOfJacobian jac(dom, gC, "both");

  lXRZ  = new TermGradGrad<Complex>  (jac, basisXi, basisRZ,  gauss, r);
  lXPhi = new TermFieldField<Complex>(jac, basisXi, basisPhi, gauss, invR);
  // Warning: the matrices A=[dXi_i, RZ_j] and B=[RZ_i, dXi_j] are such that:
  //                        A = B^T.
  // Thefore, FormulationQ3DEStarCoulombThree::weak() must be a *transposed* of
  // FormulationQ3DEStarCoulombTwo::weak().
  // In the case of C=[Xi_i, Phi_j] and D=[Phi_i, Xi_j], we have however that:
  //                        C = D.
  // Thus, we do not need to transpose.
  // However, to respect the symmetry, we will do so...

  fList.push_back(new FormulationQ3DEStarCoulombOne  (dom,hXi, hRZ, *lXRZ   ));
  fList.push_back(new FormulationQ3DEStarCoulombTwo  (dom,hXi, hPhi,*lXPhi,N));
  fList.push_back(new FormulationQ3DEStarCoulombThree(dom,hRZ, hXi, *lXRZ   ));
  fList.push_back(new FormulationQ3DEStarCoulombFour (dom,hPhi,hXi, *lXPhi,N));
}

FormulationQ3DEStarCoulomb::~FormulationQ3DEStarCoulomb(void){
  // Iterate & Delete Formulations //
  list<FormulationBlock<Complex>*>::iterator end = fList.end();
  list<FormulationBlock<Complex>*>::iterator it  = fList.begin();

  for(; it !=end; it++)
    delete *it;

  delete lXPhi;
  delete lXRZ;
}

const list<FormulationBlock<Complex>*>&
FormulationQ3DEStarCoulomb::getFormulationBlocks(void) const{
  return fList;
}

bool FormulationQ3DEStarCoulomb::isBlock(void) const{
  return false;
}
