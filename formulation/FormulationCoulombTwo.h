#ifndef _FORMULATIONCOULOMBTWO_H_
#define _FORMULATIONCOULOMBTWO_H_

#include "SmallFem.h"
#include "FunctionSpace1Form.h"
#include "FunctionSpace0Form.h"
#include "TermGradGrad.h"
#include "FormulationBlock.h"

#include "FormulationCoulomb.h"

/**
   @class FormulationCoulombTwo
   @brief Helping class for FormulationCoulomb (gradients are test functions)

   Helping class for FormulationCoulomb (gradients are test functions)

   FormulationCoulombTwo is a friend of FormulationCoulomb
 */

namespace sf{
  class FormulationCoulombTwo: public FormulationBlock<double>{
  private:
    friend class FormulationCoulomb;

  private:
    // Function Space & Domain //
    const FunctionSpace1Form* ffield;
    const FunctionSpace0Form* ttest;
    const GroupOfElement*     ddomain;

    // Local Terms //
    const TermGradGrad<double>* term;

  private:
    FormulationCoulombTwo(void);
    FormulationCoulombTwo(const GroupOfElement& domain,
                          const FunctionSpace1Form& field,
                          const FunctionSpace0Form& test,
                          const TermGradGrad<double>& term);

  public:
    virtual ~FormulationCoulombTwo(void);

    virtual double weak(size_t dofI, size_t dofJ, size_t elementId) const;
    virtual double rhs(size_t equationI, size_t elementId)          const;

    virtual const FunctionSpace&   field(void) const;
    virtual const FunctionSpace&    test(void) const;
    virtual const GroupOfElement& domain(void) const;

    virtual bool isBlock(void) const;
  };
}

/**
   @fn FormulationCoulombTwo::~FormulationCoulombTwo
   Deletes this FormulationCoulombTwo
*/

#endif
