#include "SmallFem.h"
#include "FormulationQ3DEStarCoulombOne.h"

#include "Quadrature.h"
#include "Mapper.h"

#include "TermFieldField.h"
#include "TermGradGrad.h"

using namespace std;
using namespace sf;

FormulationQ3DEStarCoulombOne::FormulationQ3DEStarCoulombOne(void){
}

FormulationQ3DEStarCoulombOne::
FormulationQ3DEStarCoulombOne(const GroupOfElement& domain,
                              const FunctionSpace0Form& field,
                              const FunctionSpace1Form& test,
                              const TermGradGrad<Complex>& lXRZ){

  // Save Data //
  this->ddomain = &domain;
  this->ffield  = &field;
  this->ttest   = &test;
  this->lXRZ    = &lXRZ;
}

FormulationQ3DEStarCoulombOne::~FormulationQ3DEStarCoulombOne(void){
}

Complex FormulationQ3DEStarCoulombOne::
weak(size_t dofI, size_t dofJ, size_t elementId) const{
  return lXRZ->getTerm(dofI, dofJ, elementId);
}

Complex FormulationQ3DEStarCoulombOne::
rhs(size_t equationI, size_t elementId) const{
  return Complex(0, 0);
}

const FunctionSpace& FormulationQ3DEStarCoulombOne::field(void) const{
  return *ffield;
}

const FunctionSpace& FormulationQ3DEStarCoulombOne::test(void) const{
  return *ttest;
}

const GroupOfElement& FormulationQ3DEStarCoulombOne::domain(void) const{
  return *ddomain;
}

bool FormulationQ3DEStarCoulombOne::isBlock(void) const{
  return true;
}
