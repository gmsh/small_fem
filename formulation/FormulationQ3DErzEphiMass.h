#ifndef _FORMULATIONQ3DERZEPHIMASS_H_
#define _FORMULATIONQ3DERZEPHIMASS_H_

// include needed header files
#include "SmallFem.h"
#include "FormulationCoupled.h"
#include "FunctionSpace0Form.h"
#include "FunctionSpace1Form.h"
#include "GroupOfJacobian.h"
#include "Quadrature.h"

/**
   @class FormulationQ3DErzEphiMass
   @brief Mass part for Oh ansatz

   @author Erik Schnaubelt
*/

namespace sf{
  class FormulationQ3DErzEphiMass: public FormulationCoupled<Complex>{
  private:
    std::list<FormulationCoupled<Complex>*> gForm;
    std::list<FormulationBlock<Complex>*>   fList;
    Basis* basisGrad;
    Basis* basisCurl;
    fullMatrix<double>* gC;
    fullVector<double>* gW;
    GroupOfJacobian*    jac;

  public:
    FormulationQ3DErzEphiMass(const GroupOfElement& domain,
                          const FunctionSpace0Form& hGrad,
                          const FunctionSpace1Form& hCurl,
                          int n,
                          void (*f)(fullVector<double>&, fullMatrix<Complex>&),
                          int order
                     );

    virtual ~FormulationQ3DErzEphiMass(void);

    virtual const std::list<FormulationBlock<Complex>*>&
                                               getFormulationBlocks(void) const;
    virtual bool isBlock(void) const;
  };
}
#endif
