#ifndef _FORMULATIONQ3DGENERALFFDUNN_H_
#define _FORMULATIONQ3DGENERALFFDUNN_H_

// include needed header files
#include "SmallFem.h"
#include "FormulationCoupled.h"
#include "FunctionSpace0Form.h"
#include "FunctionSpace1Form.h"
#include "GroupOfJacobian.h"
#include "Quadrature.h"

/**
   @class FormulationQ3DGeneralFFDunn
   @brief General Term for function-function bilinear forms 
   

   Calculates multConst * (r^exponent U, U') 
           = 1/n^2 multConst * (r^exponent r*U - 1/n*[v;0], r*U' - 1/n*[v';0]) 
           = 1/n^2 * multConst * {
               (r^(exponent + 2) U, U') + (r^(exponent) v, v) 
              -(r^(exponent + 1) U,[v';0]) - (r^(exponent + 1) [v;0], U')
       
   @author Erik Schnaubelt
*/

namespace sf{
  class FormulationQ3DGeneralFFDunn: public FormulationCoupled<Complex>{
  private:
    friend class FormulationQ3DnErzMass;
  
  
  private:
    std::list<FormulationBlock<Complex>*> fList;

  public:
    FormulationQ3DGeneralFFDunn(const GroupOfElement& domain,
                          const FunctionSpace0Form& fsGrad,
                          const FunctionSpace1Form& fsCurl,
                          double multConst, 
                          double exponent, 
                          int n, 
                          const Basis& basisGrad, 
                          const Basis& basisCurl,
                          const GroupOfJacobian& jac,
                          int G,
                          const fullMatrix<double>& gC,
                          const fullVector<double>& gW,
                          void (*f)(fullVector<double>&, fullMatrix<Complex>&)
                         );

    virtual ~FormulationQ3DGeneralFFDunn(void);

    virtual const std::list<FormulationBlock<Complex>*>&
                                               getFormulationBlocks(void) const;
    virtual bool isBlock(void) const;
  };
}

#endif
 
