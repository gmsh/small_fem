#ifndef _FORMULATIONCOULOMB_H_
#define _FORMULATIONCOULOMB_H_

#include "SmallFem.h"
#include "FormulationCoupled.h"
#include "FunctionSpace1Form.h"
#include "FunctionSpace0Form.h"
#include "TermGradGrad.h"

/**
   @class FormulationCoulomb
   @brief Coulomb gauge for magnetostatics with vector potential

   Coulomb gauge for magnetostatics with vector potential
 */

namespace sf{
  class FormulationCoulomb: public FormulationCoupled<double>{
  private:
    // Local Terms //
    TermGradGrad<double>* xiByA;
    TermGradGrad<double>* aByXi;

    // Formulations //
    std::list<FormulationBlock<double>*> fList;

  public:
    FormulationCoulomb(const GroupOfElement& domain,
                       const FunctionSpace1Form& a,
                       const FunctionSpace0Form& xi);

    virtual ~FormulationCoulomb(void);

    virtual const std::list<FormulationBlock<double>*>&
                                               getFormulationBlocks(void) const;
    virtual bool isBlock(void) const;
  };
}

/**
   @fn FormulationCoulomb::FormulationCoulomb
   @param domain A GroupOfElement for the domain
   @param a A FunctionSpace1Form for the magnetic vector potential
   @param xi A FunctionSpace0Form for the Coulomb gauge

   Instantiates a new FormulationCoulomb
   **

   @fn FormulationCoulomb::~FormulationCoulomb
   Deletes this FormulationCoulomb
*/

#endif
