#ifndef _FORMULATIONELASTOMASS_H_
#define _FORMULATIONELASTOMASS_H_

#include "FunctionSpace0Form.h"
#include "FormulationCoupled.h"
#include "TermFieldField.h"

/**
   @class FormulationElastoMass
   @brief Formulation for Elastodynamic (mass term)

   Formulation for Elastodynamic (mass term)
 */

namespace sf{
  template<typename scalar>
  class FormulationElastoMass: public FormulationCoupled<scalar>{
  private:
    // Density //
    double rho;

    // Local Term (generic) //
    TermFieldField<scalar>* termUU;

    // Formulations //
    std::list<FormulationBlock<scalar>*> fList;

  public:
    FormulationElastoMass(const GroupOfElement& domain,
                          const FunctionSpace0Form& hGradX,
                          const FunctionSpace0Form& hGradY,
                          const FunctionSpace0Form& hGradZ,
                          double rho);

    virtual ~FormulationElastoMass(void);

    virtual const std::list<FormulationBlock<scalar>*>&
                                               getFormulationBlocks(void) const;
    virtual bool isBlock(void) const;
  };
}

/**
   @fn FormulationElastoMass::FormulationElastoMass
   @param domain A GroupOfElement
   @param hGradX A FunctionSpace0Form for the displacement along x
   @param hGradY A FunctionSpace0Form for the displacement along y
   @param hGradZ A FunctionSpace0Form for the displacement along z
   @param rho The material density

   Instantiates a new FormulationElastoMass with the given parametres
   **

   @fn FormulationElastoMass::~FormulationElastoMass
   Deletes this FormulationElastoMass
*/

#endif
