#ifndef _FORMULATIONQ3DESTARMASS_H_
#define _FORMULATIONQ3DESTARMASS_H_

#include "SmallFem.h"
#include "FormulationCoupled.h"
#include "FunctionSpace0Form.h"
#include "FunctionSpace1Form.h"

#include "TermFieldField.h"
#include "TermGradGrad.h"

/**
   @class FormulationQ3DEStarMass
   @brief Mass term for the ephiStar ansatz

   @author Erik Schnaubelt
*/

namespace sf{
  class FormulationQ3DEStarMass: public FormulationCoupled<Complex>{
  private:
    std::list<FormulationBlock<Complex>*> fList;

    TermFieldField<Complex>* localFF;
    TermGradGrad<Complex>*   localGG;

  public:
    FormulationQ3DEStarMass(const GroupOfElement& domain,
                            const FunctionSpace0Form& hGrad,
                            const FunctionSpace1Form& hCurl,
                            void (*r)(fullVector<double>&,
                                      fullMatrix<Complex>&),
                            Complex (*invR)(fullVector<double>&),
                            int order);

    virtual ~FormulationQ3DEStarMass(void);

    virtual const std::list<FormulationBlock<Complex>*>&
                                               getFormulationBlocks(void) const;
    virtual bool isBlock(void) const;
  };
}

/**
   @fn FormulationQ3DEStarMass::FormulationQ3DEStarMass
   @param domain A GroupOfElement for the domain
   @param hGrad FunctionSpace for the out of plane component
   @param hCurl FunctionSpace for the in plane component
   @param r function representing the radial coordinate
   @param invR function representing the inverse of the radial coordinate
   @param order order of the Gauss quadrature

   Instantiates a new FormulationQ3DEStarMass
   **

   @fn FormulationQ3DEStarMass::~FormulationQ3DEStarMass
   Deletes this FormulationQ3DEStarMass
*/

#endif
