#include "SmallFem.h"
#include "FormulationQ3DEStarCoulombTwo.h"

#include "Quadrature.h"
#include "Mapper.h"

#include "TermFieldField.h"
#include "TermGradGrad.h"

using namespace std;
using namespace sf;

FormulationQ3DEStarCoulombTwo::FormulationQ3DEStarCoulombTwo(void){
}

FormulationQ3DEStarCoulombTwo::
FormulationQ3DEStarCoulombTwo(const GroupOfElement& domain,
                              const FunctionSpace0Form& field,
                              const FunctionSpace0Form& test,
                              const TermFieldField<Complex>& lXPhi,
                              int N){

  // Save Data //
  this->ddomain = &domain;
  this->ffield  = &field;
  this->ttest   = &test;
  this->lXPhi   = &lXPhi;
  this->minusN  = Complex(-N, 0);
}

FormulationQ3DEStarCoulombTwo::~FormulationQ3DEStarCoulombTwo(void){
}

Complex FormulationQ3DEStarCoulombTwo::
weak(size_t dofI, size_t dofJ, size_t elementId) const{
  return minusN * lXPhi->getTerm(dofI, dofJ, elementId);
}

Complex FormulationQ3DEStarCoulombTwo::
rhs(size_t equationI, size_t elementId) const{
  return Complex(0, 0);
}

const FunctionSpace& FormulationQ3DEStarCoulombTwo::field(void) const{
  return *ffield;
}

const FunctionSpace& FormulationQ3DEStarCoulombTwo::test(void) const{
  return *ttest;
}

const GroupOfElement& FormulationQ3DEStarCoulombTwo::domain(void) const{
  return *ddomain;
}

bool FormulationQ3DEStarCoulombTwo::isBlock(void) const{
  return true;
}
