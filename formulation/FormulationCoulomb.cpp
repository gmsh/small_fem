#include "SmallFem.h"
#include "Exception.h"
#include "Quadrature.h"
#include "GroupOfJacobian.h"

#include "FormulationCoulombTwo.h"
#include "FormulationCoulombOne.h"
#include "FormulationCoulomb.h"

using namespace std;
using namespace sf;

FormulationCoulomb::FormulationCoulomb(const GroupOfElement& domain,
                                       const FunctionSpace1Form& a,
                                       const FunctionSpace0Form& xi){
  // Check GroupOfElement Stats: Uniform Mesh //
  pair<bool, size_t> uniform = domain.isUniform();
  size_t               eType = uniform.second;

  if(!uniform.first)
    throw Exception("FormulationCoulomb needs a uniform mesh");

  // Get Basis (same for field and Coulomb function spaces) //
  const Basis& basisA  =  a.getBasis(eType);
  const Basis& basisXi = xi.getBasis(eType);
  const size_t order   =  a.getOrder();

  // Gaussian Quadrature //
  Quadrature gauss(eType, order, 2);
  const fullMatrix<double>& gC = gauss.getPoints();

  // Local Terms //
  basisA.preEvaluateFunctions(gC);
  basisXi.preEvaluateDerivatives(gC);
  GroupOfJacobian jac(domain, gC, "invert");

  xiByA = new TermGradGrad<double>(jac, basisXi,  basisA, gauss);
  aByXi = new TermGradGrad<double>(jac,  basisA, basisXi, gauss);

  // Formulations //
  fList.push_back(new FormulationCoulombOne(domain, xi,  a, *xiByA));
  fList.push_back(new FormulationCoulombTwo(domain,  a, xi, *aByXi));
}

FormulationCoulomb::~FormulationCoulomb(void){
  // Iterate & Delete Formulations //
  list<FormulationBlock<double>*>::iterator end = fList.end();
  list<FormulationBlock<double>*>::iterator it  = fList.begin();

  for(; it !=end; it++)
    delete *it;

  // Delete terms //
  delete xiByA;
  delete aByXi;
}

const list<FormulationBlock<double>*>&
FormulationCoulomb::getFormulationBlocks(void) const{
  return fList;
}

bool FormulationCoulomb::isBlock(void) const{
  return false;
}
