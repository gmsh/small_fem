#include "SmallFem.h"
#include "Exception.h"

#include "FormulationQ3DGeneralFGradDunn.h"
#include "FormulationQ3DBlockFGrad.h"
#include "FormulationQ3DBlockFRadialF.h"
#include "FormulationQ3DBlockFF0Form.h"
#include "FormulationQ3DBlockRadialFGrad.h"


using namespace std;
using namespace sf;

FormulationQ3DGeneralFGradDunn::FormulationQ3DGeneralFGradDunn(const GroupOfElement& domain,
                          const FunctionSpace0Form& fsGrad,      
                          const FunctionSpace1Form& fsCurl,
                          double multConst,
                          double exponent,
                          int n, 
                          const Basis& basisGrad, 
                          const Basis& basisCurl, 
                          const GroupOfJacobian& jac,
                          int G,
                          const fullMatrix<double>& gC,
                          const fullVector<double>& gW,
                          void (*f)(fullVector<double>&, fullMatrix<Complex>&)
                                            ){

      fList.push_back(
          new FormulationQ3DBlockFGrad (domain, fsGrad, fsCurl, multConst/(static_cast<double>(n)), 
                                        exponent + 2.0, basisGrad, basisCurl, jac, G, gC, gW, f));
      
      fList.push_back(
          new FormulationQ3DBlockFRadialF (domain, fsGrad, fsCurl, multConst/(static_cast<double>(n)), 
                                        exponent + 1.0, basisGrad, basisCurl, jac, G, gC, gW, f));
          
      fList.push_back(
          new FormulationQ3DBlockFF0Form (domain, fsGrad, -1.0 * multConst/(static_cast<double>(n)), 
                                          exponent, basisGrad, jac, G, gC, gW, f));
      
      fList.push_back(
          new FormulationQ3DBlockRadialFGrad (
              domain, fsGrad, -1.0 * multConst/(static_cast<double>(n)), exponent + 1.0, basisGrad, jac, G, gC, gW, f));
      
}

FormulationQ3DGeneralFGradDunn::~FormulationQ3DGeneralFGradDunn(void){
  list<FormulationBlock<Complex>*>::iterator end = fList.end();
  list<FormulationBlock<Complex>*>::iterator it  = fList.begin();

  for(; it !=end; it++)
    delete *it;
  
}

const list<FormulationBlock<Complex>*>&
FormulationQ3DGeneralFGradDunn::getFormulationBlocks(void) const{
  return fList;
}

bool FormulationQ3DGeneralFGradDunn::isBlock(void) const{
  return false;
}
 
