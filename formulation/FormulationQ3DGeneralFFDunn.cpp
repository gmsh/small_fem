#include "SmallFem.h"
#include "Exception.h"

#include "FormulationQ3DGeneralFFDunn.h"
#include "FormulationQ3DBlockFF1Form.h"
#include "FormulationQ3DBlockRadialFF.h"
#include "FormulationQ3DBlockFRadialF.h"
#include "FormulationQ3DBlockFF0Form.h"


using namespace std;
using namespace sf;

FormulationQ3DGeneralFFDunn::FormulationQ3DGeneralFFDunn(const GroupOfElement& domain,
                          const FunctionSpace0Form& fsGrad,
                          const FunctionSpace1Form& fsCurl,
                          double multConst, 
                          double exponent, 
                          int n, 
                          const Basis& basisGrad, 
                          const Basis& basisCurl, 
                          const GroupOfJacobian& jac,
                          int G,
                          const fullMatrix<double>& gC,
                          const fullVector<double>& gW,
                          void (*f)(fullVector<double>&, fullMatrix<Complex>&)
                                            ){

      fList.push_back(
          new FormulationQ3DBlockFF1Form (
              domain, fsCurl, multConst/(static_cast<double>(n*n)), exponent + 2.0, basisCurl, jac, G, gC, gW, f));
      
       fList.push_back(
              new FormulationQ3DBlockRadialFF (
                  domain, fsGrad, fsCurl, -1.0 * multConst/(static_cast<double>(n*n)), exponent + 1.0, basisCurl, basisGrad, jac, G, gC, gW, f)    
                         );
       
        fList.push_back(
              new FormulationQ3DBlockFRadialF (
                  domain, fsGrad, fsCurl, -1.0 * multConst/(static_cast<double>(n*n)), exponent + 1.0, basisGrad, basisCurl, jac, G, gC, gW, f)    
                         );
        
        fList.push_back(
          new FormulationQ3DBlockFF0Form (
              domain, fsGrad, multConst/(static_cast<double>(n*n)), exponent, basisGrad, jac, G, gC, gW, f));
}

FormulationQ3DGeneralFFDunn::~FormulationQ3DGeneralFFDunn(void){
  list<FormulationBlock<Complex>*>::iterator end = fList.end();
  list<FormulationBlock<Complex>*>::iterator it  = fList.begin();

  for(; it !=end; it++)
    delete *it;
  
}

const list<FormulationBlock<Complex>*>&
FormulationQ3DGeneralFFDunn::getFormulationBlocks(void) const{
  return fList;
}

bool FormulationQ3DGeneralFFDunn::isBlock(void) const{
  return false;
}
 
