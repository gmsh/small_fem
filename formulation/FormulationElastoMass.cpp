#include "SmallFem.h"
#include "FormulationElastoMass.h"
#include "FormulationElastoMassUU.h"

#include "GroupOfJacobian.h"
#include "TermFieldField.h"
#include "Quadrature.h"

using namespace std;
using namespace sf;

template<typename scalar>
FormulationElastoMass<scalar>::
FormulationElastoMass(const GroupOfElement& dom,
                      const FunctionSpace0Form& hGradX,
                      const FunctionSpace0Form& hGradY,
                      const FunctionSpace0Form& hGradZ,
                      double rho){
  // Density //
  this->rho = rho;

  // Check domain stats: uniform mesh //
  pair<bool, size_t> uniform = dom.isUniform();
  size_t               eType = uniform.second;

  if(!uniform.first)
    throw Exception("FormulationElastoMass needs a uniform mesh");

  // Check orders //
  if(hGradX.getOrder() != hGradY.getOrder() ||
     hGradX.getOrder() != hGradZ.getOrder())
    throw Exception("FormulationElastoMass needs a uniform orders");

  // Get Bases //
  const Basis& basisX = hGradX.getBasis(eType);
  const Basis& basisY = hGradY.getBasis(eType);
  const Basis& basisZ = hGradZ.getBasis(eType);
  const size_t order  = basisX.getOrder();

  // Gaussian Quadrature //
  Quadrature gauss(eType, order, 2);
  const fullMatrix<double>& gC = gauss.getPoints();

  // Basis functions //
  basisX.preEvaluateFunctions(gC);
  basisY.preEvaluateFunctions(gC);
  basisZ.preEvaluateFunctions(gC);

  // Jacobian //
  GroupOfJacobian jac(dom, gC, "jacobian");

  // Local Term (generic) //
  termUU = new TermFieldField<scalar>(jac, basisX, gauss);

  // Formulations //
  fList.push_back(new FormulationElastoMassUU<scalar>(dom,hGradX,*termUU,rho));
  fList.push_back(new FormulationElastoMassUU<scalar>(dom,hGradY,*termUU,rho));
  fList.push_back(new FormulationElastoMassUU<scalar>(dom,hGradZ,*termUU,rho));
}


template<typename scalar>
FormulationElastoMass<scalar>::
~FormulationElastoMass(void){
  // Iterate & Delete Formulations //
  typename list<FormulationBlock<scalar>*>::iterator end = fList.end();
  typename list<FormulationBlock<scalar>*>::iterator it  = fList.begin();

  for(; it !=end; it++)
    delete *it;

  // Delete term //
  delete termUU;
}

template<typename scalar>
const list<FormulationBlock<scalar>*>&
FormulationElastoMass<scalar>::getFormulationBlocks(void) const{
  return fList;
}

template<typename scalar>
bool FormulationElastoMass<scalar>::isBlock(void) const{
  return false;
}

////////////////////////////
// Explicit instantiation //
////////////////////////////
template class sf::FormulationElastoMass<Complex>;
template class sf::FormulationElastoMass<double>;
