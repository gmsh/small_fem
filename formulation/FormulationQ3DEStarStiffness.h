#ifndef _FORMULATIONQ3DESTARSTIFFNESS_H_
#define _FORMULATIONQ3DESTARSTIFFNESS_H_

#include "SmallFem.h"
#include "FormulationCoupled.h"
#include "FunctionSpace0Form.h"
#include "FunctionSpace1Form.h"

#include "TermFieldField.h"
#include "TermGradGrad.h"
#include "TermCurlCurl.h"
/**
   @class FormulationQ3DEStarStiffness
   @brief Stiffness term for the ephiStar ansatz

   @author Erik Schnaubelt
*/

namespace sf{
  class FormulationQ3DEStarStiffness: public FormulationCoupled<Complex>{
  private:
    std::list<FormulationBlock<Complex>*> fList;

    TermGradGrad<Complex>* localGIn;
    TermGradGrad<Complex>* localInG;

    TermGradGrad<Complex>* localGG;
    TermCurlCurl<Complex>* localCC;
    TermGradGrad<Complex>* localInIn;

  public:
    FormulationQ3DEStarStiffness(const GroupOfElement& domain,
                                 const FunctionSpace0Form& hGrad,
                                 const FunctionSpace1Form& hCurl,
                                 int   n,
                                 void (*r)(fullVector<double>&,
                                           fullMatrix<Complex>&),
                                 void (*invR)(fullVector<double>&,
                                              fullMatrix<Complex>&),
                                 int order);

    virtual ~FormulationQ3DEStarStiffness(void);

    virtual const std::list<FormulationBlock<Complex>*>&
                                               getFormulationBlocks(void) const;
    virtual bool isBlock(void) const;
  };
}

/**
   @fn FormulationQ3DEStarStiffness::FormulationQ3DEStarStiffness
   @param domain A GroupOfElement for the domain
   @param hGrad FunctionSpace for the out of plane component
   @param hCurl FunctionSpace for the in plane component
   @param n pole mode
   @param r function representing the radial coordinate
   @param invR function representing the inverse of the radial coordinate
   @param order order of the Gauss quadrature

   Instantiates a new FormulationQ3DEStarStiffness
   **

   @fn FormulationQ3DEStarStiffness::~FormulationQ3DEStarStiffness
   Deletes this FormulationQ3DEStarStiffness
*/

#endif
