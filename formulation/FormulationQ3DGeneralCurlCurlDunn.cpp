#include "SmallFem.h"
#include "Exception.h"

#include "FormulationQ3DGeneralCurlCurlDunn.h"

#include "FormulationQ3DBlockCurlCurl.h"
#include "FormulationQ3DBlockRadialFCurl.h"
#include "FormulationQ3DBlockCurlRadialF.h"
#include "FormulationQ3DBlockZFZF.h"
#include "FormulationQ3DBlockZGradZGrad.h"
#include "FormulationQ3DBlockCurlZGrad.h"
#include "FormulationQ3DBlockZFZGrad.h"
#include "FormulationQ3DBlockZGradZF.h"
#include "FormulationQ3DBlockZGradCurl.h"



using namespace std;
using namespace sf;

FormulationQ3DGeneralCurlCurlDunn::FormulationQ3DGeneralCurlCurlDunn(const GroupOfElement& domain,
                          const FunctionSpace0Form& fsGrad,
                          const FunctionSpace1Form& fsCurl,
                          double multConst, 
                          double exponent, 
                          int n, 
                          const Basis& basisGrad, 
                          const Basis& basisCurl, 
                          const GroupOfJacobian& jac,
                          int G,
                          const fullMatrix<double>& gC,
                          const fullVector<double>& gW,
                          void (*f)(fullVector<double>&, fullMatrix<Complex>&)
                                            ){

      // General Curl Curl part
      fList.push_back(
          new FormulationQ3DBlockCurlCurl (
              domain, fsCurl, multConst/(static_cast<double>(n*n)), exponent + 2.0, 
                                           basisCurl, jac, G, gC, gW, f));
      
      fList.push_back(
          new FormulationQ3DBlockRadialFCurl (
              domain, fsCurl, multConst/(static_cast<double>(n*n)), exponent + 1.0, 
                                              basisCurl, jac, G, gC, gW, f));
      
      fList.push_back(
          new FormulationQ3DBlockCurlRadialF (
              domain, fsCurl, multConst/(static_cast<double>(n*n)), exponent + 1.0, 
                                              basisCurl, jac, G, gC, gW, f));
      
      fList.push_back(
          new FormulationQ3DBlockZFZF (domain, fsCurl, multConst/(static_cast<double>(n*n)),
                                       exponent, basisCurl, jac, G, gC, gW, f));
      
      // rest 
       fList.push_back(
          new FormulationQ3DBlockZGradZGrad (domain, fsGrad, multConst/(static_cast<double>(n*n)),
                                             exponent, basisGrad, jac, G, gC, gW, f));
       
       fList.push_back(
          new FormulationQ3DBlockCurlZGrad (
            domain, fsGrad, fsCurl,  1.0 * multConst/(static_cast<double>(n*n)), exponent + 1.0, 
                                            basisGrad, basisCurl, jac, G, gC, gW, f));
       
       fList.push_back(
          new FormulationQ3DBlockZFZGrad (
            domain, fsGrad, fsCurl,  1.0 * multConst/(static_cast<double>(n*n)), exponent,  basisGrad, 
                                          basisCurl, jac, G, gC, gW, f));
       
        fList.push_back(
          new FormulationQ3DBlockZGradCurl (
            domain, fsGrad, fsCurl, 1.0 *  multConst/(static_cast<double>(n*n)), exponent + 1.0,                  
                                            basisCurl, basisGrad, jac, G, gC, gW, f));
       
       fList.push_back(
          new FormulationQ3DBlockZGradZF (
            domain, fsGrad, fsCurl,  1.0 * multConst/(static_cast<double>(n*n)), exponent,  
                                          basisCurl, basisGrad, jac, G, gC, gW, f));
   
}

FormulationQ3DGeneralCurlCurlDunn::~FormulationQ3DGeneralCurlCurlDunn(void){
  list<FormulationBlock<Complex>*>::iterator end = fList.end();
  list<FormulationBlock<Complex>*>::iterator it  = fList.begin();

  for(; it !=end; it++)
    delete *it;
  
}

const list<FormulationBlock<Complex>*>&
FormulationQ3DGeneralCurlCurlDunn::getFormulationBlocks(void) const{
  return fList;
}

bool FormulationQ3DGeneralCurlCurlDunn::isBlock(void) const{
  return false;
}
 
