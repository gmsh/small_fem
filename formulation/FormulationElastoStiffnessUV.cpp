#include "FormulationElastoStiffnessUV.h"

using namespace std;
using namespace sf;

template<typename scalar>
FormulationElastoStiffnessUV<scalar>::
FormulationElastoStiffnessUV(void){
}

template<typename scalar>
FormulationElastoStiffnessUV<scalar>::
FormulationElastoStiffnessUV(const GroupOfElement& domain,
                             const FunctionSpace0Form& field,
                             const FunctionSpace0Form& test,
                             const TermGradGrad<scalar>& term){
  // Save Data //
  this->ddomain = &domain;
  this->ffield  = &field;
  this->ttest   = &test;
  this->term    = &term;
}

template<typename scalar>
FormulationElastoStiffnessUV<scalar>::
~FormulationElastoStiffnessUV(void){
}

template<typename scalar>
scalar FormulationElastoStiffnessUV<scalar>::
weak(size_t dofI, size_t dofJ, size_t elementId) const{
  return term->getTerm(dofI, dofJ, elementId);
}

template<typename scalar>
scalar FormulationElastoStiffnessUV<scalar>::
rhs(size_t equationI, size_t elementId) const{
  return 0.;
}

template<typename scalar>
const FunctionSpace& FormulationElastoStiffnessUV<scalar>::field(void) const{
  return *ffield;
}

template<typename scalar>
const FunctionSpace& FormulationElastoStiffnessUV<scalar>::test(void) const{
  return *ttest;
}

template<typename scalar>
const GroupOfElement& FormulationElastoStiffnessUV<scalar>::domain(void) const{
  return *ddomain;
}

template<typename scalar>
bool FormulationElastoStiffnessUV<scalar>::isBlock(void) const{
  return true;
}

////////////////////////////
// Explicit instantiation //
////////////////////////////
template class sf::FormulationElastoStiffnessUV<Complex>;
template class sf::FormulationElastoStiffnessUV<double>;
