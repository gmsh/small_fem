#ifndef _FORMULATIONQ3DNERZMASS_H_
#define _FORMULATIONQ3DNERZMASS_H_

// include needed header files
#include "SmallFem.h"
#include "FormulationCoupled.h"
#include "FunctionSpace0Form.h"
#include "FunctionSpace1Form.h"
#include "GroupOfJacobian.h"
#include "Quadrature.h"

/**
   @class FormulationQ3DnErzMass
   @brief Mass part for the Dunn ansatz

   @author Erik Schnaubelt
*/

namespace sf{
  class FormulationQ3DnErzMass: public FormulationCoupled<Complex>{
  private:
    std::list<FormulationCoupled<Complex>*> gForm;
    std::list<FormulationBlock<Complex>*>   fList;
    Basis* basisGrad;
    Basis* basisCurl;
    fullMatrix<double>* gC;
    fullVector<double>* gW;
    GroupOfJacobian*    jac;

  public:
    FormulationQ3DnErzMass(const GroupOfElement& domain,
                          const FunctionSpace0Form& hGrad,
                          const FunctionSpace1Form& hCurl,
                          int n,
                           void (*f)(fullVector<double>&, fullMatrix<Complex>&),
                          int order
                     );

    virtual ~FormulationQ3DnErzMass(void);

    virtual const std::list<FormulationBlock<Complex>*>&
                                               getFormulationBlocks(void) const;
    virtual bool isBlock(void) const;
  };
}
#endif
