#ifndef _FORMULATIONQ3DPARAMRADIALFCURLCURL_H_
#define _FORMULATIONQ3DPARAMRADIALFCURLCURL_H_

#include "SmallFem.h"
#include "FunctionSpace1Form.h"
#include "GroupOfJacobian.h"

#include "FormulationBlock.h"
#include "FormulationQ3DGeneralCurlCurl.h"

namespace sf{
  class FormulationQ3DBlockRadialFCurl: public FormulationBlock<Complex>{
  private:
    friend class FormulationQ3DGeneralCurlCurl;
    friend class FormulationQ3DGeneralCurlCurlDunn;


  private:
    // multiplicative constant and exponent 
    double multConst; 
    double exponent; 
      
    // Function Space & Domain //
    const FunctionSpace1Form* fs;
    const GroupOfElement*     ddomain;

    // Gaussian Quadrature Data //
    int G;
    const fullMatrix<double>* gC;
    const fullVector<double>* gW;

    // Jacobians and basis//
    const GroupOfJacobian* jac;
    const Basis*         basis;
    
    void (*f)(fullVector<double>&, fullMatrix<Complex>&); 

  private:
    FormulationQ3DBlockRadialFCurl(void);
    FormulationQ3DBlockRadialFCurl(const GroupOfElement& domain,
                          const FunctionSpace1Form& fs,
                          double multConst, 
                          double exponent, 
                          const Basis& basis, 
                          const GroupOfJacobian& jac,
                          int G,
                          const fullMatrix<double>& gC,
                          const fullVector<double>& gW,
                          void (*f)(fullVector<double>&, fullMatrix<Complex>&)
                             );

  public:
    virtual ~FormulationQ3DBlockRadialFCurl(void);

    virtual Complex weak(size_t dofI, size_t dofJ, size_t elementId) const;
    virtual Complex rhs(size_t equationI, size_t elementId)          const;

    virtual const FunctionSpace&   field(void) const;
    virtual const FunctionSpace&    test(void) const;
    virtual const GroupOfElement& domain(void) const;

    virtual bool isBlock(void) const;
  };
}

#endif
 
