#include "SmallFem.h"
#include "FormulationSteadySlowScalar.h"

#include "Quadrature.h"
#include "Mapper.h"
#include <cmath>

using namespace std;
using namespace sf;

template<typename scalar>
FormulationSteadySlowScalar<scalar>::
FormulationSteadySlowScalar(const GroupOfElement& domain,
                            const FunctionSpace& fs,
                            double k){
  // Check domain stats: uniform mesh //
  pair<bool, size_t> uniform = domain.isUniform();
  size_t               eType = uniform.second;

  if(!uniform.first)
    throw Exception("FormulationSteadySlowScalar needs a uniform mesh");

  // Wave Squared //
  kSquare = k * k;

  // Domain //
  ddomain = &domain;

  // Save FunctionSpace & Get Basis //
  if(!fs.isScalar())
    throw Exception("FormulationSteadySlowScalar needs a scalar function space");

  fspace = &fs;
  basis  = &fs.getBasis(eType);

  // Gaussian Quadrature //
  const size_t order = basis->getOrder();

  Quadrature gaussGradGrad(eType, order - 1, 2);
  Quadrature gaussFF(eType, order, 2);

  gC1 = new fullMatrix<double>(gaussGradGrad.getPoints());
  gW1 = new fullVector<double>(gaussGradGrad.getWeights());

  gC2 = new fullMatrix<double>(gaussFF.getPoints());
  gW2 = new fullVector<double>(gaussFF.getWeights());

  G1 = gW1->size();
  G2 = gW2->size();

  // PreEvaluate
  basis->preEvaluateDerivatives(*gC1);
  basis->preEvaluateFunctions(*gC2);

  jac1 = new GroupOfJacobian(domain, *gC1, "invert");
  jac2 = new GroupOfJacobian(domain, *gC2, "jacobian");
}

template<typename scalar>
FormulationSteadySlowScalar<scalar>::~FormulationSteadySlowScalar(void){
  delete gC1;
  delete gW1;
  delete gC2;
  delete gW2;
  delete jac1;
  delete jac2;
}

template<typename scalar>
scalar FormulationSteadySlowScalar<scalar>::
weak(size_t dofI, size_t dofJ, size_t elementId) const{
  // Init Some Stuff //
  fullMatrix<double> jac;

  fullVector<double> phiGI(3);
  fullVector<double> phiGJ(3);

  double phiI;
  double phiJ;

  scalar integral1 = 0;
  scalar integral2 = 0;
  double det;

  // Get Element //
  const MElement& element = ddomain->get(elementId);

  // Get Basis Functions //
  const fullMatrix<double>& eGradFun =
    basis->getPreEvaluatedDerivatives(element);

  const fullMatrix<double>& eFun =
    basis->getPreEvaluatedFunctions(element);

  // Get Jacobians //
  const vector<pair<fullMatrix<double>, double> >& invJac =
    jac1->getInvertJacobianMatrix(elementId);

  const vector<pair<fullMatrix<double>, double> >& MJac =
    jac2->getJacobianMatrix(elementId);

  // Loop over Integration Point (Term 1) //
  for(int g = 0; g < G1; g++){
    det = invJac[g].second;
    jac = invJac[g].first;

    Mapper::hCurl(eGradFun, dofI, g, jac, phiGI);
    Mapper::hCurl(eGradFun, dofJ, g, jac, phiGJ);

    integral1 +=
      ((phiGI * phiGJ)) * fabs(det) * (*gW1)(g);
  }


  // Loop over Integration Point (Term 2) //
  for(int g = 0; g < G2; g++){
    det = MJac[g].second;

    phiI = eFun(dofI, g);
    phiJ = eFun(dofJ, g);

    integral2 +=
      ((phiI * phiJ) * kSquare) * fabs(det) * (*gW2)(g);
  }

  return integral1 - integral2;
}

template<typename scalar>
scalar FormulationSteadySlowScalar<scalar>::
rhs(size_t equationI, size_t elementId) const{
  return 0;
}

template<typename scalar>
const FunctionSpace& FormulationSteadySlowScalar<scalar>::field(void) const{
  return *fspace;
}

template<typename scalar>
const FunctionSpace& FormulationSteadySlowScalar<scalar>::test(void) const{
  return *fspace;
}

template<typename scalar>
const GroupOfElement& FormulationSteadySlowScalar<scalar>::domain(void) const{
  return *ddomain;
}

template<typename scalar>
bool FormulationSteadySlowScalar<scalar>::isBlock(void) const{
  return true;
}

////////////////////////////
// Explicit instantiation //
////////////////////////////
template class sf::FormulationSteadySlowScalar<Complex>;
template class sf::FormulationSteadySlowScalar<double>;
