#include "SmallFem.h"
#include "Formulation.h"

using namespace sf;

template<typename scalar>
Formulation<scalar>::~Formulation(void){
}

template<typename scalar>
void Formulation<scalar>::update(void){
}

////////////////////////////
// Explicit instantiation //
////////////////////////////
template class sf::Formulation<Complex>;
template class sf::Formulation<double>;
