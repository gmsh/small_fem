#include "SmallFem.h"
#include "Exception.h"
#include "FormulationQ3DEStarMass.h"
#include "FormulationQ3DEStarMassOne.h"
#include "FormulationQ3DEStarMassTwo.h"
#include "TermFieldField.h"
#include "TermGradGrad.h"

using namespace std;
using namespace sf;

FormulationQ3DEStarMass::
FormulationQ3DEStarMass(const GroupOfElement& domain,
                        const FunctionSpace0Form& hGrad,
                        const FunctionSpace1Form& hCurl,
                        void (*r)(fullVector<double>&,
                                  fullMatrix<Complex>&),
                        Complex (*invR)(fullVector<double>&),
                        int order){

  pair<bool, size_t> uniform = domain.isUniform();
  size_t               eType = uniform.second;

  if(!uniform.first)
    throw Exception("FormulationQ3DEStarMass needs a uniform mesh");

  const Basis& basisGrad = hGrad.getBasis(eType);
  const Basis& basisCurl = hCurl.getBasis(eType);

  int gOrder;
  if(order < 0)
    gOrder = max(basisGrad.getOrder(), basisCurl.getOrder()) * 2;
  else
    gOrder = order;
  Quadrature gauss(eType, gOrder, 1);

  const fullMatrix<double>& gC = gauss.getPoints();
  basisGrad.preEvaluateFunctions(gC);
  basisCurl.preEvaluateFunctions(gC);

  GroupOfJacobian jac(domain, gC, "both");

  localFF = new TermFieldField<Complex>(jac, basisGrad, gauss, invR);
  localGG = new TermGradGrad<Complex>  (jac, basisCurl, gauss, r);

  fList.push_back(new FormulationQ3DEStarMassOne(domain, hCurl, *localGG));
  fList.push_back(new FormulationQ3DEStarMassTwo(domain, hGrad, *localFF));
}

FormulationQ3DEStarMass::~FormulationQ3DEStarMass(void){
  // Iterate & Delete Formulations //
  list<FormulationBlock<Complex>*>::iterator end = fList.end();
  list<FormulationBlock<Complex>*>::iterator it  = fList.begin();

  for(; it !=end; it++)
    delete *it;

  delete localGG;
  delete localFF;
}

const list<FormulationBlock<Complex>*>&
FormulationQ3DEStarMass::getFormulationBlocks(void) const{
  return fList;
}

bool FormulationQ3DEStarMass::isBlock(void) const{
  return false;
}
