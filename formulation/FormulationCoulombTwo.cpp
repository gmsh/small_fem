#include "SmallFem.h"
#include "BasisGenerator.h"
#include "GroupOfJacobian.h"
#include "Quadrature.h"

#include "FormulationCoulombTwo.h"

using namespace std;
using namespace sf;

FormulationCoulombTwo::FormulationCoulombTwo(void){
}

FormulationCoulombTwo::FormulationCoulombTwo(const GroupOfElement& domain,
                                             const FunctionSpace1Form& field,
                                             const FunctionSpace0Form& test,
                                             const TermGradGrad<double>& term){
  // Save Data //
  this->ddomain = &domain;
  this->ffield  = &field;
  this->ttest   = &test;
  this->term    = &term;
}

FormulationCoulombTwo::~FormulationCoulombTwo(void){
}

double FormulationCoulombTwo::
weak(size_t dofI, size_t dofJ, size_t elementId) const{
  return term->getTerm(dofI, dofJ, elementId);
}

double FormulationCoulombTwo::rhs(size_t equationI, size_t elementId) const{
  return 0;
}

const FunctionSpace& FormulationCoulombTwo::field(void) const{
  return *ffield;
}

const FunctionSpace& FormulationCoulombTwo::test(void) const{
  return *ttest;
}

const GroupOfElement& FormulationCoulombTwo::domain(void) const{
  return *ddomain;
}

bool FormulationCoulombTwo::isBlock(void) const{
  return true;
}
