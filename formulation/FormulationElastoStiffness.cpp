#include "SmallFem.h"
#include "FormulationElastoStiffness.h"
#include "FormulationElastoStiffnessUV.h"

#include "GroupOfJacobian.h"
#include "TermGradGrad.h"
#include "Quadrature.h"

using namespace std;
using namespace sf;

template<typename scalar>
bool FormulationElastoStiffness<scalar>::once = true;

template<typename scalar>
double FormulationElastoStiffness<scalar>::lambda;

template<typename scalar>
double FormulationElastoStiffness<scalar>::mu;

template<typename scalar>
FormulationElastoStiffness<scalar>::
FormulationElastoStiffness(const GroupOfElement& dom,
                           const FunctionSpace0Form& hGradX,
                           const FunctionSpace0Form& hGradY,
                           const FunctionSpace0Form& hGradZ,
                           double E,
                           double nu){
  // Once ? //
  if(once)
    once = false;
  else
    throw Exception("FormulationElastoStiffness can be instanciated only once"
                    " (I know, that's stupid but I'm tired:"
                    " I'll fix that one day and drop good old c++98)");

  // Hook (Lame coefficients) //
  this->lambda = E * nu / ((1+nu) * (1-2*nu));
  this->mu     = E / (2*(1+nu));

  // Check domain stats: uniform mesh //
  pair<bool, size_t> uniform = dom.isUniform();
  size_t               eType = uniform.second;

  if(!uniform.first)
    throw Exception("FormulationElastoStiffness needs a uniform mesh");

  // Check orders //
  if(hGradX.getOrder() != hGradY.getOrder() ||
     hGradX.getOrder() != hGradZ.getOrder())
    throw Exception("FormulationElastoStiffness needs a uniform orders");

  // Get Bases //
  const Basis& basisX = hGradX.getBasis(eType);
  const Basis& basisY = hGradY.getBasis(eType);
  const Basis& basisZ = hGradZ.getBasis(eType);
  const size_t order  = basisX.getOrder();

  // Gaussian Quadrature //
  Quadrature gauss(eType, order - 1, 2);
  const fullMatrix<double>& gC = gauss.getPoints();

  // Derivatives //
  basisX.preEvaluateDerivatives(gC);
  basisY.preEvaluateDerivatives(gC);
  basisZ.preEvaluateDerivatives(gC);

  // Jacobian //
  GroupOfJacobian jac(dom, gC, "invert");

  // Local Terms //
  termXX = new TermGradGrad<scalar>(jac, basisX, basisX, gauss, Dxx);
  termYX = new TermGradGrad<scalar>(jac, basisY, basisX, gauss, Dyx);
  termZX = new TermGradGrad<scalar>(jac, basisZ, basisX, gauss, Dzx);

  termXY = new TermGradGrad<scalar>(jac, basisX, basisY, gauss, Dxy);
  termYY = new TermGradGrad<scalar>(jac, basisY, basisY, gauss, Dyy);
  termZY = new TermGradGrad<scalar>(jac, basisZ, basisY, gauss, Dzy);

  termXZ = new TermGradGrad<scalar>(jac, basisX, basisZ, gauss, Dxz);
  termYZ = new TermGradGrad<scalar>(jac, basisY, basisZ, gauss, Dyz);
  termZZ = new TermGradGrad<scalar>(jac, basisZ, basisZ, gauss, Dzz);

  // Formulations //
  fList.push_back
    (new FormulationElastoStiffnessUV<scalar>(dom, hGradX, hGradX, *termXX));
  fList.push_back
    (new FormulationElastoStiffnessUV<scalar>(dom, hGradY, hGradX, *termYX));
  fList.push_back
    (new FormulationElastoStiffnessUV<scalar>(dom, hGradZ, hGradX, *termZX));

  fList.push_back
    (new FormulationElastoStiffnessUV<scalar>(dom, hGradX, hGradY, *termXY));
  fList.push_back
    (new FormulationElastoStiffnessUV<scalar>(dom, hGradY, hGradY, *termYY));
  fList.push_back
    (new FormulationElastoStiffnessUV<scalar>(dom, hGradZ, hGradY, *termZY));

  fList.push_back
    (new FormulationElastoStiffnessUV<scalar>(dom, hGradX, hGradZ, *termXZ));
  fList.push_back
    (new FormulationElastoStiffnessUV<scalar>(dom, hGradY, hGradZ, *termYZ));
  fList.push_back
    (new FormulationElastoStiffnessUV<scalar>(dom, hGradZ, hGradZ, *termZZ));
}

template<typename scalar>
FormulationElastoStiffness<scalar>::
~FormulationElastoStiffness(void){
  // Iterate & Delete Formulations //
  typename list<FormulationBlock<scalar>*>::iterator end = fList.end();
  typename list<FormulationBlock<scalar>*>::iterator it  = fList.begin();

  for(; it !=end; it++)
    delete *it;

  // Delete terms //
  delete termXX;
  delete termYX;
  delete termZX;

  delete termXY;
  delete termYY;
  delete termZY;

  delete termXZ;
  delete termYZ;
  delete termZZ;
}

template<typename scalar>
const list<FormulationBlock<scalar>*>&
FormulationElastoStiffness<scalar>::getFormulationBlocks(void) const{
  return fList;
}

template<typename scalar>
bool FormulationElastoStiffness<scalar>::isBlock(void) const{
  return false;
}

template<typename scalar>
void FormulationElastoStiffness<scalar>::Dxx(fullVector<double>& xyz,
                                             fullMatrix<scalar>& T){
  T.scale(0.);
  T(0, 0) = 2*mu + lambda;
  T(1, 1) = mu;
  T(2, 2) = mu;
}

template<typename scalar>
void FormulationElastoStiffness<scalar>::Dyx(fullVector<double>& xyz,
                                             fullMatrix<scalar>& T){
  T.scale(0.);
  T(0, 1) = lambda;
  T(1, 0) = mu;
}

template<typename scalar>
void FormulationElastoStiffness<scalar>::Dzx(fullVector<double>& xyz,
                                             fullMatrix<scalar>& T){
  T.scale(0.);
  T(0, 2) = lambda;
  T(2, 0) = mu;
}

template<typename scalar>
void FormulationElastoStiffness<scalar>::Dxy(fullVector<double>& xyz,
                                             fullMatrix<scalar>& T){
  T.scale(0.);
  T(0, 1) = mu;
  T(1, 0) = lambda;
}

template<typename scalar>
void FormulationElastoStiffness<scalar>::Dyy(fullVector<double>& xyz,
                                             fullMatrix<scalar>& T){
  T.scale(0.);
  T(0, 0) = mu;
  T(1, 1) = 2*mu + lambda;
  T(2, 2) = mu;
}

template<typename scalar>
void FormulationElastoStiffness<scalar>::Dzy(fullVector<double>& xyz,
                                             fullMatrix<scalar>& T){
  T.scale(0.);
  T(1, 2) = lambda;
  T(2, 1) = mu;
}

template<typename scalar>
void FormulationElastoStiffness<scalar>::Dxz(fullVector<double>& xyz,
                                             fullMatrix<scalar>& T){
  T.scale(0.);
  T(0, 2) = mu;
  T(2, 0) = lambda;
}

template<typename scalar>
void FormulationElastoStiffness<scalar>::Dyz(fullVector<double>& xyz,
                                             fullMatrix<scalar>& T){
  T.scale(0.);
  T(1, 2) = mu;
  T(2, 1) = lambda;
}

template<typename scalar>
void FormulationElastoStiffness<scalar>::Dzz(fullVector<double>& xyz,
                                             fullMatrix<scalar>& T){
  T.scale(0.);
  T(0, 0) = mu;
  T(1, 1) = mu;
  T(2, 2) = 2*mu + lambda;
}

////////////////////////////
// Explicit instantiation //
////////////////////////////
template class sf::FormulationElastoStiffness<Complex>;
template class sf::FormulationElastoStiffness<double>;
