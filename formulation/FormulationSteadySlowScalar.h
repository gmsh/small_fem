#ifndef _FORMULATIONSTEADYSLOWSCALAR_H_
#define _FORMULATIONSTEADYSLOWSCALAR_H_

#include <vector>

#include "FunctionSpace.h"
#include "GroupOfJacobian.h"
#include "FormulationBlock.h"

/**
   @class FormulationSteadySlowScalar
   @brief Scalar Formulation for the steady wave problem (slow version)

   Slow version of the scalar Formulation for the steady wave problem.
   This version don't use the fast integration algorithm.
 */

namespace sf{
  template<typename scalar> class FormulationSteadySlowScalar:
    public FormulationBlock<scalar>{
  private:
    // Wavenumber Squared //
    double kSquare;

    // Gaussian Quadrature Data (Term One) //
    int G1;
    fullMatrix<double>* gC1;
    fullVector<double>* gW1;

    // Gaussian Quadrature Data (Term Two) //
    int G2;
    fullMatrix<double>* gC2;
    fullVector<double>* gW2;

    // Domain //
    const GroupOfElement* ddomain;

    // Jacobians //
    GroupOfJacobian* jac1;
    GroupOfJacobian* jac2;

    // Function Space & Basis //
    const FunctionSpace* fspace;
    const Basis*         basis;

  public:
    FormulationSteadySlowScalar(const GroupOfElement& domain,
                                const FunctionSpace& fs,
                                double k);

    virtual ~FormulationSteadySlowScalar(void);

    virtual scalar weak(size_t dofI, size_t dofJ, size_t elementId) const;
    virtual scalar rhs(size_t equationI, size_t elementId)          const;

    virtual const FunctionSpace&   field(void) const;
    virtual const FunctionSpace&    test(void) const;
    virtual const GroupOfElement& domain(void) const;

    virtual bool isBlock(void) const;
  };
}

/**
   @fn FormulationSteadySlowScalar::FormulationSteadySlowScalar
   @param domain A GroupOfElement
   @param fs A FunctionSpace1Form for both unknown and test field
   @param k a scalar

   Instantiates a new FormulationSteadySlowScalar with given parametres:
   @li domain for the domain of this Formulation
   @li fs for the function space used for the unknown field
       and the test functions
   @li k for wavenumber
   **

   @fn FormulationSteadySlowScalar::~FormulationSteadySlowScalar
   Deletes this FormulationSteadySlowScalar
*/

#endif
