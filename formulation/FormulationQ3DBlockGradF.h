#ifndef _FORMULATIONQ3DPARAMFGRADF_H_
#define _FORMULATIONQ3DPARAMFGRADF_H_

#include "SmallFem.h"
#include "FunctionSpace0Form.h"
#include "FunctionSpace1Form.h"
#include "GroupOfJacobian.h"

#include "FormulationBlock.h"
#include "FormulationQ3DGeneralFGrad.h"

namespace sf{
  class FormulationQ3DBlockGradF: public FormulationBlock<Complex>{
  private:
    friend class FormulationQ3DGeneralGradF;
    friend class FormulationQ3DGeneralGradFDunn;
    friend class FormulationQ3DErzEphiCoulomb;

  private:
    // multiplicative constant and exponent
    double multConst;
    double exponent;

    // Function Space & Domain //
    const FunctionSpace0Form* ffield;
    const FunctionSpace1Form* ttest;
    const GroupOfElement*     ddomain;

    // Gaussian Quadrature Data //
    int G;
    const fullMatrix<double>* gC;
    const fullVector<double>* gW;

    // Jacobians and basis//
    const GroupOfJacobian* jac;
    const Basis*           basisTest;
    const Basis*           basisField;

    void (*f)(fullVector<double>&, fullMatrix<Complex>&);


  private:
    FormulationQ3DBlockGradF(void);
    FormulationQ3DBlockGradF(const GroupOfElement& domain,
                             const FunctionSpace0Form& fs0,
                             const FunctionSpace1Form& fs1,
                             double multConst,
                             double exponent,
                             const Basis& basisTest,
                             const Basis& basisField,
                             const GroupOfJacobian& jac,
                             int G,
                             const fullMatrix<double>& gC,
                             const fullVector<double>& gW,
                             void (*f)(fullVector<double>&,
                                       fullMatrix<Complex>&));

  public:
    virtual ~FormulationQ3DBlockGradF(void);

    virtual Complex weak(size_t dofI, size_t dofJ, size_t elementId) const;
    virtual Complex rhs(size_t equationI, size_t elementId)          const;

    virtual const FunctionSpace&   field(void) const;
    virtual const FunctionSpace&    test(void) const;
    virtual const GroupOfElement& domain(void) const;

    virtual bool isBlock(void) const;
  };
}

#endif
