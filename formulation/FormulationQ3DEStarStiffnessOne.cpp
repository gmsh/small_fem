#include "SmallFem.h"
#include "FormulationQ3DEStarStiffnessOne.h"

#include "Quadrature.h"
#include "Mapper.h"

using namespace std;
using namespace sf;

FormulationQ3DEStarStiffnessOne::FormulationQ3DEStarStiffnessOne(void){
}

FormulationQ3DEStarStiffnessOne::
FormulationQ3DEStarStiffnessOne(const GroupOfElement& domain,
                                  const FunctionSpace1Form& fs, 
                                  const TermCurlCurl<Complex>& localCC
                                 ){
  // Save Data //
  this->ddomain = &domain;
  this->fs      = &fs;
  this->localCC = &localCC; 
  
}

FormulationQ3DEStarStiffnessOne::~FormulationQ3DEStarStiffnessOne(void){
}

Complex FormulationQ3DEStarStiffnessOne::
weak(size_t dofI, size_t dofJ, size_t elementId) const{
   Complex c(1, 0); 
   return c * localCC->getTerm(dofI, dofJ, elementId);
}

Complex FormulationQ3DEStarStiffnessOne::
rhs(size_t equationI, size_t elementId) const{
  return Complex(0, 0);
}

const FunctionSpace& FormulationQ3DEStarStiffnessOne::field(void) const{
  return *fs;
}

const FunctionSpace& FormulationQ3DEStarStiffnessOne::test(void) const{
  return *fs;
}

const GroupOfElement& FormulationQ3DEStarStiffnessOne::domain(void) const{
  return *ddomain;
}

bool FormulationQ3DEStarStiffnessOne::isBlock(void) const{
  return true;
}
