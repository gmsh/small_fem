#include "SmallFem.h"
#include "FormulationQ3DBlockZFZGrad.h"

#include "Quadrature.h"
#include "Mapper.h"

using namespace std;
using namespace sf;

FormulationQ3DBlockZFZGrad::FormulationQ3DBlockZFZGrad(void){
}

FormulationQ3DBlockZFZGrad::
FormulationQ3DBlockZFZGrad(const GroupOfElement& domain,
                          const FunctionSpace0Form& fs0,      
                          const FunctionSpace1Form& fs1,
                          double multConst, 
                          double exponent, 
                          const Basis& basisTest, 
                          const Basis& basisField, 
                          const GroupOfJacobian& jac,
                          int G,
                          const fullMatrix<double>& gC,
                          const fullVector<double>& gW,
                           void (*f)(fullVector<double>&, fullMatrix<Complex>&)
                         ){
  // Save Data //
  this->ddomain   = &domain;
  this->ffield  = &fs1;
  this->ttest   = &fs0;
  this->multConst = multConst; 
  this->exponent  = exponent; 

  // Get Basis //
  this->basisTest  = &basisTest;
  this->basisField  = &basisField;
  this->jac        = &jac; 
  this->gC         = &gC; 
  this->gW         = &gW; 
  this->G          = G;
  
  this->f          = f;
}

FormulationQ3DBlockZFZGrad::~FormulationQ3DBlockZFZGrad(void){
  
}

Complex FormulationQ3DBlockZFZGrad::
weak(size_t dofI, size_t dofJ, size_t elementId) const{
  // Init Some Stuff //
  fullMatrix<double> mat;
  fullVector<double> phiI(3);
  fullVector<double> phiJ(3);

  Complex integral = Complex(0, 0);
  double  det;
  double  pxyz[3];
  
  fullVector<double>  xyz(3);
  fullMatrix<Complex> TMatrix(3,3);
  Complex intMatPhiIPhiJ;

  // Get Element //
  const MElement& element = ddomain->get(elementId);

  // Get Basis Functions //
  const fullMatrix<double>& eField =
    basisField->getPreEvaluatedFunctions(element);
    
  const fullMatrix<double>& eTest =
    basisTest->getPreEvaluatedDerivatives(element);

  // Get Jacobians //
  const vector<pair<fullMatrix<double>, double> >& invJac =
    jac->getInvertJacobianMatrix(elementId);


  // Loop over Integration Point //
  for(int g = 0; g < G; g++){
    det = invJac[g].second;   // det(Jac) at integration point g
    mat = invJac[g].first;    // J        at integration point g
    
    ReferenceSpaceManager::
      mapFromABCtoXYZ(element, (*gC)(g, 0), (*gC)(g, 1), (*gC)(g, 2), pxyz);
                            // xyz coordinates of point g

    Mapper::hCurl(eField, dofJ, g, mat, phiJ);
    Mapper::hCurl(eTest, dofI, g, mat, phiI);
    
    xyz(0) = pxyz[0];
     xyz(1) = pxyz[1];
     xyz(2) = pxyz[2];
    
    f(xyz, TMatrix); 
    // WARNING! Only diagonal matrices possible! 
    intMatPhiIPhiJ = TMatrix(1,1) * phiI(1) * phiJ(1);

    integral +=
      intMatPhiIPhiJ * fabs(det) * (*gW)(g) * pow(pxyz[0], exponent);
  }

  // Done
  return multConst*integral;
}

Complex FormulationQ3DBlockZFZGrad::
rhs(size_t equationI, size_t elementId) const{
  return Complex(0, 0);
}

const FunctionSpace& FormulationQ3DBlockZFZGrad::field(void) const{
  return *ffield;
}

const FunctionSpace& FormulationQ3DBlockZFZGrad::test(void) const{
  return *ttest;
}

const GroupOfElement& FormulationQ3DBlockZFZGrad::domain(void) const{
  return *ddomain;
}

bool FormulationQ3DBlockZFZGrad::isBlock(void) const{
  return true;
}

