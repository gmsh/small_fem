#include "SmallFem.h"
#include "Exception.h"

#include "FormulationQ3DGeneralGradGrad.h"
#include "FormulationQ3DBlockGradGrad.h"
#include "FormulationQ3DBlockGradRadialF.h"
#include "FormulationQ3DBlockRadialFGrad.h"
#include "FormulationQ3DBlockFF0Form.h"


using namespace std;
using namespace sf;

FormulationQ3DGeneralGradGrad::FormulationQ3DGeneralGradGrad(const GroupOfElement& domain,
                          const FunctionSpace0Form& fs,
                          double multConst, 
                          double exponent,
                          double beta, 
                          const Basis& basis, 
                          const GroupOfJacobian& jac,
                          int G,
                          const fullMatrix<double>& gC,
                          const fullVector<double>& gW,
                          void (*f)(fullVector<double>&, fullMatrix<Complex>&)
                                            ){

      fList.push_back(
          new FormulationQ3DBlockGradGrad (
              domain, fs, multConst, 2*beta + exponent, basis, jac, G, gC, gW, f));
      if(beta*multConst != 0) {
          fList.push_back(
          new FormulationQ3DBlockGradRadialF (
              domain, fs, beta * multConst, 2*beta + exponent - 1, basis, jac, G, gC, gW, f));
      
          fList.push_back(
              new FormulationQ3DBlockRadialFGrad (
                  domain, fs, beta * multConst, 2*beta + exponent - 1, basis, jac, G, gC, gW, f));
          
          fList.push_back(
              new FormulationQ3DBlockFF0Form (
                  domain, fs, beta * beta * multConst, 2*beta + exponent - 2, basis, jac, G, gC, gW, f));
    }
}

FormulationQ3DGeneralGradGrad::~FormulationQ3DGeneralGradGrad(void){
  list<FormulationBlock<Complex>*>::iterator end = fList.end();
  list<FormulationBlock<Complex>*>::iterator it  = fList.begin();

  for(; it !=end; it++)
    delete *it;
  
}

const list<FormulationBlock<Complex>*>&
FormulationQ3DGeneralGradGrad::getFormulationBlocks(void) const{
  return fList;
}

bool FormulationQ3DGeneralGradGrad::isBlock(void) const{
  return false;
}
 
