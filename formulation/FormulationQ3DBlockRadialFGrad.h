#ifndef _FORMULATIONQ3DPARAMRADIALFGRADGRAD_H_
#define _FORMULATIONQ3DPARAMRADIALFGRADGRAD_H_

#include "SmallFem.h"
#include "FunctionSpace1Form.h"
#include "GroupOfJacobian.h"

#include "FormulationBlock.h"
#include "FormulationQ3DGeneralGradGrad.h"

namespace sf{
  class FormulationQ3DBlockRadialFGrad: public FormulationBlock<Complex>{
  private:
    friend class FormulationQ3DGeneralGradGrad;
    friend class FormulationQ3DGeneralFGradDunn;
    friend class FormulationQ3DErzEphiCoulomb;

  private:
    // multiplicative constant and exponent
    double multConst;
    double exponent;

    // Function Space & Domain //
    const FunctionSpace0Form* ttest;
    const FunctionSpace0Form* ffield;
    const GroupOfElement*     ddomain;

    // Gaussian Quadrature Data //
    int G;
    const fullMatrix<double>* gC;
    const fullVector<double>* gW;

    // Jacobians and basis//
    const GroupOfJacobian* jac;
    const Basis*           basisTest;
    const Basis*           basisField;

    void (*f)(fullVector<double>&, fullMatrix<Complex>&);

  private:
    FormulationQ3DBlockRadialFGrad(void);
    FormulationQ3DBlockRadialFGrad(const GroupOfElement& domain,
                                   const FunctionSpace0Form& fs,
                                   double multConst,
                                   double exponent,
                                   const Basis& basis,
                                   const GroupOfJacobian& jac,
                                   int G,
                                   const fullMatrix<double>& gC,
                                   const fullVector<double>& gW,
                                   void (*f)(fullVector<double>&,
                                             fullMatrix<Complex>&));

    FormulationQ3DBlockRadialFGrad(const GroupOfElement& domain,
                                   const FunctionSpace0Form& fRadial,
                                   const FunctionSpace0Form& fGrad,
                                   double multConst,
                                   double exponent,
                                   const Basis& basisRadial,
                                   const Basis& basisGrad,
                                   const GroupOfJacobian& jac,
                                   int G,
                                   const fullMatrix<double>& gC,
                                   const fullVector<double>& gW,
                                   void (*f)(fullVector<double>&,
                                             fullMatrix<Complex>&));

  public:
    virtual ~FormulationQ3DBlockRadialFGrad(void);

    virtual Complex weak(size_t dofI, size_t dofJ, size_t elementId) const;
    virtual Complex rhs(size_t equationI, size_t elementId)          const;

    virtual const FunctionSpace&   field(void) const;
    virtual const FunctionSpace&    test(void) const;
    virtual const GroupOfElement& domain(void) const;

    virtual bool isBlock(void) const;
  };
}

#endif
