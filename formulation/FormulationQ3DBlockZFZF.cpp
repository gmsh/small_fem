#include "SmallFem.h"
#include "FormulationQ3DBlockZFZF.h"

#include "Quadrature.h"
#include "Mapper.h"

using namespace std;
using namespace sf;

FormulationQ3DBlockZFZF::FormulationQ3DBlockZFZF(void){
}

FormulationQ3DBlockZFZF::
FormulationQ3DBlockZFZF(const GroupOfElement& domain,
                          const FunctionSpace1Form& fs,
                          double multConst, 
                          double exponent, 
                          const Basis& basis, 
                          const GroupOfJacobian& jac,
                          int G,
                          const fullMatrix<double>& gC,
                          const fullVector<double>& gW,
                        void (*f)(fullVector<double>&, fullMatrix<Complex>&)
                         ){
  // Save Data //
  this->ddomain   = &domain;
  this->fs        = &fs; 
  this->multConst = multConst; 
  this->exponent  = exponent; 

  // Get Basis //
  this->basis      = &basis;
  this->jac        = &jac; 
  this->gC         = &gC; 
  this->gW         = &gW; 
  this->G          = G;
  
  this->f          = f;
}

FormulationQ3DBlockZFZF::~FormulationQ3DBlockZFZF(void){
  
}

Complex FormulationQ3DBlockZFZF::
weak(size_t dofI, size_t dofJ, size_t elementId) const{
  // Init Some Stuff //
  fullMatrix<double> mat;
  fullVector<double> phiI(3);
  fullVector<double> phiJ(3);

  Complex integral = Complex(0, 0);
  double  det;
  double  pxyz[3];
  
  fullVector<double>  xyz(3);
  fullMatrix<Complex> TMatrix(3,3);
  Complex intMatPhiIPhiJ;

  // Get Element //
  const MElement& element = ddomain->get(elementId);

  // Get Basis Functions //
  const fullMatrix<double>& eFun =
    basis->getPreEvaluatedFunctions(element);


  // Get Jacobians //
 
    
  const vector<pair<fullMatrix<double>, double> >& invJac =
    jac->getInvertJacobianMatrix(elementId);

  // Loop over Integration Point //
  for(int g = 0; g < G; g++){
    det    = invJac[g].second;   // det(Jac) at integration point g
    mat    = invJac[g].first;
    
    ReferenceSpaceManager::
      mapFromABCtoXYZ(element, (*gC)(g, 0), (*gC)(g, 1), (*gC)(g, 2), pxyz);
                            // xyz coordinates of point g

    Mapper::hCurl(eFun, dofI, g, mat, phiI); // (e_I)|g in xyz-space
    Mapper::hCurl(eFun, dofJ, g, mat, phiJ); // (e_I)|g in xyz-space

    xyz(0) = pxyz[0];
     xyz(1) = pxyz[1];
     xyz(2) = pxyz[2];
    
    f(xyz, TMatrix); 
    // WARNING! Only diagonal matrices possible! 
    intMatPhiIPhiJ = TMatrix(1,1) * phiI(1) * phiJ(1);
                        
    
    
    integral +=
      intMatPhiIPhiJ * fabs(det) * (*gW)(g) * pow(pxyz[0], exponent);
  }

  // Done
  return multConst * integral;
}

Complex FormulationQ3DBlockZFZF::
rhs(size_t equationI, size_t elementId) const{
  return Complex(0, 0);
}

const FunctionSpace& FormulationQ3DBlockZFZF::field(void) const{
  return *fs;
}

const FunctionSpace& FormulationQ3DBlockZFZF::test(void) const{
  return *fs;
}

const GroupOfElement& FormulationQ3DBlockZFZF::domain(void) const{
  return *ddomain;
}

bool FormulationQ3DBlockZFZF::isBlock(void) const{
  return true;
}

