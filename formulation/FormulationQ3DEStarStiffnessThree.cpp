#include "SmallFem.h"
#include "FormulationQ3DEStarStiffnessThree.h"

#include "Quadrature.h"
#include "Mapper.h"

#include "TermFieldField.h"
#include "TermGradGrad.h"
#include "TermCurlCurl.h"

using namespace std;
using namespace sf;

FormulationQ3DEStarStiffnessThree::FormulationQ3DEStarStiffnessThree(void){
}

FormulationQ3DEStarStiffnessThree::
FormulationQ3DEStarStiffnessThree(const GroupOfElement& domain,
                                  const FunctionSpace1Form& fs,
                                  int n, 
                                  const TermGradGrad<Complex>& localGIn
                                   ){
  
  // Save Data //
  this->ddomain = &domain;
  this->fs      = &fs;
  this->n       = n; 
  this->localGIn = &localGIn; 

}

FormulationQ3DEStarStiffnessThree::~FormulationQ3DEStarStiffnessThree(void){
 
}

Complex FormulationQ3DEStarStiffnessThree::
weak(size_t dofI, size_t dofJ, size_t elementId) const{
  Complex c(1, 0); 
  return static_cast<double>(n)* static_cast<double>(n)* c * localGIn->getTerm(dofI, dofJ, elementId);
}

Complex FormulationQ3DEStarStiffnessThree::
rhs(size_t equationI, size_t elementId) const{
  return Complex(0, 0);
}

const FunctionSpace& FormulationQ3DEStarStiffnessThree::field(void) const{
  return *fs;
}

const FunctionSpace& FormulationQ3DEStarStiffnessThree::test(void) const{
  return *fs;
}

const GroupOfElement& FormulationQ3DEStarStiffnessThree::domain(void) const{
  return *ddomain;
}

bool FormulationQ3DEStarStiffnessThree::isBlock(void) const{
  return true;
}
