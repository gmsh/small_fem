#include "SmallFem.h"
#include "FormulationDummy.h"

using namespace sf;

template<typename scalar>
FormulationDummy<scalar>::FormulationDummy(void){
  // Empty List //
  fList.clear();
}

template<typename scalar>
FormulationDummy<scalar>::~FormulationDummy(void){
}

template<typename scalar>
const std::list<FormulationBlock<scalar>*>&
FormulationDummy<scalar>::getFormulationBlocks(void) const{
  return fList;
}

template<typename scalar>
bool FormulationDummy<scalar>::isBlock(void) const{
  return false;
}

////////////////////////////
// Explicit instantiation //
////////////////////////////
template class sf::FormulationDummy<Complex>;
template class sf::FormulationDummy<double>;
