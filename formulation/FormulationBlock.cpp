#include "SmallFem.h"
#include "FormulationBlock.h"

using namespace sf;

template<typename scalar>
FormulationBlock<scalar>::~FormulationBlock(void){
}

template<typename scalar>
bool FormulationBlock<scalar>::isBlock(void) const{
  return true;
}

////////////////////////////
// Explicit instantiation //
////////////////////////////
template class sf::FormulationBlock<Complex>;
template class sf::FormulationBlock<double>;
