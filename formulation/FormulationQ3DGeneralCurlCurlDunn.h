#ifndef _FORMULATIONQ3DGENERALCURLCURLDUNN_H_
#define _FORMULATIONQ3DGENERALCURLCURLDUNN_H_

// include needed header files
#include "SmallFem.h"
#include "FormulationCoupled.h"
#include "FunctionSpace0Form.h"
#include "FunctionSpace1Form.h"
#include "GroupOfJacobian.h"
#include "Quadrature.h"

/**
   @class FormulationQ3DGeneralCurlCurlDunn
   @brief General Term for curl-curl bilinear forms 
   

   Calculates multConst * (r^exponent curl(Erz), curl(Erz)) 
                   = 1/n^2 * multConst
                           *(r^exponent curl(r*U-[v;0]), curl(r*U'-[v';0])) 
                   = ...
       
   @author Erik Schnaubelt
*/

namespace sf{
  class FormulationQ3DGeneralCurlCurlDunn: public FormulationCoupled<Complex>{
  private:
    friend class FormulationQ3DnErzMass;
  
  
  private:
    std::list<FormulationBlock<Complex>*> fList;

  public:
    FormulationQ3DGeneralCurlCurlDunn(const GroupOfElement& domain,
                          const FunctionSpace0Form& fsGrad,
                          const FunctionSpace1Form& fsCurl,
                          double multConst, 
                          double exponent, 
                          int n, 
                          const Basis& basisGrad, 
                          const Basis& basisCurl,
                          const GroupOfJacobian& jac,
                          int G,
                          const fullMatrix<double>& gC,
                          const fullVector<double>& gW,
                          void (*f)(fullVector<double>&, fullMatrix<Complex>&)
                         );

    virtual ~FormulationQ3DGeneralCurlCurlDunn(void);

    virtual const std::list<FormulationBlock<Complex>*>&
                                               getFormulationBlocks(void) const;
    virtual bool isBlock(void) const;
  };
}

#endif
 
