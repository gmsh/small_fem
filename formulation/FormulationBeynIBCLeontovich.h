#ifndef _FORMULATIONBEYNIBCLEONTOVICH_H_
#define _FORMULATIONBEYNIBCLEONTOVICH_H_

#include "SmallFem.h"
#include "BeynContext.h"
#include "FormulationBlock.h"
#include "GroupOfElement.h"
#include "FunctionSpace1Form.h"
#include "Term.h"

/**
   @class FormulationBeynIBCLeontovich
   @brief Leontovich IBC formulation for the Beyn's method (h-formulation)

   Leontovich IBC formulation for the Beyn's method (h-formulation)
 */

namespace sf{
  class FormulationBeynIBCLeontovich: public FormulationBlock<Complex>{
  private:
    static const Complex J;
    static const double  Pi;
    static const double  Mu0;

  private:
    // Function Space & Domain //
    const FunctionSpace1Form*  ffs;
    const GroupOfElement*      ddomain;

    // Beyn Context //
    BeynContext* ctx;

    // Data //
    Complex omega;  // Angular frequency
    Complex sigma;  // Conductivity

    // Local Terms //
    Term<double>* term;

  public:
    FormulationBeynIBCLeontovich(const GroupOfElement& domain,
                                 const FunctionSpace1Form& fs,
                                 BeynContext& context,
                                 Complex sigma);

    virtual ~FormulationBeynIBCLeontovich(void);

    virtual Complex weak(size_t dofI, size_t dofJ, size_t elementId) const;
    virtual Complex rhs(size_t equationI, size_t elementId)          const;

    virtual const FunctionSpace&   field(void) const;
    virtual const FunctionSpace&    test(void) const;
    virtual const GroupOfElement& domain(void) const;

    virtual bool isBlock(void) const;
    virtual void update(void);
  };
}

/**
   @fn FormulationBeynIBCLeontovich::FormulationBeynIBCLeontovich
   @param domain A GroupOfElement
   @param fs A FunctionSpace1Form for both unknown and test field
   @param context k a BeynContext
   @param sigma a complex number

   Instantiates a new FormulationBeynIBCLeontovich with given parametres:
   @li domain for the domain of this Formulation
   @li fs for the H(Curl) function space used for the unknown field
       and the test functions
   @li context the BeynContext containing the current angular frequency
   @li sigma the electric conductivity
   **

   @fn FormulationBeynIBCLeontovich::~FormulationBeynIBCLeontovich
   Deletes this FormulationBeynIBCLeontovich
   **

   @fn FormulationBeynIBCLeontovich::update
   Asks this FormulationBeynIBCLeontovich for the new angular frequency and propagate
*/
#endif
