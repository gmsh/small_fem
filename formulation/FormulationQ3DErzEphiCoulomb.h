#ifndef _FORMULATIONQ3DERZEPHICOULOMB_H_
#define _FORMULATIONQ3DERZEPHICOULOMB_H_

// include needed header files
#include "SmallFem.h"
#include "FormulationCoupled.h"
#include "FunctionSpace0Form.h"
#include "FunctionSpace1Form.h"
#include "GroupOfJacobian.h"
#include "Quadrature.h"

/**
   @class FormulationQ3DErzEphiMass
   @brief Coulomb gauge for Oh ansatz
*/

namespace sf{
  class FormulationQ3DErzEphiCoulomb: public FormulationCoupled<Complex>{
  private:
    static void I(fullVector<double>& xyz, fullMatrix<Complex>& T);

  private:
    std::list<FormulationBlock<Complex>*> fList;

    Basis* bPhi;
    Basis* bRZ;
    Basis* bXi;

    fullMatrix<double>* gC;
    fullVector<double>* gW;
    GroupOfJacobian*    jac;

  public:
    FormulationQ3DErzEphiCoulomb(const GroupOfElement&     dom,
                                 const FunctionSpace0Form& hPhi,
                                 const FunctionSpace1Form& hRZ,
                                 const FunctionSpace0Form& hXi,
                                 int n,
                                 int order);

    virtual ~FormulationQ3DErzEphiCoulomb(void);

    virtual const std::list<FormulationBlock<Complex>*>&
                                               getFormulationBlocks(void) const;
    virtual bool isBlock(void) const;
  };
}
#endif
