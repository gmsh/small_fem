#ifndef _FORMULATIONELASTOSTIFFNESSUV_H_
#define _FORMULATIONELASTOSTIFFNESSUV_H_

#include "FormulationBlock.h"
#include "FormulationElastoStiffness.h"

/**
   @class FormulationElastoStiffnessUV
   @brief Formulation for Elastodynamic (helping formulation for stiffness UV)

   Formulation for Elastodynamic (helping formulation for stiffness UV)
 */

namespace sf{
  template<typename scalar>
  class FormulationElastoStiffnessUV: public FormulationBlock<scalar>{
  private:
    friend class FormulationElastoStiffness<scalar>;

  private:
    // Function Space & Domain //
    const FunctionSpace0Form* ffield;
    const FunctionSpace0Form* ttest;
    const GroupOfElement*     ddomain;

    // Local Terms //
    const TermGradGrad<scalar>* term;

  private:
    FormulationElastoStiffnessUV(void);
    FormulationElastoStiffnessUV(const GroupOfElement& domain,
                                 const FunctionSpace0Form& field,
                                 const FunctionSpace0Form& test,
                                 const TermGradGrad<scalar>& term);

  public:
    virtual ~FormulationElastoStiffnessUV(void);

    virtual scalar weak(size_t dofI, size_t dofJ, size_t elementId) const;
    virtual scalar rhs(size_t equationI, size_t elementId)          const;

    virtual const FunctionSpace&   field(void) const;
    virtual const FunctionSpace&    test(void) const;
    virtual const GroupOfElement& domain(void) const;

    virtual bool isBlock(void) const;
  };
}

/**
   @fn FormulationElastoStiffnessUV::~FormulationElastoStiffnessUV
   Deletes this FormulationElastoStiffnessUV
*/

#endif
