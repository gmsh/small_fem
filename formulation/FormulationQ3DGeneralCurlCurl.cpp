#include "SmallFem.h"
#include "Exception.h"

#include "FormulationQ3DGeneralCurlCurl.h"
#include "FormulationQ3DBlockCurlCurl.h"
#include "FormulationQ3DBlockRadialFCurl.h"
#include "FormulationQ3DBlockCurlRadialF.h"
#include "FormulationQ3DBlockZFZF.h"


using namespace std;
using namespace sf;

FormulationQ3DGeneralCurlCurl::FormulationQ3DGeneralCurlCurl(const GroupOfElement& domain,
                          const FunctionSpace1Form& fs,
                          double multConst, 
                          double exponent,
                          double alpha, 
                          const Basis& basis, 
                          const GroupOfJacobian& jac,
                          int G,
                          const fullMatrix<double>& gC,
                          const fullVector<double>& gW,
                          void (*f)(fullVector<double>&, fullMatrix<Complex>&)
                                            ){
      fList.push_back(
          new FormulationQ3DBlockCurlCurl (
              domain, fs, multConst, exponent + 2 * alpha, basis, jac, G, gC, gW, f));
      
      if(multConst * alpha != 0) {
      fList.push_back(
          new FormulationQ3DBlockRadialFCurl (
              domain, fs, multConst * alpha, exponent - 1 + 2 * alpha, basis, jac, G, gC, gW, f));
      
      fList.push_back(
          new FormulationQ3DBlockCurlRadialF (
              domain, fs, multConst * alpha, exponent - 1 + 2 * alpha, basis, jac, G, gC, gW, f));
      
      fList.push_back(
          new FormulationQ3DBlockZFZF (
              domain, fs, multConst * alpha * alpha, exponent - 2 + 2 * alpha, basis, jac, G, gC, gW, f));
          
    }
}

FormulationQ3DGeneralCurlCurl::~FormulationQ3DGeneralCurlCurl(void){
  list<FormulationBlock<Complex>*>::iterator end = fList.end();
  list<FormulationBlock<Complex>*>::iterator it  = fList.begin();

  for(; it !=end; it++)
    delete *it;
  
}

const list<FormulationBlock<Complex>*>&
FormulationQ3DGeneralCurlCurl::getFormulationBlocks(void) const{
  return fList;
}

bool FormulationQ3DGeneralCurlCurl::isBlock(void) const{
  return false;
}
 
