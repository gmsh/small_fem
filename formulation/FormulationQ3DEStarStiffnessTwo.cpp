#include "SmallFem.h"
#include "FormulationQ3DEStarStiffnessTwo.h"

#include "Quadrature.h"
#include "Mapper.h"

#include "TermFieldField.h"
#include "TermGradGrad.h"
#include "TermCurlCurl.h"

using namespace std;
using namespace sf;

FormulationQ3DEStarStiffnessTwo::FormulationQ3DEStarStiffnessTwo(void){
}

FormulationQ3DEStarStiffnessTwo::
FormulationQ3DEStarStiffnessTwo(const GroupOfElement& domain,
                                  const FunctionSpace0Form& fs,
                                  const TermGradGrad<Complex>& localGG
                                 ){
  // Save Data //
  this->ddomain = &domain;
  this->fs      = &fs;
  this->localGG = &localGG; 

}

FormulationQ3DEStarStiffnessTwo::~FormulationQ3DEStarStiffnessTwo(void){
}

Complex FormulationQ3DEStarStiffnessTwo::
weak(size_t dofI, size_t dofJ, size_t elementId) const{
  Complex c(1, 0); 
  return c * localGG->getTerm(dofI, dofJ, elementId);
}

Complex FormulationQ3DEStarStiffnessTwo::
rhs(size_t equationI, size_t elementId) const{
  return Complex(0, 0);
}

const FunctionSpace& FormulationQ3DEStarStiffnessTwo::field(void) const{
  return *fs;
}

const FunctionSpace& FormulationQ3DEStarStiffnessTwo::test(void) const{
  return *fs;
}

const GroupOfElement& FormulationQ3DEStarStiffnessTwo::domain(void) const{
  return *ddomain;
}

bool FormulationQ3DEStarStiffnessTwo::isBlock(void) const{
  return true;
}
