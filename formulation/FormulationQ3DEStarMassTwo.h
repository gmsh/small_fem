#ifndef _FORMULATIONQ3DESTARMASSTWO_H_
#define _FORMULATIONQ3DESTARMASSTWO_H_

#include "SmallFem.h"
#include "FunctionSpace0Form.h"
#include "GroupOfJacobian.h"

#include "FormulationBlock.h"
#include "FormulationQ3DEStarMass.h"

#include "TermFieldField.h"
#include "TermGradGrad.h"

/**
   @class FormulationQ3DEStarMassTwo
   @brief HGrad part of FormulationQ3DEStarMass

   Mass term for Quasi 3D full-wave eigenvalue Maxwell problems: HGrad part
*/

namespace sf{
  class FormulationQ3DEStarMassTwo: public FormulationBlock<Complex>{
  private:
    friend class FormulationQ3DEStarMass;

  private:
    static double C0;

  private:
    // Function Space & Domain //
    const FunctionSpace0Form* fs;
    const GroupOfElement*     ddomain;

    
     // local term // 
    const TermFieldField<Complex>* localFF;


  private:
    FormulationQ3DEStarMassTwo(void);
    FormulationQ3DEStarMassTwo(const GroupOfElement& domain,
                                 const FunctionSpace0Form& fs,
                                 const TermFieldField<Complex>& localFF
                                );

  public:
    virtual ~FormulationQ3DEStarMassTwo(void);

    virtual Complex weak(size_t dofI, size_t dofJ, size_t elementId) const;
    virtual Complex rhs(size_t equationI, size_t elementId)          const;

    virtual const FunctionSpace&   field(void) const;
    virtual const FunctionSpace&    test(void) const;
    virtual const GroupOfElement& domain(void) const;

    virtual bool isBlock(void) const;
  };
}

/**
   @fn FormulationQ3DEStarMassTwo::~FormulationQ3DEStarMassTwo
   Deletes this FormulationQ3DEStarMassTwo
*/

#endif
