#ifndef _FORMULATIONQ3DPARAMRADIALFGRADF_H_
#define _FORMULATIONQ3DPARAMRADIALFGRADF_H_

#include "SmallFem.h"
#include "FunctionSpace1Form.h"
#include "GroupOfJacobian.h"

#include "FormulationBlock.h"
#include "FormulationQ3DGeneralFGrad.h"

namespace sf{
  class FormulationQ3DBlockRadialFF: public FormulationBlock<Complex>{
  private:
    friend class FormulationQ3DGeneralGradF;
    friend class FormulationQ3DGeneralFFDunn;
    friend class FormulationQ3DGeneralCurlCurlDunn;
    friend class FormulationQ3DGeneralGradFDunn;
    
    

  private:
    // multiplicative constant and exponent 
    double multConst; 
    double exponent; 
      
    // Function Space & Domain //
    const FunctionSpace0Form* ffield;
    const FunctionSpace1Form* ttest;
    const GroupOfElement*     ddomain;

    // Gaussian Quadrature Data //
    int G;
    const fullMatrix<double>* gC;
    const fullVector<double>* gW;

    // Jacobians and basis//
    const GroupOfJacobian* jac;
    const Basis*         basisTest;   
    const Basis*         basisField;
    
    void (*f)(fullVector<double>&, fullMatrix<Complex>&); 


  private:
    FormulationQ3DBlockRadialFF(void);
    FormulationQ3DBlockRadialFF(const GroupOfElement& domain,
                          const FunctionSpace0Form& fs0,      
                          const FunctionSpace1Form& fs1,
                          double multConst, 
                          double exponent, 
                          const Basis& basisTest, 
                          const Basis& basisField,  
                          const GroupOfJacobian& jac,
                          int G,
                          const fullMatrix<double>& gC,
                          const fullVector<double>& gW,
                          void (*f)(fullVector<double>&, fullMatrix<Complex>&)
                             );

  public:
    virtual ~FormulationQ3DBlockRadialFF(void);

    virtual Complex weak(size_t dofI, size_t dofJ, size_t elementId) const;
    virtual Complex rhs(size_t equationI, size_t elementId)          const;

    virtual const FunctionSpace&   field(void) const;
    virtual const FunctionSpace&    test(void) const;
    virtual const GroupOfElement& domain(void) const;

    virtual bool isBlock(void) const;
  };
}

#endif
 
