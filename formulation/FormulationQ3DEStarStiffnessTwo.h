#ifndef _FORMULATIONQ3DESTARSTIFFNESSTWO_H_
#define _FORMULATIONQ3DESTARSTIFFNESSTWO_H_

#include "SmallFem.h"
#include "FunctionSpace0Form.h"
#include "GroupOfJacobian.h"

#include "FormulationBlock.h"
#include "FormulationQ3DEStarStiffness.h"

#include "TermFieldField.h"
#include "TermGradGrad.h"
#include "TermCurlCurl.h"

/**
   @class FormulationQ3DEStarStiffnessTwo
   @brief HGrad part of FormulationQ3DEStarStiffness

   Stiffness term for Quasi 3D full-wave eigenvalue Maxwell problems: HGrad part
*/

namespace sf{
  class FormulationQ3DEStarStiffnessTwo: public FormulationBlock<Complex>{
  private:
    friend class FormulationQ3DEStarStiffness;

  private:
    // Function Space & Domain //
    const FunctionSpace0Form* fs;
    const GroupOfElement*     ddomain;
    
     // local term // 
    const TermGradGrad<Complex>* localGG;

  private:
    FormulationQ3DEStarStiffnessTwo(void);
    FormulationQ3DEStarStiffnessTwo(const GroupOfElement& domain,
                                      const FunctionSpace0Form& fs,
                                      const TermGradGrad<Complex>& localGG
                                     );

  public:
    virtual ~FormulationQ3DEStarStiffnessTwo(void);

    virtual Complex weak(size_t dofI, size_t dofJ, size_t elementId) const;
    virtual Complex rhs(size_t equationI, size_t elementId)          const;

    virtual const FunctionSpace&   field(void) const;
    virtual const FunctionSpace&    test(void) const;
    virtual const GroupOfElement& domain(void) const;

    virtual bool isBlock(void) const;
  };
}

/**
   @fn FormulationQ3DEStarStiffnessTwo::~FormulationQ3DEStarStiffnessTwo
   Deletes this FormulationQ3DEStarStiffnessTwo
*/

#endif
