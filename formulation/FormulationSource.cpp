#include "SmallFem.h"
#include "FormulationSource.h"

#include "GroupOfJacobian.h"
#include "Quadrature.h"
#include "Exception.h"

#include "TermDummy.h"
#include "TermProjectionField.h"
#include "TermProjectionGrad.h"

using namespace std;
using namespace sf;

template<typename scalar>
FormulationSource<scalar>::
FormulationSource(const GroupOfElement& domain,
                  const FunctionSpace& fs,
                  scalar (*f)(fullVector<double>&)){
  // Save Data //
  ddomain = &domain;
  ffs     = &fs;

  // Is domain empty ? //
  if(domain.isEmpty()){
    term = new TermDummy<scalar>;
    return;
  }

  // Check domain stats: uniform mesh //
  pair<bool, size_t> uniform = domain.isUniform();
  size_t               eType = uniform.second;

  if(!uniform.first)
    throw Exception("FormulationSource needs a uniform mesh");

  // Get Basis //
  const size_t form  = fs.getForm();
  const Basis& basis = fs.getBasis(eType);
  const size_t order = basis.getOrder();

  // Check differential form
  if(form != 0)
    throw Exception("FormulationSource with a scalar function needs a 0From");

  // Gaussian Quadrature //
  Quadrature gauss(eType, order, 2);
  const fullMatrix<double>& gC = gauss.getPoints();

  // Functions //
  basis.preEvaluateFunctions(gC);

  // Local Terms //
  GroupOfJacobian jac(domain, gC, "jacobian");
  term  = new TermProjectionField<scalar>(jac, basis, gauss, f);
}

template<typename scalar>
FormulationSource<scalar>::
FormulationSource(const GroupOfElement& domain,
                  const FunctionSpace& fs,
                  scalar (*f)(const MElement& element,
                              fullVector<double>&)){
  // Save Data //
  ddomain = &domain;
  ffs     = &fs;

  // Is domain empty ? //
  if(domain.isEmpty()){
    term = new TermDummy<scalar>;
    return;
  }

  // Check domain stats: uniform mesh //
  pair<bool, size_t> uniform = domain.isUniform();
  size_t               eType = uniform.second;

  if(!uniform.first)
    throw Exception("FormulationSource needs a uniform mesh");

  // Get Basis //
  const size_t form  = fs.getForm();
  const Basis& basis = fs.getBasis(eType);
  const size_t order = basis.getOrder();

  // Check differential form
  if(form != 0)
    throw Exception("FormulationSource with a scalar function needs a 0From");

  // Gaussian Quadrature //
  Quadrature gauss(eType, order, 2);
  const fullMatrix<double>& gC = gauss.getPoints();

  // Functions //
  basis.preEvaluateFunctions(gC);

  // Local Terms //
  GroupOfJacobian jac(domain, gC, "jacobian");
  term  = new TermProjectionField<scalar>(jac, basis, gauss, f);
}

template<typename scalar>
FormulationSource<scalar>::
FormulationSource(const GroupOfElement& domain,
                  const FunctionSpace& fs,
                  fullVector<scalar> (*f)(fullVector<double>&),
                  int iOrder){
  // Save Data //
  ddomain = &domain;
  ffs     = &fs;

  // Is domain empty ? //
  if(domain.isEmpty()){
    term = new TermDummy<scalar>;
    return;
  }

  // Check domain stats: uniform mesh //
  pair<bool, size_t> uniform = domain.isUniform();
  size_t               eType = uniform.second;

  if(!uniform.first)
    throw Exception("FormulationSource needs a uniform mesh");

  // Get Basis //
  const size_t form  = fs.getForm();
  const Basis& basis = fs.getBasis(eType);
  const size_t order = basis.getOrder();

  // Check differential form
  if(form != 1)
    throw Exception("FormulationSource with a scalar function needs a 1From");

  // Gaussian Quadrature //
  int iM;
  int iO;
  if(iOrder == -1){
    iM = 2;
    iO = order;
  }
  else{
    iM = 1;
    iO = iOrder;
  }

  Quadrature gauss(eType, iO, iM);
  const fullMatrix<double>& gC = gauss.getPoints();

  // Functions //
  basis.preEvaluateFunctions(gC);

  // Local Terms //
  GroupOfJacobian jac(domain, gC, "invert");
  term  = new TermProjectionGrad<scalar>(jac, basis, gauss, f);
}

template<typename scalar>
FormulationSource<scalar>::~FormulationSource(void){
  delete term;
}

template<typename scalar>
scalar FormulationSource<scalar>::
weak(size_t dofI, size_t dofJ, size_t elementId) const{
  return 0;
}

template<typename scalar>
scalar FormulationSource<scalar>::
rhs(size_t equationI, size_t elementId) const{
  return term->getTerm(equationI, 0, elementId);
}

template<typename scalar>
const FunctionSpace& FormulationSource<scalar>::field(void) const{
  return *ffs;
}

template<typename scalar>
const FunctionSpace& FormulationSource<scalar>::test(void) const{
  return *ffs;
}

template<typename scalar>
const GroupOfElement& FormulationSource<scalar>::domain(void) const{
  return *ddomain;
}

template<typename scalar>
bool FormulationSource<scalar>::isBlock(void) const{
  return true;
}

////////////////////////////
// Explicit instantiation //
////////////////////////////
template class sf::FormulationSource<Complex>;
template class sf::FormulationSource<double>;
