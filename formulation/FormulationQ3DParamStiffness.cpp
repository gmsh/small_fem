#include "SmallFem.h"
#include "Exception.h"
#include "FormulationQ3DParamStiffness.h"

#include "FormulationQ3DGeneralCurlCurl.h"
#include "FormulationQ3DGeneralGradGrad.h"
#include "FormulationQ3DGeneralFF1Form.h"

using namespace std;
using namespace sf;

FormulationQ3DParamStiffness::FormulationQ3DParamStiffness(const GroupOfElement& domain,
                          const FunctionSpace0Form& hGrad,
                          const FunctionSpace1Form& hCurl,
                          double alpha,
                          double beta,
                          int n,
                          void (*f)(fullVector<double>&, fullMatrix<Complex>&),
                          int order
                                            ){
    
      // Check domain stats: uniform mesh //
      pair<bool, size_t> uniform = domain.isUniform();
      size_t               eType = uniform.second;

      if(!uniform.first)
        throw Exception("FormulationQ3DParamStiffness needs a uniform mesh");
      
       this->basisGrad = hGrad.getBasis(eType).copy(); 
       this->basisCurl = hCurl.getBasis(eType).copy(); 
      
      Quadrature gauss(eType,order, 1);
      
      this->gC = new fullMatrix<double>(gauss.getPoints());
      this->gW = new fullVector<double>(gauss.getWeights());
      int G = gW->size(); 
      
      basisGrad->preEvaluateDerivatives(*gC);
      basisGrad->preEvaluateFunctions(*gC);
      
      basisCurl->preEvaluateDerivatives(*gC);
      basisCurl->preEvaluateFunctions(*gC);
      
      this->jac = new GroupOfJacobian(domain, *gC, "both");
      
      switch(n) {
          case 0: 
              
              gForm.push_back(new FormulationQ3DGeneralCurlCurl(domain, hCurl, 1.0, 
                                            1.0, 0, *basisCurl, *jac, G, *gC, *gW, f));
              
              gForm.push_back(new FormulationQ3DGeneralGradGrad(domain, hGrad, 1.0, -1.0, beta, 
                                                           *basisGrad, *jac, G, *gC, *gW, f));
              
              break; 
          // n != 0
          default: 
              gForm.push_back(new FormulationQ3DGeneralCurlCurl(domain, hCurl, 1.0, 1.0, alpha,
                                                                *basisCurl, *jac, G, *gC, *gW, f));
              gForm.push_back(new FormulationQ3DGeneralFF1Form(domain, hCurl, n*n, 2.0 * alpha - 1.0,
                                                                *basisCurl, *jac, G, *gC, *gW, f));
              break; 
      }
      list<FormulationCoupled<Complex>*>::iterator end = gForm.end();
      list<FormulationCoupled<Complex>*>::iterator it  = gForm.begin();
      for(; it !=end; it++) {
         list<FormulationBlock<Complex>*> tmp; 
         tmp = (*it)->getFormulationBlocks(); 
         list<FormulationBlock<Complex>*>::iterator endTmp = tmp.end();
         list<FormulationBlock<Complex>*>::iterator itTmp  = tmp.begin();
         
         for(; itTmp !=endTmp; itTmp++) {
             fList.push_back(*itTmp); 
        }
      }
}

FormulationQ3DParamStiffness::~FormulationQ3DParamStiffness(void){
  list<FormulationBlock<Complex>*>::iterator end = fList.end();
  list<FormulationBlock<Complex>*>::iterator it  = fList.begin();

  for(; it !=end; it++)
    delete *it;

  delete jac;
  delete basisCurl;
  delete basisGrad;
  delete gW;
  delete gC;
}

const list<FormulationBlock<Complex>*>&
FormulationQ3DParamStiffness::getFormulationBlocks(void) const{
  return fList;
}

bool FormulationQ3DParamStiffness::isBlock(void) const{
  return false;
}
