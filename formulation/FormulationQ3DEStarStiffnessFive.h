#ifndef _FORMULATIONQ3DESTARSTIFFNESSFIVE_H_
#define _FORMULATIONQ3DESTARSTIFFNESSFIVE_H_

#include "SmallFem.h"
#include "FunctionSpace0Form.h"
#include "FunctionSpace1Form.h"
#include "GroupOfJacobian.h"

#include "FormulationBlock.h"
#include "FormulationQ3DEStarStiffness.h"

#include "TermFieldField.h"
#include "TermGradGrad.h"
#include "TermCurlCurl.h"

/**
   @class FormulationQ3DEStarStiffnessFive
   @brief HGrad part of FormulationQ3DEStarStiffness

   Stiffness term for Quasi 3D full-wave eigenvalue Maxwell problems: HGrad part
*/

namespace sf{
  class FormulationQ3DEStarStiffnessFive: public FormulationBlock<Complex>{
  private:
    friend class FormulationQ3DEStarStiffness;

  private:
    // Function Space & Domain //
    const FunctionSpace0Form* ttest; 
    const FunctionSpace1Form* ffield;
    const GroupOfElement*     ddomain;
    
    // parameters
    int n; 
    
    // local term // 
    const TermGradGrad<Complex>* localInG; 

  private:
    FormulationQ3DEStarStiffnessFive(void);
    FormulationQ3DEStarStiffnessFive(const GroupOfElement& domain,
                                      const FunctionSpace0Form& test,
                                      const FunctionSpace1Form& field,
                                      int n, 
                                      const TermGradGrad<Complex>& localInG
                                       );

  public:
    virtual ~FormulationQ3DEStarStiffnessFive(void);

    virtual Complex weak(size_t dofI, size_t dofJ, size_t elementId) const;
    virtual Complex rhs(size_t equationI, size_t elementId)          const;

    virtual const FunctionSpace&   field(void) const;
    virtual const FunctionSpace&    test(void) const;
    virtual const GroupOfElement& domain(void) const;

    virtual bool isBlock(void) const;
  };
}

/**
   @fn FormulationQ3DEStarStiffnessFive::~FormulationQ3DEStarStiffnessFive
   Deletes this FormulationQ3DEStarStiffnessFive
*/

#endif
