#ifndef _FORMULATIONQ3DESTARCOULOMBTHREE_H_
#define _FORMULATIONQ3DESTARCOULOMBTHREE_H_

#include "SmallFem.h"
#include "FunctionSpace0Form.h"
#include "FunctionSpace1Form.h"
#include "GroupOfJacobian.h"

#include "FormulationBlock.h"
#include "FormulationQ3DEStarCoulomb.h"

#include "TermFieldField.h"
#include "TermGradGrad.h"

/**
   @class FormulationQ3DEStarCoulombThree
   @brief Term in (aRZ_i, d xi_j) of the Coulomb gauge for the ephiStar ansatz

   Term in (aRZ_i, d xi_j) of the Coulomb gauge for the ephiStar ansatz
*/

namespace sf{
  class FormulationQ3DEStarCoulombThree: public FormulationBlock<Complex>{
  private:
    friend class FormulationQ3DEStarCoulomb;

  private:
    // Function Space & Domain //
    const FunctionSpace1Form* ffield;
    const FunctionSpace0Form* ttest;
    const GroupOfElement*     ddomain;


    // local term //
    const TermGradGrad<Complex>* lXRZ;


  private:
    FormulationQ3DEStarCoulombThree(void);
    FormulationQ3DEStarCoulombThree(const GroupOfElement& domain,
                                    const FunctionSpace1Form& field,
                                    const FunctionSpace0Form& test,
                                    const TermGradGrad<Complex>& lXRZ);

  public:
    virtual ~FormulationQ3DEStarCoulombThree(void);

    virtual Complex weak(size_t dofI, size_t dofJ, size_t elementId) const;
    virtual Complex rhs(size_t equationI, size_t elementId)          const;

    virtual const FunctionSpace&   field(void) const;
    virtual const FunctionSpace&    test(void) const;
    virtual const GroupOfElement& domain(void) const;

    virtual bool isBlock(void) const;
  };
}

/**
   @fn FormulationQ3DEStarCoulombThree::~FormulationQ3DEStarCoulombThree
   Deletes this FormulationQ3DEStarCoulombThree
*/

#endif
