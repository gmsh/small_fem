#include "SmallFem.h"
#include "FormulationQ3DBlockRadialFGrad.h"

#include "Quadrature.h"
#include "Mapper.h"

using namespace std;
using namespace sf;

FormulationQ3DBlockRadialFGrad::FormulationQ3DBlockRadialFGrad(void){
}

FormulationQ3DBlockRadialFGrad::
FormulationQ3DBlockRadialFGrad(const GroupOfElement& domain,
                               const FunctionSpace0Form& fs,
                               double multConst,
                               double exponent,
                               const Basis& basis,
                               const GroupOfJacobian& jac,
                               int G,
                               const fullMatrix<double>& gC,
                               const fullVector<double>& gW,
                               void (*f)(fullVector<double>&,
                                         fullMatrix<Complex>&)){
  // Save Data //
  this->ddomain   = &domain;
  this->ttest     = &fs;
  this->ffield    = &fs;
  this->multConst = multConst;
  this->exponent  = exponent;

  // Get Basis //
  this->basisTest  = &basis;
  this->basisField = &basis;
  this->jac        = &jac;
  this->gC         = &gC;
  this->gW         = &gW;
  this->G          = G;

  this->f          = f;
}

FormulationQ3DBlockRadialFGrad::
FormulationQ3DBlockRadialFGrad(const GroupOfElement& domain,
                               const FunctionSpace0Form& fRadial,
                               const FunctionSpace0Form& fGrad,
                               double multConst,
                               double exponent,
                               const Basis& basisRadial,
                               const Basis& basisGrad,
                               const GroupOfJacobian& jac,
                               int G,
                               const fullMatrix<double>& gC,
                               const fullVector<double>& gW,
                               void (*f)(fullVector<double>&,
                                         fullMatrix<Complex>&)){
  // Save Data //
  this->ddomain   = &domain;
  this->ttest     = &fRadial;
  this->ffield    = &fGrad;
  this->multConst = multConst;
  this->exponent  = exponent;

  // Get Basis //
  this->basisTest  = &basisRadial;
  this->basisField = &basisGrad;
  this->jac        = &jac;
  this->gC         = &gC;
  this->gW         = &gW;
  this->G          = G;

  this->f          = f;
}

FormulationQ3DBlockRadialFGrad::~FormulationQ3DBlockRadialFGrad(void){

}

Complex FormulationQ3DBlockRadialFGrad::
weak(size_t dofI, size_t dofJ, size_t elementId) const{
  // Init Some Stuff //
  fullMatrix<double> mat;
  fullVector<double> phiI(3);
  fullVector<double> phiJ(3);

  Complex integral = Complex(0, 0);
  double  det;
  double  pxyz[3];

  fullVector<double>  xyz(3);
  fullMatrix<Complex> TMatrix(3,3);
  Complex intMatPhiIPhiJ;

  // Get Element //
  const MElement& element = ddomain->get(elementId);

  // Get Basis Functions //
  const fullMatrix<double>& eFun =
    basisTest->getPreEvaluatedFunctions(element);

  const fullMatrix<double>& eGradFun =
    basisField->getPreEvaluatedDerivatives(element);

  // Get Jacobians //
  const vector<pair<fullMatrix<double>, double> >& invJac =
    jac->getInvertJacobianMatrix(elementId);


  // Loop over Integration Point //
  for(int g = 0; g < G; g++){
    det = invJac[g].second; // det(Jac) at integration point g
    mat = invJac[g].first;  // J        at integration point g
    ReferenceSpaceManager::
      mapFromABCtoXYZ(element, (*gC)(g, 0), (*gC)(g, 1), (*gC)(g, 2), pxyz);
                            // xyz coordinates of point g

    Mapper::hCurl(eGradFun, dofJ, g, mat, phiJ); // grad(e_J)|g in xyz-space
    phiI(0) = eFun(dofI, g); // e_I|g in xyz-space
    phiI(1) = 0;
    phiI(2) = 0;

    xyz(0) = pxyz[0];
     xyz(1) = pxyz[1];
     xyz(2) = pxyz[2];

    f(xyz, TMatrix);
    // WARNING! Only diagonal matrices possible!
    intMatPhiIPhiJ = TMatrix(0,0) * phiI(0) * phiJ(0)
                        + TMatrix(1,1) * phiI(1) * phiJ(1)
                        + TMatrix(2,2) * phiI(2) * phiJ(2);

    integral +=
      intMatPhiIPhiJ * fabs(det) * (*gW)(g) * pow(pxyz[0],exponent);
  }

  // Done
  return multConst * integral;
}

Complex FormulationQ3DBlockRadialFGrad::
rhs(size_t equationI, size_t elementId) const{
  return Complex(0, 0);
}

const FunctionSpace& FormulationQ3DBlockRadialFGrad::field(void) const{
  return *ffield;
}

const FunctionSpace& FormulationQ3DBlockRadialFGrad::test(void) const{
  return *ttest;
}

const GroupOfElement& FormulationQ3DBlockRadialFGrad::domain(void) const{
  return *ddomain;
}

bool FormulationQ3DBlockRadialFGrad::isBlock(void) const{
  return true;
}
