#include "FormulationBeynWave.h"
#include "GroupOfJacobian.h"
#include "Quadrature.h"
#include "Exception.h"

#include "TermFieldField.h"
#include "TermGradGrad.h"
#include "TermCurlCurl.h"
#include "TermDummy.h"

using namespace sf;

FormulationBeynWave::FormulationBeynWave(const GroupOfElement& domain,
                                         const FunctionSpace1Form& fs,
                                         BeynContext& context,
                                         double mu,
                                         double eps){
  // Save Data //
  ddomain   = &domain;
  ffs       = &fs;
  ctx       = &context;
  this->mu  = mu;
  this->eps = eps;

  // Squared angular frequency //
  omegaSq = ctx->getLambda() * ctx->getLambda();

  // Is domain empty ? //
  if(domain.isEmpty()){
    mass = new TermDummy<double>;
    stif = new TermDummy<double>;
    return;
  }

  // Check domain stats: uniform mesh //
  std::pair<bool, size_t> uniform = domain.isUniform();
  size_t                    eType = uniform.second;

  if(!uniform.first)
    throw Exception("FormulationBeynWave needs a uniform mesh");

  // Get Basis //
  const Basis& basis = fs.getBasis(eType);
  const size_t order = basis.getOrder();

  // Gaussian Quadrature //
  Quadrature gaussStif(eType, order - 1, 2);
  Quadrature gaussMass(eType, order,     2);

  const fullMatrix<double>& gCS = gaussStif.getPoints();
  const fullMatrix<double>& gCM = gaussMass.getPoints();

  // Functions //
  basis.preEvaluateDerivatives(gCS);
  basis.preEvaluateFunctions(gCM);

  // Local Terms //
  GroupOfJacobian jacM(domain, gCM, "invert");
  GroupOfJacobian jacS(domain, gCS, "jacobian");

  mass = new TermGradGrad<double>(jacM, basis, gaussMass);
  stif = new TermCurlCurl<double>(jacS, basis, gaussStif);
}

FormulationBeynWave::~FormulationBeynWave(void){
  delete stif;
  delete mass;
}

Complex FormulationBeynWave::
weak(size_t dofI, size_t dofJ, size_t elementId) const{
  return
    stif->getTerm(dofI, dofJ, elementId) / eps -
    mass->getTerm(dofI, dofJ, elementId) * omegaSq * mu;
}

Complex FormulationBeynWave::
rhs(size_t equationI, size_t elementId) const{
  return Complex(0, 0);
}

const FunctionSpace& FormulationBeynWave::field(void) const{
  return *ffs;
}

const FunctionSpace& FormulationBeynWave::test(void) const{
  return *ffs;
}

const GroupOfElement& FormulationBeynWave::domain(void) const{
  return *ddomain;
}

bool FormulationBeynWave::isBlock(void) const{
  return true;
}

void FormulationBeynWave::update(void){
  omegaSq = ctx->getLambda() * ctx->getLambda();
}
