#ifndef _FORMULATIONQ3DESTARCOULOMBFOUR_H_
#define _FORMULATIONQ3DESTARCOULOMBFOUR_H_

#include "SmallFem.h"
#include "FunctionSpace0Form.h"
#include "FunctionSpace1Form.h"
#include "GroupOfJacobian.h"

#include "FormulationBlock.h"
#include "FormulationQ3DEStarCoulomb.h"

#include "TermFieldField.h"
#include "TermGradGrad.h"

/**
   @class FormulationQ3DEStarCoulombFour
   @brief Term in (aPhi_i, xi_j) of the Coulomb gauge for the ephiStar ansatz

   Term in (aPhi_i, xi_j) of the Coulomb gauge for the ephiStar ansatz
*/

namespace sf{
  class FormulationQ3DEStarCoulombFour: public FormulationBlock<Complex>{
  private:
    friend class FormulationQ3DEStarCoulomb;

  private:
    // Function Space & Domain //
    const FunctionSpace0Form* ffield;
    const FunctionSpace0Form* ttest;
    const GroupOfElement*     ddomain;


    // local term //
    const TermFieldField<Complex>* lXPhi;

    // N //
    Complex minusN;


  private:
    FormulationQ3DEStarCoulombFour(void);
    FormulationQ3DEStarCoulombFour(const GroupOfElement& domain,
                                   const FunctionSpace0Form& field,
                                   const FunctionSpace0Form& test,
                                   const TermFieldField<Complex>& lXPhi,
                                   int N);

  public:
    virtual ~FormulationQ3DEStarCoulombFour(void);

    virtual Complex weak(size_t dofI, size_t dofJ, size_t elementId) const;
    virtual Complex rhs(size_t equationI, size_t elementId)          const;

    virtual const FunctionSpace&   field(void) const;
    virtual const FunctionSpace&    test(void) const;
    virtual const GroupOfElement& domain(void) const;

    virtual bool isBlock(void) const;
  };
}

/**
   @fn FormulationQ3DEStarCoulombFour::~FormulationQ3DEStarCoulombFour
   Deletes this FormulationQ3DEStarCoulombFour
*/

#endif
