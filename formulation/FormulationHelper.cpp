#include "SmallFem.h"
#include <set>
#include "Exception.h"
#include "FormulationHelper.h"

using namespace std;
using namespace sf;

template<typename scalar>
FormulationHelper<scalar>::FormulationHelper(void){
}

template<typename scalar>
FormulationHelper<scalar>::~FormulationHelper(void){
}

template<typename scalar>
void FormulationHelper<scalar>::
initDofMap(const FunctionSpace& fs,
           const GroupOfElement& goe,
           map<Dof, scalar>& data){

  // Get Keys from goe //
  set<Dof> dSet;
  fs.getKeys(goe, dSet);

  // Insert Dofs in data //
  set<Dof>::iterator it  = dSet.begin();
  set<Dof>::iterator end = dSet.end();

  for(; it != end; it++)
    data.insert(pair<Dof, scalar>(*it, 0));
}

template<typename scalar>
void FormulationHelper<scalar>::
initDofMap(const vector<const FunctionSpace*>& fs,
           const GroupOfElement& goe,
           vector<map<Dof, scalar> >& data){

  // Check size //
  const size_t size = data.size();
  if(size != fs.size())
    throw Exception("FormulationHelper::initDofMap: %s",
                    "vectors must have the same size");

  // Populate //
  for(size_t i = 0; i < size; i++)
    initDofMap(*fs[i], goe, data[i]);
}

////////////////////////////
// Explicit instantiation //
////////////////////////////
template class sf::FormulationHelper<Complex>;
template class sf::FormulationHelper<double>;
