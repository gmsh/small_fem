#include "SmallFem.h"
#include "Exception.h"

#include "FormulationQ3DGeneralGradF.h"
#include "FormulationQ3DBlockGradF.h"
#include "FormulationQ3DBlockRadialFF.h"


using namespace std;
using namespace sf;

FormulationQ3DGeneralGradF::FormulationQ3DGeneralGradF(const GroupOfElement& domain,
                          const FunctionSpace0Form& fsGrad,      
                          const FunctionSpace1Form& fsCurl,
                          double multConst, 
                          double alpha, 
                          double beta, 
                          const Basis& basisTest, 
                          const Basis& basisField, 
                          const GroupOfJacobian& jac,
                          int G,
                          const fullMatrix<double>& gC,
                          const fullVector<double>& gW,
                          void (*f)(fullVector<double>&, fullMatrix<Complex>&)
                                            ){

      fList.push_back(
          new FormulationQ3DBlockGradF (
              domain, fsGrad, fsCurl, multConst, alpha + beta + 1, basisTest, basisField, jac, G, gC, gW, f));
      if(multConst*beta != 0) {
          fList.push_back(
              new FormulationQ3DBlockRadialFF (
                  domain, fsGrad, fsCurl, multConst*beta, alpha + beta, basisTest, basisField, jac, G, gC, gW, f)    
                         );
      }
}

FormulationQ3DGeneralGradF::~FormulationQ3DGeneralGradF(void){
  list<FormulationBlock<Complex>*>::iterator end = fList.end();
  list<FormulationBlock<Complex>*>::iterator it  = fList.begin();

  for(; it !=end; it++)
    delete *it;
  
}

const list<FormulationBlock<Complex>*>&
FormulationQ3DGeneralGradF::getFormulationBlocks(void) const{
  return fList;
}

bool FormulationQ3DGeneralGradF::isBlock(void) const{
  return false;
}
 
