#ifndef _FORMULATIONCOULOMBONE_H_
#define _FORMULATIONCOULOMBONE_H_

#include "SmallFem.h"
#include "FunctionSpace0Form.h"
#include "FunctionSpace1Form.h"
#include "TermGradGrad.h"
#include "FormulationBlock.h"

#include "FormulationCoulomb.h"

/**
   @class FormulationCoulombOne
   @brief Helping class for FormulationCoulomb (gradient field is unknwon)

   Helping class for FormulationCoulomb (gradient field is unknwon)

   FormulationCoulombOne is a friend of FormulationCoulomb
*/

namespace sf{
  class FormulationCoulombOne: public FormulationBlock<double>{
  private:
    friend class FormulationCoulomb;

  private:
    // Function Space & Domain //
    const FunctionSpace0Form* ffield;
    const FunctionSpace1Form* ttest;
    const GroupOfElement*     ddomain;

    // Local Terms //
    const TermGradGrad<double>* term;

  private:
    FormulationCoulombOne(void);
    FormulationCoulombOne(const GroupOfElement& domain,
                          const FunctionSpace0Form& field,
                          const FunctionSpace1Form& test,
                          const TermGradGrad<double>& term);

  public:
    virtual ~FormulationCoulombOne(void);

    virtual double weak(size_t dofI, size_t dofJ, size_t elementId) const;
    virtual double rhs(size_t equationI, size_t elementId)          const;

    virtual const FunctionSpace&   field(void) const;
    virtual const FunctionSpace&    test(void) const;
    virtual const GroupOfElement& domain(void) const;

    virtual bool isBlock(void) const;
  };
}

/**
   @fn FormulationCoulombOne::~FormulationCoulombOne
   Deletes this FormulationCoulombOne
*/

#endif
