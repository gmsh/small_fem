#ifndef _FORMULATIONQ3DGENERALGRADGRAD_H_
#define _FORMULATIONQ3DGENERALGRADGRAD_H_

// include needed header files
#include "SmallFem.h"
#include "FormulationCoupled.h"
#include "FunctionSpace0Form.h"
#include "FunctionSpace1Form.h"
#include "GroupOfJacobian.h"
#include "Quadrature.h"

/**
   @class FormulationQ3DGeneralGradGrad
   @brief General Term for gradient-gradient bilinear forms 
   

   Calculates multConst * (r^exponent grad (r^beta u), grad (r^beta v)) = 
       multConst * (r^(2 * beta + exponent) grad u, grad v) + 
       multConst * beta * (r^(2 * beta + exponent - 1) grad u, [v ; 0]) +
       multConst * beta * (r^(2 * beta + exponent - 1) [u ; 0], grad v) + 
       multConst * beta^2 * (r^(2 * beta + exponent - 2) u, v)
       
   @author Erik Schnaubelt
*/


namespace sf{
  class FormulationQ3DGeneralGradGrad: public FormulationCoupled<Complex>{
  private:
    friend class FormulationQ3DBlockStiffness;
    friend class FormulationQ3DBlockMass;
  
  private:
    std::list<FormulationBlock<Complex>*> fList;

  public:
    FormulationQ3DGeneralGradGrad(const GroupOfElement& domain,
                          const FunctionSpace0Form& fs,
                          double multConst, 
                          double exponent,
                          double beta, 
                          const Basis& basis, 
                          const GroupOfJacobian& jac,
                          int G,
                          const fullMatrix<double>& gC,
                          const fullVector<double>& gW,
                          void (*f)(fullVector<double>&, fullMatrix<Complex>&)
                         );

    virtual ~FormulationQ3DGeneralGradGrad(void);

    virtual const std::list<FormulationBlock<Complex>*>&
                                               getFormulationBlocks(void) const;
    virtual bool isBlock(void) const;
  };
}

#endif
 
