#include "BeynContext.h"

using namespace std;
using namespace sf;

BeynContext::BeynContext(Complex lambda){
  this->lambda = lambda;
  this->dof    = new set<Dof>;
}

BeynContext::BeynContext(void){
  this->lambda = Complex(0, 0);
  this->dof    = new set<Dof>;
}

BeynContext::~BeynContext(void){
  delete this->dof;
}

Complex BeynContext::getLambda(void) const{
  return lambda;
}

void BeynContext::getDof(set<Dof>& dof) const{
  dof.clear();         // Clear
  dof = *(this->dof);  // And copy
}

void BeynContext::setLambda(Complex lambda){
  this->lambda = lambda;
}

void BeynContext::setDof(const set<Dof>& dof){
  this->dof->clear(); // Clear
  *(this->dof) = dof; // And copy
}
