#ifndef _BEYNCONTEXT_H_
#define _BEYNCONTEXT_H_

#include "SmallFem.h"
#include "Dof.h"
#include <set>

/**
   @class BeynContext
   @brief Context for Beyn's eigenvalue algorithm

   This class is the context for Beyn's eigenvalue algorithm.
   It remembers the current eigenparameter considered by the Beyn's method.
 */

namespace sf{
  class BeynContext{
  private:
    Complex        lambda;
    std::set<Dof>* dof;

  public:
     BeynContext(Complex lambda);
     BeynContext(void);
    ~BeynContext(void);

    Complex getLambda(void)            const;
    void    getDof(std::set<Dof>& dof) const;

    void    setLambda(Complex lambda);
    void    setDof(const std::set<Dof>& dof);
  };
}

/**
   @fn BeynContext::BeynContext(Complex)
   @param lambda A complex number
   Instantiates a new BeynContext with lambda as the current eigenparameter
   **

   @fn BeynContext::BeynContext(void)
   Instantiates a new BeynContext with the current eigenparameter set to zero
   **

   @fn BeynContext::~BeynContext
   Deletes this BeynContext
   **

   @fn BeynContext::getLambda
   @return Returns the current eigenparameter
   **

   @fn BeynContext::getDof
   @param dof A set of Dofs
   Populates the given map with the Dofs of this BeynContext
   **

   @fn BeynContext::setLambda
   @param lambda A complex number
   Sets the given value as the new current eigenparameter
   **

   @fn BeynContext::setDof
   @param dof A set of Dofs
   Sets the given set of Dofs as this BeynContext Dofs
*/
#endif
