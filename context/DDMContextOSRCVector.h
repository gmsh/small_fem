#ifndef _DDMCONTEXTOSRCVECTOR_H_
#define _DDMCONTEXTOSRCVECTOR_H_

#include "DDMContext.h"

/**
   @class DDMContextOSRCVector
   @brief Context for DDM vectorial OSRC

   This class is a DDMContext for vectorial OSRC.
   In addition to a DDMContext, this class alows the definition
   of the vectorial OSRC wavenumber and its chi value.
 */

namespace sf{
  class DDMContextOSRCVector: public DDMContext{
  private:
    const std::vector<const FunctionSpace*>* phi;
    const std::vector<const FunctionSpace*>* rho;
    const FunctionSpace*                     r;

    int     NPade;
    double  theta;
    Complex k;
    Complex keps;

  public:
    DDMContextOSRCVector(const GroupOfElement& domain,
                         std::vector<const GroupOfElement*>& dirichlet,
                         const FunctionSpace& fSpace,
                         const FunctionSpace& fSpaceG,
                         const std::vector<const FunctionSpace*>& phi,
                         const std::vector<const FunctionSpace*>& rho,
                         const FunctionSpace& r,
                         Complex k, Complex keps,
                         int NPade, double theta);

    virtual ~DDMContextOSRCVector(void);

    int     getNPade(void) const;
    double  getRotation(void) const;
    Complex getWavenumber(void) const;
    Complex getComplexWavenumber(void) const;

    const std::vector<const FunctionSpace*>&
      getPhiFunctionSpace(void) const;

    const std::vector<const FunctionSpace*>&
      getRhoFunctionSpace(void) const;

    const FunctionSpace& getRFunctionSpace(void) const;
  };
}

/**
   @fn DDMContextOSRCVector::DDMContextOSRCVector
   @param domain The DDM border for the vectorial OSRC problem
   @param dirichlet The Dirichlet border for the vectorial OSRC problem
   @param fSpace The FunctionSpace for the vectorial OSRC field
   @param fSpaceG The FunctionSpace for the vectorial OSRC DDM
   @param phi A vector with the auxiliary FunctionSpace for vector OSRC problem
   @param k The wavenumber of the vectorial OSRC problem
   @param keps The complexified wavenumber of the vectorial OSRC problem
   @param NPade The number of Pade term tu use
   @param theta The complex rotation tu use

   Instanciates a new DDMContextOSRCVector with the given parameters
   **

   @fn DDMContextOSRCVector::~DDMContext
   Deletes this DDMContextOSRCVector
   **

   @fn DDMContextOSRCVector::getNPade
   @return Returns the number of Pade term of this DDMContextOSRCVector
   **

   @fn DDMContextOSRCVector::getRotation
   @return Returns the complex rotation of this DDMContextOSRCVector
   **

   @fn DDMContextOSRCVector::getWavenumer
   @return Returns the wavenumber of this DDMContextOSRCVector
   **

   @fn DDMContextOSRCVector::getComplexWavenumer
   @return Returns the complexified wavenumber of this DDMContextOSRCVector
   **

   @fn DDMContextOSRCVector::getPhiFunctionSpace
   @return Returns the vector of auxiliary FunctionSpace
   of this DDMContextOSRCVector
   **

   @fn DDMContextOSRCVector::getRhoFunctionSpace
   @return Returns the vector of auxiliary FunctionSpace
   of this DDMContextOSRCVector
   **

   @fn DDMContextOSRCVector::getRFunctionSpace
   @return Returns the auxiliary FunctionSpace of this DDMContextOSRCVector
 */

//////////////////////
// Inline Functions //
//////////////////////

inline int sf::DDMContextOSRCVector::getNPade(void) const{
  return NPade;
}

inline double sf::DDMContextOSRCVector::getRotation(void) const{
  return theta;
}

inline sf::Complex sf::DDMContextOSRCVector::getWavenumber(void) const{
  return k;
}

inline sf::Complex sf::DDMContextOSRCVector::getComplexWavenumber(void) const{
  return keps;
}

inline const std::vector<const sf::FunctionSpace*>&
sf::DDMContextOSRCVector::getPhiFunctionSpace(void) const{
  return *phi;
}

inline const std::vector<const sf::FunctionSpace*>&
sf::DDMContextOSRCVector::getRhoFunctionSpace(void) const{
  return *rho;
}

inline const sf::FunctionSpace&
sf::DDMContextOSRCVector::getRFunctionSpace(void) const{
  return *r;
}

#endif
