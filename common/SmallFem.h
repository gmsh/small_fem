#ifndef _SMALLFEM_H_
#define _SMALLFEM_H_

#include <complex>
#include <string>
#include <mpi.h>
#include "gmsh/onelab.h"
#include "Options.h"

/**
   @class SmallFem
   @brief Initialize and finalize SmallFem sessions.

   Initialize and finalize SmallFem sessions.
   It also handels this SmallFem session Options and other useful stuffs.

   A SmallFem session has the following default Options keywords
   @li -solver
   @li -onelab
*/

/**
   @typedef Complex
   @brief A short cut for std::complex<double>

   A short cut for std::complex<double>
 */

namespace sf{
  typedef std::complex<double> Complex;

  class SmallFem{
  private:
    static bool        initOne;
    static bool        finaOne;
    static Options*    option;
    static std::string myKeywords;

  public:
     SmallFem(void);
    ~SmallFem(void);

    static void Keywords(const std::string& keywords);
    static void Initialize(int argc, char** argv);
    static void Finalize(void);

    static bool     isMaster(void);
    static Options& getOptions(void);
    static double   getVMPeak(void);

  private:
    static void initOnelab(void);
    static void initPetsc(char** argv);

  public:
    class OnelabHelper{
    friend class SmallFem;

    private:
      static onelab::remoteNetworkClient* olClient;
      static std::string                  olName;
      static std::string                  olAddress;

    public:
      static onelab::remoteNetworkClient& client(void);
      static std::string                  name(void);
      static std::string                  address(void);
      static std::string                  action(void);

      static double   defineNumber(std::string name, double value);
      static double      getNumber(std::string name);
      static std::string getString(std::string name);
    };
  };
}

/**
   @fn SmallFem::SmallFem
   Instantiates a new SmallFem

   Not needed, since SmallFem is bunch of static class
   **

   @fn SmallFem::~SmallFem
   Deletes this SmallFem

   Not needed, since SmallFem is bunch of static class
   **

   @fn SmallFem::Keywords
   @param keywords A string with comma separated keywords

   This sets new valid Options keywords for this SmallFem session
   from the given string.

   If new keywords are to be set, SmallFem::Keywords() must be called
   before SmallFem::Initialize().
   Otherwise, an Exception is throw.

   @see SmallFem::getOptions()
   **

   @fn SmallFem::Initialize
   @param argv A vector of char*
   @param argc The size of the previous vector

   This class method initializes SmallFem.
   Moreover it parses {argv[0], ..., argv[argc - 1]} for Options.

   @see SmallFem::getOptions()
   **

   @fn SmallFem::Finalize
   This class method finalizes SmallFem
   **

   @fn SmallFem::isMaster
   @return Returns true if this process is the master process; returns false otherwise
   **

   @fn SmallFem::getOptions
   @return Returns the Options found in SmallFem::Initialize()
   **

   @fn SmallFem::getVMPeak
   @return Returns the peak virtual memory usage in MB

   @fn SmallFem::OnelabHelper::client
   @return Returns the ONELAB remote client if available; throws an Exception otherwise
   **

   @fn SmallFem::OnelabHelper::name
   @return Returns the name of this ONELAB client if available; throws an Exception otherwise
   **

   @fn SmallFem::OnelabHelper::address
   @return Returns the address of this ONELAB client if available; throws an Exception otherwise
   **

   @fn SmallFem::OnelabHelper::action
   @return Returns the action of this ONELAB client if available; throws an Exception otherwise
   **

   @fn SmallFem::OnelabHelper::defineNumber
   @param name A ONELAB entry
   @param value A value

   Defines a new number in the ONELAB database with the given name and the given default value.
   If the given entry already exists, nothing happens.

   @return Returns SmallFem::OnelabHelper::getNumber(name)
   **

   @fn SmallFem::OnelabHelper::getNumber
   @param name A ONELAB entry
   @return Returns the number stored at the given entry in the ONELAB database (an Exception is thrown if the entry does not exist)
   **

   @fn SmallFem::OnelabHelper::getNumber
   @param name A ONELAB entry
   @return Returns the number stored at the given entry in the ONELAB database (an Exception is thrown if the entry does not exist)
   **

   @fn SmallFem::OnelabHelper::getString
   @param name A ONELAB entry
   @return Returns the string stored at the given entry in the ONELAB database (an Exception is thrown if the entry does not exist)
   **
*/

#endif
