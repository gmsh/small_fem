#include <mpi.h>
#include <vector>
#include "gmsh/BasisFactory.h"
#include "gmsh/Context.h"

#include "SmallFemConfig.h"
#include "SmallFem.h"
#include "Exception.h"

#ifdef HAVE_PETSC
#include "petsc.h"
#endif
#ifdef HAVE_SLEPC
#include "slepc.h"
#endif

#include "ReferenceSpaceManager.h"
#include "GroupOfElement.h"
#include "FunctionSpace.h"

bool         sf::SmallFem::initOne    = false;
bool         sf::SmallFem::finaOne    = false;
sf::Options* sf::SmallFem::option     = NULL;
std::string  sf::SmallFem::myKeywords = "-solver,-onelab";

onelab::remoteNetworkClient* sf::SmallFem::OnelabHelper::olClient  = NULL;
std::string                  sf::SmallFem::OnelabHelper::olName    = "";
std::string                  sf::SmallFem::OnelabHelper::olAddress = "";

sf::SmallFem::SmallFem(void){
}

sf::SmallFem::~SmallFem(void){
  if(option)
    delete option;
}

void sf::SmallFem::Keywords(const std::string& keywords){
  if(initOne)
    throw Exception("SmallFem: cannot set Keywords after Initialize");

  if(!keywords.empty()){
    myKeywords.append(",");
    myKeywords.append(keywords);
  }
}

void sf::SmallFem::Initialize(int argc, char** argv){
  // Initialize only once
  if(!initOne){
    // Call MPI
    MPI_Init(&argc, &argv);

    // Get Options
    option = new Options(argc, argv, myKeywords);

    // ONELAB
    initOnelab();

    // PETSc And SLEPc
    initPetsc(argv);

    // Gmsh Instance
    CTX::instance();

    // Done
    initOne = true;
  }
}

void sf::SmallFem::Finalize(void){
  // Finalize only once and if Initialize first

  if(!finaOne && initOne){
    // Clear Gmsh Instance
    CTX* tmp = CTX::instance();

    if(tmp)
      delete tmp;

    // Clear Gmsh BasisFactory
    BasisFactory::clearAll();

    // Clear SmallFEM ReferenceSpace
    ReferenceSpaceManager::clear();

    // Delete Options
    delete option;
    option = NULL;

    // Finalize MPI, PETSc and SLEPc
#ifdef HAVE_PETSC
    PetscPopErrorHandler();
    PetscFinalize();
#endif
#ifdef HAVE_SLEPC
    SlepcFinalize();
#endif
    MPI_Finalize();

    // Finalize GroupOfElement & FunctionSpace
    GroupOfElement::Finalize();
    FunctionSpace::Finalize();

    // ONELAB
    if(OnelabHelper::olClient)
      delete OnelabHelper::olClient;

    // Done
    finaOne = true;
  }
}

bool sf::SmallFem::isMaster(void){
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  return rank == 0;
}

sf::Options& sf::SmallFem::getOptions(void){
  return *option;
}

double sf::SmallFem::getVMPeak(void){
  // Stream //
  std::ifstream stream("/proc/self/status", std::ifstream::in);
  char          tmp[1048576];
  double        vmPeak = -1;

  // Init //
  for(int i = 0; i < 1048576; i++)
    tmp[i] = 0;

  // Is open ? //
  if(!stream.is_open())
    throw Exception("SmallFem::getVMPeak(): cannot open /proc/self/status");

  // Look for "VmPeak:" //
  stream >> tmp;
  while(strncmp(tmp, "VmPeak:", 1048576) != 0){
    stream.getline(tmp, 1048576);
    stream >> tmp;
  }

  // Read VmPeak //
  stream >> vmPeak;

  // Close & Return //
  stream.close();
  return vmPeak / 1024;
}

void sf::SmallFem::initOnelab(void){
  std::vector<std::string> argOnelab;
  argOnelab.clear();

  try{
    argOnelab = option->getValue("-onelab");
    argOnelab.erase(argOnelab.begin()); // Remove "-onelab" from vector
  }
  catch(Exception& ex){
    // No onelab Fields
    argOnelab.clear();
  }

  if(argOnelab.size() >= 2){
    OnelabHelper::olName    = argOnelab[0];
    OnelabHelper::olAddress = argOnelab[1];
    OnelabHelper::olClient  =
      new onelab::remoteNetworkClient(OnelabHelper::olName,
                                      OnelabHelper::olAddress);
  }
  else{
    OnelabHelper::olClient  = NULL;
  }
}

void sf::SmallFem::initPetsc(char** argv){
  std::vector<std::string> argPetsc;
  argPetsc.clear();

  try{
    argPetsc = option->getValue("-solver");
    argPetsc.erase(argPetsc.begin()); // Remove "-solver" from vector
  }
  catch(Exception& ex){
    // No solver Fields
    argPetsc.clear();
  }

  int    argPetscSize   = argPetsc.size() + 1;
  char** argCStylePetsc = new char*[argPetscSize];

  argCStylePetsc[0] = argv[0];
  Options::cStyle(argPetsc, argCStylePetsc, 1);

#ifdef HAVE_PETSC
  PetscInitialize(&argPetscSize, &argCStylePetsc, PETSC_NULL, PETSC_NULL);
#endif
#ifdef HAVE_SLEPC
  SlepcInitialize(&argPetscSize, &argCStylePetsc, PETSC_NULL, PETSC_NULL);
#endif

#ifdef HAVE_PETSC
  // Stop PETSc when error
  PetscPushErrorHandler(PetscAbortErrorHandler, NULL);
#endif

  delete[] argCStylePetsc;
}

onelab::remoteNetworkClient& sf::SmallFem::OnelabHelper::client(void){
  if(!olClient)
    throw Exception("SmallFem: no ONELAB server");
  return *olClient;
}

std::string sf::SmallFem::OnelabHelper::name(void){
  if(!olClient)
    throw Exception("SmallFem: no ONELAB server");
  return olName;
}

std::string sf::SmallFem::OnelabHelper::address(void){
  if(!olClient)
    throw Exception("SmallFem: no ONELAB server");
  return olAddress;
}

std::string sf::SmallFem::OnelabHelper::action(void){
  std::string action;
  std::vector<onelab::string> ns;
  client().get(ns, olName + "/Action");

  if(ns.size())
    action = ns[0].getValue();

  return action;
}

double sf::SmallFem::OnelabHelper::defineNumber(std::string name, double value){
  // Get number at name
  std::vector<onelab::number> ns;
  client().get(ns, name);

  if(ns.empty()){ // If nothing found, create number and return its value
    onelab::number n(name, value);
    client().set(n);
    return value;
  }
  else{           // Otherwise, return the stored value
    return ns[0].getValue();
  }
}

std::string sf::SmallFem::OnelabHelper::getString(std::string name){
  std::vector<onelab::string> n;
  client().get(n, name);

  if(n.empty())
    throw Exception("SmallFem::OnelabHelper::getString(): "
                    "entry %s not found", name.c_str());

  return n[0].getValue();
}

double sf::SmallFem::OnelabHelper::getNumber(std::string name){
  std::vector<onelab::number> n;
  client().get(n, name);

  if(n.empty())
    throw Exception("SmallFem::OnelabHelper::getNumber(): "
                    "entry %s not found", name.c_str());

  return n[0].getValue();
}
