#include <cstdio>
#include "Exception.h"

using namespace std;
using namespace sf;

Exception::Exception(void){
  why = new string;
}

Exception::Exception(const string format, ...){
  char    buf[256];
  va_list list;

  va_start(list, format);

  if(vsnprintf(buf, 256, format.c_str(), list) < 0)
    why = new string("Unknown Exception");

  else
    why = new string(buf);
}

Exception::~Exception(void) throw(){
  delete why;
}

const char* Exception::what(void) const throw(){
  return why->c_str();
}
