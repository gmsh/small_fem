#include "SmallFem.h"
#include "SmallFemConfig.h"
#include "SolverPastix.h"
#include "Exception.h"

using namespace sf;

#ifdef HAVE_PASTIX

//extern double solver_iter_tol;

template <typename scalar>
SolverPastix<scalar> :: SolverPastix(void){
    /*          Init variables            */
    spm          = (spmatrix_t*) malloc( sizeof( spmatrix_t ) );
    pastix_data  = NULL;
    rhs          = NULL;
    hasMatrix    = false;
    hasRHS       = false;
    isFactorized = false;

    /*            Get num threads         */
    int nThreads;
    #pragma omp parallel
    {
      #pragma omp master
      nThreads = omp_get_num_threads();
    }

    /*         Pastix Init Param          */
    pastixInitParam( iparm, dparm );

    /*              IPARM                 */
    iparm[IPARM_VERBOSE             ] = PastixVerboseNot;
    iparm[IPARM_COMPRESS_METHOD     ] = PastixCompressMethodPQRCP;
    iparm[IPARM_COMPRESS_ORTHO      ] = PastixCompressOrthoPartialQR;
    //iparm[IPARM_FACTORIZATION       ] = PastixFactHETRF;
    iparm[IPARM_REFINEMENT          ] = PastixRefineGMRES;
    iparm[IPARM_GMRES_IM            ] = 500;
    iparm[IPARM_NBITER              ] = 500;
    // iparm[IPARM_IO_STRATEGY         ] = PastixIOSaveGraph;

    iparm[IPARM_THREAD_NBR          ] = nThreads;
    // iparm[IPARM_AUTOSPLIT_COMM      ] = 1;
    // iparm[IPARM_PRODUCE_STATS       ] = 0;
    // iparm[IPARM_COMPRESS_MIN_HEIGHT ] = 14;
    // iparm[IPARM_COMPRESS_MIN_WIDTH  ] = 26;

    /*              DPARM                 */
    //dparm[DPARM_COMPRESS_MIN_RATIO  ] = 1;
    //dparm[DPARM_EPSILON_REFINEMENT  ] = solver_iter_tol;

    /*           PASTIX INIT              */
    pastixInit( &pastix_data, MPI_COMM_WORLD, iparm, dparm );
}

template <typename scalar>
SolverPastix<scalar> :: ~SolverPastix(void){
    /*          PASTIX FINALIZE           */
    pastixFinalize( &pastix_data );

    /*               FREE                 */
    if( spm )
        free( spm );

    if( rhs )
        free( rhs );
}

template <typename scalar>
void SolverPastix<scalar> :: setMatrixCommonPre(SolverMatrix<scalar>& A){
    /* Finalize matrix (format)  */
    A.finalize("csc");

    /*          Pastix           */
    spm->mtxtype  = SpmGeneral;
    spm->fmttype  = SpmCSC;
    spm->n        = A.nColumns();
    spm->dof      = 1;
    spm->dofs     = NULL;
    spm->layout   = SpmColMajor;
    spm->loc2glob = NULL;

    /*          Computed         */
    spm->gN       = 0;
    spm->gnnz     = 0;
    spm->gNexp    = 0;
    spm->nexp     = 0;
    spm->gnnzexp  = 0;
    spm->nnzexp   = 0;
}

template <typename scalar>
void SolverPastix<scalar> :: setMatrixCommonPos(SolverMatrix<scalar>& A){
    /*       Update SPM          */
    spmUpdateComputedFields( spm );
    //FILE *mat;
    //mat = fopen("matrix.txt","w");
    //spmPrint(spm,mat);

    /*          State            */
    //    this->N            = N;
    this->hasMatrix    = true;
    this->isFactorized = false;

    /*        Norm Matrix        */
    normA = spmNorm( SpmFrobeniusNorm, spm );
    spmScalMatrix( 1./normA, spm );
}

template <>
void SolverPastix<double> :: setMatrix(SolverMatrix<double>& A){
    setMatrixCommonPre( A );

    spm->flttype = SpmDouble;
    spm->nnz     = A.get( &(spm->rowptr), &(spm->colptr),
                          (double**)&(spm->values));

    setMatrixCommonPos( A );
}

template <>
void SolverPastix<Complex> :: setMatrix(SolverMatrix<Complex>& A){
    setMatrixCommonPre( A );

    spm->flttype = SpmComplex64;
    spm->nnz     = A.get( &(spm->rowptr), &(spm->colptr),
                          (Complex**)&(spm->values));

    setMatrixCommonPos( A );
}

template <typename scalar>
void SolverPastix<scalar> :: setRHS(SolverVector<scalar>& b){
    /*          Check            */
    if(!hasMatrix)
      throw Exception("SolverPasitx -- Cannot set RHS: need to set Matrix first");

    if(hasRHS)
      free(rhs);

    /*          Copy             */
    rhs  =  (scalar*) malloc( spm->n * sizeof(scalar) );
    memcpy( rhs, b.getData(), spm->n * sizeof(scalar) );

    /*         Norm RHS          */
    for( int i = 0; i<spm->n; i++ )
      rhs[i]/=normA;

    this->hasRHS = true;
}

template<typename scalar>
void SolverPastix<scalar> :: factorize(void){
    if(!hasMatrix)
      throw Exception("SolverPastix -- cannot factorize: no matrix set");

    if(isFactorized)
      return;

    /*          PASTIX ANALYSE            */
    pastix_task_analyze( pastix_data, spm );

    /*          PASTIX NUMFACT            */
    pastix_task_numfact( pastix_data, spm );

    /*     Should be factorized by now    */
    this->isFactorized = true;
}

template <typename scalar>
void SolverPastix<scalar> :: solve(fullVector<scalar>& x){
    if(!hasMatrix)
      throw Exception("SolverPastix -- cannot solve: needs matrix");

    if(!hasRHS)
      throw Exception("SolverPastix -- cannot solve: needs right hand side");

    if(!isFactorized)
      throw Exception("SolverPastix -- cannot solve: needs factorized matrix");

    /*          PASTIX SOLVE              */
    pastix_task_solve( pastix_data, 1, rhs, spm->n );

    /*          PASTIX REFINE             */
    // pastix_task_refine( pastix_data, spm->n , nrhs, X, spm->n,
    //                     this->rhs, spm->n);

    /*          COPY SOLUTION             */
    copy(rhs, x, spm->n );
}

template <typename scalar>
void SolverPastix<scalar> :: solve(SolverMatrix<scalar>& A,
                                   SolverVector<scalar>& rhs,
                                   fullVector<scalar>&   x){
    setMatrix(A);
    setRHS(rhs);
    factorize();
    solve(x);
}

template<typename scalar>
void SolverPastix<scalar> :: setOption(std::string option,
                                       std::string value){
    if(option.compare("blr") == 0)
      setBLR(atof(value.c_str()));
    else if(option.compare("verbose") == 0)
      setVerbosity(atoi(value.c_str()));
}

template<typename scalar>
void SolverPastix<scalar> :: copy(scalar *data,
                                  fullVector<scalar>& out,
                                  int size){
    /*          Alloc           */
    out.resize(size);
    /*          Copy            */
    for(int i = 0; i < size; i++)
      out(i) = data[i];
}

template<typename scalar>
void SolverPastix<scalar> :: setBLR(double tol){
    if(tol == 0){
      iparm[IPARM_COMPRESS_WHEN       ] = PastixCompressNever;
    }
    else{
      iparm[IPARM_COMPRESS_WHEN       ] = PastixCompressWhenBegin;
      dparm[DPARM_COMPRESS_TOLERANCE  ] = tol;
    }
}

template<typename scalar>
void SolverPastix<scalar> :: setVerbosity(int lvl){
  switch(lvl){
  case 0: iparm[IPARM_VERBOSE] = PastixVerboseNot; break;
  case 1: iparm[IPARM_VERBOSE] = PastixVerboseNo;  break;
  case 2: iparm[IPARM_VERBOSE] = PastixVerboseYes; break;

  default: break;
  }
}

#else

template <typename scalar>
SolverPastix<scalar> :: SolverPastix(void){
  throw Exception("Cannot use PaStiX: not activated");
}

template <typename scalar>
SolverPastix<scalar> :: ~SolverPastix(void){
}

template <typename scalar>
void SolverPastix<scalar> :: setRHS(SolverVector<scalar>& b){
  throw Exception("Cannot use PaStiX: not activated");
}

template <typename scalar>
void SolverPastix<scalar> :: setMatrix(SolverMatrix<scalar>& A){
  throw Exception("Cannot use PaStiX: not activated");
}

template<typename scalar>
void SolverPastix<scalar> :: factorize(void){
  throw Exception("Cannot use PaStiX: not activated");
}

template <typename scalar>
void SolverPastix<scalar> :: solve(fullVector<scalar>& x){
  throw Exception("Cannot use PaStiX: not activated");
}

template <typename scalar>
void SolverPastix<scalar> :: solve(SolverMatrix<scalar>& A,
                                   SolverVector<scalar>& rhs,
                                   fullVector<scalar>&   x){
  throw Exception("Cannot use PaStiX: not activated");
}

template<typename scalar>
void SolverPastix<scalar> :: setOption(std::string option,
                                       std::string value){
  throw Exception("Cannot use PaStiX: not activated");
}

#endif

template class sf::SolverPastix<double>;
template class sf::SolverPastix<Complex>;
