#ifndef _SOLVERBEYN_H_
#define _SOLVERBEYN_H_

#include "BeynContext.h"
#include "Formulation.h"
#include "SmallFem.h"
#include "System.h"

/**
   @class SolverBeyn
   @brief An eigenvalue solver using Beyn's method

   An eigenvalue solver using Beyn's method.
   This method can be found in the paper:
   W.J. Beyn, "An integral method for solving nonlinear eigenvalue problems",
   Linear Algebra and its Applications, 2012, 436.
*/

namespace sf{
  class SolverBeyn{
  private:
    static const Complex J;  // Imaginary unit
    static const double  Pi; // Pi

  private:
    // Input data
    BeynContext*         context;
    std::vector<size_t>* gId;
    System<Complex>*     system;

    int                  nNode;
    double               radius;
    Complex              origin;

    // Paramters
    double svdTol;
    int    lStart;
    int    lStep;
    int    maxIt;

    // Output data
    fullVector<Complex>*             lambda;
    std::vector<fullVector<Complex> >* eigV;

  public:
     SolverBeyn(BeynContext& context, System<Complex>& system,
                int nNode, double radius, Complex origin);
    ~SolverBeyn(void);

    void getEigenValues(fullVector<Complex>& val)          const;
    void getEigenVector(fullVector<Complex>& vec, int eig) const;
    int  getNComputedSolution(void)                        const;

    void setRankTestRelativeTolerance(double rTol);
    void setStartingColumnSize(int lStart);
    void setIncreaseColumnStep(int lStep);
    void setMaxIteration(int maxIt);
    void solve(void);

  private:
    void clear(void);
    void simple(fullVector<Complex>& lambda,
                std::vector<fullVector<Complex> >& eigV);
    void  block(fullVector<Complex>& lambda,
                std::vector<fullVector<Complex> >& eigV);

  private:
    // Core stuff helpers
    void getGlobalId(void);
    void integrateCircle(std::vector<fullMatrix<Complex> >& I,
                         std::vector<int>& power, fullMatrix<Complex>& V);
    void manySolve(fullMatrix<Complex>& X, fullMatrix<Complex>& B,
                   Complex lambda);

    // Matrix V0, W0 and B helpers
    static void extract(fullMatrix<Complex>& out, const fullMatrix<Complex>& in,
                        int nRow, int nCol, bool isConjugateTranspose);
    static void extract(fullMatrix<Complex>& V0,
                        fullMatrix<Complex>& W0,
                        fullMatrix<Complex>& S0Inv,
                        const fullMatrix<Complex>& V,
                        const fullMatrix<Complex>& Wh,
                        const std::vector<double>& sigma,
                        int m, int k, int l);
    static void    getB(fullMatrix<Complex>& B,
                        fullMatrix<Complex>& A1, fullMatrix<Complex>& V0,
                        fullMatrix<Complex>& W0, fullMatrix<Complex>& S0Inv);

    // BLAS / LAPACK helpers
    static void getVH(fullMatrix<Complex>& A, int nRow, int nCol);
    static void scale(fullMatrix<Complex>& A, Complex x);
    static void  axpy(fullMatrix<Complex>& Y, fullMatrix<Complex>& X,
                      Complex alpha);
    static void  gemv(fullMatrix<Complex>& A, fullVector<Complex>& x,
                      fullVector<Complex>& y);
    static void  gemm(fullMatrix<Complex>& A, fullMatrix<Complex>& B,
                      fullMatrix<Complex>& C,
                      char transA, char transB);
    static void   svd(const fullMatrix<Complex>& A, std::vector<double>& s,
                      fullMatrix<Complex>& U, fullMatrix<Complex>& V);
    static int   rank(const std::vector<double>& s, double rTolRank);
    static void   eig(const fullMatrix<Complex>& A, fullVector<Complex>& l,
                      fullMatrix<Complex>& V);

    // Column fullMatrix to fullVector helpers
    static void getColumn(fullMatrix<Complex>& A, fullVector<Complex>& v,
                          int j, const std::vector<size_t>& idx);
    static void setColumn(fullVector<Complex>& v, fullMatrix<Complex>& A,
                          int j, const std::vector<size_t>& idx);

    // Concatentate matrices
    static void concatenate(const std::vector<fullMatrix<Complex> >& A,
                            int begin, int end, fullMatrix<Complex>& B);

    // Is master proc?
    static bool isMaster(void);
  };
}

/**
   @fn SolverBeyn::SolverBeyn
   @param context A BeynContext
   @param system A System
   @param nNode An positive integer
   @param radius A real value
   @param origin A complex value
   Instanciates a new SolverBeyn with the given System
   (allowing to inverted the operator considered in the eigenvalue problem).
   The integration contour used by the Beyn's method is a circle given by radius
   and origin (for respectivle its radius and origin in the complex plane).
   The integration uses nNode points.
   **

   @fn SolverBeyn::~SolverBeyn
   Deletes this SolverBeyn
   **

   @fn SolverBeyn::getEigenValues
   @param eig A vector
   The vector eig is now a proxy to the eigenvalues of this SolverBeyn
   **

   @fn SolverBeyn::getEigenVectors
   @param vec A vector
   @param eig An integer
   The vector vec is now a proxy to the eig'th eigenvector of this SolverBeyn
   **

   @fn SolverBeyn::getNComputedSolution
   @return Returns the number of eigenvalues computed by this SolverBeyn
   **

   @fn SolverBeyn::setRankTestRelativeTolerance(double rTol)
   @param rTol A real number
   The rank test relative tolerance of the Beyn's method is set to rTol
   **

   @fn SolverBeyn::setEigenvalueSolverTolerance(double tol)
   @param tol A real number
   The linear eigenvalue solver tolerance of the Beyn's method is set to tol.
   **

   @fn SolverBeyn::setStartingColumnSize(int lStart)
   @param lStart A greater or equal to one positive integer
   The initial number of columns of VHat in the Beyn's method is set to lStart
   **

   @fn SolverBeyn::setIncreaseColumnStep(int lStep)
   @param lStep A greater or equal to one positive integer
   The matrix VHat in the Beyn's method will be increase of lStep columns
   at each iteration
   **

   @fn SolverBeyn::setsetMaxIteration(int maxIt)
   @param maxIt A greater or equal to one positive integer
   The maximum number of iteration is set to maxIt when constructing matrix A0
   **

   @fn SolverBeyn::solve
   Solves the given eigenvalue problem
*/

#endif
