#include <iomanip>
#include "SolverBeyn.h"

using namespace std;
using namespace sf;

void SolverBeyn::simple(fullVector<Complex>& lambda,
                        vector<fullVector<Complex> >& eigV){
  // Be sure that the Sytem is local
  if(!system->getDofManager().isLocal())
    throw Exception("SolverBeyn: the System must be local");

  // Be sure that the System is assembled,
  // so that the DofManger's global ID space is generated
  system->assemble();

  // Get global ids and solver size
  getGlobalId();
  int size = gId->size();

  // Init loop
  bool hasK = false;
  int    it = 0;
  int     l = lStart;
  int     k = 0;
  if(isMaster())
    cout << "Staring simple Beyn's algorithm: "
         << size << " unknowns..." << endl;

  // Prepare A[0], A[1], VHat, V and Wh
  fullMatrix<Complex> V;
  fullMatrix<Complex> Wh;
  fullMatrix<Complex> VHat;
  vector<fullMatrix<Complex> > A(2);
  vector<int> power(2);
  power[0] = 0;
  power[1] = 1;

  // Search A[0] and A[1]
  vector<double> sigma;
  while(hasK == false && it != maxIt){
    if(isMaster())                            // Display iteration count
      cout << " # Iteration " << it+1 << ": " //  |
           << flush;                          //  |
                                              // ---
    getVH(VHat, size, l);                     // Get VHat
    integrateCircle(A, power, VHat);          // Compute A[0] and A[1]
    svd(A[0], sigma, V, Wh);                  // Compute the SVD of A[0]
    k = rank(sigma, svdTol);                  // Rank test
    if(k < l || size == l)                    //  + Rank drop or full A[0]?
      hasK = true;                            //  |---> A[0] is good
    else                                      //  + Full rank?
      l = l + lStep;                          //  |---> Increase l
                                              // ---
    it++;                                     // Keep on searching A0
                                              // -|-
    if(isMaster())                            // Display rank info
      cout << std::scientific << std::setprecision(1)
           << sigma[0]  << " | " << sigma[sigma.size()-1]
           << " [" << k << "]"   << endl;
  }

  // Signal maximum iteration
  if(it == maxIt && isMaster())
      cout << "Maximum number of iteration reached!" << endl;

  // Displays signular values and signal we are done with A0
  if(isMaster()){
    cout << "Done!" << endl
         << "Sigular values: ";
    for(size_t i = 0; i < sigma.size()-1; i++)
      cout << std::scientific << std::setprecision(1) << sigma[i] << " | ";
    cout << std::scientific << std::setprecision(1) << sigma[sigma.size()-1]
         << endl
         << "Constructing linear EVP..." << endl;
  }

  // Extract V0, W0 and S0Inv
  fullMatrix<Complex> V0, W0, S0Inv;
  extract(V0, W0, S0Inv, V, Wh, sigma, size, k, l);

  // Compute B
  fullMatrix<Complex> B;
  getB(B, A[1], V0, W0, S0Inv);

  // Eigenvalues of B
  if(isMaster())
    cout << "Solving linear EVP..." << endl;

  fullMatrix<Complex> tmpV;
  eig(B, lambda, tmpV);

  // Eigenvectors
  vector<size_t> idx(k);
  for(int i = 0; i < k; i++)
    idx[i] = i;

  eigV.resize(k);
  fullVector<Complex> tmp(k);
  for(int i = 0; i < k; i++){
    getColumn(tmpV, tmp, i, idx);
    gemv(V0, tmp, eigV[i]);
  }
}
