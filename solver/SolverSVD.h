#ifndef _SOLVERSVD_H_
#define _SOLVERSVD_H_

#include "SmallFemConfig.h"
#include "SmallFem.h"
#include <vector>

#ifdef HAVE_SLEPC
#include "slepcsvd.h"

/**
   @class SolverSVD
   @brief This class is an singular value solver

   This class is an singular value solver:
   @f$\qquad\mathbf{A} = \mathbf{U}\mathbf{\Sigma}\mathbf{V}^\star@f$,

   The Solver used is <a href="http://www.grycap.upv.es/slepc/">SLEPc</a>.
 */

namespace sf{
  class SolverSVD{
  private:
    double rTolRank;
    SVD    svdctx;
    Mat    A;

  public:
     SolverSVD(void);
    ~SolverSVD(void);

    void getSingularValues(std::vector<double>& sigma)        const;
    void getNonZeroSingularValues(std::vector<double>& sigma) const;
    void getSingularMatrices(Mat U, Mat V)                    const;
    int  getRank(void)                                        const;

    void setMatrix(Mat A);
    void setRankRelativeTolerance(double rTol);

    void solve(void);

 private:
    static void newVec(Vec* v, int size, int sLoc);
    static void newVec(Vec* v, int size);
  };
}

/**
   @fn SolverSVD::SolverSVD
   Instantiates a new SolverSVD
   **

   @fn SolverSVD::~SolverSVD
   Deletes this SolverSVD
   **

   @fn SolverSVD::getSingularValues
   @param eig A vector
   The vector eig is populated with the singular values of this SolverSVD
   **

   @fn SolverSVD::getNonZeroSingularValues
   @param eig A vector
   The vector eig is populated with the non-zero singular values of this SolverSVD
   @see SolverSVD::getRank
   **


   @fn SolverSVD::getSingularMatrices
   @param U A matrix
   @param V A matrix
   U and V are populated such that matrix A is decomposed as:
   @f$\qquad\mathbf{A} = \mathbf{U}\mathbf{\Sigma}\mathbf{V}^\star@f$
   **

   @fn SolverSVD::getRank
   @return Returns the rank found by this SolverSVD
   @see SolverSVD::setRankRelativeTolerance
   **

   @fn SolverSVD::setMatrix
   @param A A matrix

   The matrix to be decomposed by this SolverSVD is now the given matrix
   **

   @fn SolverSVD::setRankRelativeTolerance
   @param rTol a real value
   Sets the rank realitve tolerance to rTol
   **

   @fn SolverSVD::solve
   Solves this SolverSVD
*/

#endif
#endif
