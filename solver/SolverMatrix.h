#ifndef _SOLVERMATRIX_H_
#define _SOLVERMATRIX_H_

#include <string>
#include <vector>
#include <omp.h>

#include "gmsh/fullMatrix.h"

/**
   @class SolverMatrix
   @brief A class handling a Solver linear system matrix

   This class represents a matrix used by a Solver.

   A SolverMatrix data can be recovered into
   three vectors, r[], c[] and a[], such that a[k] is
   the entry of the matrix at row (r[k] - 1) and column (c[k] - 1).
   Values at the same position should be added.

   During the construction of the matrix, multiple values
   can be added in a thread-safe manner.

   Once the matrix assembled, it can be finalized into different
   sparse storage format.
*/

namespace sf{
  template<typename scalar> class SolverMatrix{
  private:
    // Size //
    size_t nRow;
    size_t nCol;
    size_t nNZ;

    // Non zero terms & threads offset //
    std::vector<size_t> nxt;
    std::vector<size_t> min;
    std::vector<size_t> max;

    // Data //
    int*    row;
    int*    col;
    int*    colptr;
    scalar* value;

    // Format //
    static std::string coo; // Coordinate list
    static std::string csc; // Compressed sparse column
    std::string format;     // Format
    bool   isFinalized;     // Is format known yet?

  public:
     SolverMatrix(size_t nRow, size_t nCol, std::vector<size_t> nonZero);
    ~SolverMatrix(void);

    size_t nRows(void)    const;
    size_t nColumns(void) const;

    void   add(size_t row, size_t col, scalar   value);
    size_t get(int**  row, int**  col, scalar** value);
    void   mul(const fullVector<scalar>& vec, fullVector<scalar>& out) const;

    void finalize(std::string format);

    std::string   toMatlab(std::string matrixName) const;
    void writeToMatlabFile(std::string fileName, std::string matrixName) const;

  private:
    static bool compare(int al, int bl, int ac, int bc);
    static void swap(int *row, int *col, scalar *val,
                     int pos1, int pos2);
    static void sort(int *row, int *col, scalar *val,
                     int start, int N  );
    static void heapify(int *row, int *col, scalar *val,
                        int N, int baseval, int root);
    static void addValues(int *row, int *col, scalar *val,
                          int pos1, int pos2);
    static  int merge(int *row, int*col, scalar *value,
                      int start, int N);

    int    mergeDuplicates(void);
    void   countCalls(std::vector<int>& calls);
    void   updateColptr(std::vector<int>& calls);
    void   initColptr(void);

    SolverMatrix(void);
    std::string matlabCommon(std::string matrixName) const;
  };
}

/**
   @fn SolverMatrix::SolverMatrix
   @param nRow The number of row of this SolverMatrix
   @param nCol The number of column of this SolverMatrix
   @param nonZero Per-Thread non zero term number

   Instanciates an new SolverMatrix of zero values with the given number of
   rows and columns.

   The number of non-zero term per-thread should also be given.
   nonZero[i] is the number of non-zero term that will be added by thread i.
   **

   @fn SolverMatrix::~SolverMatrix

   Deletes this SolverMatrix
   **

   @fn SolverMatrix::nRows;
   @return Returns the number of rows of this SolverMatrix
   **

   @fn SolverMatrix::nColumns;
   @return Returns the number of columns of this SolverMatrix
   **

   @fn SolverMatrix::add
   @param row A row index of this SolverMatrix
   @param col A column index of this SolverMatrix
   @param value A real number

   Adds the given value at the given row and column.
   Indices are given in a C style manner (that is starting at 0)
   **

   @fn SolverMatrix::get(int**  row, int**  col, scalar** value)
   @param row A pointer to an int*
   @param col A pointer to an int*
   @param value A pointer to a scalar*

   @return Returns the size of this SolverMatrix non zero entries

   *row now points the memory array storing the rows.
   *col now points the memory array storing the columns.
   *value now points the memory array storing the values.

   The three memory arrays are such that:
   A[(*row)[k] - 1, (*col)[k] - 1] = *(value)[k],
   where A is this SolverMatrix.
   **

   @fn SolverMatrix::mul(const SolverVector<scalar>&, fullVector<scalar>&) const
   @param vec A fullVector
   @param out A fullVector

   Computes the product between this matrix and the vector vec.
   The result is stored in vector out (which is automatically resized)
   **

   @fn SolverMatrix::finalize()
   @param format A sparse matrix format

   Finalizes the internal data structures of this matrix after the assembly.
   These structures depend on the chosen format.

   @note The supported sparse matrix format are "coo" and "csc"
   **

   @fn SolverMatrix::toMatlab(std::string matrixName) const
   @param matrixName A string
   @return Returns a string that can be used in Octave/Matlab
   to reproduce this SolverMatrix, whose name will be the given one
   **

   @fn SolverMatrix::writeToMatlabFile
   @param fileName A string
   @param matrixName A string

   Writes this matrix in Octave/Matlab format into the given file,
   and with the given name
 */

//////////////////////
// Inline Functions //
//////////////////////

template<typename scalar>
inline void sf::SolverMatrix<scalar>::add(size_t row, size_t col, scalar value){
  // Get Thread num //
  const int threadId = omp_get_thread_num();

  // Next index //
  const size_t myNxt = this->nxt[threadId];

  // Add term (convert to Fortran style) //
  this->row[myNxt]   = row + 1;
  this->col[myNxt]   = col + 1;
  this->value[myNxt] = value;

  // Done //
  this->nxt[threadId]++;
}

#endif
