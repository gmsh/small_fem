#include "SmallFem.h"
#include "SolverVector.h"
#include <iomanip>
#include <sstream>

using namespace std;
using namespace sf;

namespace sf{
  template<>
  string SolverVector<Complex>::toMatlab(string vectorName) const{
    // Init
    stringstream stream;

    // Name
    stream << vectorName << " = [";

    // Values
    for(size_t i = 0; i < N; i++)
      stream << std::scientific << std::setprecision(16)
             << "(" << this->v[i].real() << " + i * "<< this->v[i].imag()
             << ")" << "; ";
    stream << "];";

    // Return
    return stream.str();
  }
}
