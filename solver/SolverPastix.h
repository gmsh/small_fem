#ifndef _SOLVERPASTIX_H_
#define _SOLVERPASTIX_H_

#include "Solver.h"
#include "SolverMatrix.h"
#include "SmallFemConfig.h"

#ifdef HAVE_PASTIX
extern "C"{
#include "pastix.h"
#include "spm.h"
}
#endif

/**
   @class SolverPastix
   @brief Wrapper for PaStiX solver

   This class implements a Solver using the sparse
   supernodal solver PaStiX.

   The solver can be downloaded at
   <a href="https://gitlab.inria.fr/solverstack/pastix"</a>

   @author Yoann BERRAHI
*/

namespace sf{
  template<typename scalar> class SolverPastix: public Solver<scalar>{
  private:
    // State //
    bool hasMatrix;
    bool hasRHS;
    bool isFactorized;

#ifdef HAVE_PASTIX
    // Matrix //
    spmatrix_t    *spm; // Pastix Matrix Structure
#endif

    // RHS //
    scalar        *rhs;

#ifdef HAVE_PASTIX
    // Pastix //
    pastix_data_t *pastix_data;
    pastix_int_t   iparm[IPARM_SIZE];
    double         dparm[DPARM_SIZE];
    double         normA;
#endif

  public:
    SolverPastix(void);
    virtual ~SolverPastix(void);

    virtual void solve(SolverMatrix<scalar>& A,
                       SolverVector<scalar>& rhs,
                       fullVector<scalar>&   x);

    virtual void setMatrix(SolverMatrix<scalar>& A);
    virtual void setRHS(SolverVector<scalar>& b);

    virtual void factorize(void);
    virtual void solve(fullVector<scalar>& x);

    virtual void setOption(std::string option,
                           std::string value);

  private:
    void setMatrixCommonPre(SolverMatrix<scalar>& A);
    void setMatrixCommonPos(SolverMatrix<scalar>& A);

    static void copy(scalar* data,
                     fullVector<scalar>& out,
                     int size);

    void setBLR(double tol);
    void setVerbosity(int lvl);
  };// End class //
}// End namespace //

/**
   @fn SolverPastix::SolverPastix
   Instanciates a new SolverPastix
   **

   @fn SolverPastix::~SolverPastix
   Deletes this SolverPastix
*/

#endif
