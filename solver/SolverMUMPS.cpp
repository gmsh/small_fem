#include "SmallFem.h"
#include "SolverMUMPS.h"

using namespace std;
using namespace sf;

template<typename scalar>
void SolverMUMPS<scalar>::setOption(string option, string value){
  if(option.compare("blr") == 0)
    setBLR(atof(value.c_str()));
  else if(option.compare("verbose") == 0)
    setVerbosity(atoi(value.c_str()));
}

template<typename scalar>
void SolverMUMPS<scalar>::
copy(double* data, double** out, size_t size){
  // Alloc
  *out = new double[size];

  // Copy
  for(size_t i = 0; i < size; i++)
    (*out)[i] = data[i];
}

template<typename scalar>
void SolverMUMPS<scalar>::
copy(Complex* data, mumps_double_complex** out, size_t size){
  // Alloc mumps struct
  *out = new mumps_double_complex[size];

  // Copy
  for(size_t i = 0; i < size; i++){
    (*out)[i].r = data[i].real();
    (*out)[i].i = data[i].imag();
  }
}

template<typename scalar>
void SolverMUMPS<scalar>::
copy(double* data, fullVector<double>& out, size_t size){
  // Alloc
  out.resize(size);

  // Copy
  for(size_t i = 0; i < size; i++)
    out(i) = data[i];
}

template<typename scalar>
void SolverMUMPS<scalar>::
copy(mumps_double_complex* data, fullVector<Complex>& out, size_t size){
  // Alloc
  out.resize(size);

  // Copy
  for(size_t i = 0; i < size; i++)
    out(i) = complex<double>(data[i].r, data[i].i);
}

////////////////////////////
// Explicit instantiation //
////////////////////////////
template class sf::SolverMUMPS<double>;
template class sf::SolverMUMPS<Complex>;
