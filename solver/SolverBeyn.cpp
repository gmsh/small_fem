#include "SolverBeyn.h"

using namespace std;
using namespace sf;

#if !defined(F77NAME)
#define F77NAME(x) (x##_)
#endif
extern "C"{
  void F77NAME(zscal)(int *n, Complex *alpha, Complex *x, int *incx);
  void F77NAME(zaxpy)(int *n, Complex *alpha, Complex *x, int *incx,
                      Complex *y, int *incy);
  void F77NAME(zgemv)(const char *trans, int *m, int *n,
                      Complex *alpha, Complex* a, int *lda,
                      Complex *x, int *incx,
                      Complex *beta, Complex *y, int *incy);
  void F77NAME(zgemm)(const char *transa, const char *transb,
                      int *m, int *n, int *k,
                      Complex *alpha, Complex *a, int *lda,
                      Complex *b, int *ldb, Complex *beta,
                      Complex *c, int *ldc);
  void F77NAME(zgesvd)(const char* jobu, const char *jobvt, int *M, int *N,
                       Complex *A, int *lda, double *S, Complex *U, int *ldu,
                       Complex *VT, int *ldvt, Complex *work, int *lwork,
                       double *rwork, int *info);
  void F77NAME(zgeev)(const char *jobvl, const char *jobvr, int *n,
                      Complex *a, int *lda, Complex *w,
                      Complex *vl, int *ldvl, Complex *vr, int *ldvr,
                      Complex* work, int *lwork, double *rwork, int *info);
}

const Complex SolverBeyn::J  = Complex(0, 1);
const double  SolverBeyn::Pi = 4 * atan(1.0);

SolverBeyn::SolverBeyn(BeynContext& context, System<Complex>& system,
                       int nNode, double radius, Complex origin){
  // Save input
  this->context = &context;
  this->gId     = NULL;
  this->system  = &system;

  // Default parameters
  this->svdTol = 1e-3;
  this->lStart = 4;
  this->lStep  = 3;
  this->maxIt  = 10;

  // Output data
  lambda = NULL;
  eigV   = NULL;

  // Path (circle)
  this->nNode  = nNode;
  this->radius = radius;
  this->origin = origin;
}

SolverBeyn::~SolverBeyn(void){
  clear();
}

void SolverBeyn::getEigenValues(fullVector<Complex>& val) const{
  if(lambda)
    val.setAsProxy(*lambda, 0, lambda->size());
  else
    throw Exception("SolverBeyn::getEigenValue(): call solve() before");
}

void SolverBeyn::getEigenVector(fullVector<Complex>& vec, int eig) const{
  if(eig < 0)
    throw Exception("SolverBeyn::getEigenVector(): eig must be greater "
                    "or equal to 0");

  if(eig >= getNComputedSolution())
    throw Exception("SolverBeyn()::getEigenVector(): eig must be smaller "
                    "than SolverBeyn::getNComputedSolution()");

  vec.setAsProxy((*eigV)[eig], 0, (*eigV)[eig].size());
}

int SolverBeyn::getNComputedSolution(void) const{
  if(lambda)
    return lambda->size();
  else
    return 0;
}

void SolverBeyn::setRankTestRelativeTolerance(double rTol){
  this->svdTol = rTol;
}

void SolverBeyn::setStartingColumnSize(int lStart){
  this->lStart = lStart;
}

void SolverBeyn::setIncreaseColumnStep(int lStep){
  this->lStep = lStep;
}

void SolverBeyn::setMaxIteration(int maxIt){
  this->maxIt = maxIt;
}

void SolverBeyn::solve(void){
  // Clear previous stuff
  clear();

  // Allocate fresh data
  lambda = new fullVector<Complex>;
  eigV   = new vector<fullVector<Complex> >;

  simple(*lambda, *eigV);
}

void SolverBeyn::clear(void){
  if(lambda)
    delete lambda;
  lambda = NULL;

  if(eigV)
    delete eigV;
  eigV = NULL;

  if(gId)
    delete gId;
  gId = NULL;
}

void SolverBeyn::getGlobalId(void){
  // Get context dofs and serialize them
  set<Dof>    sDof; context->getDof(sDof);
  vector<Dof>  dof(sDof.begin(), sDof.end());
  sDof.clear();

  // Sort dofs according to their global ids
  const DofManager<Complex>& dofM = system->getDofManager();
  dofM.sortDof(dof);

  // Get (sorted) global ids
  int size = dof.size();
  gId      = new vector<size_t>(size);
  dofM.getGlobalId(dof, *gId);
}

void SolverBeyn::integrateCircle(vector<fullMatrix<Complex> >& I,
                                 vector<int>& power,
                                 fullMatrix<Complex>& V){
  // Get size and check
  int size = I.size();
  if(size != int(power.size()))
    throw Exception("SolverBeyn::Integrate(): "
                    "I and power don't have the same size");

  // Init
  int                          nRow = V.size1();
  int                          nCol = V.size2();
  fullMatrix<Complex>          tmp;
  vector<fullMatrix<Complex> > F(size);

  for(int j = 0; j < size; j++)
    I[j].resize(nRow, nCol, true);

  // Share integration points among processes
  int nProc;
  int myProc;
  MPI_Comm_size(MPI_COMM_WORLD, &nProc);
  MPI_Comm_rank(MPI_COMM_WORLD, &myProc);

  int myNNode = nNode/nProc;
  if(myProc < nNode%nProc)
    myNNode++;

  int* allNNode = new int[nProc];
  MPI_Allgather(&myNNode, 1, MPI_INT, allNNode, 1, MPI_INT, MPI_COMM_WORLD);
  MPI_Barrier(MPI_COMM_WORLD);

  int myStart = 0;
  for(int i = 0; i < myProc; i++)
    myStart += allNNode[i];
  int myEnd = myStart + allNNode[myProc];
  delete[] allNNode;

  // Temp I
  vector<fullMatrix<Complex> > iTmp(size);
  for(int j = 0; j < size; j++)
    iTmp[j].resize(nRow, nCol, true);

  // Loop over integation points and integrate (locally per process)
  for(int i = myStart; i < myEnd; i++){
    Complex   t = Complex(2*Pi * i / nNode, 0);
    Complex phi = origin + Complex(radius, 0) * std::exp(J*t);

    manySolve(tmp, V, phi);
    for(int j = 0; j < size; j++){
      F[j].copy(tmp);
      scale(F[j], std::pow(phi, power[j]) * std::exp(J*t));
      axpy(iTmp[j], F[j], 1);
    }
  }

  // Gather all partial results
  for(int j = 0; j < size; j++)
    MPI_Allreduce(iTmp[j].getDataPtr(), I[j].getDataPtr(), nRow*nCol,
                  MPI_C_DOUBLE_COMPLEX, MPI_SUM, MPI_COMM_WORLD);
  MPI_Barrier(MPI_COMM_WORLD);

  // Final integral value
  for(int j = 0; j < size; j++)
    scale(I[j], Complex(radius/(double)(nNode), 0));
}

void SolverBeyn::manySolve(fullMatrix<Complex>& X, fullMatrix<Complex>& B,
                           Complex lambda){
  // Update context for new lambda, update system and reassemble it
  context->setLambda(lambda);
  system->update();
  system->assemble();

  // Get size
  int size = B.size1();
  int nRHS = B.size2();

  // Allocate X
  X.resize(size, nRHS, true);

  // Temp vectors
  int sSize = system->getSize();
  fullVector<Complex> x(sSize);
  fullVector<Complex> b(sSize);

  // First solve
  getColumn(B, b, 0, *gId);
  system->clearRHS();
  system->addToRHS(b);
  system->solve();
  system->getSolution(x, 0);
  setColumn(x, X, 0, *gId);

  // Solve again for the other RHS
  for(int i = 1; i < nRHS; i++){
    getColumn(B, b, i, *gId);
    system->clearRHS();
    system->addToRHS(b);
    system->solveAgain();
    system->getSolution(x, 0);
    setColumn(x, X, i, *gId);
  }
}

void SolverBeyn::extract(fullMatrix<Complex>& out,
                         const fullMatrix<Complex>& in,
                         int nRow, int nCol, bool isConjugateTranspose){
  // Allocate
  out.resize(nRow, nCol, true);

  // Populate in a column-major way for fullMatrix
  if(isConjugateTranspose)
    for(int j = 0; j < nCol; j++)
      for(int i = 0; i < nRow; i++)
        out(i, j) = conj(in(j, i));
  else
    for(int j = 0; j < nCol; j++)
      for(int i = 0; i < nRow; i++)
        out(i, j) = in(i, j);
}

void SolverBeyn::extract(fullMatrix<Complex>& V0,
                         fullMatrix<Complex>& W0,
                         fullMatrix<Complex>& S0Inv,
                         const fullMatrix<Complex>& V,
                         const fullMatrix<Complex>& Wh,
                         const vector<double>& sigma,
                         int m, int k, int l){
  // Extract V0, W0h
  extract(V0, V,  m, k, false); // V0 =  V(1:m, 1:k)
  extract(W0, Wh, l, k, true);  // W0 = Wh**H(1:l, 1:k)

  // Extract S0Inv
  S0Inv.resize(k, k, true);
  for(int i = 0; i < k; i++)
    S0Inv(i, i) = 1.0/sigma[i];
}

void SolverBeyn::getB(fullMatrix<Complex>& B,
                      fullMatrix<Complex>& A1, fullMatrix<Complex>& V0,
                      fullMatrix<Complex>& W0, fullMatrix<Complex>& S0Inv){
  // Temp
  fullMatrix<Complex> tmp0;
  fullMatrix<Complex> tmp1;

  // Do
  gemm(V0,   A1,    tmp0, 'C', 'N'); // tmp0 =   V0**H * A1
  gemm(tmp0, W0,    tmp1, 'N', 'N'); // tmp1 =  (V0**H * A1) * W0
  gemm(tmp1, S0Inv, B,    'N', 'N'); //    B = ((V0**H * A1) * W0h) * S0Inv
}

void SolverBeyn::getVH(fullMatrix<Complex>& A, int nRow, int nCol){
  // Init Matrix
  A.resize(nRow, nCol, true);

//  // Init Rand
//  double rr;
//  double ri;
//  srand(time(NULL));
//
//  // Populate with rand numbers
//  for(int i = 0; i < nRow; i++){
//    for(int j = 0; j < nCol; j++){
//      rr = static_cast<double>(std::rand()) / static_cast<double>(RAND_MAX);
//      ri = static_cast<double>(std::rand()) / static_cast<double>(RAND_MAX);
//
//      A(i, j) = Complex(rr, ri);
//    }
//  }

  for(int j = 0; j < nCol; j++){
    bool   zero = true;
    Complex sum = Complex(0, 0);

    // Populate column j
    for(int i = 0; i < nRow; i++){
      if(i%(j+1) == 0)
        zero = !zero;

      if(zero)
        A(i, j) = Complex(0, 0);
      else
        A(i, j) = Complex(1, 0);

      sum += A(i, j);
    }

    // Normalize column j
    for(int i = 0; i < nRow; i++)
      A(i, j) = A(i, j) / sum;
  }

//  // Rank test
//  vector<double> s;
//  fullMatrix<Complex> U;
//  fullMatrix<Complex> V;
//  svd(A, s, U, V);
//  cout << endl << rank(s, 1e-12) << endl;
//
//  // Disp
//  for(int i = 0; i < nRow; i++){
//    cout << endl;
//    for(int j = 0; j < nCol; j++){
//      cout << A(i, j) << " ";
//    }
//  }
}

void SolverBeyn::scale(fullMatrix<Complex>& A, Complex x){
  int N = A.size1() * A.size2();
  int stride = 1;
  Complex s = x;
  F77NAME(zscal)(&N, &s, A.getDataPtr(), &stride);
}

void SolverBeyn::axpy(fullMatrix<Complex>& Y, fullMatrix<Complex>& X,
                      Complex alpha){
  int    M = X.size1() * X.size2();
  int INCX = 1;
  int INCY = 1;
  F77NAME(zaxpy)(&M, &alpha, X.getDataPtr(), &INCX, Y.getDataPtr(), &INCY);
}

void SolverBeyn::gemv(fullMatrix<Complex>& A, fullVector<Complex>& x,
                      fullVector<Complex>& y){
  // Init
  int         M = A.size1();
  int         N = A.size2();
  Complex alpha = Complex(1, 0);
  int       LDA = M;
  int      INCX = 1;
  Complex  beta = Complex(0, 0);
  int      INCY = 1;

  // Alloc
  y.resize(M, true);

  // Do
  F77NAME(zgemv)("N", &M, &N,
                 &alpha, A.getDataPtr(), &LDA, x.getDataPtr(), &INCX,
                 &beta,  y.getDataPtr(), &INCY);
}

void SolverBeyn::gemm(fullMatrix<Complex>& A, fullMatrix<Complex>& B,
                      fullMatrix<Complex>& C,
                      char transA, char transB){
  // Init
  int M, K, LDA;
  switch(transA){
  case 'N':
  case 'n':
    M   = A.size1();
    K   = A.size2();
    LDA = M;
    break;

  case 'T':
  case 't':
    M   = A.size2();
    K   = A.size1();
    LDA = K;
    break;

  case 'C':
  case 'c':
    M   = A.size2();
    K   = A.size1();
    LDA = K;
    break;

  default:
    throw Exception("gemm: transA = %c unknown", transA);
  }

  int N, LDB;
  switch(transB){
  case 'N':
  case 'n':
    N   = B.size2();
    LDB = K;
    if(K != B.size1()) throw Exception("gemm: K does not agree");
    break;

  case 'T':
  case 't':
    N   = B.size1();
    LDB = N;
    if(K != B.size2()) throw Exception("gemm: K does not agree");
    break;

  case 'C':
  case 'c':
    N   = B.size1();
    LDB = N;
    if(K != B.size2()) throw Exception("gemm: K does not agree");
    break;

  default:
    throw Exception("gemm: transB = %c unknown", transB);
  }

  int       LDC = M;
  Complex alpha = Complex(1, 0);
  Complex  beta = Complex(0, 0);

  // Alloc
  C.resize(M, N, true);

  // Do
  F77NAME(zgemm)(&transA, &transB, &M, &N, &K, &alpha,
                 A.getDataPtr(), &LDA, B.getDataPtr(), &LDB,
                 &beta, C.getDataPtr(), &LDC);
}

void SolverBeyn::svd(const fullMatrix<Complex>& A, vector<double>& s,
                     fullMatrix<Complex>& U, fullMatrix<Complex>& V){
  // Copy A since it can be destroyed by Xgesvd
  fullMatrix<Complex> tmp;
  tmp.copy(A);

  // Init
  char jobU = 'S'; // the first min(m,n) columns of U are returned in U
  char jobV = 'S'; // the first min(m,n) rows of V**H are returned in VT;
  int     M = A.size1();
  int     N = A.size2();
  int lWork = std::max(1, 2*std::min(M, N) + std::max(M, N)) * 1.5;
  fullVector<Complex> work(lWork);
  fullVector<double> rwork(std::min(M, N)*5);

  // Alloc
  int K = std::min(M, N);
  s.resize(K);
  U.resize(M, K, true);
  V.resize(K, N, true);

  // Run
  int info;
  F77NAME(zgesvd)(&jobU, &jobV, &M, &N, tmp.getDataPtr(), &M, s.data(),
                  U.getDataPtr(), &M, V.getDataPtr(), &K,
                  work.getDataPtr(), &lWork, rwork.getDataPtr(), &info);
  // Check
  if(info > 0)
    throw Exception("SolverBeyn::svd: SVD did not converge");
  if(info < 0)
    throw Exception("SolverBeyn::svd: "
                    "wrong %d-th argument in SVD decomposition", -info);
}

int SolverBeyn::rank(const vector<double>& s, double rTolRank){
  vector<double> sigma(s);
  double maxSigma = sigma[0];
  int         nSV = sigma.size();

  for(int i = 0; i < nSV; i++)
    sigma[i] = sigma[i] / maxSigma;

  // Rank
  int rank = 0;
  for(int i = 0; i < nSV; i++)
    if(sigma[i] > rTolRank)
      rank++;

  // Done
  return rank;
}

void SolverBeyn::eig(const fullMatrix<Complex>& A, fullVector<Complex>& l,
                     fullMatrix<Complex>& V){
  // Copy A since it can be destroyed by Xgeev
  fullMatrix<Complex> tmp;
  tmp.copy(A);

  // Check if square
  if(A.size1() != A.size2())
    throw Exception("SolverBeyn::eig: matrix not square");

  // Init
  char jobL = 'N'; // left eigenvectors of A are not computed
  char jobR = 'V'; // right eigenvectors of A are computed
  int     N = A.size1();
  int lWork = std::max(1, 2*N) * 1.5;
  fullVector<Complex> work(lWork);
  fullVector<double> rwork(2*N);

  // Alloc
  l.resize(N, true);
  V.resize(N, N, true);

  // Run
  int info;
  F77NAME(zgeev)(&jobL, &jobR, &N, tmp.getDataPtr(), &N, l.getDataPtr(),
                 NULL, &N, V.getDataPtr(), &N,
                 work.getDataPtr(), &lWork, rwork.getDataPtr(), &info);
  // Check
  if(info > 0)
    throw Exception("SolverBeyn::eig: QR failed");
  if(info < 0)
    throw Exception("SolverBeyn::eig: "
                    "wrong %d-th argument in SVD decomposition", -info);
}

void SolverBeyn::getColumn(fullMatrix<Complex>& A, fullVector<Complex>& v,
                           int j, const vector<size_t>& idx){
  // Check size
  int vSize = idx.size();
  int  nRow = A.size1();

  if(vSize != nRow)
    throw Exception("SolverBeyn::getColumn(): matrix and vector "
                    "don't have the same size (matrix %d, vector %d)",
                    nRow, vSize);

  // Init v
  v.scale(Complex(0, 0));

  // Populate v
  for(int i = 0; i < vSize; i++)
    v(idx[i]) = A(i, j);
}

void SolverBeyn::setColumn(fullVector<Complex>& v, fullMatrix<Complex>& A,
                           int j, const vector<size_t>& idx){
  // Check size
  int nRow = A.size1();

  if(int(idx.size()) != nRow)
    throw Exception("SolverBeyn::setColumn(): matrix and vector "
                    "don't have the same size (matrix %d, vector %d)",
                    nRow, int(idx.size()));


  // Populate A
  for(int i = 0; i < nRow; i++)
    A(i, j) = v(idx[i]);
}

void SolverBeyn::concatenate(const vector<fullMatrix<Complex> >& A,
                             int begin, int end, fullMatrix<Complex>& B){
  // Get size
  int         nMat = end-begin;
  int         nRow = A[begin].size1();
  vector<int> nCol(nMat);

  for(int i = begin; i < end; i++){
    int       tmp = A[i].size1();
    nCol[i-begin] = A[i].size2();

    if(tmp != nRow)
      throw Exception("SolverBeyn::concatenate() "
                      "matrices must have the same number of rows");
  }

  // Allocate new matrix
  int nColB = 0;
  for(int i = begin; i < end; i++)
    nColB += nCol[i-begin];

  B.resize(nRow, nColB, true);

  // Populate it
  int offset = 0;
  for(int i = begin; i < end; i++){
    // Do
    B.copy(A[i], 0, nRow, 0, nCol[i-begin], 0, offset);

    // Next column offset
    offset += nCol[i-begin];
  }
}

bool SolverBeyn::isMaster(void){
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  return rank == 0;
}
