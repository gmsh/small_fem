#include "SmallFem.h"
#include "Solver.h"

using namespace sf;

template<typename scalar>
Solver<scalar>::~Solver(void){
}

////////////////////////////
// Explicit instantiation //
////////////////////////////
template class sf::Solver<double>;
template class sf::Solver<Complex>;
