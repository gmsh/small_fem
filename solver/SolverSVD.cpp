#include "SolverSVD.h"
#include "Exception.h"

#ifdef HAVE_SLEPC
using namespace std;
using namespace sf;

SolverSVD::SolverSVD(void){
  // Init SLEPc stuff
  SVDCreate(PETSC_COMM_WORLD, &svdctx);
  SVDSetWhichSingularTriplets(svdctx, SVD_LARGEST);
  SVDSetType(svdctx, SVDTRLANCZOS);
  SVDSetTolerances(svdctx, 1e-15, 1000);

  // NB: User options are set in solve

  // Default rank relative tolerance
  rTolRank = 1e-12;
}

SolverSVD::~SolverSVD(void){
  SVDDestroy(&svdctx);
}

void SolverSVD::getSingularValues(vector<double>& sigma) const{
  // Number of SVs
  int nConv;
  SVDGetConverged(svdctx, &nConv);

  // Clear and resize vector if needed;
  if(nConv != (int)(sigma.size())){
    sigma.clear();
    sigma.resize(nConv);
  }

  // Populate
  for(int i = 0; i < nConv; i++)
    SVDGetSingularTriplet(svdctx, i, &sigma[i], NULL, NULL);
}

void SolverSVD::getNonZeroSingularValues(vector<double>& sigma) const{
  // All singular values and rank
  vector<double> all; getSingularValues(all);
  int          rank = getRank();

  // Initialize sigma and copy SVs up to rank (sorted in largest first)
  sigma.resize(rank);
  for(int i = 0; i < rank; i++)
    sigma[i] = all[i];
}

void SolverSVD::getSingularMatrices(Mat U, Mat V) const{
  // NB: U and V such that A = U * Sigma * V^H
  // Number of SVs
  int nConv;
  SVDGetConverged(svdctx, &nConv);

  // Get sizes
  int sizeU, sizeV;
  MatGetSize(U, &sizeU, &sizeV);

  // Zeroing U and V
  MatZeroEntries(U);
  MatZeroEntries(V);

  // Init Vec
  double sigma;
  Vec u; newVec(&u, sizeU);
  Vec v; newVec(&v, sizeV);
  PetscScalar* dataU;
  PetscScalar* dataV;

  // Ownership of each row of U
  int rowStartU, rowEndU, nRowLocalU;
  MatGetOwnershipRange(U, &rowStartU, &rowEndU);
  nRowLocalU = rowEndU-rowStartU;

  // Ownership of each row of V
  int rowStartV, rowEndV, nRowLocalV;
  MatGetOwnershipRange(V, &rowStartV, &rowEndV);
  nRowLocalV = rowEndV-rowStartV;

  // Row global indices of U for each process
  PetscInt* idxRowU = new PetscInt[nRowLocalU];
  for(int i = 0; i < nRowLocalU; i++)
    idxRowU[i] = i + rowStartU;

  // Row global indices of V for each process
  PetscInt* idxRowV = new PetscInt[nRowLocalV];
  for(int i = 0; i < nRowLocalV; i++)
    idxRowV[i] = i + rowStartV;

  for(int i = 0; i < nConv; i++){
    SVDGetSingularTriplet(svdctx, i, &sigma, u, v);

    VecGetArray(u, &dataU);
    VecGetArray(v, &dataV);

    MatSetValues(U, nRowLocalU, idxRowU, 1, &i, dataU, INSERT_VALUES);
    MatSetValues(V, nRowLocalV, idxRowV, 1, &i, dataV, INSERT_VALUES);

    VecRestoreArray(u, &dataU);
    VecRestoreArray(v, &dataV);
  }

  // Start to assemble U and V
  MatAssemblyBegin(U, MAT_FINAL_ASSEMBLY);
  MatAssemblyBegin(V, MAT_FINAL_ASSEMBLY);

  // Clear
  VecDestroy(&u);
  VecDestroy(&v);
  delete[] idxRowU;
  delete[] idxRowV;

  // Wait for assembling U and V
  MatAssemblyEnd(U, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(V, MAT_FINAL_ASSEMBLY);
}

int SolverSVD::getRank(void) const{
  // Normalized SVs
  vector<double> sigma; getSingularValues(sigma);
  double     maxSigma = sigma[0];
  int             nSV = sigma.size();

  for(int i = 0; i < nSV; i++)
    sigma[i] = sigma[i] / maxSigma;

  // Rank
  int rank = 0;
  for(int i = 0; i < nSV; i++)
    if(sigma[i] > rTolRank)
      rank++;

  // Done
  return rank;
}

void SolverSVD::setMatrix(Mat A){
  this->A = A;
}

void SolverSVD::setRankRelativeTolerance(double rTol){
  this->rTolRank = rTol;
}

void SolverSVD::solve(void){
  // Number of singular values (all)
  int nRow, nCol, nSV;
  MatGetSize(A, &nRow, &nCol);
  if(nRow < nCol)
    nSV = nRow;
  else
    nSV = nCol;

  // Set operator
  SVDSetOperator(svdctx, A);
  SVDSetDimensions(svdctx, nSV, PETSC_DEFAULT, PETSC_DEFAULT);
  SVDSetFromOptions(svdctx);

  // Be sure to impose SVD_LARGEST (even if user changed it)
  SVDSetWhichSingularTriplets(svdctx, SVD_LARGEST);

  // Compute SVD
  SVDSolve(svdctx);

  // Check
  int nConv;
  SVDGetConverged(svdctx, &nConv);
  if(nConv != nSV)
    throw Exception("SolverSVD: found less SVs than expected");
}

void SolverSVD::newVec(Vec* v, int size, int sLoc){
  VecCreate(PETSC_COMM_WORLD, v);
  VecSetSizes(*v, sLoc, size);
  VecSetType(*v, VECMPI);
  VecSetUp(*v);
}

void SolverSVD::newVec(Vec* v, int size){
  newVec(v, size, PETSC_DETERMINE);
}
#endif
