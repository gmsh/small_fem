#include <iomanip>
#include "SolverBeyn.h"

using namespace std;
using namespace sf;

// Implements the block variant of Beyn's alogorithm proposed in:
// A. Imakura et al., "Relationships among contour integral-based methods
// for solving generalized eigenvalue problems",
// Japan Journal of Industrial and Applied Mathematics, 2016, 33.

void SolverBeyn::block(fullVector<Complex>& lambda,
                       vector<fullVector<Complex> >& eigV){
  // M stuff
  int M = 2;
  if(lStart < M){
    cout << "Original column space for VHat is smaller than M" << endl
         << "Force lStart = M" << endl;
    lStart = M;
  }

  // Be sure that the Sytem is local
  if(!system->getDofManager().isLocal())
    throw Exception("SolverBeyn: the System must be local");

  // Be sure that the system is assemble,
  // so that the DofManger's global ID space is generated
  system->assemble();

  // Get global ids and solver size
  getGlobalId();
  int size = gId->size();

  // Init loop
  bool hasK = false;
  int    it = 0;
  int     l = lStart;
  int     k = 0;
  if(isMaster())
    cout << "Staring block Beyn's algorithm: "
         << size << " unknowns..." << endl;

  // Prepare A[i]s, S, VHat, V and Wh
  fullMatrix<Complex>          V;
  fullMatrix<Complex>          Wh;
  fullMatrix<Complex>          VHat;
  fullMatrix<Complex>          S;
  vector<fullMatrix<Complex> > A(M+1);
  vector<int>                  power(M+1);
  for(int i = 0; i < M+1; i++)
    power[i] = i;

  // Search S
  vector<double> sigma;
  while(hasK == false && it != maxIt){
    if(isMaster())                            // Display iteration count
      cout << " # Iteration " << it+1 << ": " //  |
           << flush;                          //  |
                                              // ---
    getVH(VHat, size, l);                     // Get VHat
    integrateCircle(A, power, VHat);          // Compute all the A[i]s
    //A[0].print("A0");
    //A[1].print("A1");
    //A[2].print("A2");
    concatenate(A, 0, M, S);                  // Build S
    //S.print("S");
    svd(S, sigma, V, Wh);                     // Compute the SVD of S
    k = rank(sigma, svdTol);                  // Rank test
    if(k < l || size == l)                    //  + Rank drop with respect to l?
      hasK = true;                            //  |---> S is good
    else                                      //  + Full rank?
      l = l + lStep;                          //  |---> Increase l
                                              // ---
    it++;                                     // Keep on searching S
                                              // -|-
    if(isMaster())                            // Display rank info
      cout << std::scientific << std::setprecision(1)
           << sigma[0]  << " | " << sigma[sigma.size()-1]
           << " [k=" << k << " / l=" << l << "]" << endl;
  }

  // Signal maximum iteration
  if(it == maxIt && isMaster())
      cout << "Maximum number of iteration reached!" << endl;

  // Displays signular values and signal we are done with A0
  if(isMaster()){
    cout << "Done!" << endl
         << "Sigular values: ";
    for(size_t i = 0; i < sigma.size()-1; i++)
      cout << std::scientific << std::setprecision(1) << sigma[i] << " | ";
    cout << std::scientific << std::setprecision(1) << sigma[sigma.size()-1]
         << endl
         << "Constructing linear EVP..." << endl;
  }

  // Extract V0, W0 and S0Inv
  fullMatrix<Complex> V0, W0, S0Inv;
  extract(V0, W0, S0Inv, V, Wh, sigma, size, k, l*M);

  // Compute B
  fullMatrix<Complex> B;
  fullMatrix<Complex> Sp;
  concatenate(A, 1, M+1, Sp);
  //Sp.print("Sp");
  getB(B, Sp, V0, W0, S0Inv);

  // Eigenvalues of B
  if(isMaster())
    cout << "Solving linear EVP..." << endl;

  fullMatrix<Complex> tmpV;
  eig(B, lambda, tmpV);

  // Eigenvectors
  vector<size_t> idx(k);
  for(int i = 0; i < k; i++)
    idx[i] = i;

  eigV.resize(k);
  fullVector<Complex> tmp(k);
  for(int i = 0; i < k; i++){
    getColumn(tmpV, tmp, i, idx);
    gemv(V0, tmp, eigV[i]);
  }
}
