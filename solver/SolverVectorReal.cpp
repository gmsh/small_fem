#include "SmallFem.h"
#include "SolverVector.h"
#include <iomanip>
#include <sstream>

using namespace std;
using namespace sf;

namespace sf{
  template<>
  string SolverVector<double>::toMatlab(string vectorName) const{
    // Init
    stringstream stream;

    // Name
    stream << vectorName << " = [";

    // Values
    for(size_t i = 0; i < N; i++)
      stream << std::setprecision(16) << std::scientific
             << this->v[i] << "; ";
    stream << "];";

    // Return
    return stream.str();
  }
}
