#include "SmallFem.h"
#include "SolverMatrix.h"
#include <iomanip>
#include <sstream>

using namespace std;
using namespace sf;

namespace sf{
  template<>
    string SolverMatrix<double>::toMatlab(string matrixName) const{
    // Init
    stringstream stream;
    size_t N = this->max[this->max.size() - 1];

    // Common part
    stream << matlabCommon(matrixName);

    // Values
    stream << "[";
    for(size_t i = 0; i < N; i++)
      stream << std::setprecision(16) << std::scientific
             << this->value[i] << " ";
    stream << "]";

    // Done
    stream << ");";

    // Return
    return stream.str();
  }
}
