#ifndef _SOLVERPETSC_H_
#define _SOLVERPETSC_H_

#include "Solver.h"
#include "SmallFemConfig.h"

#ifdef HAVE_PETSC
#include "petscmat.h"
#include "petscvec.h"
#include "petscksp.h"

/**
   @class SolverPETSc
   @brief A Solver using the PETSc library

   This class interfaces the solvers fromt the PETSc library.
   The default solver is an unpreconditioned GMRES(20)
   with a maximum iteration count of 100.
*/

namespace sf{
  template<typename scalar> class SolverPETSc: public Solver<scalar>{
  private:
    Mat A;
    Vec b;

    KSP solver;
    PC  precond;

    void (*post)(KSP ksp, PC pc);

  public:
    SolverPETSc(void);
    virtual ~SolverPETSc(void);

    virtual void solve(SolverMatrix<scalar>& A,
                       SolverVector<scalar>& rhs,
                       fullVector<scalar>& x);

    virtual void setMatrix(SolverMatrix<scalar>& A);
    virtual void setRHS(SolverVector<scalar>& rhs);

    virtual void factorize(void);
    virtual void solve(fullVector<scalar>& x);

    virtual void setOption(std::string option,
                           std::string value);

    KSP  getKSP(void);
    PC   getPC(void);
    void setPostKSPSetUp(void (*post)(KSP ksp, PC pc));

  private:
    PetscScalar* toPetscScalar(scalar* a, size_t size);
    void copy(Vec in, fullVector<scalar>& out);
  };
}

/**
   @fn SolverPETSc::SolverPETSc
   Instanciates a new SolverPETSc
   **

   @fn SolverPETSc::~SolverPETSc
   Deletes this SolverPETSc
   **

   @fn SolverPETSc::getKSP
   @return Returns the Krylov subspace solver of this SolverPETSc
   **

   @fn SolverPETSc::getPC
   @return Returns the preconditioner of this SolverPETSc
   **

   @fn SolverPETSc::setPostKSPSetUp
   @param A pointer to a function taking  (KSP, PC) as input and returning void
   Sets a function to be called right after a KSP has been set up [KSPSetUp()].
   By default, this function is NULL, and nothing special is done...
*/

#endif
#endif
