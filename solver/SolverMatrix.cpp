#include <sstream>
#include <fstream>
#include "SmallFem.h"
#include "Exception.h"
#include "SolverMatrix.h"

using namespace sf;

template<typename scalar>
std::string SolverMatrix<scalar>::coo = std::string("coo");

template<typename scalar>
std::string SolverMatrix<scalar>::csc = std::string("csc");

template<typename scalar>
SolverMatrix<scalar>::SolverMatrix(void){
}

template<typename scalar>
SolverMatrix<scalar>::
SolverMatrix(size_t nRow, size_t nCol, std::vector<size_t> nonZero){
  // Matrix size //
  this->nRow   = nRow;
  this->nCol   = nCol;
  this->colptr = NULL;

  // Get num threads //
  size_t nThreads;
  #pragma omp parallel
  {
    #pragma omp master
    nThreads = omp_get_num_threads();
  }

  // Check thread numbers //
  if(nonZero.size() != nThreads)
    throw Exception("%s %s %s", "SolverMatrix: ", "number of non zero entry ",
                    "doesn't match number of threads");

  // Ranges //
  nxt.resize(nThreads);
  max.resize(nThreads);
  min.resize(nThreads);

  min[0] = 0;
  for(size_t i = 1, j = 0; i < nThreads; i++, j++)
    min[i] = min[j] + nonZero[j];

  max[0] = nonZero[0];
  for(size_t i = 1, j = 0; i < nThreads; i++, j++)
    max[i] = max[j] + nonZero[i];

  for(size_t i = 0; i < nThreads; i++)
    nxt[i] = min[i];

  // Matrix data //
  nNZ   = max[nThreads - 1];
  row   = new    int[nNZ];
  col   = new    int[nNZ];
  value = new scalar[nNZ];

  // Format //
  format      = std::string("");
  isFinalized = false;
}

template<typename scalar>
SolverMatrix<scalar>::~SolverMatrix(void){
  delete[] row;
  delete[] col;
  delete[] value;

  if(colptr)
    delete[] colptr;
}

template<typename scalar>
size_t SolverMatrix<scalar>::nRows(void) const{
  return nRow;
}

template<typename scalar>
size_t SolverMatrix<scalar>::nColumns(void) const{
  return nCol;
}

template<typename scalar>
size_t SolverMatrix<scalar>::get(int** row, int** col, scalar** value){
  // Be sure matrix is finalized
  if(!isFinalized)
    throw Exception("SolverMatrix::get(): matrix must be finalized first");

  // Return data depending on format
  if(format.compare(csc) == 0){
    *row   = this->row;
    *col   = this->colptr;
    *value = this->value;
  }
  else if(format.compare(coo) == 0){
    *row   = this->row;
    *col   = this->col;
    *value = this->value;
  }
  else
    throw Exception("SolverMatrix::get(): unknown format %s", format.c_str());

  // Vector size
  return this->nNZ;
}

template<typename scalar>
void SolverMatrix<scalar>::mul(const fullVector<scalar>& vec,
                               fullVector<scalar>& out) const{
  // Be sure matrix is finalized
  if(!isFinalized)
    throw Exception("SolverMatrix::mul(): matrix must be finalized first");

  // TODO
  if(format.compare(coo) != 0)
    throw Exception("SolverMatrix::mul(): never tested with format %s",
                    format.c_str());

  // Check size
  if(vec.size() != (int)(nCol))
    throw Exception("SolverMatrix::mul(): "
                    "matrix and vector sizes do not match");

  // Allocate out
  out.resize(nCol);
  out.scale(0.);

  // Multiply
  for(size_t i = 0; i < nNZ; i++)
    out(row[i]-1) += value[i] * vec(col[i]-1); // -1 for Fotran to C
}

template<typename scalar>
void SolverMatrix<scalar>::finalize(std::string format){
  if(isFinalized)
    return;

  if(format.compare(csc) == 0){
    initColptr();
    mergeDuplicates();
  }
  else if(format.compare(coo) == 0){
  }
  else
    throw Exception("SolverMatrix::finalize(): unknown format %s",
                    format.c_str());

  this->format = format;
  isFinalized  = true;
}

template<typename scalar>
void SolverMatrix<scalar>::writeToMatlabFile(std::string fileName,
                                             std::string matrixName) const{
  std::ofstream stream;
  stream.open(fileName.c_str());
  stream << toMatlab(matrixName) << std::endl;
  stream.close();
}

template<typename scalar>
std::string SolverMatrix<scalar>::matlabCommon(std::string matrixName) const{
  // Stream and size
  std::stringstream stream;
  size_t N = this->nNZ;

  // Call to 'sparse'
  //stream << matrixName << " = sparse(";

  // Rows
  //stream << "[";
  for(size_t i = 0; i < N; i++)
    stream << this->row[i] << ";";
  stream << std::endl; //"], ";

  // Columns
  //stream << "[";
  for(size_t i = 0; i < N; i++)
    stream << this->col[i] << ";";
  stream << std::endl; //"], ";

  // Return
  return stream.str();
}

template<typename scalar>
bool SolverMatrix<scalar>::compare(int al, int bl, int ac, int bc){
  // compare function   (a<b) ?           //
  // Negatives are greater than positives //
  if(ac<0)
    return false;
  if(bc<0)
    return true;
  else{
    if(ac==bc)
      return (al<bl);
    else
      return (ac<bc);
  }
}

template<typename scalar>
void SolverMatrix<scalar>::swap(int* row, int* col, scalar* val,
                                int pos1, int pos2){
  int tmp1;
  int tmp2;
  scalar tmp3;
  // Swapping rows //
  tmp1      = row[pos2];
  row[pos2] = row[pos1];
  row[pos1] = tmp1;
  // Swapping cols //
  tmp2      = col[pos2];
  col[pos2] = col[pos1];
  col[pos1] = tmp2;
  // Swapping values //
  tmp3      = val[pos2];
  val[pos2] = val[pos1];
  val[pos1] = tmp3;
}

template<typename scalar>
void SolverMatrix<scalar>::addValues(int* row, int* col, scalar* values,
                                     int pos1, int pos2){
  //Adds the value located at pos2//
  //to the value at located at pos1//
  //The changes col[pos2], row[pos2] and value[pos2] to -1//
  values[pos1]  = values[pos1] + values[pos2];
  values[pos2]  = -1;
  col[pos2]     = -1;
  row[pos2]     = -1;
}

template<typename scalar>
void SolverMatrix<scalar>::heapify(int* row, int* col, scalar* value,
                                   int N, int baseval, int root){
  // Indexes of elements //
  int largest = root;
  int left    = 2*root+1-baseval;
  int right   = 2*root+2-baseval;

  // Is left elt larger ?       //
  if(left  < N && compare(row[largest], row[left],  col[largest], col[left]))
    largest=left;

  // Is right elt even larger ? //
  if(right < N && compare(row[largest], row[right], col[largest], col[right]))
    largest=right;

  // If largest has changed swap and heapify //
  if(largest!=root){
    swap(row, col, value, largest, root);
    heapify(row, col, value, N, baseval, largest);
  }
}

template<typename scalar>
void SolverMatrix<scalar>::sort(int* row, int* col, scalar* values,
                                int start, int end){
  // from start elt to end (N) elt
  // meaning the end elt is the elt number N
  for(int i = (end+start)/2-1; i >= start; i--)
    heapify(row, col, values, end, start, i);

  for(int i=end-1; i>=start; i--){
    swap(row, col, values, start, i);
    heapify(row, col, values ,i ,start ,start);
  }
}

template<typename scalar>
void SolverMatrix<scalar>::countCalls(std::vector<int>& calls){
  int nCall = calls.size();
  for(int i = 0; i < nCall; i++)
    calls[i] = 0;

  int k  = 0;
  for(int i = 0; i < nCall; i++){
    while(k < (int)(this->nNZ) && this->col[k] == i + 1){
      calls[i]++;
      k++;
    }
  }
}

template<typename scalar>
void SolverMatrix<scalar>::updateColptr(std::vector<int>& calls){
  this->colptr[0] = 1;
  int size = calls.size();
  for(int i = 1; i < size + 1; i++){
    this->colptr[i] = this->colptr[i-1] + calls[i-1];
  }
}

template<typename scalar>
int SolverMatrix<scalar>::merge(int* row, int* col, scalar* values,
                                int start, int N){
  int ref   = start;
  int count =     0;
  for(int i = start+1; i<N; i++){
    if(row[i] == row[ref]){
      addValues(row,col,values,ref,i);
      count++;
    }
    else{
      ref=i;
    }
  }
  return count;
}

template<typename scalar>
int SolverMatrix<scalar>::mergeDuplicates(void){
  int count = 0;
  std::vector<int> calls(nCol);
  /*         sort by col first        */
  sort(row, col, value, 0, this->nNZ);
  /*          Creates colptr          */
  countCalls(calls);
  updateColptr(calls);
  /*     Adds duplicates and put -1   */
  for(size_t i = 0; i < nCol ; i++)
    count += merge(this->row, this->col, this->value,
                   colptr[i]-1, colptr[i+1]-1);

  /*            Sort Again            */
  sort(row, col, value, 0, this->nNZ);
  /*            Update the nNZ        */
  this->nNZ = this->nNZ - count;
  /* Update colptr without duplicates */
  countCalls(calls);
  updateColptr(calls);
  /*               Done               */
  return count;
}

template<typename scalar>
void SolverMatrix<scalar>::initColptr(void){
  this->colptr = new int[nCol+1];

  for(size_t i = 0; i < nCol+1; i++)
    colptr[i] = 0;
}

////////////////////////////
// Explicit instantiation //
////////////////////////////
template class sf::SolverMatrix<double>;
template class sf::SolverMatrix<Complex>;
