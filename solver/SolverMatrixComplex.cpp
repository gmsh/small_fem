#include "SmallFem.h"
#include "SolverMatrix.h"
#include <iomanip>
#include <sstream>

using namespace std;
using namespace sf;

namespace sf{
  template<>
  string SolverMatrix<Complex>::toMatlab(string matrixName) const{
    // Init
    stringstream stream;
    size_t N = this->max[this->max.size() - 1];

    // Common part
    stream << matlabCommon(matrixName);

    // Values
    //stream << "[";
    for(size_t i = 0; i < N; i++)
      stream << std::scientific << std::setprecision(16)
             << "(" << this->value[i].real() << " + i * "<< this->value[i].imag()
             << ")" << ";";
    stream << std::endl; //"]";

    // Number of rows and columns
    //stream << ");";

    // Return
    return stream.str();
  }
}
