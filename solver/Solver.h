#ifndef _SOLVER_H_
#define _SOLVER_H_

#include "gmsh/fullMatrix.h"
#include "SolverVector.h"
#include "SolverMatrix.h"

/**
   @interface Solver
   @brief Common interface for solvers

   This class is a common interface for linear system solvers.
 */

namespace sf{
  template<typename scalar> class Solver{
  public:
    virtual ~Solver(void);
    virtual void solve(SolverMatrix<scalar>& A,
                       SolverVector<scalar>& rhs,
                       fullVector<scalar>& x) = 0;

    virtual void setMatrix(SolverMatrix<scalar>& A) = 0;
    virtual void setRHS(SolverVector<scalar>& rhs) = 0;

    virtual void factorize(void) = 0;
    virtual void solve(fullVector<scalar>& x) = 0;

    virtual void setOption(std::string option,
                           std::string value) = 0;
  };
}

/**
   @fn Solver::~Solver
   Deletes this Solver
   **

   @fn Solver::solve(SolverMatrix<scalar>&,SolverVector<scalar>&,fullVector<scalar>&)
   @param A A SolverMatrix
   @param rhs A SolverVector
   @param x A fullVector

   Solver the linear system Ax = rhs.

   The matrix and the right hand side can be modified,
   depending on the actual implementation.
   This should be found in the description of the actual solver.
   **

   @fn Solver::setMatrix
   @param A A SolverMatrix
   The given matrix is now this Solver matrix
   **

   @fn Solver::setRHS
   @param rhs A SolverVector
   The given vector is now this Solver right hand side
   **

   @fn Solver::factorize
   Computes the LU decomposition of the given matrix
   **

   @fn Solver::solve(fullVector<scalar>& x)
   @param x The fullVector where the solution will be stored
   Uses the previous Solver::factorize() and Solver::setRHS()
   to compute the solution of the linear system
   **

   @fn Solver::setOption
   @param option A string defining an option
   @param value A string defining a value
   Sets the option of this Solver to the given value
 */

#endif
