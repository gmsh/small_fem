#include "SmallFem.h"
#include "Exception.h"
#include "SolverPETSc.h"

#ifdef HAVE_PETSC

using namespace std;
using namespace sf;

template<typename scalar>
SolverPETSc<scalar>::SolverPETSc(void){
  // Unpreconditioned GMRES(20) by default (with maximum 100 iteration)
  KSPCreate(MPI_COMM_WORLD, &solver);

  KSPSetType(solver, "gmres");
  KSPSetTolerances(solver, PETSC_DEFAULT,
                   PETSC_DEFAULT, PETSC_DEFAULT, 100);
  KSPGMRESSetRestart(solver, 20);

  KSPGetPC(solver, &precond);
  PCSetType(precond, PCNONE);

  // Do nothing spectial after KSPSetUp is called
  post = NULL;
}

template<typename scalar>
SolverPETSc<scalar>::~SolverPETSc(void){
  KSPDestroy(&solver);
  MatDestroy(&A);
  VecDestroy(&b);
}

template<typename scalar>
void SolverPETSc<scalar>::setMatrix(SolverMatrix<scalar>& A){
  // Finalize matrix (format)
  A.finalize("coo");

  // Get data
  int*      row;
  int*      col;
  scalar* value;
  int       nNZ = A.get(&row, &col, &value);

  // Copy to PETSc
  PetscScalar* petsc = toPetscScalar(value, nNZ);
  MatCreateSeqAIJFromTriple(PETSC_COMM_SELF,
                            A.nRows(), A.nColumns(), row, col,
                            petsc, &(this->A), nNZ, PETSC_TRUE);

  // Clear
  delete[] petsc;
}

template<typename scalar>
void SolverPETSc<scalar>::setRHS(SolverVector<scalar>& rhs){
  // Size
  size_t size = rhs.getSize();

  // PETSc
  VecCreate(PETSC_COMM_SELF, &b);
  VecSetSizes(b, size, size);
  VecSetType(b, "seq");

  // Populate with copies
  scalar* data = rhs.getData();
  for(size_t i = 0; i < size; i++)
    VecSetValue(b, i, data[i], INSERT_VALUES);
}

template<typename scalar>
void SolverPETSc<scalar>::factorize(void){
  throw Exception("SolverPETSc::factorize(): cannot be called");
}

template<typename scalar>
void SolverPETSc<scalar>::solve(fullVector<scalar>& x){
  // Assemble PETSc matrix and PETSc vector
  MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);
  VecAssemblyBegin(b);
  MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);
  VecAssemblyEnd(b);

  // Size (from RHS)
  PetscInt size;
  VecGetSize(b, &size);

  // Assign matrix
  KSPSetOperators(solver, A, A);

  // Override with PETSc database
  KSPSetFromOptions(solver);
  PCSetFromOptions(precond);

  // Set up KSP solver and do other stuff if needed
  KSPSetUp(solver);
  if(post)
    post(solver, precond);

  // Initialize PETSc solution vector
  Vec xPetsc;
  VecCreate(PETSC_COMM_SELF, &xPetsc);
  VecSetSizes(xPetsc, size, size);
  VecSetType(xPetsc, "seq");

  // Solve and copy xPetsc to x
  KSPSolve(solver, b, xPetsc);
  copy(xPetsc, x);

  // Clear
  VecDestroy(&xPetsc);
}

template<typename scalar>
void SolverPETSc<scalar>::solve(SolverMatrix<scalar>& A,
                                SolverVector<scalar>& rhs,
                                fullVector<scalar>& x){
  setMatrix(A);
  setRHS(rhs);
  solve(x);
}

template<typename scalar>
void SolverPETSc<scalar>::setOption(string option,
                                    string value){
  cout << "Warning: "
          "use PETSc database system instead of SolverPETSc::setOption() - "
          "we did nothing..." << endl;
}

template<typename scalar>
KSP SolverPETSc<scalar>::getKSP(void){
  return solver;
}

template<typename scalar>
PC SolverPETSc<scalar>::getPC(void){
  return precond;
}

template<typename scalar>
void SolverPETSc<scalar>::setPostKSPSetUp(void (*post)(KSP ksp, PC pc)){
  this->post = post;
}

////////////////////////////
// Explicit instantiation //
////////////////////////////
template class sf::SolverPETSc<Complex>;
template class sf::SolverPETSc<double>;

////////////////////
// Specialization //
////////////////////
template<>
PetscScalar* SolverPETSc<double>::toPetscScalar(double* a, size_t size){
  PetscScalar* petsc = new PetscScalar[size];
  for(size_t i = 0; i < size; i++)
    petsc[i] = a[i];

  return petsc;
}

template<>
PetscScalar* SolverPETSc<Complex>::toPetscScalar(Complex* a, size_t size){
  PetscScalar* petsc = new PetscScalar[size];
  for(size_t i = 0; i < size; i++)
    petsc[i] = a[i];

  return petsc;
}

template<>
void SolverPETSc<double>::copy(Vec in, fullVector<double>& out){
  PetscScalar* data;
  PetscInt     size;
  VecGetArray(in, &data);
  VecGetSize(in, &size);

  out.resize(size);
  for(PetscInt i = 0; i < size; i++)
    out(i) = PetscRealPart(data[i]);
}

template<>
void SolverPETSc<Complex>::copy(Vec in, fullVector<Complex>& out){
  PetscScalar* data;
  PetscInt     size;
  VecGetArray(in, &data);
  VecGetSize(in, &size);

  out.resize(size);
  for(PetscInt i = 0; i < size; i++)
    out(i) = data[i];
}

#endif
