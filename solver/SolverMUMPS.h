#ifndef _SOLVERMUMPS_H_
#define _SOLVERMUMPS_H_

#include "mumps_c_types.h"
#include "dmumps_c.h"
#include "zmumps_c.h"

#include "Solver.h"
#include "SmallFem.h"

/**
   @class SolverMUMPS
   @brief A Solver using the MUMPS library

   This class implements a Solver using the
   MUltifrontal Massively Parallel sparse direct %Solver (MUMPS) library.

   This library can be download at
    <a href="http://mumps.enseeiht.fr">http://mumps.enseeiht.fr</a>
*/

namespace sf{
  template<typename scalar> class SolverMUMPS: public Solver<scalar>{
  private:
    // State //
    bool hasMatrix;
    bool hasRHS;
    bool isFactorized;

    // Size //
    int nUnknown;

    // Matrix //
    int*                  row;
    int*                  col;
    scalar*               value;  // Complex AND Real case
    mumps_double_complex* valueC; // Complex case only

    // RHS //
    double*               rhsR; // Real    case
    mumps_double_complex* rhsC; // Complex case

    // MUMPS //
    DMUMPS_STRUC_C idR;
    ZMUMPS_STRUC_C idC;

  public:
    SolverMUMPS(void);
    virtual ~SolverMUMPS(void);

    virtual void solve(SolverMatrix<scalar>& A,
                       SolverVector<scalar>& rhs,
                       fullVector<scalar>& x);

    virtual void setMatrix(SolverMatrix<scalar>& A);
    virtual void setRHS(SolverVector<scalar>& rhs);

    virtual void factorize(void);
    virtual void solve(fullVector<scalar>& x);

    virtual void setOption(std::string option,
                           std::string value);

  private:
    void freeMatrix(void);
    void freeRHS(void);

    static void copy(double*               data, double**               out,
                     size_t size);
    static void copy(Complex*              data, mumps_double_complex** out,
                     size_t size);
    static void copy(double*               data, fullVector<double>&    out,
                     size_t size);
    static void copy(mumps_double_complex* data, fullVector<Complex>&   out,
                     size_t size);

    void setBLR(double tol);
    void setVerbosity(int lvl);
  };
}

/**
   @fn SolverMUMPS::SolverMUMPS
   Instanciates a new SolverMUMPS
   **

   @fn SolverMUMPS::~SolverMUMPS
   Deletes this SolverMUMPS
*/

#endif
