#ifndef _TRANSFORMATIONSHELLRECTANGULAR_H_
#define _TRANSFORMATIONSHELLRECTANGULAR_H_

#include "TransformationShell.h"

/**
   @class TransformationShellRectangular
   @brief Rectangular TransformationShell

   Rectangular TransformationShell
*/

namespace sf{
  class TransformationShellRectangular: public TransformationShell{
  public:
    TransformationShellRectangular(const GroupOfElement& geo,
                                   double Rin, double Rout, int direction,
                                   const std::vector<double>& center,
                                   int power=1,double overL=0,bool is2D=false);
    virtual ~TransformationShellRectangular(void);

  protected:
    virtual double   r(double X[3])           const;
    virtual double dxR(double X[3], double R) const;
    virtual double dyR(double X[3], double R) const;
    virtual double dzR(double X[3], double R) const;

  private:
    static double sign(double a);
  };
}

/**
   @fn TransformationShellRectangular::TransformationShellRectangular
   @param direction The axis along which the transform applies (0=X; 1=Y; 2=Z)

   Instantiates a new TransformationShellRectangular with the given parameters.
   **

   @fn TransformationShellRectangular::~TransformationShellRectangular
   Deletes this TransformationShellRectangular
*/

#endif
