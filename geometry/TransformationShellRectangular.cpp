#include "TransformationShellRectangular.h"

using namespace sf;
using namespace std;

TransformationShellRectangular::
TransformationShellRectangular(const GroupOfElement& geo,
                               double Rin, double Rout, int direction,
                               const vector<double>& center,
                               int power, double overL, bool is2D):
  TransformationShell(geo, Rin, Rout, direction, center, power, overL, is2D){
}

TransformationShellRectangular::~TransformationShellRectangular(void){
}


double TransformationShellRectangular::r(double X[3]) const{
  return std::abs(X[d]-C[d]);
}

double TransformationShellRectangular::dxR(double X[3], double R) const{
  return maskX[d]*sign(R);
}

double TransformationShellRectangular::dyR(double X[3], double R) const{
  return maskY[d]*sign(R);
}

double TransformationShellRectangular::dzR(double X[3], double R) const{
  return maskZ[d]*sign(R);
}

double TransformationShellRectangular::sign(double a){
  if(a < 0)      return -1.0;
  else if(a > 0) return +1.0;
  else           return  0.0;
}
