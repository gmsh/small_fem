#ifndef _GEOHELPER_H_
#define _GEOHELPER_H_

#include "gmsh/MElement.h"
#include "gmsh/fullMatrix.h"
#include "GroupOfElement.h"
#include "FunctionSpace0Form.h"

/**
   @class GeoHelper
   @brief Helper functions for geometry related topics.

   Helper functions for geometry related topics (e.g. tangent, normal).
*/

namespace sf{
  template<typename scalar> class GeoHelper{
  public:
     GeoHelper(void);
    ~GeoHelper(void);

    static fullVector<scalar> tangent(const MElement& element,
                                     fullVector<double>& xyz);
    static void mat3by3InvertInPlace(fullMatrix<scalar>& A, scalar det);

    static void deformation(const GroupOfElement& goe,
                            const FunctionSpace0Form& fsX,
                            const FunctionSpace0Form& fsY,
                            const FunctionSpace0Form& fsZ,
                            const std::map<Dof, scalar>& dofX,
                            const std::map<Dof, scalar>& dofY,
                            const std::map<Dof, scalar>& dofZ,
                            double mult,
                            Mesh& mesh,
                            int alignMaxWith = 0,
                            double angle = 0);

  private:
    static void gatherDofCoef(const GroupOfElement& goe,
                              const FunctionSpace0Form& fs,
                              const std::map<Dof, scalar>& dof,
                              std::vector<std::vector<scalar> >& coef);
    static void toLagrange(const GroupOfElement& goe,
                           const FunctionSpace0Form& fs,
                           const std::vector<std::vector<scalar> >& coefD,
                           std::vector<std::vector<scalar> >& coefL);
  };
}

/**
   @fn GeoHelper::GeoHelper
   Instantiates a new GeoHelper
   (unneeded since GeoHelper got only class methods)
   **

   @fn GeoHelper::~GeoHelper
   Deletes this GeoHelper
   **

   @fn GeoHelper::tangent
   @param element An MElement
   @param xyz The physical coordinate of a point of element
   @return Returns the unit tangent vector of this element evaluated at the given coordinate
   @note The MElement is assumed to a be a line
   **

   @fn GeoHelper::mat3by3InvertInPlace
   @param A A 3 by 3 matrix
   @param det The determinant of this 3 by 3 matrix

   Inverts (in place) the given 3 by 3 matrix
   **

   @fn GeoHelper::deformation
   @param goe A GroupOfElement
   @param fsX A FunctionSpace0Form
   @param fsY A FunctionSpace0Form
   @param fsZ A FunctionSpace0Form
   @param dofX A map<Dof, scalar>
   @param dofY A map<Dof, scalar>
   @param dofZ A map<Dof, scalar>
   @param mult A real number
   @param mesh A Mesh
   @param alignMaxWith An integer between 0 (default) and 3
   @param angle An angle in degree

   Let fX(x,y,z) (resp. fY, fZ) be the field defined by fsX (resp. fsY, fsZ) and
   dofX (resp. dofY, dofZ) on the given GroupOfElement.
   And let f be the vector field f(x,y,z) = [fX(x,y,z), fY(x,y,z), fZ(x,y,z)].
   This method deforms the given Mesh according to the vector field f/||f||*mult

   If alignMaxWith != 0,
   the deformation field is rotated along the given axis (1:x, 2:y, 3:z),
   such its maximum is aligned with the 'next' axis (1:y, 2:z, 3:x).
   If angle != 0, the solution is rotated further by the given amount.

   @note If dof is a Complex map, the real parts of fX, fY and fZ are used.
   **
 */

#endif
