#ifndef _TRANSFORMATION_H_
#define _TRANSFORMATION_H_

#include <vector>
#include "gmsh/MElement.h"
#include "gmsh/fullMatrix.h"

/**
   @intreface Transformation
   @brief Base interface for geometrical transformations

   This is the base interface for a geometrical transformation of an element.
*/

namespace sf{
  class Transformation{
  public:
    virtual ~Transformation(void);
    virtual double getJacobianOfTransformation(const MElement& element,
                                               double a, double b, double c,
                                               fullMatrix<double>& jac) const=0;
  };
}

/**
   @fn Transformation::~Transformation
   Deletes this Transformation
   **

   @fn Transformation::getJacobianOfTransformation
   @param element A MElement
   @param a The 'A' coordinate of a point in the ABC space of the given element
   @param b The 'B' coordinate of a point in the ABC space of the given element
   @param c The 'C' coordinate of a point in the ABC space of the given element
   @param jac A 3 by 3 allocated fullMatrix

   Fills the given matrix with the jacobian, evaluated at (a, b, c),
   of the mapping between the ABC space and the XYZ space

   @return Returns the determinant of the jacobian matrix
 */

#endif
