#ifndef _TRANSFORMATIONSHELLCYLINDRICAL_H_
#define _TRANSFORMATIONSHELLCYLINDRICAL_H_

#include "TransformationShell.h"

/**
   @class TransformationShellCylindrical
   @brief Cylindrical TransformationShell

   Cylindrical TransformationShell
*/

namespace sf{
  class TransformationShellCylindrical: public TransformationShell{
  private:
    int d1;
    int d2;

  public:
    TransformationShellCylindrical(const GroupOfElement& geo,
                                   double Rin, double Rout, int direction,
                                   const std::vector<double>& center,
                                   int power=1,double overL=0,bool is2D=false);
    virtual ~TransformationShellCylindrical(void);

  protected:
    virtual double   r(double X[3])           const;
    virtual double dxR(double X[3], double R) const;
    virtual double dyR(double X[3], double R) const;
    virtual double dzR(double X[3], double R) const;
  };
}

/**
   @fn TransformationShellCylindrical::TransformationShellCylindrical
   @param direction The normal defining the cylindrical shell used by the transform (0=X; 1=Y; 2=Z)

   Instantiates a new TransformationShellCylindrical with the given parameters.
   **

   @fn TransformationShellCylindrical::~TransformationShellCylindrical
   Deletes this TransformationShellCylindrical
*/

#endif
