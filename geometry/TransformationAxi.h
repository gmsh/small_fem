#ifndef _TRANSFORMATIONAXI_H_
#define _TRANSFORMATIONAXI_H_

#include <string>
#include "Transformation.h"

/**
   @class TransformationAxi
   @brief Axisymmetrical geometrical Transformation.

   Axisymmetrical geometrical Transformation.
   The Transformation is defined in the 'XY' plane, where 'X' is the radius.
   In order to avoid singularities near R = 0, no transfomration is applied if
   |R| is bellow a given threshold.

*/

namespace sf{
  class TransformationAxi: public Transformation{
  private:
    double RThreshold;

  public:
    TransformationAxi(double RThreshold);
    TransformationAxi(void);
    virtual ~TransformationAxi(void);

    virtual double getJacobianOfTransformation(const MElement& element,
                                               double a, double b, double c,
                                               fullMatrix<double>& jac) const;
  };
}

/**
   @fn TransformationAxi::TransformationAxi(double)
   @param RThreshold a double

   Instantiates a new TransformationAxi with the given
   radius threshold (bellow which no transformation is applied,
   in order to avoid singularities near |R| = 0).
   **

   @fn TransformationAxi::TransformationAxi(void)
   Same as TransformationAxi::TransformationAxi(1e-12)
   **

   @fn TransformationAxi::~TransformationAxi
   Deletes this TransformationAxi
*/

#endif
