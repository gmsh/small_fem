#ifndef _TRANSFORMATIONSHELL_H_
#define _TRANSFORMATIONSHELL_H_

#include "Transformation.h"
#include "GroupOfElement.h"

/**
   @interface TransformationShell
   @brief Interface for shell Transformation%s

   Interface for shell Transformation%s, as discussed in
   Henrotte et al.,
   "Finite element modelling with transformation techniques",
   IEEE transaction on magnetics, no. 3, vol. 35, pp. 1434-1437 (1999)
   [DOI: 10.1109/20.767235].

   In case of a 2D mapping, 'XY' is assumed to be the working plane.
*/

namespace sf{
  class TransformationShell: public Transformation{
  protected:
    static const int maskX[3];
    static const int maskY[3];
    static const int maskZ[3];

  protected:
    std::set<const MElement*, MElementPtrLessThan> process;
    double              Rin;
    double              Rout;
    int                 d;    // Direction
    std::vector<double> C;    // Center
    int                 p;    // Power
    double              is2D;

  public:
    TransformationShell(const GroupOfElement& geo,
                        double Rin, double Rout, int direction,
                        const std::vector<double>& center,
                        int power=1, double overL=0, bool is2D=false);

    virtual ~TransformationShell(void);

    virtual double getJacobianOfTransformation(const MElement& element,
                                               double a, double b, double c,
                                               fullMatrix<double>& jac) const;

  protected:
    virtual double   r(double X[3])           const = 0;
    virtual double dxR(double X[3], double R) const = 0;
    virtual double dyR(double X[3], double R) const = 0;
    virtual double dzR(double X[3], double R) const = 0;
  };
}

/**
   @fn TransformationShell::TransformationShell
   @param geo A GroupOfElement defining the region to transform
   @param Rin The inner radius of the shell transformation
   @param Rout The outer radius of the shell transformation
   @param direction The direction the transformation (the precise meaning being defined by the implementing class)
   @param center A vector defining the coordinate of the transformation center
   @param power The power of the shell transformation
   @param overL Inverse value of the shell transformation at R=Rout (if overL=0 then the transformation is infinite at R=Rout)
   @param is2D A boolean indicating if the transformation is 2D (true) or 3D (false)

   Instantiates a new TransformationShell with the given parameters.
   **

   @fn TransformationShell::~TransformationShell
   Deletes this TransformationShell
*/

#endif
