#include <iostream>

#include "TransformationAxi.h"
#include "ReferenceSpaceManager.h"

using namespace std;
using namespace sf;

TransformationAxi::TransformationAxi(double RThreshold){
  this->RThreshold = RThreshold;
}

TransformationAxi::TransformationAxi(void){
  this->RThreshold = 1e-12;
}

TransformationAxi::~TransformationAxi(void){
}

double
TransformationAxi::getJacobianOfTransformation(const MElement& element,
                                               double a, double b, double c,
                                               fullMatrix<double>& jac) const{
  // Radius in physical space
  double R;
  double tmp[3];
  ReferenceSpaceManager::mapFromABCtoXYZ(element, a, b, c, tmp);
  R = tmp[0]; // R = X

  // No transformation if R is too close to 0
  if(std::abs(R) < RThreshold)
    R = 1;

  // Jacobian
  jac.setAll(0);
  jac(0, 0) = 1;
  jac(1, 1) = 1;
  jac(2, 2) = R;

  // Return determinant
  return R;
}
