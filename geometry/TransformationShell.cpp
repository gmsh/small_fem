#include "TransformationShell.h"
#include "Exception.h"

using namespace sf;
using namespace std;

const int TransformationShell::maskX[3] = {1, 0, 0};
const int TransformationShell::maskY[3] = {0, 1, 0};
const int TransformationShell::maskZ[3] = {0, 0, 1};


TransformationShell::TransformationShell(const GroupOfElement& geo,
                                         double Rin, double Rout, int direction,
                                         const vector<double>& center,
                                         int power, double overL, bool is2D){
  // Validity //
  if(direction < 0 || direction > 2)
    throw Exception("TransformationShell: "
                    "direction must be 0(X), 1(Y) or 2(Z)");

  // Store elements to process //
  const vector<const MElement*>& element = geo.getAll();
  process.insert(element.begin(), element.end());

  // Store inner radius, direction, center, power and is2d //
  this->Rin  = Rin;
  this->d    = direction;
  this->C    = center;
  this->p    = power;
  this->is2D = is2D;

  // Outer radius (with possible correction for a finite mapping at R=Rout) //
  if(overL == 0)
    this->Rout = Rout;
  else
    this->Rout = (Rout*Rout - Rin*Rin*overL) / (Rout - Rin*overL);
}

TransformationShell::~TransformationShell(void){
}

double TransformationShell::
getJacobianOfTransformation(const MElement& element,
                            double a, double b, double c,
                            fullMatrix<double>& jac) const{
  // Make sure jac is 0 //
  double det = 0;
  jac.setAll(0);

  // Check if element must be transformed //
  if(process.count(&element)){              // If yes, apply transformation
    double X[3];                            // --> Physical coordinates
    ReferenceSpaceManager::                 //     ...
      mapFromABCtoXYZ(element, a, b, c, X); //     ...
                                            //     ...
    double R = r(X);                        // --> Radius
    if(R == Rout)                           // --> Avoid singularity if R=Rout
      R = R - Rout/1e9;                     //     (one could use an identity
                                            //      mapping too, as in GetDP)
    double dRdx = dxR(X, R);                // --> X-derivative
    double dRdy = dyR(X, R);                // --> Y-derivative
    double dRdz = dzR(X, R);                // --> Z-derivative
                                            //     ...
    double F    = pow((Rin*(Rout-Rin))/     // --> F factor
                      (R  *(Rout-R)), p);   //     ...
    double T    = p*(Rout-2*R)/(Rout-R);    // --> Theta factor
                                            //     ...
    double nX   = (X[0]-C[0])/R;            // --> nX factor
    double nY   = (X[1]-C[1])/R;            // --> nY factor
    double nZ   = (X[2]-C[2])/R;            // --> nZ factor
                                            //     ...
    jac(0, 0) = F * (1 - T * nX * dRdx);    // --> Jacobian of transformation
    jac(0, 1) = F * (  - T * nX * dRdy);    //     ...
    jac(0, 2) = F * (  - T * nX * dRdz);    //     ...
    jac(1, 0) = F * (  - T * nY * dRdx);    //     ...
    jac(1, 1) = F * (1 - T * nY * dRdy);    //     ...
    jac(1, 2) = F * (  - T * nY * dRdz);    //     ...
    jac(2, 0) = F * (  - T * nZ * dRdx);    //     ...
    jac(2, 1) = F * (  - T * nZ * dRdy);    //     ...
    jac(2, 2) = F * (1 - T * nZ * dRdz);    //     ...
                                            //     ...
    if(is2D){                               // --> If is 2D:
      jac(2, 2) = 1;                        //     ~~] don't map along z
      det       = F * F * (1-T);            //     ~~] compute determinant
    }                                       //     ...
    else{                                   // --> Otherwise:
      det       = F * F * F * (1-T);        //     ~~] compute determinant
    }                                       //     ...
  }                                         //     ...
  else{                                     // Otherwise, do nothing
    jac(0, 0) = 1;                          // --> Identity transformation
    jac(1, 1) = 1;                          //     ...
    jac(2, 2) = 1;                          //     ...
    det       = 1;                          //     ...
  }

  // Done: return determinant //
  return det;
}
