#include "GroupOfJacobian.h"
#include "Exception.h"

using namespace std;
using namespace sf;

const string GroupOfJacobian::jacobianString = string("jacobian");
const string GroupOfJacobian::invertString   = string("invert");
const string GroupOfJacobian::bothString     = string("both");

GroupOfJacobian::GroupOfJacobian(const GroupOfElement& goe,
                                 const fullMatrix<double>& point,
                                 const string type){
  // Save //
  this->goe   = &goe;
  this->point = &point;
  this->type  = type;

  // Clear //
  jacInv.clear();
  jacMat.clear();

  // Compute jacobian matrices
  computeJacobian(goe.getAll(), point);

  // Optionally, compute inverse jacobians //
  if((!type.compare(bothString)) || (!type.compare(invertString)))
    computeInvert();
  else if(!type.compare(jacobianString))
    ; // Do nothing, jacobians are already computed
  else
    throw Exception("Unknown Jacobian type: %s (Types are '%s', '%s' and '%s')",
                    type.c_str(), jacobianString.c_str(),
                    invertString.c_str(), bothString.c_str());

  // Optionally, delete unneeded stuff //
  if((!type.compare(invertString)))
    jacMat.clear(); // Jacobians are not needed anymore (but keep inverts)
}

GroupOfJacobian::~GroupOfJacobian(void){
}

void GroupOfJacobian::computeJacobian(const vector<const MElement*>& element,
                                      const fullMatrix<double>& point){
  // Size //
  const size_t nElement = element.size();

  // Alloc //
  jacMat.resize(nElement);

  // Compute //
  // Compute first jacobian outside OpenMP
  // Need to generate GMSH nodalBasis outside a thread
  // so that Vandermond matrix is not inverted
  // in nested threads (limitation of OpenBLAS)
  computeJacobian(0, *element[0], point);

  #pragma omp parallel for
  for(size_t e = 1; e < nElement; e++)
    computeJacobian(e, *element[e], point);
}

void GroupOfJacobian::computeJacobian(size_t e,
                                      const MElement& element,
                                      const fullMatrix<double>& point){
  // Init //
  const size_t nPoint = point.size1();
  jacMat[e].resize(nPoint);

  // Fill //
  for(size_t p = 0; p < nPoint; p++){
    jacMat[e][p].first.resize(3, 3, true);
    jacMat[e][p].second =
      ReferenceSpaceManager::getJacobian(element,
                                         point(p, 0), point(p, 1), point(p, 2),
                                         jacMat[e][p].first);
  }
}

void GroupOfJacobian::computeInvert(void){
  // Alloc //
  const size_t nElement = goe->getNumber();
  const size_t nPoint   = point->size1();
  jacInv.resize(nElement);

  #pragma omp parallel
  {
    #pragma omp for
    for(size_t e = 0; e < nElement; e++)
      jacInv[e].resize(nPoint);

  // Loop and invert //
    #pragma omp for
    for(size_t e = 0; e < nElement; e++){
      for(size_t p = 0; p < nPoint; p++){
        jacInv[e][p].first.resize(3, 3, true);
        naiveInvert(jacMat[e][p].first,jacMat[e][p].second, jacInv[e][p].first);
        jacInv[e][p].second = jacMat[e][p].second;
      }
    }
  }
}

const vector<pair<fullMatrix<double>, double> >&
GroupOfJacobian::getJacobianMatrix(size_t e) const{
  if(jacMat.size())
    return jacMat[e];

  else
    throw Exception
      ("This GroupOfJacobian type is %s -- can't get non inverted jacobian",
       type.c_str());
}

const vector<pair<fullMatrix<double>, double> >&
GroupOfJacobian::getInvertJacobianMatrix(size_t e) const{
  if(jacInv.size())
    return jacInv[e];

  else
    throw Exception
      ("This GroupOfJacobian type is %s -- can't get inverted jacobian",
       type.c_str());
}

void GroupOfJacobian::naiveInvert(const fullMatrix<double>& A,
                                  double detA,
                                  fullMatrix<double>& invA){
  invA(0, 0) = (A(1, 1) * A(2, 2) - A(1, 2) * A(2, 1)) / detA;
  invA(0, 1) = (A(0, 2) * A(2, 1) - A(0, 1) * A(2, 2)) / detA;
  invA(0, 2) = (A(0, 1) * A(1, 2) - A(0, 2) * A(1, 1)) / detA;

  invA(1, 0) = (A(1, 2) * A(2, 0) - A(1, 0) * A(2, 2)) / detA;
  invA(1, 1) = (A(0, 0) * A(2, 2) - A(0, 2) * A(2, 0)) / detA;
  invA(1, 2) = (A(0, 2) * A(1, 0) - A(0, 0) * A(1, 2)) / detA;

  invA(2, 0) = (A(1, 0) * A(2, 1) - A(1, 1) * A(2, 0)) / detA;
  invA(2, 1) = (A(0, 1) * A(2, 0) - A(0, 0) * A(2, 1)) / detA;
  invA(2, 2) = (A(0, 0) * A(1, 1) - A(0, 1) * A(1, 0)) / detA;
}
