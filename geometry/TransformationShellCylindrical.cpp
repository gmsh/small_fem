#include "TransformationShellCylindrical.h"

using namespace sf;
using namespace std;

TransformationShellCylindrical::
TransformationShellCylindrical(const GroupOfElement& geo,
                               double Rin, double Rout, int direction,
                               const vector<double>& center,
                               int power, double overL, bool is2D):
  TransformationShell(geo, Rin, Rout, direction, center, power, overL, is2D){
  d1 = (d+1)%3;
  d2 = (d+2)%3;
}

TransformationShellCylindrical::~TransformationShellCylindrical(void){
}


double TransformationShellCylindrical::r(double X[3]) const{
  return sqrt((X[d1]-C[d1])*(X[d1]-C[d1]) + (X[d2]-C[d2])*(X[d2]-C[d2]));
}

double TransformationShellCylindrical::dxR(double X[3], double R) const{
  if(maskX[d1] == 1 || maskX[d2] == 1)
    return (X[0] - C[0]) / R;
  else
    return 0.0;
}

double TransformationShellCylindrical::dyR(double X[3], double R) const{
  if(maskY[d1] == 1 || maskY[d2] == 1)
    return (X[1] - C[1]) / R;
  else
    return 0.0;
}

double TransformationShellCylindrical::dzR(double X[3], double R) const{
  if(maskZ[d1] == 1 || maskZ[d2] == 1)
    return (X[2] - C[2]) / R;
  else
    return 0.0;
}
