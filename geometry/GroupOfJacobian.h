#ifndef _GROUPOFJACOBIAN_H_
#define _GROUPOFJACOBIAN_H_

#include <string>
#include "gmsh/fullMatrix.h"
#include "GroupOfElement.h"

/**
   @class GroupOfJacobian
   @brief A set of Jacobian matrices

   A set of Jacobian matrices defined over a GroupOfElement
   and a set of points in the reference space.
   The Jacobian matrix is defined as:

   @f$J~=~\left(
   \begin{array}{ccc}
   \displaystyle\frac{\partial{}x}{\partial{}u} &
   \displaystyle\frac{\partial{}y}{\partial{}u} &
   \displaystyle\frac{\partial{}z}{\partial{}u}\\
   \displaystyle\frac{\partial{}x}{\partial{}v} &
   \displaystyle\frac{\partial{}y}{\partial{}v} &
   \displaystyle\frac{\partial{}z}{\partial{}v}\\
   \displaystyle\frac{\partial{}x}{\partial{}w} &
   \displaystyle\frac{\partial{}y}{\partial{}w} &
   \displaystyle\frac{\partial{}z}{\partial{}w}\\
   \end{array}
   \right)@f$
 */

namespace sf{
  class GroupOfJacobian{
  private:
    static const std::string jacobianString;
    static const std::string invertString;
    static const std::string bothString;

  private:
    typedef std::pair<fullMatrix<double>, double> jac_pair;
    typedef std::vector<jac_pair>                 jac_t;

  private:
    const GroupOfElement*       goe;
    const fullMatrix<double>* point;
    std::string                type;
    std::vector<jac_t>       jacMat;
    std::vector<jac_t>       jacInv;

  public:
    GroupOfJacobian(const GroupOfElement& goe,
                    const fullMatrix<double>& point,
                    const std::string type);
   ~GroupOfJacobian(void);

    const std::vector<std::pair<fullMatrix<double>, double> >&
    getJacobianMatrix(size_t e) const;
    const std::vector<std::pair<fullMatrix<double>, double> >&
    getInvertJacobianMatrix(size_t e) const;

    const GroupOfElement&     getAllElements(void) const;
    const fullMatrix<double>& getAllPoints(void)   const;

  private:
    void computeJacobian(const std::vector<const MElement*>& element,
                         const fullMatrix<double>& point);
    void computeJacobian(size_t e,
                         const MElement& element,
                         const fullMatrix<double>& point);
    void computeInvert(void);

    static void naiveInvert(const fullMatrix<double>& A,
                            double detA,
                            fullMatrix<double>& invA);
  };
}

/**
   @fn GroupOfJacobian::GroupOfJacobian
   @param goe A group of element (GroupOfElement)
   @param point A [ N x 3 ] matrix (a set of N 3D-points in the reference space)
   @param type A string

   Instantiate a GroupOfJacobian.
   This group will contain the Jacobian matrices
   of the elements of the given GroupOfElement, computed at the given points,
   and for the given type.

   The input string type can take the following values:
   @li jacobian, to compute the jacobian matrices
   @li invert, to comupte the inverted jacobian matrices
   @li both, to compute both inverted and non inverted jacobian matrices
   **

   @fn GroupOfJacobian::~GroupOfJacobian
   Deletes this GroupOfJacobian
   **

   @fn GroupOfJacobian::getJacobianMatrix
   @param e An integer
   @note In what follows, let e be an index of an element in the GroupOfElement
   defining this GroupOfJacobian,
   and let p be an index of a point defining this GroupOfJacobian.
   @return Returns a vector of pairs v, such that:
   @li v[p].first is the Jacobian matrix
   of the eth element evalutated at the pth point
   @li v[p].second is the determinant of the Jacobian matrix
   of the eth element evalutated at the pth point
   **

   @fn GroupOfJacobian::getInvertJacobianMatrix
   @param e An integer
   @note In what follows, let e be an index of an element in the GroupOfElement
   defining this GroupOfJacobian,
   and let p be an index of a point defining this GroupOfJacobian.
   @return Returns a vector of pairs v, such that:
   @li v[p].first is the inverted Jacobian matrix
   of the eth element evalutated at the pth point
   @li v[p].second is the determinant of the (non-inverted) Jacobian matrix
   of the eth element evalutated at the pth point
   **

   @fn GroupOfJacobian::getAllElements
   @return Returns the GroupOfElement defining this GroupOfJacobian
   **

   @fn GroupOfJacobian::getAllPoints
   @return Returns the points (in the reference space coordinate system)
   defining this GroupOfJacobian
 */

/////////////////////
// Inline Function //
/////////////////////

inline const sf::GroupOfElement&
sf::GroupOfJacobian::getAllElements(void) const{
  return *goe;
}

inline const fullMatrix<double>&
sf::GroupOfJacobian::getAllPoints(void) const{
  return *point;
}

#endif
