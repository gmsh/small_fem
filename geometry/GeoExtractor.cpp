#include "GeoExtractor.h"

using namespace std;
using namespace sf;

GeoExtractor::GeoExtractor(void){
}

GeoExtractor::~GeoExtractor(void){
}

void GeoExtractor::elementExtract(const map<int, vector<GEntity*> >& group,
                                  MapElement& element,
                                  multimap<int, const MElement*>& physical){
  // Iterator on groups
  map<int, vector<GEntity*> >::const_iterator it  = group.begin();
  map<int, vector<GEntity*> >::const_iterator end = group.end();

  for(; it != end; it++){
    // Current vector of GEntity and physical tag (for code clarity)
    int                     myPhysical = it->first;
    const vector<GEntity*>& myEntity   = it->second;

    // Iterate on entities in this group
    size_t nEntity = myEntity.size();
    for(size_t i = 0; i < nEntity; i++){
      // Iterate on elements in this entity and insert
      size_t nElement = myEntity[i]->getNumMeshElements();
      for(size_t j = 0; j < nElement; j++){
        element.insert(myEntity[i]->getMeshElement(j));
        physical.insert(pair<int, const MElement*>(myPhysical,
                                                   myEntity[i]->getMeshElement(j)));
      }
    }
  }
}

void GeoExtractor::vertexExtract(const MapElement& element, MapVertex& vertex){
  // Iterate on Elements
  MapElement::const_iterator end = element.end();
  MapElement::const_iterator  it = element.begin();

  for(; it != end; it++){
    // Get Current Element
    MElement* myElement = const_cast<MElement*>(it->first);

    // Iterate on Vertices
    const size_t N = myElement->getNumVertices();
    for(size_t i = 0; i < N; i++){
      // Take Current Vertex
      MVertex* myVertex = myElement->getVertex(i);

      // Insert
      vertex.insert(myVertex);
    }
  }
}

void GeoExtractor::edgeExtract(const MapElement& element, MapEntity& edge){
  // Iterate on Elements
  MapElement::const_iterator end = element.end();
  MapElement::const_iterator  it = element.begin();

  for(; it != end; it++){
    // Get Current Element
    MElement* myElement = const_cast<MElement*>(it->first);

    // Iterate on Edges
    const size_t N = myElement->getNumEdges();

    for(size_t i = 0; i < N; i++){
      // Take Current Edge
      const MEdge myEdge = myElement->getEdge(i);

      // Get Vertex Global ID
      vector<int> myVertex(2);
      myVertex[0] = myEdge.getVertex(0)->getNum();
      myVertex[1] = myEdge.getVertex(1)->getNum();

      // Try to Insert
      edge.insert(myVertex);
    }
  }
}

void GeoExtractor::faceExtract(const MapElement& element, MapEntity& face){
  // Iterate on Elements
  MapElement::const_iterator end = element.end();
  MapElement::const_iterator  it = element.begin();

  // Iterate on Elements
  for(; it != end; it++){
    // Get Current Element
    MElement* myElement = const_cast<MElement*>(it->first);

    // Iterate on Faces
    const size_t N = myElement->getNumFaces();

    for(size_t i = 0; i < N; i++){
      // Take Current Face
      const MFace myFace = myElement->getFace(i);

      // Get Vertex Global ID
      const int    nVertex = myFace.getNumVertices();
      vector<int> myVertex(nVertex);

      for(int i = 0; i < nVertex; i++)
        myVertex[i] = myFace.getVertex(i)->getNum();

      // Try to Insert
      face.insert(myVertex);
    }
  }
}
