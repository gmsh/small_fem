#include "SmallFem.h"
#include "GeoHelper.h"

#include "BasisGenerator.h"
#include "BasisLagrange.h"

using namespace sf;
using namespace std;

template<typename scalar>
GeoHelper<scalar>::GeoHelper(void){
}

template<typename scalar>
GeoHelper<scalar>::~GeoHelper(void){
}

template<typename scalar>
fullVector<scalar> GeoHelper<scalar>::tangent(const MElement& element,
                                              fullVector<double>& xyz){
  // Get local coordinates //
  double uvw[3];
  element.xyz2uvw(xyz.getDataPtr(), uvw);

  // Get jacobian matrix at uvw //
  fullMatrix<double> jac(3, 3);
  jac.scale(0);
  element.getJacobian(uvw[0], uvw[1], uvw[2], jac);

  // Get jacobian first column (tangent vector in xyz) //
  fullVector<scalar> t(3);
  t(0) = jac(0, 0);
  t(1) = jac(0, 1);
  t(2) = jac(0, 2);

  // Normalize
  double norm = sqrt(jac(0, 0)*jac(0, 0) +
                     jac(0, 1)*jac(0, 1) +
                     jac(0, 2)*jac(0, 2));
  t(0) = t(0) / norm;
  t(1) = t(1) / norm;
  t(2) = t(2) / norm;

  // Done //
  return t;
}

template<typename scalar>
void GeoHelper<scalar>::mat3by3InvertInPlace(fullMatrix<scalar>& A,
                                             scalar det){
  scalar inv00 = (A(1, 1) * A(2, 2) - A(1, 2) * A(2, 1)) / det;
  scalar inv01 = (A(0, 2) * A(2, 1) - A(0, 1) * A(2, 2)) / det;
  scalar inv02 = (A(0, 1) * A(1, 2) - A(0, 2) * A(1, 1)) / det;

  scalar inv10 = (A(1, 2) * A(2, 0) - A(1, 0) * A(2, 2)) / det;
  scalar inv11 = (A(0, 0) * A(2, 2) - A(0, 2) * A(2, 0)) / det;
  scalar inv12 = (A(0, 2) * A(1, 0) - A(0, 0) * A(1, 2)) / det;

  scalar inv20 = (A(1, 0) * A(2, 1) - A(1, 1) * A(2, 0)) / det;
  scalar inv21 = (A(0, 1) * A(2, 0) - A(0, 0) * A(2, 1)) / det;
  scalar inv22 = (A(0, 0) * A(1, 1) - A(0, 1) * A(1, 0)) / det;

  A(0, 0) = inv00; A(0, 1) = inv01; A(0, 2) = inv02;
  A(1, 0) = inv10; A(1, 1) = inv11; A(1, 2) = inv12;
  A(2, 0) = inv20; A(2, 1) = inv21; A(2, 2) = inv22;
}

template<typename scalar>
void GeoHelper<scalar>::deformation(const GroupOfElement& goe,
                                    const FunctionSpace0Form& fsX,
                                    const FunctionSpace0Form& fsY,
                                    const FunctionSpace0Form& fsZ,
                                    const map<Dof, scalar>& dofX,
                                    const map<Dof, scalar>& dofY,
                                    const map<Dof, scalar>& dofZ,
                                    double mult,
                                    Mesh& mesh,
                                    int alignMaxWith,
                                    double angle){
  // Check alignMaxWith //
  if(alignMaxWith != 0 && alignMaxWith != 3)
    throw Exception("GeoHelper<scalar>::deformation: "
                    "alignMaxWith = {1, 2} is not (yet) implemented");

  // Check if Dofs exist in FS and associate them back to their elements //
  vector<vector<scalar> > coefDX; gatherDofCoef(goe, fsX, dofX, coefDX);
  vector<vector<scalar> > coefDY; gatherDofCoef(goe, fsY, dofY, coefDY);
  vector<vector<scalar> > coefDZ; gatherDofCoef(goe, fsZ, dofZ, coefDZ);

  // Project those coefficients in a Lagrange basis //
  vector<vector<scalar> > coefLX; toLagrange(goe, fsX, coefDX, coefLX);
  vector<vector<scalar> > coefLY; toLagrange(goe, fsY, coefDY, coefLY);
  vector<vector<scalar> > coefLZ; toLagrange(goe, fsZ, coefDZ, coefLZ);

  // Get maximal magnitude of real part and angle //
  double max = 0;
  double mAngle = 0;
  for(size_t e = 0; e < coefLX.size(); e++){
    for(size_t v = 0; v < coefLX[e].size(); v++){
      double mag = sqrt(std::real(coefLX[e][v])*std::real(coefLX[e][v]) +
                        std::real(coefLY[e][v])*std::real(coefLY[e][v]) +
                        std::real(coefLZ[e][v])*std::real(coefLZ[e][v]));
      if(mag > max){
        max    = mag;
        mAngle = atan2(std::real(coefLY[e][v]), std::real(coefLX[e][v]));
      }
    }
  }

  // Rotation //
  if(alignMaxWith == 0)
    mAngle = 0;

  double eAg = angle / 360 * 2*(atan(1)*4);
  double Rxx = +cos(-mAngle + eAg);
  double Rxy = -sin(-mAngle + eAg);
  double Ryx = +sin(-mAngle + eAg);
  double Ryy = +cos(-mAngle + eAg);

  for(size_t e = 0; e < coefLX.size(); e++){
    for(size_t v = 0; v < coefLX[e].size(); v++){
      scalar tX = coefLX[e][v];
      scalar tY = coefLY[e][v];

      coefLX[e][v] = tX*Rxx + tY*Rxy;
      coefLY[e][v] = tX*Ryx + tY*Ryy;
    }
  }

  // Get all elements //
  vector<const MElement*> element = goe.getAll();

  // Get the maximim MVertex number by looping on all elements //
  size_t vMax = 0;
  vector<MVertex*> vertex;
  for(size_t e = 0; e < element.size(); e++){
    const_cast<MElement*>(element[e])->getVertices(vertex); // I know: ugly :(
    for(size_t v = 0; v < vertex.size(); v++)
      if(vertex[v]->getNum() > vMax)
        vMax = vertex[v]->getNum();
  }

  // Allocate vector to keep track of MVertex //
  vector<bool> notVisited(vMax, true);

  // Loop on every element and deform according to real(coefL) / max * mult //
  for(size_t e = 0; e < element.size(); e++){
    const_cast<MElement*>(element[e])->getVertices(vertex); // I know: ugly :(
    for(size_t v = 0; v < vertex.size(); v++){
      if(notVisited[vertex[v]->getNum()]){
        vertex[v]->x() += std::real(coefLX[e][v]) / max * mult;
        vertex[v]->y() += std::real(coefLY[e][v]) / max * mult;
        vertex[v]->z() += std::real(coefLZ[e][v]) / max * mult;
        notVisited[vertex[v]->getNum()] = false;
      }
    }
  }
}

template<typename scalar>
void GeoHelper<scalar>::gatherDofCoef(const GroupOfElement& goe,
                                      const FunctionSpace0Form& fs,
                                      const map<Dof, scalar>& dof,
                                      vector<vector<scalar> >& coef){
  // Number of elements
  size_t nElement = goe.getNumber();

  // Get all Dofs and allocate space to store their value //
  const vector<vector<Dof> >& vDof = fs.getKeys(goe);
  coef.resize(vDof.size());
  for(size_t e = 0; e < nElement; e++)
    coef[e].resize(vDof[e].size());

  // Populate coef from dof map //
  typename std::map<Dof, scalar>::const_iterator isFound;
  typename std::map<Dof, scalar>::const_iterator notFound = dof.end();
  for(size_t e = 0; e < nElement; e++){
    for(size_t i = 0; i < vDof[e].size(); i++){
      isFound = dof.find(vDof[e][i]);
      if(isFound != notFound){
        coef[e][i] = isFound->second;
      }
      else{
        cout << "Warning: dof not found " << vDof[e][i].toString() << endl;
        coef[e][i] = 0;
      }
    }
  }
}

template<typename scalar>
void GeoHelper<scalar>::toLagrange(const GroupOfElement& goe,
                                   const FunctionSpace0Form& fs,
                                   const vector<vector<scalar> >& coefD,
                                   vector<vector<scalar> >& coefL){
  // Get all elements //
  vector<const MElement*> element = goe.getAll();

  // Lagrange Basis //
  const vector<size_t> typeStat = goe.getTypeStats();
  const size_t         nGeoType = typeStat.size();

  vector<BasisLagrange*> lagrange(nGeoType, NULL);
  for(size_t i = 0; i < nGeoType; i++)
    if(typeStat[i])
      lagrange[i] = static_cast<BasisLagrange*>
        (BasisGenerator::generate(i, 0, fs.getBasis(i).getOrder(),
                                  Basis::Family::Lagrange));
  // Initialize Lagrange coef vector and project all elements //
  coefL.resize(coefD.size());
  for(size_t e = 0; e < element.size(); e++)
    coefL[e] =
      lagrange[element[e]->getType()]->project(*element[e], coefD[e], fs);

  // Clean //
  for(size_t i = 0; i < nGeoType; i++)
    if(typeStat[i])
      delete lagrange[i];
}

////////////////////////////
// Explicit instantiation //
////////////////////////////
template class sf::GeoHelper<double>;
template class sf::GeoHelper<Complex>;
