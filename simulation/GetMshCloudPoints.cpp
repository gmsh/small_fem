// small_fem libraries
#include "SmallFem.h"
#include "System.h"
#include "Mesh.h"
// Gmsh libraries
#include "gmsh/MElement.h"
#include "gmsh/pointsGenerators.h"
//system libraries
#include <fstream>
#include <iostream>
#include <iomanip>
#include <string>


/*
 @function physSampler
 @brief    smaples a physical curve

 Option keys:
    -msh        mesh file
    -phys       tag of the physical
    -level      refinement level in each element
    -name       name of output file

 @author Ramah Sharaf
 */

using namespace std;
using namespace sf;


void compute(const Options& option){
  // onelab
  try{
    if(SmallFem::OnelabHelper::action() == "initialize")
      SmallFem::OnelabHelper::client()
        .sendOpenProjectRequest(SmallFem::OnelabHelper::name() + ".geo");

    if(SmallFem::OnelabHelper::action() != "compute")
      return;
  }
  catch(...){
  }

  // data
  string mshFileName;
  int phys;
  int level;
  try{
    mshFileName = SmallFem::OnelabHelper::name()  + ".msh";
    phys        = SmallFem::OnelabHelper::getNumber("99MeshCloud/00Physical");
    level       = SmallFem::OnelabHelper::getNumber("99MeshCloud/01Level");
  }
  catch(...){
    mshFileName = option.getValue("-msh")[1];
    phys        = atoi(option.getValue("-phys")[1].c_str());
    level       = atoi(option.getValue("-level")[1].c_str());
  }

  // get physical domain
  Mesh msh(mshFileName);
  GroupOfElement domain = msh.getFromPhysical(phys);
  const vector<const MElement*> element = domain.getAll();

  // level
  if(level < 1)
    throw Exception("Level must be greater than 0");

  // get Langrange points for all types of elements
  vector<fullMatrix<double> > sample(9);
  sample[TYPE_LIN] = gmshGeneratePointsLine(level);
  sample[TYPE_TRI] = gmshGeneratePointsTriangle(level);
  sample[TYPE_QUA] = gmshGeneratePointsQuadrangle(level);
  // missing points will just have an empty matrix...

  // get or determine file name
  size_t dot = mshFileName.find_last_of('.');
  size_t slash = mshFileName.find_last_of('/');
  const string prod = mshFileName.substr(slash+1,dot-1-slash);

  string fname = "physical-"+to_string(phys)+"_of-"+prod+".txt";
  try {
    fname = option.getValue("-name")[1];
  }
  catch(...){}
  ofstream   txtStream;

  // write points to
  txtStream.open(fname);
  txtStream.precision(16);

  size_t counter = 0;
  vector<double> Sp3(3);

  for(size_t i = 0; i < element.size();i++) {
    int type = ElementType::getPrimaryType(element[i]->getType());
    if(sample[type].size1() == 0)
      throw Exception("Unknown element type %d", type);

    double uvw[3] = {0, 0, 0};
    for(int j = 0; j < sample[type].size1(); j++) {
      for(int k = 0; k < sample[type].size2(); k++)
        uvw[k] = sample[type](j, k);

      element[i]->pnt(uvw[0], uvw[1], uvw[2], Sp3.data());
      txtStream  << scientific << Sp3[0] << " "
                               << Sp3[1] << " "
                               << Sp3[2] << endl;
      counter++;
    }
  }

  cout << "Wrote: " << counter << " points to "
       << fname << endl << flush;
}

int main(int argc, char** argv){
  SmallFem::Keywords("-msh,-phys,-level,-name");
  SmallFem::getOptions();
  SmallFem::Initialize(argc, argv);
  compute(SmallFem::getOptions());
  SmallFem::Finalize();
  return 0;
}
