#ifndef _HAROCHEHELPER_H_
#define _HAROCHEHELPER_H_

#include <string>
#include "gmsh/fullMatrix.h"
#include "SmallFem.h"

/**
   @class Haroche Helper
   @brief Helping stuff for simulation/Haroche

   Helping stuff for simulation/Haroche
 */

// Material //
class Material{
 public:
  static const double cSquare;

  class Air{
  public:
    static void Epsilon(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void MuEps(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void OverMuEps(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
  };

  class XYZ{
  public:
    static void Epsilon(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void MuEps(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void OverMuEps(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
  };

  class XZ{
  public:
    static void Epsilon(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void MuEps(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void OverMuEps(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
  };

  class YZ{
  public:
    static void Epsilon(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void MuEps(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void OverMuEps(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
  };

  class XY{
  public:
    static void Epsilon(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void MuEps(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void OverMuEps(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
  };

  class X{
  public:
    static void Epsilon(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void MuEps(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void OverMuEps(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
  };

  class Y{
  public:
    static void Epsilon(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void MuEps(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void OverMuEps(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
  };

  class Z{
  public:
    static void Epsilon(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void MuEps(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void OverMuEps(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
  };
};

// PML //
class PML{
 private:
  static const sf::Complex a;
  static const sf::Complex one;

  static double Xmax;
  static double Ymax;
  static double Zmax;
  static double SizeX;
  static double SizeY;
  static double SizeZ;
  static double kHaroche;

 public:
  static void   read(std::string filename);
  static double getK(void);

 private:
  static sf::Complex dampingX(fullVector<double>& xyz);
  static sf::Complex dampingY(fullVector<double>& xyz);
  static sf::Complex dampingZ(fullVector<double>& xyz);

 public:
  class Air{
  public:
    static sf::Complex sx(fullVector<double>& xyz);
    static sf::Complex sy(fullVector<double>& xyz);
    static sf::Complex sz(fullVector<double>& xyz);

    static sf::Complex Lxx(fullVector<double>& xyz);
    static sf::Complex Lyy(fullVector<double>& xyz);
    static sf::Complex Lzz(fullVector<double>& xyz);
  };

  class XYZ{
  public:
    static sf::Complex sx(fullVector<double>& xyz);
    static sf::Complex sy(fullVector<double>& xyz);
    static sf::Complex sz(fullVector<double>& xyz);

    static sf::Complex Lxx(fullVector<double>& xyz);
    static sf::Complex Lyy(fullVector<double>& xyz);
    static sf::Complex Lzz(fullVector<double>& xyz);
  };

  class XZ{
  public:
    static sf::Complex sx(fullVector<double>& xyz);
    static sf::Complex sy(fullVector<double>& xyz);
    static sf::Complex sz(fullVector<double>& xyz);

    static sf::Complex Lxx(fullVector<double>& xyz);
    static sf::Complex Lyy(fullVector<double>& xyz);
    static sf::Complex Lzz(fullVector<double>& xyz);
  };

  class YZ{
  public:
    static sf::Complex sx(fullVector<double>& xyz);
    static sf::Complex sy(fullVector<double>& xyz);
    static sf::Complex sz(fullVector<double>& xyz);

    static sf::Complex Lxx(fullVector<double>& xyz);
    static sf::Complex Lyy(fullVector<double>& xyz);
    static sf::Complex Lzz(fullVector<double>& xyz);
  };

  class XY{
  public:
    static sf::Complex sx(fullVector<double>& xyz);
    static sf::Complex sy(fullVector<double>& xyz);
    static sf::Complex sz(fullVector<double>& xyz);

    static sf::Complex Lxx(fullVector<double>& xyz);
    static sf::Complex Lyy(fullVector<double>& xyz);
    static sf::Complex Lzz(fullVector<double>& xyz);
  };

  class X{
  public:
    static sf::Complex sx(fullVector<double>& xyz);
    static sf::Complex sy(fullVector<double>& xyz);
    static sf::Complex sz(fullVector<double>& xyz);

    static sf::Complex Lxx(fullVector<double>& xyz);
    static sf::Complex Lyy(fullVector<double>& xyz);
    static sf::Complex Lzz(fullVector<double>& xyz);
  };

  class Y{
  public:
    static sf::Complex sx(fullVector<double>& xyz);
    static sf::Complex sy(fullVector<double>& xyz);
    static sf::Complex sz(fullVector<double>& xyz);

    static sf::Complex Lxx(fullVector<double>& xyz);
    static sf::Complex Lyy(fullVector<double>& xyz);
    static sf::Complex Lzz(fullVector<double>& xyz);
  };

  class Z{
  public:
    static sf::Complex sx(fullVector<double>& xyz);
    static sf::Complex sy(fullVector<double>& xyz);
    static sf::Complex sz(fullVector<double>& xyz);

    static sf::Complex Lxx(fullVector<double>& xyz);
    static sf::Complex Lyy(fullVector<double>& xyz);
    static sf::Complex Lzz(fullVector<double>& xyz);
  };
};

#endif
