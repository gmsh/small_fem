#ifndef _BOUBOUCHONSHELPER_H_
#define _BOUBOUCHONSHELPER_H_

#include "gmsh/fullMatrix.h"
#include "SmallFem.h"

/**
   @class Boubouchons Helper
   @brief Helping stuff for simulation/Boubouchons

   Helping stuff for simulation/Boubouchons
 */

// Material //
class Material{
 public:
  class XYZ{
  public:
    static void Eps(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
  };

  class XZ{
  public:
    static void Eps(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
  };

  class YZ{
  public:
    static void Eps(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
  };

  class XY{
  public:
    static void Eps(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
  };

  class X{
  public:
    static void Eps(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
  };

  class Y{
  public:
    static void Eps(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
  };

  class Z{
  public:
    static void Eps(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
  };
};

// PML //
class PML{
 private:
  static const sf::Complex a;
  static const sf::Complex one;

 private:
  static sf::Complex dampingX(fullVector<double>& xyz);
  static sf::Complex dampingY(fullVector<double>& xyz);
  static sf::Complex dampingZ(fullVector<double>& xyz);

 public:
  class XYZ{
  public:
    static sf::Complex sx(fullVector<double>& xyz);
    static sf::Complex sy(fullVector<double>& xyz);
    static sf::Complex sz(fullVector<double>& xyz);

    static sf::Complex Lxx(fullVector<double>& xyz);
    static sf::Complex Lyy(fullVector<double>& xyz);
    static sf::Complex Lzz(fullVector<double>& xyz);
  };

  class XZ{
  public:
    static sf::Complex sx(fullVector<double>& xyz);
    static sf::Complex sy(fullVector<double>& xyz);
    static sf::Complex sz(fullVector<double>& xyz);

    static sf::Complex Lxx(fullVector<double>& xyz);
    static sf::Complex Lyy(fullVector<double>& xyz);
    static sf::Complex Lzz(fullVector<double>& xyz);
  };

  class YZ{
  public:
    static sf::Complex sx(fullVector<double>& xyz);
    static sf::Complex sy(fullVector<double>& xyz);
    static sf::Complex sz(fullVector<double>& xyz);

    static sf::Complex Lxx(fullVector<double>& xyz);
    static sf::Complex Lyy(fullVector<double>& xyz);
    static sf::Complex Lzz(fullVector<double>& xyz);
  };

  class XY{
  public:
    static sf::Complex sx(fullVector<double>& xyz);
    static sf::Complex sy(fullVector<double>& xyz);
    static sf::Complex sz(fullVector<double>& xyz);

    static sf::Complex Lxx(fullVector<double>& xyz);
    static sf::Complex Lyy(fullVector<double>& xyz);
    static sf::Complex Lzz(fullVector<double>& xyz);
  };

  class X{
  public:
    static sf::Complex sx(fullVector<double>& xyz);
    static sf::Complex sy(fullVector<double>& xyz);
    static sf::Complex sz(fullVector<double>& xyz);

    static sf::Complex Lxx(fullVector<double>& xyz);
    static sf::Complex Lyy(fullVector<double>& xyz);
    static sf::Complex Lzz(fullVector<double>& xyz);
  };

  class Y{
  public:
    static sf::Complex sx(fullVector<double>& xyz);
    static sf::Complex sy(fullVector<double>& xyz);
    static sf::Complex sz(fullVector<double>& xyz);

    static sf::Complex Lxx(fullVector<double>& xyz);
    static sf::Complex Lyy(fullVector<double>& xyz);
    static sf::Complex Lzz(fullVector<double>& xyz);
  };

  class Z{
  public:
    static sf::Complex sx(fullVector<double>& xyz);
    static sf::Complex sy(fullVector<double>& xyz);
    static sf::Complex sz(fullVector<double>& xyz);

    static sf::Complex Lxx(fullVector<double>& xyz);
    static sf::Complex Lyy(fullVector<double>& xyz);
    static sf::Complex Lzz(fullVector<double>& xyz);
  };
};

#endif
