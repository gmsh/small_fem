#ifndef _SCATTERINGHELPER_H_
#define _SCATTERINGHELPER_H_

#include "gmsh/fullMatrix.h"
#include "SmallFem.h"

/**
   @class Scattering Helper
   @brief Helping stuff for simulation/Scattering

   Helping stuff for simulation/Scattering
 */

// Math constant //
class Math{
 public:
  static const double Pi;
  static const double nm;
};

// Wave //
class Wave{
 public:
  static const double lambda0;
  static const double theta0;
  static const double phi0;
  static const double psi0;
};

// Material //
class Material{
 public:
  static const double      mu0;
  static const double      epsilon0;
  static const sf::Complex epsIn;
  static const sf::Complex epsOut;

  class In{
  public:
    static void Epsilon(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Epsilon1(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Mu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
  };

  class Out{
  public:
    static void Epsilon(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Epsilon1(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Mu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
  };

  class XYZ{
  public:
    static void Epsilon(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Epsilon1(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Mu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
  };

  class XZ{
  public:
    static void Epsilon(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Epsilon1(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Mu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
  };

  class YZ{
  public:
    static void Epsilon(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Epsilon1(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Mu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
  };

  class XY{
  public:
    static void Epsilon(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Epsilon1(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Mu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
  };

  class X{
  public:
    static void Epsilon(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Epsilon1(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Mu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
  };

  class Y{
  public:
    static void Epsilon(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Epsilon1(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Mu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
  };

  class Z{
  public:
    static void Epsilon(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Epsilon1(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Mu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
  };
};

// Constant //
class Constant{
 public:
  static const double cel;
  static const double Freq;
  static const double omega0;
  static const double k0;

  static const double Ae;
  static const double alpha0;
  static const double beta0;
  static const double gamma0;

  static const double Ex0;
  static const double Ey0;
  static const double Ez0;
};

// Signal //
class Signal{
 public:
  static sf::Complex Prop(fullVector<double>& xyz);
  static fullVector<sf::Complex> Einc(fullVector<double>& xyz);

  class In{
  public:
    static fullVector<sf::Complex> source(fullVector<double>& xyz);
  };

  class Out{
  public:
    static fullVector<sf::Complex> source(fullVector<double>& xyz);
  };

  class PML{
  public:
    static fullVector<sf::Complex> source(fullVector<double>& xyz);
  };
};

// PML //
class PML{
 private:
  static const sf::Complex a;
  static const sf::Complex one;

 public:
  class Volume{
  public:
    static sf::Complex sx(fullVector<double>& xyz);
    static sf::Complex sy(fullVector<double>& xyz);
    static sf::Complex sz(fullVector<double>& xyz);

    static sf::Complex Lxx(fullVector<double>& xyz);
    static sf::Complex Lyy(fullVector<double>& xyz);
    static sf::Complex Lzz(fullVector<double>& xyz);
  };

  class XYZ{
  public:
    static sf::Complex sx(fullVector<double>& xyz);
    static sf::Complex sy(fullVector<double>& xyz);
    static sf::Complex sz(fullVector<double>& xyz);

    static sf::Complex Lxx(fullVector<double>& xyz);
    static sf::Complex Lyy(fullVector<double>& xyz);
    static sf::Complex Lzz(fullVector<double>& xyz);
  };

  class XZ{
  public:
    static sf::Complex sx(fullVector<double>& xyz);
    static sf::Complex sy(fullVector<double>& xyz);
    static sf::Complex sz(fullVector<double>& xyz);

    static sf::Complex Lxx(fullVector<double>& xyz);
    static sf::Complex Lyy(fullVector<double>& xyz);
    static sf::Complex Lzz(fullVector<double>& xyz);
  };

  class YZ{
  public:
    static sf::Complex sx(fullVector<double>& xyz);
    static sf::Complex sy(fullVector<double>& xyz);
    static sf::Complex sz(fullVector<double>& xyz);

    static sf::Complex Lxx(fullVector<double>& xyz);
    static sf::Complex Lyy(fullVector<double>& xyz);
    static sf::Complex Lzz(fullVector<double>& xyz);
  };

  class XY{
  public:
    static sf::Complex sx(fullVector<double>& xyz);
    static sf::Complex sy(fullVector<double>& xyz);
    static sf::Complex sz(fullVector<double>& xyz);

    static sf::Complex Lxx(fullVector<double>& xyz);
    static sf::Complex Lyy(fullVector<double>& xyz);
    static sf::Complex Lzz(fullVector<double>& xyz);
  };

  class X{
  public:
    static sf::Complex sx(fullVector<double>& xyz);
    static sf::Complex sy(fullVector<double>& xyz);
    static sf::Complex sz(fullVector<double>& xyz);

    static sf::Complex Lxx(fullVector<double>& xyz);
    static sf::Complex Lyy(fullVector<double>& xyz);
    static sf::Complex Lzz(fullVector<double>& xyz);
  };

  class Y{
  public:
    static sf::Complex sx(fullVector<double>& xyz);
    static sf::Complex sy(fullVector<double>& xyz);
    static sf::Complex sz(fullVector<double>& xyz);

    static sf::Complex Lxx(fullVector<double>& xyz);
    static sf::Complex Lyy(fullVector<double>& xyz);
    static sf::Complex Lzz(fullVector<double>& xyz);
  };

  class Z{
  public:
    static sf::Complex sx(fullVector<double>& xyz);
    static sf::Complex sy(fullVector<double>& xyz);
    static sf::Complex sz(fullVector<double>& xyz);

    static sf::Complex Lxx(fullVector<double>& xyz);
    static sf::Complex Lyy(fullVector<double>& xyz);
    static sf::Complex Lzz(fullVector<double>& xyz);
  };
};

#endif
