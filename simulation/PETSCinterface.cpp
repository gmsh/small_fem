#include <iostream>
#include <fstream>
#include <set>
#include <utility>
#include <vector>
#include "petscksp.h"
#include "zmumps_c.h"
#include "slepceps.h"

using namespace std;
char help[] = "\nWrapper for petsc.\nTakes input a matrix and a rhs files\n\n";

template<class setOfPairs>
int findMax(const setOfPairs& pairs) {
  int previous=0;
  for (auto const &a : pairs)
    if(a.second > previous)
      previous = a.second;
  return previous;
}
template<class setOfPairs>
vector<pair<int,int>> OrderSet(const setOfPairs& setPairs, int n) {
  int previous, rowSum=0, index=0;
  vector<pair<int,int>> v(n);
  previous = setPairs.begin()->first;
  for (auto const &a : setPairs) {
    if(a.first == previous)
      rowSum++;
    else {
      v[index] = {previous,rowSum}; v[index+1] = {a.first,rowSum};
      rowSum = 1;
      index++;
    }

    previous = a.first;
  }
  return v;
}
template<class setOfPairs>
vector<int> OrderSetVec(const setOfPairs& setPairs, int n) {
  int index=0;
  vector<int> v(n);
  for (auto const &a : setPairs) {
    v[index] = a.second; index++;
  }
  return v;
}


int main(int argc, char** argv) {
  ifstream  matFile;
  ifstream  rhsFile;
  std::string FileInMat = argv[1];
  std::string FileInRHS = argv[2];
  bool   solveSys;


  if (argc < 4)
    solveSys = false;
  else
    solveSys = (bool) atoi(argv[3]);

  Mat             A;
  Vec             x, b;//, u;
  KSP             ksp;
  PC              pc;
//  PetscErrorCode  ierr;
  PetscInt        Prwn,Pcln,nNZ,m, n;
//  PetscReal       norm;
//  PetscMPIInt     size; //setting up a parallel solver
  PetscScalar     val;
  PetscScalar     realPrt, imagPrt;
  string          str;
  int Drwn, Dcln;
  set<pair<int,int>> rcPairSet;

  std::cout << "Matrix A         : " << FileInMat << endl
            << "Vector b         : " << FileInRHS << endl
            << "Solve sys (true?): " << solveSys  << endl << flush;

  // Init
  PetscInitialize(&argc,&argv,(char*)0,help);
//  PetscInitializeNoArguments();

  matFile.open(FileInMat);

  if(!matFile || !rhsFile)
    std::cout << "One or both files is/are unreachable" << endl;

  matFile >> str;
  m = (int) atof(str.c_str());
  matFile >> str;
  n = (int) atof(str.c_str());
  matFile >> str;
  nNZ = (int) atof(str.c_str());

  std::cout << "Matrix size: " << m << " x " << n << endl
            << "Nonzero elements in A: " << nNZ << endl << flush;

  for(int i=0; i<nNZ;i++) {
    matFile >> str;
    Drwn = (int) atof(str.c_str()) -1;
    matFile >> str;
    Dcln = (int) atof(str.c_str()) -1;
    matFile >> str;
    matFile >> str;
    rcPairSet.insert({Drwn,Dcln});
  }

  matFile.close();

  vector<pair<int,int>> uniqueRows = OrderSet(rcPairSet,n);

  // vector of nnz
  vector<int> nnzRows(n);
  nnzRows = OrderSetVec(uniqueRows,n);
  rcPairSet.clear();
  uniqueRows.clear();

  // create from sizes
  VecCreate(PETSC_COMM_SELF,&x);
  VecSetSizes(x,PETSC_DECIDE,n);
  VecSetFromOptions(x);
  VecDuplicate(x,&b);
//  VecDuplicate(b,&u);

  MatCreate(PETSC_COMM_SELF,&A);
  MatSetFromOptions(A);
  MatSetType(A,MATSEQAIJ);
  MatSetSizes(A,PETSC_DECIDE,PETSC_DECIDE,m,n);
//  MatSetUp(A);
  MatSeqAIJSetPreallocation(A,0,nnzRows.data());
//  MatSeqAIJSetPreallocation(A,20,NULL);

  // Assemble
  matFile.open(FileInMat);
  rhsFile.open(FileInRHS);
  matFile >> str; matFile >> str; matFile >> str;
// assemble matrix
  for(int i=0; i<nNZ;i++) {
    matFile >> str;
    Drwn = (int) atof(str.c_str());
    matFile >> str;
    Dcln = (int) atof(str.c_str());
    matFile >> str;
    realPrt = atof(str.c_str());
    matFile >> str;
    imagPrt = atof(str.c_str());
    val = realPrt + imagPrt*PETSC_i;          // rwn cln realPrt imagPrt
    Prwn = (PetscInt) Drwn - 1;              // 0-based indexing
    Pcln = (PetscInt) Dcln - 1;
    MatSetValues(A,1,&Prwn,1,&Pcln,&val,ADD_VALUES);
    }

  MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);



  // assemble rhs
  for(int i =0; i<n; i++){
    rhsFile >> str;
    realPrt = atof(str.c_str());
    rhsFile >> str;
    imagPrt = atof(str.c_str());
    val = realPrt + imagPrt*PETSC_i;
    VecSetValues(b,1,&i,&val,INSERT_VALUES);
  }
  VecAssemblyBegin(b);
  VecAssemblyEnd(b);

  matFile.close();
  rhsFile.close();

  std::cout << "System assembled" << endl << flush;

  // Create PC and Solve
  KSPCreate(PETSC_COMM_WORLD,&ksp);
  KSPSetOperators(ksp,A,A);


   // SETUP SOLVER
  KSPGetPC(ksp,&pc);
  PCSetType(pc,PCLU); //(using LUD) types: PCJACOBI  PCLU
  PCFactorSetMatSolverType(pc,MATSOLVERMUMPS);
  PCFactorSetUpMatSolverType(pc);
//  ierr = KSPSetTolerances(ksp,1.e-5,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);CHKERRQ(ierr); //target val? (1e-50 default)
  KSPSetFromOptions(ksp);

//  Mat F;
//
//  PCFactorGetMatrix(pc,&F);
//  MatMumpsSetIcntl(F,7,0);
  if(solveSys) {
    KSPSolve(ksp,b,x); std::cout << "System solved" << endl << flush;}
//  printf("%d\n",F->num_ass);
//  int ih;
//  MatMumpsGetInfo(F,1,&ih);
//  ZMUMPS_STRUC_C ide = ((Mat_MUMPS*)F->data)->id;
//
//  //  cout << ide.job << endl;
//  int * perm = ide.sym_perm;
//  for(int i = 0; i < n; i++)
//    cout << perm[i] << endl;

 // ierr = VecAXPY(x,-1.0,u);CHKERRQ(ierr);
  //ierr = VecNorm(x,NORM_2,&norm);CHKERRQ(ierr);
  //ierr = KSPGetIterationNumber(ksp,&its);CHKERRQ(ierr); // should be irrelvant
  //ierr = PetscPrintf(PETSC_COMM_WORLD,"Norm of error %g, Iterations %D\n",(double)norm,its);CHKERRQ(ierr);
  //

  PetscFinalize();
  SlepcFinalize();
  return 0;
}
