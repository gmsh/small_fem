#include "SmallFem.h"
#include "TransformationShellCylindrical.cpp"

#include "FunctionSpace1Form.h"
#include "FormulationStiffness.h"
#include "FormulationSource.h"
#include "FormulationHelper.h"

#include "System.h"
#include "SystemHelper.h"
#include "SolverPETSc.h"

#include "GeoHelper.h"
#include "Integrator.h"
#include "Interpolator.h"
#include "Evaluator.h"

using namespace std;
using namespace sf;

static double Pi  = 4 * atan(1);
static double Mu0 = 4 * Pi * 1e-7;
static double Nu0 = 1/Mu0;
static double I0;

void                  Nu(fullVector<double>& xyz, fullMatrix<double>& T);
fullVector<double>    Js(fullVector<double>& xyz);
double        inductance(const GroupOfElement& flux,
                         const FunctionSpace1Form& fsA,
                         const System<double>& system);

void asmSubDomain(IS* is,
                  const FunctionSpace& fs,
                  const vector<const GroupOfElement*>& dom,
                  const DofManager<double>& dofM){
  // Get all DoFs
  set<Dof> allDofTmp;
  for(size_t i = 0; i < dom.size(); i++)
    fs.getKeys(*dom[i], allDofTmp);

  // Get all low-order DoFs
  set<Dof> lorDofTmp;
  for(size_t i = 0; i < dom.size(); i++)
    fs.getSpecialKeys(*dom[i], Basis::Filter::LowOrder, lorDofTmp);

  // Select only non-fixed DoFs (all DoFs)
  set<Dof> allDof;
  for(set<Dof>::const_iterator it=allDofTmp.begin(); it!=allDofTmp.end(); it++)
    if(dofM.getGlobalId(*it) != DofManager<double>::isFixedId())
      allDof.insert(*it);
  allDofTmp.clear();

  // Select only non-fixed DoFs (low-order DoFs)
  set<Dof> lorDof;
  for(set<Dof>::const_iterator it=lorDofTmp.begin(); it!=lorDofTmp.end(); it++)
    if(dofM.getGlobalId(*it) != DofManager<double>::isFixedId())
      lorDof.insert(*it);
  lorDofTmp.clear();

  // Size
  size_t nLor = lorDof.size();                 // Number of low-order  DoFs
  size_t nHor = allDof.size() - lorDof.size(); // Number of high-order DoFs

  // Allocate vector for low-order and high-order indices
  vector<PetscInt> lorIdx; lorIdx.reserve(nLor);
  vector<PetscInt> horIdx; horIdx.reserve(nHor);

  // Iterate an all DoFs, and set their global IDs in the right index vector
  for(set<Dof>::const_iterator it = allDof.begin(); it != allDof.end(); it++){
    set<Dof>::const_iterator found = lorDof.find(*it); // Is it a Low-order DoF?
    if(found == lorDof.end())                          //  ---> No: high-order
      horIdx.push_back(dofM.getGlobalId(*it));         //   ! 0-based index !
    else                                               //  ---> Yes: low-order
      lorIdx.push_back(dofM.getGlobalId(*it));         //   ! 0-based index !
  }

  // Consistency
  // set<PetscInt> check(lorIdx.begin(), lorIdx.end());
  // for(size_t i = 0; i < horIdx.size(); i++){
  //   set<PetscInt>::const_iterator found = check.find(horIdx[i]);
  //   if(found != check.end())
  //     throw Exception("Something is wrong with DoF: %d", horIdx[i]);
  // }
  //
  // cout << "# Unfixed low-order DoFs:  " << nLor        << endl
  //      << "# Unfixed high-order DoFs: " << nHor        << endl
  //      << "# Unfixed totla DoFs:      " << nLor + nHor << endl;

  // Create ISs
  ISCreateGeneral(PETSC_COMM_SELF,nLor,lorIdx.data(),PETSC_COPY_VALUES, &is[0]);
  ISCreateGeneral(PETSC_COMM_SELF,nHor,horIdx.data(),PETSC_COPY_VALUES, &is[1]);
}

void asmSubSolver(KSP ksp, PC pc){
  // Get SubKSPs
  PetscInt nLocal, first;
  KSP*     subSolver;
  PC       subPC[2];
  PCASMGetSubKSP(pc, &nLocal, &first, &subSolver);

  // Check
  if(nLocal != 2)
    throw Exception("Something wrong happend...");

  // Low-order block (default: LU factorization with MUMPS)
  KSPGetPC(subSolver[0], &subPC[0]);
  PCSetType(subPC[0], PCLU);
  PCFactorSetMatSolverType(subPC[0], "mumps");
  KSPSetType(subSolver[0], KSPPREONLY);

  // High-order block (default: 10 CG iterations with ILU)
  KSPGetPC(subSolver[1], &subPC[1]);
  PCSetType(subPC[1], PCILU);
  KSPSetType(subSolver[1], KSPCG);
  KSPSetTolerances(subSolver[1], 1e-9,
                   PETSC_DEFAULT, PETSC_DEFAULT, 10);

  // Prefix options from data base
  KSPSetOptionsPrefix(subSolver[0], "sublo_"); // KSP: low-order block
  KSPSetOptionsPrefix(subSolver[1], "subho_"); // KSP: high-order block
  PCSetOptionsPrefix(subPC[0], "sublo_");      // PC:  low-order block
  PCSetOptionsPrefix(subPC[1], "subho_");      // PC:  high-order block

  // Set options from data base (with prefix)
  KSPSetFromOptions(subSolver[0]); // KSP: low-order block
  KSPSetFromOptions(subSolver[1]); // KSP: high-order block
  PCSetFromOptions(subPC[0]);      // PC:  low-order block
  PCSetFromOptions(subPC[1]);      // PC:  high-order block
}

void compute(const Options& option){
  // Onelab //
  try{
    if(SmallFem::OnelabHelper::action() == "initialize")
      SmallFem::OnelabHelper::client()
        .sendOpenProjectRequest(SmallFem::OnelabHelper::name() + ".geo");

    if(SmallFem::OnelabHelper::action() != "compute")
      return;
  }
  catch(...){
  }

  // Get data //
  string mesh;
  string solver;
  int    order;
  int    nPhi;
  double rInf;
  double dInf;
  int    post;
  try{
    mesh   = SmallFem::OnelabHelper::name()  + ".msh";
    solver = SmallFem::OnelabHelper::getString("03Solver/04Library");
    order  = SmallFem::OnelabHelper::getNumber("03Solver/00Order");
    I0     = SmallFem::OnelabHelper::getNumber("04Problem/00Current");
    nPhi   = SmallFem::OnelabHelper::getNumber("04Problem/03Flux curves");
    rInf   = SmallFem::OnelabHelper::getNumber("01Geo/02Shell/00Inner radius");
    dInf   = SmallFem::OnelabHelper::getNumber("01Geo/02Shell/01Outer radius");
    post   = SmallFem::OnelabHelper::getNumber("03Solver/05View field?");
  }
  catch(...){
    mesh   = option.getValue("-msh")[1];
    solver = "petsc";
    order  = atoi(option.getValue("-o")[1].c_str());
    I0     = 1;
    nPhi   = 8;
    rInf   = atof(option.getValue("-rinf")[1].c_str());
    dInf   = atof(option.getValue("-dinf")[1].c_str());
    post   = 1;
  }

  // Get Domains //
  Mesh msh(mesh);
  GroupOfElement air   = msh.getFromPhysical(1);
  GroupOfElement shell = msh.getFromPhysical(2);
  GroupOfElement dOut  = msh.getFromPhysical(3);
  GroupOfElement dCcc  = msh.getFromPhysical(4);
  GroupOfElement src   = msh.getFromPhysical(5);
  GroupOfElement tree  = msh.getFromPhysical(7);

  vector<GroupOfElement*> flux(nPhi);
  for(int i = 0; i < nPhi; i++)
    flux[i] = new GroupOfElement(msh.getFromPhysical(100+i));

  // Full Domain //
  vector<const GroupOfElement*> domain(6+nPhi);
  domain[0] = &air;
  domain[1] = &shell;
  domain[2] = &dOut;
  domain[3] = &dCcc;
  domain[4] = &src;
  domain[5] = &tree;
  for(int i = 0; i < nPhi; i++)
    domain[6+i] = flux[i];

  // Transformation //
  vector<double> tCenter(3, 0);
  TransformationShellCylindrical tShell(shell, rInf, dInf, 2, tCenter);
  ReferenceSpaceManager::setTransformation(tShell);

  // Function Space //
  string fsFamily = Basis::Family::Hierarchical;
  string fsOption = Basis::Option::NoGradient;

  FunctionSpace1Form fsA (domain, order, fsFamily, fsOption);

  // Formulations //
  cout << "Assembling..." << endl << flush;
  FormulationStiffness<double> stiffAir  (air,   fsA, fsA, Nu);
  FormulationStiffness<double> stiffShell(shell, fsA, fsA, Nu);
  FormulationSource<double>        source(src,   fsA,      Js);

  // System //
  System<double> system(solver);
  system.addFormulation(stiffAir);
  system.addFormulation(stiffShell);
  system.addFormulation(source);

  // BCs //
  SystemHelper<double>::dirichletZero(system, fsA, dCcc);
  SystemHelper<double>::dirichletZero(system, fsA, dOut);
  SystemHelper<double>::tctGauge     (system, fsA, tree);

  // Assembly per say //
  system.assemble();

  cout << "CCC -- Order " << order
       << ": " << system.getSize()
       << endl << flush;

  // Get solver to use ASM (if PETSc) //
  IS is[2];
  if(!solver.compare("petsc")){
    SolverPETSc<double>& solver =
      dynamic_cast<SolverPETSc<double>&>(system.getSolver());

    // ASM preconditioner
    PC precond = solver.getPC();
    PCSetType(precond, PCASM);

    // Define the subdomains (low-order and higher-order DoFs)
    asmSubDomain(is, fsA, domain, system.getDofManager());

    // Setup ASM
    PCASMSetType(precond, PC_ASM_BASIC);                     // Basic Schwarz
    PCASMSetLocalType(precond, PC_COMPOSITE_MULTIPLICATIVE); // Multiplicative
    PCASMSetLocalSubdomains(precond, 2, is, NULL);           // Subdomains

    // Setup subsolvers
    solver.setPostKSPSetUp(asmSubSolver);
  }

  // Solve //
  cout << "Solving..." << endl << flush;
  system.solve();

  // Compute L //
  vector<double> L(nPhi);
  for(int i = 0; i < nPhi; i++)
    L[i] = inductance(*flux[i], fsA, system);

  for(int i = 0; i < nPhi; i++)
    cout << "Inductance (" << i << ")" << ": " << L[i] << " [H]" << endl;

  try{
    char buf[1024];
    for(int i = 0; i < nPhi; i++){
      snprintf(buf, 1024, "05Output/01Inductance [H]/%02d", i);
      SmallFem::OnelabHelper::client().set(onelab::number(string(buf), L[i]));
    }
  }
  catch(...){
  }

  // Average L //
  double aL = 0;
  for(int i = 0; i < nPhi; i++)
    aL += L[i];
  aL /= nPhi;

  cout << "Average: " << aL << " [H]" << endl;
  try{
    SmallFem::OnelabHelper::client()
      .set(onelab::number("05Output/02Average [H]", aL));
  }
  catch(...){
  }

  // Write Sol //
  try{
    option.getValue("-nopos");
  }
  catch(...){
    if(post){
      cout << "Field map..." << endl;
      FEMSolution<double> feSol(true);
      system.getSolution(feSol, fsA, air);
    //system.getSolution(feSol, fsA, shell);
      feSol.write("ccc_b");

      try{
        SmallFem::OnelabHelper::client().sendMergeFileRequest("ccc_b.msh");
      }
      catch(...){
      }
    }
  }

  // Clear PETSc stuff (if we used PETSc) //
  if(!solver.compare("petsc")){
    dynamic_cast<SolverPETSc<double>&>(system.getSolver());

    ISDestroy(&is[0]);
    ISDestroy(&is[1]);
  }

  // Clear other stuff //
  for(int i = 0; i < nPhi; i++)
    delete flux[i];

  // Done //
  cout << "Done :)!" << endl;
}

int main(int argc, char** argv){
  // Init SmallFem //
  SmallFem::Keywords("-msh,-o,-nopos,-rinf,-dinf");
  SmallFem::Initialize(argc, argv);

  compute(SmallFem::getOptions());

  SmallFem::Finalize();
  return 0;
}

void Nu(fullVector<double>& xyz, fullMatrix<double>& T){
  T.scale(0);

  T(0, 0) = Nu0;
  T(1, 1) = Nu0;
  T(2, 2) = Nu0;
}

fullVector<double> Js(fullVector<double>& xyz){
  fullVector<double> v(3);

  v(0) = 0;
  v(1) = 0;
  v(2) = +I0;

  return v;
}

double inductance(const GroupOfElement& flux,
                  const FunctionSpace1Form& fsA,
                  const System<double>& system){
  // Integrator and quadrature points //
  Integrator<double> integrator(flux, 1); // Only 'Nedelec Dofs' will contribute
  vector<pair<const MElement*, fullMatrix<double> > > iQ;
  integrator.getQuadraturePoints(iQ);

  // a //
  map<Dof, double>  aDof;
  SystemHelper<double>::dofMap(fsA, flux, aDof, Basis::Filter::LowOrder);
  system.getSolution(aDof, 0);

  // a at quadrature points //
  vector<pair<const MElement*, fullMatrix<double> > > a;
  Interpolator<double>::interpolate(fsA, aDof, iQ, a, false);

  // Tangent vector at quadrature points //
  vector<pair<const MElement*, fullMatrix<double> > > tg;
  Evaluator<double>::evaluate(GeoHelper<double>::tangent, iQ, tg);

  // Dot product //
  vector<pair<const MElement*, fullMatrix<double> > > aDotT;
  Evaluator<double>::dot(a, tg, aDotT);

  // Integrate, compute L (i.e., divide flux by I0) and return
  return integrator.integrate(aDotT) / I0;
}
