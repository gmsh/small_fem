#include "SmallFem.h"
#include "Mesh.h"

#include "FunctionSpace0Form.h"
#include "FunctionSpace1Form.h"
#include "FormulationQ3DErzEphiStiffness.h"
#include "FormulationQ3DEStarStiffness.h"
#include "FormulationQ3DEStarCoulomb.h"
#include "FormulationQ3DErzEphiCoulomb.h"
#include "FormulationSource.h"
#include "FormulationHelper.h"
#include "System.h"
#include "SystemHelper.h"
#include "NodeSolution.h"
#include "Integrator.h"
#include "Interpolator.h"
#include "Evaluator.h"
#include "GeoHelper.h"
#include <iomanip>

using namespace std;
using namespace sf;

static double Pi  = 4 * atan(1);
static double Mu0 = 4 * Pi * 1e-7;
static double Nu0 = 1/Mu0;
static int    N;
static double Rj;
static double I0;

void                    Nu(fullVector<double>& xyz, fullMatrix<Complex>& T);
void                   NuR(fullVector<double>& xyz, fullMatrix<Complex>& T);
void                NuInvR(fullVector<double>& xyz, fullMatrix<Complex>& T);
void                     R(fullVector<double>& xyz, fullMatrix<Complex>& T);
Complex               invR(fullVector<double>& xyz);
Complex              zeroS(fullVector<double>& xyz);
fullVector<Complex>  zeroV(fullVector<double>& xyz);
fullVector<Complex>    rJs(fullVector<double>& xyz);
fullVector<Complex>   rrJs(fullVector<double>& xyz);
fullVector<Complex>  aRZN1(fullVector<double>& xyz);
fullVector<Complex> bPhiN1(fullVector<double>& xyz);
fullVector<Complex>  uRZN1(fullVector<double>& xyz);

void ohArz(const vector<pair<const MElement*, fullMatrix<double> > >& point,
           const FunctionSpace1Form& hRZ,
           const FunctionSpace0Form& hPhi,
           const map<Dof, Complex>& uRZDof,
           const map<Dof, Complex>& uPhiDof,
           vector<pair<const MElement*, fullMatrix<Complex> > >& aRZ);

void ohBPhi(const vector<pair<const MElement*, fullMatrix<double> > >& point,
            const FunctionSpace1Form& hRZ,
            const FunctionSpace0Form& hPhi,
            const map<Dof, Complex>& uRZDof,
            const map<Dof, Complex>& uPhiDof,
            vector<pair<const MElement*, fullMatrix<Complex> > >& bPhi);

double       inductanceOh(const GroupOfElement& loop, int order,
                          const FunctionSpace1Form& hRZ,
                          const FunctionSpace0Form& hPhi,
                          const System<Complex>& system);
double inductanceAPhiStar(const GroupOfElement& loop, int order,
                          const FunctionSpace1Form& hRZ,
                          const FunctionSpace0Form& hPhi,
                          const System<Complex>& system);

double       l2Oh(const GroupOfElement& dom, int order,
                  const FunctionSpace1Form& hRZ,
                  const FunctionSpace0Form& hPhi,
                  const System<Complex>& system);
double l2APhiStar(const GroupOfElement& dom, int order,
                  const FunctionSpace1Form& hRZ,
                  const FunctionSpace0Form& hPhi,
                  const System<Complex>& system);

void compute(const Options& option){
  // Onelab //
  try{
    if(SmallFem::OnelabHelper::action() == "initialize")
      SmallFem::OnelabHelper::client()
        .sendOpenProjectRequest(SmallFem::OnelabHelper::name() + ".geo");

    if(SmallFem::OnelabHelper::action() != "compute")
      return;
  }
  catch(...){
  }

  // Get data //
  string mesh;
  int    order;
  int    ansatz = 1; // 0: aPhiStar | 1: oh
  try{
    mesh   =        SmallFem::OnelabHelper::name()  + ".msh";
    order  =    int(SmallFem::OnelabHelper::getNumber("Input/05Solver/"
                                                      "00FE order"));
    ansatz =    int(SmallFem::OnelabHelper::getNumber("Input/05Solver/"
                                                      "01Q3D ansatz"));
    N      =    int(SmallFem::OnelabHelper::getNumber("Input/04Problem/"
                                                      "03Current mode [-]"));
    Rj     = double(SmallFem::OnelabHelper::getNumber("Input/03Source/"
                                                      "00Radial offset [m]"));
    I0     = double(SmallFem::OnelabHelper::getNumber("Input/03Source/"
                                                      "02Current [A]"));
  }
  catch(...){
    mesh  = option.getValue("-msh")[1];
    order = atoi(option.getValue("-o")[1].c_str());

    N     = atoi(option.getValue("-n")[1].c_str());;
    Rj    = atof(option.getValue("-rj")[1].c_str());
    I0    = 1;
  }

  // Get Domains //
  Mesh msh(mesh);
  GroupOfElement air  = msh.getFromPhysical(3);
  GroupOfElement inf  = msh.getFromPhysical(4);
  GroupOfElement src  = msh.getFromPhysical(5);
  GroupOfElement dCcc = msh.getFromPhysical(6);
  GroupOfElement dAir = msh.getFromPhysical(7);
  GroupOfElement dInf = msh.getFromPhysical(8);
  GroupOfElement loop = msh.getFromPhysical(10);

  // Full Domain //
  vector<const GroupOfElement*> domain(7);
  domain[0] = &air;
  domain[1] = &inf;
  domain[2] = &src;
  domain[3] = &dCcc;
  domain[4] = &dAir;
  domain[5] = &dInf;
  domain[6] = &loop;

  // To exclude from hXi //
  vector<const GroupOfElement*> exclude(3);
  exclude[0] = &dCcc;
  exclude[1] = &dAir;
  exclude[2] = &dInf;

  // Function Space //
  string fsFamily = Basis::Family::Hierarchical;
  string fsOption = Basis::Option::NoGradient;
  int orderRZ     = order;
  int orderPhi    = order+1;
  int orderInt    = std::max(2*orderRZ + 3, 2*orderPhi + 1);

  FunctionSpace1Form hRZ (domain,          orderRZ, fsFamily, fsOption);
  FunctionSpace0Form hPhi(domain,          orderPhi);
  FunctionSpace0Form hXi (domain, exclude, 1);

  // Compute //
  cout << "Assembling..." << endl << flush;
  Formulation<Complex>* q3dAir;
  Formulation<Complex>* q3dInf;
  Formulation<Complex>* q3dCAir;
  Formulation<Complex>* q3dCInf;
  Formulation<Complex>* q3dSrc;

  switch(ansatz){
  case 0: // aPhiStar
    cout << "APhiStar -- " << flush;

    q3dAir  = new FormulationQ3DEStarStiffness(air, hPhi, hRZ, N, NuR, NuInvR,
                                               orderInt);
    q3dInf  = new FormulationQ3DEStarStiffness(inf, hPhi, hRZ, N, NuR, NuInvR,
                                               orderInt);

    q3dCAir = new FormulationQ3DEStarCoulomb(air, hPhi, hRZ, hXi, N, R, invR,
                                             orderInt);
    q3dCInf = new FormulationQ3DEStarCoulomb(inf, hPhi, hRZ, hXi, N, R, invR,
                                             orderInt);

    q3dSrc  = new FormulationSource<Complex>(src, hRZ, rJs, orderInt);
    break;

  case 1: // oh
    cout << "Oh -- " << flush;

    q3dAir  = new FormulationQ3DErzEphiStiffness(air, hPhi,hRZ,N, Nu, orderInt);
    q3dInf  = new FormulationQ3DErzEphiStiffness(inf, hPhi,hRZ,N, Nu, orderInt);

    q3dCAir = new FormulationQ3DErzEphiCoulomb(air, hPhi,hRZ,hXi, N, orderInt);
    q3dCInf = new FormulationQ3DErzEphiCoulomb(inf, hPhi,hRZ,hXi, N, orderInt);

    q3dSrc  = new FormulationSource<Complex>(src, hRZ, rrJs, orderInt);
    break;

  default:
    throw Exception("Unknown q3d ansatz");
  }

  System<Complex> system;
  system.addFormulation(*q3dAir);
  system.addFormulation(*q3dInf);
  system.addFormulation(*q3dCAir);
  system.addFormulation(*q3dCInf);
  system.addFormulation(*q3dSrc);

  SystemHelper<Complex>::dirichlet(system, hRZ,  dCcc, zeroV);
  SystemHelper<Complex>::dirichlet(system, hRZ,  dAir, zeroV);
  SystemHelper<Complex>::dirichlet(system, hRZ,  dInf, zeroV);
//switch(ansatz){
//  case 0: SystemHelper<Complex>::dirichlet(system, hRZ,  dInf, aRZN1); break;
//  case 1: SystemHelper<Complex>::dirichlet(system, hRZ,  dInf, uRZN1); break;
//}

  SystemHelper<Complex>::dirichlet(system, hPhi, dCcc, zeroS);
  SystemHelper<Complex>::dirichlet(system, hPhi, dAir, zeroS);
  SystemHelper<Complex>::dirichlet(system, hPhi, dInf, zeroS);

  system.assemble();

  cout << "CCC -- Order " << order
       << ": " << system.getSize()
       << endl << flush;

  cout << "Solving..." << endl << flush;
  system.solve();

  // Compute L //
  double L = 0;
  switch(ansatz){
    case 0: L = inductanceAPhiStar(loop, order, hRZ, hPhi, system); break;
    case 1: L =       inductanceOh(loop, order, hRZ, hPhi, system); break;
  }
  cout << std::scientific << std::setprecision(16)
       << "Impedance [H]: " << L << endl;

  // L2 (debugging: full air case only) //
//double L2Air = -1;
//double L2Inf = -1;
//
//switch(ansatz){
//case 0:
//  L2Air = l2APhiStar(air, order, hRZ, hPhi, system);
//  L2Inf = l2APhiStar(inf, order, hRZ, hPhi, system);
//  break;
//
//case 1:
//  L2Air = l2Oh(air, order, hRZ, hPhi, system);
//  L2Inf = l2Oh(inf, order, hRZ, hPhi, system);
//  break;
//}
//
//cout << L2Air << endl
//     << L2Inf << endl;

  // Write Sol //
  try{
    option.getValue("-nopos");
  }
  catch(...){
    cout << "Postprocessing..." << endl << flush;

    FEMSolution<Complex> feURz(false);
    system.getSolution(feURz, hRZ, air);
    system.getSolution(feURz, hRZ, inf);
    feURz.write("URz");

    FEMSolution<Complex> feUPhi(false);
    system.getSolution(feUPhi, hPhi, air);
    system.getSolution(feUPhi, hPhi, inf);
    feUPhi.write("UPhi");
  }

  // Merge in Onelab //
  try{
    SmallFem::OnelabHelper::client().sendMergeFileRequest("URz.msh");
    SmallFem::OnelabHelper::client().sendMergeFileRequest("UPhi.msh");
  }
  catch(...){
  }

  try{
    SmallFem::OnelabHelper::client().set(onelab::number("Output/02Modal L [H]",
                                                        L));
//  SmallFem::OnelabHelper::client().set(onelab::number("Output/03Error Air [-]",
//                                                      L2Air));
//  SmallFem::OnelabHelper::client().set(onelab::number("Output/04Error Inf [-]",
//                                                      L2Inf));
  }
  catch(...){
  }

  // Clean //
  delete q3dAir;
  delete q3dInf;
  delete q3dCAir;
  delete q3dCInf;
  delete q3dSrc;

  // Done //
  cout << "Done :)!" << endl;
}

int main(int argc, char** argv){
  // Init SmallFem //
  SmallFem::Keywords("-msh,-o,-n,-rj,-nopos");
  SmallFem::Initialize(argc, argv);

  compute(SmallFem::getOptions());

  SmallFem::Finalize();
  return 0;
}

void Nu(fullVector<double>& xyz, fullMatrix<Complex>& T){
  T.scale(0);

  T(0, 0) = Complex(Nu0, 0);
  T(1, 1) = Complex(Nu0, 0);
  T(2, 2) = Complex(Nu0, 0);
}

void NuR(fullVector<double>& xyz, fullMatrix<Complex>& T){
  T.scale(0);

  T(0, 0) = Complex(Nu0 * xyz(0), 0);
  T(1, 1) = Complex(Nu0 * xyz(0), 0);
  T(2, 2) = Complex(Nu0 * xyz(0), 0);
}

void NuInvR(fullVector<double>& xyz, fullMatrix<Complex>& T){
  T.scale(0);

  T(0, 0) = Complex(Nu0 * 1.0/xyz(0), 0);
  T(1, 1) = Complex(Nu0 * 1.0/xyz(0), 0);
  T(2, 2) = Complex(Nu0 * 1.0/xyz(0), 0);
}

void R(fullVector<double>& xyz, fullMatrix<Complex>& T){
  T.scale(0);

  T(0, 0) = Complex(xyz(0), 0);
  T(1, 1) = Complex(xyz(0), 0);
  T(2, 2) = Complex(xyz(0), 0);
}

Complex invR(fullVector<double>& xyz){
  return Complex(1.0/xyz(0), 0);
}

Complex zeroS(fullVector<double>& xyz){
  return Complex(0, 0);
}

fullVector<Complex> zeroV(fullVector<double>& xyz){
  fullVector<Complex> v(3);
  v(0) = Complex(0, 0);
  v(1) = Complex(0, 0);
  v(2) = Complex(0, 0);

  return v;
}

fullVector<Complex> rJs(fullVector<double>& xyz){
  fullVector<Complex> v(3);
  v(0) = Complex(0, 0);

  if(N == 0)
    v(1) = Complex(xyz(0) * I0 / (2*Pi*Rj) / N, 0);
  else
    v(1) = Complex(xyz(0) * I0 / (1*Pi*Rj) / N, 0);

  v(2) = Complex(0, 0);
  return v;
}

fullVector<Complex> rrJs(fullVector<double>& xyz){
  fullVector<Complex> v = rJs(xyz);

  v(0) *= xyz(0);
  v(1) *= xyz(0);
  v(2) *= xyz(0);

  return v;
}

fullVector<Complex> aRZN1(fullVector<double>& xyz){
  double Jz1 = I0/(1*Pi*Rj);
  double r   = xyz(0);

  fullVector<Complex> v(3);
  v(0) = Complex(0, 0);

  if(r < Rj)
    v(1) = Complex(Mu0*Jz1/2     *    r,  0);
  else
    v(1) = Complex(Mu0*Jz1*Rj*Rj / (2*r), 0);

  v(2) = Complex(0, 0);
  return v;
}

fullVector<Complex> bPhiN1(fullVector<double>& xyz){
  double Jz1 = I0/(1*Pi*Rj);
  double r   = xyz(0);

  fullVector<Complex> v(3);
  v(0) = Complex(0, 0);
  v(1) = Complex(0, 0);

  if(r < Rj)
    v(2) = Complex(+Mu0*Jz1       /  2,      0);
  else
    v(2) = Complex(-Mu0*Jz1*Rj*Rj / (2*r*r), 0);

  return v;
}

fullVector<Complex> uRZN1(fullVector<double>& xyz){
  double Jz1 = I0/(1*Pi*Rj);
  double r   = xyz(0);

  fullVector<Complex> v(3);
  v(0) = Complex(0, 0);

  if(r < Rj)
    v(1) = Complex(Mu0*Jz1/2,               0);
  else
    v(1) = Complex(Mu0*Jz1*Rj*Rj / (2*r*r), 0);

  v(2) = Complex(0, 0);
  return v;
}

void ohArz(const vector<pair<const MElement*, fullMatrix<double> > >& point,
           const FunctionSpace1Form& hRZ,
           const FunctionSpace0Form& hPhi,
           const map<Dof, Complex>& uRZDof,
           const map<Dof, Complex>& uPhiDof,
           vector<pair<const MElement*, fullMatrix<Complex> > >& aRZ){

  // Interpolate //
  vector<pair<const MElement*, fullMatrix<Complex> > > uRZ;
  vector<pair<const MElement*, fullMatrix<Complex> > > uPhi;
  Interpolator<Complex>::interpolate(hRZ,   uRZDof, point, uRZ );
  Interpolator<Complex>::interpolate(hPhi, uPhiDof, point, uPhi);

  // uPhi * rHat //
  size_t nElement = point.size();
  vector<pair<const MElement*, fullMatrix<Complex> > > uPhiRHat(nElement);
  for(size_t e = 0; e < nElement; e++){
    int nP = uPhi[e].second.size1();
    uPhiRHat[e].second.resize(nP, 3);
    uPhiRHat[e].first = uPhi[e].first;

    for(int i = 0; i < nP; i++){
      uPhiRHat[e].second(i, 0) = uPhi[e].second(i, 0);
      uPhiRHat[e].second(i, 1) = Complex(0, 0);
      uPhiRHat[e].second(i, 2) = Complex(0, 0);
    }
  }

  // uRZ * r //
  vector<pair<const MElement*, fullMatrix<Complex> > > uRZr(nElement);
  for(size_t e = 0; e < nElement; e++){
    int nP = uRZ[e].second.size1();
    uRZr[e].second.resize(nP, 3);
    uRZr[e].first = uRZ[e].first;

    for(int i = 0; i < nP; i++){
      double r = point[e].second(i, 0);

      uRZr[e].second(i, 0) = uRZ[e].second(i, 0) * r;
      uRZr[e].second(i, 1) = uRZ[e].second(i, 1) * r;
      uRZr[e].second(i, 2) = uRZ[e].second(i, 2) * r;
    }
  }

  // aRZ (finally \o/) //
  aRZ.resize(nElement);
  for(size_t e = 0; e < nElement; e++){
    int nP = uRZ[e].second.size1();
    aRZ[e].second.resize(nP, 3);
    aRZ[e].first = uRZ[e].first;

    for(int i = 0; i < nP; i++){
      aRZ[e].second(i, 0) = uRZr[e].second(i, 0) - uPhiRHat[e].second(i, 0) / N;
      aRZ[e].second(i, 1) = uRZr[e].second(i, 1) - uPhiRHat[e].second(i, 1) / N;
      aRZ[e].second(i, 2) = uRZr[e].second(i, 2) - uPhiRHat[e].second(i, 2) / N;
    }
  }
}

void ohBPhi(const vector<pair<const MElement*, fullMatrix<double> > >& point,
            const FunctionSpace1Form& hRZ,
            const FunctionSpace0Form& hPhi,
            const map<Dof, Complex>& uRZDof,
            const map<Dof, Complex>& uPhiDof,
            vector<pair<const MElement*, fullMatrix<Complex> > >& bPhi){

  // Interpolate field and derivatives //
  vector<pair<const MElement*, fullMatrix<Complex> > > uRZ;
  vector<pair<const MElement*, fullMatrix<Complex> > > duRZ;
  vector<pair<const MElement*, fullMatrix<Complex> > > duPhi;
  Interpolator<Complex>::interpolate(hRZ,   uRZDof, point, uRZ,  false);
  Interpolator<Complex>::interpolate(hRZ,   uRZDof, point, duRZ,  true);
  Interpolator<Complex>::interpolate(hPhi, uPhiDof, point, duPhi, true);

  // bPhi //
  size_t nElement = point.size();
  bPhi.resize(nElement);
  for(size_t e = 0; e < nElement; e++){
    int nP = point[e].second.size1();
    bPhi[e].second.resize(nP, 3);
    bPhi[e].first = point[e].first;

    for(int i = 0; i < nP; i++){
      double r = point[e].second(i, 0);

      bPhi[e].second(i, 0) = Complex(0, 0);
      bPhi[e].second(i, 1) = Complex(0, 0);
      bPhi[e].second(i, 2) =   duRZ[e].second(i, 2) * r / N +
                                uRZ[e].second(i, 1)     / N +
                              duPhi[e].second(i, 1)     / N;
    }
  }
}

double inductanceOh(const GroupOfElement& loop, int order,
                    const FunctionSpace1Form& hRZ,
                    const FunctionSpace0Form& hPhi,
                    const System<Complex>& system){
  // Integrator and quadrature points //
  Integrator<Complex> integrator(loop, 2*(order+2));
  vector<pair<const MElement*, fullMatrix<double> > > iQ;
  integrator.getQuadraturePoints(iQ);

  // Auxiliary fields //
  map<Dof, Complex>  uRZDof;
  map<Dof, Complex> uPhiDof;
  FormulationHelper<Complex>::initDofMap(hRZ,  loop,  uRZDof);
  FormulationHelper<Complex>::initDofMap(hPhi, loop, uPhiDof);
  system.getSolution( uRZDof, 0);
  system.getSolution(uPhiDof, 0);

  // aRZ at quadrature points //
  vector<pair<const MElement*, fullMatrix<Complex> > > aRZ;
  ohArz(iQ, hRZ, hPhi, uRZDof, uPhiDof, aRZ);

  // Tangent vector at quadrature points //
  vector<pair<const MElement*, fullMatrix<Complex> > > tg;
  Evaluator<Complex>::evaluate(GeoHelper<Complex>::tangent, iQ, tg);

  // Dot product //
  vector<pair<const MElement*, fullMatrix<Complex> > > aDotT;
  Evaluator<Complex>::dot(aRZ, tg, aDotT);

  // Integrate, compute L (i.e., divide flux by I0) and return
  return integrator.integrate(aDotT).real() / I0;
}

double inductanceAPhiStar(const GroupOfElement& loop, int order,
                          const FunctionSpace1Form& hRZ,
                          const FunctionSpace0Form& hPhi,
                          const System<Complex>& system){
  // Integrator and quadrature points //
  Integrator<Complex> integrator(loop, 2*(order+2));
  vector<pair<const MElement*, fullMatrix<double> > > iQ;
  integrator.getQuadraturePoints(iQ);

  // aRZ //
  map<Dof, Complex>  aRZDof;
  FormulationHelper<Complex>::initDofMap(hRZ, loop, aRZDof);
  system.getSolution(aRZDof, 0);

  // aRZ at quadrature points //
  vector<pair<const MElement*, fullMatrix<Complex> > > aRZ;
  Interpolator<Complex>::interpolate(hRZ, aRZDof, iQ, aRZ, false);

  // Tangent vector at quadrature points //
  vector<pair<const MElement*, fullMatrix<Complex> > > tg;
  Evaluator<Complex>::evaluate(GeoHelper<Complex>::tangent, iQ, tg);

  // Dot product //
  vector<pair<const MElement*, fullMatrix<Complex> > > aDotT;
  Evaluator<Complex>::dot(aRZ, tg, aDotT);

  // Integrate, compute L (i.e., divide flux by I0) and return
  return integrator.integrate(aDotT).real() / I0;
}

double l2Oh(const GroupOfElement& dom, int order,
            const FunctionSpace1Form& hRZ,
            const FunctionSpace0Form& hPhi,
            const System<Complex>& system){
  // Integrator and quadrature points //
  Integrator<Complex> integrator(dom, 2*(order+2));
  vector<pair<const MElement*, fullMatrix<double> > > iQ;
  integrator.getQuadraturePoints(iQ);

  // Auxiliary fields //
  map<Dof, Complex>  uRZDof;
  map<Dof, Complex> uPhiDof;
  FormulationHelper<Complex>::initDofMap(hRZ,  dom,  uRZDof);
  FormulationHelper<Complex>::initDofMap(hPhi, dom, uPhiDof);
  system.getSolution( uRZDof, 0);
  system.getSolution(uPhiDof, 0);

  // aRZ at quadrature points //
  //vector<pair<const MElement*, fullMatrix<Complex> > > aRZFem;
  //ohArz(iQ, hRZ, hPhi, uRZDof, uPhiDof, aRZFem);

  // bPhi at quadrature points //
  vector<pair<const MElement*, fullMatrix<Complex> > > bPhiFem;
  ohBPhi(iQ, hRZ, hPhi, uRZDof, uPhiDof, bPhiFem);

  // Analytic value (aRZ) //
  //vector<pair<const MElement*, fullMatrix<Complex> > > aRZAna;
  //Evaluator<Complex>::evaluate(aRZN1, iQ, aRZAna);

  // Analytic value (bPhi) //
  vector<pair<const MElement*, fullMatrix<Complex> > > bPhiAna;
  Evaluator<Complex>::evaluate(bPhiN1, iQ, bPhiAna);

  // Return L2 error (aRZ) //
  //return Evaluator<Complex>::l2Error(aRZFem, aRZAna, integrator).real();

  // Return L2 error (bPhi) //
  return Evaluator<Complex>::l2Error(bPhiFem, bPhiAna, integrator).real();
}

double l2APhiStar(const GroupOfElement& dom, int order,
                  const FunctionSpace1Form& hRZ,
                  const FunctionSpace0Form& hPhi,
                  const System<Complex>& system){
  // Integrator and quadrature points //
  Integrator<Complex> integrator(dom, 2*(order+2));
  vector<pair<const MElement*, fullMatrix<double> > > iQ;
  integrator.getQuadraturePoints(iQ);

  // aRZ //
  map<Dof, Complex>  aRZDof;
  FormulationHelper<Complex>::initDofMap(hRZ, dom, aRZDof);
  system.getSolution(aRZDof, 0);

  // bPhi at quadrature points //
  vector<pair<const MElement*, fullMatrix<Complex> > > bPhiFem;
  Interpolator<Complex>::interpolate(hRZ, aRZDof, iQ, bPhiFem, true);

  // Analytic value (bPhi) //
  vector<pair<const MElement*, fullMatrix<Complex> > > bPhiAna;
  Evaluator<Complex>::evaluate(bPhiN1, iQ, bPhiAna);

  // Return L2 error (bPhi) //
  return Evaluator<Complex>::l2Error(bPhiFem, bPhiAna, integrator).real();
}
