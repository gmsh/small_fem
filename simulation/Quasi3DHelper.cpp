#include "SmallFem.h"
#include "Quasi3DHelper.h"

using namespace sf;

void Quasi3DHelper::radius(fullVector<double>& xyz, fullMatrix<Complex>& T){
  T.scale(0);

  T(0, 0) = Complex(xyz(0),0);
  T(1, 1) = Complex(xyz(0),0); 
  T(2, 2) = Complex(xyz(0),0); 
}

void Quasi3DHelper::invRadius(fullVector<double>& xyz, fullMatrix<Complex>& T){
  T.scale(0);

  T(0, 0) = Complex(static_cast<double>(1.0) /xyz(0),0);
  T(1, 1) = Complex(static_cast<double>(1.0) /xyz(0),0); 
  T(2, 2) = Complex(static_cast<double>(1.0) /xyz(0),0); 
} 

Complex Quasi3DHelper::invRadiusScalar(fullVector<double>& xyz){
  return Complex(static_cast<double>(1.0) / xyz(0),0);
}

void Quasi3DHelper::identityTensor(fullVector<double>& xyz, fullMatrix<Complex>& T){
  T.scale(0);

  T(0, 0) = Complex(static_cast<double>(1.0), 0.0); 
  T(1, 1) = Complex(static_cast<double>(1.0), 0.0); 
  T(2, 2) = Complex(static_cast<double>(1.0), 0.0); 
}
