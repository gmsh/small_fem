#include "SmallFem.h"
#include "GeoHelper.h"
#include "SystemHelper.h"

using namespace std;
using namespace sf;

void compute(const Options& option){
  // Onelab //
  try{
    if(SmallFem::OnelabHelper::action() != "compute")
      return;
  }
  catch(...){
  }

  // Get data //
  string mesh;
  size_t physical;
  size_t order;
  string dofXFile;
  string dofYFile;
  string dofZFile;
  double mult;
  string output;
  int    alignWith;
  double angle;
  try{
    mesh     = SmallFem::OnelabHelper::getString("09Deform/00Mesh");
    physical = SmallFem::OnelabHelper::getNumber("09Deform/01Physical");
    order    = SmallFem::OnelabHelper::getNumber("09Deform/02FE order");
    dofXFile = SmallFem::OnelabHelper::getString("09Deform/03X Dofs");
    dofYFile = SmallFem::OnelabHelper::getString("09Deform/04Y Dofs");
    dofZFile = SmallFem::OnelabHelper::getString("09Deform/05Z Dofs");
    mult     = SmallFem::OnelabHelper::getNumber("09Deform/06Scaling factor");
    output   = SmallFem::OnelabHelper::getString("09Deform/07Output mesh");
    alignWith= SmallFem::OnelabHelper::getNumber("09Deform/08Align with");
    angle    = SmallFem::OnelabHelper::getNumber("09Deform/09Extra angle");
  }
  catch(...){
    mesh     = option.getValue("-msh")[1];
    physical = atoi(option.getValue("-phys")[1].c_str());
    order    = atoi(option.getValue("-o")[1].c_str());
    dofXFile = option.getValue("-dofX")[1];
    dofYFile = option.getValue("-dofY")[1];
    dofZFile = option.getValue("-dofZ")[1];
    mult     = atof(option.getValue("-mult")[1].c_str());
    output   = option.getValue("-out")[1];
    alignWith= atoi(option.getValue("-align")[1].c_str());
    angle    = atof(option.getValue("-angle")[1].c_str());
  }

  // Get domain //
  Mesh msh(mesh);
  GroupOfElement domain(msh.getFromPhysical(physical));

  // Function spaces //
  FunctionSpace0Form hGradX(domain, order);
  FunctionSpace0Form hGradY(domain, order);
  FunctionSpace0Form hGradZ(domain, order);

  // Read (complex) dofs //
  map<Dof, Complex> dofX; SystemHelper<Complex>::readDof(dofXFile, dofX);
  map<Dof, Complex> dofY; SystemHelper<Complex>::readDof(dofYFile, dofY);
  map<Dof, Complex> dofZ; SystemHelper<Complex>::readDof(dofZFile, dofZ);

  // Deform //
  GeoHelper<Complex>::deformation(domain,
                                  hGradX, hGradY, hGradZ,
                                  dofX, dofY, dofZ,
                                  mult, msh, alignWith, angle);

  // Write //
  msh.getModel().writeMSH(output, 4.1);
}

int main(int argc, char** argv){
  // SmallFEM //
  SmallFem::Keywords("-msh,-phys,-o,-dofX,-dofY,-dofZ,"
                     "-mult,-out,-algin,-angle");
  SmallFem::Initialize(argc, argv);

  compute(SmallFem::getOptions());

  SmallFem::Finalize();
  return 0;
}
