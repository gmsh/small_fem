#include "SmallFem.h"
#include "System.h"
#include "SystemHelper.h"
#include "Interpolator.h"
#include "Integrator.h"

#include "FormulationHelper.h"
#include "FormulationSilverMuller.h"
#include "FormulationSource.h"
#include "FormulationSteadyWave.h"

#include <iostream>

using namespace std;
using namespace sf;

// Some static values //
////////////////////////
static const Complex I  = Complex(0, 1);
static const double  Pi = M_PI;
static const int     m  = 2;
static const double  a  = 1;

static const double  ky = m * Pi / a;
static       double  k;
static       Complex kx;

// Prototypes (see end of file for implementation) //
/////////////////////////////////////////////////////
// Dirichlet and Neumann
Complex  fNeumann(fullVector<double>& xyz);
Complex fAnalytic(fullVector<double>& xyz);

// Helpers
void    getKx(void);
void evaluate(Complex (*f)(fullVector<double>& xyz),
              const vector<pair<const MElement*, fullMatrix<double> > >& point,
              vector<pair<const MElement*, fullMatrix<Complex> > >& eval);

// L2 Error
double modulusSquare(Complex a);
void   modDiffSquare(const vector<pair<const MElement*, fullMatrix<Complex> > >& A,
                     const vector<pair<const MElement*, fullMatrix<Complex> > >& B,
                     vector<pair<const MElement*, fullMatrix<double> > >& L2);
void       modSquare(const vector<pair<const MElement*, fullMatrix<Complex> > >& A,
                     vector<pair<const MElement*, fullMatrix<double> > >& L2);

// Computations //
//////////////////
void compute(const Options& option){
  // Get Parameters //
  const size_t order = atoi(option.getValue("-o")[1].c_str());
  k                  = atof(option.getValue("-k")[1].c_str());

  cout << "Scalar duct" << endl
       << "Wavenumber: " << k     << endl
       << "Order:      " << order << endl << flush;

  // Compute kx //
  getKx();

  // Get Domains //
  Mesh                msh(option.getValue("-msh")[1]);
  GroupOfElement   volume(msh.getFromPhysical(7));
  GroupOfElement   source(msh.getFromPhysical(5));
  GroupOfElement infinity(msh.getFromPhysical(4));

  // Full Domain //
  vector<const GroupOfElement*> domain(3);
  domain[0] = &volume;
  domain[1] = &source;
  domain[2] = &infinity;

  // Function Space //
  FunctionSpace0Form fs(domain, order);

  // Steady Wave Formulation //
  FormulationSteadyWave<Complex> wave(volume,   fs, k);
  FormulationSilverMuller         abc(infinity, fs, k);
  FormulationSource<Complex>      src(source,   fs, fNeumann);

  // Solve //
  System<Complex> system;
  system.addFormulation(wave);
  system.addFormulation(abc);
  system.addFormulation(src);

  // Assemble
  system.assemble();
  cout << "Assembled: " << system.getSize() << endl << flush;

  // Sove
  system.solve();
  cout << "Solved!" << endl << flush;

  // Draw Solution //
  try{
    option.getValue("-nopos");
  }

  catch(...){
    cout << "Writing solution..." << endl << flush;

    FEMSolution<Complex> feSol;
    system.getSolution(feSol, fs, domain);
    feSol.write("duct");
  }

  // L2 Error //
  try{
    // Get integrator order
    const size_t iO = atoi(option.getValue("-l2")[1].c_str());
    cout << "L2 Error (Quadrature " << iO << ")..." << endl;

    // Get integrator
    Integrator<double> integrator(volume, iO);

    // Get quadrature points
    vector<pair<const MElement*, fullMatrix<double> > > qP;
    integrator.getQuadraturePoints(qP);

    // Analytic value
    vector<pair<const MElement*, fullMatrix<Complex> > > anaValue;
    evaluate(fAnalytic, qP, anaValue);

    // FEM solution
    set<Dof> dof;
    fs.getKeys(volume, dof);

    set<Dof>::iterator end = dof.end();
    set<Dof>::iterator  it = dof.begin();
    map<Dof, Complex>  sol;

    for(; it != end; it++)
      sol.insert(pair<Dof, Complex>(*it, 0));

    system.getSolution(sol, 0);

    // Interpolate FEM solution
    vector<pair<const MElement*, fullMatrix<Complex> > > femValue;
    Interpolator<Complex>::interpolate(fs, sol, qP, femValue);

    // mod(ana - fem)^2
    vector<pair<const MElement*, fullMatrix<double> > > modDiffValue;
    modDiffSquare(anaValue, femValue, modDiffValue);

    // mod(ana)^2
    vector<pair<const MElement*, fullMatrix<double> > > modValue;
    modSquare(anaValue, modValue);

    // L2 Error
    double diff = integrator.integrate(modDiffValue);
    double ref  = integrator.integrate(modValue);
    cout << "L2 Error: " << scientific << sqrt(diff/ref) << endl;
  }

  catch(...){
  }
}

int main(int argc, char** argv){
  // Init SmallFem //
  SmallFem::Keywords("-msh,-o,-k,-nopos,-l2");
  SmallFem::Initialize(argc, argv);

  compute(SmallFem::getOptions());

  SmallFem::Finalize();
  return 0;
}

// Helper functions //
//////////////////////
// Source and analytical solution
Complex  fNeumann(fullVector<double>& xyz){
  return cos(m * Pi * xyz(1));
}

Complex fAnalytic(fullVector<double>& xyz){
  Complex A2 = -1.0/(I*kx) / (1.0 - (kx+k)/(kx-k) * exp(-4.0*I*kx));
  Complex A1 = +1.0/(I*kx) + A2;

  return (A1*exp(+I*kx*xyz(0)) + A2*exp(-I*kx*xyz(0))) * cos(m*Pi*xyz(1)) * -1.0;
}

// Helpers
void getKx(void){
  kx = sqrt(Complex(k * k, 0) - (ky * ky));
}

void evaluate(Complex (*f)(fullVector<double>& xyz),
              const vector<pair<const MElement*, fullMatrix<double> > >& point,
              vector<pair<const MElement*, fullMatrix<Complex> > >& eval){

  // Alloc eval //
  const size_t nElement = point.size();
  eval.clear();
  eval.resize(nElement);

  // Loop on elements //
  #pragma omp parallel for
  for(size_t e = 0; e < nElement; e++){
    // Copy element
    eval[e].first = point[e].first;

    // Number of points
    const size_t nPoint = point[e].second.size1();

    // Allocate
    eval[e].second.resize(nPoint, 1);

    // Loop on points and evaluate
    for(size_t i = 0; i < nPoint; i++){
      fullVector<double> xyz(3);

      xyz(0) = point[e].second(i, 0);
      xyz(1) = point[e].second(i, 1);
      xyz(2) = point[e].second(i, 2);

      eval[e].second(i, 0) = f(xyz);
    }
  }
}

// L2 Error
double modulusSquare(Complex a){
  return (a.real() * a.real()) + (a.imag() * a.imag());
}

void modDiffSquare(const vector<pair<const MElement*, fullMatrix<Complex> > >& A,
                   const vector<pair<const MElement*, fullMatrix<Complex> > >& B,
                   vector<pair<const MElement*, fullMatrix<double> > >& L2){

  // Allocate L2 //
  const size_t nElement = A.size();
  L2.clear();
  L2.resize(nElement);

  // Check size //
  if(B.size() != A.size())
    throw Exception("l2Error: sizes of A and B do not match");

  // Iterate on elements //
  for(size_t e = 0; e < nElement; e++){
    // Number of points
    const size_t nPoint = A[e].second.size1();

    // Check with B
    if(size_t(B[e].second.size1()) != nPoint)
      throw Exception("l2Error: sizes of A and B do not match");

    // Allocate
    L2[e].second.resize(nPoint, 1);

    // Copy Element
    L2[e].first = A[e].first;

    // Check Element
    if(A[e].first->getNum() != B[e].first->getNum())
      throw Exception("l2Error: elements of A and B do not match");

    // Loop on points and compute mod
    for(size_t i = 0; i < nPoint; i++)
      L2[e].second(i, 0) = modulusSquare(A[e].second(i, 0) - B[e].second(i, 0));
  }
}

void modSquare(const vector<pair<const MElement*, fullMatrix<Complex> > >& A,
               vector<pair<const MElement*, fullMatrix<double> > >& L2){

  // Allocate L2 //
  const size_t nElement = A.size();
  L2.clear();
  L2.resize(nElement);

  // Iterate on elements //
  for(size_t e = 0; e < nElement; e++){
    // Number of points
    const size_t nPoint = A[e].second.size1();

    // Allocate
    L2[e].second.resize(nPoint, 1);

    // Copy Element
    L2[e].first = A[e].first;

    // Loop on points and compute mod
    for(size_t i = 0; i < nPoint; i++)
      L2[e].second(i, 0) = modulusSquare(A[e].second(i, 0));
  }
}
