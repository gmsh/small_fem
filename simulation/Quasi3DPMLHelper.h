#ifndef _Q3DPMLHELPER_H_
#define _Q3DPMLHELPER_H_

#include "gmsh/fullMatrix.h"
#include "SmallFem.h"

/**
   @file Quasi3DPMLHelper
   @brief defines auxiliary functions for the Quasi-3D eStar solver with PML

   Among these are the material definitions used for the PML.

   @author Erik Schnaubelt
*/

  // Math constant //
class Math{
 public:
  static const double Pi;
};

// Material //
class Material{
 public:
  static const sf::Complex nuR;
  static const sf::Complex epsDomain;
  static const sf::Complex epsSphere;

  class Dom{
  public:
    static void Epsilon(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void EpsilonRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static sf::Complex EpsilonInvRad(fullVector<double>& xyz);
    static void Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void NuRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void NuInvRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
  };

  class Sphere{
  public:
    static void Epsilon(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void EpsilonRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static sf::Complex EpsilonInvRad(fullVector<double>& xyz);
    static void Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void NuRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void NuInvRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
  };

  class LowerZZ{
  public:
    static void Epsilon(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void EpsilonRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static sf::Complex EpsilonInvRad(fullVector<double>& xyz);
    static void Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void NuRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void NuInvRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
  };

  class LowerRZ{
  public:
    static void Epsilon(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void EpsilonRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static sf::Complex EpsilonInvRad(fullVector<double>& xyz);
    static void Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void NuRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void NuInvRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
  };

  class UpperRZ{
  public:
    static void Epsilon(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void EpsilonRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static sf::Complex EpsilonInvRad(fullVector<double>& xyz);
    static void Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void NuRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void NuInvRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
  };

  class UpperZZ{
  public:
    static void Epsilon(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void EpsilonRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static sf::Complex EpsilonInvRad(fullVector<double>& xyz);
    static void Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void NuRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void NuInvRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
  };

  class RR{
  public:
    static void Epsilon(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void EpsilonRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static sf::Complex EpsilonInvRad(fullVector<double>& xyz);
    static void Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void NuRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
    static void NuInvRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
  };

};



// PML //
class PML{
 private:
  static const sf::Complex one;
  static const double alpha;
  static const double d;
  static const double w;


 public:
  class Volume{
  public:
    static sf::Complex sr(fullVector<double>& xyz);
    static sf::Complex sz(fullVector<double>& xyz);
    static sf::Complex rTilde(fullVector<double>& xyz);

    static sf::Complex Lrr(fullVector<double>& xyz);
    static sf::Complex Lzz(fullVector<double>& xyz);
    static sf::Complex LPhiPhi(fullVector<double>& xyz);
  };

  class LowerZZ{
  public:
    static sf::Complex sr(fullVector<double>& xyz);
    static sf::Complex sz(fullVector<double>& xyz);
    static sf::Complex rTilde(fullVector<double>& xyz);

    static sf::Complex Lrr(fullVector<double>& xyz);
    static sf::Complex Lzz(fullVector<double>& xyz);
    static sf::Complex LPhiPhi(fullVector<double>& xyz);
  };

  class LowerRZ{
  public:
    static sf::Complex sr(fullVector<double>& xyz);
    static sf::Complex sz(fullVector<double>& xyz);
    static sf::Complex rTilde(fullVector<double>& xyz);

    static sf::Complex Lrr(fullVector<double>& xyz);
    static sf::Complex Lzz(fullVector<double>& xyz);
    static sf::Complex LPhiPhi(fullVector<double>& xyz);
  };

  class UpperRZ{
  public:
    static sf::Complex sr(fullVector<double>& xyz);
    static sf::Complex sz(fullVector<double>& xyz);
    static sf::Complex rTilde(fullVector<double>& xyz);

    static sf::Complex Lrr(fullVector<double>& xyz);
    static sf::Complex Lzz(fullVector<double>& xyz);
    static sf::Complex LPhiPhi(fullVector<double>& xyz);
  };

  class UpperZZ{
  public:
    static sf::Complex sr(fullVector<double>& xyz);
    static sf::Complex sz(fullVector<double>& xyz);
    static sf::Complex rTilde(fullVector<double>& xyz);

    static sf::Complex Lrr(fullVector<double>& xyz);
    static sf::Complex Lzz(fullVector<double>& xyz);
    static sf::Complex LPhiPhi(fullVector<double>& xyz);
  };

  class RR{
  public:
    static sf::Complex sr(fullVector<double>& xyz);
    static sf::Complex sz(fullVector<double>& xyz);
    static sf::Complex rTilde(fullVector<double>& xyz);

    static sf::Complex Lrr(fullVector<double>& xyz);
    static sf::Complex Lzz(fullVector<double>& xyz);
    static sf::Complex LPhiPhi(fullVector<double>& xyz);
  };

};

#endif
