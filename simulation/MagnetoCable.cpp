#include "SmallFem.h"
#include "Mesh.h"

#include "FunctionSpace1Form.h"
#include "FormulationStiffness.h"
#include "FormulationSource.h"
#include "FormulationHelper.h"
#include "TransformationShellCylindrical.h"
#include "System.h"
#include "SystemHelper.h"

using namespace std;
using namespace sf;

static double Pi  = 4 * atan(1);
static double Mu0 = 4 * Pi * 1e-7;
static double Nu0 = 1/Mu0;
static double R;

void               Nu(fullVector<double>& xyz, fullMatrix<double>& T);
fullVector<double> Js(fullVector<double>& xyz);

void compute(const Options& option){
  // Onelab //
  try{
    if(SmallFem::OnelabHelper::action() == "initialize")
      SmallFem::OnelabHelper::client()
        .sendOpenProjectRequest(SmallFem::OnelabHelper::name() + ".geo");

    if(SmallFem::OnelabHelper::action() != "compute")
      return;
  }
  catch(...){
  }

  // Get data //
  string mesh;
  int    order;
         R    = 1;
  double Rin  = 4;
  double Rout = 8;
  try{
    mesh  = SmallFem::OnelabHelper::name() + ".msh";
    order = SmallFem::OnelabHelper::getNumber("03Solver/01Order");
    R     = SmallFem::OnelabHelper::getNumber("00Geo/00Cable radius");
    Rin   = SmallFem::OnelabHelper::getNumber("00Geo/04Shell tr. inner radius");
    Rout  = SmallFem::OnelabHelper::getNumber("00Geo/05Shell tr. outer radius");
  }
  catch(...){
    mesh  = option.getValue("-msh")[1];
    order = atoi(option.getValue("-o")[1].c_str());
  }

  // Get Domains //
  Mesh msh(mesh);
  GroupOfElement src   = msh.getFromPhysical(1);
  GroupOfElement air   = msh.getFromPhysical(2);
  GroupOfElement shell = msh.getFromPhysical(3);
  GroupOfElement bnd   = msh.getFromPhysical(4);
  GroupOfElement tree  = msh.getFromPhysical(5);

  // Transformation //
  vector<double> tCenter(3, 0);
  TransformationShellCylindrical tShell(shell, Rin, Rout, 2, tCenter);
  ReferenceSpaceManager::setTransformation(tShell);

  // Full Domain //
  vector<const GroupOfElement*> domain(5);
  domain[0] = &src;
  domain[1] = &air;
  domain[2] = &shell;
  domain[3] = &bnd;
  domain[4] = &tree;

  // Function Space //
  string fsFamily = Basis::Family::Hierarchical;
  string fsOption = Basis::Option::NoGradient;

  FunctionSpace1Form  fsA(domain, order, fsFamily, fsOption);

  // Formulation //
  FormulationStiffness<double> stiffSrc(src,   fsA,fsA, Nu);
  FormulationStiffness<double> stiffAir(air,   fsA,fsA, Nu);
  FormulationStiffness<double> stiffShe(shell, fsA,fsA, Nu);
  FormulationSource<double>      source(src,   fsA,     Js);

  // System //
  System<double> system;
  system.addFormulation(stiffSrc);
  system.addFormulation(stiffAir);
  system.addFormulation(stiffShe);
  system.addFormulation(source);

  // BCs //
  SystemHelper<double>::dirichletZero(system, fsA, bnd);
  SystemHelper<double>::tctGauge(system, fsA, tree);

  // Assembling //
  cout << "Assembling..." << endl;
  system.assemble();

  cout << "Cable -- Order " << order
       << ": " << system.getSize()
       << endl << flush;

  // Solve //
  cout << "Solving..." << endl;
  system.solve();

  // Write Sol //
  try{
    option.getValue("-nopos");
  }
  catch(...){
    cout << "Field map..." << endl;
    FEMSolution<double> feSolB(true);
    system.getSolution(feSolB, fsA, src);
    system.getSolution(feSolB, fsA, air);
    system.getSolution(feSolB, fsA, shell);
    feSolB.write("cable_b");

    try{
      SmallFem::OnelabHelper::client().sendMergeFileRequest("cable_b.msh");
    }
    catch(...){
    }
  }

  // Done //
  cout << "Done :)!" << endl;
}

int main(int argc, char** argv){
  // Init SmallFem //
  SmallFem::Keywords("-msh,-o,-gauge,-nopos");
  SmallFem::Initialize(argc, argv);

  compute(SmallFem::getOptions());

  SmallFem::Finalize();
  return 0;
}

void Nu(fullVector<double>& xyz, fullMatrix<double>& T){
  T.scale(0);

  T(0, 0) = Nu0;
  T(1, 1) = Nu0;
  T(2, 2) = Nu0;
}

fullVector<double> Js(fullVector<double>& xyz){
  fullVector<double> v(3);

  v(0) = 0;
  v(1) = 0;
  v(2) = 1/(Pi*R*R);

  return v;
}
