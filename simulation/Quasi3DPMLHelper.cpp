#include "SmallFem.h"
#include "Quasi3DPMLHelper.h"
#include <cmath>


using namespace std;
using namespace sf;


// Math constant //
const double Math::Pi = atan(1.0) * 4;

// PML //
const Complex PML::one   = Complex(1.0,  0);
const double  PML::d     = 5.0;
const double  PML::w     = 5.0;
const double  PML::alpha = 5.0;

// Volume: dom + sphere 
Complex PML::Volume::sr(fullVector<double>& xyz){
  return one;
}

Complex PML::Volume::sz(fullVector<double>& xyz){
  return one;
}

Complex PML::Volume::rTilde(fullVector<double>& xyz){
  return xyz(0);
}

Complex PML::Volume::Lrr(fullVector<double>& xyz){
  return (rTilde(xyz) / xyz(0)) * (sz(xyz) / sr(xyz));
}

Complex PML::Volume::Lzz(fullVector<double>& xyz){
  return (rTilde(xyz) / xyz(0)) * (sr(xyz) / sz(xyz));
}

Complex PML::Volume::LPhiPhi(fullVector<double>& xyz){
  return (xyz(0) /rTilde(xyz))  * sz(xyz) * sr(xyz);
}

// LowerZZ
Complex PML::LowerZZ::sr(fullVector<double>& xyz){
  return one;
}

// Complex PML::LowerZZ::sz(fullVector<double>& xyz){
//   Complex lowZZ = Complex(1.0, - alpha * pow((xyz(1) + d)/w, 2)); 
//   return lowZZ;
// }

Complex PML::LowerZZ::sz(fullVector<double>& xyz){
  Complex lowZZ = Complex(1.0, 1.0); 
  return lowZZ;
}


Complex PML::LowerZZ::rTilde(fullVector<double>& xyz){
  return xyz(0);
}

Complex PML::LowerZZ::Lrr(fullVector<double>& xyz){
  return (rTilde(xyz) / xyz(0)) * (sz(xyz) / sr(xyz));
    
}

Complex PML::LowerZZ::Lzz(fullVector<double>& xyz){
  return (rTilde(xyz) / xyz(0)) * (sr(xyz) / sz(xyz));
}

Complex PML::LowerZZ::LPhiPhi(fullVector<double>& xyz){
  return (xyz(0) /rTilde(xyz))  * sz(xyz) * sr(xyz);
}

// LowerRZ
// Complex PML::LowerRZ::sr(fullVector<double>& xyz){
//   Complex lowRZ = Complex(1.0, - alpha * pow((xyz(0) - d)/w, 2)); 
//   return lowRZ;
// }

Complex PML::LowerRZ::sr(fullVector<double>& xyz){
  Complex lowRZ = Complex(1.0, 1.0); 
  return lowRZ;
}

// Complex PML::LowerRZ::sz(fullVector<double>& xyz){
//   Complex lowZZ = Complex(1.0, - alpha * pow((xyz(1) + d)/w, 2)); 
//   return lowZZ;
// }

Complex PML::LowerRZ::sz(fullVector<double>& xyz){
  Complex lowZZ = Complex(1.0, 1.0); 
  return lowZZ;
}

// Complex PML::LowerRZ::rTilde(fullVector<double>& xyz){
//   Complex lowRZ = Complex(xyz(0), - alpha * (pow(xyz(0) - d,3))/(3*pow(w,2))); 
//   return lowRZ;
// }

Complex PML::LowerRZ::rTilde(fullVector<double>& xyz){
  Complex lowRZ = Complex(xyz(0), xyz(0)); 
  return lowRZ;
}

Complex PML::LowerRZ::Lrr(fullVector<double>& xyz){
  return (rTilde(xyz) / xyz(0)) * (sz(xyz) / sr(xyz));
}

Complex PML::LowerRZ::Lzz(fullVector<double>& xyz){
  return (rTilde(xyz) / xyz(0)) * (sr(xyz) / sz(xyz));
}

Complex PML::LowerRZ::LPhiPhi(fullVector<double>& xyz){
  return (xyz(0) /rTilde(xyz))  * sz(xyz) * sr(xyz);
}

// UpperRZ
// Complex PML::UpperRZ::sr(fullVector<double>& xyz){
//   Complex lowRZ = Complex(1.0, - alpha * pow((xyz(0) - d)/w, 2)); 
//   return lowRZ; 
// }

Complex PML::UpperRZ::sr(fullVector<double>& xyz){
  Complex lowRZ = Complex(1.0, 1.0); 
  return lowRZ; 
}

// Complex PML::UpperRZ::sz(fullVector<double>& xyz){
//   Complex lowZZ = Complex(1.0, - alpha * pow((xyz(1) - d)/w, 2)); 
//   return lowZZ;
// }

Complex PML::UpperRZ::sz(fullVector<double>& xyz){
  Complex lowZZ = Complex(1.0, 1.0); 
  return lowZZ;
}

// Complex PML::UpperRZ::rTilde(fullVector<double>& xyz){
//   Complex lowRZ = Complex(xyz(0), - alpha * (pow(xyz(0) - d,3))/(3*pow(w,2))); 
//   return lowRZ;
// }

Complex PML::UpperRZ::rTilde(fullVector<double>& xyz){
  Complex lowRZ = Complex(xyz(0), xyz(0)); 
  return lowRZ;
}

Complex PML::UpperRZ::Lrr(fullVector<double>& xyz){
  return (rTilde(xyz) / xyz(0)) * (sz(xyz) / sr(xyz));
}

Complex PML::UpperRZ::Lzz(fullVector<double>& xyz){
  return (rTilde(xyz) / xyz(0)) * (sr(xyz) / sz(xyz));
}

Complex PML::UpperRZ::LPhiPhi(fullVector<double>& xyz){
  return (xyz(0) /rTilde(xyz))  * sz(xyz) * sr(xyz);
}

// UpperZZ
Complex PML::UpperZZ::sr(fullVector<double>& xyz){
  return one;
}

// Complex PML::UpperZZ::sz(fullVector<double>& xyz){
//   Complex lowZZ = Complex(1.0, - alpha * pow((xyz(1) - d)/w, 2)); 
//   return lowZZ;
// }

Complex PML::UpperZZ::sz(fullVector<double>& xyz){
  Complex lowZZ = Complex(1.0, 1.0); 
  return lowZZ;
}

Complex PML::UpperZZ::rTilde(fullVector<double>& xyz){
  return xyz(0);
}

Complex PML::UpperZZ::Lrr(fullVector<double>& xyz){
  return (rTilde(xyz) / xyz(0)) * (sz(xyz) / sr(xyz));
}

Complex PML::UpperZZ::Lzz(fullVector<double>& xyz){
  return (rTilde(xyz) / xyz(0)) * (sr(xyz) / sz(xyz));
}

Complex PML::UpperZZ::LPhiPhi(fullVector<double>& xyz){
  return (xyz(0) /rTilde(xyz))  * sz(xyz) * sr(xyz);
}

// RR
// Complex PML::RR::sr(fullVector<double>& xyz){
//   Complex lowRZ = Complex(1.0, - alpha * pow((xyz(0) - d)/w, 2)); 
//   return lowRZ; 
// }

Complex PML::RR::sr(fullVector<double>& xyz){
  Complex lowRZ = Complex(1.0, 1.0); 
  return lowRZ; 
}

Complex PML::RR::sz(fullVector<double>& xyz){
  return one;
}

// Complex PML::RR::rTilde(fullVector<double>& xyz){
//   Complex lowRZ = Complex(xyz(0), - alpha * (pow(xyz(0) - d,3))/(3*pow(w,2))); 
//   return lowRZ;
// }

Complex PML::RR::rTilde(fullVector<double>& xyz){
  Complex lowRZ = Complex(xyz(0), xyz(0)); 
  return lowRZ;
}

Complex PML::RR::Lrr(fullVector<double>& xyz){
  return (rTilde(xyz) / xyz(0)) * (sz(xyz) / sr(xyz));
}

Complex PML::RR::Lzz(fullVector<double>& xyz){
  return (rTilde(xyz) / xyz(0)) * (sr(xyz) / sz(xyz));
}

Complex PML::RR::LPhiPhi(fullVector<double>& xyz){
  return (xyz(0) /rTilde(xyz))  * sz(xyz) * sr(xyz);
}

// Material //
// Constant
const Complex  Material::nuR       =  Complex(1.0,0);
const Complex  Material::epsDomain =  Complex(1.0,0);
const Complex  Material::epsSphere =  Complex(40.0,0);

// Dom
void Material::Dom::Epsilon(fullVector<double>& xyz, fullMatrix<Complex>& T){
  T.scale(0);

  T(0, 0) = epsDomain;
  T(1, 1) = epsDomain;
  T(2, 2) = epsDomain;
}

void Material::Dom::EpsilonRad(fullVector<double>& xyz, fullMatrix<Complex>& T){
  T.scale(0);

  T(0, 0) = xyz(0) * epsDomain;
  T(1, 1) = xyz(0) * epsDomain;
  T(2, 2) = xyz(0) * epsDomain;
}

Complex Material::Dom::EpsilonInvRad(fullVector<double>& xyz){
  return epsDomain / xyz(0);
}
void Material::Dom::Nu(fullVector<double>& xyz, fullMatrix<Complex>& T){
  T.scale(0);

  T(0, 0) = nuR;
  T(1, 1) = nuR;
  T(2, 2) = nuR;
}

void Material::Dom::NuRad(fullVector<double>& xyz, fullMatrix<Complex>& T){
  T.scale(0);

  T(0, 0) = xyz(0) * nuR;
  T(1, 1) = xyz(0) * nuR;
  T(2, 2) = xyz(0) * nuR;
}

void Material::Dom::NuInvRad(fullVector<double>& xyz, fullMatrix<Complex>& T){
  T.scale(0);

  T(0, 0) = 1.0/xyz(0) * nuR;
  T(1, 1) = 1.0/xyz(0) * nuR;
  T(2, 2) = 1.0/xyz(0) * nuR;
}

// Sphere
void Material::Sphere::Epsilon(fullVector<double>& xyz, fullMatrix<Complex>& T){
  T.scale(0);

  T(0, 0) = epsSphere;
  T(1, 1) = epsSphere;
  T(2, 2) = epsSphere;
}

void Material::Sphere::EpsilonRad(fullVector<double>& xyz, fullMatrix<Complex>& T){
  T.scale(0);

  T(0, 0) = xyz(0) * epsSphere;
  T(1, 1) = xyz(0) * epsSphere;
  T(2, 2) = xyz(0) * epsSphere;
}

Complex Material::Sphere::EpsilonInvRad(fullVector<double>& xyz){
  return epsSphere / xyz(0);
}

void Material::Sphere::Nu(fullVector<double>& xyz, fullMatrix<Complex>& T){
  T.scale(0);
  T(0, 0) = nuR;
  T(1, 1) = nuR;
  T(2, 2) = nuR;
}

void Material::Sphere::NuRad(fullVector<double>& xyz, fullMatrix<Complex>& T){
  T.scale(0);
  T(0, 0) = xyz(0) * nuR;
  T(1, 1) = xyz(0) * nuR;
  T(2, 2) = xyz(0) * nuR;
}

void Material::Sphere::NuInvRad(fullVector<double>& xyz, fullMatrix<Complex>& T){
  T.scale(0);

  T(0, 0) = 1.0/xyz(0) * nuR;
  T(1, 1) = 1.0/xyz(0) * nuR;
  T(2, 2) = 1.0/xyz(0) * nuR;
}

// LowerZZ
void Material::LowerZZ::Epsilon(fullVector<double>& xyz, fullMatrix<Complex>& T){
  T.scale(0);

  T(0, 0) = epsDomain * PML::LowerZZ::Lrr(xyz);
  T(1, 1) = epsDomain * PML::LowerZZ::Lzz(xyz);
  T(2, 2) = epsDomain * PML::LowerZZ::LPhiPhi(xyz);
}

void Material::LowerZZ::EpsilonRad(fullVector<double>& xyz, fullMatrix<Complex>& T){
  T.scale(0);

  T(0, 0) = xyz(0) * epsDomain * PML::LowerZZ::Lrr(xyz);
  T(1, 1) = xyz(0) * epsDomain * PML::LowerZZ::Lzz(xyz);
  T(2, 2) = xyz(0) * epsDomain * PML::LowerZZ::LPhiPhi(xyz);
}

Complex Material::LowerZZ::EpsilonInvRad(fullVector<double>& xyz){
  return epsDomain * PML::LowerZZ::LPhiPhi(xyz)/ xyz(0);
}

void Material::LowerZZ::Nu(fullVector<double>& xyz, fullMatrix<Complex>& T){
  T.scale(0);

  T(0, 0) = nuR / PML::LowerZZ::Lrr(xyz);
  T(1, 1) = nuR / PML::LowerZZ::Lzz(xyz);
  T(2, 2) = nuR / PML::LowerZZ::LPhiPhi(xyz);
}

void Material::LowerZZ::NuRad(fullVector<double>& xyz, fullMatrix<Complex>& T){
  T.scale(0);

  T(0, 0) = xyz(0) * nuR / PML::LowerZZ::Lrr(xyz);
  T(1, 1) = xyz(0) * nuR / PML::LowerZZ::Lzz(xyz);
  T(2, 2) = xyz(0) * nuR / PML::LowerZZ::LPhiPhi(xyz);
}

void Material::LowerZZ::NuInvRad(fullVector<double>& xyz, fullMatrix<Complex>& T){
  T.scale(0);

  T(0, 0) = 1.0/xyz(0) * nuR / PML::LowerZZ::Lrr(xyz);
  T(1, 1) = 1.0/xyz(0) * nuR / PML::LowerZZ::Lzz(xyz);
  T(2, 2) = 1.0/xyz(0) * nuR / PML::LowerZZ::LPhiPhi(xyz);
}

// LowerRZ
void Material::LowerRZ::Epsilon(fullVector<double>& xyz, fullMatrix<Complex>& T){
  T.scale(0);

  T(0, 0) = epsDomain * PML::LowerRZ::Lrr(xyz);
  T(1, 1) = epsDomain * PML::LowerRZ::Lzz(xyz);
  T(2, 2) = epsDomain * PML::LowerRZ::LPhiPhi(xyz);
}

void Material::LowerRZ::EpsilonRad(fullVector<double>& xyz, fullMatrix<Complex>& T){
  T.scale(0);

  T(0, 0) = xyz(0) * epsDomain * PML::LowerRZ::Lrr(xyz);
  T(1, 1) = xyz(0) * epsDomain * PML::LowerRZ::Lzz(xyz);
  T(2, 2) = xyz(0) * epsDomain * PML::LowerRZ::LPhiPhi(xyz);
}

Complex Material::LowerRZ::EpsilonInvRad(fullVector<double>& xyz){
  return epsDomain * PML::LowerRZ::LPhiPhi(xyz)/ xyz(0);

}

void Material::LowerRZ::Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T){
  T.scale(0);

  T(0, 0) = nuR / PML::LowerRZ::Lrr(xyz);
  T(1, 1) = nuR / PML::LowerRZ::Lzz(xyz);
  T(2, 2) = nuR / PML::LowerRZ::LPhiPhi(xyz);
}

void Material::LowerRZ::NuRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T){
  T.scale(0);

  T(0, 0) = xyz(0) * nuR / PML::LowerRZ::Lrr(xyz);
  T(1, 1) = xyz(0) * nuR / PML::LowerRZ::Lzz(xyz);
  T(2, 2) = xyz(0) * nuR / PML::LowerRZ::LPhiPhi(xyz);
}

void Material::LowerRZ::NuInvRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T){
  T.scale(0);

  T(0, 0) = 1.0/xyz(0) * nuR / PML::LowerRZ::Lrr(xyz);
  T(1, 1) = 1.0/xyz(0) * nuR / PML::LowerRZ::Lzz(xyz);
  T(2, 2) = 1.0/xyz(0) * nuR / PML::LowerRZ::LPhiPhi(xyz);
}

// UpperRZ
void Material::UpperRZ::Epsilon(fullVector<double>& xyz, fullMatrix<sf::Complex>& T){
  T.scale(0);

  T(0, 0) = epsDomain * PML::UpperRZ::Lrr(xyz);
  T(1, 1) = epsDomain * PML::UpperRZ::Lzz(xyz);
  T(2, 2) = epsDomain * PML::UpperRZ::LPhiPhi(xyz);
}

void Material::UpperRZ::EpsilonRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T){
  T.scale(0);

  T(0, 0) = xyz(0) * epsDomain * PML::UpperRZ::Lrr(xyz);
  T(1, 1) = xyz(0) * epsDomain * PML::UpperRZ::Lzz(xyz);
  T(2, 2) = xyz(0) * epsDomain * PML::UpperRZ::LPhiPhi(xyz);
}

Complex Material::UpperRZ::EpsilonInvRad(fullVector<double>& xyz){
  return epsDomain * PML::UpperRZ::LPhiPhi(xyz)/ xyz(0);
}

void Material::UpperRZ::Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T){
  T.scale(0);

  T(0, 0) = nuR / PML::UpperRZ::Lrr(xyz);
  T(1, 1) = nuR / PML::UpperRZ::Lzz(xyz);
  T(2, 2) = nuR / PML::UpperRZ::LPhiPhi(xyz);
}

void Material::UpperRZ::NuRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T){
  T.scale(0);

  T(0, 0) = xyz(0) * nuR / PML::UpperRZ::Lrr(xyz);
  T(1, 1) = xyz(0) * nuR / PML::UpperRZ::Lzz(xyz);
  T(2, 2) = xyz(0) * nuR / PML::UpperRZ::LPhiPhi(xyz);
}

void Material::UpperRZ::NuInvRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T){
  T.scale(0);

  T(0, 0) = 1.0/xyz(0) * nuR / PML::UpperRZ::Lrr(xyz);
  T(1, 1) = 1.0/xyz(0) * nuR / PML::UpperRZ::Lzz(xyz);
  T(2, 2) = 1.0/xyz(0) * nuR / PML::UpperRZ::LPhiPhi(xyz);
}

// UpperZZ
void Material::UpperZZ::Epsilon(fullVector<double>& xyz, fullMatrix<sf::Complex>& T){
  T.scale(0);

  T(0, 0) = epsDomain * PML::UpperZZ::Lrr(xyz);
  T(1, 1) = epsDomain * PML::UpperZZ::Lzz(xyz);
  T(2, 2) = epsDomain * PML::UpperZZ::LPhiPhi(xyz);
}

void Material::UpperZZ::EpsilonRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T){
  T.scale(0);

  T(0, 0) = xyz(0) * epsDomain * PML::UpperZZ::Lrr(xyz);
  T(1, 1) = xyz(0) * epsDomain * PML::UpperZZ::Lzz(xyz);
  T(2, 2) = xyz(0) * epsDomain * PML::UpperZZ::LPhiPhi(xyz);
}

Complex Material::UpperZZ::EpsilonInvRad(fullVector<double>& xyz){
  return epsDomain * PML::UpperZZ::LPhiPhi(xyz)/ xyz(0);
}

void Material::UpperZZ::Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T){
  T.scale(0);

  T(0, 0) = nuR / PML::UpperZZ::Lrr(xyz);
  T(1, 1) = nuR / PML::UpperZZ::Lzz(xyz);
  T(2, 2) = nuR / PML::UpperZZ::LPhiPhi(xyz);
}

void Material::UpperZZ::NuRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T){
  T.scale(0);

  T(0, 0) = xyz(0) * nuR / PML::UpperZZ::Lrr(xyz);
  T(1, 1) = xyz(0) * nuR / PML::UpperZZ::Lzz(xyz);
  T(2, 2) = xyz(0) * nuR / PML::UpperZZ::LPhiPhi(xyz);
}

void Material::UpperZZ::NuInvRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T){
  T.scale(0);

  T(0, 0) = 1.0/xyz(0) * nuR / PML::UpperZZ::Lrr(xyz);
  T(1, 1) = 1.0/xyz(0) * nuR / PML::UpperZZ::Lzz(xyz);
  T(2, 2) = 1.0/xyz(0) * nuR / PML::UpperZZ::LPhiPhi(xyz);
}

// RR
void Material::RR::Epsilon(fullVector<double>& xyz, fullMatrix<sf::Complex>& T){
  T.scale(0);

  T(0, 0) = epsDomain * PML::RR::Lrr(xyz);
  T(1, 1) = epsDomain * PML::RR::Lzz(xyz);
  T(2, 2) = epsDomain * PML::RR::LPhiPhi(xyz);
}

void Material::RR::EpsilonRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T){
  T.scale(0);

  T(0, 0) = xyz(0) * epsDomain * PML::RR::Lrr(xyz);
  T(1, 1) = xyz(0) * epsDomain * PML::RR::Lzz(xyz);
  T(2, 2) = xyz(0) * epsDomain * PML::RR::LPhiPhi(xyz);
}

Complex Material::RR::EpsilonInvRad(fullVector<double>& xyz){
  return epsDomain * PML::RR::LPhiPhi(xyz)/ xyz(0);

}

void Material::RR::Nu(fullVector<double>& xyz, fullMatrix<sf::Complex>& T){
  T.scale(0);

  T(0, 0) = nuR / PML::RR::Lrr(xyz);
  T(1, 1) = nuR / PML::RR::Lzz(xyz);
  T(2, 2) = nuR / PML::RR::LPhiPhi(xyz);
}

void Material::RR::NuRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T){
  T.scale(0);

  T(0, 0) = xyz(0) * nuR / PML::RR::Lrr(xyz);
  T(1, 1) = xyz(0) * nuR / PML::RR::Lzz(xyz);
  T(2, 2) = xyz(0) * nuR / PML::RR::LPhiPhi(xyz);
}

void Material::RR::NuInvRad(fullVector<double>& xyz, fullMatrix<sf::Complex>& T){
  T.scale(0);

  T(0, 0) = 1.0/xyz(0) * nuR / PML::RR::Lrr(xyz);
  T(1, 1) = 1.0/xyz(0) * nuR / PML::RR::Lzz(xyz);
  T(2, 2) = 1.0/xyz(0) * nuR / PML::RR::LPhiPhi(xyz);
}

