#include "SmallFem.h"
#include "Mesh.h"
#include "FunctionSpace0Form.h"
#include "DofManager.h"

#include <iostream>

using namespace std;
using namespace sf;

void compute(const Options& option){
  // Mesh
  Mesh msh(option.getValue("-msh")[1]);

  // Domain
  GroupOfElement    volume = msh.getFromPhysical(7);
  GroupOfElement boundary0 = msh.getFromPhysical(6);
  GroupOfElement boundary1 = msh.getFromPhysical(5);

  vector<const GroupOfElement*> dom(3);
  dom[0] = &volume;
  dom[1] = &boundary0;
  dom[2] = &boundary1;

  // Functionspace
  FunctionSpace0Form fs(dom, 1);

  // Dofs
  DofManager<double> dofM;
  const vector<vector<Dof> >& dof = fs.getKeys(volume);
  dofM.addToDofManager(dof);

  // Boundary conditions
  const vector<vector<Dof> >& fixOne = fs.getKeys(boundary0);
  for(size_t i = 0; i < fixOne.size(); i++)
    for(size_t j = 0; j < fixOne[i].size(); j++)
      dofM.fixValue(fixOne[i][j], -1);

  const vector<vector<Dof> >& fixTwo = fs.getKeys(boundary1);
  for(size_t i = 0; i < fixTwo.size(); i++)
    for(size_t j = 0; j < fixTwo[i].size(); j++)
      dofM.fixValue(fixTwo[i][j], +1);


  dofM.generateGlobalIdSpace();
  dofM.writePre("smallfem.pre");

  // Display?
  try{
    option.getValue("-disp");
    cout << " ## DofManager ##" << endl
         << dofM.toString()     << endl;
  }
  catch(...){
  }
}

int main(int argc, char** argv){
  // SmallFEM //
  SmallFem::Keywords("-msh,-disp");
  SmallFem::Initialize(argc, argv);

  compute(SmallFem::getOptions());

  SmallFem::Finalize();
  return 0;
}
