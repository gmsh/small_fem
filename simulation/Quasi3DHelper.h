#ifndef _Q3DHELPER_H_
#define _Q3DHELPER_H_

#include "gmsh/fullMatrix.h"
#include "SmallFem.h"

/**
   @class Quasi3DHelper
   @brief defines auxiliary functions for the Quasi-3D eStar solver

   @author Erik Schnaubelt
*/

class Quasi3DHelper{
    public:

        static void radius(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
        static void invRadius(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
        static void identityTensor(fullVector<double>& xyz, fullMatrix<sf::Complex>& T);
        static sf::Complex invRadiusScalar(fullVector<double>& xyz);
};
#endif
