#include "SmallFem.h"
#include "Timer.h"

#include "ReferenceSpacePoint.h"
#include "ReferenceSpaceLine.h"
#include "ReferenceSpaceTri.h"
#include "ReferenceSpaceQuad.h"
#include "ReferenceSpaceTet.h"
#include "ReferenceSpaceHex.h"
#include "ReferenceSpacePyr.h"
#include "ReferenceSpacePri.h"

#include "BasisGenerator.h"
#include "BasisTriLagrange.h"
#include "BasisLine0Form.h"
#include "BasisLine1Form.h"
#include "BasisLineNedelec.h"
#include "BasisTri0Form.h"
#include "BasisQuadNedelec.h"

#include "System.h"
#include "SystemHelper.h"

#include "FormulationSilverMuller.h"
#include "FormulationEMDA.h"
#include "FormulationProjection.h"

#include "FormulationSteadyWave.h"
#include "FormulationStiffness.h"
#include "FormulationMass.h"

#include "Mesh.h"
#include "GroupOfJacobian.h"
#include "GeoHelper.h"

#include "PermutationTree.h"

#include "SolverMatrix.h"
#include "SolverVector.h"
#include "SolverMUMPS.h"

#include "Integrator.h"
#include "Interpolator.h"

#include "TextSolution.h"

#include <complex>
#include <iostream>
#include <cmath>

using namespace std;
using namespace sf;

void compute(const Options& option){
}

int main(int argc, char** argv){
  // SmallFEM //
  SmallFem::Keywords("-msh");
  SmallFem::Initialize(argc, argv);

  compute(SmallFem::getOptions());

  SmallFem::Finalize();
  return 0;
}
