#include "SmallFem.h"
#include "Mesh.h"
#include "SystemHelper.h"
#include "System.h"

#include "FormulationElastoStiffness.h"
#include "FormulationSource.h"

#include "Integrator.h"
#include "Interpolator.h"
#include "Evaluator.h"

using namespace std;
using namespace sf;

static double P = 1;
static double E;
static double nu;
static double Rin;
static double Rout;

double Px(fullVector<double>& xyz){
  return P * cos(atan2(xyz(1), xyz(0)));
}

double Py(fullVector<double>& xyz){
  return P * sin(atan2(xyz(1), xyz(0)));
}

double Ur(fullVector<double>& xyz){
  double a = P * Rin*Rin             / (Rout*Rout-Rin*Rin) * (1+nu)*(1-2*nu)/E;
  double b = P * Rin*Rin * Rout*Rout / (Rout*Rout-Rin*Rin) * (1+nu)/E;
  double r = sqrt(xyz(0)*xyz(0) + xyz(1)*xyz(1));

  return a*r + b/r;
}

double Ux(fullVector<double>& xyz){
  return Ur(xyz) * cos(atan2(xyz(1), xyz(0)));
}

double Uy(fullVector<double>& xyz){
  return Ur(xyz) * sin(atan2(xyz(1), xyz(0)));
}

double Uz(fullVector<double>& xyz){
  return 0;
}

void compute(const Options& option){
  // Onelab //
  try{
    if(SmallFem::OnelabHelper::action() == "initialize")
      SmallFem::OnelabHelper::client()
        .sendOpenProjectRequest(SmallFem::OnelabHelper::name() + ".geo");

    if(SmallFem::OnelabHelper::action() != "compute")
      return;
  }
  catch(...){
  }

  // Get data //
  string mesh;
  size_t order;
  E    = 50e6;
  nu   = 0.25;
  Rin  = 0.2;
  Rout = 0.3;
  try{
    mesh  = SmallFem::OnelabHelper::name()  + ".msh";
    order = SmallFem::OnelabHelper::getNumber("03Solver/00Order");
    E     = SmallFem::OnelabHelper::getNumber("00Physics/00Young's modulus");
    nu    = SmallFem::OnelabHelper::getNumber("00Physics/01Poisson's ratio");
    Rin   = SmallFem::OnelabHelper::getNumber("01Geo/00Inner radius");
    Rout  = SmallFem::OnelabHelper::getNumber("01Geo/00Outre radius");
  }
  catch(...){
    mesh  = option.getValue("-msh")[1];
    order = atoi(option.getValue("-o")[1].c_str());
  }

  // Get Domain //
  Mesh msh(mesh);
  GroupOfElement volume(msh.getFromPhysical(1));
  GroupOfElement  outer(msh.getFromPhysical(2));
  GroupOfElement   side(msh.getFromPhysical(3));
  GroupOfElement  inner(msh.getFromPhysical(4));
  GroupOfElement   pinX(msh.getFromPhysical(5)); // allows sliding along x-axis
  GroupOfElement   pinY(msh.getFromPhysical(6)); // allows sliding along y-axis

  // Full Domain //
  vector<const GroupOfElement*> domain(6);
  domain[0] = &volume;
  domain[1] = &outer;
  domain[2] = &side;
  domain[3] = &inner;
  domain[4] = &pinX;
  domain[5] = &pinY;

  // Function Space //
  FunctionSpace0Form hGradX(domain, order);
  FunctionSpace0Form hGradY(domain, order);
  FunctionSpace0Form hGradZ(domain, order);

  // Formulations & System //
  FormulationElastoStiffness<double> stiff(volume, hGradX,hGradY,hGradZ, E,nu);
  FormulationSource<double>           srcX(inner, hGradX, Px);
  FormulationSource<double>           srcY(inner, hGradY, Py);

  System<double> sys;
  sys.addFormulation(stiff);
  sys.addFormulation(srcX);
  sys.addFormulation(srcY);

  // Dirichlet //
  SystemHelper<double>::dirichletZero(sys, hGradZ, side);
  SystemHelper<double>::dirichletZero(sys, hGradY, pinX);
  SystemHelper<double>::dirichletZero(sys, hGradX, pinY);

  // Assemble and Solve //
  cout << "Pressure problem" << endl << flush;

  sys.assemble();
  cout << "Assembled: " << sys.getSize() << endl << flush;

  sys.solve();
  cout << "Solved" << endl << flush;

  // Write Sol //
  try{
    option.getValue("-nopos");
  }
  catch(...){
    // FEMSolutions
    cout << "Post processing" << endl << flush;
    FEMSolution<double> feSol;

    map<Dof, double> dofX; SystemHelper<double>::dofMap(hGradX, volume, dofX);
    map<Dof, double> dofY; SystemHelper<double>::dofMap(hGradY, volume, dofY);
    map<Dof, double> dofZ; SystemHelper<double>::dofMap(hGradZ, volume, dofZ);

    sys.getSolution(dofX, 0);
    sys.getSolution(dofY, 0);
    sys.getSolution(dofZ, 0);

    feSol.addCoefficients(0,0, volume, hGradX,dofX, hGradY,dofY, hGradZ,dofZ);
    feSol.write("pressure");

    // Merge in Onelab
    try{
      SmallFem::OnelabHelper::client().sendMergeFileRequest("pressure.msh");
    }
    catch(...){
    }
  }


  // L2 Error //
  try{
    option.getValue("-nol2");
  }
  catch(...){
    // Integrator & quadrature points
    cout << "Computing error..." << endl << flush;
    vector<pair<const MElement*, fullMatrix<double> > > qP;
    Integrator<double> integrator(volume, order*4);
    integrator.getQuadraturePoints(qP);

    // Analytic value
    vector<pair<const MElement*, fullMatrix<double> > > anaValue;
    Evaluator<double>::evaluate(Ux, qP, anaValue);

    // FEM solution
    set<Dof> dof;
    hGradX.getKeys(volume, dof);

    set<Dof>::iterator end = dof.end();
    set<Dof>::iterator  it = dof.begin();
    map<Dof, double>  sol;

    for(; it != end; it++)
      sol.insert(pair<Dof, double>(*it, 0));

    sys.getSolution(sol, 0);

    // Interpolate FEM solution
    vector<pair<const MElement*, fullMatrix<double> > > femValue;
    Interpolator<double>::interpolate(hGradX, sol, qP, femValue, false);

    // L2 Error
    double l2 = Evaluator<double>::l2Error(anaValue, femValue, integrator);
    cout << "L2 Error: " << scientific << l2 <<endl;

    // Send it to Onelab, if possible
    try{
      SmallFem::OnelabHelper::client()
        .set(onelab::number("04Output/00L2 Error [-]", l2));
    }
    catch(...){
    }
  }
}

int main(int argc, char** argv){
  // Init SmallFem //
  SmallFem::Keywords("-msh,-o,-nopos,-nol2");
  SmallFem::Initialize(argc, argv);

  // Compute //
  compute(SmallFem::getOptions());

  // Done //
  SmallFem::Finalize();
  return 0;
}
