// include needed headers

#include "SmallFem.h"
#include "Mesh.h"
#include "Interpolator.h"
#include "Integrator.h"

#include "FunctionSpace0Form.h"
#include "FunctionSpace1Form.h"
#include "FormulationQ3DEStarStiffness.h"
#include "FormulationQ3DEStarMass.h"
#include "FormulationQ3DParamStiffness.h"
#include "FormulationQ3DParamMass.h"
#include "FormulationQ3DnErzMass.h"
#include "FormulationQ3DnErzStiffness.h"
#include "FormulationQ3DErzEphiStiffness.h"
#include "FormulationQ3DErzEphiMass.h"

#include "Quasi3DHelper.h"

#include "SystemEigen.h"
#include "SystemHelper.h"
#include "FEMSolution.h"
#include "TextSolution.h"


#include <iostream>
#include <fstream>
#include <iomanip>

/**
   @class Quasi3D
   @brief Quasi-3D solver for axisymmetric problems

   This class is the driver for all different transformations
   to construct a quasi-3D finite element method for axisymmetric problems.
   The used transformation can be given by user via the option "-ansatz" with:
   - 0/default: the ephiStar = r ephi ansatz
   - 1:         the parameterized transformation
   - 2:         Dunn ansatz
   - 3:         Oh ansatz

   For all other possible options, please consider the code.

   @author Erik Schnaubelt
*/

using namespace std;
using namespace sf;

// define needed zeros
Complex fZeroScal(fullVector<double>& xyz){
  return Complex(0, 0);
}

fullVector<Complex> fZeroVect(fullVector<double>& xyz){
  fullVector<Complex> f(3);

  f(0) = Complex(0, 0);
  f(1) = Complex(0, 0);
  f(2) = Complex(0, 0);

  return f;
}


void compute(const Options& option){
  // Get Domains //
  Mesh msh(option.getValue("-msh")[1]);
  GroupOfElement dom     = msh.getFromPhysical(120);
  GroupOfElement nonsymm = msh.getFromPhysical(101);
  GroupOfElement symm    = msh.getFromPhysical(102);
  GroupOfElement mid     = msh.getFromPhysical(103);

  // Full Domain //
  vector<const GroupOfElement*> domain(3);
  domain[0] = &dom;
  domain[1] = &nonsymm;
  domain[2] = &symm;

  // Get parameters from user input //
  double shift;
  double tol;
  double alpha;
  double beta;
  int    n;
  int    nEig;
  int    orderHCurl;
  int    orderHGrad;
  int    orderGauss;
  int    ansatz; // 0: ephiStar = r ephi, 1: parameterized, default: 0
  string ansatzMap[4] = {"ephiStar", "parameterized",
                         "Dunn", "Oh"}; // to map the ansatz index to a name

  try{
    shift = atof(option.getValue("-shift")[1].c_str());
  }
  catch(...){
    // default is for TE111 //
    shift = 2.380341795042317e+19;
  }

  try{
    tol = atof(option.getValue("-tol")[1].c_str());
  }
  catch(...){
    tol = 1e-6;
  }

  try{
    n = atoi(option.getValue("-n")[1].c_str());
  }
  catch(...){
    n = 1;
  }

  try{
    alpha = atof(option.getValue("-alpha")[1].c_str());
  }
  catch(...){
    alpha = 1.0;
  }

  try{
    beta = atof(option.getValue("-beta")[1].c_str());
  }
  catch(...){
    switch(n) {
    case 0:
      beta = 2.0;
      break;

    default:
      beta = 1.0;
      break;
    }
  }

  try{
    nEig = atoi(option.getValue("-nEig")[1].c_str());
  }
  catch(...){
    nEig = 20;
  }

  try{
    orderHGrad = atoi(option.getValue("-oHG")[1].c_str());
  }
  catch(...){
    orderHGrad = 1;
  }

  try{
    orderHCurl = atoi(option.getValue("-oHC")[1].c_str());
  }
  catch(...){
    orderHCurl = 0;
  }

  try{
    orderGauss = atoi(option.getValue("-oGauss")[1].c_str());
  }
  catch(...){
    orderGauss = -1;
  }

  try{
    ansatz = atoi(option.getValue("-ansatz")[1].c_str());
  }
  catch(...){
    ansatz = 0;
  }

  // Function Space //
  FunctionSpace0Form hGrad(domain, orderHGrad);
  FunctionSpace1Form hCurl(domain, orderHCurl);
  SystemEigen system;

  Formulation<Complex>* trafoStiff;
  Formulation<Complex>* trafoMass;

  // Choose right formulation depending on ansatz //
  // default is eStar
  switch(ansatz){
  case 0:
    trafoStiff = new FormulationQ3DEStarStiffness(dom, hGrad, hCurl, n,
                                                  Quasi3DHelper::radius,
                                                  Quasi3DHelper::invRadius,
                                                  orderGauss);

    trafoMass  = new FormulationQ3DEStarMass(dom, hGrad, hCurl,
                                             Quasi3DHelper::radius,
                                             Quasi3DHelper::invRadiusScalar,
                                             orderGauss);
    break;

  case 1:
    trafoStiff = new FormulationQ3DParamStiffness(dom, hGrad, hCurl,
                                                  alpha, beta, n,
                                                  Quasi3DHelper::identityTensor,
                                                  orderGauss);

    trafoMass  = new FormulationQ3DParamMass(dom, hGrad, hCurl,
                                             alpha, beta, n,
                                             Quasi3DHelper::identityTensor,
                                             orderGauss);
    break;

  case 2:
    trafoStiff = new FormulationQ3DnErzStiffness(dom, hGrad, hCurl,
                                                 n,
                                                 Quasi3DHelper::identityTensor,
                                                 orderGauss);

    trafoMass  = new FormulationQ3DnErzMass(dom, hGrad, hCurl,
                                            n,
                                            Quasi3DHelper::identityTensor,
                                            orderGauss);
    break;

  case 3:
    trafoStiff = new FormulationQ3DErzEphiStiffness(dom, hGrad, hCurl,
                                                    n,
                                                    Quasi3DHelper::identityTensor,
                                                    orderGauss);

    trafoMass  = new FormulationQ3DErzEphiMass(dom, hGrad, hCurl,
                                               n,
                                               Quasi3DHelper::identityTensor,
                                               orderGauss);
    break;

  default:
    trafoStiff = new FormulationQ3DEStarStiffness(dom, hGrad, hCurl, n,
                                                  Quasi3DHelper::radius,
                                                  Quasi3DHelper::invRadius,
                                                  orderGauss);


    trafoMass  = new FormulationQ3DEStarMass(dom, hGrad, hCurl,
                                             Quasi3DHelper::radius,
                                             Quasi3DHelper::invRadiusScalar,
                                             orderGauss);
    break;
  }

  system.addFormulation(*trafoStiff);
  system.addFormulationB(*trafoMass);

  //PEC is the same for all trafos
  SystemHelper<Complex>::dirichlet(system, hCurl, nonsymm, fZeroVect);
  SystemHelper<Complex>::dirichlet(system, hGrad, nonsymm, fZeroScal);

  // bc dependent on ansatz
  switch(ansatz) {
  case 0:
    SystemHelper<Complex>::dirichlet(system, hGrad, symm, fZeroScal);
    if(n > 0){
      cout<< "WARNING: dirichlet on symm for OutOfPlane" << endl;
      SystemHelper<Complex>::dirichlet(system, hCurl, symm, fZeroVect);
    }

    break;

  case 1:
    if(n == 0){
      if(beta < 1.5) {
        cout<< "WARNING: dirichlet on symm for OutOfPlane" << endl;
        SystemHelper<Complex>::dirichlet(system, hGrad, symm, fZeroScal);
      }
    }
    else {
      if ((n == 1 && beta < 0.5) || (n > 1 && beta <= 1)){
        cout<< "WARNING: dirichlet on symm for OutOfPlane" << endl;
        SystemHelper<Complex>::dirichlet(system, hGrad, symm, fZeroScal);
      }
    }
    break;

  case 2:
    if(n != 1 && n != -1){
      cout<< "WARNING: dirichlet on symm for OutOfPlane" << endl;
      SystemHelper<Complex>::dirichlet(system, hGrad, symm, fZeroScal);
    }
    break;

  case 3:
    if(n != 1 && n != -1){
      cout<< "WARNING: dirichlet on symm for OutOfPlane" << endl;
      SystemHelper<Complex>::dirichlet(system, hGrad, symm, fZeroScal);
    }
    break;
  }

  cout << "Assembling..." << endl;
  system.assemble();

  // Set solver parameters //
  system.setWhichEigenpairs("target_magnitude");
  system.setTarget(Complex(shift, 0));
  system.setNumberOfEigenValues(nEig);
  system.setTolerance(tol);

  // Header output //
  cout << "Quasi3D ("
       << "ansatz: "      << ansatzMap[ansatz] << ", "
       << "alpha: "       << alpha             << ", "
       << "beta: "        << beta              << ", "
       << "oGauss: "      << orderGauss        << ", "
       << "shift: "       << shift             << ", "
       << "tol: "         << tol               << ", "
       << "nEig: "        << nEig              << ", "
       << "n: "           << n                 << ", "
       << "Order HGrad: " << orderHGrad        << ", "
       << "Order HCurl: " << orderHCurl        << "),"
       << " "             << system.getSize()  << endl << flush;

  cout << "Solving..." << endl;
  system.solve();

  // Display //
  fullVector<Complex> eigenValue;

  // Get number of computed eigenvalues
  const size_t nEigenValue = system.getNComputedSolution();
  system.getEigenValues(eigenValue);

  cout << "Number of found Eigenvalues: " << nEigenValue
       << endl
       << endl
       << "Number\tomega = sqrt(Eigenvalue)" << endl;

  // Console Output of eigenvalues //
  for(size_t i = 0; i < nEigenValue; i++)
    cout << std::scientific << std::setprecision(16) << "#" << i + 1  << "\t"
         << sqrt(eigenValue(i)) << endl;

  // Dump eigenvalues into file //
  ofstream myfile;
  myfile.open("./eigen.txt");

  for(size_t i = 0; i < nEigenValue; i++){
    if (myfile.is_open()) {
      myfile << std::scientific << std::setprecision(16)
	     << sqrt(eigenValue(i).real()) ;
      myfile << "\n";
    } else {
      cout << "Unable to open file.";
    }
  }
  myfile.close();

  // Write Gmsh output files //
  try{
    option.getValue("-nopos");
  }
  catch(...){
    //get solutions and write pos file
    FEMSolution<Complex> feSolCurl;
    system.getSolution(feSolCurl, hCurl, domain);
    feSolCurl.write("eigenModesCurl");

    FEMSolution<Complex> feSolGrad;
    system.getSolution(feSolGrad, hGrad, domain);
    feSolGrad.write("eigenModesGrad");

    TextSolution txtSol;
    for(size_t i = 0; i < nEigenValue; i++){
      stringstream stream;
      stream << "omega = sqrt(Eigenvalue) " << i + 1 << ": "
             << std::scientific << std::setprecision(16)
             << sqrt(eigenValue(i)) << " ";

      txtSol.addValues(i * 2 + 0, stream.str().append("(real part)"));
      txtSol.addValues(i * 2 + 1, stream.str().append("(imaginary part)"));
    }
    txtSol.write("eigenModesTxt");
  }

  // Solution on cut
  try{
    option.getValue("-cut"); // Should I do the cut?

    // Get integrator order
    const size_t iO = 10;
    cout << "Cut (Quadrature " << iO << ")..." << endl;

    // Get integrator
    Integrator<double> integrator(dom, iO);

    // Get quadrature points
    vector<pair<const MElement*, fullMatrix<double> > > qP;
    integrator.getQuadraturePoints(qP);

    // FEM solution
    set<Dof> dof;
    hGrad.getKeys(dom, dof);
    hCurl.getKeys(dom, dof);

    set<Dof>::iterator end = dof.end();
    set<Dof>::iterator  it = dof.begin();
    map<Dof, Complex>  sol;

    for(; it != end; it++)
      sol.insert(pair<Dof, Complex>(*it, 0));

    system.getSolution(sol, 0);

    // Interpolate FEM solution
    int nPoint = 1000;
    fullMatrix<double>  points(nPoint, 3);
    vector<bool>        isValid;

    cout << "Cut (Points " << nPoint << ")..." << endl;
    for(int i = 0; i < nPoint; i++){
      points(i, 0) = i*(0.015/(nPoint-1));
      points(i, 1) = 0.27/2;
      points(i, 2) = 0;
    }

    fullMatrix<Complex> femValuesPhi;
    Interpolator<Complex>::
      interpolate(dom, hGrad, sol, points, femValuesPhi, isValid);
    Interpolator<Complex>::write("cutPhi.csv", points, femValuesPhi);

    fullMatrix<Complex> femValuesGradPhi;
    Interpolator<Complex>::
      interpolate(dom, hGrad, sol, points, femValuesGradPhi, isValid, true);
    Interpolator<Complex>::write("cutGradPhi.csv", points, femValuesGradPhi);

    fullMatrix<Complex> femValuesRZ;
    Interpolator<Complex>::
      interpolate(dom, hCurl, sol, points, femValuesRZ, isValid);
    Interpolator<Complex>::write("cutRZ.csv", points, femValuesRZ);
  }
  catch(...){
  }
}

int main(int argc, char** argv){
  // Init SmallFem //
  SmallFem::Keywords("-msh,-shift,-nEig,-tol,-alpha,-beta,-n,-nopos,-cut,"
                     "-oHC,-oHG,-oGauss,-ansatz");
  SmallFem::Initialize(argc, argv);

  compute(SmallFem::getOptions());

  SmallFem::Finalize();
  return 0;
}
