#include "VecTool.h"
#include "SVDEngine.h"

using namespace std;

SVDEngine::SVDEngine(void){
  // Init SLEPc stuff
  SVDCreate(PETSC_COMM_WORLD, &svdctx);
  SVDSetWhichSingularTriplets(svdctx, SVD_LARGEST);
  SVDSetType(svdctx, SVDTRLANCZOS);
  SVDSetTolerances(svdctx, 1e-15, 1000);

  // Default TOLZ
  TOLZ = 1e-12;
}

SVDEngine::~SVDEngine(void){
  SVDDestroy(&svdctx);
}

void SVDEngine::tolZ(double t){
  this->TOLZ = t;
}

void SVDEngine::svd(Mat A){
  // Number of singular values (all)
  int nRow, nCol, nSV;
  MatGetSize(A, &nRow, &nCol);
  if(nRow < nCol)
    nSV = nRow;
  else
    nSV = nCol;

  // Set operator
  SVDSetOperator(svdctx, A);
  SVDSetDimensions(svdctx, nSV, PETSC_DEFAULT, PETSC_DEFAULT);
  SVDSetFromOptions(svdctx);

  // Compute SVD
  SVDSolve(svdctx);

  // Check
  int nConv;
  SVDGetConverged(svdctx, &nConv);
  if(nConv != nSV)
    throw runtime_error("SVDEngine: found less SVs than expected");
}

void SVDEngine::sv(vector<double>& sigma) const{
  // Number of SVs
  int nConv;
  SVDGetConverged(svdctx, &nConv);

  // Clear and resize vector if needed;
  if(nConv != (int)(sigma.size())){
    sigma.clear();
    sigma.resize(nConv);
  }

  // Populate
  for(int i = 0; i < nConv; i++)
    SVDGetSingularTriplet(svdctx, i, &sigma[i], NULL, NULL);
}

void SVDEngine::sv(vector<Complex>& sigma) const{
  vector<double> sigmaRe;
  sv(sigmaRe);
  toComplex(sigmaRe, sigma);
}

void SVDEngine::nzsv(vector<double>& sigma) const{
  // Number of SVs
  int nConv;
  SVDGetConverged(svdctx, &nConv);

  // Count non zero SVs
  int     nz = 0;
  double tmp;
  for(int  i = 0; i < nConv; i++){
    SVDGetSingularTriplet(svdctx, i, &tmp, NULL, NULL);
    if(!(abs(tmp) < TOLZ))
      nz++;
  }

  // Clear and resize vector if needed;
  if(nz != (int)(sigma.size())){
    sigma.clear();
    sigma.resize(nz);
  }

  // Populate
  for(int  i = 0; i < nConv; i++){
    SVDGetSingularTriplet(svdctx, i, &tmp, NULL, NULL);
    if(!(abs(tmp) < TOLZ))
      sigma[i] = tmp;
  }
}

void SVDEngine::nzsv(vector<Complex>& sigma) const{
  vector<double> sigmaRe;
  nzsv(sigmaRe);
  toComplex(sigmaRe, sigma);
}

void SVDEngine::uv(Mat U, Mat V) const{
  // NB: U and V such that A = U * Sigma * V^H
  // Number of SVs
  int nConv;
  SVDGetConverged(svdctx, &nConv);

  // Get sizes
  int sizeU, sizeV;
  MatGetSize(U, &sizeU, &sizeV);

  // Zeroing U and V
  MatZeroEntries(U);
  MatZeroEntries(V);

  // Init Vec
  double sigma;
  Vec u; VecTool::newVec(&u, sizeU);
  Vec v; VecTool::newVec(&v, sizeV);
  PetscScalar* dataU;
  PetscScalar* dataV;

  // Ownership of each row of U
  int rowStartU, rowEndU, nRowLocalU;
  MatGetOwnershipRange(U, &rowStartU, &rowEndU);
  nRowLocalU = rowEndU-rowStartU;

  // Ownership of each row of V
  int rowStartV, rowEndV, nRowLocalV;
  MatGetOwnershipRange(V, &rowStartV, &rowEndV);
  nRowLocalV = rowEndV-rowStartV;

  // Row global indices of U for each process
  PetscInt* idxRowU = new PetscInt[nRowLocalU];
  for(int i = 0; i < nRowLocalU; i++)
    idxRowU[i] = i + rowStartU;

  // Row global indices of V for each process
  PetscInt* idxRowV = new PetscInt[nRowLocalV];
  for(int i = 0; i < nRowLocalV; i++)
    idxRowV[i] = i + rowStartV;

  for(int i = 0; i < nConv; i++){
    SVDGetSingularTriplet(svdctx, i, &sigma, u, v);

    VecGetArray(u, &dataU);
    VecGetArray(v, &dataV);

    MatSetValues(U, nRowLocalU, idxRowU, 1, &i, dataU, INSERT_VALUES);
    MatSetValues(V, nRowLocalV, idxRowV, 1, &i, dataV, INSERT_VALUES);

    VecRestoreArray(u, &dataU);
    VecRestoreArray(v, &dataV);
  }

  // Start to assemble U and V
  MatAssemblyBegin(U, MAT_FINAL_ASSEMBLY);
  MatAssemblyBegin(V, MAT_FINAL_ASSEMBLY);

  // Clear
  VecDestroy(&u);
  VecDestroy(&v);
  delete[] idxRowU;
  delete[] idxRowV;

  // Wait for assembling U and V
  MatAssemblyEnd(U, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(V, MAT_FINAL_ASSEMBLY);
}

int SVDEngine::rank(void) const{
  // Non zero SVs
  vector<double> sigma;
  nzsv(sigma);

  // Done
  return sigma.size();
}

void SVDEngine::toComplex(const vector<double>& Re,
                                vector<Complex>& Cmplx) const{
  // Clear and resize vector if needed;
  int size = Re.size();
  if(size != (int)(Cmplx.size())){
    Cmplx.clear();
    Cmplx.resize(size);
  }

  // Populate
  for(int  i = 0; i < size; i++)
    Cmplx[i] = Complex(Re[i]);
}
