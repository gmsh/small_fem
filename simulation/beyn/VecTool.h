#ifndef _VECTOOL_H_
#define _VECTOOL_H_

#include "petscvec.h"

class VecTool{
 public:
  static void newVec(Vec* v, int size);
  static void newVec(Vec* v, int size, int sLoc);
};

#endif
