#ifndef _PLOT_H_
#define _PLOT_H_

#include "Options.h"
#include "petscvec.h"

void plot(const sf::Options& option, Vec solV, std::string name);

#endif
