#ifndef _EIGENGINE_H_
#define _EIGENGINE_H_

#include <vector>
#include "Math.h"
#include "slepceps.h"

class EigEngine{
 private:
  double TOLZ;
  EPS eigctx;

 public:
   EigEngine(void);
  ~EigEngine(void);

  void tolZ(double t);
  void  eig(Mat A);

  void  val(std::vector<Complex>& lambda) const;
  void  vec(Vec x, int i)                 const;
};

#endif
