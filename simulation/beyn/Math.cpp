#include "Math.h"

using namespace std;

const Scalar  Math::PI = acos(0) * 2;
const Complex Math::J  = Complex(0, 1);
