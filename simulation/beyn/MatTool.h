#ifndef _MATTOOL_H_
#define _MATTOOL_H_

#include <string>
#include <vector>
#include "petscmat.h"

class MatTool{
 public:
  static void newDense( Mat* A, int nRow, int nCol, int nRowLoc, int nColLoc);
  static void newDense( Mat* A, int nRow, int nCol);
  static void newSparse(Mat* A, int nRow, int nCol);
  static void newDenseDiagInv(Mat* A, int size,
                              const std::vector<PetscScalar>& v, int sLoc);

  static void  read(std::string filename, Mat* A);
  static void write(std::string filename, Mat  A, std::string name = "petsc");
  static void  rand(Mat *A, int nRow, int nCol);
};

#endif
