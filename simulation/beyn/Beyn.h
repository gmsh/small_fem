#ifndef _BEYN_H_
#define _BEYN_H_

#include <vector>
#include "Math.h"
#include "Operator.h"
#include "EigEngine.h"
#include "petscmat.h"

class Beyn{
 private:
  double  radius;
  Complex origin;
  int     nodes;
  double  svdTolZ;
  double  eigTolV;
  int     lStart;
  int     lStep;
  int     maxIt;
  int     m, l, k, kLoc;
  const   Operator* op;

 public:
   Beyn(const Operator& op);
  ~Beyn(void);

  void simple(std::vector<Complex>& lambda, std::vector<Vec>& eigV);

  void setRadius(double R);
  void setNNodes(int n);
  void setSVDTolZ(double tol);
  void setEigTolV(double tol);
  void setLStart(int l);
  void setMaxIt(int max);

 private:
  void extract(Mat* out, Mat in, int nRow, int nCol);
  void extract(Mat* V0, Mat* W0, Mat* S0Inv,
               Mat V, Mat W, std::vector<Complex>& S0D);
  void getB(Mat* B, Mat A1, Mat V0, Mat W0, Mat S0Inv)  const;
  void validate(const std::vector<Complex>& lambda,
                EigEngine& eig,
                Mat V0,
                std::vector<Complex>& valid,
                std::vector<double>& residual)          const;
  void display(const std::vector<Complex>& v,
               const std::vector<double>& r)            const;
  void display(const std::vector<Complex>& v)           const;
  bool isMaster(void)                                   const;
};

#endif
