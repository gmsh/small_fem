#ifndef _CIRCLE_H_
#define _CIRCLE_H_

#include "SmallFem.h"
#include "Options.h"
#include "fullMatrix.h"

void compute(const Options& option,
             const fullVector<Complex>& rhs, fullVector<Complex>& xSF,
             Complex k, bool useRHS);
#endif
