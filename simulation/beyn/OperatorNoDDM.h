#ifndef _OPERATORNODDM_H_
#define _OPERATORNODDM_H_

#include "Operator.h"

class OperatorNoDDM: public Operator{
 private:
  int size;

 public:
  OperatorNoDDM(void);
  virtual ~OperatorNoDDM(void);

  virtual int  getSize(void) const;
  virtual void solve(Vec* x, Vec b, sf::Complex lambda) const;
  virtual void solve(Mat* X, Mat B, sf::Complex lambda) const;
};

#endif
