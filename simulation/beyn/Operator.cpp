#include "Operator.h"
#include "Exception.h"

using namespace sf;

Operator::Operator(void){
}

Operator::~Operator(void){
}

void Operator::getColumn(Mat A, Vec v, int i){
  // Check size
  int nRow, nCol, vSize;
  MatGetSize(A, &nRow,  &nCol);
  VecGetSize(v, &vSize);
  if(vSize != nRow)
    throw Exception("Operator::getColumn: matrix and vector "
                    "don't have the same size");
  // Zeroing v
  VecZeroEntries(v);

  // Column
  MatGetColumnVector(A, v, i);
}

void Operator::setColumn(Vec v, Mat A, int i){
  // Check size
  int nRow, nCol, vSize;
  MatGetSize(A, &nRow,  &nCol);
  VecGetSize(v, &vSize);
  if(vSize != nRow)
    throw Exception("Operator::setColumn: matrix and vector "
                    "don't have the same size");

  // Ownership of each row
  int rowStart, rowEnd, nRowLocal;
  MatGetOwnershipRange(A, &rowStart, &rowEnd);
  nRowLocal = rowEnd-rowStart;

  // Row global indices for each process
  PetscInt* idxRow = new PetscInt[nRowLocal];
  for(int i = 0; i < nRowLocal; i++)
    idxRow[i] = i + rowStart;

  // Set column
  PetscScalar* a;
  VecGetArray(v, &a);
  MatSetValues(A, nRowLocal, idxRow, 1, &i, a, INSERT_VALUES);
  VecRestoreArray(v, &a);

  // Clear
  delete[] idxRow;
}

void Operator::denseAllocate(Mat* A, int nRow, int nCol){
  // Create matrix
  MatCreate(PETSC_COMM_WORLD, A);
  MatSetType(*A, MATMPIAIJ);
  MatSetSizes(*A, PETSC_DECIDE, PETSC_DECIDE, nRow, nCol);
  MatSetUp(*A);

  // Get local column size & perform dense preallocation
  int nColLocal;
  MatGetLocalSize(*A, NULL, &nColLocal);
  MatMPIAIJSetPreallocation(*A, nColLocal, NULL, nCol-nColLocal, NULL);

  // Touch every element
  int rowStart, rowEnd;
  MatGetOwnershipRange(*A, &rowStart, &rowEnd);

  for(int i = rowStart; i < rowEnd; i++)
    for(int j = 0; j < nCol; j++)
      MatSetValue(*A, i, j, 0, INSERT_VALUES);

  // Done
  MatAssemblyBegin(*A, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(  *A, MAT_FINAL_ASSEMBLY);
}

void Operator::multiSolve(Mat* X, Mat B, Complex lambda, const Operator& op){
  // Get size
  int size, nRHS;
  MatGetSize(B, &size,  &nRHS);

  // Allocate X
  denseAllocate(X, size, nRHS);

  // Temp vectors
  Vec b;
  VecCreate(PETSC_COMM_WORLD, &b);
  VecSetSizes(b, PETSC_DECIDE, size);
  VecSetType(b, VECMPI);
  VecSetUp(b);

  // Loop on columns (RHS) & Solve
  Vec x;
  for(int i = 0; i < nRHS; i++){
    getColumn(B, b, i);
    op.solve(&x, b, lambda);
    setColumn(x, *X, i);
    VecDestroy(&x);
  }

  // Assemble X
  MatAssemblyBegin(*X, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(  *X, MAT_FINAL_ASSEMBLY);

  // Clear
  VecDestroy(&b);
}
