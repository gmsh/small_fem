#include "SmallFem.h"
#include "SolverDDM.h"
#include "MPIOStream.h"

#include "DDMContextEMDA.h"
#include "DDMContextOO2.h"
#include "DDMContextJFLee.h"
#include "DDMContextOSRCScalar.h"
#include "DDMContextOSRCVector.h"

#include "System.h"
#include "SystemHelper.h"
#include "FormulationHelper.h"

#include "FormulationOO2.h"
#include "FormulationEMDA.h"
#include "FormulationJFLee.h"
#include "FormulationOSRCScalar.h"
#include "FormulationOSRCVector.h"

#include "FormulationDummy.h"
#include "FormulationSteadyWave.h"
#include "FormulationSource.h"

#include "FormulationUpdateEMDA.h"
#include "FormulationUpdateOO2.h"
#include "FormulationUpdateJFLee.h"
#include "FormulationUpdateOSRCScalar.h"
#include "FormulationUpdateOSRCVector.h"

#include <cmath>
#include <iostream>

using namespace std;
using namespace sf;

static const int    scal  = 0;
static const int    vect  = 1;

Complex fSourceScal(fullVector<double>& xyz){
  return Complex(1, 0);
}

fullVector<Complex> fSourceVect(fullVector<double>& xyz){
  fullVector<Complex> tmp(3);
  tmp.scale(0);

  tmp(0) = Complex(0, 0);
  tmp(1) = Complex(1, 0);
  tmp(2) = Complex(0, 0);

  return tmp;
}

Complex fZeroScal(fullVector<double>& xyz){
  return Complex(0, 0);
}

fullVector<Complex> fZeroVect(fullVector<double>& xyz){
  fullVector<Complex> tmp(3);
  tmp.scale(0);
  return tmp;
}

void getFullB(Vec myB, Vec* allB){
  VecScatter scatter;
  VecScatterCreateToAll(myB, &scatter, allB);
  VecScatterBegin(scatter, myB, *allB, INSERT_VALUES, SCATTER_FORWARD);
  VecScatterEnd(  scatter, myB, *allB, INSERT_VALUES, SCATTER_FORWARD);
  VecScatterDestroy(&scatter);
}

void populateGlobalDofManager(DofManager<Complex>&  gDofM,
                              const FunctionSpace&  fs,
                              const GroupOfElement& goe,
                              const GroupOfElement& fixed){
  // Add all dofs
  set<Dof> local;
  fs.getKeys(goe, local);
  gDofM.addToDofManager(local);

  // Get fixed dofs
  set<Dof> dirichlet;
  fs.getKeys(fixed, dirichlet);
  set<Dof>::iterator it  = dirichlet.begin();
  set<Dof>::iterator end = dirichlet.end();

  for(; it != end; it++)
    gDofM.fixValue(*it, 0);
}

void populateFullLocalDofManager(DofManager<Complex>&  lDofM,
                                 const FunctionSpace&  fs,
                                 const vector<const FunctionSpace*>& aux,
                                 const GroupOfElement& goe,
                                 const GroupOfElement& fixed,
                                 const GroupOfElement& ddmBorder){
  // Add all primary dofs
  set<Dof> pDof;
  fs.getKeys(goe, pDof);
  lDofM.addToDofManager(pDof);

  // Add all auxiliary dofs
  for(size_t i = 0; i < aux.size(); i++){
    set<Dof> aDof;
    aux[i]->getKeys(ddmBorder, aDof);
    lDofM.addToDofManager(aDof);
  }

  // Get fixed dofs
  set<Dof> dirichlet;
  fs.getKeys(fixed, dirichlet);
  set<Dof>::iterator it  = dirichlet.begin();
  set<Dof>::iterator end = dirichlet.end();

  for(; it != end; it++)
    lDofM.fixValue(*it, 0);
}

pair<int, int> getLocalToGlobalMap(const FunctionSpace&  fs,
                                   const GroupOfElement& goe,
                                   const GroupOfElement& fixed,
                                   vector<size_t>& lgMap,
                                   vector<Dof>& lDof){
  // Global Volume Dof Map //
  DofManager<Complex> gDofM(false);
  populateGlobalDofManager(gDofM, fs, goe, fixed);
  gDofM.generateGlobalIdSpace();

  // Local Volume Dof Map //
  DofManager<Complex> lDofM(true);
  populateGlobalDofManager(lDofM, fs, goe, fixed);
  lDofM.generateGlobalIdSpace();

  // Local to Global Map //
  lDof.clear();
  lDofM.populateDofVector(lDof);
  lgMap.resize(lDof.size());
  for(size_t i = 0; i < lgMap.size(); i++)
    lgMap[i] = gDofM.getGlobalId(lDof[i]);

  // Return global size;
  return pair<int, int>(gDofM.getGlobalSize(), gDofM.getLocalSize());
}

int getLocalToBlockMap(const FunctionSpace& fs,
                       const vector<const FunctionSpace*>& aux,
                       const GroupOfElement& goe,
                       const GroupOfElement& fixed,
                       const GroupOfElement& ddmBorder,
                       const vector<Dof>& pDof,
                       vector<size_t>& lbMap){

  // Full local DofManager //
  DofManager<Complex> lDofM;
  populateFullLocalDofManager(lDofM, fs, aux, goe, fixed, ddmBorder);
  lDofM.generateGlobalIdSpace();

  // Populate map //
  int noAuxSize = pDof.size();
  lbMap.resize(noAuxSize);

  for(int i = 0; i < noAuxSize; i++)
    lbMap[i] = lDofM.getGlobalId(pDof[i]);

  // Return full local size
  return lDofM.getLocalSize();
}

void compute(const Options& option,
             Vec bGlobal, Vec* xGlobal, Complex k, bool useRHS){
  // MPI //
  int nProcs;
  int myProc;
  MPIOStream cout(0, std::cout);

  MPI_Comm_size(MPI_COMM_WORLD,&nProcs);
  MPI_Comm_rank(MPI_COMM_WORLD,&myProc);

 // Get Type //
  int type;
  if(option.getValue("-type")[1].compare("scalar") == 0)
    type = scal;

  else if(option.getValue("-type")[1].compare("vector") == 0)
    type = vect;

  else
    throw Exception("Bad -type: %s", option.getValue("-type")[1].c_str());

  // Get Parameters //
  const string ddmType  = option.getValue("-ddm")[1];
  const size_t order    = atoi(option.getValue("-o")[1].c_str());
  const size_t maxIt    = atoi(option.getValue("-max")[1].c_str());

  // DDM Formulations //
  const string emdaType("emda");
  const string oo2Type("oo2");
  const string osrcType("osrc");
  const string jflType("jfl");

  // Variables
  const double Pi = atan(1.0) * 4;
  double lc       = 0;
  double chi      = 0;
  Complex ooA     = 0;
  Complex ooB     = 0;
  int NPade       = 0;
  Complex keps    = 0;

  // EMDA Stuff
  if(ddmType == emdaType)
    chi = 0; //atof(option.getValue("-chi")[1].c_str()) * k;

  // OO2 Stuff
  else if(ddmType == oo2Type){
    lc = atof(option.getValue("-lc")[1].c_str());

    double ooXsiMin = 0;
    double ooXsiMax = Pi / lc;
    double ooDeltaK = Pi / .06;

    Complex tmp0 =
      (k.real() * k.real() - ooXsiMin * ooXsiMin) * (k.real() * k.real() - (k.real() - ooDeltaK) * (k.real() - ooDeltaK));

    Complex tmp1 =
      (ooXsiMax * ooXsiMax - k.real() * k.real()) * ((k.real() + ooDeltaK) * (k.real() + ooDeltaK) - k.real() * k.real());

    Complex ooAlpha = pow(tmp0, 0.25) * Complex(0, 1);
    Complex ooBeta  = pow(tmp1, 0.25);

    ooA = -(ooAlpha * ooBeta - k.real() * k.real()) / (ooAlpha + ooBeta);
    ooB = Complex(-1, 0) / (ooAlpha + ooBeta);
  }

  // OSRC Stuff
  else if(ddmType == osrcType){
    double ck = atof(option.getValue("-ck")[1].c_str());
    NPade     = atoi(option.getValue("-pade")[1].c_str());
    keps      = k + k * ck * Complex(0, 1);
  }

  // Jin Fa Lee Stuff
  else if(ddmType == jflType){
    lc = atof(option.getValue("-lc")[1].c_str());
  }

  // Unknown Stuff
  else
    throw Exception("DDM Circle: Formulation %s is not known", ddmType.c_str());

  // Get Domains //
  Mesh msh(option.getValue("-msh")[1]);
  GroupOfElement volume(msh);
  GroupOfElement wall(msh);
  GroupOfElement ddmBorder(msh);

  // Wall
  if(myProc == nProcs - 1)
    wall.add(msh.getFromPhysical(2000 + nProcs - 1));

  // Volume
  volume.add(msh.getFromPhysical(100 + myProc));

  // DDM border
  if(myProc > 0)
    ddmBorder.add(msh.getFromPhysical(4000 + myProc - 1));

  if(myProc < nProcs - 1)
    ddmBorder.add(msh.getFromPhysical(4000 + myProc));

  // Full Domain //
  vector<const GroupOfElement*> domain(3);
  domain[0] = &volume;
  domain[1] = &wall;
  domain[2] = &ddmBorder;

  // Dirichlet Border //
  vector<const GroupOfElement*> dirichlet(1);
  dirichlet[0] = &wall;

  // Function Space //
  FunctionSpace*                  fs = NULL;
  FunctionSpace*                  fG = NULL;
  vector<const FunctionSpace*> auxFs;

  if(type == scal){
    fs = new FunctionSpace0Form(domain,    order);
    fG = new FunctionSpace0Form(ddmBorder, order);
  }

  else{
    fs = new FunctionSpace1Form(domain,    order);
    fG = new FunctionSpace1Form(ddmBorder, order);
  }

  // OSRC
  vector<const FunctionSpace0Form*> OSRCScalPhi;
  vector<const FunctionSpace1Form*> OSRCVectPhi;
  vector<const FunctionSpace0Form*> OSRCVectRho;
  FunctionSpace1Form*               OSRCVectR = NULL;

  if(ddmType == osrcType && type == scal){
    OSRCScalPhi.resize(NPade);
    auxFs.resize(NPade);

    for(int j = 0; j < NPade; j++){
      OSRCScalPhi[j] = new FunctionSpace0Form(ddmBorder, order);
      auxFs[j]       = OSRCScalPhi[j];
    }
  }

  if(ddmType == osrcType && type == vect){
    OSRCVectPhi.resize(NPade);
    OSRCVectRho.resize(NPade);
    auxFs.resize(2*NPade + 1);

    for(int j = 0; j < NPade; j++){
      OSRCVectPhi[j] = new FunctionSpace1Form(ddmBorder, order);
      auxFs[j] = OSRCVectPhi[j];
    }

    if(order == 0){
      for(int j = 0; j < NPade; j++){
        OSRCVectRho[j] = new FunctionSpace0Form(ddmBorder, 1);
        auxFs[j + NPade] = OSRCVectRho[j];
      }
    }
    else{
      for(int j = 0; j < NPade; j++){
        OSRCVectRho[j] = new FunctionSpace0Form(ddmBorder, order);
        auxFs[j + NPade] = OSRCVectRho[j];
      }
    }

    OSRCVectR = new FunctionSpace1Form(ddmBorder, order);
    auxFs[2*NPade] = OSRCVectR;
  }

  // Jin Fa Lee
  FunctionSpace1Form* JFPhi = NULL;
  FunctionSpace0Form* JFRho = NULL;

  if(ddmType == jflType){
    JFPhi = new FunctionSpace1Form(ddmBorder, order);

    if(order == 0)
      JFRho = new FunctionSpace0Form(ddmBorder, 1);
    else
      JFRho = new FunctionSpace0Form(ddmBorder, order);

    auxFs.resize(2);
    auxFs[0] = JFPhi;
    auxFs[1] = JFRho;
  }

  // Local to Global Map //
  vector<size_t> lgMap;
  vector<Dof>    lDofNoAux;
  pair<int, int> allSize =
    getLocalToGlobalMap(*fs, volume, wall, lgMap, lDofNoAux);

  // Local to Block Map (to reject auxiliary unknowns when populating myB) //
  //                                                                       //
  // Actual RHS with "true" unknowns (x) and auxialiary unknowns (o):      //
  //                 +------+------+------+------+                         //
  //        RHS[i] = | xxxx | oooo | xxxx | oooo |                         //
  //                 +------+------+------+------+                         //
  //                                                                       //
  // What is needed:                                                       //
  //                 +------+------+------+------+                         //
  // RHS[lbMap[i]] = | xxxx | xxxx | oooo | oooo |                         //
  //                 +------+------+------+------+                         //
  vector<size_t> lbMap;
  int fullLocalSize =
    getLocalToBlockMap(*fs, auxFs, volume, wall, ddmBorder, lDofNoAux, lbMap);

  // RHS //
  fullVector<Complex> myB(fullLocalSize);//(allSize.second);
  myB.scale(0);
  Vec bScat;
  if(useRHS)
    getFullB(bGlobal, &bScat);

  for(int i = 0; i < allSize.second; i++){
    int     idx = lgMap[i];
    Complex value;

    if(useRHS)
      VecGetValues(bScat, 1, &idx, &value);
    else
      value = Complex(0, 0);

    myB(lbMap[i]) = value;
  }

  if(useRHS)
    VecDestroy(&bScat);

  // Steady Wave Formulation //
  FormulationSteadyWave<Complex> wave(volume, *fs, k);
  Formulation<Complex>*          vSrc;
  FormulationDummy<Complex>      infinity;

  if(type == scal)
    vSrc = new FormulationSource<Complex>(volume, *fs, fSourceScal);
  else
    vSrc = new FormulationSource<Complex>(volume, *fs, fSourceVect);

  // DDM Solution Map //
  map<Dof, Complex> ddmG;
  map<Dof, Complex> rhsG;
  FormulationHelper::initDofMap(*fG, ddmBorder, ddmG);
  FormulationHelper::initDofMap(*fG, ddmBorder, rhsG);

  // Ddm Formulation //
  DDMContext*         context = NULL;
  Formulation<Complex>*   ddm = NULL;
  Formulation<Complex>* upDdm = NULL;

  if(ddmType == emdaType){
    context = new DDMContextEMDA(ddmBorder, dirichlet, *fs, *fG, k, chi);
    context->setDDMDofs(ddmG);

    ddm     = new FormulationEMDA(static_cast<DDMContextEMDA&>(*context));
    upDdm   = new FormulationUpdateEMDA(static_cast<DDMContextEMDA&>(*context));
  }

  else if(ddmType == oo2Type){
    context = new DDMContextOO2(ddmBorder, dirichlet, *fs, *fG, ooA, ooB);
    context->setDDMDofs(ddmG);

    ddm     = new FormulationOO2(static_cast<DDMContextOO2&>(*context));
    upDdm   = new FormulationUpdateOO2(static_cast<DDMContextOO2&>(*context));
  }

  else if(ddmType == osrcType && type == scal){
    context = new DDMContextOSRCScalar
                                  (ddmBorder, dirichlet, *fs, *fG,
                                   OSRCScalPhi, k, keps, NPade, M_PI / 4.);
    context->setDDMDofs(ddmG);

    ddm     = new FormulationOSRCScalar
                                 (static_cast<DDMContextOSRCScalar&>(*context));
    upDdm   = new FormulationUpdateOSRCScalar
                                 (static_cast<DDMContextOSRCScalar&>(*context));
  }

  else if(ddmType == osrcType && type == vect){
    context = new DDMContextOSRCVector
                                  (ddmBorder, dirichlet, *fs, *fG,
                                   OSRCVectPhi, OSRCVectRho, *OSRCVectR,
                                   k, keps, NPade, M_PI / 2.);
    context->setDDMDofs(ddmG);

    ddm   = new FormulationOSRCVector
                                 (static_cast<DDMContextOSRCVector&>(*context));
    upDdm = new FormulationUpdateOSRCVector
                                 (static_cast<DDMContextOSRCVector&>(*context));
  }

  else if(ddmType == jflType){
    context = new DDMContextJFLee(ddmBorder, dirichlet, *fs, *fG,
                                  *JFPhi, *JFRho, k.real(), lc);
    context->setDDMDofs(ddmG);

    ddm   = new FormulationJFLee(static_cast<DDMContextJFLee&>(*context));
    upDdm = new FormulationUpdateJFLee(static_cast<DDMContextJFLee&>(*context));
  }

  else
    throw Exception("Unknown %s DDM border term", ddmType.c_str());

  // Solve Non homogenous problem //
  //cout << "Solving non homogenous problem" << endl << flush;

  System<Complex>* nonHomogenous = new System<Complex>;
  nonHomogenous->addFormulation(wave);
  nonHomogenous->addFormulation(*ddm);
  //nonHomogenous->addFormulation(*vSrc);

  // Constraint
  if(fs->isScalar())
    SystemHelper<Complex>::dirichlet(*nonHomogenous, *fs, wall, fZeroScal);
  else
    SystemHelper<Complex>::dirichlet(*nonHomogenous, *fs, wall, fZeroVect);

  // Assemble & Solve
  nonHomogenous->assemble();
  nonHomogenous->addToRHS(myB);
  nonHomogenous->solve();

  // Solve Non homogenous DDM problem //
  //cout << "Computing right hand side" << endl << flush;

  context->setSystem(*nonHomogenous);
  upDdm->update(); // update volume solution (at DDM border)

  System<Complex>* nonHomogenousDDM = new System<Complex>;
  nonHomogenousDDM->addFormulation(*upDdm);

  nonHomogenousDDM->assemble();
  nonHomogenousDDM->solve();
  nonHomogenousDDM->getSolution(rhsG, 0);

  // Clear Systems //
  delete nonHomogenous;
  delete nonHomogenousDDM;

  // DDM Solver //
  //cout << "Solving DDM problem" << endl << flush;

  SolverDDM* solver =
    new SolverDDM(wave, infinity, *context, *ddm, *upDdm, rhsG);

  // Solve
  solver->setMaximumIteration(maxIt);
  solver->setRestart(maxIt); // No restart!
  //cout << " ! Warning: no restart ! " << endl;
  solver->solve();

  // Get Solution
  solver->getSolution(ddmG);
  context->setDDMDofs(ddmG);

  // Get history
  vector<double> history;
  solver->getHistory(history);
  cout << history.size() << endl;

  // Clear DDM //
  delete solver;

  // Full Problem //
  //cout << "Solving full problem" << endl << flush;
  ddm->update();

  System<Complex> full;
  full.addFormulation(wave);
  full.addFormulation(*ddm);
  //full.addFormulation(*vSrc);

  // Constraint
  if(fs->isScalar())
    SystemHelper<Complex>::dirichlet(full, *fs, wall, fZeroScal);
  else
    SystemHelper<Complex>::dirichlet(full, *fs, wall, fZeroVect);

  full.assemble();
  full.addToRHS(myB);
  full.solve();

  // Get Solution vector
  fullVector<Complex> mySolution;
  full.getSolution(mySolution, 0);

  VecCreate(MPI_COMM_WORLD, xGlobal);
  VecSetType(*xGlobal, VECMPI);
  VecSetSizes(*xGlobal, PETSC_DECIDE, allSize.first);

  for(int i = 0; i < allSize.second; i++)
    VecSetValue(*xGlobal, lgMap[i], mySolution(lbMap[i]), INSERT_VALUES);
  VecAssemblyBegin(*xGlobal);
  VecAssemblyEnd(*xGlobal);

  // Draw Solution //
  try{
    option.getValue("-nopos");
  }
  catch(...){
    cout << "Writing full problem" << endl << flush;

    stringstream stream;
    stream << "circle" << myProc;

    FEMSolution<Complex> feSol;
    full.getSolution(feSol, *fs, volume);

    feSol.setSaveMesh(false);
    feSol.setBinaryFormat(true);
    feSol.setParition(myProc + 1);
    feSol.write("ddmCircle");
  }

  // Clean //
  delete ddm;
  delete upDdm;
  delete context;
  delete vSrc;
  delete fs;
  delete fG;

  if(JFPhi)
    delete JFPhi;

  if(JFRho)
    delete JFRho;

  if((int)(OSRCScalPhi.size()) == NPade)
    for(int j = 0; j < NPade; j++)
      delete OSRCScalPhi[j];

  if((int)(OSRCVectPhi.size()) == NPade)
    for(int j = 0; j < NPade; j++)
      delete OSRCVectPhi[j];

  if((int)(OSRCVectRho.size()) == NPade)
    for(int j = 0; j < NPade; j++)
      delete OSRCVectRho[j];

  if(OSRCVectR)
    delete OSRCVectR;
}
