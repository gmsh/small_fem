#ifndef _MATH_H_
#define _MATH_H_

#include <complex>

typedef double               Scalar;
typedef std::complex<Scalar> Complex;

class Math{
 public:
  static const Scalar  PI;
  static const Complex  J;
};

#endif
