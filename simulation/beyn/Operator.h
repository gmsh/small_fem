#ifndef _OPERATOR_H_
#define _OPERATOR_H_

#include "SmallFem.h"
#include "petscvec.h"
#include "petscmat.h"

class Operator{
 protected:
  Operator(void);

 public:
  virtual ~Operator(void);

  virtual int  getSize(void) const = 0;
  virtual void solve(Vec* x, Vec b, sf::Complex lambda) const = 0;
  virtual void solve(Mat* X, Mat B, sf::Complex lambda) const = 0;

 protected:
  static void getColumn(Mat A, Vec v, int i);
  static void setColumn(Vec v, Mat A, int i);
  static void denseAllocate(Mat* A, int nRow, int nCol);
  static void multiSolve(Mat* X, Mat B, sf::Complex lambda, const Operator& op);
};

#endif
