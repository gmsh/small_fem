#include "VecTool.h"

void VecTool::newVec(Vec* v, int size, int sLoc){
  VecCreate(PETSC_COMM_WORLD, v);
  VecSetSizes(*v, sLoc, size);
  VecSetType(*v, VECMPI);
  VecSetUp(*v);
}

void VecTool::newVec(Vec* v, int size){
  newVec(v, size, PETSC_DETERMINE);
}
