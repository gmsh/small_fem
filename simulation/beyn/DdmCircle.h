#ifndef _DDMCIRCLE_H_
#define _DDMCIRCLE_H_

#include "SmallFem.h"
#include "Options.h"

void compute(const sf::Options& option,
             Vec bGlobal, Vec* xGlobal, sf::Complex k, bool useRHS);
#endif
