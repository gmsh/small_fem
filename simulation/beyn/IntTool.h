#ifndef _INTTOOL_H_
#define _INTTOOL_H_

#include <vector>
#include "petscmat.h"
#include "Operator.h"
#include "Math.h"

class IntTool{
 public:
  static void path(std::vector<Complex>& myPath, int nNode,
                   double radius, Complex origin);
  static void integrate(Mat* I, const std::vector<Complex>& path,
                        const Operator& op, int power, Mat V);
};

#endif
