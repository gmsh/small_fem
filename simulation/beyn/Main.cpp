#include "SmallFem.h"
#include "OperatorNoDDM.h"
#include "OperatorDDM.h"

#include "Beyn.h"
#include "MatTool.h"
#include "Plot.h"

#include <iostream>
#include <sstream>

using namespace std;
using namespace sf;

void compute(const Options& options){
  //OperatorNoDDM op;
  OperatorDDM op;
  if(SmallFem::isMaster())
    cout << "Problem size: " << op.getSize() << endl;

  Beyn beyn(op);
  vector<Complex> lambda;
  vector<Vec>     v;
  beyn.simple(lambda, v);

  for(size_t i = 0; i < lambda.size(); i++){
    stringstream stream;
    stream << "v" << i+1;
    plot(options, v[i], stream.str());
  }

  for(size_t i = 0; i < lambda.size(); i++){
    VecDestroy(&v[i]);
  }
}

int main(int argc, char** argv){
  // Init SmallFem //
  SmallFem::Keywords("-msh,-o,-type,-max,-ddm,-chi,-lc,-ck,-pade,-n,-nopos");
  SmallFem::Initialize(argc, argv);

  // Compute
  compute(SmallFem::getOptions());

  // Done //
  SmallFem::Finalize();
  return 0;
}
