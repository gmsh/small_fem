#ifndef _SVDENGINE_H_
#define _SVDENGINE_H_

#include <vector>
#include "Math.h"
#include "slepcsvd.h"

class SVDEngine{
 private:
  double TOLZ;
  SVD svdctx;

 public:
   SVDEngine(void);
  ~SVDEngine(void);

  void tolZ(double t);
  void  svd(Mat A);

  void   sv(std::vector<double>& sigma)  const;
  void   sv(std::vector<Complex>& sigma) const;
  void nzsv(std::vector<double>& sigma)  const;
  void nzsv(std::vector<Complex>& sigma) const;
  void   uv(Mat U, Mat V)                const;
  int  rank(void)                        const;

 private:
  void toComplex(const std::vector<double>&  Re,
                       std::vector<Complex>& Cmplx) const;
};

#endif
