#include "Plot.h"
#include "System.h"
#include "SystemHelper.h"

#include "FormulationHelper.h"
#include "FormulationSource.h"
#include "FormulationSilverMuller.h"
#include "FormulationSteadyWave.h"

#include <iostream>

using namespace std;
using namespace sf;

static const int scal = 0;
static const int vect = 1;

Complex fZeroScalPlot(fullVector<double>& xyz){
  return Complex (0, 0);
}

fullVector<Complex> fZeroVectPlot(fullVector<double>& xyz){
  fullVector<Complex> tmp(3);
  tmp.scale(0);
  return tmp;
}

void plot(const Options& option, Vec solV, string name){
  // Vector all to all //
  Vec solVFull;
  VecScatter scatter;
  VecScatterCreateToAll(solV, &scatter, &solVFull);
  VecScatterBegin(scatter, solV, solVFull, INSERT_VALUES, SCATTER_FORWARD);
  VecScatterEnd(  scatter, solV, solVFull, INSERT_VALUES, SCATTER_FORWARD);
  VecScatterDestroy(&scatter);

  // MPI //
  int myProc;
  MPI_Comm_rank(MPI_COMM_WORLD, &myProc);
  if(myProc != 0)
    return;

  // Get Type //
  int type;
  if(option.getValue("-type")[1].compare("scalar") == 0){
    //cout << "Scalar Disc Scattering" << endl << flush;
    type = scal;
  }

  else if(option.getValue("-type")[1].compare("vector") == 0){
    //cout << "Vectorial Disc Scattering" << endl << flush;
    type = vect;
  }

  else
    throw Exception("Bad -type: %s", option.getValue("-type")[1].c_str());

  // Get Parameters //
  const size_t nDom  = atoi(option.getValue("-n")[1].c_str());
  const size_t order = atoi(option.getValue("-o")[1].c_str());

  //cout << "Wavenumber: " << k     << endl
  //     << "Order:      " << order << endl
  //     << "# Domain:   " << nDom  << endl << flush;

  // Get Domains //
  Mesh msh(option.getValue("-msh")[1]);
  GroupOfElement volume(msh);
  GroupOfElement wall(msh);

  // Wall
  wall.add(msh.getFromPhysical(2000 + nDom - 1));

  // Volume
  for(size_t i = 0; i < nDom; i++)
    volume.add(msh.getFromPhysical(100 + i));

  // Full Domain //
  vector<const GroupOfElement*> domain(2);
  domain[0] = &volume;
  domain[1] = &wall;

  // Function Space //
  FunctionSpace* fs = NULL;

  if(type == scal)
    fs = new FunctionSpace0Form(domain, order);
  else
    fs = new FunctionSpace1Form(domain, order);

  // Get dofs and DofManager
  set<Dof> dof;
  DofManager<Complex> dofM;
  fs->getKeys(volume, dof);
  dofM.addToDofManager(dof);

  // Fixed dofs
  set<Dof> fixed;
  fs->getKeys(wall, fixed);
  set<Dof>::iterator it  = fixed.begin();
  set<Dof>::iterator end = fixed.end();

  for(; it != end; it++)
    dofM.fixValue(*it, 0);

  // Get dof IDs
  vector<Dof> dofV;
  dofM.generateGlobalIdSpace();
  dofM.populateDofVector(dofV);

  // Map
  map<Dof, Complex> sol;
  for(int i = 0; i < (int)(dofM.getGlobalSize()); i++){
    Complex tmp;
    VecGetValues(solVFull, 1, &i, &tmp);
    sol.insert(pair<Dof, Complex>(dofV[i], tmp));
  }

  // Plot
  FEMSolution<Complex> feSol;
  feSol.addCoefficients(0, 0, volume, *fs, sol);
  feSol.write(name);
}
