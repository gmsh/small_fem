#include <exception>
#include "EigEngine.h"

using namespace std;

EigEngine::EigEngine(void){
  // Init SLEPc stuff
  EPSCreate(PETSC_COMM_WORLD, &eigctx);
  EPSSetWhichEigenpairs(eigctx, EPS_LARGEST_MAGNITUDE);
  EPSSetProblemType(eigctx, EPS_NHEP);
  EPSSetType(eigctx, "krylovschur");
  EPSSetTolerances(eigctx, 1e-15, 1000);

  // Default TOLZ
  TOLZ = 1e-12;
}

EigEngine::~EigEngine(void){
  EPSDestroy(&eigctx);
}

void EigEngine::tolZ(double t){
  this->TOLZ = t;
}

void EigEngine::eig(Mat A){
  // Number of eigen values
  int size, nCol;
  MatGetSize(A, &size, &nCol);
  if(size != nCol)
    throw runtime_error("EigEngine: matrix not square");

  // Set operator
  EPSSetOperators(eigctx, A, NULL);
  EPSSetDimensions(eigctx, size, PETSC_DECIDE, PETSC_DECIDE);
  EPSSetFromOptions(eigctx);

  // Compute eigenvalues
  EPSSolve(eigctx);

  // Check
  int nConv;
  EPSGetConverged(eigctx, &nConv);
  if(nConv != size)
    throw runtime_error("EigEngine: found less eigenvalues than expected");
}

void EigEngine::val(vector<Complex>& lambda) const{
  // Number of eigenvalues
  int nConv;
  EPSGetConverged(eigctx, &nConv);

  // Clear and resize vector if needed;
  if(nConv != (int)(lambda.size())){
    lambda.clear();
    lambda.resize(nConv);
  }

  // Populate
  for(int i = 0; i < nConv; i++)
    EPSGetEigenpair(eigctx, i, &lambda[i], NULL, NULL, NULL);
}

void EigEngine::vec(Vec x, int i) const{
  EPSGetEigenpair(eigctx, i, NULL, NULL, x, NULL);
}
