#include "OperatorDDM.h"
#include "FunctionSpace.h"
#include "GroupOfElement.h"
#include "DdmCircle.h"

using namespace std;
using namespace sf;

OperatorDDM::OperatorDDM(void){
  // Zero size RHS
  Vec bGlobal;

  VecCreate(MPI_COMM_WORLD, &bGlobal);
  VecSetType(bGlobal, VECMPI);
  VecSetSizes(bGlobal, PETSC_DECIDE, 0);

  // Solve
  Vec xGlobal;
  compute(SmallFem::getOptions(), bGlobal, &xGlobal, 0, false);

  // Get Size
  VecGetSize(xGlobal, &size);

  // Finalize
  GroupOfElement::Finalize();
  FunctionSpace::Finalize();

  // Clear
  VecDestroy(&bGlobal);
  VecDestroy(&xGlobal);
}

OperatorDDM::~OperatorDDM(void){
}

int OperatorDDM::getSize(void) const{
  return size;
}

void OperatorDDM::solve(Vec* x, Vec b, Complex lambda) const{
  // Compute
  compute(SmallFem::getOptions(), b, x, lambda, true);

  // Finalize
  GroupOfElement::Finalize();
  FunctionSpace::Finalize();
}

void OperatorDDM::solve(Mat* X, Mat B, Complex lambda) const{
  // Multisolve
  multiSolve(X, B, lambda, *this);
}
