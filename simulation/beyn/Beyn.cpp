#include <iostream>
#include <iomanip>
#include <list>

#include "Beyn.h"
#include "MatTool.h"
#include "VecTool.h"
#include "IntTool.h"
#include "SVDEngine.h"

using namespace std;

Beyn::Beyn(const Operator& op){
  this->radius  = 0.5;
  //this->origin  = Complex(3.9, 0); /*Complex(2.5, 0);*/ // Scalar
  this->origin  = Complex(3.1, 0); /*Complex(1.8, 0);*/ // Vector
  this->nodes   = 40;
  this->svdTolZ = 1e-3;
  this->eigTolV = 1e6;
  this->lStart  = 3;
  this->lStep   = 1;
  this->maxIt   = 100;

  this->m       = -1;
  this->l       = -1;
  this->k       = -1;
  this->kLoc    = -1;

  this->op      = &op;
}

Beyn::~Beyn(void){
}

void Beyn::simple(vector<Complex>& lambda, vector<Vec>& eigV){
  // Path
  vector<Complex> path;
  IntTool::path(path, nodes, radius, origin);

  // Init
  SVDEngine svd;   svd.tolZ(svdTolZ);
  bool      hasK = false;
  int       it   = 0;
      this->m    = op->getSize();
      this->l    = lStart;

  // Search A0
  Mat VHat, A0;
  while(hasK == false && it != maxIt){
    if(isMaster())
      cout << "Iteration: " << it+1 << endl;    // Iteration count

    MatTool::rand(&VHat, m, l);                 // Take a random VHat
    IntTool::integrate(&A0, path, *op, 0, VHat);// Compute A0
    svd.svd(A0);                                // SVD
    vector<double> s; svd.sv(s);
    if(isMaster())
      cout << "  # " << s[0] << " | " << s[s.size()-1] << endl;
    k = svd.rank();                             // Rank test
    if(k == 0)                                  //  + Found a null rank
      throw runtime_error("Rank negative!");    //    --> Error
    else if(k < l || m == l)                    //  + Found a null SV or full A0
      hasK = true;                              //    --> A0 is good
    else                                        //  + No null SV
      l = l + lStep;                            //    --> Increase l

    if(!hasK) MatDestroy(&VHat);                // Clear
    if(!hasK) MatDestroy(&A0);
    it++;                                       // Keep on searching A0
  }

  // Check if it worked
  if(it == maxIt)
    throw runtime_error("Maximum number of iteration reached!");
  if(isMaster())
    cout << "Done!" << endl
         << "Constructing linear EVP..." << endl;

  // Compute V and W
  Mat V; MatTool::newDense(&V, m, l);
  Mat W; MatTool::newDense(&W, l, l);
  svd.uv(V, W);

  // Extract V0, W0 and S0Inv
  vector<Complex> S0D; svd.nzsv(S0D); // S0D = diag(S, k)
  Mat   V0, W0, S0Inv; extract(&V0, &W0, &S0Inv, V, W, S0D);

  // Compute A1 and B
  Mat A1; IntTool::integrate(&A1, path, *op, 1, VHat);
  Mat B;  getB(&B, A1, V0, W0, S0Inv);

  // Eigenvalues of B
  if(isMaster())
    cout << "Solving linear EVP..." << endl;
  EigEngine eig;
  eig.eig(B);
  eig.val(lambda);
  eigV.resize(lambda.size());

  for(size_t i = 0; i < lambda.size(); i++){
    Vec tmp;
    MatCreateVecs(B, PETSC_NULL, &tmp);
    VecTool::newVec(&eigV[i], this->m);

    eig.vec(tmp, i);
    MatMult(V0, tmp, eigV[i]);

    VecDestroy(&tmp);
  }

  // Validate & display
  vector<Complex> valid;
  vector<double>  residual;
  //validate(lambda, eig, V0, valid, residual);
  if(isMaster())
    //display(valid, residual);
    display(lambda);

  // Clear
  MatDestroy(&VHat);
  MatDestroy(&A0);
  MatDestroy(&V);
  MatDestroy(&W);
  MatDestroy(&V0);
  MatDestroy(&W0);
  MatDestroy(&S0Inv);
  MatDestroy(&A1);
  MatDestroy(&B);
}

void Beyn::setRadius(double R){
  this->radius = R;
}

void Beyn::setNNodes(int n){
  this->nodes = n;
}

void Beyn::setSVDTolZ(double tol){
  this->svdTolZ = tol;
}

void Beyn::setEigTolV(double tol){
  this->eigTolV = tol;
}

void Beyn::setLStart(int l){
  this->lStart = l;
}

void Beyn::setMaxIt(int max){
  this->maxIt = max;
}

void Beyn::extract(Mat* out, Mat in, int nRow, int nCol){
  // Ownership of each row
  int rowStart, rowEnd, nRowLocal;
  MatGetOwnershipRange(in, &rowStart, &rowEnd);

  // Number of local rows to extract
  if(nRow < rowStart)
    // nRow is was previous range
    //  --> I should remove the entire process
    nRowLocal = 0;
  else if(nRow >= rowEnd)
    // Not in my range (in next range)
    //  --> I should take the entire process
    nRowLocal = rowEnd-rowStart;
  else
    // In my range
    //  --> I should take only a part of this process
    nRowLocal = nRow-rowStart;

  // Ownership for each column (diagonal part)
  int colStart, colEnd, nColLocal;
  MatGetOwnershipRangeColumn(in, &colStart, &colEnd);

  // Number of local columns to extract
  if(nCol < colStart)
    // nCol is was previous range
    //  --> I should remove the entire process
    nColLocal = 0;
  else if(nCol >= colEnd)
    // Not in my range (in next range)
    //  --> I should take the entire process
    nColLocal = colEnd-colStart;
  else
    // In my range
    //  --> I should take only a part of this process
    nColLocal = nCol-colStart;

  // Create index sets for extracting sub matrice
  IS isRow; ISCreateStride(PETSC_COMM_WORLD, nRowLocal, rowStart, 1, &isRow);
  IS isCol; ISCreateStride(PETSC_COMM_WORLD, nColLocal, colStart, 1, &isCol);
  //ISView(isRow, PETSC_VIEWER_STDOUT_WORLD);

  // Extract sub matrix
  MatGetSubMatrix(in, isRow, isCol, MAT_INITIAL_MATRIX, out);

  // Free
  ISDestroy(&isRow);
  ISDestroy(&isCol);
}

void Beyn::extract(Mat* V0, Mat* W0, Mat* S0Inv,
                   Mat V, Mat W, vector<Complex>& S0D){
  // Extract V0, W0
  extract(V0, V, m, k); // V0 = V(1:m, 1:k)
  extract(W0, W, l, k); // W0 = W(1:l, 1:k)

  // Local k for S0Inv: kLoc
  MatGetLocalSize(*V0, NULL, &kLoc);

  // Extract S0Inv
  MatTool::newDenseDiagInv(S0Inv, k, S0D, kLoc); // S0Inv = inv(diag(S, 1:k))
}

void Beyn::getB(Mat* B, Mat A1, Mat V0, Mat W0, Mat S0Inv) const{
  // Temp
  Mat tmp0;
  Mat tmp1;

  // Conjugate V0
  MatConjugate(V0);

  // Compute
  MatTransposeMatMult(V0, A1, MAT_INITIAL_MATRIX,
                      PETSC_DEFAULT, &tmp0);     // tmp0 = V0h*A1
  MatMatMult(tmp0, W0, MAT_INITIAL_MATRIX,
             PETSC_DEFAULT, &tmp1);              // tmp1 = (V0h*A1)*W0
  MatMatMult(tmp1, S0Inv, MAT_INITIAL_MATRIX,
             PETSC_DEFAULT, B);                  // B    = ((V0h*A1)*W0)*S0Inv

  // ReConjugate
  MatConjugate(V0);

  // Free
  MatDestroy(&tmp0);
  MatDestroy(&tmp1);
}

//void Beyn::validate(const vector<Complex>& lambda,
//                          EigEngine&eig,
//                          Mat V0,
//                          vector<Complex>& valid,
//                          vector<double>& residual) const{
//  // Init
//  list<Complex> validList;
//  list<double>  residualList;
//  int    nEig = lambda.size();
//  Mat    eval;
//  double norm;
//
//  // Allocate
//  Vec    x; VecTool::newVec(&x,    k, kLoc);
//  Vec proj; VecTool::newVec(&proj, m);
//  Vec  res; VecTool::newVec(&res,  m);
//
//  // Loop on canditates
//  for(int i = 0; i < nEig; i++){
//    op->evaluate(&eval, lambda[i]);   // Evaluate operator at eigenvalue
//    eig.vec(x, i);                    // Take eigenvector
//    MatMult(  V0,    x, proj);        // Project it on V0
//    MatMult(eval, proj,  res);        // Compute residual
//    VecNorm(res, NORM_2, &norm);      // Residual norm
//    if(norm < eigTolV){               // If small enough...
//      validList.push_back(lambda[i]); // ... Accept it :-)!
//      residualList.push_back(norm);
//    }
//    MatDestroy(&eval);
//  }
//
//  // Serialize
//     valid.assign(   validList.begin(),    validList.end());
//  residual.assign(residualList.begin(), residualList.end());
//
//  // Free
//  VecDestroy(&x);
//  VecDestroy(&proj);
//  VecDestroy(&res);
//}

void Beyn::display(const vector<Complex>& v,
                   const vector<double>& r) const{
  cout << "Eigenvalues:" << endl
       << "----------- " << endl;
  for(size_t i = 0; i < v.size(); i++)
    cout << std::scientific << std::showpos << std::setprecision(16)
         << "("   << real(v[i]) << ")" << " + "
         << "("   << imag(v[i]) << ")" << "*i"
         << " : " << r[i]              << endl;
  cout << "----------- " << endl
       << "Found: "      << v.size()   << endl;
}

void Beyn::display(const vector<Complex>& v) const{
  cout << "Eigenvalues:" << endl
       << "----------- " << endl;
  for(size_t i = 0; i < v.size(); i++)
    cout << std::scientific << std::showpos << std::setprecision(16)
         << "("   << real(v[i]) << ")" << " + "
         << "("   << imag(v[i]) << ")" << "*i" << endl;
  cout << "----------- " << endl
       << "Found: "      << v.size()   << endl;
}

bool Beyn::isMaster(void) const{
  int rank;
  MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

  return rank == 0;
}
