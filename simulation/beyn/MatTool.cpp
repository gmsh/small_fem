#include "MatTool.h"
#include "SVDEngine.h"

using namespace std;

void MatTool::newDense( Mat* A, int nRow, int nCol, int nRowLoc, int nColLoc){
  // Create matrix
  MatCreate(PETSC_COMM_WORLD, A);
  MatSetType(*A, MATMPIAIJ);
  MatSetSizes(*A, nRowLoc, nColLoc, nRow, nCol);
  MatSetUp(*A);

  // Get local column size & perform dense preallocation
  int nColLocal;
  MatGetLocalSize(*A, NULL, &nColLocal);
  MatMPIAIJSetPreallocation(*A, nColLocal, NULL, nCol-nColLocal, NULL);

  // Touch every element
  int rowStart, rowEnd;
  MatGetOwnershipRange(*A, &rowStart, &rowEnd);

  for(int i = rowStart; i < rowEnd; i++)
    for(int j = 0; j < nCol; j++)
      MatSetValue(*A, i, j, 0, INSERT_VALUES);

  // Done
  MatAssemblyBegin(*A, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(  *A, MAT_FINAL_ASSEMBLY);
}

void MatTool::newDense(Mat* A, int nRow, int nCol){
  newDense(A, nRow, nCol, PETSC_DECIDE, PETSC_DECIDE);
}

void MatTool::newSparse(Mat* A, int nRow, int nCol){
  MatCreate(PETSC_COMM_WORLD, A);
  MatSetType(*A, MATMPIAIJ);
  MatSetSizes(*A, PETSC_DECIDE, PETSC_DECIDE, nRow, nCol);
  MatSetUp(*A);
}

void MatTool::newDenseDiagInv(Mat* A, int size,
                              const vector<PetscScalar>& v, int sLoc){
  // Init matrix
  newDense(A, size, size, sLoc, sLoc);

  // Get ownership
  int rowStart, rowEnd;
  MatGetOwnershipRange(*A, &rowStart, &rowEnd);

  // Populate diagonal with inverse
  for(int i = rowStart; i < rowEnd; i++)
    MatSetValue(*A, i, i, 1.0/v[i], INSERT_VALUES);

  // Assmeble
  MatAssemblyBegin(*A, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(  *A, MAT_FINAL_ASSEMBLY);
}

void MatTool::read(string filename, Mat* A){
  PetscViewer viewer;

  MatCreate(PETSC_COMM_WORLD, A);
  MatSetType(*A, MATMPIAIJ);
  PetscViewerBinaryOpen(PETSC_COMM_WORLD, filename.c_str(),
                        FILE_MODE_READ,   &viewer);
  MatLoad(*A, viewer);

  PetscViewerDestroy(&viewer);
}

void MatTool::write(string filename, Mat A, string name){
  PetscViewer viewer;

  PetscObjectSetName((PetscObject)(A), name.c_str());
  PetscViewerASCIIOpen(PETSC_COMM_WORLD, filename.c_str(), &viewer);
  PetscViewerPushFormat(viewer, PETSC_VIEWER_ASCII_MATLAB);
  MatView(A, viewer);

  PetscViewerDestroy(&viewer);
}

void MatTool::rand(Mat *A, int nRow, int nCol){
  // Init Rand
  PetscRandom rand;
  PetscRandomCreate(PETSC_COMM_WORLD,&rand);
  PetscRandomSetType(rand,PETSCRAND48);
  PetscRandomSetSeed(rand, time(NULL));
  PetscRandomSeed(rand);

  // Init Matrix
  newDense(A, nRow, nCol);

  // Get ownership
  int rowStart, rowEnd;
  Complex value;
  MatGetOwnershipRange(*A, &rowStart, &rowEnd);

  // Populate with rand numbers
  for(int i = rowStart; i < rowEnd; i++){
    for(int j = 0; j < nCol; j++){
      PetscRandomGetValue(rand, &value);
      MatSetValue(*A, i, j, value, INSERT_VALUES);
    }
  }

  // Assemble & clear
  MatAssemblyBegin(*A, MAT_FINAL_ASSEMBLY);
  PetscRandomDestroy(&rand);
  MatAssemblyEnd(*A, MAT_FINAL_ASSEMBLY);
}
