#include "OperatorNoDDM.h"
#include "GroupOfElement.h"
#include "FunctionSpace.h"
#include "Circle.h"

using namespace std;
using namespace sf;

OperatorNoDDM::OperatorNoDDM(void){
  // Get size //
  fullVector<Complex> xSF;
  fullVector<Complex> rhs;
  compute(SmallFem::getOptions(), rhs, xSF, 0, false);
  MPI_Barrier(MPI_COMM_WORLD);

  // Broadcast //
  size = xSF.size();
  MPI_Bcast(&size, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Barrier(MPI_COMM_WORLD);

  // Finalize //
  GroupOfElement::Finalize();
  FunctionSpace::Finalize();
}

OperatorNoDDM::~OperatorNoDDM(void){
}

int OperatorNoDDM::getSize(void) const{
  return size;
}

void OperatorNoDDM::solve(Vec* x, Vec b, Complex lambda) const{
  // Gather b on all process //
  Vec full;
  VecScatter scatter;
  VecScatterCreateToAll(b, &scatter, &full);
  VecScatterBegin(scatter, b, full, INSERT_VALUES, SCATTER_FORWARD);
  VecScatterEnd(  scatter, b, full, INSERT_VALUES, SCATTER_FORWARD);
  VecScatterDestroy(&scatter);

  // Serialize RHS //
  vector<int> idx(size);
  for(int i = 0; i < size; i++)
    idx[i] = i;

  fullVector<Complex> rhs(size);
  VecGetValues(full, size, idx.data(), rhs.getDataPtr());
  VecDestroy(&full);
  MPI_Barrier(MPI_COMM_WORLD);

  // Start computations //
  fullVector<Complex> xSF;
  compute(SmallFem::getOptions(), rhs, xSF, lambda, true);
  MPI_Barrier(MPI_COMM_WORLD);

  // PETSc solution vector //
  VecCreate(MPI_COMM_WORLD, x);
  VecSetType(*x, VECMPI);
  VecSetSizes(*x, PETSC_DECIDE, size);

  for(int i = 0; i < xSF.size(); i++) // Iterate on LOCAL size (xSF.size())
    VecSetValue(*x, i, xSF(i), INSERT_VALUES);

  VecAssemblyBegin(*x);
  VecAssemblyEnd(*x);
  MPI_Barrier(MPI_COMM_WORLD);

  // Finalize //
  GroupOfElement::Finalize();
  FunctionSpace::Finalize();
}

void OperatorNoDDM::solve(Mat* X, Mat B, Complex lambda) const{
  // Multisolve
  multiSolve(X, B, lambda, *this);
}
