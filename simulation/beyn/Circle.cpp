#include "SmallFem.h"
#include "System.h"
#include "SystemHelper.h"

#include "FormulationHelper.h"
#include "FormulationSource.h"
#include "FormulationSilverMuller.h"
#include "FormulationSteadyWave.h"

#include <iostream>

using namespace std;
using namespace sf;

static const int scal = 0;
static const int vect = 1;

Complex fZeroScal(fullVector<double>& xyz){
  return Complex (0, 0);
}

fullVector<Complex> fZeroVect(fullVector<double>& xyz){
  fullVector<Complex> tmp(3);
  tmp.scale(0);
  return tmp;
}

Complex fSourceScal(fullVector<double>& xyz){
  return Complex (1, 0);
}

fullVector<Complex> fSourceVect(fullVector<double>& xyz){
  fullVector<Complex> tmp(3);

  tmp.scale(0);
  tmp(0) = Complex(0, 0);
  tmp(1) = Complex(1, 0);
  tmp(2) = Complex(0, 0);

  return tmp;
}

void compute(const Options& option,
             const fullVector<Complex>& rhs, fullVector<Complex>& xSF,
             Complex k, bool useRHS){

  // MPI //
  int myProc;
  MPI_Comm_rank(MPI_COMM_WORLD, &myProc);
  if(myProc != 0)
    return;

  // Get Type //
  int type;
  if(option.getValue("-type")[1].compare("scalar") == 0){
    //cout << "Scalar Disc Scattering" << endl << flush;
    type = scal;
  }

  else if(option.getValue("-type")[1].compare("vector") == 0){
    //cout << "Vectorial Disc Scattering" << endl << flush;
    type = vect;
  }

  else
    throw Exception("Bad -type: %s", option.getValue("-type")[1].c_str());

  // Get Parameters //
  const size_t nDom  = atoi(option.getValue("-n")[1].c_str());
  const size_t order = atoi(option.getValue("-o")[1].c_str());

  //cout << "Wavenumber: " << k     << endl
  //     << "Order:      " << order << endl
  //     << "# Domain:   " << nDom  << endl << flush;

  // Get Domains //
  Mesh msh(option.getValue("-msh")[1]);
  GroupOfElement volume(msh);
  GroupOfElement wall(msh);

  // Wall
  wall.add(msh.getFromPhysical(2000 + nDom - 1));

  // Volume
  for(size_t i = 0; i < nDom; i++)
    volume.add(msh.getFromPhysical(100 + i));

  // Full Domain //
  vector<const GroupOfElement*> domain(2);
  domain[0] = &volume;
  domain[1] = &wall;

  // Function Space //
  FunctionSpace* fs = NULL;

  if(type == scal)
    fs = new FunctionSpace0Form(domain, order);
  else
    fs = new FunctionSpace1Form(domain, order);

  // Steady Wave Formulation //
  FormulationSteadyWave<Complex>  wave(volume, *fs, k);
  Formulation<Complex>*           vSrc;

  if(type == scal)
    vSrc = new FormulationSource<Complex>(volume, *fs, fSourceScal);
  else
    vSrc = new FormulationSource<Complex>(volume, *fs, fSourceVect);

  // Solve //
  System<Complex> system;
  system.addFormulation(wave);
  //system.addFormulation(*vSrc);

  // Constraint
  if(fs->isScalar())
    SystemHelper<Complex>::dirichlet(system, *fs, wall, fZeroScal);
  else
    SystemHelper<Complex>::dirichlet(system, *fs, wall, fZeroVect);

  // Assemble
  system.assemble();
  //cout << "Assembled: " << system.getSize() << endl << flush;

  // RHS
  if(useRHS)
    system.addToRHS(rhs);

  // Sove
  system.solve();
  //cout << "Solved!" << endl << flush;

  // Get Solution
  system.getSolution(xSF, 0);

  // Draw Solution //
  try{
    option.getValue("-nopos");
  }

  catch(...){
    cout << "Writing solution..." << endl << flush;

    FEMSolution<Complex> feSol;
    system.getSolution(feSol, *fs, volume);
    feSol.write("disc");
  }

  // Clean //
  delete vSrc;
  delete fs;
}
