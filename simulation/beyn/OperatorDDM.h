#ifndef _OPERATORDDM_H_
#define _OPERATORDDM_H_

#include "Operator.h"

class OperatorDDM: public Operator{
 private:
  int size;

 public:
  OperatorDDM(void);
  virtual ~OperatorDDM(void);

  virtual int  getSize(void) const;
  virtual void solve(Vec* x, Vec b, sf::Complex lambda) const;
  virtual void solve(Mat* X, Mat B, sf::Complex lambda) const;
};

#endif
