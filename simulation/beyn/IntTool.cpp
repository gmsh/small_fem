#include "IntTool.h"
#include "MatTool.h"
#include "Solver.h"
#include <iostream>

using namespace std;

void IntTool::path(vector<Complex>& myPath, int nNode,
                   double radius, Complex origin){
  double step = 1.0 / (double)(nNode);
  int nNodePlusOne = nNode + 1;

  myPath.clear();
  myPath.resize(nNode + 1);

  for(int i = 0; i < nNodePlusOne; i++)
    myPath[i] =
      origin +
      Complex(radius) * exp(Math::J * Complex(2 * Math::PI * i) * step);
}

void IntTool::integrate(Mat* I, const vector<Complex>& path,
                        const Operator& op, int power, Mat V){
  // MPI
  int rank;
  MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

  // Init
  int nRow, nCol;
  Mat F1, F2;//, eval;
  MatGetSize(V, &nRow, &nCol);
  MatTool::newDense(I, nRow, nCol);

  // Init Loop
  //if(rank == 0)
  //  cout << path[0] << endl;
  op.solve(&F1, V, path[0]);
  MatScale(F1, std::pow(path[0], power));

  // Loop over integation points and integrate
  int pathSizeMinus = path.size() - 1;
  for(int i = 0; i < pathSizeMinus; i++){
    //if(rank == 0)
    //  cout << path[i+1] << endl;
    op.solve(&F2, V, path[i+1]);
    MatScale(F2, std::pow(path[i+1], power));

    MatAXPY(*I, path[i+1] - path[i], F1, SAME_NONZERO_PATTERN); // I += F1 * a
    MatAXPY(*I, path[i+1] - path[i], F2, SAME_NONZERO_PATTERN); // I += F2 * a
    MatCopy(F2, F1, SAME_NONZERO_PATTERN);                      // F1 = F2

    MatDestroy(&F2);
  }
  MatDestroy(&F1);

  // Assemble I
  MatAssemblyBegin(*I, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(*I, MAT_FINAL_ASSEMBLY);

  // Final integral value
  MatScale(*I, Complex(0.5) / (Complex(2 * Math::PI) * Math::J));
}
