#include "SmallFem.h"
#include "Timer.h"

#include "ReferenceSpaceLine.h"
#include "ReferenceSpaceTri.h"
#include "ReferenceSpaceQuad.h"
#include "ReferenceSpaceTet.h"
#include "ReferenceSpaceHex.h"
#include "ReferenceSpacePyr.h"
#include "ReferenceSpacePri.h"

#include <iostream>

using namespace std;
using namespace sf;

void compute(const Options& option){
  Timer time;

  cout << "Line: " << flush;
  time.start();
  ReferenceSpaceLine line;
  time.stop();
  cout << line.getNOrientation()
       << " (" << time.time() << " " << time.unit() << ")" << endl << flush;

  cout << "Triangle: " << flush;
  time.start();
  ReferenceSpaceTri tri;
  time.stop();
  cout << tri.getNOrientation()
       << " (" << time.time() << " " << time.unit() << ")" << endl << flush;

  cout << "Quadrangle: " << flush;
  time.start();
  ReferenceSpaceQuad quad;
  time.stop();
  cout << quad.getNOrientation()
       << " (" << time.time() << " " << time.unit() << ")" << endl << flush;

  cout << "Tetrahedron: " << flush;
  time.start();
  ReferenceSpaceTet tet;
  time.stop();
  cout << tet.getNOrientation()
       << " (" << time.time() << " " << time.unit() << ")" << endl << flush;

  cout << "Hexahedron: " << flush;
  time.start();
  ReferenceSpaceHex hex;
  time.stop();
  cout << hex.getNOrientation()
       << " (" << time.time() << " " << time.unit() << ")" << endl << flush;

  cout << "Pyramid: " << flush;
  time.start();
  ReferenceSpacePyr pyr;
  time.stop();
  cout << pyr.getNOrientation()
       << " (" << time.time() << " " << time.unit() << ")" << endl << flush;

  cout << "Prism: " << flush;
  time.start();
  ReferenceSpacePri  pri;
  time.stop();
  cout << pri.getNOrientation()
       << " (" << time.time() << " " << time.unit() << ")" << endl << flush;
}

int main(int argc, char** argv){
  // SmallFEM //
  SmallFem::Keywords("");
  SmallFem::Initialize(argc, argv);

  compute(SmallFem::getOptions());

  SmallFem::Finalize();
  return 0;
}
