#include <iomanip>
#include "SmallFem.h"
#include "Mesh.h"
#include "FunctionSpace0Form.h"
#include "FunctionSpace1Form.h"

#include "System.h"
#include "SystemHelper.h"
#include "SolverBeyn.h"
#include "BeynContext.h"
#include "FormulationBeynWave.h"
#include "FormulationBeynIBCLeontovich.h"

#include "TextSolution.h"
#include "FEMSolution.h"

using namespace std;
using namespace sf;

static const double Pi   = atan(1.0) * 4;
static const double C0   = 299792458;
static const double Mu0  = 4 * Pi * 1e-7;
static const double Eps0 = 1.0 / (Mu0 * C0 * C0);


// SPHERE //
//R=100e-3;
//S=1e15;
//W=(8.225427440398396e09, 2.463612245369305e01);
//
//R=100e-3;
//S=1e12;
//W=(8.225426685971924e09, 7.790625769346680e02);
//
//R=100e-3;
//S=1e09;
//W=(8.225402828912047e09, 2.463610207934244e04);
//
//R=100e-3;
//S=1e06;
//W=(8.224648402450217e09, 7.790422152008058e05);
//
//R=100e-3;
//S=1e03;
//W=(8.200791751916328e09, 2.461613930717722e07);
//
//R=100e-3;
//S=1e00;
//W=(7.457230553910303e+09, 7.713787718642848e+08);

// CIRCLE //
// R=1e-3;
// S=1e15;
// W=(1.1e12, 0);

bool isMaster(void){
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  return rank == 0;
}

void compute(const Options& option){
  // Input data //
  Complex sigma   = Complex(1e09, 0);

  int      lStart = 20;
  int       lStep = 100;
  double  rankTol = 1e-2;

  int      nNode  = 20;
  Complex origin  = Complex(8e9, 2e7);
  double  radius  = 1e9;

  // Get Domain //
  Mesh              msh(option.getValue("-msh")[1]);
  GroupOfElement volume(msh.getFromPhysical(7));
  GroupOfElement border(msh.getFromPhysical(5));

  // Full Domain //
  vector<const GroupOfElement*> domain(2);
  domain[0] = &volume;
  domain[1] = &border;

  // Function space//
  FunctionSpace1Form fs(domain, atoi(option.getValue("-o")[1].c_str()));

  // Beyn Context //
  set<Dof>        bDof; fs.getKeys(volume, bDof);
  BeynContext bContext;
  bContext.setDof(bDof);

  // Formulation //
  FormulationBeynWave        wave(volume, fs, bContext, Mu0, Eps0);
  FormulationBeynIBCLeontovich sL(border, fs, bContext, sigma);

  // System //
  System<Complex> sys;
  sys.addFormulation(wave);
  sys.addFormulation(sL);

  // Beyn Solver //
  SolverBeyn beyn(bContext, sys, nNode, radius, origin);
  beyn.setStartingColumnSize(lStart);
  beyn.setIncreaseColumnStep(lStep);
  beyn.setRankTestRelativeTolerance(rankTol);
  beyn.solve();

  // Get Eigenvalues //
  int                   nEig = beyn.getNComputedSolution();
  fullVector<Complex> lambda;  beyn.getEigenValues(lambda);

  if(isMaster())
    for(int i = 0; i < nEig; i++)
      cout << std::fixed << std::noshowpos
           << " # " << i + 1 << ": "
           << std::scientific << std::showpos << std::setprecision(16)
           << lambda(i) << endl;

  // Post view //
  if(isMaster()){
    try{
      option.getValue("-nopos");
    }
    catch(...){
      // Views
      TextSolution         txSol;
      FEMSolution<Complex> feSol;

      // Sort Dofs
      vector<Dof> dofV(bDof.begin(), bDof.end());
      sys.getDofManager().sortDof(dofV);

      // Iterate on eigenvalues
      int nDof = dofV.size();
      for(int i = 0; i < nEig; i++){
        // Populate <Dof, Value> map
        fullVector<Complex> eig; beyn.getEigenVector(eig, i);
        map<Dof, Complex>   sol;
        for(int j = 0; j < nDof; j++)
          sol.insert(pair<Dof, Complex>(dofV[j], eig(j)));

        // Name
        stringstream stream;
        stream << "Eigenvalue " << i+1 << ": " << lambda(i) << " ";
        txSol.addValues(i*2+0, stream.str().append("[real part]"));
        txSol.addValues(i*2+1, stream.str().append("[imaginary part]"));

        // Field
        feSol.addCoefficients(i, 0, volume, fs, sol);
      }

      // Write
      txSol.write("ibc");
      feSol.write("ibc");
    }
  }
}

int main(int argc, char** argv){
  // Init SmallFem //
  SmallFem::Keywords("-msh,-o,-nopos");
  SmallFem::Initialize(argc, argv);

  compute(SmallFem::getOptions());

  SmallFem::Finalize();
  return 0;
}
