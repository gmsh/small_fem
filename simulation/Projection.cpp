#include "SmallFem.h"
#include "Mesh.h"
#include "System.h"
#include "FEMSolution.h"
#include "Interpolator.h"
#include "Evaluator.h"
#include "Integrator.h"
#include "FunctionSpace0Form.h"
#include "FunctionSpace1Form.h"
#include "FormulationProjection.h"

#include "gmsh/fullMatrix.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cmath>

using namespace std;
using namespace sf;

//////////////////////////////////////////////
// Functions to project (scalar and vector) //
//////////////////////////////////////////////

// !WARNING!: The analytical solution is ALLWAYS 3D,
//            this may corrupt the error of a 2D FEM projection!!!

Complex fScal(fullVector<double>& xyz){
  return Complex(1, 1) * (sin(10 * xyz(0)) +
                          sin(10 * xyz(1)) +
                          sin(10 * xyz(2)));
}

fullVector<Complex> fVect(fullVector<double>& xyz){
  fullVector<Complex> res(3);

  res(0) = Complex(1, 1) * sin(10 * xyz(0));
  res(1) = Complex(1, 1) * sin(10 * xyz(1));
  res(2) = Complex(1, 1) * sin(10 * xyz(2));

  return res;
}

/////////////////////////////////////
// Prototypes of helping functions //
/////////////////////////////////////

// FEM solution (scalar and vector) //
int  fem(Complex (*f)(fullVector<double>& xyz),
         GroupOfElement& domain,
         string family,
         size_t order,
         vector<pair<const MElement*, fullMatrix<double> > >& qP,
         vector<pair<const MElement*, fullMatrix<Complex> > >& fSol,
         bool nopos,
         const Options& option);

int  fem(fullVector<Complex> (*f)(fullVector<double>& xyz),
         GroupOfElement& domain,
         string family,
         size_t order,
         vector<pair<const MElement*, fullMatrix<double> > >& qP,
         vector<pair<const MElement*, fullMatrix<Complex> > >& fSol,
         bool nopos,
         const Options& option);

// Write Octave file //
void write(bool isScalar, const fullMatrix<double>& l2, string name);

////////////////////////////////////
// SmallFem Computations and main //
////////////////////////////////////

void compute(const Options& option){
  // Get integration order for computing L2 Error //
  const size_t iO = atoi(option.getValue("-l2")[1].c_str());
  cout << "## Quadrature order for L2 error integration: " << iO << endl;

  // Get FEM Orders //
  const size_t nOrder = option.getValue("-o").size() - 1;
  vector<int>   order(nOrder);

  for(size_t i = 0; i < nOrder; i++)
    order[i] = atoi(option.getValue("-o")[i + 1].c_str());

  // Get FEM Meshes //
  const size_t  nMesh = option.getValue("-msh").size() - 1;
  vector<string> mesh(nMesh);

  for(size_t i = 0; i < nMesh; i++)
    mesh[i] = option.getValue("-msh")[i + 1];

  // Post Processing ? //
  bool nopos;
  try{
    option.getValue("-nopos");
    nopos = 1;
  }
  catch(Exception& ex){
    nopos = 0;
  }

  // Scalar or Vector //
  string   type = option.getValue("-type")[1];
  string family;
  bool isScalar;

  if(type.compare("scalar") == 0){
    isScalar = true;
    family   = Basis::Family::Hierarchical;
  }
  else if(type.compare("lagrange") == 0){
    isScalar = true;
    family   = Basis::Family::Lagrange;
  }
  else if(type.compare("vector") == 0){
    isScalar = false;
    family   = Basis::Family::Hierarchical;
  }
  else
    throw Exception("Unknown type: %s", type.c_str());

  cout << "## Family (is scalar?): " << family << " (" << isScalar << ")"
       << endl;

  // Analytical solution & FEM Solution & L2 Error //
  cout << "## FEM Solutions" << endl << flush;
  fullMatrix<double> l2(nOrder, nMesh);

  // Iterate on Meshes
  for(size_t i = 0; i < nMesh; i++){
    cout << " ** Mesh: " << mesh[i] << endl << flush;
    Mesh           msh(mesh[i]);
    GroupOfElement domain = msh.getFromPhysical(7);

    // Integrator & quadrature points
    vector<pair<const MElement*, fullMatrix<double> > > qP;
    Integrator<Complex> integrator(domain, iO);
    integrator.getQuadraturePoints(qP);

    // Analytical solution
    vector<pair<const MElement*, fullMatrix<Complex> > > aSol;
    if(isScalar)
      Evaluator<Complex>::evaluate(fScal, qP, aSol);
    else
      Evaluator<Complex>::evaluate(fVect, qP, aSol);

    // Iterate on Orders
    for(size_t j = 0; j < nOrder; j++){
      vector<pair<const MElement*, fullMatrix<Complex> > > fSol;
      int size;
      cout << "  -- Order " << order[j] << " " << flush;

      // FEM
      if(isScalar)
        size = fem(fScal, domain, family, order[j], qP, fSol, nopos, option);
      else
        size = fem(fVect, domain, family, order[j], qP, fSol, nopos, option);

      cout << " (" << size << "): " << flush;

      // L2 Error
      l2(j, i) = Evaluator<Complex>::l2Error(aSol, fSol, integrator).real();
      cout << l2(j, i) << endl << flush;
    }
  }

  // Display //
  cout << "## L2 Error" << endl << flush;
  write(isScalar, l2, option.getValue("-name")[1]);
}

int main(int argc, char** argv){
  // Init SmallFem //
  SmallFem::Keywords("-msh,-o,-type,-l2,-nopos,-name,-solver,-blr");
  SmallFem::Initialize(argc, argv);

  compute(SmallFem::getOptions());

  SmallFem::Finalize();
  return 0;
}

///////////////////////////////////////
// Implentation of helping functions //
///////////////////////////////////////

int  fem(Complex (*f)(fullVector<double>& xyz),
         GroupOfElement& domain,
         string family,
         size_t order,
         vector<pair<const MElement*, fullMatrix<double> > >& qP,
         vector<pair<const MElement*, fullMatrix<Complex> > >& fSol,
         bool nopos,
         const Options& option){
  // Which solver ? //
  string solver;
  try{
    solver = option.getValue("-solver")[1];
  }
  catch(...){
    solver = string("mumps");
  }

  // Projection //
  stringstream stream;

  FunctionSpace0Form fSpace(domain, order, family);
  FormulationProjection<Complex> projection(domain, fSpace, f);
  System<Complex> sysProj(solver);

  sysProj.addFormulation(projection);

  // BLR ?
  try{
    sysProj.getSolver().setOption("blr", option.getValue("-blr")[1].c_str());
  }
  catch(...){
  }

  // Assemble and Solve //
  sysProj.assemble(); cout << "." << flush;
  sysProj.solve();    cout << "." << flush;

  // Get Dofs //
  set<Dof> dof;
  fSpace.getKeys(domain, dof);

  set<Dof>::iterator    end = dof.end();
  set<Dof>::iterator     it = dof.begin();
  map<Dof, Complex>   sysSol;

  for(; it != end; it++)
    sysSol.insert(pair<Dof, Complex>(*it, 0));

  // Get Solution //
  sysProj.getSolution(sysSol, 0);

  // Interpolate FEM solution
  Interpolator<Complex>::interpolate(fSpace, sysSol, qP, fSol);

  // Post-processing //
  if(!nopos){
    FEMSolution<Complex> feSol;
    stream << "projection_Mesh" << domain.getNumber() << "_Order" << order;

    sysProj.getSolution(feSol, fSpace, domain);
    feSol.write(stream.str());
  }

  // Return size //
  return sysProj.getSize();
}

int  fem(fullVector<Complex> (*f)(fullVector<double>& xyz),
         GroupOfElement& domain,
         string family,
         size_t order,
         vector<pair<const MElement*, fullMatrix<double> > >& qP,
         vector<pair<const MElement*, fullMatrix<Complex> > >& fSol,
         bool nopos,
         const Options& option){
  // Which solver ? //
  string solver;
  try{
    solver = option.getValue("-solver")[1];
  }
  catch(...){
    solver = string("mumps");
  }

  // Projection //
  stringstream stream;

  FunctionSpace1Form fSpace(domain, order, family);
  FormulationProjection<Complex> projection(domain, fSpace, f);
  System<Complex> sysProj(solver);

  sysProj.addFormulation(projection);

  // BLR ?
  try{
    sysProj.getSolver().setOption("blr", option.getValue("-blr")[1].c_str());
  }
  catch(...){
  }

  // Assemble and Solve //
  sysProj.assemble(); cout << "." << flush;
  sysProj.solve();    cout << "." << flush;

  // Get Dofs //
  set<Dof> dof;
  fSpace.getKeys(domain, dof);

  set<Dof>::iterator    end = dof.end();
  set<Dof>::iterator     it = dof.begin();
  map<Dof, Complex>   sysSol;

  for(; it != end; it++)
    sysSol.insert(pair<Dof, Complex>(*it, 0));

  // Get Solution //
  sysProj.getSolution(sysSol, 0);

  // Interpolate FEM solution
  Interpolator<Complex>::interpolate(fSpace, sysSol, qP, fSol);

  // Post-processing //
  if(!nopos){
    FEMSolution<Complex> feSol;
    stream << "projection_Mesh" << domain.getNumber() << "_Order" << order;

    sysProj.getSolution(feSol, fSpace, domain);
    feSol.write(stream.str());
  }

  // Return size //
  return sysProj.getSize();
}

void write(bool isScalar, const fullMatrix<double>& l2, string name){
  // Stream
  ofstream stream;
  string   fileName;
  if(isScalar)
    fileName = name + "Node.m";
  else
    fileName = name + "Edge.m";

  stream.open(fileName.c_str());

  // Matrix data
  const size_t l2Row      = l2.size1();
  const size_t l2ColMinus = l2.size2() - 1;

  // Clean Octave
  stream << "close all;" << endl
         << "clear all;" << endl
         << endl;

  // Mesh (Assuming uniform refinement)
  stream << "h = [1, ";
  for(size_t i = 1; i < l2ColMinus; i++)
    stream << 1 / pow(2, i) << ", ";

  stream << 1 / pow(2, l2ColMinus) << "];" << endl;

  // Order (Assuming uniform refinement)
  stream << "p = [1:" << l2Row << "];" << endl
         << endl;

  // Matrix of l2 error (l2[Order][Mesh])
  stream << "l2 = ..." << endl
         << "    [..." << endl;

  for(size_t i = 0; i < l2Row; i++){
    stream << "        ";

    for(size_t j = 0; j < l2ColMinus; j++)
      stream << scientific << showpos
             << l2(i, j) << " , ";

    stream << scientific << showpos
           << l2(i, l2ColMinus) << " ; ..." << endl;
  }

  stream << "    ];" << endl
         << endl;

  // Delta
  stream << "P = size(p, 2);"                                   << endl
         << "H = size(h, 2);"                                   << endl
         << endl
         << "delta = zeros(P, H - 1);"                          << endl
         << endl
         << "for i = 1:H-1"                                     << endl
         << "  delta(:, i) = ..."                               << endl
         << "    (log10(l2(:, i + 1)) - log10(l2(:, i))) / ..." << endl
         << "    (log10(1/h(i + 1))   - log10(1/h(i)));"        << endl
         << "end"                                               << endl
         << endl
         << "delta"                                             << endl
         << endl;

  // Plot
  stream << "figure;"                  << endl
         << "loglog(1./h, l2', '-*');" << endl
         << "grid;"
         << endl;

  // Title
  stream << "title(" << "'" << name << ": ";
  if(isScalar)
    stream << "Nodal";
  else
    stream << "Edge";

  stream << "');" << endl
         << endl;

  // Axis
  stream << "xlabel('1/h [-]');"      << endl
         << "ylabel('L2 Error [-]');" << endl;
  // Close
  stream.close();
}
