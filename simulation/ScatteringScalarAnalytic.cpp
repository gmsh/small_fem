#include "SmallFemConfig.h"
#ifdef HAVE_GSL

#include "SmallFem.h"
#include "Exception.h"
#include "GroupOfElement.h"
#include "NodeSolution.h"
#include <iostream>
#include <gsl/gsl_sf_bessel.h>

using namespace std;
using namespace sf;

static const Complex J = Complex(0, 1);
static double  k;
static double R0;
static double R1;
static int  type;

static Complex bessel1(int n, double x){
  return Complex(gsl_sf_bessel_Jn(n, x), 0);
}

static Complex bessel2(int n, double x){
  return Complex(gsl_sf_bessel_Yn(n, x), 0);
}

static Complex hankel1(int n, double x){
  return bessel1(n, x) + J * bessel2(n, x);
}

static Complex hankel2(int n, double x){
  return bessel1(n, x) - J * bessel2(n, x);
}

static Complex derivH(int n, double x, vector<Complex>& fn, int nMax){
  Complex fn0 = fn.at(n+0+nMax);
  Complex fn1 = fn.at(n+1+nMax);
  return Complex(n, 0) * fn0 / Complex(x, 0) - fn1;
}

static Complex derivJ(int n, double x, vector<Complex>& fn, int nMax){
  Complex fnM = fn.at(n-1 + nMax+1);
  Complex fnP = fn.at(n+1 + nMax+1);
  return Complex(0.5, 0) * (fnM - fnP);
}

static Complex unSoft(int n, double r, double phi,
                      vector<Complex>& H1R0, vector<Complex>& H2R0,
                      vector<Complex>& H1R1, vector<Complex>& H2R1,
                      vector<Complex>& J1R0,
                      int nMax){
  Complex alpha = 0;
  Complex beta  = -J*k;

  Complex bo  = -alpha*Complex(n*n, 0) / (R1*R1) - beta;
  Complex A11 = H1R0[n+nMax];
  Complex A12 = H2R0[n+nMax];
  Complex A21 = k*derivH(n, k*R1, H1R1, nMax) - bo*H1R1[n+nMax];
  Complex A22 = k*derivH(n, k*R1, H2R1, nMax) - bo*H2R1[n+nMax];

  Complex d = A11*A22 - A21*A12;

  Complex a = -A22*real(+A11) / d;
  Complex b = -A21*real(-A12) / d;

  return (a*hankel1(n, k*r) + b*hankel2(n, k*r)) * exp(J*(n*phi));
}

static Complex unHard(int n, double r, double phi,
                      vector<Complex>& H1R0, vector<Complex>& H2R0,
                      vector<Complex>& H1R1, vector<Complex>& H2R1,
                      vector<Complex>& J1R0,
                      int nMax){
  Complex alpha = 0;
  Complex beta  = -J*k;

  Complex bo  = -alpha*Complex(n*n, 0) / (R1*R1) - beta;
  Complex A11 =   derivH(n, k*R0, H1R0, nMax);
  Complex A12 =   derivH(n, k*R0, H2R0, nMax);
  Complex A21 = k*derivH(n, k*R1, H1R1, nMax) - bo*H1R1[n+nMax];
  Complex A22 = k*derivH(n, k*R1, H2R1, nMax) - bo*H2R1[n+nMax];

  Complex d = A11*A22 - A21*A12;

  Complex a = -A22*derivJ(n, k*R0, J1R0, nMax) / d;
  Complex b = +A21*derivJ(n, k*R0, J1R0, nMax) / d;

  return (a*hankel1(n, k*r) + b*hankel2(n, k*r)) * exp(J*(n*phi));
}

Complex fAnalytic(fullVector<double>& xyz){
  double  r   =  sqrt(xyz(0)*xyz(0) + xyz(1)*xyz(1));
  double  phi = atan2(xyz(1)        , xyz(0));
  int    nMax = int(k*R0) + 30;

  vector<Complex> H1R0(2*nMax+1 + 1); // +1 for derivatives
  for(int n = -nMax; n <= nMax+1; n++)
    H1R0[n+nMax] = hankel1(n, k*R0);

  vector<Complex> H2R0(2*nMax+1 + 1); // +1 for derivatives
  for(int n = -nMax; n <= nMax+1; n++)
    H2R0[n+nMax] = hankel2(n, k*R0);

  vector<Complex> H1R1(2*nMax+1 + 1); // +1 for derivatives
  for(int n = -nMax; n <= nMax+1; n++)
    H1R1[n+nMax] = hankel1(n, k*R1);

  vector<Complex> H2R1(2*nMax+1 + 1); // +1 for derivatives
  for(int n = -nMax; n <= nMax+1; n++)
    H2R1[n+nMax] = hankel2(n, k*R1);

  // Precompute Bessel
  vector<Complex> J1R0(2*nMax+1 + 2, 0); // +2 for derivatives
  for(int n = -nMax; n <= nMax+2; n++)
    J1R0[n+nMax] = bessel1(n-1, k*R0);

  // Compute solution
  Complex ret = Complex(0, 0);
  if(type == 0)
    for(int n = -nMax; n <= nMax; n++)
      ret += pow(J, n) * unHard(n, r, phi, H1R0, H2R0, H1R1, H2R1, J1R0, nMax);
  else
    for(int n = -nMax; n <= nMax; n++)
      ret += pow(J, n) * unSoft(n, r, phi, H1R0, H2R0, H1R1, H2R1, J1R0, nMax);

  return ret;
}

void compute(const Options& option){
  // Get Parameters //
  k  = atof(option.getValue("-k")[1].c_str());
  R0 = atof(option.getValue("-R0")[1].c_str());
  R1 = atof(option.getValue("-R1")[1].c_str());

  if(option.getValue("-type")[1].compare("hard") == 0){
    cout << "Hard scattering" << endl << flush;
    type = 0;
  }
  else if(option.getValue("-type")[1].compare("soft") == 0){
    cout << "Soft scattering" << endl << flush;
    type = 1;
  }
  else
    throw Exception("Bad -type: %s", option.getValue("-type")[1].c_str());

  // Get Domains //
  Mesh              msh(option.getValue("-msh")[1]);
  GroupOfElement volume(msh.getFromPhysical(1));

  // Grab solution //
  MapVertex vertex;
  volume.getAllVertex(vertex);

  MapVertex::iterator it  = vertex.begin();
  MapVertex::iterator end = vertex.end();

  map<const MVertex*, vector<Complex> > values;
  for(; it != end; it++){
    fullVector<double> xyz(3);
    xyz(0) = it->first->x();
    xyz(1) = it->first->y();
    xyz(2) = it->first->z();

    vector<Complex> f(1, fAnalytic(xyz));
    values.insert(pair<const MVertex*, vector<Complex> >(it->first, f));
  }

  // Drawing name //
  stringstream name;
  try{
    name << option.getValue("-name")[1];
  }
  catch(...){
    name << "discAna";
  }

  // Draw //
  NodeSolution<Complex> view;
  view.addNodeValue(0, 0, msh, values);
  view.write(name.str());
}

int main(int argc, char** argv){
  // Init SmallFem //
  SmallFem::Keywords("-msh,-k,-R0,-R1,-name,-type");
  SmallFem::Initialize(argc, argv);

  compute(SmallFem::getOptions());

  SmallFem::Finalize();
  return 0;
}

#else

#include "Exception.h"
int main(int argc, char** argv){
  throw sf::Exception("GSL not activated");
  return 0;
}

#endif
