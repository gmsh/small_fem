#include "SmallFemConfig.h"
#ifdef HAVE_GSL

#include "SmallFem.h"
#include "System.h"
#include "SystemHelper.h"
#include "Interpolator.h"
#include "Integrator.h"
#include "Evaluator.h"

#include "FormulationHelper.h"
#include "FormulationSilverMuller.h"
#include "FormulationSource.h"
#include "FormulationSteadyWave.h"

#include <gsl/gsl_sf_bessel.h>
#include <fstream>
#include <iostream>

using namespace std;
using namespace sf;

// Some static values //
////////////////////////
static const Complex J = Complex(0, 1);
static double  k;
static double R0;
static double R1;
static int  type;

// Prototypes (see end of file for implementation) //
/////////////////////////////////////////////////////
// Dirichlet and Neumann
Complex fDirichlet(fullVector<double>& xyz);
Complex fNeumann(const MElement& element, fullVector<double>& xyz);

// Analytical solution
Complex   bessel1(int n, double x);
Complex   bessel2(int n, double x);
Complex   hankel1(int n, double x);
Complex   hankel2(int n, double x);
Complex     deriv(int n, double x, vector<Complex>& fn, int nMax);
Complex    unSoft(int n, double r, double phi,
                  vector<Complex>& H1R0, vector<Complex>& H2R0,
                  vector<Complex>& H1R1, vector<Complex>& H2R1,
                  vector<Complex>& J1R0,
                  int nMax);
Complex    unHard(int n, double r, double phi,
                  vector<Complex>& H1R0, vector<Complex>& H2R0,
                  vector<Complex>& H1R1, vector<Complex>& H2R1,
                  vector<Complex>& J1R0,
                  int nMax);
Complex fAnalytic(fullVector<double>& xyz);

// Computations //
//////////////////
void compute(const Options& option){
  // Get Parameters //
  const size_t order = atoi(option.getValue("-o")[1].c_str());
  k                  = atof(option.getValue("-k")[1].c_str());

  cout << "Scalar scattering" << endl;
  if(option.getValue("-type")[1].compare("hard") == 0){
    cout << "Hard scattering" << endl << flush;
    type = 0;
  }
  else if(option.getValue("-type")[1].compare("soft") == 0){
    cout << "Soft scattering" << endl << flush;
    type = 1;
  }
  else
    throw Exception("Bad -type: %s", option.getValue("-type")[1].c_str());

  cout << "Wavenumber: " << k     << endl
       << "Order:      " << order << endl << flush;

  // Get Domains //
  Mesh                msh(option.getValue("-msh")[1]);
  GroupOfElement   volume(msh.getFromPhysical(1));
  GroupOfElement   source(msh.getFromPhysical(2));
  GroupOfElement infinity(msh.getFromPhysical(3));

  // Full Domain //
  vector<const GroupOfElement*> domain(3);
  domain[0] = &volume;
  domain[1] = &source;
  domain[2] = &infinity;

  // Function Space //
  FunctionSpace0Form fs(domain, order);

  // Steady Wave Formulation //
  FormulationSteadyWave<Complex>                wave(volume,   fs, k);
  FormulationSilverMuller                        abc(infinity, fs, k);
  FormulationSource<Complex>*                    src = NULL;
  if(type == 0) src = new FormulationSource<Complex>(source,   fs, fNeumann);

  // Solve //
  System<Complex> system;
  system.addFormulation(wave);
  system.addFormulation(abc);
  if(type == 0) system.addFormulation(*src);

  // Constraint
  if(type == 1) SystemHelper<Complex>::dirichlet(system, fs, source, fDirichlet);

  // Assemble
  system.assemble();
  cout << "Assembled: " << system.getSize() << endl << flush;

  // Sove
  system.solve();
  cout << "Solved!" << endl << flush;

  // Draw Solution //
  try{
    option.getValue("-nopos");
  }
  catch(...){
    cout << "Writing solution..." << endl << flush;

    stringstream stream;
    try{
      stream << option.getValue("-name")[1];
    }
    catch(...){
      stream << "discFem";
    }

    FEMSolution<Complex> feSol;
    system.getSolution(feSol, fs, domain);
    feSol.write(stream.str());
  }

  // L2 Error //
  try{
    // Get integrator order
    const size_t iO = atoi(option.getValue("-l2")[1].c_str());
    cout << "L2 Error (Quadrature " << iO << ")..." << endl;

    // Get Analytical parameters
    R0 = atof(option.getValue("-R0")[1].c_str());
    R1 = atof(option.getValue("-R1")[1].c_str());

    // Get integrator & quadrature points
    vector<pair<const MElement*, fullMatrix<double> > > qP;
    Integrator<Complex> integrator(volume, iO);
    integrator.getQuadraturePoints(qP);

    // Analytic value
    vector<pair<const MElement*, fullMatrix<Complex> > > anaValue;
    Evaluator<Complex>::evaluate(fAnalytic, qP, anaValue);

    // FEM solution
    set<Dof> dof;
    fs.getKeys(volume, dof);

    set<Dof>::iterator end = dof.end();
    set<Dof>::iterator  it = dof.begin();
    map<Dof, Complex>  sol;

    for(; it != end; it++)
      sol.insert(pair<Dof, Complex>(*it, 0));

    system.getSolution(sol, 0);

    // Interpolate FEM solution
    vector<pair<const MElement*, fullMatrix<Complex> > > femValue;
    Interpolator<Complex>::interpolate(fs, sol, qP, femValue);

    // L2 Error
    cout << "L2 Error: " << scientific
         << Evaluator<Complex>::l2Error(anaValue, femValue, integrator) << endl;
  }
  catch(...){
  }

  // Free
  if(src)
    delete src;
}

// Main //
//////////
int main(int argc, char** argv){
  // Init SmallFem //
  SmallFem::Keywords("-msh,-o,-k,-l2,-R0,-R1,-name,-nopos,-type");

  SmallFem::Initialize(argc, argv);

  compute(SmallFem::getOptions());

  SmallFem::Finalize();
  return 0;
}

// Helper functions //
//////////////////////
// Dirichlet and Neumann
Complex fDirichlet(fullVector<double>& xyz){
  double theta = 0;
  double p     = xyz(0) * cos(theta) + xyz(1) * sin(theta);

  return -exp(J*k*p);
}

Complex fNeumann(const MElement& element,
                 fullVector<double>& xyz){
  // Get local coordinates //
  double uvw[3];
  element.xyz2uvw(xyz.getDataPtr(), uvw);

  // Get jacobian matrix at uvw //
  fullMatrix<double> jac(3, 3);
  jac.scale(0);
  element.getJacobian(uvw[0], uvw[1], uvw[2], jac);

  // Get jacobian first column (tangent vector in xyz) //
  fullVector<double> t(3);
  t(0) = jac(0, 0);
  t(1) = jac(0, 1);
  t(2) = jac(0, 2);

  // Normal vector in xyz //
  fullVector<double> ez(3);
  ez(0) = 0;
  ez(1) = 0;
  ez(2) = 1;

  fullVector<double> n(3);
  prodve(t.getDataPtr(), ez.getDataPtr(), n.getDataPtr());
  /*
  // With vertices //
  int nVert = element.getNumVertices();
  vector<const MVertex*> v(nVert, NULL);
  for(int i = 0; i < nVert; i++)
    v[i] = element.getVertex(i);

  n(0) = v[1]->y() - v[0]->y();
  n(1) = v[0]->x() - v[1]->x();
  n(2) = 0.0;
  */
  // Normalize normal //
  double norm = n.norm();
  n(0) = n(0) / norm;
  n(1) = n(1) / norm;
  n(2) = n(2) / norm;

  // Complex normal vector //
  //fullVector<Complex> nC(3);
  //nC(0) = Complex(n(0), 0);
  //nC(1) = Complex(n(1), 0);
  //nC(2) = Complex(n(2), 0);

  // uInc //
  double theta = 0;
  double p     = xyz(0) * cos(theta) + xyz(1) * sin(theta);
  Complex uInc = -exp(J*k*p);

  // grad(uInc) //
  fullVector<Complex> grad(3);
  grad(0) = Complex(0, 1) * k * uInc * cos(theta);
  grad(1) = Complex(0, 1) * k * uInc * sin(theta);
  grad(2) = Complex(0, 0);

  // dnu //
  Complex dnU = grad(0) * n(0) + grad(1) * n(1) + grad(2) * n(2);

  //  cout << element.getNum() << ": "
  //       << n(0) << ", " << n(1) << ", " << n(2) << endl;

  // Done //
  return -dnU;
}

// Analytical solution
Complex bessel1(int n, double x){
  return Complex(gsl_sf_bessel_Jn(n, x), 0);
}

Complex bessel2(int n, double x){
  return Complex(gsl_sf_bessel_Yn(n, x), 0);
}

Complex hankel1(int n, double x){
  return bessel1(n, x) + J * bessel2(n, x);
}

Complex hankel2(int n, double x){
  return bessel1(n, x) - J * bessel2(n, x);
}

Complex derivH(int n, double x, vector<Complex>& fn, int nMax){
  Complex fn0 = fn[n+0 + nMax];
  Complex fn1 = fn[n+1 + nMax];
  return Complex(n, 0) * fn0 / Complex(x, 0) - fn1;
}

Complex derivJ(int n, double x, vector<Complex>& fn, int nMax){
  Complex fnM = fn[n-1 + nMax+1];
  Complex fnP = fn[n+1 + nMax+1];
  return Complex(0.5, 0) * (fnM - fnP);
}

Complex unSoft(int n, double r, double phi,
               vector<Complex>& H1R0, vector<Complex>& H2R0,
               vector<Complex>& H1R1, vector<Complex>& H2R1,
               vector<Complex>& J1R0,
               int nMax){
  Complex alpha = 0;
  Complex beta  = -J*k;

  Complex bo  = -alpha*Complex(n*n, 0) / (R1*R1) - beta;
  Complex A11 = H1R0[n+nMax];
  Complex A12 = H2R0[n+nMax];
  Complex A21 = k*derivH(n, k*R1, H1R1, nMax) - bo*H1R1[n+nMax];
  Complex A22 = k*derivH(n, k*R1, H2R1, nMax) - bo*H2R1[n+nMax];

  Complex d = A11*A22 - A21*A12;

  Complex a = -A22*real(+A11) / d;
  Complex b = -A21*real(-A12) / d;

  return (a*hankel1(n, k*r) + b*hankel2(n, k*r)) * exp(J*(n*phi));
}

Complex unHard(int n, double r, double phi,
               vector<Complex>& H1R0, vector<Complex>& H2R0,
               vector<Complex>& H1R1, vector<Complex>& H2R1,
               vector<Complex>& J1R0,
               int nMax){
  Complex alpha = 0;
  Complex beta  = -J*k;

  Complex bo  = -alpha*Complex(n*n, 0) / (R1*R1) - beta;
  Complex A11 =   derivH(n, k*R0, H1R0, nMax);
  Complex A12 =   derivH(n, k*R0, H2R0, nMax);
  Complex A21 = k*derivH(n, k*R1, H1R1, nMax) - bo*H1R1[n+nMax];
  Complex A22 = k*derivH(n, k*R1, H2R1, nMax) - bo*H2R1[n+nMax];

  Complex d = A11*A22 - A21*A12;

  Complex a = -A22*derivJ(n, k*R0, J1R0, nMax) / d;
  Complex b = +A21*derivJ(n, k*R0, J1R0, nMax) / d;

  return (a*hankel1(n, k*r) + b*hankel2(n, k*r)) * exp(J*(n*phi));
}

Complex fAnalytic(fullVector<double>& xyz){
  double  r   =  sqrt(xyz(0)*xyz(0) + xyz(1)*xyz(1));
  double  phi = atan2(xyz(1)        , xyz(0));
  int    nMax = int(k*R0) + 30;

  // Precompute Hankel
  vector<Complex> H1R0(2*nMax+1 + 1); // +1 for derivatives
  for(int n = -nMax; n <= nMax+1; n++)
    H1R0[n+nMax] = hankel1(n, k*R0);

  vector<Complex> H2R0(2*nMax+1 + 1); // +1 for derivatives
  for(int n = -nMax; n <= nMax+1; n++)
    H2R0[n+nMax] = hankel2(n, k*R0);

  vector<Complex> H1R1(2*nMax+1 + 1); // +1 for derivatives
  for(int n = -nMax; n <= nMax+1; n++)
    H1R1[n+nMax] = hankel1(n, k*R1);

  vector<Complex> H2R1(2*nMax+1 + 1); // +1 for derivatives
  for(int n = -nMax; n <= nMax+1; n++)
    H2R1[n+nMax] = hankel2(n, k*R1);

  // Precompute Bessel
  vector<Complex> J1R0(2*nMax+1 + 2, 0); // +2 for derivatives
  for(int n = -nMax; n <= nMax+2; n++)
    J1R0[n+nMax] = bessel1(n-1, k*R0);

  // Compute solution
  Complex ret = Complex(0, 0);
  if(type == 0)
    for(int n = -nMax; n <= nMax; n++)
      ret += pow(J, n) * unHard(n, r, phi, H1R0, H2R0, H1R1, H2R1, J1R0, nMax);
  else
    for(int n = -nMax; n <= nMax; n++)
      ret += pow(J, n) * unSoft(n, r, phi, H1R0, H2R0, H1R1, H2R1, J1R0, nMax);

  return ret;
}

#else

#include "Exception.h"
int main(int argc, char** argv){
  throw sf::Exception("GSL not activated");
  return 0;
}

#endif
