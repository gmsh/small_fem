#include "SmallFem.h"
#include "Mesh.h"
#include "System.h"
#include "SystemHelper.h"

#include "FormulationElastoStiffness.h"
#include "FormulationElastoMass.h"
#include "FormulationSource.h"

#include "Interpolator.h"
#include "Integrator.h"
#include "Evaluator.h"

using namespace std;
using namespace sf;

static double pi    = 4 * atan(1);
static double E     = 50e6;
static double nu    = 0.25;
static double rho   = 2.2e3;
static double omega = 1;
static double L     = 1;

double Fx(fullVector<double>& xyz){
  double d  = (1+nu) * (1-2*nu);
  double k  = 2*pi/L;
  double x  = xyz(0);

  //return (rho * omega*omega - E/d * (1-nu) * k*k) * sin(k*x);
  return -E/d * (1-nu) * k*k * sin(k*x);
}

double Fy(fullVector<double>& xyz){
  double d  = (1+nu) * (1-2*nu);
  double k  = 2*pi/L;
  double x  = xyz(0);

  return -E/d * nu * k*k * sin(k*x);
}

double Fz(fullVector<double>& xyz){
  double d  = (1+nu) * (1-2*nu);
  double k  = 2*pi/L;
  double x  = xyz(0);

  return -E/d * nu * k*k * sin(k*x);
}

double Sy(fullVector<double>& xyz){
  double d  = (1+nu) * (1-2*nu);
  double k  = 2*pi/L;
  double x  = xyz(0);
  double y  = xyz(1);

  double sign = 0;
  if(y == 0)
    sign = -1;
  else if(y == 1)
    sign = +1;
  else
    throw Exception("ARG!");

  return sign * E/d * nu * k * cos(k*x);
}

double Sz(fullVector<double>& xyz){
  double d  = (1+nu) * (1-2*nu);
  double k  = 2*pi/L;
  double x  = xyz(0);
  double z  = xyz(2);

  double sign = 0;
  if(z == 0)
    sign = -1;
  else if(z == 1)
    sign = +1;
  else
    throw Exception("ARG!");

  return sign * E/d * nu * k * cos(k*x);
}

double Ux(fullVector<double>& xyz){
  double k  = 2*pi/L;
  double x  = xyz(0);

  return sin(k*x);
}

void compute(const Options& option){
  // Onelab //
  try{
    if(SmallFem::OnelabHelper::action() == "initialize")
      SmallFem::OnelabHelper::client()
        .sendOpenProjectRequest(SmallFem::OnelabHelper::name() + ".geo");

    if(SmallFem::OnelabHelper::action() != "compute")
      return;
  }
  catch(...){
  }

  // Get data //
  string mesh;
  size_t order;
  try{
    mesh  = SmallFem::OnelabHelper::name()  + ".msh";
    order = SmallFem::OnelabHelper::getNumber("03Solver/00Order");
    E     = SmallFem::OnelabHelper::getNumber("00Physics/00Young's modulus");
    nu    = SmallFem::OnelabHelper::getNumber("00Physics/01Poisson's ratio");
    rho   = SmallFem::OnelabHelper::getNumber("00Physics/02Density");
    omega = SmallFem::OnelabHelper::getNumber("00Physics/03Angular frequency");
    L     = SmallFem::OnelabHelper::getNumber("01Geo/00Length");
  }
  catch(...){
    mesh  = option.getValue("-msh")[1];
    order = atoi(option.getValue("-o")[1].c_str());
  }

  cout << mesh  << endl
       << order << endl
       << E     << endl
       << nu    << endl
       << rho   << endl
       << omega << endl
       << L     << endl;

  // Get Domain //
  Mesh msh(mesh);
  GroupOfElement    volume(msh.getFromPhysical(1));
  GroupOfElement boundaryX(msh.getFromPhysical(2));
  GroupOfElement boundaryY(msh.getFromPhysical(3));
  GroupOfElement boundaryZ(msh.getFromPhysical(4));

  // Full Domain //
  vector<const GroupOfElement*> domain(4);
  domain[0] = &volume;
  domain[1] = &boundaryX;
  domain[2] = &boundaryY;
  domain[3] = &boundaryZ;

  // Function Space //
  FunctionSpace0Form hGradX(domain, order);
  FunctionSpace0Form hGradY(domain, order);
  FunctionSpace0Form hGradZ(domain, order);

  // Formulations & System //
  FormulationElastoStiffness<double> stiff(volume, hGradX,hGradY,hGradZ, E,nu);
  //FormulationElastoMass<double>       mass(volume, hGradX,hGradY,hGradZ,
  //                                         omega*omega*rho);
  FormulationSource<double>           srcX(volume, hGradX, Fx);
  FormulationSource<double>           srcY(volume, hGradY, Fy);
  FormulationSource<double>           srcZ(volume, hGradZ, Fz);

  FormulationSource<double>         sigmaY(boundaryY, hGradY, Sy);
  FormulationSource<double>         sigmaZ(boundaryZ, hGradZ, Sz);

  System<double> sys;
  sys.addFormulation(stiff);
  //sys.addFormulation(mass);
  sys.addFormulation(srcX);
  sys.addFormulation(srcY);
  sys.addFormulation(srcZ);
  sys.addFormulation(sigmaY);
  sys.addFormulation(sigmaZ);

  // Dirichlet //
  SystemHelper<double>::dirichletZero(sys, hGradX, boundaryX);
  SystemHelper<double>::dirichletZero(sys, hGradY, boundaryX);
  SystemHelper<double>::dirichletZero(sys, hGradZ, boundaryX);

  //SystemHelper<double>::dirichletZero(sys, hGradY, boundaryY);
  SystemHelper<double>::dirichletZero(sys, hGradZ, boundaryY);

  SystemHelper<double>::dirichletZero(sys, hGradY, boundaryZ);
  //SystemHelper<double>::dirichletZero(sys, hGradZ, boundaryZ);

  // Assemble and Solve //
  cout << "Elastodynamics" << endl << flush;

  sys.assemble();
  cout << "Assembled: " << sys.getSize() << endl << flush;

  sys.solve();
  cout << "Solved" << endl << flush;

  // Display //
  try{
    option.getValue("-nopos");
  }
  catch(...){
    // FEMSolutions
    map<Dof, double> dofX;
    map<Dof, double> dofY;
    map<Dof, double> dofZ;

    SystemHelper<double>::dofMap(hGradX, volume, dofX);
    SystemHelper<double>::dofMap(hGradY, volume, dofY);
    SystemHelper<double>::dofMap(hGradZ, volume, dofZ);

    sys.getSolution(dofX, 0);
    sys.getSolution(dofY, 0);
    sys.getSolution(dofZ, 0);

    FEMSolution<double> feSol;
    //feSol.addCoefficients(0,0, volume, hGradX,dofX, hGradY,dofY, hGradZ,dofZ);
    feSol.addCoefficients(0,0, volume, hGradY,dofY);
    feSol.write("elasto");

    // Merge in Onelab
    try{
      SmallFem::OnelabHelper::client().sendMergeFileRequest("elasto.msh");
    }
    catch(...){
    }
  }

  // L2 Error //
  try{
    option.getValue("-nol2");
  }
  catch(...){
    // Integrator & quadrature points
    cout << "Computing error..." << endl;
    vector<pair<const MElement*, fullMatrix<double> > > qP;
    Integrator<double> integrator(volume, order*4);
    integrator.getQuadraturePoints(qP);

    // Analytic value
    vector<pair<const MElement*, fullMatrix<double> > > Ua;
    Evaluator<double>::evaluate(Ux, qP, Ua);

    // FEM solution
    map<Dof, double> sol;
    SystemHelper<double>::dofMap(hGradX, volume, sol);
    sys.getSolution(sol, 0);

    // Interpolate FEM solution
    vector<pair<const MElement*, fullMatrix<double> > > Uf;
    Interpolator<double>::interpolate(hGradX, sol, qP, Uf);

    // L2 Error
    double l2 = Evaluator<double>::l2Error(Ua, Uf, integrator);
    cout << "L2 Error: " << scientific << l2 <<endl;

    // Send it to Onelab, if possible
    try{
      SmallFem::OnelabHelper::client()
        .set(onelab::number("04Output/00L2 Error [-]", l2));
    }
    catch(...){
    }
  }


  double c = sqrt(E*(1-nu)/(rho*(1+nu)*(1-2*nu)));
  cout << "Speed of sound:    " << c         << endl
       << "Angular frequency: " << omega     << endl
       << "Wavenumber:        " << omega / c << endl
       << "Cutoff wavenumber: " << 2*pi  / L << endl;
}

int main(int argc, char** argv){
  // Init SmallFem //
  SmallFem::Keywords("-msh,-o,-nopos,-nol2");
  SmallFem::Initialize(argc, argv);

  // Compute //
  compute(SmallFem::getOptions());

  // Done //
  SmallFem::Finalize();
  return 0;
}
