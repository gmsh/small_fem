#include "SmallFem.h"
#include "Mesh.h"

#include "FunctionSpace1Form.h"
#include "FormulationStiffness.h"
#include "FormulationCoulomb.h"
#include "FormulationSource.h"
#include "FormulationHelper.h"
#include "System.h"
#include "SystemHelper.h"

#include "Integrator.h"
#include "Interpolator.h"
#include "Evaluator.h"
#include "GeoHelper.h"

using namespace std;
using namespace sf;

static double Pi   = 4 * atan(1);
static double Mu0  = 4 * Pi * 1e-7;
static double Nu0  = 1/Mu0;
static double FAna = 4 * Pi;

void                 Nu(fullVector<double>& xyz, fullMatrix<double>& T);
fullVector<double>   js(fullVector<double>& xyz);
fullVector<double> bAna(fullVector<double>& xyz);
fullVector<double>  tan(const MElement& element, fullVector<double>& xyz);
double             flux(const GroupOfElement& loop,
                        const FunctionSpace1Form& fsA,
                        const System<double>& system);

void compute(const Options& option){
  // Onelab //
  try{
    if(SmallFem::OnelabHelper::action() == "initialize")
      SmallFem::OnelabHelper::client()
        .sendOpenProjectRequest(SmallFem::OnelabHelper::name() + ".geo");

    if(SmallFem::OnelabHelper::action() != "compute")
      return;
  }
  catch(...){
  }

  // Get data //
  string mesh;
  int    gauge;
  int    order;
  try{
    mesh  = SmallFem::OnelabHelper::name() + ".msh";
    gauge = SmallFem::OnelabHelper::getNumber("03Solver/00Gauge");
    order = SmallFem::OnelabHelper::getNumber("03Solver/01Order");
  }
  catch(...){
    mesh  = option.getValue("-msh")[1];
    gauge = atoi(option.getValue("-gauge")[1].c_str());
    order = atoi(option.getValue("-o")[1].c_str());
  }

  // Check Gauge //
  if(gauge != 1 && gauge != 2)
    throw Exception("Unknowns gauge %d (1: Tree/cotree | 2: Coulomb", gauge);

  // Get Domains //
  Mesh msh(mesh);
  GroupOfElement omega = msh.getFromPhysical(1);
  GroupOfElement gamma = msh.getFromPhysical(2);
  GroupOfElement tree  = msh.getFromPhysical(3);
  GroupOfElement loop  = msh.getFromPhysical(4);

  // Full Domain //
  vector<const GroupOfElement*> domain(4);
  domain[0] = &omega;
  domain[1] = &gamma;
  domain[2] = &tree;
  domain[3] = &loop;

  // Function Space //
  string fsFamily = Basis::Family::Hierarchical;
  string fsOption = Basis::Option::NoGradient;

  FunctionSpace1Form  fsA(domain, order, fsFamily, fsOption);
  FunctionSpace0Form* fsXi = NULL;
  if(gauge == 2) // Coulomb
    fsXi = new FunctionSpace0Form(domain, 1, fsFamily);

  // Formulation //
  FormulationStiffness<double> stiff(omega, fsA, fsA, Nu);
  FormulationSource<double>    sourc(omega, fsA,      js);

  FormulationCoulomb*          coulomb = NULL;
  if(gauge == 2){ // Coulomb
    coulomb = new FormulationCoulomb(omega, fsA, *fsXi);
  }

  // System //
  System<double> system;
  system.addFormulation(stiff);
  system.addFormulation(sourc);
  if(gauge == 2){ // Coulomb
    system.addFormulation(*coulomb);
  }

  // BCs //
  SystemHelper<double>::dirichletZero(system, fsA, gamma);

  if(gauge == 1){ // Tree/cotree
    SystemHelper<double>::tctGauge(system, fsA, tree);
  }
  if(gauge == 2){ // Coulomb
    SystemHelper<double>::dirichletZero(system, *fsXi, gamma);
  }

  // Assembling //
  cout << "Assembling..." << endl;
  system.assemble();

  cout << "Cube -- Order " << order
       << ": " << system.getSize()
       << endl << flush;

  // Solve //
  cout << "Solving..." << endl;
  system.solve();

  // Flux through loop //
  double FFem = flux(loop, fsA, system);
  double FErr = abs(FAna - FFem) / FAna;
  cout << "Flux [Wb] (fem|ana|error) = "
       << scientific << FFem << " | " << FAna << " | " << FErr << endl;
  try{
    SmallFem::OnelabHelper::client()
      .set(onelab::number("04Output/01Flux [Wb]", FFem));
    SmallFem::OnelabHelper::client()
      .set(onelab::number("04Output/02Flux error [-]", FErr));
  }
  catch(...){
  }

  // Write Sol //
  try{
    option.getValue("-nopos");
  }
  catch(...){
    cout << "Field map..." << endl;
    FEMSolution<double> feSolB(true);
    system.getSolution(feSolB, fsA, omega);
    feSolB.write("cube_b");

    FEMSolution<double> feSolA(false);
    system.getSolution(feSolA, fsA, omega);
    feSolA.write("cube_a");

    try{
      SmallFem::OnelabHelper::client().sendMergeFileRequest("cube_b.msh");
      SmallFem::OnelabHelper::client().sendMergeFileRequest("cube_a.msh");
    }
    catch(...){
    }
  }

  try{
    option.getValue("-nol2");
  }
  catch(...){
    // L2 Error in gap //
    // Integrator & quadrature points
    cout << "Computing error..." << endl;
    vector<pair<const MElement*, fullMatrix<double> > > qP;
    Integrator<double> integrator(omega, order*4);
    integrator.getQuadraturePoints(qP);

    // Analytic value
    vector<pair<const MElement*, fullMatrix<double> > > anaValue;
    Evaluator<double>::evaluate(bAna, qP, anaValue);

    // FEM solution
    set<Dof> dof;
    fsA.getKeys(omega, dof);

    set<Dof>::iterator end = dof.end();
    set<Dof>::iterator  it = dof.begin();
    map<Dof, double>  sol;

    for(; it != end; it++)
      sol.insert(pair<Dof, double>(*it, 0));

    system.getSolution(sol, 0);

    // Interpolate FEM solution (derivative of vector potential!)
    vector<pair<const MElement*, fullMatrix<double> > > femValue;
    Interpolator<double>::interpolate(fsA, sol, qP, femValue, true);

    // L2 Error
    double l2 = Evaluator<double>::l2Error(anaValue, femValue, integrator);
    cout << "L2 Error: " << scientific << l2 <<endl;

    // Send it to Onelab, if possible
    try{
      SmallFem::OnelabHelper::client()
        .set(onelab::number("04Output/00L2 Error [-]", l2));
    }
    catch(...){
    }
  }

  // Clean //
  if(gauge == 2){ // Coulomb
    delete fsXi;
    delete coulomb;
  }

  // Done //
  cout << "Done :)!" << endl;
}

int main(int argc, char** argv){
  // Init SmallFem //
  SmallFem::Keywords("-msh,-o,-gauge,-nopos,-nol2");
  SmallFem::Initialize(argc, argv);

  compute(SmallFem::getOptions());

  SmallFem::Finalize();
  return 0;
}

void Nu(fullVector<double>& xyz, fullMatrix<double>& T){
  T.scale(0);

  T(0, 0) = Nu0;
  T(1, 1) = Nu0;
  T(2, 2) = Nu0;
}

fullVector<double> js(fullVector<double>& xyz){
  fullVector<double> v(3);

  v(0) = 0;
  v(1) = 0;
  v(2) = 2.0/Mu0*sin(xyz(0))*sin(xyz(1));

  return v;
}

fullVector<double> bAna(fullVector<double>& xyz){

  fullVector<double> v(3);

  v(0) = +sin(xyz(0)) * cos(xyz(1));
  v(1) = -cos(xyz(0)) * sin(xyz(1));
  v(2) = 0;

  return v;
}

fullVector<double> tan(const MElement& element, fullVector<double>& xyz){
  fullVector<double> tmp = GeoHelper<double>::tangent(element, xyz);
  if(Pi/2*0.99 <= xyz(0) && xyz(0) <= Pi/2*1.01)
    tmp.scale(-1);

  return tmp;
}

double flux(const GroupOfElement& loop,
            const FunctionSpace1Form& fsA,
            const System<double>& system){
  // Integrator and quadrature points //
  Integrator<double> integrator(loop, 1); // Only 'Nedelec Dofs' will contribute
  vector<pair<const MElement*, fullMatrix<double> > > iQ;
  integrator.getQuadraturePoints(iQ);

  // a //
  map<Dof, double>  aDof;
  SystemHelper<double>::dofMap(fsA, loop, aDof, Basis::Filter::LowOrder);
  system.getSolution(aDof, 0);

  // a at quadrature points //
  vector<pair<const MElement*, fullMatrix<double> > > a;
  Interpolator<double>::interpolate(fsA, aDof, iQ, a, false);

  // Tangent vector at quadrature points //
  vector<pair<const MElement*, fullMatrix<double> > > tg;
  Evaluator<double>::evaluate(tan, iQ, tg);

  // Dot product //
  vector<pair<const MElement*, fullMatrix<double> > > aDotT;
  Evaluator<double>::dot(a, tg, aDotT);

  // Integrate //
  return integrator.integrate(aDotT);
}
