#include "SmallFem.h"
#include "Mesh.h"

#include "FunctionSpace1Form.h"
#include "FormulationStiffness.h"
#include "FormulationCoulomb.h"
#include "FormulationSource.h"
#include "FormulationHelper.h"
#include "System.h"
#include "SystemHelper.h"

#include "GeoHelper.h"
#include "Integrator.h"
#include "Interpolator.h"
#include "Evaluator.h"
#include "NodeSolution.h"

using namespace std;
using namespace sf;

static double Pi  = 4 * atan(1);
static double Mu0 = 4 * Pi * 1e-7;
static double rA;
static double rB;
static double rC;
static double l;
static double I0;

void                    Nu(fullVector<double>& xyz, fullMatrix<double>& T);
fullVector<double> jsInner(fullVector<double>& xyz);
fullVector<double> jsOuter(fullVector<double>& xyz);
fullVector<double>    bAna(fullVector<double>& xyz);
double                  l2(const GroupOfElement& goe,
                           const FunctionSpace1Form& fsA,
                           const System<double>& system);
double                lAna(void);
double          inductance(const GroupOfElement& loop,
                           const FunctionSpace1Form& fsA,
                           const System<double>& system);

void compute(const Options& option){
  // Onelab //
  try{
    if(SmallFem::OnelabHelper::action() == "initialize")
      SmallFem::OnelabHelper::client()
        .sendOpenProjectRequest(SmallFem::OnelabHelper::name() + ".geo");

    if(SmallFem::OnelabHelper::action() != "compute")
      return;
  }
  catch(...){
  }

  // Get data //
  string mesh;
  int    gauge;
  int    order;
  rA = 0.2; // Default values
  rB = 0.5; // ...
  rC = 0.6; // ...
  l  = 1;   // ...
  I0 = 1;   // ...
  try{
    mesh  = SmallFem::OnelabHelper::name() + ".msh";
    gauge = SmallFem::OnelabHelper::getNumber("03Solver/00Gauge");
    order = SmallFem::OnelabHelper::getNumber("03Solver/01Order");
    rA    = SmallFem::OnelabHelper::getNumber("00Geo/00Inner radius [m]");
    rB    = SmallFem::OnelabHelper::getNumber("00Geo/01Gap radius [m]");
    rC    = SmallFem::OnelabHelper::getNumber("00Geo/02Outer radius [m]");
    l     = SmallFem::OnelabHelper::getNumber("00Geo/03Length [m]");
    I0    = SmallFem::OnelabHelper::getNumber("02Source/00Current [A]");
  }
  catch(...){
    mesh  = option.getValue("-msh")[1];
    gauge = atoi(option.getValue("-gauge")[1].c_str());
    order = atoi(option.getValue("-o")[1].c_str());
  }

  // Check Gauge //
  if(gauge != 1 && gauge != 2)
    throw Exception("Unknowns gauge %d (1: Tree/cotree | 2: Coulomb", gauge);

  // Get Domains //
  Mesh msh(mesh);
  GroupOfElement inner = msh.getFromPhysical(1);
  GroupOfElement gap   = msh.getFromPhysical(2);
  GroupOfElement outer = msh.getFromPhysical(3);
  GroupOfElement side  = msh.getFromPhysical(4);
  GroupOfElement top   = msh.getFromPhysical(5);
  GroupOfElement tree  = msh.getFromPhysical(6);
  GroupOfElement loopP = msh.getFromPhysical(8);
  GroupOfElement loopM = msh.getFromPhysical(9);

  // Full Domain //
  vector<const GroupOfElement*> domain(8);
  domain[0] = &inner;
  domain[1] = &gap;
  domain[2] = &outer;
  domain[3] = &side;
  domain[4] = &top;
  domain[5] = &tree;
  domain[6] = &loopP;
  domain[7] = &loopM;

  // Function Space //
  string fsFamily = Basis::Family::Hierarchical;
  string fsOption = Basis::Option::NoGradient;

  FunctionSpace1Form  fsA(domain, order, fsFamily, fsOption);
  FunctionSpace0Form* fsXi = NULL;
  if(gauge == 2) // Coulomb
    fsXi = new FunctionSpace0Form(domain, 1, fsFamily);

  // Formulation //
  FormulationStiffness<double> stiffInner(inner, fsA, fsA, Nu);
  FormulationStiffness<double> stiffGap  (gap,   fsA, fsA, Nu);
  FormulationStiffness<double> stiffOuter(outer, fsA, fsA, Nu);

  FormulationSource<double>    sourcInner(inner, fsA,     jsInner);
  FormulationSource<double>    sourcOuter(outer, fsA,     jsOuter);

  FormulationCoulomb*          coulombInner = NULL;
  FormulationCoulomb*          coulombGap   = NULL;
  FormulationCoulomb*          coulombOuter = NULL;
  if(gauge == 2){ // Coulomb
    coulombInner = new FormulationCoulomb(inner, fsA, *fsXi);
    coulombGap   = new FormulationCoulomb(gap,   fsA, *fsXi);
    coulombOuter = new FormulationCoulomb(outer, fsA, *fsXi);
  }

  // System //
  System<double> system;
  system.addFormulation(stiffInner);
  system.addFormulation(stiffGap);
  system.addFormulation(stiffOuter);
  system.addFormulation(sourcInner);
  system.addFormulation(sourcOuter);
  if(gauge == 2){ // Coulomb
    system.addFormulation(*coulombInner);
    system.addFormulation(*coulombGap);
    system.addFormulation(*coulombOuter);
  }

  // BCs //
  SystemHelper<double>::dirichletZero(system, fsA, side);
  SystemHelper<double>::dirichletZero(system, fsA, top);

  if(gauge == 1){ // Tree/cotree
    SystemHelper<double>::tctGauge(system, fsA, tree);
  }
  if(gauge == 2){ // Coulomb
    SystemHelper<double>::dirichletZero(system, *fsXi, side);
    SystemHelper<double>::dirichletZero(system, *fsXi, top);
  }

  // Assembling //
  cout << "Assembling..." << endl;
  system.assemble();

  cout << "Coax -- Order " << order
       << ": " << system.getSize()
       << endl << flush;

  // Solve //
  cout << "Solving..." << endl;
  system.solve();

  // Comupute inductance //
  double LFem = inductance(loopP, fsA, system) - inductance(loopM, fsA, system);
  double LAna = lAna();
  double LErr = abs(LAna - LFem) / LAna;
  cout << "Inductance [H] (fem|ana|error) = "
       << scientific << LFem << " | " << LAna << " | " << LErr << endl;

  try{
    SmallFem::OnelabHelper::client()
      .set(onelab::number("04Output/01Inductance [H]", LFem));
    SmallFem::OnelabHelper::client()
      .set(onelab::number("04Output/02Inductance error [-]", LErr));
  }
  catch(...){
  }

  // Write Sol //
  try{
    option.getValue("-nopos");
  }
  catch(...){
    cout << "Field map..." << endl;
    FEMSolution<double> feSol(true);
  //system.getSolution(feSol, fsA, loopP);
  //system.getSolution(feSol, fsA, loopM);
    system.getSolution(feSol, fsA, inner);
    system.getSolution(feSol, fsA, gap);
    system.getSolution(feSol, fsA, outer);
    feSol.write("coax_b");

    try{
      SmallFem::OnelabHelper::client().sendMergeFileRequest("coax_b.msh");
    }
    catch(...){
    }
  }

  try{
    option.getValue("-nol2");
  }
  catch(...){
    // L2 Error in gap //
    cout << "Computing error..." << endl;
    double ll2 = l2(gap, fsA, system);
    cout << "L2 Error: " << scientific << ll2 <<endl;

    // Send it to Onelab, if possible
    try{
      SmallFem::OnelabHelper::client()
        .set(onelab::number("04Output/00L2 Error [-]", ll2));
    }
    catch(...){
    }
  }

  // Clean //
  if(gauge == 2){ // Coulomb
    delete fsXi;
    delete coulombInner;
    delete coulombGap;
    delete coulombOuter;
  }

  // Done //
  cout << "Done :)!" << endl;
}

int main(int argc, char** argv){
  // Init SmallFem //
  SmallFem::Keywords("-msh,-o,-gauge,-nopos,-nol2");
  SmallFem::Initialize(argc, argv);

  compute(SmallFem::getOptions());

  SmallFem::Finalize();
  return 0;
}

void Mu(fullVector<double>& xyz, fullMatrix<double>& T){
  T.setAll(0);
  T(0, 0) = Mu0;
  T(1, 1) = Mu0;
  T(2, 2) = Mu0;
}

void Nu(fullVector<double>& xyz, fullMatrix<double>& T){
  T.setAll(0);

  T(0, 0) = 1.0/Mu0;
  T(1, 1) = 1.0/Mu0;
  T(2, 2) = 1.0/Mu0;
}

fullVector<double> jsInner(fullVector<double>& xyz){
  fullVector<double> v(3);

  v(0) = 0;
  v(1) = 0;
  v(2) = +I0 / (Pi * rA*rA);

  return v;
}

fullVector<double> jsOuter(fullVector<double>& xyz){
  fullVector<double> v(3);

  v(0) = 0;
  v(1) = 0;
  v(2) = -I0 / (Pi * (rC*rC - rB*rB));

  return v;
}

fullVector<double> bAna(fullVector<double>& xyz){
  double r   = sqrt(xyz(0)*xyz(0) + xyz(1)*xyz(1));
  double phi = atan2(xyz(1), xyz(0));
  double bMag;

  if(r <= rA)
    bMag = (Mu0 * I0) / (2*Pi * rA*rA) * r;
  else if(r <= rB)
    bMag = (Mu0 * I0) / (2*Pi * r);
  else
    bMag = (Mu0 * I0) / (2*Pi * r)     * ((rC*rC - r*r)/(rC*rC - rB*rB));

  fullVector<double> v(3);
  v(0) = -sin(phi) * bMag;
  v(1) = +cos(phi) * bMag;
  v(2) = 0;

  return v;
}

double l2(const GroupOfElement& goe,
          const FunctionSpace1Form& fs,
          const System<double>& system){
  // Integrator & quadrature points
  vector<pair<const MElement*, fullMatrix<double> > > qP;
  Integrator<double> integrator(goe, fs.getOrder()*4);
  integrator.getQuadraturePoints(qP);

  // Analytic value
  vector<pair<const MElement*, fullMatrix<double> > > anaValue;
  Evaluator<double>::evaluate(bAna, qP, anaValue);

  // FEM solution
  set<Dof> dof;
  fs.getKeys(goe, dof);

  set<Dof>::iterator end = dof.end();
  set<Dof>::iterator  it = dof.begin();
  map<Dof, double>  sol;

  for(; it != end; it++)
    sol.insert(pair<Dof, double>(*it, 0));

  system.getSolution(sol, 0);

  // Interpolate FEM solution (derivative of vector potential!)
  vector<pair<const MElement*, fullMatrix<double> > > femValue;
  Interpolator<double>::interpolate(fs, sol, qP, femValue, true);

  // L2 Error
  return Evaluator<double>::l2Error(anaValue, femValue, integrator);
}

double lAna(void){
  return l * (Mu0 * I0) / (2*Pi) * log(rB/rA);
}

double inductance(const GroupOfElement& loop,
                  const FunctionSpace1Form& fsA,
                  const System<double>& system){
  // Integrator and quadrature points //
  Integrator<double> integrator(loop, 1); // Only 'Nedelec Dofs' will contribute
  vector<pair<const MElement*, fullMatrix<double> > > iQ;
  integrator.getQuadraturePoints(iQ);

  // a //
  map<Dof, double>  aDof;
  SystemHelper<double>::dofMap(fsA, loop, aDof, Basis::Filter::LowOrder);
  system.getSolution(aDof, 0);

  // a at quadrature points //
  vector<pair<const MElement*, fullMatrix<double> > > a;
  Interpolator<double>::interpolate(fsA, aDof, iQ, a, false);

  // Tangent vector at quadrature points //
  vector<pair<const MElement*, fullMatrix<double> > > tg;
  Evaluator<double>::evaluate(GeoHelper<double>::tangent, iQ, tg);

  // Dot product //
  vector<pair<const MElement*, fullMatrix<double> > > aDotT;
  Evaluator<double>::dot(a, tg, aDotT);

  // Integrate, compute L (i.e., divide flux by I0) and return
  return integrator.integrate(aDotT) / I0;
}
