#include "SmallFem.h"
#include "Mesh.h"
#include "SystemEigen.h"
#include "SystemHelper.h"

#include "FormulationElastoStiffness.h"
#include "FormulationElastoMass.h"

#include "TextSolution.h"

#include <iostream>
#include <iomanip>
#include <complex>
#include <cmath>

using namespace std;
using namespace sf;

void compute(const Options& option){
  // Onelab //
  try{
    if(SmallFem::OnelabHelper::action() == "initialize")
      SmallFem::OnelabHelper::client()
        .sendOpenProjectRequest(SmallFem::OnelabHelper::name() + ".geo");

    if(SmallFem::OnelabHelper::action() != "compute")
      return;
  }
  catch(...){
  }

  // Get data //
  string mesh;
  size_t order;
  double omega = 748.848;
  double E     = 50e6;
  double nu    = 0.25;
  double rho   = 2.2e3;
  try{
    mesh  = SmallFem::OnelabHelper::name()  + ".msh";
    order = SmallFem::OnelabHelper::getNumber("03Solver/00Order");
    omega = SmallFem::OnelabHelper::getNumber("03Solver/01Omega guess");
    E     = SmallFem::OnelabHelper::getNumber("00Physics/00Young's modulus");
    nu    = SmallFem::OnelabHelper::getNumber("00Physics/01Poisson's ratio");
    rho   = SmallFem::OnelabHelper::getNumber("00Physics/02Density");
  }
  catch(...){
    mesh  = option.getValue("-msh")[1];
    order = atoi(option.getValue("-o")[1].c_str());
  }

  // Get Domain //
  Mesh msh(mesh);
  GroupOfElement   volume(msh.getFromPhysical(1));
  GroupOfElement boundary(msh.getFromPhysical(2));

  // Full Domain //
  vector<const GroupOfElement*> domain(2);
  domain[0] = &volume;
  domain[1] = &boundary;

  // Function Space //
  FunctionSpace0Form hGradX(domain, order);
  FunctionSpace0Form hGradY(domain, order);
  FunctionSpace0Form hGradZ(domain, order);

  // Formulations & System //
  FormulationElastoStiffness<Complex> stiff(volume, hGradX,hGradY,hGradZ, E,nu);
  FormulationElastoMass<Complex>       mass(volume, hGradX,hGradY,hGradZ, rho);

  SystemEigen sys;
  sys.addFormulation(stiff);
  sys.addFormulationB(mass);

  // Dirichlet //
  // None

  // Assemble and Solve //
  cout << "Eigenvalues problem" << endl << flush;

  sys.assemble();
  cout << "Assembled: " << sys.getSize() << endl << flush;

  // Set number of eigenvalues
  sys.setNumberOfEigenValues(20);

  // Set shift
  sys.setWhichEigenpairs("target_magnitude");
  sys.setTarget(Complex(omega * omega, 0));

  // Set tolerance (if any, else default)
  try{
    sys.setTolerance(atof(option.getValue("-tol")[1].c_str()));
  }
  catch(...){
  }

  // Solve
  sys.solve();
  cout << "Solved" << endl << flush;

  // Display //
  fullVector<Complex> eigenValue;
  const size_t nEigenValue = sys.getNComputedSolution();
  sys.getEigenValues(eigenValue);

  for(size_t i = 0; i < nEigenValue; i++)
    eigenValue(i) = sqrt(eigenValue(i)); // Keep only angular frenquency

  cout << "Number of found Eigenvalues: " << nEigenValue
       << endl
       << endl
       << "Number\tsqrt(Eigenvalue)" << endl;

  for(size_t i = 0; i < nEigenValue; i++)
    cout << "#" << i + 1  << "\t" << eigenValue(i) << endl;

  // Error with respect to first guess //
  double l2 = std::abs(eigenValue(0).real() - omega) / eigenValue(0).real();
  cout << endl
       << "L2 error with respect to first guess "
       << "(" << omega << "): " << std::scientific << l2 << endl;

  // Send to Onelab
  try{
    SmallFem::OnelabHelper::client()
      .set(onelab::number("04Output/00L2 Error [-]", l2));
  }
  catch(...){
  }

  // Write Sol //
  try{
    option.getValue("-nopos");
  }
  catch(...){
    cout << "Creating field maps..." << endl << flush;

    // Eigenvalues
    size_t nEigenValue = sys.getNComputedSolution();

    // FEMSolutions
    FEMSolution<Complex> feSol;

    // Loop on eigenvalues
    for(size_t i = 0; i < nEigenValue; i++){
      std::map<Dof, Complex> dofX;
      std::map<Dof, Complex> dofY;
      std::map<Dof, Complex> dofZ;

      SystemHelper<Complex>::dofMap(hGradX, volume, dofX);
      SystemHelper<Complex>::dofMap(hGradY, volume, dofY);
      SystemHelper<Complex>::dofMap(hGradZ, volume, dofZ);

      sys.getSolution(dofX, i);
      sys.getSolution(dofY, i);
      sys.getSolution(dofZ, i);

      feSol.addCoefficients(i,0, volume, hGradX,dofX, hGradY,dofY, hGradZ,dofZ);
    }

    feSol.write("eigenModes");

    // Eigenvalues
    TextSolution        eigenSol;
    fullVector<Complex> eigenValue;

    sys.getEigenValues(eigenValue);

    for(size_t i = 0; i < nEigenValue; i++){
      stringstream stream;
      stream << "Angular frequency " << i + 1 << ": " << eigenValue(i) << " ";

      eigenSol.addValues(i * 2 + 0, stream.str().append("(real part)"));
      eigenSol.addValues(i * 2 + 1, stream.str().append("(imaginary part)"));
    }

    eigenSol.write("eigenModes");

    // Merge in Onelab
    try{
      if(SmallFem::OnelabHelper::getNumber("03Solver/06Merge view?")){
        SmallFem::OnelabHelper::client().sendMergeFileRequest("eigenModes.pos");
        SmallFem::OnelabHelper::client().sendMergeFileRequest("eigenModes.msh");
      }
    }
    catch(...){
    }
  }

  // Save Dofs ? //
  bool savedof;
  try{
    savedof = SmallFem::OnelabHelper::getNumber("03Solver/03Save Dofs");
  }
  catch(...){
    savedof = 0;
  }

  if(savedof)
    cout << "Saving dofs..." << endl << flush;

  for(size_t i = 0; savedof && i < sys.getNComputedSolution(); i++){
    // Maps
    map<Dof, Complex> dofCX;
    map<Dof, Complex> dofCY;
    map<Dof, Complex> dofCZ;

    SystemHelper<Complex>::dofMap(hGradX, volume, dofCX);
    SystemHelper<Complex>::dofMap(hGradY, volume, dofCY);
    SystemHelper<Complex>::dofMap(hGradZ, volume, dofCZ);

    // Solution
    sys.getSolution(dofCX, i);
    sys.getSolution(dofCY, i);
    sys.getSolution(dofCZ, i);

    // File name
    stringstream fileX;
    stringstream fileY;
    stringstream fileZ;

    fileX << "dofX_" << std::setfill('0') << std::setw(4) << i + 1 << ".map";
    fileY << "dofY_" << std::setfill('0') << std::setw(4) << i + 1 << ".map";
    fileZ << "dofZ_" << std::setfill('0') << std::setw(4) << i + 1 << ".map";

    // Save
    SystemHelper<Complex>::writeDof(dofCX, fileX.str());
    SystemHelper<Complex>::writeDof(dofCY, fileY.str());
    SystemHelper<Complex>::writeDof(dofCZ, fileZ.str());
  }

  // Done
  cout << "Done :)!  [VMPeak: " << SmallFem::getVMPeak() << "]" << endl;
}

int main(int argc, char** argv){
  // Init SmallFem //
  SmallFem::Keywords("-msh,-o,-nopos,-tol");
  SmallFem::Initialize(argc, argv);

  // Compute //
  compute(SmallFem::getOptions());

  // Done //
  SmallFem::Finalize();
  return 0;
}
