#include "SmallFem.h"
#include "Timer.h"
#include "System.h"
#include "SystemHelper.h"
#include "Interpolator.h"
#include "Evaluator.h"
#include "Integrator.h"
#include "FormulationHelper.h"
#include "FormulationSilverMuller.h"
#include "FormulationSteadyWave.h"
#include "FormulationSteadySlowVector.h"
#include "FormulationSteadySlowScalar.h"

#include <iostream>

using namespace std;
using namespace sf;

static const int    scal = 0;
static const int    vect = 1;

static const Complex I  = Complex(0, 1);
static const double  Pi = M_PI;

static const Complex E0 = Complex(1, 0);
static const double  a  = 0.25;
static const double  b  = 0.25;
static const int     m  = 1;
static const int     n  = 1;

static const double  ky = m * Pi / a;
static const double  kz = n * Pi / b;
static       double  k;
static       Complex kx;

static       bool    isTE;
static       bool    is2D;

////////////////////
// Sources Fileds //
////////////////////
Complex             fSourceScal(fullVector<double>& xyz);
Complex                fAnaScal(fullVector<double>& xyz);
Complex               fZeroScal(fullVector<double>& xyz);
fullVector<Complex> fSourceVect(fullVector<double>& xyz);
fullVector<Complex>    fAnaVect(fullVector<double>& xyz);
fullVector<Complex>   fZeroVect(fullVector<double>& xyz);

Complex fSourceScal(fullVector<double>& xyz){
  const double y = xyz(1);
  const double z = xyz(2);

  if(is2D)
    return E0 * sin(ky * y);
  else
    return E0 * sin(ky * y) * sin(kz * z);
}

Complex fAnaScal(fullVector<double>& xyz){
  const double x  = xyz(0);
  const double y  = xyz(1);
  const double z  = xyz(2);

  if(is2D)
    return E0 * sin(ky * y)               * exp(I * kx * x);
  else
    return E0 * sin(ky * y) * sin(kz * z) * exp(I * kx * x);
}

Complex fZeroScal(fullVector<double>& xyz){
  return Complex(0, 0);
}

fullVector<Complex> fSourceVect(fullVector<double>& xyz){
  const double y  = xyz(1);
  const double z  = xyz(2);

  fullVector<Complex> tmp(3);

  if(is2D){
    if(isTE){
      tmp(0) = Complex(0, 0);
      tmp(1) = -E0 * cos(ky * y);
      tmp(2) = Complex(0, 0);
    }
    else{
      // TMm 2D
      tmp(0) = -E0 * I * ky / k * sin(ky * y);
      tmp(1) = +E0 *     kx / k * cos(ky * y);
      tmp(2) = Complex(0, 0);
    }
  }

  else{
    if(isTE){
      // TEmn 3D
      tmp(0) = Complex(0, 0);
      tmp(1) = -E0 * cos(ky * y) * sin(kz * z);
      tmp(2) = +E0 * sin(ky * y) * cos(kz * z);
    }

    else{
      // TMmn 3D
      tmp(0) = E0                                 * sin(ky * y) * sin(kz * z);
      tmp(1) = E0 * (I * kx * ky) / (k*k - kx*kx) * cos(ky * y) * sin(kz * z);
      tmp(2) = E0 * (I * kx * kz) / (k*k - kx*kx) * sin(ky * y) * cos(kz * z);
    }
  }

  return tmp;
}

fullVector<Complex> fAnaVect(fullVector<double>& xyz){
  const double x  = xyz(0);
  const double y  = xyz(1);
  const double z  = xyz(2);

  fullVector<Complex> tmp(3);

  if(is2D){
    if(isTE){
      tmp(0) = Complex(0, 0);
      tmp(1) = -E0 * cos(ky * y)              * exp(I * kx * x);
      tmp(2) = Complex(0, 0);
    }
    else{
      // TMm 2D
      tmp(0) = -E0 * I * ky / k * sin(ky * y) * exp(I * kx * x);
      tmp(1) = +E0 *     kx / k * cos(ky * y) * exp(I * kx * x);
      tmp(2) = Complex(0, 0);
    }
  }

  else{
    if(isTE){
      // TEmn 3D
      tmp(0) = Complex(0, 0);
      tmp(1) = -E0 * cos(ky * y) * sin(kz * z) * exp(I * kx * x);
      tmp(2) = +E0 * sin(ky * y) * cos(kz * z) * exp(I * kx * x);
    }

    else{
      // TMmn 3D
      tmp(0) = E0                           * sin(ky*y) * sin(kz*z) * exp(I*kx*x);
      tmp(1) = E0*(I*kx*ky) / (k*k - kx*kx) * cos(ky*y) * sin(kz*z) * exp(I*kx*x);
      tmp(2) = E0*(I*kx*kz) / (k*k - kx*kx) * sin(ky*y) * cos(kz*z) * exp(I*kx*x);
    }
  }

  return tmp;
}

fullVector<Complex> fZeroVect(fullVector<double>& xyz){
  fullVector<Complex> tmp(3);

  tmp(0) = Complex(0, 0);
  tmp(1) = Complex(0, 0);
  tmp(2) = Complex(0, 0);

  return tmp;
}

/////////////
// Helpers //
/////////////
void getKx(void){
  if(is2D)
    kx = sqrt(Complex(k * k, 0) - (ky * ky));
  else
    kx = sqrt(Complex(k * k, 0) - (ky * ky) - (kz * kz));
}

/////////////
// Compute //
/////////////
void compute(const Options& option){
  // Timers
  Timer tAssembly;
  Timer tSolve;

  // Get Type //
  int type;
  if(option.getValue("-type")[1].compare("scalar") == 0){
    cout << "Scalar Waveguide" << endl << flush;
    type = scal;
  }

  else if(option.getValue("-type")[1].compare("vector") == 0){
    cout << "Vetorial Waveguide" << endl << flush;
    type = vect;
  }

  else
    throw Exception("Bad -type: %s", option.getValue("-type")[1].c_str());

  // Get Parameters //
  const size_t order = atoi(option.getValue("-o")[1].c_str());
  k                  = atof(option.getValue("-k")[1].c_str());

  // Get Mode //
  string mode = option.getValue("-mode")[1];
  if(mode.compare("te") == 0)
    isTE = true;
  else if(mode.compare("tm") == 0)
    isTE = false;
  else
    throw Exception("Unknown mode %s", mode.c_str());

  // Is 2D ? //
  try{
    option.getValue("-2d");
    is2D = true;
  }
  catch(...){
    is2D = false;
  }

  // Which solver ? //
  string solver;
  try{
    solver = option.getValue("-solver")[1];
  }
  catch(...){
    solver = string("mumps");
  }

  // Compute kx //
  getKx();

  // Compute kInfinity for mode matching in silver-muller //
  Complex kInf;
  if(isTE)
    kInf = kx;
  else
    kInf = (k * k) / kx;

  cout << "Wavenumber: " << k            << endl
       << "Mode:       " << mode.c_str() << endl
       << "Order:      " << order        << endl << flush;

  // Get Domains //
  Mesh msh(option.getValue("-msh")[1]);
  GroupOfElement infinity(msh.getFromPhysical(4));
  GroupOfElement   source(msh.getFromPhysical(5));
  GroupOfElement     zero(msh.getFromPhysical(6));
  GroupOfElement   volume(msh.getFromPhysical(7));

  // Full Domain //
  vector<const GroupOfElement*> domain(4);
  domain[0] = &volume;
  domain[1] = &source;
  domain[2] = &zero;
  domain[3] = &infinity;

  // Function Space //
  tAssembly.start();
  FunctionSpace* fs = NULL;

  if(type == scal)
    fs = new FunctionSpace0Form(domain, order);
  else
    fs = new FunctionSpace1Form(domain, order);

  // Steady Wave Formulation //
  Formulation<Complex>*        wave;
  FormulationSilverMuller radiation(infinity, *fs, kInf);

  try{
    option.getValue("-slow");
    cout << "Slow Formulation" << endl << flush;
    if(type == vect)
      wave = new FormulationSteadySlowVector<Complex>(volume, *fs, k);
    else
      wave = new FormulationSteadySlowScalar<Complex>(volume, *fs, k);
  }

  catch(...){
    cout << "Fast Formulation" << endl << flush;
    wave = new FormulationSteadyWave<Complex>(volume, *fs, k);
  }

  // Solve //
  System<Complex> system(solver);
  system.addFormulation(*wave);
  system.addFormulation(radiation);

  // Constraint
  if(fs->isScalar()){
    SystemHelper<Complex>::dirichlet(system, *fs, zero  , fZeroScal);
    SystemHelper<Complex>::dirichlet(system, *fs, source, fSourceScal);
  }
  else{
    SystemHelper<Complex>::dirichlet(system, *fs, zero  , fZeroVect);
    SystemHelper<Complex>::dirichlet(system, *fs, source, fSourceVect);
  }

  // Assemble
  system.assemble();
  tAssembly.stop();
  cout << "Assembled: " << system.getSize()
       << "! (" << tAssembly.time() << " " << tAssembly.unit() << ")"
       << endl << flush;

  // BLR ?
  try{
    system.getSolver().setOption("blr", option.getValue("-blr")[1].c_str());
  }
  catch(...){
  }

  // Sove
  tSolve.start();
  system.solve();
  tSolve.stop();
  cout << "Solved!"
       << " (" << tSolve.time() << " " << tSolve.unit() << ")" << endl << flush
       << "Peak Memory: " << SmallFem::getVMPeak() << " [MB]"  << endl << flush;

  // Draw Solution //
  try{
    option.getValue("-nopos");
  }

  catch(...){
    cout << "Writing solution..." << endl << flush;

    FEMSolution<Complex> feSol;
    system.getSolution(feSol, *fs, volume);
    feSol.write("waveguide");
  }

  // L2 Error //
  try{
    // Timer
    Timer tL2;
    tL2.start();

    // Get integrator order
    const size_t iO = atoi(option.getValue("-l2")[1].c_str());
    cout << "L2 Error (Quadrature " << iO << ")..." << endl;

    // Get integrator
    Integrator<Complex> integrator(volume, iO);

    // Get quadrature points
    vector<pair<const MElement*, fullMatrix<double> > > qP;
    integrator.getQuadraturePoints(qP);

    // Analytic solution
    vector<pair<const MElement*, fullMatrix<Complex> > > anaValue;
    if(type == scal)
      Evaluator<Complex>::evaluate(fAnaScal, qP, anaValue);
    else
      Evaluator<Complex>::evaluate(fAnaVect, qP, anaValue);

    // FEM solution
    set<Dof> dof;
    fs->getKeys(volume, dof);

    set<Dof>::iterator    end = dof.end();
    set<Dof>::iterator     it = dof.begin();
    map<Dof, Complex>     sol;

    for(; it != end; it++)
      sol.insert(pair<Dof, Complex>(*it, 0));

    system.getSolution(sol, 0);

    // Interpolate FEM solution
    vector<pair<const MElement*, fullMatrix<Complex> > > femValue;
    Interpolator<Complex>::interpolate(*fs, sol, qP, femValue);

    // L2 Error
    cout << "L2 Error: " << scientific
         << Evaluator<Complex>::l2Error(anaValue, femValue, integrator); 
    
    tL2.stop();
    cout << " (" << tL2.time() << " " << tL2.unit() << ")" << endl;
  }
  catch(...){
  }

  // Clean //
  delete wave;
  delete fs;
}

int main(int argc, char** argv){
  // Init SmallFem //
  SmallFem::Keywords("-msh,-o,-k,-type,-2d,-mode,-slow,-nopos,-l2,"
                     "-solver,-blr");
  SmallFem::Initialize(argc, argv);

  compute(SmallFem::getOptions());

  SmallFem::Finalize();
  return 0;
}
