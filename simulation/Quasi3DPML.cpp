// include needed headers

#include "SmallFem.h"
#include "Mesh.h"

#include "FunctionSpace0Form.h"
#include "FunctionSpace1Form.h"
#include "FormulationQ3DEStarStiffness.h"
#include "FormulationQ3DEStarMass.h"
#include "FormulationQ3DParamStiffness.h"
#include "FormulationQ3DParamMass.h"
#include "FormulationQ3DnErzMass.h"
#include "FormulationQ3DnErzStiffness.h"
#include "FormulationQ3DErzEphiStiffness.h"
#include "FormulationQ3DErzEphiMass.h"

#include "Quasi3DPMLHelper.h"

#include "SystemEigen.h"
#include "SystemHelper.h"
#include "FEMSolution.h"
#include "TextSolution.h"


#include <iostream>
#include <fstream>
#include <iomanip>
 
/**
   @class Quasi3DPML
   @brief Quasi-3D solver for axisymmetric problems with PML 
   
   This class is the driver to setup and solve a quasi3D problem with PML to model 
   open boundaries. Until now, only the EStar ansatz is supported. Furthermore,
   at the moment, PML options have to be set manually in Quasi3DPMLHelper.cpp. 
   
   For all possible options that can be given to the executable, 
   please consider the code. 
  
   @author Erik Schnaubelt
*/

using namespace std;
using namespace sf;

// define needed zeros
Complex fZeroScal(fullVector<double>& xyz){
  return Complex(0, 0);
}

fullVector<Complex> fZeroVect(fullVector<double>& xyz){
  fullVector<Complex> f(3);

  f(0) = Complex(0, 0);
  f(1) = Complex(0, 0);
  f(2) = Complex(0, 0);

  return f;
}


void compute(const Options& option){
  // Get Domains //
  Mesh msh(option.getValue("-msh")[1]);
  
  GroupOfElement dom     = msh.getFromPhysical(120);
  GroupOfElement nonsymm = msh.getFromPhysical(101);
  GroupOfElement symm    = msh.getFromPhysical(102);
  GroupOfElement lowerZZ = msh.getFromPhysical(121);
  GroupOfElement lowerRZ = msh.getFromPhysical(122);
  GroupOfElement upperRZ = msh.getFromPhysical(124);
  GroupOfElement upperZZ = msh.getFromPhysical(125);
  GroupOfElement RR      = msh.getFromPhysical(123);
  GroupOfElement sphere  = msh.getFromPhysical(126);

  // Full Domain //
  vector<const GroupOfElement*> domain(9);
  domain[0] = &dom;
  domain[1] = &nonsymm;
  domain[2] = &symm;
  domain[3] = &lowerZZ;
  domain[4] = &lowerRZ;
  domain[5] = &upperRZ;
  domain[6] = &upperZZ;
  domain[7] = &RR;
  domain[8] = &sphere;


  // Get parameters from user input //
  double shift;
  double tol;
  double alpha; 
  double beta;
  int    n; 
  int    nEig;
  int    orderHCurl;
  int    orderHGrad;
  int    orderGauss;
  int    ansatz; // 0: ephiStar = r ephi, 1: parameterized, default: 0 
  string ansatzMap[4] = {"ephiStar", "parameterized", "Dunn", "Oh"}; // to map the ansatz index to a name
  
  try{
    shift = atof(option.getValue("-shift")[1].c_str());
  }
  catch(...){
    // default is for TE111 //
    shift = 2.380341795042317e+19;
  }

  try{
    tol = atof(option.getValue("-tol")[1].c_str());
  }
  catch(...){
    tol = 1e-6;
  }
  
  try{
    n = atoi(option.getValue("-n")[1].c_str());
  }
  catch(...){
    n = 1;
  }
  
  try{
    alpha = atof(option.getValue("-alpha")[1].c_str());
  }
  catch(...){
    alpha = 1.0;
  }
  
  try{
    beta = atof(option.getValue("-beta")[1].c_str());
  }
  catch(...){
    switch(n) {
        case 0: 
            beta = 2.0; 
            break; 
        
        default: 
            beta = 1.0;
            break;
    }
  }

  try{
    nEig = atoi(option.getValue("-nEig")[1].c_str());
  }
  catch(...){
    nEig = 20;
  }
  
  try{
    orderHGrad = atoi(option.getValue("-oHG")[1].c_str());
  }
  catch(...){
    orderHGrad = 1;
  }

   try{
    orderHCurl = atoi(option.getValue("-oHC")[1].c_str());
  }
  catch(...){
    orderHCurl = 0;
  }
  try{
    orderGauss = atoi(option.getValue("-oGauss")[1].c_str());
  }
  catch(...){
    orderGauss = 6;
  }
  
  try{
    ansatz = atoi(option.getValue("-ansatz")[1].c_str());
  }
  catch(...){
    ansatz = 0;
  }
  
  // Function Space //
  FunctionSpace0Form hGrad(domain, orderHGrad);
  FunctionSpace1Form hCurl(domain, orderHCurl);
  SystemEigen system;
  
  Formulation<Complex>* domainStiff; 
  Formulation<Complex>* domainMass; 
  
  Formulation<Complex>* sphereStiff; 
  Formulation<Complex>* sphereMass; 
  
  Formulation<Complex>* lowerZZStiff; 
  Formulation<Complex>* lowerZZMass; 
  
  Formulation<Complex>* lowerRZStiff; 
  Formulation<Complex>* lowerRZMass; 
  
  Formulation<Complex>* upperRZStiff; 
  Formulation<Complex>* upperRZMass; 
  
  Formulation<Complex>* upperZZStiff; 
  Formulation<Complex>* upperZZMass; 
  
  Formulation<Complex>* RRStiff; 
  Formulation<Complex>* RRMass; 
  
  // define formulations for every part of the domain using the aux. functions 
  // from Quasi3DPMLHelper
  
  switch(ansatz) {
      case 0: 
          domainStiff = new FormulationQ3DEStarStiffness(dom, hGrad, hCurl, n, Material::Dom::NuRad, 
                                                         Material::Dom::NuInvRad, orderGauss);
          
          domainMass = new FormulationQ3DEStarMass(dom, hGrad, hCurl, Material::Dom::EpsilonRad, 
                                                   Material::Dom::EpsilonInvRad, orderGauss);
          
          
          
          sphereStiff = new FormulationQ3DEStarStiffness(sphere, hGrad, hCurl, n, Material::Sphere::NuRad, 
                                                         Material::Sphere::NuInvRad, orderGauss);
          
          sphereMass = new FormulationQ3DEStarMass(sphere, hGrad, hCurl, Material::Sphere::EpsilonRad, 
                                                   Material::Sphere::EpsilonInvRad, orderGauss);
          
          
          
          lowerZZStiff = new FormulationQ3DEStarStiffness(lowerZZ, hGrad, hCurl, n, Material::LowerZZ::NuRad, 
                                                          Material::LowerZZ::NuInvRad, orderGauss);
          
          lowerZZMass = new FormulationQ3DEStarMass(lowerZZ, hGrad, hCurl, Material::LowerZZ::EpsilonRad, 
                                                    Material::LowerZZ::EpsilonInvRad, orderGauss);
          
          
          
          lowerRZStiff = new FormulationQ3DEStarStiffness(lowerRZ, hGrad, hCurl, n, Material::LowerRZ::NuRad, 
                                                          Material::LowerRZ::NuInvRad, orderGauss);
          
          lowerRZMass = new FormulationQ3DEStarMass(lowerRZ, hGrad, hCurl, Material::LowerRZ::EpsilonRad, 
                                                Material::LowerRZ::EpsilonInvRad, orderGauss);
          
          
          
          upperRZStiff = new FormulationQ3DEStarStiffness(upperRZ, hGrad, hCurl, n, Material::UpperRZ::NuRad, 
                                                          Material::UpperRZ::NuInvRad, orderGauss);
          
          upperRZMass = new FormulationQ3DEStarMass(upperRZ, hGrad, hCurl, Material::UpperRZ::EpsilonRad, 
                                                    Material::UpperRZ::EpsilonInvRad, orderGauss);
          
          
          
          upperZZStiff = new FormulationQ3DEStarStiffness(upperZZ, hGrad, hCurl, n, Material::UpperZZ::NuRad, 
                                                          Material::UpperZZ::NuInvRad, orderGauss);
          
          upperZZMass = new FormulationQ3DEStarMass(upperZZ, hGrad, hCurl, Material::UpperZZ::EpsilonRad, 
                                                    Material::UpperZZ::EpsilonInvRad, orderGauss);
          
          
          
          RRStiff = new FormulationQ3DEStarStiffness(RR, hGrad, hCurl, n, Material::RR::NuRad, 
                                                     Material::RR::NuInvRad, orderGauss);
          
          RRMass = new FormulationQ3DEStarMass(RR, hGrad, hCurl, Material::RR::EpsilonRad, 
                                               Material::RR::EpsilonInvRad, orderGauss);
      break; 

      case 1:
          domainStiff = new FormulationQ3DParamStiffness(dom, hGrad, hCurl, 
                                                    alpha, beta, n, Material::Dom::Nu, orderGauss);
          domainMass  = new FormulationQ3DParamMass(dom, hGrad, hCurl, 
                                               alpha, beta, n, Material::Dom::Epsilon, orderGauss);
          
          sphereStiff = new FormulationQ3DParamStiffness(sphere, hGrad, hCurl, 
                                                    alpha, beta, n, Material::Sphere::Nu, orderGauss);
          sphereMass  = new FormulationQ3DParamMass(sphere, hGrad, hCurl, 
                                               alpha, beta, n, Material::Sphere::Epsilon, orderGauss);
          
          lowerZZStiff = new FormulationQ3DParamStiffness(lowerZZ, hGrad, hCurl, 
                                                    alpha, beta, n, Material::LowerZZ::Nu, orderGauss);
          lowerZZMass  = new FormulationQ3DParamMass(lowerZZ, hGrad, hCurl, 
                                               alpha, beta, n, Material::LowerZZ::Epsilon, orderGauss);
          
          lowerRZStiff = new FormulationQ3DParamStiffness(lowerRZ, hGrad, hCurl, 
                                                    alpha, beta, n, Material::LowerRZ::Nu, orderGauss);
          lowerRZMass  = new FormulationQ3DParamMass(lowerRZ, hGrad, hCurl, 
                                               alpha, beta, n, Material::LowerRZ::Epsilon, orderGauss);
          
          upperRZStiff = new FormulationQ3DParamStiffness(upperRZ, hGrad, hCurl, 
                                                    alpha, beta, n, Material::UpperRZ::Nu, orderGauss);
          upperRZMass  = new FormulationQ3DParamMass(upperRZ, hGrad, hCurl, 
                                               alpha, beta, n, Material::UpperRZ::Epsilon, orderGauss);
          
          upperZZStiff = new FormulationQ3DParamStiffness(upperZZ, hGrad, hCurl, 
                                                    alpha, beta, n, Material::UpperZZ::Nu, orderGauss);
          upperZZMass  = new FormulationQ3DParamMass(upperZZ, hGrad, hCurl, 
                                               alpha, beta, n, Material::UpperZZ::Epsilon, orderGauss);
          
          RRStiff = new FormulationQ3DParamStiffness(RR, hGrad, hCurl, 
                                                    alpha, beta, n, Material::RR::Nu, orderGauss);
          RRMass  = new FormulationQ3DParamMass(RR, hGrad, hCurl, 
                                               alpha, beta, n, Material::RR::Epsilon, orderGauss);
          break;
      
      case 2: 
          domainStiff = new FormulationQ3DnErzStiffness(dom, hGrad, hCurl, 
                                                     n, Material::Dom::Nu, orderGauss);
          domainMass  = new FormulationQ3DnErzMass(dom, hGrad, hCurl, 
                                               n, Material::Dom::Epsilon, orderGauss);
          
          sphereStiff = new FormulationQ3DnErzStiffness(sphere, hGrad, hCurl, 
                                                     n, Material::Sphere::Nu, orderGauss);
          sphereMass  = new FormulationQ3DnErzMass(sphere, hGrad, hCurl, 
                                               n, Material::Sphere::Epsilon, orderGauss);
          
          lowerZZStiff = new FormulationQ3DnErzStiffness(lowerZZ, hGrad, hCurl, 
                                                     n, Material::LowerZZ::Nu, orderGauss);
          lowerZZMass  = new FormulationQ3DnErzMass(lowerZZ, hGrad, hCurl, 
                                               n, Material::LowerZZ::Epsilon, orderGauss);
          
          lowerRZStiff = new FormulationQ3DnErzStiffness(lowerRZ, hGrad, hCurl, 
                                                     n, Material::LowerRZ::Nu, orderGauss);
          lowerRZMass  = new FormulationQ3DnErzMass(lowerRZ, hGrad, hCurl, 
                                               n, Material::LowerRZ::Epsilon, orderGauss);
          
          upperRZStiff = new FormulationQ3DnErzStiffness(upperRZ, hGrad, hCurl, 
                                                     n, Material::UpperRZ::Nu, orderGauss);
          upperRZMass  = new FormulationQ3DnErzMass(upperRZ, hGrad, hCurl, 
                                               n, Material::UpperRZ::Epsilon, orderGauss);
          
          upperZZStiff = new FormulationQ3DnErzStiffness(upperZZ, hGrad, hCurl, 
                                                     n, Material::UpperZZ::Nu, orderGauss);
          upperZZMass  = new FormulationQ3DnErzMass(upperZZ, hGrad, hCurl, 
                                               n, Material::UpperZZ::Epsilon, orderGauss);
          
          RRStiff = new FormulationQ3DnErzStiffness(RR, hGrad, hCurl, 
                                                     n, Material::RR::Nu, orderGauss);
          RRMass  = new FormulationQ3DnErzMass(RR, hGrad, hCurl, 
                                               n, Material::RR::Epsilon, orderGauss);
          break; 
          
      case 3: 
          domainStiff = new FormulationQ3DErzEphiStiffness(dom, hGrad, hCurl, 
                                                     n, Material::Dom::Nu, orderGauss);
          domainMass  = new FormulationQ3DErzEphiMass(dom, hGrad, hCurl, 
                                                     n, Material::Dom::Nu, orderGauss);
          
          sphereStiff = new FormulationQ3DErzEphiStiffness(sphere, hGrad, hCurl, 
                                                     n, Material::Sphere::Nu, orderGauss);
          sphereMass  = new FormulationQ3DErzEphiMass(sphere, hGrad, hCurl, 
                                                     n, Material::Sphere::Nu, orderGauss);
          
          lowerZZStiff = new FormulationQ3DErzEphiStiffness(lowerZZ, hGrad, hCurl, 
                                                     n, Material::LowerZZ::Nu, orderGauss);
          lowerZZMass  = new FormulationQ3DErzEphiMass(lowerZZ, hGrad, hCurl, 
                                                     n, Material::LowerZZ::Nu, orderGauss);
          
          lowerRZStiff = new FormulationQ3DErzEphiStiffness(lowerRZ, hGrad, hCurl, 
                                                     n, Material::LowerRZ::Nu, orderGauss);
          lowerRZMass  = new FormulationQ3DErzEphiMass(lowerRZ, hGrad, hCurl, 
                                                     n, Material::LowerRZ::Nu, orderGauss);
          
          upperRZStiff = new FormulationQ3DErzEphiStiffness(upperRZ, hGrad, hCurl, 
                                                     n, Material::UpperRZ::Nu, orderGauss);
          upperRZMass  = new FormulationQ3DErzEphiMass(upperRZ, hGrad, hCurl, 
                                                     n, Material::UpperRZ::Nu, orderGauss);
          
          upperZZStiff = new FormulationQ3DErzEphiStiffness(upperZZ, hGrad, hCurl, 
                                                     n, Material::UpperZZ::Nu, orderGauss);
          upperZZMass  = new FormulationQ3DErzEphiMass(upperZZ, hGrad, hCurl, 
                                                     n, Material::UpperZZ::Nu, orderGauss);
          
          RRStiff = new FormulationQ3DErzEphiStiffness(RR, hGrad, hCurl, 
                                                     n, Material::RR::Nu, orderGauss);
          RRMass  = new FormulationQ3DErzEphiMass(RR, hGrad, hCurl, 
                                                     n, Material::RR::Nu, orderGauss);

          break; 
          
          default: 
          domainStiff = new FormulationQ3DEStarStiffness(dom, hGrad, hCurl, n, Material::Dom::NuRad, 
                                                         Material::Dom::NuInvRad, orderGauss);
          
          domainMass = new FormulationQ3DEStarMass(dom, hGrad, hCurl, Material::Dom::EpsilonRad, 
                                                   Material::Dom::EpsilonInvRad, orderGauss);
          
          
          
          sphereStiff = new FormulationQ3DEStarStiffness(sphere, hGrad, hCurl, n, Material::Sphere::NuRad, 
                                                         Material::Sphere::NuInvRad, orderGauss);
          
          sphereMass = new FormulationQ3DEStarMass(sphere, hGrad, hCurl, Material::Sphere::EpsilonRad, 
                                                   Material::Sphere::EpsilonInvRad, orderGauss);
          
          
          
          lowerZZStiff = new FormulationQ3DEStarStiffness(lowerZZ, hGrad, hCurl, n, Material::LowerZZ::NuRad, 
                                                          Material::LowerZZ::NuInvRad, orderGauss);
          
          lowerZZMass = new FormulationQ3DEStarMass(lowerZZ, hGrad, hCurl, Material::LowerZZ::EpsilonRad, 
                                                    Material::LowerZZ::EpsilonInvRad, orderGauss);
          
          
          
          lowerRZStiff = new FormulationQ3DEStarStiffness(lowerRZ, hGrad, hCurl, n, Material::LowerRZ::NuRad, 
                                                          Material::LowerRZ::NuInvRad, orderGauss);
          
          lowerRZMass = new FormulationQ3DEStarMass(lowerRZ, hGrad, hCurl, Material::LowerRZ::EpsilonRad, 
                                                Material::LowerRZ::EpsilonInvRad, orderGauss);
          
          
          
          upperRZStiff = new FormulationQ3DEStarStiffness(upperRZ, hGrad, hCurl, n, Material::UpperRZ::NuRad, 
                                                          Material::UpperRZ::NuInvRad, orderGauss);
          
          upperRZMass = new FormulationQ3DEStarMass(upperRZ, hGrad, hCurl, Material::UpperRZ::EpsilonRad, 
                                                    Material::UpperRZ::EpsilonInvRad, orderGauss);
          
          
          
          upperZZStiff = new FormulationQ3DEStarStiffness(upperZZ, hGrad, hCurl, n, Material::UpperZZ::NuRad, 
                                                          Material::UpperZZ::NuInvRad, orderGauss);
          
          upperZZMass = new FormulationQ3DEStarMass(upperZZ, hGrad, hCurl, Material::UpperZZ::EpsilonRad, 
                                                    Material::UpperZZ::EpsilonInvRad, orderGauss);
          
          
          
          RRStiff = new FormulationQ3DEStarStiffness(RR, hGrad, hCurl, n, Material::RR::NuRad, 
                                                     Material::RR::NuInvRad, orderGauss);
          
          RRMass = new FormulationQ3DEStarMass(RR, hGrad, hCurl, Material::RR::EpsilonRad, 
                                               Material::RR::EpsilonInvRad, orderGauss);
      break; 
          
  }
  
  
  
  system.addFormulation(*domainStiff);
  system.addFormulationB(*domainMass);
  system.addFormulation(*sphereStiff);
  system.addFormulationB(*sphereMass);
  system.addFormulation(*lowerZZStiff);
  system.addFormulationB(*lowerZZMass);
  system.addFormulation(*lowerRZStiff);
  system.addFormulationB(*lowerRZMass);
  system.addFormulation(*upperRZStiff);
  system.addFormulationB(*upperRZMass);
  system.addFormulation(*upperZZStiff);
  system.addFormulationB(*upperZZMass);
  system.addFormulation(*RRStiff);
  system.addFormulationB(*RRMass);
  
  //PEC is the same for all trafos 
  SystemHelper<Complex>::dirichlet(system, hCurl, nonsymm, fZeroVect);
  SystemHelper<Complex>::dirichlet(system, hGrad, nonsymm, fZeroScal);
  
  // bc dependent on ansatz 
    switch(ansatz) {
      case 0: 
          SystemHelper<Complex>::dirichlet(system, hGrad, symm, fZeroScal);
          if(n > 0){
             cout<< "WARNING: dirichlet on symm for OutOfPlane" << endl;
             SystemHelper<Complex>::dirichlet(system, hCurl, symm, fZeroVect);
          }
          
          break; 
             
      case 1: 
          if(n == 0){
              if(beta < 1.5) {
                    cout<< "WARNING: dirichlet on symm for OutOfPlane" << endl;
                    SystemHelper<Complex>::dirichlet(system, hGrad, symm, fZeroScal);
              }
          }
          else {
              if ((n == 1 && beta < 0.5) || (n > 1 && beta <= 1)){
                  cout<< "WARNING: dirichlet on symm for OutOfPlane" << endl;
                  SystemHelper<Complex>::dirichlet(system, hGrad, symm, fZeroScal);
              }
          }
          break; 
          
      case 2: 
          if(n != 1 && n != -1){
             cout<< "WARNING: dirichlet on symm for OutOfPlane" << endl;
             SystemHelper<Complex>::dirichlet(system, hGrad, symm, fZeroScal); 
          }
          break; 
          
      case 3:
          if(n != 1 && n != -1){
              cout<< "WARNING: dirichlet on symm for OutOfPlane" << endl;
             SystemHelper<Complex>::dirichlet(system, hGrad, symm, fZeroScal); 
          }
          break; 
  }
  
  cout << "Assembling..." << endl; 
  system.assemble();
          
  // Set solver parameters //
  system.setWhichEigenpairs("target_magnitude");
  system.setTarget(Complex(shift, 0));
  system.setNumberOfEigenValues(nEig);
  system.setTolerance(tol);

  // Header output //
  cout << "Quasi3D PML: (ansatz: " << ansatzMap[ansatz] <<  ", alpha: "<< alpha << ", beta: " << beta << ", shift: " << shift <<  ", n: " << n << ", Order HGrad: " << orderHGrad << ", Order HCurl: " << orderHCurl <<") , " << system.getSize() << endl << flush;

  cout << "Solving..." << endl;
  system.solve();

  // Display //
  fullVector<Complex> eigenValue;
  
  //get number of computed eigenvalues
  const size_t nEigenValue = system.getNComputedSolution();
  system.getEigenValues(eigenValue);

  cout << "Number of found Eigenvalues: " << nEigenValue
       << endl
       << endl
       << "Number\tomega = sqrt(Eigenvalue)" << endl;

  // Console Output of eigenvalues // 
  for(size_t i = 0; i < nEigenValue; i++)
    cout << setprecision (16) << "#" << i + 1  << "\t"
         << sqrt(eigenValue(i)) << endl;
         
  // Dump eigenvalues into file // 
  ofstream fileEVReal;
  fileEVReal.open("./eigenReal.txt"); 
  
  for(size_t i = 0; i < nEigenValue; i++){
      if (fileEVReal.is_open()) {
          fileEVReal << setprecision (16) << sqrt(eigenValue(i).real()) ;
          fileEVReal << "\n";
      } else {
          cout << "Unable to open file."; 
      } 
  }    
  fileEVReal.close();  
  
  ofstream fileEVImag;
  fileEVImag.open("./eigenImag.txt"); 
  
  for(size_t i = 0; i < nEigenValue; i++){
      if (fileEVImag.is_open()) {
          fileEVImag << setprecision (16) << sqrt(abs(eigenValue(i).imag())) ;
          fileEVImag << "\n";
      } else {
          cout << "Unable to open file."; 
      } 
  }    
  fileEVImag.close();  

//   // Dump eigenvector into .csv // 
//   for(size_t i = 0; i < nEigenValue; i++){
//   fullVector<Complex> sol;
//   system.getSolution(sol, i);
//   ofstream evFile;
// 
//   evFile.open(string("./ev").append(string(std::to_string(i)).append(string(".csv"))));
//   for(int j = 0; j < sol.size() - 1; j++)
//       evFile  << sol(j).real() << ",";
// 
//   evFile  << sol(sol.size() - 1).real();
//   evFile << endl;
//   evFile.close();
//   }
  
  // Write Gmsh output files //
  try{
    option.getValue("-nopos");
  }
  catch(...){
    //get solutions and write pos file
    FEMSolution<Complex> feSolCurl;
    system.getSolution(feSolCurl, hCurl, domain);
    feSolCurl.write("eigenModesCurl" + ansatzMap[ansatz]);

    FEMSolution<Complex> feSolGrad;
    system.getSolution(feSolGrad, hGrad, domain);
    feSolGrad.write("eigenModesGrad" + ansatzMap[ansatz]);

    TextSolution txtSol;
    for(size_t i = 0; i < nEigenValue; i++){
      stringstream stream;
      stream << "omega = sqrt(Eigenvalue) " << i + 1 << ": " << sqrt(eigenValue(i)) << " ";

      txtSol.addValues(i * 2 + 0, stream.str().append("(real part)"));
      txtSol.addValues(i * 2 + 1, stream.str().append("(imaginary part)"));
    }
    txtSol.write("eigenModesTxt" + ansatzMap[ansatz]);
  }
}

int main(int argc, char** argv){
  // Init SmallFem //
  SmallFem::Keywords("-msh,-shift,-nEig,-tol,-alpha,-beta,-n,-nopos,-oHC,-oHG,-oGauss,-ansatz");
  SmallFem::Initialize(argc, argv);

  compute(SmallFem::getOptions());

  SmallFem::Finalize();
  return 0;
}

